
package com.pearl.accounting.syscore.services.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
//import com.pearl.theadping.sysui.client.views.windows.DoubleEntryWindow;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.TheadpDao;
import com.pearl.accounting.syscore.services.AccountService;
import com.pearl.accounting.syscore.services.ClientStatementService;
import com.pearl.accounting.syscore.services.CustomerAccountService;
import com.pearl.accounting.syscore.services.FixedDataService;
import com.pearl.accounting.syscore.services.GeneralLedgerService;
import com.pearl.accounting.syscore.services.LoanService;
import com.pearl.accounting.syscore.services.ReceiptnoService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.syscore.services.TheadpService;
import com.pearl.accounting.syscore.services.TransGLService;
import com.pearl.accounting.syscore.services.TransMLService;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.FixedData;
import com.pearl.accounting.sysmodel.LoanDeposit;
import com.pearl.accounting.sysmodel.Receiptno;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.Theadp;

@Service("TheadpService")
@Transactional
public class TheadpServiceImpl implements TheadpService {
	@Autowired
	private TheadpDao theadpDao;

	@Autowired
	private LoanService loanService;
	@Autowired
	private TransGLService transglService;
	@Autowired
	private TransMLService transmlService;
	@Autowired
	private GeneralLedgerService generalLedgerService;
	@Autowired
	private SystemUserService systemUserService;
	@Autowired
	private ClientStatementService clientStatementService;
	@Autowired
	private AccountService accountService;
	@Autowired
	private CustomerAccountService customeraccountService;

	@Autowired
	private FixedDataService fixeddataService;
//	@Autowired
//	private AccountDao accountDao;
//
//	
	@Autowired
	private ReceiptnoService receiptnoService;

	public SwizFeedback save(Theadp theadp) {
		try {
//             

			theadp.setAcode(account1(theadp));
			theadp.setCreatedBy(systemUserService.getLoggedInUser());
			theadp.setUpdatedBy(systemUserService.getLoggedInUser());
			theadp.setAcctNo(accno(theadp));
			theadp.setHDNO(getthead());
			theadpDao.save(theadp);
			fixeddataService.update(gettheadupdate());
			receiptnoService.save(generateReceiptno(theadp));

			return new SwizFeedback(true, "Transaction posted successfully");

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}

	}

	public SwizFeedback update(Theadp theadp) {
		try {

			Theadp ledger = find(theadp.getHDNO());
			if (ledger != null) {
				theadp.setCreatedBy(ledger.getCreatedBy());
				theadp.setUpdatedBy(systemUserService.getLoggedInUser());
				theadp.setDateCreated(ledger.getDateCreated());
				theadp.setDateUpdated(new Date());
                
				theadpDao.update(theadp);
				return new SwizFeedback(true, "Transaction Updated successfully");
			} else {
				return new SwizFeedback(false, "Transaction does not exist in the general ledger");
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public SwizFeedback delete(Theadp theadp) {
		try {
			Theadp ledger = find(theadp.getHDNO());
			if (ledger != null) {

				ledger.setUpdatedBy(systemUserService.getLoggedInUser());
				ledger.setDateUpdated(new Date());
				ledger.setStatus(Status.DELETED);
				theadpDao.update(theadp);
				return new SwizFeedback(true, "Transaction Deleted successfully");
			} else {
				return new SwizFeedback(false, "Transaction does not exist in the general ledger");
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public List<Theadp> find() {
		try {
			Search search = new Search();
			search.addFilterEqual("status", Status.ACTIVE);
			return theadpDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	public Theadp find(String HDNO) {
		try {
			return theadpDao.find(HDNO);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

//	public int gettheadp() {
//		int count = 0;
//		try {
//               
//			FixedData hdno = fixeddataService.find("1");
//        int had =  hdno.getTheadid()+1;
//       
//			
//        count= had;
//		} catch (Exception ex) {
//
//		}
//       System.out.println(count);
//		return count;
//	}
	/*
	 * public FixedData gettheadupdate() {
	 * 
	 * FixedData fix = fixeddataService.find("1"); if (fix.getTheadid()>0) {
	 * 
	 * int thead = fix.getTheadid() + 1; fix.setIDNo(fix.getIDNo());
	 * fix.setTheadid(thead);
	 * System.out.println("here is were the new thead  "+thead +
	 * "colum add up in fixeddata" + fix.getTheadid()); } else {
	 * System.out.println("empty here didn't get thead "); } return fix;
	 * 
	 * }
	 */
	public String getthead() {
		String thead = null;
		FixedData fix = fixeddataService.find("1");
		int theads = fix.getTheadid();
		thead = String.valueOf(theads);
		//int newthead = theads + 1;
//		fix.setIDNo(fix.getIDNo(public String currenthead(Theadp thead) {
//		String theadmax = null;
//		int maxd = Integer.parseInt(getthead()) + 1;
//		theadmax = String.valueOf(maxd);
//		return theadmax;
//	));
//		fix.setTheadid(newthead);
//		fixeddataService.update(fix);  
		return thead;
	}

	 public FixedData gettheadupdate() {
		  
		  FixedData fix = fixeddataService.find("1"); 
		  int theads1 = Integer.parseInt(getthead());

		  if ((getthead()!=null)&&(theads1>0)) {
			  
		  int thead = theads1 + 1; 
		  fix.setIDNo(fix.getIDNo());
		  fix.setTheadid(thead);	  
		  System.out.println("here is were the new thead  "+thead +
		  "colum add up in fixeddata" + getthead());
		  } 
		  else {
		 System.out.println("empty here didn't get thead ");
		 }
		  return fix;
		
	 }

//	public String gethdno() {
//		String hdbno = null ;
//		try {
//			int dr = getLoanCount() ;
//			hdbno=String.valueOf(getLoanCount());
//			
//		}catch(Exception ex) 
//		
//		}
//		return hdbno;
//	}
//	

	public String account1(Theadp theadp) {
		String loanNo = null;
		try {
			if (theadp.getAccount() != null) {
				Account acode = accountService.find(theadp.getAccount().getSysid());
				if (acode != null) {
					loanNo = acode.getACODE();
				}

			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return loanNo;
	}

	public String accno(Theadp theadp) {
		String acc = null;
		try {
			if (theadp.getCustomerAccount() != null) {
				CustomerAccount customer = customeraccountService.find(theadp.getAccount().getSysid());
				if (customer.getClientCode() != null) {
					acc = customer.getClientCode();
				}
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return acc;
	}

	public String getrtno(Theadp theadp) {
		String answer = null;

		Date date = theadp.getTdates();
		// Date date = Calendar.getInstance().getTime();
		DateFormat dateFormat = new SimpleDateFormat("ddMyyyy");
		String strDate = dateFormat.format(date);
		String thead = theadp.getHDNO();
		String cons = "RT";
		answer = cons + strDate + "/" + thead;
		System.out.println("Converted String: " + answer);

		return answer;
	}

	public Receiptno generateReceiptno(Theadp theadp) {
		Receiptno receiptno = new Receiptno();
		/// int total = (int)(theadp.getLoandeposit()+loandeposit.getInterestdeposit());
		receiptno.setAmount(theadp.getAmount());
		NumberToWord words = new NumberToWord();
		receiptno.setAMTWORDS(words.convert((int) theadp.getAmount()));
		receiptno.setTheadID(Integer.parseInt(theadp.getHDNO()));
		receiptno.setRTVRNO(getrtno(theadp));

		return receiptno;
	}

}

//private TransML generatememberstmc(Theadp payment) {
//	TransML gl = new TransML();
//	gl.setCreatedBy(systemUserService.getLoggedInUser());
//	gl.setUpdatedBy(systemUserService.getLoggedInUser());
//	gl.setDateCreated(new Date());
//	gl.setDateUpdated(new Date());
//	gl.setStatus(Status.ACTIVE);
//	gl.setDataSourceScreen(DataSourceScreen.DoubleEntry);
//	gl.setMEMBERID(payment.getAcctNo());
//	gl.setCustomeraccount(payment.getCustomerAccount());
//	
//	gl.setFSAVE(Fsave.Y);
////	gl.setPRODUCTID(generateacodeproducttype(payment));
//	int loanid = Integer.parseInt(payment.getLoanNumber());
//
//	gl.setLOANID(loanid);
//	gl.setTDATE(payment.getTdates());
//	gl.setTTYPE(Ttype.C);
//	gl.setAMOUNT(payment.getAmount());
//	gl.setPAYDET(payment.getRemarks());
//	gl.setVNO(payment.getVNO());
//	gl.setJNLID(0);
//	gl.setTHEADID(payment.getHDNO());
//	gl.setLoan(payment.getLoanid());
//
//	SystemUser staff = systemUserService.find(payment.getUpdatedBy().getId());
//	String name = staff.getUsername();
//	gl.setSTAFFNAME(name);
//	gl.setCustomeraccount(payment.getCustomerAccount());
//
//	return gl;
//}
//private TransGL generateGLc(Theadp payment) {
//	TransGL gl = new TransGL();
//	gl.setDateCreated(new Date());
//	gl.setDateUpdated(new Date());
//	gl.setDataSourceScreen(DataSourceScreen.DoubleEntry);
//	gl.setCreatedBy(systemUserService.getLoggedInUser());
//	gl.setUpdatedBy(systemUserService.getLoggedInUser());
//	gl.setStatus(Status.ACTIVE);
//	gl.setACODE(payment.getAcodeC());
//	gl.setFsave(Fsave.Y);
//	gl.setAMOUNT(payment.getAmount());
//	gl.setAmtUsd(0);
//	gl.setRecAmt(0);
//	gl.setTDATE(payment.getTdates());
//	gl.setTTYPE(Ttype.C);
//	gl.setPAYDET(payment.getVNO());
//	gl.setREMARKS(payment.getRemarks());
//	gl.setICODE(payment.getPeriodcode());
//	gl.setYRCODE(payment.getYear());
//	gl.setPAYPERSON(payment.getRemarks());
//	gl.setMemberID(payment.getAcctNo());
//	gl.setTheadID(payment.getHDNO());
//	// gl.setJNLNO(0);
//	gl.setYrRec(0);
//	gl.setTrec(0);
//	gl.setAccount(payment.getDebit());
//	SystemUser staff = systemUserService.find(payment.getUpdatedBy().getId());
//	String name = staff.getUsername();
//	gl.setStaffName(name);
//	if (payment.getCustomerAccount() != null) {
//
//		gl.setCustomeraccount(payment.getCustomerAccount());
//	}
//
//	return gl;
//}
