package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.MultiDao;
import com.pearl.accounting.sysmodel.Multi;

@Repository("MultiDao")
public class MultiDaoImpl extends SwiziBaseDaoImpl<Multi> implements MultiDao {

}
