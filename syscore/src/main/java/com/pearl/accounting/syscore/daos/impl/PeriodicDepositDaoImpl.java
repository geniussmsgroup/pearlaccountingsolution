package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.PeriodicDepositDao;
import com.pearl.accounting.sysmodel.PeriodicDeposit;

@Repository("PeriodicDepositDao")
public class PeriodicDepositDaoImpl extends SwiziBaseDaoImpl<PeriodicDeposit> implements PeriodicDepositDao{

}
