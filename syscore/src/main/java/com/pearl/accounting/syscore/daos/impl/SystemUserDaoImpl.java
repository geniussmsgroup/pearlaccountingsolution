package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.SystemUserDao;
import com.pearl.accounting.sysmodel.SystemUser;
 

@Repository("SystemUserDao")
public class SystemUserDaoImpl extends SwiziBaseDaoImpl<SystemUser> implements SystemUserDao {

}
