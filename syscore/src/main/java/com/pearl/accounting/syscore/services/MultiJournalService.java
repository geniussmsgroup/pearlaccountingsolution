package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.MultiJournal;
import com.pearl.accounting.sysmodel.MultiJournalDetail;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysmodel.RepaymentSchedule;

public interface MultiJournalService {

	public SwizFeedback save(MultiJournal multiJournal);

	public SwizFeedback update(MultiJournal multiJournal);

	public SwizFeedback delete(MultiJournal multiJournal);

	public List<MultiJournal> find();

	public MultiJournal find(String id);

	public List<MultiJournalDetail> find(MultiJournal multiJournal);
}
