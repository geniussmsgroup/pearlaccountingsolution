package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.CashBookPaymentDao;
import com.pearl.accounting.sysmodel.CashBookPayment;

@Repository("CashBookPaymentDao")
public class CashBookPaymentDaoImpl extends SwiziBaseDaoImpl<CashBookPayment> implements CashBookPaymentDao {

}
