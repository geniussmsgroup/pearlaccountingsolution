package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.UserRole;

public interface UserRoleService {
	
	public SwizFeedback save(UserRole userRole);

	public SwizFeedback update(UserRole userRole);

	public SwizFeedback delete(UserRole userRole);

	public List<UserRole> find();

	public UserRole find(String id);

}
