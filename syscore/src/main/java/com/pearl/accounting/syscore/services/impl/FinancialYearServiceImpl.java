package com.pearl.accounting.syscore.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.FinancialYearDao;
import com.pearl.accounting.syscore.services.FinancialYearService;
import com.pearl.accounting.sysmodel.FinancialYear;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.SwizFeedback;

@Service("FinancialYearService")
@Transactional
public class FinancialYearServiceImpl implements FinancialYearService {

	@Autowired
	private FinancialYearDao financialYearDao;

	public SwizFeedback save(FinancialYear financialYear) {
		try {
			financialYearDao.save(financialYear);
			return new SwizFeedback(true, "Saved successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}

	}

	public SwizFeedback update(FinancialYear financialYear) {
		try {
			financialYearDao.update(financialYear);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public SwizFeedback delete(FinancialYear financialYear) {
		try {
			financialYearDao.delete(financialYear);
			return new SwizFeedback(true, "Deleted successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public List<FinancialYear> find() {
		try {

			return financialYearDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public FinancialYear find(String id) {
		try {
			return financialYearDao.find(id);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public SwizFeedback activate(FinancialYear financialYear) {
		try {
			FinancialYear year = find(financialYear.getId());
			if (year != null) {
				year.setActivationStatus(Status.ACTIVE);
				financialYearDao.save(year);
				return new SwizFeedback(true, "Activated successfully");
			} else {
				return new SwizFeedback(true, "Financial year does not exist");
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}

	}

	public SwizFeedback deactivate(FinancialYear financialYear) {
		try {
			FinancialYear year = find(financialYear.getId());
			if (year != null) {
				year.setActivationStatus(Status.IN_ACTIVE);
				financialYearDao.save(year);
				return new SwizFeedback(true, "De-activated successfully");
			} else {
				return new SwizFeedback(true, "Financial year does not exist");
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}

	}

	public FinancialYear findActive() {
		try {
			Search search = new Search();
			search.addFilterEqual("activationStatus", Status.ACTIVE);
			List<FinancialYear> financialYears = financialYearDao.search(search);
			if (financialYears != null) {
				if (!financialYears.isEmpty()) {
					return financialYears.get(0);
				}
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

}
