package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.Receiptno;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface ReceiptnoService {

	public SwizFeedback save(Receiptno account);

	public SwizFeedback update(Receiptno account);

	public SwizFeedback delete(Receiptno account);

	public List<Receiptno> find();
   /// public Account findcbal(Float cbal  );
	public Receiptno find(String Sysid);
	

//	public Account findByName(String accountName);
//
//	public Account findByName2(String ACODE);
//	public String AtypeA(Account account);
//	public String productid(Account account);
}
