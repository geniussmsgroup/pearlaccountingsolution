package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.TransMLDao;
import com.pearl.accounting.sysmodel.TransML;

@Repository("TransMLDao")

public class TransMLDaoImpl extends SwiziBaseDaoImpl<TransML> implements TransMLDao{

}
