package com.pearl.accounting.syscore.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.SystemUserDao;
import com.pearl.accounting.syscore.services.EmailNotificationConfigurationService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.syscore.services.UserPermissionService;
import com.pearl.accounting.sysmodel.ConfigRole;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.SystemUser;
import com.pearl.accounting.sysmodel.UserPermission; 

@Service("SystemUserService")
@Transactional
public class SystemUserServiceImpl implements SystemUserService {

	@Autowired
	private SystemUserDao systemUserDao;

	@Autowired
	private UserPermissionService permissionService;
	
	@Autowired
	private EmailNotificationConfigurationService emailNotificationConfigurationService;

	public SwizFeedback save(SystemUser user) {
		try {
			if (findByUserName(user.getUsername()) == null) {

				String password=user.getPassword();
				String encodedPassword = encodePassword(user.getPassword());
				
				if (encodedPassword != null) {
					System.out.println("Trying to create user 2");
					user.setPassword(encodedPassword);
					user.setRole(ConfigRole.ROLE_USER);
					SystemUser savedUser = systemUserDao.save(user);
					System.out.println("Trying to create user3");
					
					/*if (savedUser!=null) {

							String mailRecipient = savedUser.getEmailAddress();
							String recipientName = savedUser.getFirstName() + " " + savedUser.getLastName();
							String notificationMessage = "Your user account has been created in savings & credit management tool. \nUsername: " + user.getUsername() + "\nPassword: " + password
									+ "\nPlease login and change your password.";

							emailNotificationConfigurationService.sendMail(mailRecipient, recipientName, notificationMessage);
					}*/
					return new SwizFeedback(true, "Successfully saved");

				}
			} else {
				return new SwizFeedback(false, "ERROR: Username already exists.");
			}
		} catch (Exception ex) {
			System.out.println("Trying to create user error 4");

			return new SwizFeedback(false, ex.getMessage());
		}

		System.out.println("Trying to create user error 5");

		return new SwizFeedback(false, "Error Trying to create user account");

	}

	public SwizFeedback update(SystemUser systemUser) {
		try {
			SystemUser userToEdit = find(systemUser.getId());
			if (userToEdit != null) {

				userToEdit.setDateUpdated(new Date());
				userToEdit.setEmailAddress(systemUser.getEmailAddress());

				userToEdit.setFirstName(systemUser.getFirstName());
				userToEdit.setLastName(systemUser.getLastName());
				userToEdit.setPhoneNumber(systemUser.getPhoneNumber()); 
				userToEdit.setUpdatedBy(getLoggedInUser());
				userToEdit.setUsername(systemUser.getUsername());
				userToEdit.setUserRole(systemUser.getUserRole());

				SystemUser savedUser =systemUserDao.update(userToEdit);
				/*
				if (savedUser!=null) {

					String mailRecipient = savedUser.getEmailAddress();
					String recipientName = savedUser.getFirstName() + " " + savedUser.getLastName();
					String notificationMessage = "Your user account has been updated in savings & credit management tool. " 
							+ "\nPlease login to review the changes effected.";

					emailNotificationConfigurationService.sendMail(mailRecipient, recipientName, notificationMessage);
			}*/
				return new SwizFeedback(true, "Updated successfully");
			} else {
				return new SwizFeedback(false,
						"Selected user doesnt not exist. Please contact your system administrator");
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public SwizFeedback delete(SystemUser systemUser) {
		try {
			SystemUser userToDelete = find(systemUser.getId());
			if(userToDelete!=null){
				userToDelete.setStatus(Status.DELETED);
				systemUserDao.delete(userToDelete);
				return new SwizFeedback(true, "Saved successfully");
			}else{
				return new SwizFeedback(false, "Selected user does not exist.");
			}
			
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public List<SystemUser> find() {
		try {
			Search search=new Search();
			search.addFilterEqual("status", Status.ACTIVE);
			return systemUserDao.search(search);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public SystemUser find(String id) {
		try {
			return systemUserDao.find(id);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	@Transactional(readOnly = true)
	public SystemUser findByUserName(String username) {
		try {
			Search search = new Search();
			search.addFilterEqual("username", username);
			return systemUserDao.searchUnique(search);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	private String encodePassword(String password) {
		String encodedPassword = null;
		try {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			encodedPassword = passwordEncoder.encode(password);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

		return encodedPassword;
	}

	public SystemUser getLoggedInUser() {
		org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder
				.getContext().getAuthentication().getPrincipal();
		String username = user.getUsername(); // get logged in username
		return findByUserName(username);
	}

	public List<UserPermission> getLoginUserPermissions() {
		List<UserPermission> list = new ArrayList<UserPermission>();
		try {
			SystemUser systemUser = getLoggedInUser();
			if (systemUser != null) {
				list = permissionService.find(systemUser.getUserRole());
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

		return list;

	}
	
	@Transactional
	public SwizFeedback resetUserPassword(SystemUser usr) {
		try {

			SystemUser user = find(usr.getId());
			if (user != null) {
				String encodedPassword = encodePassword("password");
				user.setPassword(encodedPassword);
				systemUserDao.update(user);
				
				 

					String mailRecipient = user.getEmailAddress();
					String recipientName = user.getFirstName() + " " + user.getLastName();
					String notificationMessage = "Your passoword has been reset in OPM refugee M&E. \nUsername: " + user.getUsername() + "\nPassword: password"
							+ "\nPlease login and change your password.";

					emailNotificationConfigurationService.sendMail(mailRecipient, recipientName, notificationMessage);
				 
				
				return new SwizFeedback(true,"Password reset was successful");
			}else{
				return new SwizFeedback(false,"ERROR: Password reset was NOT successful");
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage()); 
			return new SwizFeedback(false,ex.getMessage());
		}
		 
	}

}
