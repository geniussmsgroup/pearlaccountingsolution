package com.pearl.accounting.syscore.daos;

import com.pearl.accounting.sysmodel.FixedData;

public interface FixedDataDao extends SwiziBaseDao<FixedData> {

}
