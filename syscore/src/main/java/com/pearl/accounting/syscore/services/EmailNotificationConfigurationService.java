package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.EmailNotificationConfiguration;
 
  

public interface EmailNotificationConfigurationService {

	public boolean save(EmailNotificationConfiguration configuration);

	public boolean update(EmailNotificationConfiguration configuration);

	public boolean delete(EmailNotificationConfiguration configuration);

	public EmailNotificationConfiguration find(String id);

	public List<EmailNotificationConfiguration> find(); 
	
	public boolean sendMail(String mailRecipient, String recipientName, String notificationMessage);
}
