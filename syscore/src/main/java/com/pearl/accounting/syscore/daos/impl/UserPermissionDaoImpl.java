package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.UserPermissionDao;
import com.pearl.accounting.sysmodel.UserPermission;
 

@Repository("UserPermissionDao")
public class UserPermissionDaoImpl extends SwiziBaseDaoImpl<UserPermission> implements UserPermissionDao {

}
