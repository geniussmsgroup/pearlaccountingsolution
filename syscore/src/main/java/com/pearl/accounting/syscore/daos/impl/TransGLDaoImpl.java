package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.TransGLDao;
import com.pearl.accounting.sysmodel.TransGL;

@Repository("TransGLDao")

public class TransGLDaoImpl extends SwiziBaseDaoImpl<TransGL> implements TransGLDao{

}
