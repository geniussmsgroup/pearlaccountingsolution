package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.Intervaltb;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface IntervaltbService {

	public SwizFeedback save(Intervaltb interval);

	public SwizFeedback update(Intervaltb interval);

	public SwizFeedback delete(Intervaltb interval);

	public List<Intervaltb> find();

	public Intervaltb find(String Sysid);
	public  SwizFeedback  activeperiod(Intervaltb intervaltb);
	
	public Intervaltb findByName(String accountName);
	public Intervaltb findByName2(String accountName);

	public List<Intervaltb> findActive();
}
