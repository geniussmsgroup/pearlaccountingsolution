package com.pearl.accounting.syscore.daos;

import com.pearl.accounting.sysmodel.RepaymentSchedule;

public interface RepaymentScheduleDao extends SwiziBaseDao<RepaymentSchedule>{

}
