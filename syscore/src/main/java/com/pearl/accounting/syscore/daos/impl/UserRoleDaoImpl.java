package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.UserRoleDao;
import com.pearl.accounting.sysmodel.UserRole;
 

@Repository("UserRoleDao")
public class UserRoleDaoImpl extends SwiziBaseDaoImpl<UserRole> implements UserRoleDao {

}
