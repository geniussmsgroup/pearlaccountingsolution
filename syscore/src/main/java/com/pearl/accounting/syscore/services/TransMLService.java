package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.ClientStatement;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.TransML;

public interface TransMLService {

	public SwizFeedback save(TransML transml);

	public SwizFeedback update(TransML transml);

	public SwizFeedback delete(TransML transml);

	public List<TransML> find();

	public TransML find(String Sysid);
	
	public SwizFeedback save(List<TransML> transml);

}
