package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.RepaymentScheduleDao;
import com.pearl.accounting.sysmodel.RepaymentSchedule;

@Repository("RepaymentScheduleDao")
public class RepaymentScheduleDaoImpl extends SwiziBaseDaoImpl<RepaymentSchedule> implements RepaymentScheduleDao{

}
