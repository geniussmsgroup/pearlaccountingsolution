package com.pearl.accounting.syscore.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.MultiDao;
import com.pearl.accounting.syscore.daos.MultiDetailDao;
import com.pearl.accounting.syscore.services.AccountService;
import com.pearl.accounting.syscore.services.ClientStatementService;
import com.pearl.accounting.syscore.services.GeneralLedgerService;
import com.pearl.accounting.syscore.services.MultiService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.syscore.services.TheadpService;
import com.pearl.accounting.syscore.services.TransGLService;
import com.pearl.accounting.syscore.services.TransMLService;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.ClientStatement;
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.DataSourceScreen;
import com.pearl.accounting.sysmodel.GeneralLedger;
import com.pearl.accounting.sysmodel.Multi;
import com.pearl.accounting.sysmodel.MultiDetail;
import com.pearl.accounting.sysmodel.MultiJournal;
import com.pearl.accounting.sysmodel.MultiJournalDetail;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.TransGL;
import com.pearl.accounting.sysmodel.TransML;
import com.pearl.accounting.sysmodel.Ttype;

@Service("MultiService")
@Transactional
public class MultiServiceImpl implements MultiService {

	@Autowired
	private MultiDao multiJournalDao;

	@Autowired
	private MultiDetailDao multiDetailDao;

	@Autowired
	private SystemUserService systemUserService;

	@Autowired
	private GeneralLedgerService generalLedgerService;

	@Autowired
	private ClientStatementService clientStatementService;
	@Autowired
	private TheadpService theadpService;
	@Autowired
	private AccountService accountService;
	@Autowired
	private TransGLService transglService;
	@Autowired
	private TransMLService transmlService;

	public SwizFeedback save(Multi multi) {

		try {

			multi.setCreatedBy(systemUserService.getLoggedInUser());
			multi.setUpdatedBy(systemUserService.getLoggedInUser());
			multi.setThead(theadpService.getthead());

			Multi journal = multiJournalDao.save(multi);

			if (journal != null) {

				SwizFeedback feedback = generalLedgerService.save(generateGeneralLedger(journal));
				// SwizFeedback feedback4 = transmlService.save(generateClientStm(journal));

				SwizFeedback feedback2 = clientStatementService.save(generateClientStatement(journal));
				// SwizFeedback feedback3 = transglService.save(generateTransgl(journal));

				if (feedback.isResponse() && feedback2.isResponse()) {

					return new SwizFeedback(true, "Transaction successfully posted");

				} else {
					return feedback;
				}

			} else {
				return new SwizFeedback(false, "Error occured while posting transaction. Please try again");
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}

	}

	// debit and credit the receiveing account and Paying account in the general
	// ledger

//	private List<TransGL> generateTransgl(Multi payment) {
//
//		List<TransGL> list = new ArrayList<TransGL>();
//
//		List<MultiDetail> details = findByPayment(payment);
//
//		for (MultiDetail detail : details) {
//
//			if (detail.getTtype() != null) {
//
//				TransGL generalLedger = new TransGL();
//				
//	
//				generalLedger.setAcode(detail.getAccount());
//				generalLedger.setAssessmentPeriod(payment.getAssessmentPeriod());
//				generalLedger.setBalanceCredit(0);
//				generalLedger.setBalanceDebt(0);
//				generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
//				generalLedger.setDateCreated(new Date());
//				generalLedger.setDateUpdated(new Date());
//				generalLedger.setFinancialYear(payment.getFinancialYear());
//				generalLedger.setPayDet(payment.getReceiptNumber());
//				generalLedger.setPostReferenceNo(detail.getId());
//				generalLedger.setDataSourceScreen(DataSourceScreen.MULTI_JOURNAL);
//				generalLedger.setRemarks(DataSourceScreen.MULTI_JOURNAL.getDataSourceScreen() + " :" + detail.getRemarks());
//				generalLedger.setStatus(Status.ACTIVE);
//                generalLedger.setTheadId(payment.getThead());
//				generalLedger.setTransactionDescription(detail.getAccount().getAccountName());
//				generalLedger.setTdate(payment.getPaymentDate());
//				generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
//
//				if (detail.getTtype().getTransactionType()
//						.equalsIgnoreCase(Ttype.C.getTransactionType())) {
//
//					generalLedger.setTtype(Ttype.C);
//					generalLedger.setTransactionCredit(detail.getAmountPaid());
//					generalLedger.setAmount(detail.getAmountPaid());
//					
//					generalLedger.setTransactionDebt(0);
//
//				} else {
//
//					generalLedger.setTtype(Ttype.D);
//					generalLedger.setTransactionCredit(0);
//					generalLedger.setTransactionDebt(detail.getAmountPaid());
//				    generalLedger.setAmount(detail.getAmountPaid());
//				}
//
//				list.add(generalLedger);
//
//			}
//
//		}
//
//		return list;
//	}
//	

	private List<GeneralLedger> generateGeneralLedger(Multi payment) {

		List<GeneralLedger> list = new ArrayList<GeneralLedger>();

		List<MultiDetail> details = findByPayment(payment);

		for (MultiDetail detail : details) {

			if (detail.getTtype() != null) {

				GeneralLedger generalLedger = new GeneralLedger();

				generalLedger.setAccount(detail.getAccount());
				generalLedger.setPeriod(payment.getPeriod());
				generalLedger.setBalanceCredit(0);
				generalLedger.setBalanceDebt(0);
				generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
				generalLedger.setDateCreated(new Date());
				generalLedger.setDateUpdated(new Date());
				generalLedger.setYear(payment.getYear());
				generalLedger.setPaymentDetails(payment.getTransactionDetails());
				generalLedger.setPostReferenceNo(detail.getId());
				generalLedger.setDataSourceScreen(DataSourceScreen.MULTI_JOURNAL);
				generalLedger
						.setRemarks(DataSourceScreen.MULTI_JOURNAL.getDataSourceScreen() + " :" + detail.getRemarks());
				generalLedger.setStatus(Status.ACTIVE);
				generalLedger.setThead(payment.getThead());

				generalLedger.setTransactionDescription(detail.getAccount().getAccountName());
				generalLedger.setTransationDate(payment.getPaymentDate());
				generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());

				if (detail.getTtype().getTransactionType().equalsIgnoreCase(Ttype.C.getTransactionType())) {

					generalLedger.setTtype(Ttype.C);
					generalLedger.setTransactionCredit(detail.getAmountPaid());
					generalLedger.setTransactionDebt(0);

				} else {

					generalLedger.setTtype(Ttype.D);
					generalLedger.setTransactionCredit(0);
					generalLedger.setTransactionDebt(detail.getAmountPaid());
				}

				list.add(generalLedger);

			}

		}

		return list;
	}

	private List<ClientStatement> generateClientStatement(Multi payment) {

		List<ClientStatement> list = new ArrayList<ClientStatement>();

		try {

			List<MultiDetail> details = findByPayment(payment);

			System.out.println("details: " + details.size());

			for (MultiDetail detail : details) {

				Account account = accountService.find(detail.getAccount().getSysid());

				if (account != null) {

					System.out.println("account tract: " + account.getAnalReq().equalsIgnoreCase("Y"));

					if (account.getAnalReq().equalsIgnoreCase("Y")) {

						ClientStatement clientStatement = new ClientStatement();

						clientStatement.setPeriod(payment.getPeriod());
						clientStatement.setBalanceCredit(0);
						clientStatement.setBalanceDebt(0);
						clientStatement.setCreatedBy(systemUserService.getLoggedInUser());
						clientStatement.setDateCreated(new Date());
						clientStatement.setDateUpdated(new Date());
						clientStatement.setYear(payment.getYear());

						if (account.getProductType() != null) {
							// clientStatement.setPaydet(account.getProductType().getProductType());
							clientStatement.setProductType(account.getProductType().getProductId());
						}

						clientStatement.setPostReferenceNo(payment.getId());
						clientStatement.setRemarks(detail.getRemarks());
						clientStatement.setStatus(Status.ACTIVE);

						System.out.println("details: 1");

						clientStatement.setTransactionDescription(account.getAccountName());
						clientStatement.setTtype(detail.getTtype());
						clientStatement.setTransationDate(payment.getPaymentDate());
						clientStatement.setUpdatedBy(systemUserService.getLoggedInUser());

						// System.out.println("details: 2");

						if (detail.getTtype().getTransactionType().equalsIgnoreCase(Ttype.C.getTransactionType())) {
							clientStatement.setTransactionCredit(detail.getAmountPaid());
							clientStatement.setTransactionDebt(0);
							// System.out.println("details: 3");
						} else {
							clientStatement.setTransactionDebt(detail.getAmountPaid());
							clientStatement.setTransactionCredit(0);
							// System.out.println("details: 4");
						}

						list.add(clientStatement);

					} else {

						System.out.println("isTrackPData null");
					}

				} else {

					System.out.println("account null");
				}

			}

			System.out.println("list: " + list.size());
		} catch (Exception ex) {
			System.out.println("details: " + ex.getMessage());
		}
		return list;
	}

//	private List<TransML> generateClientStm(Multi payment) {
//
//		List<TransML> list = new ArrayList<TransML>();
//
//		List<MultiDetail> details = findByPayment(payment);
//
//		System.out.println("details: " + details.size());
//
//		for (MultiDetail detail : details) {
//
//			Account account = accountService.find(detail.getAccount().getId());
//
//			if (account != null) {
//
//				System.out.println("account tract: " + account.isTrackPData());
//				
//				if (account.isTrackPData()) {
//
//					TransML clientStatement = new TransML();
//					
//					
//					
//					clientStatement.setCustomerAccount(detail.getCustomerAccount());
//					clientStatement.setAssessmentPeriod(payment.getAssessmentPeriod());
//					clientStatement.setBalanceCredit(0);
//					clientStatement.setBalanceDebt(0);
//					clientStatement.setCreatedBy(systemUserService.getLoggedInUser());
//					clientStatement.setDateCreated(new Date());
//					clientStatement.setDateUpdated(new Date());
//					clientStatement.setFinancialYear(payment.getFinancialYear());
//					clientStatement.setPayDet(account.getProductType().getProductType());
//					clientStatement.setPostReferenceNo(payment.getId());
//					clientStatement.setProductType(account.getProductType());
//					clientStatement.setRemarks(detail.getRemarks());
//					clientStatement.setStatus(Status.ACTIVE);
//					clientStatement.setThead(payment.getThead());
//
//
//					clientStatement.setTransactionDescription(account.getAccountName());
//					clientStatement.setTtype(detail.getTtype());
//					clientStatement.setTransationDate(payment.getPaymentDate());
//					clientStatement.setUpdatedBy(systemUserService.getLoggedInUser());
//
//					if (detail.getTtype().getTransactionType()
//							.equalsIgnoreCase(Ttype.C.getTransactionType())) {
//						clientStatement.setTransactionCredit(detail.getAmountPaid());
//						clientStatement.setTransactionDebt(0);
//					} else {
//						clientStatement.setTransactionDebt(detail.getAmountPaid());
//						clientStatement.setTransactionCredit(0);
//					}
//
//					list.add(clientStatement);
//
//				} else {
//
//					System.out.println("isTrackPData null");
//				}
//
//			} else {
//
//				System.out.println("account null");
//			}
//
//		}
//
//		System.out.println("list: " + list.size());
//
//		return list;
//	}
////
	public SwizFeedback update(Multi multi) {
		try {
			multi.setCreatedBy(systemUserService.getLoggedInUser());
			multi.setUpdatedBy(systemUserService.getLoggedInUser());

			multiJournalDao.update(multi);
			return new SwizFeedback(true, "Transaction Updated successfully");

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public SwizFeedback delete(Multi multi) {
		try {
			multi.setCreatedBy(systemUserService.getLoggedInUser());
			multi.setUpdatedBy(systemUserService.getLoggedInUser());

			multiJournalDao.delete(multi);
			return new SwizFeedback(true, "Transaction Deleted successfully");

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	@Transactional(readOnly = true)
	public List<Multi> find() {
		List<Multi> list = new ArrayList<Multi>();
		try {
			for (Multi multi : multiJournalDao.find()) {
				if (multi.getMultiDetails() != null) {
					multi.setMultiDetails(null);

				}

//				if() {}
				list.add(multi);
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return list;
	}

	public Multi find(String id) {
		try {
			return multiJournalDao.find(id);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	@Transactional(readOnly = true)
	public List<MultiDetail> find(Multi multi) {

		List<MultiDetail> list = new ArrayList<MultiDetail>();

		try {

			Search search = new Search();
			search.addFilterEqual("multi", multi);

			List<MultiDetail> details = multiDetailDao.search(search);

			for (MultiDetail journalDetail : details) {

				if (journalDetail.getMulti() != null) {
					journalDetail.setMulti(null);
					CustomerAccount customer = new CustomerAccount();
					String test = (customer.getClientCode());

					journalDetail.setCust(test);
				}

				list.add(journalDetail);
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return list;
	}

	public List<MultiDetail> findByPayment(Multi multi) {

		List<MultiDetail> list = new ArrayList<MultiDetail>();

		try {

			Search search = new Search();
			search.addFilterEqual("multi", multi);
			list = multiDetailDao.search(search);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return list;
	}

}
