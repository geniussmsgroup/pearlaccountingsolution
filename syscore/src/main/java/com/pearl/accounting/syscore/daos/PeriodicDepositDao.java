package com.pearl.accounting.syscore.daos;

import com.pearl.accounting.sysmodel.PeriodicDeposit;

public interface PeriodicDepositDao  extends SwiziBaseDao<PeriodicDeposit>{

}
