package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.EmailNotificationConfigurationDao;
import com.pearl.accounting.sysmodel.EmailNotificationConfiguration;
 

@Repository("EmailNotificationConfigurationDao")
public class EmailNotificationConfigurationDaoImpl extends SwiziBaseDaoImpl<EmailNotificationConfiguration>
		implements EmailNotificationConfigurationDao {

}
