package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.LoanOrderView;
import com.pearl.accounting.sysmodel.LoanbalanceView;

public interface LoanOrderViewService {


	public List<LoanOrderView> find();

	public LoanOrderView find(String  Lnbal );


}
