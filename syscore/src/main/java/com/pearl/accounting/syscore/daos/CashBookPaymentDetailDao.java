package com.pearl.accounting.syscore.daos;

import com.pearl.accounting.sysmodel.CashBookPaymentDetail;

public interface CashBookPaymentDetailDao extends SwiziBaseDao<CashBookPaymentDetail>{

}
