package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.CustomerAccountDao;
import com.pearl.accounting.sysmodel.CustomerAccount;

@Repository("CustomerAccountDao")
public class CustomerAccountDaoImpl extends SwiziBaseDaoImpl<CustomerAccount> implements CustomerAccountDao {

}
