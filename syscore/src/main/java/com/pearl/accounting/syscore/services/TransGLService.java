package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.TransGL;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface TransGLService {
	public SwizFeedback save(TransGL transgl);

	public SwizFeedback update(TransGL transgl);

	public SwizFeedback delete(TransGL transgl);

	public List<TransGL> find();

	public TransGL find(String id);
	
	public SwizFeedback save(List<TransGL> transgl);

}
