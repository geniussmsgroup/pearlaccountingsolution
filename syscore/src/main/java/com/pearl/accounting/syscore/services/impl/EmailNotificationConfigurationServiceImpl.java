package com.pearl.accounting.syscore.services.impl;

import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pearl.accounting.syscore.daos.EmailNotificationConfigurationDao;
import com.pearl.accounting.syscore.services.EmailNotificationConfigurationService;
import com.pearl.accounting.sysmodel.EmailNotificationConfiguration;

@Service("EmailNotificationConfigurationService")
@Transactional
public class EmailNotificationConfigurationServiceImpl implements EmailNotificationConfigurationService {

	@Autowired
	private EmailNotificationConfigurationDao emailNotificationConfigurationDao;

	public boolean save(EmailNotificationConfiguration configuration) {
		try {

			List<EmailNotificationConfiguration> emailNotificationConfigurations = find();

			if (emailNotificationConfigurations == null) {
				emailNotificationConfigurationDao.save(configuration);
				return true;
			} else {
				if (emailNotificationConfigurations.size() == 0) {
					emailNotificationConfigurationDao.save(configuration);
					return true;
				} else {

					System.out.println("Email Notification Configuration already exists");
				}
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

		return false;
	}

	public boolean update(EmailNotificationConfiguration configuration) {
		try {
			emailNotificationConfigurationDao.update(configuration);
			return true;

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return false;
	}

	public boolean delete(EmailNotificationConfiguration configuration) {
		try {
			emailNotificationConfigurationDao.delete(configuration);
			return true;

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return false;
	}

	public EmailNotificationConfiguration find(String id) {
		try {
			return emailNotificationConfigurationDao.find(id);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	public List<EmailNotificationConfiguration> find() {
		try {
			return emailNotificationConfigurationDao.find();

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	public boolean sendMail(String mailRecipient, String recipientName, String notificationMessage) {

		EmailNotificationConfiguration configuration = find().get(0);

		if (configuration != null) {
			final String host = configuration.getHostConfiguration();
			final String port = configuration.getPortNumberConfiguration();
			final String password = configuration.getPasswordConfiguration();
			final String user = configuration.getUserConfiguration();

			final String emailSubject = configuration.getSubjectConfiguration();
			final String emailMessageBody = notificationMessage;
			final String applicationLink = configuration.getApplicationLinkConfiguration();

			// Get the session object
			//
			Properties props1 = new Properties();
			props1.put("mail.smtp.host", host);
			props1.put("mail.smtp.auth", "true");
			//props.put("mail.smtp.starttls.enable", "false");
			props1.put("mail.smtp.ssl.enable", "true");
			props1.put("mail.smtp.socketFactory.port", port);
			props1.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			props1.put("mail.smtp.port", port);
			props1.put("mail.smtp.socketFactory.fallback", "true");
			
			Properties props = new Properties();
			props.put("mail.smtp.host", host);
			
			props.put("mail.smtp.port", port);
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");

			Session session = Session.getInstance(props, new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(user, password);
				}
			});

			// Compose the message
			try {
				MimeMessage message = new MimeMessage(session);
				message.setFrom(new InternetAddress(user));

				message.addRecipient(Message.RecipientType.TO, new InternetAddress(mailRecipient));

				message.setSubject(emailSubject);

				String messageBody = "Dear " + recipientName + ",\n" + emailMessageBody + " \n" + applicationLink
						+ "/\n\nThank you.";
				message.setText(messageBody);

				System.out.println("Email: "+messageBody);
				// send the message
				Transport.send(message);

				System.out.println("message sent successfully...");

				return true;

			} catch (MessagingException e) {
				e.printStackTrace();
			}

		}
		return false;
	}

}
