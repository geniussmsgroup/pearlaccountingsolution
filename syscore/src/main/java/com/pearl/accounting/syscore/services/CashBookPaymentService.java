package com.pearl.accounting.syscore.services;

import java.util.List;
 
import com.pearl.accounting.sysmodel.CashBookPayment;
import com.pearl.accounting.sysmodel.CashBookPaymentDetail;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface CashBookPaymentService {

	public SwizFeedback savetoSpring(CashBookPayment cashBookPayment);

	public SwizFeedback update(CashBookPayment cashBookPayment);

	public SwizFeedback delete(CashBookPayment cashBookPayment);

	public List<CashBookPayment> find();

	public CashBookPayment find(String id);
	
	public List<CashBookPaymentDetail> find(CashBookPayment cashBookPayment);
}
