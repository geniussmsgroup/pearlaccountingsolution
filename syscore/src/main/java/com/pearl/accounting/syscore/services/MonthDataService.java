package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.Intervaltb;
import com.pearl.accounting.sysmodel.MonthData;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface MonthDataService {

	public SwizFeedback save(MonthData monthData);

	public SwizFeedback update(MonthData monthData);

	public SwizFeedback delete(MonthData monthData);

	public List<MonthData> find();
	
	public SwizFeedback NextInterval();

	public MonthData find(String id);
	
	
}
