package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.CashBookReceipt;
import com.pearl.accounting.sysmodel.CashBookReceiptDetail;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface CashBookReceiptService {
	
	public SwizFeedback save(CashBookReceipt cashBookReceipt);

	public SwizFeedback update(  CashBookReceipt cashBookReceipt);

	public SwizFeedback delete(  CashBookReceipt cashBookReceipt);

	public List<CashBookReceipt> find();

	public CashBookReceipt find(String id);
	
	public List<CashBookReceiptDetail> find(CashBookReceipt cashBookReceipt);

}
