package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.TheadDao;
import com.pearl.accounting.sysmodel.Thead;

@Repository("TheadDao")
public class TheadDaoImpl extends SwiziBaseDaoImpl<Thead> implements TheadDao{

}
