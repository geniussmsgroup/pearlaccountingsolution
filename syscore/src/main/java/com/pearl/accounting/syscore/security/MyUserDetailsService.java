package com.pearl.accounting.syscore.security;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pearl.accounting.syscore.daos.SystemUserDao;
import com.pearl.accounting.syscore.daos.UserPermissionDao;
import com.pearl.accounting.syscore.daos.UserRoleDao;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.sysmodel.ConfigRole;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.SystemUser;
import com.pearl.accounting.sysmodel.SystemView;
import com.pearl.accounting.sysmodel.UserPermission;
import com.pearl.accounting.sysmodel.UserRole;

@Service("userDetailsService") 
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	private SystemUserService userService;

	@Autowired
	private SystemUserDao userDao;
	
	@Autowired
	private UserRoleDao userRoleDao;
	
	@Autowired
	private UserPermissionDao userPermissionDao;

	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		System.out.println("MyUserDetailsService Authenticating user..");

		// setting initial user
		List<SystemUser> users = userDao.find();
		if (users == null) {

			System.out.println("Creating user role");
			
			UserRole userRole = new UserRole();
			userRole.setCode("ADM");
			userRole.setDateCreated(new Date());
			userRole.setDateUpdated(new Date());
			userRole.setDescription("Monintor & manage all system transactions");
			userRole.setEmployeeRole("Administrator");
			userRole.setStatus(Status.ACTIVE);

			UserRole role = userRoleDao.save(userRole);

			if (role != null) {
				
				System.out.println("Creating UserPermission ");
				
				UserPermission permission = new UserPermission();
				permission.setDateCreated(new Date());
				permission.setDateUpdated(new Date());
				permission.setStatus(Status.ACTIVE);
				permission.setSystemView(SystemView.VIEW_SYSTEMS_ADMIN);
				permission.setUserRole(role);

				UserPermission userPermission = userPermissionDao.save(permission);

				if (userPermission != null) {
					
					System.out.println("Creating SystemUser");
					
					SystemUser user = new SystemUser();
					user.setEnabled(true);
					user.setPassword("password");
					user.setUsername("admin");

					user.setDateCreated(new Date());
					user.setDateUpdated(new Date());
					user.setEmailAddress("kfredrick35@gmail.com");
					user.setFirstName("Kasoma");
					user.setLastName("Fredrick");
					user.setPhoneNumber("0700408387");
					// user.setProjectPartner(projectPartner);
					user.setStatus(Status.ACTIVE);
					user.setUserRole(role);
					user.setRole(ConfigRole.ROLE_USER);

					userService.save(user);
				}

			}

		} else if (users.size() == 0) {
			
			System.out.println("Creating user role 2");
			
			UserRole userRole = new UserRole();
			userRole.setCode("ADM");
			userRole.setDateCreated(new Date());
			userRole.setDateUpdated(new Date());
			userRole.setDescription("Monintor & manage all system transactions");
			userRole.setEmployeeRole("Administrator");
			userRole.setStatus(Status.ACTIVE);

			UserRole role = userRoleDao.save(userRole);

			
			
			if (role != null) {
				
				System.out.println("role Id: "+role.getId());
				
				System.out.println("Creating UserPermission 2");
				
				UserPermission permission = new UserPermission();
				permission.setDateCreated(new Date());
				permission.setDateUpdated(new Date());
				permission.setStatus(Status.ACTIVE);
				permission.setSystemView(SystemView.VIEW_SYSTEMS_ADMIN);
				permission.setUserRole(role);

				UserPermission userPermission = userPermissionDao.save(permission);

				if (userPermission != null) {
					
					System.out.println("Creating SystemUser 2");
					
					SystemUser user = new SystemUser();
					user.setEnabled(true);
					user.setPassword("password");
					user.setUsername("admin");

					user.setDateCreated(new Date());
					user.setDateUpdated(new Date());
					user.setEmailAddress("kfredrick35@gmail.com");
					user.setFirstName("Kasoma");
					user.setLastName("Fredrick");
					user.setPhoneNumber("0700408387");
					// user.setProjectPartner(projectPartner);
					user.setStatus(Status.ACTIVE);
					user.setUserRole(role);
					user.setRole(ConfigRole.ROLE_USER);

					userService.save(user);
				}

			}

		}

		SystemUser user = new SystemUser();
		user = userService.findByUserName(username);

		if (user != null) {
			System.out.println("logining is ====>>" + user.getUsername());
		}
		List<GrantedAuthority> authorities = buildUserAuthority(user);
		return buildUserForAuthentication(user, authorities);
	}

	// com.planetsystems.model.utils.utils_model.User user to
	// org.springframework.security.core.userdetails.User
	private User buildUserForAuthentication(SystemUser user, List<GrantedAuthority> authorities) {
		return new User(user.getUsername(), user.getPassword(), user.isEnabled(), true, true, true, authorities);
	}

	private List<GrantedAuthority> buildUserAuthority(SystemUser systemUser) {

		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

		// Build user's authorities

		// setAuths.add(new
		// GrantedAuthorityImpl(systemUser.getUserRole().getCode()));
		setAuths.add(new GrantedAuthorityImpl(systemUser.getRole().getUserRole()));

		List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);

		return Result;
	}

}
