package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysmodel.LoanRepayment;
import com.pearl.accounting.sysmodel.LoanRepayments;
import com.pearl.accounting.sysmodel.RepaymentSchedule;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface LoanRepaymentsService {

	public SwizFeedback save(LoanRepayments loanRepayments);

	public SwizFeedback update(LoanRepayments loanRepayments);

	public SwizFeedback delete(LoanRepayments loanRepayments);

	public List<LoanRepayments> find();

	public LoanRepayments find(String Sysid);

//	public List<LoanRepayments> find(Loan loan);

//public List<LoanRepayments> getLoanRepayment(Loan loan);

}
