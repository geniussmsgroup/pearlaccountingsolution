package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.WithdrawalDao;
import com.pearl.accounting.sysmodel.Withdrawal;

@Repository("WithdrawalDao")
public class WithdrawalDaoImpl extends SwiziBaseDaoImpl<Withdrawal> implements WithdrawalDao {

}
