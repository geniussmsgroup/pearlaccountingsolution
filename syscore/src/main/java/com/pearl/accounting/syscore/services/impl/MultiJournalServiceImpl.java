package com.pearl.accounting.syscore.services.impl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.MultiJournalDao;
import com.pearl.accounting.syscore.daos.MultiJournalDetailDao;
import com.pearl.accounting.syscore.services.AccountService;
import com.pearl.accounting.syscore.services.ClientStatementService;
import com.pearl.accounting.syscore.services.CustomerAccountService;
import com.pearl.accounting.syscore.services.GeneralLedgerService;
import com.pearl.accounting.syscore.services.LoanService;
import com.pearl.accounting.syscore.services.MultiJournalService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.syscore.services.TheadpService;
import com.pearl.accounting.syscore.services.TransGLService;
import com.pearl.accounting.syscore.services.TransMLService;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.CashBookPayment;
import com.pearl.accounting.sysmodel.CashBookPaymentDetail;
import com.pearl.accounting.sysmodel.CashBookReceipt;
import com.pearl.accounting.sysmodel.CashBookReceiptDetail;
import com.pearl.accounting.sysmodel.ClientStatement;
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.DataSourceScreen;
import com.pearl.accounting.sysmodel.Fsave;
import com.pearl.accounting.sysmodel.GeneralLedger;
import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysmodel.MultiJournal;
import com.pearl.accounting.sysmodel.MultiJournalDetail;
import com.pearl.accounting.sysmodel.ProductType;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.SystemUser;
import com.pearl.accounting.sysmodel.Theadp;
import com.pearl.accounting.sysmodel.TransGL;
import com.pearl.accounting.sysmodel.TransML;
import com.pearl.accounting.sysmodel.Ttype;

@Service("MultiJournalService")
@Transactional
public class MultiJournalServiceImpl implements MultiJournalService {

	@Autowired
	private MultiJournalDao multiJournalDao;

	@Autowired
	private MultiJournalDetailDao multiJournalDetailDao;

	@Autowired
	private SystemUserService systemUserService;

	@Autowired
	private GeneralLedgerService generalLedgerService;

	@Autowired
	private ClientStatementService clientStatementService;

	@Autowired
	private AccountService accountService;

	@Autowired
	private CustomerAccountService customerAccountService;
	@Autowired
	private LoanService loanService;
	@Autowired
	private TheadpService theadpService;
	@Autowired
	private TransGLService transglService;
	@Autowired
	private TransMLService transmlService;

	public SwizFeedback save(MultiJournal multiJournal) {

		try {

			multiJournal.setCreatedBy(systemUserService.getLoggedInUser());
			multiJournal.setUpdatedBy(systemUserService.getLoggedInUser());
			multiJournal.setThead(theadpService.getthead());
			MultiJournal journal = multiJournalDao.save(multiJournal);

			if (journal != null) {
                theadpService.save(savethead(journal));
				SwizFeedback feedback = generalLedgerService.save(generateGeneralLedger(journal));

				SwizFeedback feedback2 = clientStatementService.save(generateClientStatement(journal));
				SwizFeedback feedback3 = transglService.save(generateTransGL(journal));
				SwizFeedback feedback4 = transmlService.save(generatememberstmc(journal));

				if (feedback2.isResponse() && feedback3.isResponse() && feedback.isResponse()&&feedback4.isResponse()) {

					return new SwizFeedback(true, "Transaction successfully posted");

				} else {
					return feedback;
				}

			} else {
				return new SwizFeedback(false, "Error occured while posting transaction. Please try again");
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}

	}

	// debit and credit the receiveing account and Paying account in the general
	// ledger
	private List<GeneralLedger> generateGeneralLedger(MultiJournal payment) {

		List<GeneralLedger> list = new ArrayList<GeneralLedger>();

		List<MultiJournalDetail> details = findByPayment(payment);

		for (MultiJournalDetail detail : details) {

			if (detail.getTtype() != null) {

				GeneralLedger generalLedger = new GeneralLedger();
				generalLedger.setThead(payment.getThead());
				generalLedger.setAccount(detail.getTransactionAccount());
				generalLedger.setPeriod(payment.getPeriod());
				generalLedger.setBalanceCredit(0);
				generalLedger.setBalanceDebt(0);
				generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
				generalLedger.setDateCreated(new Date());
				generalLedger.setDateUpdated(new Date());
				generalLedger.setYear(payment.getYear());
				generalLedger.setPaymentDetails(detail.getPaydetails());
				generalLedger.setPostReferenceNo(detail.getId());
				generalLedger.setDataSourceScreen(DataSourceScreen.MULTI_JOURNAL);
				generalLedger
						.setRemarks(DataSourceScreen.MULTI_JOURNAL.getDataSourceScreen() + " :" + detail.getRemarks());
				generalLedger.setStatus(Status.ACTIVE);
				generalLedger.setThead(payment.getThead());
				generalLedger.setTransactionDescription(detail.getTransactionAccount().getAccountName());
				generalLedger.setLoanid(detail.getLoan());
				generalLedger.setTransationDate(payment.getTransactionDate());
				
//				
				generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());

				if (detail.getTtype().getTransactionType().equalsIgnoreCase(Ttype.C.getTransactionType())) {

					generalLedger.setTtype(Ttype.C);
					generalLedger.setTransactionCredit(detail.getAmountPosted());
					generalLedger.setTransactionDebt(0);

				} else {

					generalLedger.setTtype(Ttype.D);
					generalLedger.setTransactionCredit(0);
					generalLedger.setTransactionDebt(detail.getAmountPosted());
				}

				list.add(generalLedger);

			}

		}

		return list;
	}

	private List<TransGL> generateTransGL(MultiJournal payment) {
		List<TransGL> list = new ArrayList<TransGL>();

		List<MultiJournalDetail> details = findByPayment(payment);

		for (MultiJournalDetail detail : details) {

			if (detail.getTtype() != null) {
				TransGL gl = new TransGL();
				// TransGL gl = new TransGL();
				gl.setDateCreated(new Date());
				gl.setDateUpdated(new Date());
				gl.setDataSourceScreen(DataSourceScreen.MULTI_JOURNAL);
				gl.setCreatedBy(systemUserService.getLoggedInUser());
				gl.setUpdatedBy(systemUserService.getLoggedInUser());
				gl.setStatus(Status.ACTIVE);
				// gl.setACODE(getcashbookdetailcodes(payment));
				gl.setFsave(Fsave.Y);
				gl.setTheadID(payment.getThead());
				gl.setAmtUsd(0);
				gl.setRecAmt(0);
				gl.setTDATE(payment.getTransactionDate());
				gl.setTTYPE(Ttype.D);
				gl.setPAYDET(payment.getTransactionDetails());
				gl.setREMARKS(payment.getRemarks() + detail.getRemarks());
				gl.setICODE(payment.getPeriodcode());
				gl.setYRCODE(payment.getYear());
				gl.setPAYPERSON(payment.getRemarks());
				if (detail.getCustomerAccount() != null) {
					CustomerAccount customer = customerAccountService.find(detail.getCustomerAccount().getSysid());
					gl.setMemberID(customer.getClientCode());
				} else {
					gl.setMemberID(null);

				}
				gl.setTheadID(payment.getThead());
				Account account = accountService.find(detail.getTransactionAccount().getSysid());

				gl.setACODE(account.getACODE());
				gl.setYrRec(0);
				gl.setTrec(0);
				gl.setAccount(detail.getTransactionAccount());
				SystemUser staff = systemUserService.find(payment.getUpdatedBy().getId());
				String name = staff.getUsername();
				gl.setStaffName(name);

				if (detail.getTtype().getTransactionType().equalsIgnoreCase(Ttype.C.getTransactionType())) {

					gl.setTTYPE(Ttype.C);
					gl.setAMOUNT(detail.getAmountPosted());

				} else {

					gl.setTTYPE(Ttype.D);
					gl.setAMOUNT(detail.getAmountPosted());
				}

				list.add(gl);

			}

		}

		return list;
	}

	private List<ClientStatement> generateClientStatement(MultiJournal payment) {

		List<ClientStatement> list = new ArrayList<ClientStatement>();

		try {

			List<MultiJournalDetail> details = findByPayment(payment);

			System.out.println("details: " + details.size());

			for (MultiJournalDetail detail : details) {

				Account account = accountService.find(detail.getTransactionAccount().getSysid());

				if (account != null) {
//                    account.getAccountName().equalsIgnoreCase("yes");
					System.out.println("account tract: " + account.getAnalReq().equalsIgnoreCase("Y"));

//					if (account.getAnalReq().equalsIgnoreCase("Y")) {
					if (detail.getCustomerAccount() != null) {
						ClientStatement clientStatement = new ClientStatement();
						clientStatement.setCustomerAccount(detail.getCustomerAccount());
						clientStatement.setPeriod(payment.getPeriod());
						clientStatement.setBalanceCredit(0);
						clientStatement.setBalanceDebt(0);
						clientStatement.setCreatedBy(systemUserService.getLoggedInUser());
						clientStatement.setDateCreated(new Date());
						clientStatement.setDateUpdated(new Date());
						clientStatement.setYear(payment.getYear());
						clientStatement.setThead(payment.getThead());
						clientStatement.setLoanid(detail.getLoan());
						if (account.getProductType() != null) {
							clientStatement.setThead(payment.getThead());

						}
						clientStatement.setPaymentDetails(detail.getRemarks());
						clientStatement.setPostReferenceNo(payment.getId());
						clientStatement.setRemarks(detail.getRemarks());
						clientStatement.setStatus(Status.ACTIVE);

						System.out.println("details: 1");

						clientStatement.setTransactionDescription(account.getAccountName());
						clientStatement.setTtype(detail.getTtype());
						clientStatement.setTransationDate(payment.getTransactionDate());
						clientStatement.setUpdatedBy(systemUserService.getLoggedInUser());

						// System.out.println("details: 2");

						if (detail.getTtype().getTransactionType().equalsIgnoreCase(Ttype.C.getTransactionType())) {
							clientStatement.setTransactionCredit(detail.getAmountPosted());
							clientStatement.setTransactionDebt(0);
						} else {
							clientStatement.setTransactionDebt(detail.getAmountPosted());
							clientStatement.setTransactionCredit(0);
						}

						list.add(clientStatement);
					}
//					} else {
//
//						System.out.println("isTrackPData null");
//					}

				} else {

					System.out.println("account null");
				}

			}

			System.out.println("list: " + list.size());
		} catch (Exception ex) {
			System.out.println("details: " + ex.getMessage());
		}
		return list;
	}

	private List<TransML> generatememberstmc(MultiJournal multijournal) {

		List<TransML> list = new ArrayList<TransML>();
		try {
			List<MultiJournalDetail> details = findByPayment(multijournal);
			System.out.println("details: " + details.size());

			for (MultiJournalDetail detail : details) {

				if (detail.getCustomerAccount() != null) {
					CustomerAccount ant = customerAccountService.find(detail.getCustomerAccount().getSysid());

					TransML gl = new TransML();
					gl.setCreatedBy(systemUserService.getLoggedInUser());
					gl.setUpdatedBy(systemUserService.getLoggedInUser());
					gl.setDateCreated(new Date());
					gl.setDateUpdated(new Date());
					gl.setStatus(Status.ACTIVE);
					gl.setDataSourceScreen(DataSourceScreen.MULTI_JOURNAL);
					gl.setMEMBERID(ant.getClientCode());
					gl.setCustomeraccount(detail.getCustomerAccount());
					gl.setFSAVE(Fsave.Y);
					Loan loan = loanService.find(detail.getLoan().getLOANID());
					int loanid = Integer.parseInt(loan.getLOANID());
					gl.setLOANID(loanid);
					gl.setTDATE(multijournal.getTransactionDate());
					gl.setAMOUNT(multijournal.getAmountPosted());
					gl.setPAYDET(multijournal.getTransactionDetails() + detail.getRemarks());
					gl.setVNO(multijournal.getTransactionDetails() + " " + multijournal.getRemarks());
					gl.setTHEADID(multijournal.getThead());
					gl.setLoan(detail.getLoan());
					gl.setPRODUCTID("LNP");
					SystemUser staff = systemUserService.find(multijournal.getUpdatedBy().getId());
					String name = staff.getUsername();
					gl.setSTAFFNAME(name);
					gl.setStatus(Status.ACTIVE);
					gl.setTTYPE(detail.getTtype());
					list.add(gl);
					System.out.println("list: " + list.size());
				} else {
					System.out.println(" account null");

				}
			}
		} catch (Exception ex) {
			System.out.println("details: " + ex.getMessage());

		}

		return list;
	}
	
	
	public Theadp savethead(MultiJournal multijournal) {
		
		Theadp thead = new Theadp();
		thead.setAmount(multijournal.getAmountPosted());
		thead.setYear(thead.getYear());
		thead.setDataSourceScreen(DataSourceScreen.MULTI_JOURNAL);
		thead.setRemarks(multijournal.getRemarks());
		thead.setVNO(multijournal.getTransactionDetails());
		thead.setTdates(multijournal.getTransactionDate());
		thead.setPeriodcode(multijournal.getPeriodcode());
		thead.setStatus(Status.ACTIVE);
		thead.setDateCreated(new Date());
		thead.setDateUpdated(new Date());
		thead.setPeriod(multijournal.getPeriod());

		
		
		
		return thead;
		
	}
	
	
	

//	private List<ClientStatement> generateClientStatement(MultiJournal payment) {
//
//		List<ClientStatement> list = new ArrayList<ClientStatement>();
//
//		try {
//
//			List<MultiJournalDetail> details = findByPayment(payment);
//
//			System.out.println("details: " + details.size());
//
//			for (MultiJournalDetail detail : details) {
//
//				Account account = accountService.find(detail.getTransactionAccount().getSysid());
//
//				if (account != null) {
////                    account.getAccountName().equalsIgnoreCase("yes");
//					System.out.println("account tract: " + account.getAnalReq().equalsIgnoreCase("Y"));
//
////					if (account.getAnalReq().equalsIgnoreCase("Y")) {
//					if (detail.getCustomerAccount() != null) {
//						ClientStatement clientStatement = new ClientStatement();
//						clientStatement.setCustomerAccount(detail.getCustomerAccount());
//						clientStatement.setPeriod(payment.getPeriod());
//						clientStatement.setBalanceCredit(0);
//						clientStatement.setBalanceDebt(0);
//						clientStatement.setCreatedBy(systemUserService.getLoggedInUser());
//						clientStatement.setDateCreated(new Date());
//						clientStatement.setDateUpdated(new Date());
//						clientStatement.setYear(payment.getYear());
//						clientStatement.setThead(payment.getThead());
//						clientStatement.setLoanid(detail.getLoan());
//						if (account.getProductType() != null) {
//							clientStatement.setThead(payment.getThead());
//							;
//
//						}
//						clientStatement.setPaymentDetails(detail.getRemarks());
//						clientStatement.setPostReferenceNo(payment.getId());
//						clientStatement.setRemarks(detail.getRemarks());
//						clientStatement.setStatus(Status.ACTIVE);
//
//						System.out.println("details: 1");
//
//						clientStatement.setTransactionDescription(account.getAccountName());
//						clientStatement.setTtype(detail.getTtype());
//						clientStatement.setTransationDate(payment.getTransactionDate());
//						clientStatement.setUpdatedBy(systemUserService.getLoggedInUser());
//
//						// System.out.println("details: 2");
//
//						if (detail.getTtype().getTransactionType().equalsIgnoreCase(Ttype.C.getTransactionType())) {
//							clientStatement.setTransactionCredit(detail.getAmountPosted());
//							clientStatement.setTransactionDebt(0);
//						} else {
//							clientStatement.setTransactionDebt(detail.getAmountPosted());
//							clientStatement.setTransactionCredit(0);
//						}
//
//						list.add(clientStatement);
//					}
////					} else {
////
////						System.out.println("isTrackPData null");
////					}
//
//				} else {
//
//					System.out.println("account null");
//				}
//
//			}
//
//			System.out.println("list: " + list.size());
//		} catch (Exception ex) {
//			System.out.println("details: " + ex.getMessage());
//		}
//		return list;
//	}

	public SwizFeedback update(MultiJournal multiJournal) {
		try {
			multiJournal.setCreatedBy(systemUserService.getLoggedInUser());
			multiJournal.setUpdatedBy(systemUserService.getLoggedInUser());

			multiJournalDao.update(multiJournal);
			return new SwizFeedback(true, "Transaction Updated successfully");

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public SwizFeedback delete(MultiJournal multiJournal) {
		try {
			multiJournal.setCreatedBy(systemUserService.getLoggedInUser());
			multiJournal.setUpdatedBy(systemUserService.getLoggedInUser());

			multiJournalDao.delete(multiJournal);
			return new SwizFeedback(true, "Transaction Deleted successfully");

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	@Transactional(readOnly = true)
	public List<MultiJournal> find() {
		List<MultiJournal> list = new ArrayList<MultiJournal>();
		try {
			for (MultiJournal multiJournal : multiJournalDao.find()) {

				if (multiJournal.getMultiJournalDetails() != null) {
					multiJournal.setMultiJournalDetails(null);

				}
//				if ((multiJournal.getMultiJournalDetails().getCustomerAccount() != null) && (journalDetail.getLoan() != null)) {
//					journalDetail.setCustomerAccount(journalDetail.getCustomerAccount());
//					journalDetail.setLoan(journalDetail.getLoan());757862522
//
//				} else {
//					journalDetail.setLoan(null);
//					journalDetail.setCustomerAccount(null);
//
//				}

				list.add(multiJournal);
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return list;
	}

	public MultiJournal find(String id) {
		try {
			return multiJournalDao.find(id);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	@Transactional(readOnly = true)
	public List<MultiJournalDetail> find(MultiJournal multiJournal) {

		List<MultiJournalDetail> list = new ArrayList<MultiJournalDetail>();

		try {

			Search search = new Search();
			search.addFilterEqual("multiJournal", multiJournal);

			List<MultiJournalDetail> details = multiJournalDetailDao.search(search);

			for (MultiJournalDetail journalDetail : details) {

				if (journalDetail.getMultiJournal() != null) {
					journalDetail.setMultiJournal(null);
                    journalDetail.setTheadId(multiJournal.getThead());
				}
//				if ((journalDetail.getCustomerAccount() != null) && (journalDetail.getLoan() != null)) {
//					journalDetail.setCustomerAccount(journalDetail.getCustomerAccount());
//					journalDetail.setLoan(journalDetail.getLoan());
//
//				} else {
//					journalDetail.setLoan(null);
//					journalDetail.setCustomerAccount(null);
//
//				}

				list.add(journalDetail);
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return list;
	}

	public List<MultiJournalDetail> findByPayment(MultiJournal multiJournal) {

		List<MultiJournalDetail> list = new ArrayList<MultiJournalDetail>();

		try {

			Search search = new Search();
			search.addFilterEqual("multiJournal", multiJournal);
			list = multiJournalDetailDao.search(search);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return list;
	}

//	private String getMultiJournalDetailcodes(MultiJournal multijournal) {
//		List<String> list = new ArrayList<String>();
//		String result = null;
//		try {
//			List<MultiJournalDetail> details = findByPayment(multijournal);
//			Account account = new Account();
//			for (MultiJournalDetail detail : details) {
//				MultiJournalDetail cashBookDetails = new MultiJournalDetail();
//				account = accountService.find(cashBookDetails.getTransactionAccount().getId());
//				result = account.getAccountCode();
//
//			}
//			list.add(result);
//
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//		}
//		return result;
//	}

//	private String getactno(MultiJournal multijournal) {
//		// List<String> list = new ArrayList<String>();
//		String result = null;
//		try {
//			List<MultiJournalDetail> details = findByPayment(multijournal);
//			// Account account = new Account();
//			for (MultiJournalDetail detail : details) {
//				MultiJournalDetail cashBookDetails = new MultiJournalDetail();
//				CustomerAccount account = customerAccountService.find(cashBookDetails.getCustomerAccount().getId());
//				result = account.getAccountNo();
//
//			}
//
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//		}
//		return result;
//	}

//	private int getids() {
//		int num = 0;
//		try {
//			List<MultiJournal> windows = multiJournalDao.findAll();
//			num = windows.size();
//		} catch (Exception ex) {
//
//		}
//
//		return num;
//	}
//
}
