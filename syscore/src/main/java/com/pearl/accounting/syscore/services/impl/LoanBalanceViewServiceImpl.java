package com.pearl.accounting.syscore.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pearl.accounting.syscore.daos.AccountDao;
import com.pearl.accounting.syscore.daos.LoanBalanceViewDao;
import com.pearl.accounting.syscore.services.AtypeAservice;
import com.pearl.accounting.syscore.services.LoanBalanceViewService;
import com.pearl.accounting.syscore.services.ProductSetupService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.sysmodel.LoanbalanceView;

@Service("LoanBalanceService")
@Transactional
public class LoanBalanceViewServiceImpl implements LoanBalanceViewService {
	@Autowired
	private LoanBalanceViewDao loanbalanceViewDao;
	
	public List<LoanbalanceView> find() {
		try {

			return loanbalanceViewDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public LoanbalanceView find(String MemberId) {
		try {

			return loanbalanceViewDao.find(MemberId);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

}
