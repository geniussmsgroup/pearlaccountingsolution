package com.pearl.accounting.syscore.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pearl.accounting.syscore.daos.LoanOrderViewDao;
import com.pearl.accounting.syscore.services.LoanOrderViewService;
import com.pearl.accounting.sysmodel.LoanOrderView;

@Service("LoanOrderService")
@Transactional
public class LoanOrderViewServiceImpl implements LoanOrderViewService {
	@Autowired
	private LoanOrderViewDao LoanOrderViewDao;
	
	public List<LoanOrderView> find() {
		try {

			return LoanOrderViewDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public LoanOrderView find(String Lnbal) {
		try {

			return LoanOrderViewDao.find(Lnbal);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

}
