package com.pearl.accounting.syscore.security;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;


public class SwizimaAuthenticationProvider implements AuthenticationProvider {

	private MyUserDetailsService myUserDetailsService = new MyUserDetailsService();

	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		UserDetails userDetails = myUserDetailsService.loadUserByUsername((String) authentication.getPrincipal());
		return new UsernamePasswordAuthenticationToken(userDetails, authentication.getCredentials(), userDetails.getAuthorities());
	}

	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return true;
	}

}
