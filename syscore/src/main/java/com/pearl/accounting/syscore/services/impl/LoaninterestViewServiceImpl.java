package com.pearl.accounting.syscore.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pearl.accounting.syscore.daos.AccountDao;
import com.pearl.accounting.syscore.daos.LoaninterestViewDao;
import com.pearl.accounting.syscore.services.AtypeAservice;
import com.pearl.accounting.syscore.services.LoanBalanceViewService;
import com.pearl.accounting.syscore.services.LoaninterestViewService;
import com.pearl.accounting.syscore.services.ProductSetupService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.sysmodel.LoanbalanceView;
import com.pearl.accounting.sysmodel.LoaninterestView;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.TransGL;

@Service("LoaninterestViewService")
@Transactional
public class LoaninterestViewServiceImpl implements LoaninterestViewService {
	@Autowired
	private LoaninterestViewDao loaninterestViewDao;
	@Autowired
	private LoanBalanceViewService loanbalService;

	public List<LoaninterestView> find() {
		try {

			return loaninterestViewDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public LoaninterestView find(String MemberId) {
		try {
			return loaninterestViewDao.find(MemberId);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}
//	public LoaninterestView[] find(List<String> MemberId) {
//		try {
//
//			String[] ledgers=new String[MemberId.size()];
////			
//			return loaninterestViewDao.find(MemberId.toArray(ledgers));
//
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//
//		}
//		return null;
//	}

//	public Double getinterestrate(List<LoanbalanceView> MemberId) {
//		String actno = null;
//		try {
//			LoanbalanceView[] gets = new LoanbalanceView[MemberId.size()];
//			LoaninterestView  loanint=  loaninterestViewDao.find(MemberId.toArray(gets));
//			loanint.getInterestRate();
//			//LoanbalanceView acode = atypeService.find(account.getAccountType().getAtypecd());
//               //  accountcd = acode.getAtypecd();
//			
//			
//               actno = loanint.getMemberId();
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//
//		}		return accountcd;	
//		
//	}
//	public LoaninterestView findbyid(List<LoanbalanceView> loanbalancegl) {
//		try {
//			LoanbalanceView[] ledgers=new LoanbalanceView[loanbalancegl.size()];
//			
//			return loaninterestViewDao.findOne(loanbalancegl.toArray(ledgers));
//
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//		}
//return null;
//	}
	public float getinterestrate(LoanbalanceView loanbalance) {
		float interestrate = 0;
		try {

			List<LoaninterestView> loanints = loaninterestViewDao.find();

			for (LoaninterestView loanint : loanints) {
				LoaninterestView memberid = loaninterestViewDao.find(loanbalance.getMemberId());
				String acts = memberid.getMemberId();
				if (loanint.getMemberId() == acts) {

					float clientrate = loanint.getInterestRate() / 100F;
					interestrate = (clientrate);

				}
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return interestrate;

	}

	public LoaninterestView getmemberids(LoanbalanceView loanbalance) {
		LoaninterestView actno = new LoaninterestView();
		try {

			List<LoaninterestView> loanints = loaninterestViewDao.find();

			for (LoaninterestView loanint : loanints) {
				// if (loanbalance.getLnbal() > 0) {
				LoaninterestView memberid = loaninterestViewDao.find(loanbalance.getMemberId());
				if (loanbalance.getLnbal() > 0) {

					String acts = memberid.getMemberId();
					if (loanint.getMemberId() == acts) {
						if (acts != null) {
							// LoaninterestView loanintw =new LoaninterestView();
							float clientrate = loanint.getInterestRate() / 100F;
							actno.setInterestRate(clientrate);
							// actno = String.valueOf(loanint.getMemberId());
							actno.setMemberId(acts);
							actno.setLoanID(memberid.getLoanID());
						}
					}

				}
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return actno;

	}

}
