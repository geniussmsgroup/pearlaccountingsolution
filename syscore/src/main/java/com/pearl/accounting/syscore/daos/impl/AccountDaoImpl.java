package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.AccountDao;
import com.pearl.accounting.sysmodel.Account;

@Repository("AccountDao")
public class AccountDaoImpl extends SwiziBaseDaoImpl<Account> implements AccountDao {

}
