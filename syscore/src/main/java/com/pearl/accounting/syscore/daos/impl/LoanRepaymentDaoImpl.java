package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.LoanRepaymentDao;
import com.pearl.accounting.sysmodel.LoanRepayment;

@Repository("LoanRepaymentDao")
public class LoanRepaymentDaoImpl extends SwiziBaseDaoImpl<LoanRepayment> implements LoanRepaymentDao{

}
