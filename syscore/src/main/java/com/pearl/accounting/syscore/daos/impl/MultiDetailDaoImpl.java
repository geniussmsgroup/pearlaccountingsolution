package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.MultiDetailDao;
import com.pearl.accounting.sysmodel.MultiDetail;

@Repository("MultiDetailDao")
public class MultiDetailDaoImpl extends SwiziBaseDaoImpl<MultiDetail> implements MultiDetailDao{

}
