package com.pearl.accounting.syscore.daos;

import com.pearl.accounting.sysmodel.FinancialYear;

public interface FinancialYearDao extends SwiziBaseDao<FinancialYear>{

}
