package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.MyDao;
import com.pearl.accounting.sysmodel.My;

@Repository("MyDao")
public class MyDaoImpl extends SwiziBaseDaoImpl<My> implements MyDao {

}
