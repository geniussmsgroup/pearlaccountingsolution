package com.pearl.accounting.syscore.services.impl;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Month;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.AccountDao;
import com.pearl.accounting.syscore.daos.IntervaltbDao;
import com.pearl.accounting.syscore.daos.MonthDataDao;
import com.pearl.accounting.syscore.services.AccountService;
import com.pearl.accounting.syscore.services.IntervaltbService;
import com.pearl.accounting.syscore.services.MonthDataService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.sysmodel.Intervaltb;
import com.pearl.accounting.sysmodel.MonthData;
import com.pearl.accounting.sysmodel.ProductType;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.Theadp;

@Service("MonthDataService")
@Transactional
public class MonthDataServiceImpl implements MonthDataService {

	@Autowired
	private MonthDataDao monthDataDao;

	@Autowired
	private IntervaltbService intervaltbService;

	@Autowired
	private SystemUserService systemUserService;

	public SwizFeedback save(MonthData monthData) {
		try {
			monthDataDao.save(monthData);
			return new SwizFeedback(true, "Saved successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}

	}

	public SwizFeedback update(MonthData monthData) {
		try {
			monthDataDao.update(monthData);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}
	}

	public SwizFeedback delete(MonthData monthData) {
		try {
			monthDataDao.delete(monthData);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}
	}

	public List<MonthData> find() {
		try {

			return monthDataDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public MonthData find(String id) {
		try {

			return monthDataDao.find(id);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	
	
	
	public SwizFeedback NextInterval() {
		// TODO Auto-generated method stub
try {
		List<MonthData> list_from_monthdata = find();
			int size_of_monthData = list_from_monthdata.size();

			
			// getting the FINTCODE FROM THE LAST ROW
		int integer_code = Integer.parseInt(list_from_monthdata.get(size_of_monthData - 1).getFINTCODE());
		
		// getting the start date from the last row 
		Date start = list_from_monthdata.get(size_of_monthData - 1).getFSTART();
		
		// getting the end date from the last row 
		Date end = list_from_monthdata.get(size_of_monthData - 1).getFEND();
		String Fintcode;
	
		//getting the id of the last row, which is a string
		String ssid =list_from_monthdata.get(size_of_monthData - 1).getSysid();
		
		//converting that id from string to integer
		int ssidint = Integer.parseInt(ssid);
		int sys_id_new= ssidint+1;
				String sysid_back_to_String = Integer.toString(sys_id_new); 
		MonthData mond = new MonthData();
		if (integer_code < 12) {
			integer_code += 1;
			if(integer_code<10) {
				
				 Fintcode = "0"+Integer.toString(integer_code);

			}else {
				 Fintcode = Integer.toString(integer_code);

				
			}
		} else {
			integer_code = 1;
			 Fintcode = "01";

		}
		
		
	
		

		String fname = null;
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(start);
		cal.add(Calendar.MONTH, 1);

		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(end);
		cal2.add(Calendar.MONTH, 1);

		String newStart1 = new SimpleDateFormat("dd/MM/yyyy").format(cal.getTime());
		Date newStart = new SimpleDateFormat("dd/MM/yyyy").parse(newStart1);
		
		String newEnd2 = new SimpleDateFormat("dd/MM/yyyy").format(cal2.getTime());
		Date newEnd = new SimpleDateFormat("dd/MM/yyyy").parse(newEnd2);

		
		String [] dateParts = newEnd2.split("/");
		String day;
		String yearstr;
		yearstr = dateParts[2];
		String mon = dateParts[1];
		
		

		
		int Year = Integer.parseInt(yearstr);
		
//		String cd1 = "-" +Integer.toString(cd);
		String yrname = Integer.toString(Year);
		String sys_id = yrname+mon;
		int month1 = Integer.parseInt(mon);
			
		// switch case inorder to identify the months from integers
		switch (month1) {
		case 01:
			fname = "January";
			break;
		case 02:
			fname = "February";
			break;
		case 03:
			fname = "March";
			break;
		case 04:
			fname = "April";
			break;
		case 05:
			fname = "May";
			break;
		case 06:
			fname = "June";
			break;
		case 07:
			fname = "July";
			break;
		case 8:
			fname = "August";
			break;
		case 9:
			fname = "September";
			break;
		case 10:
			fname = "October";
			break;
		case 11:
			fname ="November";
			break;
		case 12:
			fname = "December";
			break;
	
		}
		
		
		mond.setFEND(newEnd);
		mond.setFSTART(newStart);
		mond.setFINTCODE(Fintcode);		
		mond.setYRCODE(Year);
		mond.setFINTNAME(fname);
		mond.setSysid(sysid_back_to_String);
		
		SwizFeedback feed = save(mond);
		if(feed!=null) {
		Intervaltb tb = new Intervaltb();
		tb.setFEND(newEnd);
		tb.setFSTART(newStart);
		String fintname_for_intervaltb = fname+yrname;
		tb.setFINTNAME(fintname_for_intervaltb);
		tb.setFINTCODE(Fintcode);
		tb.setYRCODE(Year);
		tb.setSysId(sysid_back_to_String);
		SwizFeedback feed2 = intervaltbService.save(tb);
		
		return feed2;
		}else {
			
			return new SwizFeedback(false, "Save Failed");
			}
		
}catch(

	Exception ex)
	{

		return new SwizFeedback(false, "Entire Process failed");
	}
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public SwizFeedback NextInterval1() {
		// TODO Auto-generated method stub
try {
		List<MonthData> list_from_monthdata = find();
			int size_of_monthData = list_from_monthdata.size();

//		List<MonthData> lis2 = find(new PageRequest(0, siz, Direction.DESC, "sysid"));
		int integer_code = Integer.parseInt(list_from_monthdata.get(0).getFINTCODE());
		Date start = list_from_monthdata.get(0).getFSTART();
		Date end = list_from_monthdata.get(0).getFEND();
//		String month = lis.get(siz - 1).getFINTNAME();
		String Fintcode;
	
		String ssid =list_from_monthdata.get(0).getSysid();
		int ssidint = Integer.parseInt(ssid);
		int sys_id_new= ssidint+1;
				String sysid_back_to_String = Integer.toString(sys_id_new); 
		MonthData mond = new MonthData();
		if (integer_code < 12) {
			integer_code += 1;
			if(integer_code<10) {
				
				 Fintcode = "0"+Integer.toString(integer_code);

			}else {
				 Fintcode = Integer.toString(integer_code);

				
			}
		} else {
			integer_code = 1;
			 Fintcode = "01";

		}
		
		
	
		

		String fname = null;
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(start);
		cal.add(Calendar.MONTH, 1);

		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(end);
		cal2.add(Calendar.MONTH, 1);

		String newStart1 = new SimpleDateFormat("dd/MM/yyyy").format(cal.getTime());
		Date newStart = new SimpleDateFormat("dd/MM/yyyy").parse(newStart1);
		
		String newEnd2 = new SimpleDateFormat("dd/MM/yyyy").format(cal2.getTime());
		Date newEnd = new SimpleDateFormat("dd/MM/yyyy").parse(newEnd2);

		
		String [] dateParts = newEnd2.split("/");
		String day;
		String yearstr;
		yearstr = dateParts[2];
		String mon = dateParts[1];
		
		

		
		int Year = Integer.parseInt(yearstr);
		
//		String cd1 = "-" +Integer.toString(cd);
		String yrname = Integer.toString(Year);
		String sys_id = yrname+mon;
		int month1 = Integer.parseInt(mon);
				
		switch (month1) {
		case 01:
			fname = "January";
			break;
		case 02:
			fname = "February";
			break;
		case 03:
			fname = "March";
			break;
		case 04:
			fname = "April";
			break;
		case 05:
			fname = "May";
			break;
		case 06:
			fname = "June";
			break;
		case 07:
			fname = "July";
			break;
		case 8:
			fname = "August";
			break;
		case 9:
			fname = "September";
			break;
		case 10:
			fname = "October";
			break;
		case 11:
			fname ="November";
			break;
		case 12:
			fname = "December";
			break;
	
		}
		
		
		mond.setFEND(newEnd);
		mond.setFSTART(newStart);
		mond.setFINTCODE(Fintcode);		
		mond.setYRCODE(Year);
		mond.setFINTNAME(fname);
		mond.setSysid(sysid_back_to_String);
		
		SwizFeedback feed = save(mond);
		if(feed!=null) {
		Intervaltb tb = new Intervaltb();
		tb.setFEND(newEnd);
		tb.setFSTART(newStart);
		String fintname_for_intervaltb = fname+yrname;
		tb.setFINTNAME(fintname_for_intervaltb);
		tb.setFINTCODE(Fintcode);
		tb.setYRCODE(Year);
		tb.setSysId(sysid_back_to_String);
		SwizFeedback feed2 = intervaltbService.save(tb);
		
		return feed2;
		}else {
			
			return new SwizFeedback(false, "Save Failed");
			}
		
}catch(

	Exception ex)
	{

		return new SwizFeedback(false, "Entire Process failed");
	}
}
	
	
	
	
	
	
	
	
	
	
	
	
	

}
