package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.LoantestDao;
import com.pearl.accounting.sysmodel.Loantest;

@Repository("LoantestDao")
public class LoantestDaoImpl extends SwiziBaseDaoImpl<Loantest> implements LoantestDao{

}
