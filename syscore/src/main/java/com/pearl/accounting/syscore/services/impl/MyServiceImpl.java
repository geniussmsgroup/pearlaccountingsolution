package com.pearl.accounting.syscore.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.AccountDao;
import com.pearl.accounting.syscore.daos.MyDao;
import com.pearl.accounting.syscore.services.AccountService;
import com.pearl.accounting.syscore.services.MyService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.My;
import com.pearl.accounting.sysmodel.SwizFeedback;
@Service("MyService")
@Transactional
public class MyServiceImpl implements MyService {

	@Autowired
	private MyDao myDao;
	
	@Autowired
	private SystemUserService systemUserService;

	public SwizFeedback save(My my) {
		try {
			myDao.save(my);
			return new SwizFeedback(true, "Saved successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}

	}

	public SwizFeedback update(My my) {
		try {
			myDao.update(my);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}
	}

	public SwizFeedback delete(My my) {
		try {
			myDao.delete(my);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}
	}

	public List<My> find() {
		try {

			return myDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public My find(String id) {
		try {

			return myDao.find(id);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}
	
	public My findByName(String accountName) {
		try {
			
			Search search=new Search();
			search.addFilterILike("accountName", accountName); 
			return myDao.searchUnique(search);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

}
