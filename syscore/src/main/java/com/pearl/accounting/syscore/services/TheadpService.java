package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.Theadp;
import com.pearl.accounting.sysmodel.TransGL;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.FixedData;
import com.pearl.accounting.sysmodel.MultiJournal;
import com.pearl.accounting.sysmodel.MultiJournalDetail;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface TheadpService {
	public SwizFeedback save(Theadp theadp);

	public SwizFeedback update(Theadp theadp);

	public SwizFeedback delete(Theadp theadp);

	public List<Theadp> find();

	public Theadp find(String HDNO);
	
	//public int gettheadp(); 
	public String  getthead();
}
