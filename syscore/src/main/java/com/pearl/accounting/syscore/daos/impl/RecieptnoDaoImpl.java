package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.ReceiptnoDao;
import com.pearl.accounting.sysmodel.Receiptno;


@Repository("ReceiptnoDao")
public class RecieptnoDaoImpl extends SwiziBaseDaoImpl<Receiptno> implements ReceiptnoDao {

}
