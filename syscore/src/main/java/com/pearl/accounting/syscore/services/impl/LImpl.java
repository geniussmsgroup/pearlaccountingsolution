//package com.pearl.accounting.syscore.services.impl;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.googlecode.genericdao.search.Search;
//import com.pearl.accounting.syscore.daos.LoanDao;
//import com.pearl.accounting.syscore.daos.LoanRepayment2Dao;
//import com.pearl.accounting.syscore.services.AccountService;
//import com.pearl.accounting.syscore.services.ClientStatementService;
//import com.pearl.accounting.syscore.services.GeneralLedgerService;
//import com.pearl.accounting.syscore.services.LoanRepayment2Service;
//import com.pearl.accounting.syscore.services.LoanRepayment2Service;
//import com.pearl.accounting.syscore.services.LoanService;
//import com.pearl.accounting.syscore.services.SystemUserService;
//import com.pearl.accounting.syscore.services.TheadpService;
//import com.pearl.accounting.syscore.services.TransGLService;
//import com.pearl.accounting.sysmodel.Account;
//import com.pearl.accounting.sysmodel.ClientStatement;
//import com.pearl.accounting.sysmodel.DataSourceScreen;
//import com.pearl.accounting.sysmodel.GeneralLedger;
//import com.pearl.accounting.sysmodel.Loan;
//import com.pearl.accounting.sysmodel.LoanRepayment2;
//import com.pearl.accounting.sysmodel.ProductType;
//import com.pearl.accounting.sysmodel.Status;
//import com.pearl.accounting.sysmodel.SwizFeedback;
//import com.pearl.accounting.sysmodel.TransGL;
//import com.pearl.accounting.sysmodel.TransactionType;
//import com.pearl.accounting.sysmodel.Ttype;
//
//@Service("LoanRepayment2Service")
//@Transactional
//public class LImpl implements LoanRepayment2Service {
//
//	@Autowired
//	private LoanRepayment2Dao loanRepayment2Dao;
//
//	@Autowired
//	private SystemUserService systemUserService;
//
//	@Autowired
//	private GeneralLedgerService generalLedgerService;
//
//	@Autowired
//	private AccountService accountService;
//
//	@Autowired
//	private ClientStatementService clientStatementService;
//
//	@Autowired
//	private LoanService loanService;
//	@Autowired
//	private TransGLService transglService;
//	@Autowired
//	private TheadpService theadpService;
//
//	public SwizFeedback save(LoanRepayment2 loanRepayment) {
//	
//		try {
//
//			String find = loanRepayment.getLoannumber();
//			Loan loan1 =loanService.findBy(find);
//				loanRepayment.setLoan(loan1);
//		        
//			
//			loanRepayment.setCreatedBy(systemUserService.getLoggedInUser());
//			loanRepayment.setUpdatedBy(systemUserService.getLoggedInUser());
//			loanRepayment.setThead(theadpService.gethdnoCount());
//			 Account interest = accountService.findByName("Loan Interest");
//			loanRepayment.setPayingAccountinterest(interest);
//					
//			Account payingAccount = accountService.findByName("Loan");
//			loanRepayment.setPayingAccount(payingAccount);
//        	
//			
//            
//			
//		
//			LoanRepayment2 repayment = loanRepayment2Dao.save(loanRepayment);
//			
//			
//			
//			if (repayment != null) {
//                TransGL trgl =generateTransGLdr(repayment);
//				GeneralLedger generalLedger = generateGeneralLedgerdr(repayment);
//				ClientStatement clientStatement = generateClientStatement(repayment);
//                ClientStatement clientStatementint =generateClientStatementint(repayment);
//				GeneralLedger generalLedger3=generateGeneralLedgerintcr(repayment,interest);
//				GeneralLedger generalLedger2 = generateGeneralLedgercr(repayment, payingAccount);
//				TransGL trgl3=generateTransGLintcr(repayment,interest);
//				TransGL trgl2 = generateTransGLcr(repayment, payingAccount);
//				
//				
//				if(repayment.getDepositedAmount()>0) {
//				generalLedgerService.save(generalLedger);
//				generalLedgerService.save(generalLedger2);
//                 transglService.save(trgl);
//                 transglService.save(trgl2);                 
//                clientStatementService.save(clientStatement);
//			}
//				if  (repayment.getInterest()>0) {
//                generalLedgerService.save(generalLedger3);
//				clientStatementService.save(clientStatementint);
//				  transglService.save(trgl3);
//                }
//				return new SwizFeedback(true, "Payment posted successfully");
//
//			} 
//			else {
//
//				return new SwizFeedback(false, "Loan repayment not posted successfully. Please try again.");
//			}
////			    }else {
////
////				return new SwizFeedback(false, "Loan account is not having loan. Please try again.");
////			}
//			
//			    } catch (Exception ex) {
//			System.out.println(ex.getMessage());
//			return new SwizFeedback(false, ex.getMessage());
//		}
//	}
//	// debit the receiveing account in the general ledger
//	private GeneralLedger generateGeneralLedgerdr(LoanRepayment2 repayment) {
//
//		GeneralLedger generalLedger = new GeneralLedger();
//
//		generalLedger.setAccount(repayment.getReceiveingAccount());
//		generalLedger.setAssessmentPeriod(repayment.getAssessmentPeriod());
//		generalLedger.setBalanceCredit(0);
//		generalLedger.setBalanceDebt(0);
//		generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
//		generalLedger.setDateCreated(new Date());
//		generalLedger.setDateUpdated(new Date());
//		generalLedger.setFinancialYear(repayment.getFinancialYear());
//		generalLedger.setPaymentDetails(repayment.getPaymentDetails());
//		generalLedger.setPostReferenceNo(repayment.getId());
//		generalLedger.setDataSourceScreen(DataSourceScreen.Loan);
//		generalLedger.setRemarks(ProductType.Loan.getProductType()+repayment.getRemarks());
//		generalLedger.setStatus(Status.ACTIVE);
//		generalLedger.setTransactionCredit(0);
//		generalLedger.setTransactionDebt(repayment.getDepositedAmount()+repayment.getInterest());
//		generalLedger.setTransactionDescription(ProductType.Loan.getProductType());
//		generalLedger.setTransactionType(TransactionType.DR);
//		generalLedger.setTransationDate(new Date());
//		generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
//        generalLedger.setThead(repayment.getThead());
//        return generalLedger;
//	}
//	private TransGL generateTransGLdr(LoanRepayment2 repayment) {
//
//		TransGL generalLedger = new TransGL();
//
//		generalLedger.setAcc(repayment.getReceiveingAccount());
//		generalLedger.setAssessmentPeriod(repayment.getAssessmentPeriod());
//		generalLedger.setBalanceCredit(0);
//		generalLedger.setBalanceDebt(0);
//        generalLedger.setTheadId(repayment.getThead());
//
//		generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
//		generalLedger.setDateCreated(new Date());
//		generalLedger.setDateUpdated(new Date());
//		//generalLedger.setFinancialYear(repayment.getFinancialYear());
//		generalLedger.setPayDet(repayment.getPaymentDetails());
//		generalLedger.setPostReferenceNo(repayment.getId());
//		generalLedger.setDataSourceScreen(DataSourceScreen.Loan);
//		generalLedger.setRemarks(ProductType.Loan.getProductType()+repayment.getRemarks());
//		generalLedger.setStatus(Status.ACTIVE);
//		generalLedger.setTransactionCredit(0);
//		generalLedger.setAmount(repayment.getDepositedAmount()+repayment.getInterest());;
//
//		generalLedger.setTransactionDebt(repayment.getDepositedAmount()+repayment.getInterest());
//		generalLedger.setTransactionDescription(ProductType.Loan.getProductType());
//		generalLedger.setTtype(Ttype.D);
//		//generalLedger.setTransationDate(new Date());
//		generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
//
//		return generalLedger;
//	}
//	// credit the loan A/C in the general ledger
//	private GeneralLedger generateGeneralLedgercr(LoanRepayment2 repayment, Account loanAccount) {
//		try {
//
//			if (loanAccount != null) {
//				GeneralLedger generalLedger = new GeneralLedger();
//				generalLedger.setAccount(loanAccount);
//				generalLedger.setAssessmentPeriod(repayment.getAssessmentPeriod());
//				generalLedger.setBalanceCredit(0);
//				generalLedger.setBalanceDebt(0);
//				generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
//				generalLedger.setDateCreated(new Date());
//				generalLedger.setDateUpdated(new Date());
//				generalLedger.setFinancialYear(repayment.getFinancialYear());
//				generalLedger.setPaymentDetails(repayment.getPaymentDetails());
//				generalLedger.setPostReferenceNo(repayment.getId());
//				generalLedger.setDataSourceScreen(DataSourceScreen.LoanRepayment);
//				generalLedger.setRemarks(ProductType.Loan.getProductType()+" "+repayment.getRemarks());
//				generalLedger.setStatus(Status.ACTIVE);
//				generalLedger.setTransactionCredit(repayment.getDepositedAmount());
//				generalLedger.setTransactionDebt(0);
//				generalLedger.setTransactionDescription(ProductType.Loan.getProductType());
//				generalLedger.setTransactionType(TransactionType.CR);
//				generalLedger.setTransationDate(new Date());
//				generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
//		        generalLedger.setThead(repayment.getThead());
//
//				return generalLedger;
//			}
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//		}
//		return null;
//	}
//	private TransGL generateTransGLcr(LoanRepayment2 repayment, Account loanAccount) {
//		try {
//
//			if (loanAccount != null) {
//				TransGL generalLedger = new TransGL();
//				generalLedger.setAcc(loanAccount);
//				generalLedger.setAssessmentPeriod(repayment.getAssessmentPeriod());
//				generalLedger.setBalanceCredit(0);
//				generalLedger.setBalanceDebt(0);
//				generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
//				generalLedger.setDateCreated(new Date());
//				generalLedger.setDateUpdated(new Date());
//				generalLedger.setAmount(repayment.getDepositedAmount());;
//				generalLedger.setPayDet(repayment.getPaymentDetails());
//				generalLedger.setPostReferenceNo(repayment.getId());
//				generalLedger.setDataSourceScreen(DataSourceScreen.LoanRepayment);
//				generalLedger.setRemarks(ProductType.Loan.getProductType()+" "+repayment.getRemarks());
//				generalLedger.setStatus(Status.ACTIVE);
//				generalLedger.setTransactionCredit(repayment.getDepositedAmount());
//				generalLedger.setTransactionDebt(0);
//				generalLedger.setTransactionDescription(ProductType.Loan.getProductType());
//				generalLedger.setTtype(Ttype.C);
//		        generalLedger.setTheadId(repayment.getThead());
//				//generalLedger.setTransationDate(new Date());
//				generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
//				return generalLedger;
//			}
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//		}
//		return null;
//	}
//	private GeneralLedger generateGeneralLedgerintcr(LoanRepayment2 repayment, Account interestb) {
//		try {
//
//			if (interestb != null) {
//				GeneralLedger generalLedger = new GeneralLedger();
//				generalLedger.setAccount(interestb);
//				generalLedger.setAssessmentPeriod(repayment.getAssessmentPeriod());
//				generalLedger.setBalanceCredit(0);
//				generalLedger.setBalanceDebt(0);
//				generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
//				generalLedger.setDateCreated(new Date());
//				generalLedger.setDateUpdated(new Date());
//				generalLedger.setFinancialYear(repayment.getFinancialYear());
//				generalLedger.setPaymentDetails(repayment.getPaymentDetails());
//				generalLedger.setPostReferenceNo(repayment.getId());
//				generalLedger.setDataSourceScreen(DataSourceScreen.LoanRepayment);
//				generalLedger.setRemarks(ProductType.LoanInterest.getProductType()+" "+repayment.getRemarks());
//				generalLedger.setStatus(Status.ACTIVE);
//				generalLedger.setTransactionCredit(repayment.getInterest());
//				generalLedger.setTransactionDebt(0);
//				generalLedger.setTransactionDescription(ProductType.LoanInterest.getProductType());
//				generalLedger.setTransactionType(TransactionType.CR);
//				generalLedger.setTransationDate(new Date());
//				generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
//		        generalLedger.setThead(repayment.getThead());
//
//				return generalLedger;
//			}
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//		}
//		return null;
//	}
//	private TransGL generateTransGLintcr(LoanRepayment2 repayment, Account interestA) {
//		try {
//
//			if (interestA != null) {
//				TransGL generalLedger = new TransGL();
//				generalLedger.setAcc(interestA);
//				generalLedger.setAssessmentPeriod(repayment.getAssessmentPeriod());
//				generalLedger.setBalanceCredit(0);
//				generalLedger.setBalanceDebt(0);
//				generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
//				generalLedger.setDateCreated(new Date());
//				generalLedger.setDateUpdated(new Date());
//				//generalLedger.setFinancialYear(repayment.getFinancialYear());
//				generalLedger.setPayDet(repayment.getPaymentDetails());
//				generalLedger.setPostReferenceNo(repayment.getId());
//				generalLedger.setDataSourceScreen(DataSourceScreen.LoanRepayment);
//				generalLedger.setRemarks(ProductType.LoanInterest.getProductType()+" "+repayment.getRemarks());
//				generalLedger.setStatus(Status.ACTIVE);
//				generalLedger.setTransactionCredit(repayment.getInterest());
//				generalLedger.setTransactionDebt(0);
//				generalLedger.setTransactionDescription(ProductType.LoanInterest.getProductType());
//				generalLedger.setTtype(Ttype.C);
//		        generalLedger.setTheadId(repayment.getThead());
//				generalLedger.setAmount(repayment.getInterest());;
//				generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
//				return generalLedger;
//			}
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//		}
//		return null;
//	}
//
//	private ClientStatement generateClientStatement(LoanRepayment2 repayment) {
//
//		ClientStatement clientStatement = new ClientStatement();
//
//					clientStatement.setCustomerAccount(repayment.getCustomerAccount());
//
//					clientStatement.setAssessmentPeriod(repayment.getAssessmentPeriod());
//					clientStatement.setBalanceCredit(0);
//					clientStatement.setBalanceDebt(0);
//					clientStatement.setCreatedBy(systemUserService.getLoggedInUser());
//					clientStatement.setDateCreated(new Date());
//					clientStatement.setDateUpdated(new Date());
//					clientStatement.setFinancialYear(repayment.getFinancialYear());
//					clientStatement.setPaymentDetails(repayment.getPaymentDetails());
//					clientStatement.setPostReferenceNo(repayment.getId());
//					clientStatement.setProductType(ProductType.Loan);
//					clientStatement.setRemarks(ProductType.Loan.getProductType() +" "+repayment.getRemarks());
//					clientStatement.setStatus(Status.ACTIVE);
//					clientStatement.setTransactionCredit(repayment.getDepositedAmount());
//					clientStatement.setTransactionDebt(0);
//					clientStatement.setTransactionDescription(ProductType.Loan.getProductType());
//					clientStatement.setTransactionType(TransactionType.CR);
//					clientStatement.setTransationDate(new Date());
//					clientStatement.setUpdatedBy(systemUserService.getLoggedInUser());
//					clientStatement.setThead(repayment.getThead());
//
//					return clientStatement;
//				
//
//
//		
//	}
//
//
//	private ClientStatement generateClientStatementint(LoanRepayment2 repayment) {
//
//		ClientStatement clientStatement = new ClientStatement();
//
//		
////				
//					clientStatement.setCustomerAccount(repayment.getCustomerAccount());
//
//					clientStatement.setAssessmentPeriod(repayment.getAssessmentPeriod());
//					clientStatement.setBalanceCredit(0);
//					clientStatement.setBalanceDebt(0);
//					clientStatement.setCreatedBy(systemUserService.getLoggedInUser());
//					clientStatement.setDateCreated(new Date());
//					clientStatement.setDateUpdated(new Date());
//					clientStatement.setFinancialYear(repayment.getFinancialYear());
//					clientStatement.setPaymentDetails(repayment.getPaymentDetails());
//					clientStatement.setPostReferenceNo(repayment.getId());
//					clientStatement.setProductType(ProductType.LoanInterest);
//					clientStatement.setRemarks(ProductType.LoanInterest.getProductType() +" "+repayment.getRemarks());
//					clientStatement.setStatus(Status.ACTIVE);
//					clientStatement.setTransactionCredit(repayment.getInterest());
//					clientStatement.setTransactionDebt(0);
//					clientStatement.setTransactionDescription(ProductType.LoanInterest.getProductType());
//					clientStatement.setTransactionType(TransactionType.CR);
//					clientStatement.setTransationDate(new Date());
//					clientStatement.setUpdatedBy(systemUserService.getLoggedInUser());
//					clientStatement.setThead(repayment.getThead());
//					return clientStatement;
//
//	}
//
//	public SwizFeedback update(LoanRepayment2 loanRepayment) {
//		try {
//			loanRepayment2Dao.update(loanRepayment);
//			return new SwizFeedback(true, "Payment Updated successfully");
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//			return new SwizFeedback(false, ex.getMessage());
//		}
//	}
//
//	public SwizFeedback delete(LoanRepayment2 loanRepayment) {
//		try {
//			loanRepayment2Dao.delete(loanRepayment);
//			return new SwizFeedback(true, "Payment deleted successfully");
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//			return new SwizFeedback(false, ex.getMessage());
//		}
//	}
//
//	public List<LoanRepayment2> find() {
//		try {
//			return loanRepayment2Dao.find();
//
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//
//		}
//		return null;
//	}
//
//	public LoanRepayment2 find(String id) {
//		try {
//			return loanRepayment2Dao.find(id);
//
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//
//		}
//		return null;
//	}
//	
////	
//}
