package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.PeriodicDeposit;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface PeriodicDepositService {
	public SwizFeedback save(PeriodicDeposit deposit);

	public SwizFeedback update(PeriodicDeposit deposit);

	public SwizFeedback delete(PeriodicDeposit deposit);

	public List<PeriodicDeposit> find();

	public PeriodicDeposit find(String id);
	
	public float getTotalDeposits(CustomerAccount Sysid);


}
