package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.SystemUser;
import com.pearl.accounting.sysmodel.UserPermission;

public interface SystemUserService {

	public SwizFeedback save(SystemUser systemUser);

	public SwizFeedback update(SystemUser systemUser);

	public SwizFeedback delete(SystemUser systemUser);

	public List<SystemUser> find();

	public SystemUser find(String id);

	public SystemUser findByUserName(String username);
	
	public SystemUser getLoggedInUser();
	
	public List<UserPermission> getLoginUserPermissions();
	
	public SwizFeedback resetUserPassword(SystemUser usr);

}
