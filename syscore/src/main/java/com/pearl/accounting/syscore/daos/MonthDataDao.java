package com.pearl.accounting.syscore.daos;

import com.pearl.accounting.sysmodel.MonthData;

public interface MonthDataDao extends SwiziBaseDao<MonthData>{

}
