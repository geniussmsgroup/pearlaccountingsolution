package com.pearl.accounting.syscore.daos;

import com.pearl.accounting.sysmodel.SystemUser;

public interface SystemUserDao extends SwiziBaseDao<SystemUser>{

}
