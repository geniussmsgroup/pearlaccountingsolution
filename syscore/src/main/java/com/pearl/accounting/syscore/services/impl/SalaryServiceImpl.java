package com.pearl.accounting.syscore.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pearl.accounting.syscore.daos.SalaryDao;
import com.pearl.accounting.syscore.services.SalaryService;
import com.pearl.accounting.sysmodel.Salary;
import com.pearl.accounting.sysmodel.SwizFeedback;

@Service("SalaryService")
@Transactional
public class SalaryServiceImpl implements SalaryService {

	@Autowired
	private SalaryDao salaryDao;

	public SwizFeedback save(Salary salary) {
		try {
			salaryDao.save(salary);
			return new SwizFeedback(true, "Success");
		} catch (Exception exception) {
			System.out.println(exception.getMessage());
		}
		return null;
	}

	public List<Salary> find() {
		try {
			return salaryDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

}
