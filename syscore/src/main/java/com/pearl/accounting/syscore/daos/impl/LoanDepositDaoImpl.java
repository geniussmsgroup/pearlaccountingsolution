package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.LoanDepositDao;
import com.pearl.accounting.sysmodel.LoanDeposit;

@Repository("LoanDepositDao")
public class LoanDepositDaoImpl extends SwiziBaseDaoImpl<LoanDeposit> implements LoanDepositDao{

}
