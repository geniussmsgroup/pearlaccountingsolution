package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysmodel.LoanRepayment;
import com.pearl.accounting.sysmodel.RepaymentSchedule;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface LoanRepaymentService {

	public SwizFeedback save(LoanRepayment loanRepayment);

	public SwizFeedback update(LoanRepayment deposit);

	public SwizFeedback delete(LoanRepayment deposit);

	public List<LoanRepayment> find();

	public LoanRepayment find(String id);

	public List<LoanRepayment> find(Loan loan);
	
public List<LoanRepayment> getLoanRepayment(Loan loan);

}
