package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.Init;
import com.pearl.accounting.sysmodel.SwizFeedback;


public interface AccountSService {
	public SwizFeedback save(Init init);

	public SwizFeedback update(Init init);

	public SwizFeedback delete(Init init);

	public List<Init> find();

	public Init find(String ACode);
	
	public Init findByName(String Aname);

}
