package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.CashBookReceipt;
import com.pearl.accounting.sysmodel.CashBookReceiptDetail;
import com.pearl.accounting.sysmodel.Multi;
import com.pearl.accounting.sysmodel.MultiDetail;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface MultiService {
	
	public SwizFeedback save(Multi multi);

	public SwizFeedback update(  Multi multi);

	public SwizFeedback delete(  Multi multi);

	public List<Multi> find();

	public Multi find(String id);
	
	public List<MultiDetail> find(Multi multi);

}
