package com.pearl.accounting.syscore.services.impl;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.AccountDao;
import com.pearl.accounting.syscore.daos.IntervaltbDao;
import com.pearl.accounting.syscore.services.AccountService;
import com.pearl.accounting.syscore.services.IntervaltbService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.sysmodel.AssessmentPeriod;
import com.pearl.accounting.sysmodel.Intervaltb;
import com.pearl.accounting.sysmodel.ProductType;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.Theadp;

@Service("IntervaltbService")
@Transactional
public class IntervaltbServiceImpl implements IntervaltbService {

	@Autowired
	private IntervaltbDao intervaltbDao;
	
	@Autowired
	private SystemUserService systemUserService;

	public SwizFeedback save(Intervaltb interval) {
		try {
			intervaltbDao.save(interval);
			return new SwizFeedback(true, "Saved successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}

	}

	public SwizFeedback update(Intervaltb interval) {
		try {
			intervaltbDao.update(interval);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}
	}

	public SwizFeedback delete(Intervaltb interval) {
		try {
			intervaltbDao.delete(interval);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}
	}

	public List<Intervaltb> find() {
		try {

			return intervaltbDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public Intervaltb find(String Sysid) {
		try {

			return intervaltbDao.find(Sysid);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}
	
	public Intervaltb findByName(String accountName) {
		try {
			
			Search search=new Search();
			search.addFilterILike("accountName", accountName); 
			return intervaltbDao.searchUnique(search);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}
	public Intervaltb findByName2(String accountName) {
		try {
			
			Search search=new Search();
			search.addFilterILike("accountName", accountName); 
			return intervaltbDao.searchUnique(search);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	

	public SwizFeedback activeperiod(Intervaltb intervaltb) {
		try {
           System.out.println("test the id "+intervaltb.getSysId());
			Intervaltb period = find(intervaltb.getSysId());
 
			if (period != null) {
				System.out.println("testafter "+period.getStatus());
				period.setStatus(Status.ACTIVE);
				intervaltbDao.update(period);
				return new SwizFeedback(true, "Activated successfully");
			} else {
				return new SwizFeedback(true, "Assessement period does not exist.");
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}

	}
	public List<Intervaltb> findActive() {
		try {
			Search search = new Search();
			search.addFilterEqual("status", Status.ACTIVE);
			List<Intervaltb> assessmentPeriods = intervaltbDao.search(search);
			if (assessmentPeriods != null) {
				if (!assessmentPeriods.isEmpty()) {
					 assessmentPeriods.get(0);
					 return assessmentPeriods;
				}
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	
		


}
