package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.GeneralLedger;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface GeneralLedgerService {

	public SwizFeedback save(GeneralLedger generalLedger);

	public SwizFeedback update(GeneralLedger generalLedger);

	public SwizFeedback delete(GeneralLedger generalLedger);

	public List<GeneralLedger> find();

	public GeneralLedger find(String id);
	
	public SwizFeedback save(List<GeneralLedger> generalLedgers);

}
