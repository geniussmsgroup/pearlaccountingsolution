package com.pearl.accounting.syscore.daos;

import com.pearl.accounting.sysmodel.CashBookReceipt;

public interface CashBookReceiptDao extends SwiziBaseDao<CashBookReceipt>{

}
