package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.ProductSetupDao;
import com.pearl.accounting.sysmodel.ProductSetup;



@Repository("ProductSetupDao")
public class ProductSetupDaoImpl extends SwiziBaseDaoImpl<ProductSetup> implements ProductSetupDao {
}
