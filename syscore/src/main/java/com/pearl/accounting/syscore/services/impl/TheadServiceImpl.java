package com.pearl.accounting.syscore.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pearl.accounting.syscore.daos.TheadDao;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.syscore.services.TheadService;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.Thead;

@Transactional
@Service("TheadService")
public class TheadServiceImpl implements TheadService {

	@Autowired
	private TheadDao theadDao;
	@Autowired
	private SystemUserService systemUserService;

	@Override
	public String generateCode() {
		try {
			Thead thead = new Thead();
			thead.setCreatedBy(systemUserService.getLoggedInUser());
			thead.setUpdatedBy(systemUserService.getLoggedInUser());
			thead.setDateCreated(new Date());
			thead.setDateUpdated(new Date());
			thead.setStatus(Status.ACTIVE);
			thead.setHdNo((getRecordCount()+1)+"");
			Thead thead2=theadDao.save(thead);
			if(thead2!=null) {
				return thead2.getHdNo();
			}
		} catch (Exception ex) {

		}
		return null;
	}


	private long getRecordCount() {
		int count = 0;
		try {

			List<Thead> theads = theadDao.findAll();
			count = theads.size();

		} catch (Exception ex) {

		}

		return count;
	}

}
