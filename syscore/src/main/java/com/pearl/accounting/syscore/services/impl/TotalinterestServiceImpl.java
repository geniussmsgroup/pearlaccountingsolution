
package com.pearl.accounting.syscore.services.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pearl.accounting.syscore.daos.totalinterestDao;
import com.pearl.accounting.syscore.services.AccountService;
import com.pearl.accounting.syscore.services.AtypeAservice;
import com.pearl.accounting.syscore.services.CustomerAccountService;
import com.pearl.accounting.syscore.services.FixedDataService;
import com.pearl.accounting.syscore.services.GeneralLedgerService;
import com.pearl.accounting.syscore.services.LoanBalanceViewService;
import com.pearl.accounting.syscore.services.LoanService;
import com.pearl.accounting.syscore.services.LoaninterestViewService;
import com.pearl.accounting.syscore.services.ProductSetupService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.syscore.services.TheadpService;
import com.pearl.accounting.syscore.services.TotalinterestService;
import com.pearl.accounting.syscore.services.TransGLService;
import com.pearl.accounting.syscore.services.TransMLService;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.DataSourceScreen;
import com.pearl.accounting.sysmodel.FixedData;
import com.pearl.accounting.sysmodel.Fsave;
import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysmodel.LoanbalanceView;
import com.pearl.accounting.sysmodel.LoaninterestView;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.SystemUser;
import com.pearl.accounting.sysmodel.Theadp;
import com.pearl.accounting.sysmodel.Totalinterest;
import com.pearl.accounting.sysmodel.TransGL;
import com.pearl.accounting.sysmodel.TransML;
import com.pearl.accounting.sysmodel.Ttype;

@Service("TotalinterestService")
@Transactional
public class TotalinterestServiceImpl implements TotalinterestService {

	@Autowired
	private totalinterestDao totalinterestDao;
	@Autowired
	private FixedDataService fixeddataService;
	@Autowired
	private AtypeAservice atypeService;
	@Autowired
	private ProductSetupService productsetupService;
	@Autowired
	private LoanBalanceViewService loanbalService;
	@Autowired
	private LoaninterestViewService loanintService;
	@Autowired
	private LoanService loanService;
	@Autowired
	private AccountService accountService;
	@Autowired
	private SystemUserService systemUserService;
	@Autowired
	private TransMLService transmlService;
	@Autowired
	private TransGLService transglService;
	@Autowired
	private GeneralLedgerService generalledgerService;
	@Autowired
	private TheadpService theadpService;
	@Autowired
	private CustomerAccountService customerAccountService;

	public SwizFeedback save(Totalinterest totalinterest) {
		try {

			totalinterest.setCreatedBy(systemUserService.getLoggedInUser());
			totalinterest.setUpdatedBy(systemUserService.getLoggedInUser());
			totalinterest.setStatus(Status.ACTIVE);
			totalinterest.setDateCreated(new Date());
			totalinterest.setDateUpdated(new Date());
			totalinterest.setAmount(gettotalinterest());
			totalinterest.setSysid("0");
			totalinterest.setThead(theadpService.getthead());
			//// totalinterest.setTdate(getfixeddate().getTdate());
//			totalinterest.setATYPECD(AtypeA(totalinterest));
//			totalinterest.setProductid(productid(totalinterest));
			Totalinterest totalint = totalinterestDao.save(totalinterest);
			if (totalint != null) {
				Theadp thead = savetothead(totalint);
				TransGL transgld = gettransgldebit(totalint);
				TransGL transglc = gettransglcredit(totalint);
				List<TransML> transml = generatememberstmc(totalint);
				FixedData fix = addifixeddate();
				theadpService.save(thead);
				transglService.save(transgld);
				transglService.save(transglc);
				transmlService.save(transml);
				fixeddataService.update(fix);
			}
			// Date.parse(s)
			return new SwizFeedback(true, "The interest for " + " " + totalint.getTdate()
					+ " and the total interest is  " + totalint.getAmount()+" is Save Successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}

	}

	public SwizFeedback update(Totalinterest totalinterest) {
		try {
			totalinterestDao.update(totalinterest);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}
	}

	public SwizFeedback delete(Totalinterest totalinterest) {
		try {
			totalinterestDao.delete(totalinterest);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}
	}

	public List<Totalinterest> find() {
		try {

			return totalinterestDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

//	public Date   getdates() {
//           		
//		
//		return null;
//		
//	}
////////////saveing to theadp table
	public Theadp savetothead(Totalinterest totalinterest) {
		Account acode = accountService.find("8");
		Account ocode = accountService.findByName2("31");

		Theadp thead = new Theadp();
		thead.setAccount(acode);
		thead.setCustomerAccount(null);
		thead.setAmount(totalinterest.getAmount());
		thead.setRemarks("PeriodicDayComputeInterest");
		thead.setOCODE(ocode);
		thead.setAcode(acode.getACODE());
		thead.setAcctNo(ocode.getACODE());
		thead.setStatus(Status.ACTIVE);
		thead.setTdates(totalinterest.getTdate());
	    thead.setHDNO(totalinterest.getThead());
		thead.setYear(totalinterest.getYearcode());
		thead.setInterest(0);
		thead.setDateCreated(new Date());
		thead.setDateUpdated(new Date());
		thead.setDataSourceScreen(DataSourceScreen.LoanInterest);
		thead.setPeriodcode(totalinterest.getPeriodcode());
		return thead;
	}

/////////////saving the debit to transgl 
	////////////Loan Interest Receivable
	public TransGL gettransgldebit(Totalinterest totalinterest) {
		// TransGL generalledger =new TransGL();
		Account acode = accountService.findByName2("8");

		TransGL gl = new TransGL();
		gl.setACODE(acode.getACODE());

		gl.setDateCreated(new Date());
		gl.setDateUpdated(new Date());
		gl.setDataSourceScreen(DataSourceScreen.LoanInterest);
		gl.setAccount(acode);
		gl.setAMOUNT(gettotalinterest());
///*************gl.setTheadID(loan.getThead());
		gl.setTDATE(totalinterest.getTdate());
		gl.setTTYPE(Ttype.D);
		gl.setPAYDET("PeriodicDayComputeInterest");
		gl.setREMARKS("PeriodicDayComputeInterest");
		gl.setFsave(Fsave.Y);
		gl.setCustomeraccount(null);
		gl.setRDATE(new Date());
		gl.setYRCODE(totalinterest.getYearcode());
		gl.setICODE(totalinterest.getPeriodcode());
		// gl.setYRCODE(0);
		gl.setTrecN("0");
		gl.setTrecN("0");
		gl.setMemberID(null);
		gl.setStatus(Status.ACTIVE);
		gl.setDateCreated(new Date());
		gl.setDateUpdated(new Date());
		gl.setDataSourceScreen(DataSourceScreen.LoanInterest);
		gl.setICODE("0");

		return gl;

	}

/////////////saving the credit to transgl
	//////////saving to Accrued Interest 	
	public TransGL gettransglcredit(Totalinterest totalinterest) {
		// TransGL generalledger =new TransGL();
		Account acode = accountService.findByName2("31");

		TransGL gl = new TransGL();
		gl.setACODE(acode.getACODE());
		gl.setStatus(Status.ACTIVE);
		gl.setDateCreated(new Date());
		gl.setDateUpdated(new Date());
		gl.setDataSourceScreen(DataSourceScreen.LoanInterest);
		gl.setAccount(acode);
		gl.setAMOUNT(gettotalinterest());
///*************gl.setTheadID(loan.getThead());
		gl.setTDATE(totalinterest.getTdate());
		gl.setTTYPE(Ttype.C);
		gl.setPAYDET("PeriodicDayComputeInterest");
		gl.setREMARKS("PeriodicDayComputeInterest");
		gl.setFsave(Fsave.Y);
		gl.setCustomeraccount(null);
		gl.setRDATE(new Date());
		gl.setYRCODE(totalinterest.getYearcode());
		gl.setICODE(totalinterest.getPeriodcode());
		// gl.setYRCODE(0);
		gl.setTrecN("0");
		gl.setTrecN("0");
		gl.setMemberID(null);

		gl.setICODE("0");

		return gl;

	}

/////////////saving list   to transml 
	private List<TransML> generatememberstmc(Totalinterest totalinterest) {

		List<TransML> list = new ArrayList<TransML>();
		try {
			List<LoanbalanceView> loanbals = loanbalService.find();
			float inttotal = 0;
			String fear = null;
			for (LoanbalanceView loanbal : loanbals) {
				// LoanbalanceView loanbalance = new LoanbalanceView();
				if ((loanbal.getLnbal() > 0) && (loanbal.getMemberId() != null)) {

					LoaninterestView rate = loanintService.getmemberids(loanbal); // String membre=
																					// loanintService.getmemberids(loanbals)
																					// ;

					if ((rate.getInterestRate() > 0)) {
						float interest = Math.round(loanbal.getLnbal() * rate.getInterestRate() * 12 / 365F);
						inttotal = inttotal + interest;
						fear = rate.getMemberId();

						System.out.println("for the clientcode is " + fear + " and the code is  " + rate.getLoanID()
								+ " " + interest);
						if (loanbal.getMemberId() != null) {
							CustomerAccount ant = customerAccountService.findByName(fear);

							TransML gl = new TransML();
							gl.setCreatedBy(systemUserService.getLoggedInUser());
							gl.setUpdatedBy(systemUserService.getLoggedInUser());
							gl.setDateCreated(new Date());
							gl.setDateUpdated(new Date());
							gl.setStatus(Status.ACTIVE);
							gl.setDataSourceScreen(DataSourceScreen.MULTI_JOURNAL);
							gl.setMEMBERID(fear);
							gl.setCustomeraccount(ant);
							gl.setFSAVE(Fsave.Y);
							Loan loan = loanService.find(String.valueOf(rate.getLoanID()));
							// int loanid = Integer.parseInt(rate.getLoanID());
							gl.setLOANID(rate.getLoanID());
							gl.setTDATE(totalinterest.getTdate());
							gl.setAMOUNT(interest);
							gl.setPAYDET("PeriodicDayComputeInterest");
							gl.setVNO("PeriodicDayComputeInterest");
							gl.setTHEADID(totalinterest.getThead());
							gl.setLoan(loan);
							gl.setPRODUCTID("LIRP");
							SystemUser staff = systemUserService.find(totalinterest.getUpdatedBy().getId());
							String name = staff.getUsername();
							gl.setSTAFFNAME(name);
							gl.setStatus(Status.ACTIVE);
							gl.setTTYPE(Ttype.D);
							list.add(gl);
							System.out.println("list: " + list.size());
						} else {
							System.out.println(" account null");

						}
					}
				}
			}
		} catch (Exception ex) {
			System.out.println("details: " + ex.getMessage());

		}

		return list;
	}

//////////adding to the table
	public FixedData addifixeddate() {
		///FixedData tdate2 = new FixedData();
		FixedData fixed = fixeddataService.find("1");
	//	for (FixedData fixed : fixeds) {
			fixed.getNextIntDte();
			System.out.println("the old date  is =  " + fixed.getNextIntDte());

			Calendar cal = Calendar.getInstance();
			cal.setTime(fixed.getNextIntDte());

			// Adding 1 Day to the current date
			cal.add(Calendar.DAY_OF_MONTH, 1);
			Date te = cal.getTime();			
			// tdate2.setBFToDate(te);
			fixed.setNextIntDte(te);

			fixed.setIDNo(fixed.getIDNo());
		    fixed.setTheadid(fixed.getTheadid()); 
//		int newtheadid=	fixed.getTheadid() +1;
//			tdate2.setTheadid(newtheadid);
			

		//}
		System.out.println("the new date is= " + fixed.getNextIntDte());
		// Date after adding one day to the current date
		return fixed;

	}

////////////here am get the maxdate in  fixed data table 
//	public Totalinterest getfixeddate() {
//		Totalinterest tdate = new Totalinterest();
//		List<FixedData> fixeds = fixeddataService.find();
//		for (FixedData fixed : fixeds) {
//			tdate.setTdate(fixed.getNextIntDte());
//			System.out.println("date for inserting stm " + fixed.getNextIntDte());
//		}
//		return tdate;
//
//	}
////////generating for the total
	public List<LoanbalanceView> getinterest() {
		List<LoanbalanceView> list = new ArrayList<LoanbalanceView>();
		// float interest = 0;

		List<LoanbalanceView> loanbal = loanbalService.find();
		float inttotal = 0;
		String fear = null;
		for (LoanbalanceView loanbals : loanbal) {
			LoanbalanceView loanbalance = new LoanbalanceView();
			if ((loanbals.getLnbal() > 0) && (loanbals.getMemberId() != null)) {

				LoaninterestView rate = loanintService.getmemberids(loanbals);
				// String membre= loanintService.getmemberids(loanbals) ;

				if ((rate.getInterestRate() > 0)) {
					float interest = Math.round(loanbals.getLnbal() * rate.getInterestRate() * 12 / 365F);
					inttotal = inttotal + interest;
					fear = rate.getMemberId();
//
//					System.out.println(
//							"for the clientcode is " + fear + " and the code is  " + rate.getLoanID() + " " + interest);
					loanbalance.setMemberId(fear);

					loanbalance.setLnbal(interest);

				}
			}
			list.add(loanbalance);

		}
		System.out.println("compuinting interest =" + (Math.round(inttotal)));

		return list;

	}

/////////////////method that picsthe total;
	public float gettotalinterest() {
		float total = 0;
		try {
			List<LoanbalanceView> tot = getinterest();
			for (LoanbalanceView ans : tot) {
				// total
				float answer = ans.getLnbal() + 0;
				total = total + answer;
			}
			System.out.println("total =" + total);

		} catch (Exception e) {
			// TODO: handle exception
		}

		return total;

	}

//	public String findmemberid(String MemberId) {
//		String memberid = null;
//			try {
//				LoanBalance acode = atypeService.find(account.getAccountType().getAtypecd());
//	                 accountcd = acode.getAtypecd();
//	           
//			} catch (Exception ex) {
//				System.out.println(ex.getMessage());
//	
//			}		return accountcd;	
//			
//		}
//	
//	

//	public  float[]  productid() {
//		String Object = null;
//		try {
//			List<LoanbalanceView> loanbal = loanbalService.find();
//			
//			for () {
//				
//			}
//			
//			
//			
//			
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//	
//		}
//		return ;	
//		}
//       

//	public LoaninterestView[] find(List<String> MemberId) {
//		try {
//
//			String[] ledgers=new String[MemberId.size()];
////			
//			return loaninterestViewDao.find(MemberId.toArray(ledgers));
//
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//
//		}
//		return null;
//	}

//	public SwizFeedback save(List<GeneralLedger> generalLedgers) {
//		try {
//			GeneralLedger[] ledgers=new GeneralLedger[generalLedgers.size()];
//			
//			generalLedgerDao.save(generalLedgers.toArray(ledgers));
//			return new SwizFeedback(true, "Transaction posted successfully");
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//			return new SwizFeedback(false, ex.getMessage());
//		}
//
//	}

//	public Totalinterest find(String Sysid) {
//		try {
//
//			return accountDao.find(Sysid);
//
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//
//		}
//		return null;
//	}
//
//	public Account findByName(String accountName) {
//		try {
//
//			Search search = new Search();
//			search.addFilterILike("accountName", accountName);
//			return accountDao.searchUnique(search);
//
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//
//		}
//		return null;
//	}
//
//	public Account findByName2(String productType) {
//		try {
//
//			Search search = new Search();
//			search.addFilterILike("productType", productType);
//			return accountDao.searchUnique(search);
//
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//
//		}
//		return null;
//	}
//	public String AtypeA(Account account) {
//	String accountcd = null;
//		try {
//			AtypeA acode = atypeService.find(account.getAccountType().getAtypecd());
//                 accountcd = acode.getAtypecd();
//           
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//
//		}		return accountcd;	
//		
//	}
//public  String productid(Account account) {
//	String productsetup = null;
//	try {
//          ProductSetup producttype = productsetupService.find(account.getProductType().getProductId());
//                     productsetup = producttype.getProductId();
//
//	} catch (Exception ex) {
//		System.out.println(ex.getMessage());
//
//	}
//	return productsetup;	
//	}
//
//@Override
//public Account findcbal(Float cbal) {
//	// TODO Auto-generated method stub
//	return null;
//}
// 
}
