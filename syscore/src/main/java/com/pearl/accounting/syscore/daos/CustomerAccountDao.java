package com.pearl.accounting.syscore.daos;

import com.pearl.accounting.sysmodel.CustomerAccount;

public interface CustomerAccountDao extends SwiziBaseDao<CustomerAccount>{

}
