//package com.pearl.accounting.syscore.services.impl;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
////import com.pearl.accounting.sysui.client.views.windows.DoubleEntryWindow;
//
//import com.googlecode.genericdao.search.Search;
//
//import com.pearl.accounting.syscore.daos.TheadpDao;
//import com.pearl.accounting.syscore.services.SystemUserService;
//import com.pearl.accounting.syscore.services.TheadpService;
//import com.pearl.accounting.syscore.services.TransGLService;
//
//import com.pearl.accounting.sysmodel.TransGL;
//import com.pearl.accounting.sysmodel.Theadp;
//import com.pearl.accounting.sysmodel.Status;
//import com.pearl.accounting.sysmodel.SwizFeedback;
//import com.pearl.accounting.sysmodel.Ttype;
//
//@Service("TheadpService")
//@Transactional
//public class copy implements TheadpService {
//
//	@Autowired
//	private TheadpDao theadpDao;
//
//	@Autowired
//	private SystemUserService systemUserService;
//
//	@Autowired
//	private TransGLService transglService;
//	
//	
//
//	public SwizFeedback save(Theadp theadp) {
//		try {
//			theadp.setCreatedBy(systemUserService.getLoggedInUser());
//			theadp.setUpdatedBy(systemUserService.getLoggedInUser());
//			//theadp.setThead(theadpService.generateCode());
//
//			 theadpDao.save(theadp);
//			 return new SwizFeedback(true, "Saved successfully");
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//			return new SwizFeedback(true, ex.getMessage());
//		}
//
//			// Theadp theadp1 =
////			if (theadp1 != null) {
////
////				SwizFeedback feedback = transglService.save(generateGeneralLedger(theadp1));
////
////				if (feedback.isResponse()) {
////					return new SwizFeedback(true, " posted successfully posted");
////				} else {
////					return feedback;
////				}
////			} else {
////				return new SwizFeedback(false, "Error while posting transaction. Please try again.");
////			}
//
////		} catch (Exception ex) {
////			System.out.println(ex.getMessage());
////			return new SwizFeedback(true, ex.getMessage());
////		}
//
////	}
////
////
////	// debit and credit the receiveing account and Paying account in the general
////	// ledger
////	private TransGL generateGeneralLedger(Theadp theadp2) {
////            
////		
////		     TransGL generalLedger = new TransGL();
////		    
////		    
////		  // debit account
////		    // theadp2.getTransgl(generalLedger);   
////			generalLedger.setAcode(theadp2.getDebit());
////			generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
////			generalLedger.setDateCreated(new Date());
////			generalLedger.setDateUpdated(new Date());
////			generalLedger.setSdate(theadp2.getTdate());
////			generalLedger.setPayDet(theadp2.getRemarks());
////			generalLedger.setId(theadp2.getId());
////			generalLedger.setStatus(Status.ACTIVE);
////			generalLedger.setTtype(Ttype.D);
////			generalLedger.setAmount(theadp2.getAmount());
////			generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
////			generalLedger.setTheadId(theadp2.getHDNO());
////
////			//credit
////		
////
////			    generalLedger.setTtype(Ttype.C);
////				generalLedger.setAcode(theadp2.getCredit());
////				generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
////				generalLedger.setDateCreated(new Date());
////				generalLedger.setDateUpdated(new Date());
////				generalLedger.setSdate(theadp2.getTdate());
////				generalLedger.setPayDet(theadp2.getRemarks());
////				generalLedger.setId(theadp2.getId());
////				generalLedger.setStatus(Status.ACTIVE);
////				generalLedger.setTransactionDebt(0);
////				generalLedger.setAmount(theadp2.getAmount());
////				generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
////				generalLedger.setTheadId(theadp2.getHDNO());
////				
////				
////				return generalLedger;
////		
////	
//	}
//
//	public SwizFeedback update(Theadp theadp) {
//		try {
//			// cashBookPayment.setCreatedBy(systemUserService.getLoggedInUser());
//		        theadp.setUpdatedBy(systemUserService.getLoggedInUser());
//
//			theadpDao.update(theadp);
//			return new SwizFeedback(true, "Payment successfully Updated");
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//			return new SwizFeedback(true, ex.getMessage());
//		}
//	}
//
//	public SwizFeedback delete(Theadp theadp) {
//		try {
//			// cashBookPayment.setCreatedBy(systemUserService.getLoggedInUser());
//			// cashBookPayment.setUpdatedBy(systemUserService.getLoggedInUser());
//
//			theadpDao.delete(theadp);
//			return new SwizFeedback(true, "Payment successfully Deleted");
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//			return new SwizFeedback(true, ex.getMessage());
//		}
//
//	}
//	public List<Theadp> find() {
//		try {
//
//			return theadpDao.find();
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//
//		}
//		return null;
//	}
//	
//	public Theadp find(String HDNO) {
//		try {
//			return theadpDao.find(HDNO);
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//		}
//		return null;
//	}
//	public Theadp findByName(String paydet) {
//		try {
//			
//			Search search=new Search();
//			search.addFilterILike("paydet", paydet); 
//			return theadpDao.searchUnique(search);
//
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//
//		}
//		return null;
//	}
//
//
//	
//	
//	
//}
