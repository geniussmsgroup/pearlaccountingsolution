package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.FinancialYearDao;
import com.pearl.accounting.sysmodel.FinancialYear;
 

@Repository("FinancialYearDao")
public class FinancialYearDaoImpl extends SwiziBaseDaoImpl<FinancialYear> implements FinancialYearDao {

}
