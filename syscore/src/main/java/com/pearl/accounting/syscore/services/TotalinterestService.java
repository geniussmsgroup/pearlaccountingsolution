package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.Totalinterest;

public interface TotalinterestService {

	public SwizFeedback save(Totalinterest totalinterest);

	public SwizFeedback update(Totalinterest totalinterest);

	public SwizFeedback delete(Totalinterest totalinterest);

	public List<Totalinterest> find();
//    public Account findcbal(Float cbal  );
//	public Account find(String Sysid);
//
//	public Account findByName(String accountName);
//
//	public Account findByName2(String productType);
//	public String AtypeA(Account account);
//	public String productid(Account account);
}
