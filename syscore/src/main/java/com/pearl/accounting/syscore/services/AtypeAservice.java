package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.AtypeA;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface AtypeAservice {
	public SwizFeedback save(AtypeA atypea);

	public SwizFeedback update(AtypeA atypea);

	public SwizFeedback delete(AtypeA atypea);

	public List<AtypeA> find();

	public AtypeA find(String id);
	
	public AtypeA findByName(String atypecd);

}
