package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.AccountDao;
import com.pearl.accounting.syscore.daos.totalinterestDao;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.Totalinterest;

@Repository("totalinterestDao")
public class totalinterestDaoImpl extends SwiziBaseDaoImpl<Totalinterest> implements totalinterestDao {

}
