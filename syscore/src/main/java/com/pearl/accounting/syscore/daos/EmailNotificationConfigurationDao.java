package com.pearl.accounting.syscore.daos;

import com.pearl.accounting.sysmodel.EmailNotificationConfiguration;

public interface EmailNotificationConfigurationDao extends SwiziBaseDao<EmailNotificationConfiguration> {

}
