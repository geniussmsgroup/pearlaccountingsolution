package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.LoanDeposit;
import com.pearl.accounting.sysmodel.Loantest;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface LoanDepositService {
	public SwizFeedback save(LoanDeposit loandeposit);

	public SwizFeedback update(LoanDeposit loandeposit);

	public SwizFeedback delete(LoanDeposit loandeposit);

	public List<LoanDeposit> find();

	public LoanDeposit find(String Id);
	/// public float getloanbalance(CustomerAccount Sysid);

}
