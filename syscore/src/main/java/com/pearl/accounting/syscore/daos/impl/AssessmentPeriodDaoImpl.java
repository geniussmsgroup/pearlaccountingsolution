package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.AssessmentPeriodDao;
import com.pearl.accounting.sysmodel.AssessmentPeriod;
 

@Repository("AssessmentPeriodDao")
public class AssessmentPeriodDaoImpl extends SwiziBaseDaoImpl<AssessmentPeriod> implements AssessmentPeriodDao {

}
