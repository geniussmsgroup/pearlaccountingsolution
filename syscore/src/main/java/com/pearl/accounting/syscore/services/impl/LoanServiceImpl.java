//package com.pearl.accounting.syscore.services.impl;
//
//import java.text.DecimalFormat;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.List;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//import com.googlecode.genericdao.search.Search;
//import com.pearl.accounting.syscore.daos.LoanDao;
//import com.pearl.accounting.syscore.daos.RepaymentScheduleDao;
//import com.pearl.accounting.syscore.services.AccountService;
//import com.pearl.accounting.syscore.services.ClientStatementService;
//import com.pearl.accounting.syscore.services.CustomerAccountService;
//import com.pearl.accounting.syscore.services.FinancialYearService;
//import com.pearl.accounting.syscore.services.GeneralLedgerService;
//import com.pearl.accounting.syscore.services.IntervaltbService;
//import com.pearl.accounting.syscore.services.LoanService;
//import com.pearl.accounting.syscore.services.SystemUserService;
//import com.pearl.accounting.syscore.services.TheadService;
//import com.pearl.accounting.syscore.services.TheadpService;
//import com.pearl.accounting.syscore.services.TransGLService;
//import com.pearl.accounting.syscore.services.TransMLService;
//import com.pearl.accounting.sysmodel.Account;
//import com.pearl.accounting.sysmodel.ClientStatement;
//import com.pearl.accounting.sysmodel.CustomerAccount;
//import com.pearl.accounting.sysmodel.DataSourceScreen;
//import com.pearl.accounting.sysmodel.FinancialYear;
//import com.pearl.accounting.sysmodel.Fsave;
//import com.pearl.accounting.sysmodel.GeneralLedger;
//import com.pearl.accounting.sysmodel.InterestType;
//import com.pearl.accounting.sysmodel.Intervaltb;
//import com.pearl.accounting.sysmodel.Loan;
//import com.pearl.accounting.sysmodel.ProductType;
//import com.pearl.accounting.sysmodel.RepaymentSchedule;
//import com.pearl.accounting.sysmodel.Status;
//import com.pearl.accounting.sysmodel.SwizFeedback;
//import com.pearl.accounting.sysmodel.SystemUser;
//import com.pearl.accounting.sysmodel.TransGL;
//import com.pearl.accounting.sysmodel.TransML;
//import com.pearl.accounting.sysmodel.Ttype;
//
//@Service("LoanService")
//@Transactional
//public class LoanServiceImpl implements LoanService {
//
//	@Autowired
//	private LoanDao loanDao;
//
//	@Autowired
//	private RepaymentScheduleDao repaymentScheduleDao;
//
//	@Autowired
//	private SystemUserService systemUserService;
//
//	@Autowired
//	private FinancialYearService financialYearService;
//
//	@Autowired
//	private GeneralLedgerService generalLedgerService;
//
//	@Autowired
//	private ClientStatementService clientStatementService;
//
//	@Autowired
//	private AccountService accountService;
//	@Autowired
//	private CustomerAccountService customeraccountService;
//
//	@Autowired
//	private TheadpService theadpService;
//	@Autowired
//	private TransGLService transglService;
//	@Autowired
//	private TransMLService transmlService;
//	@Autowired
//	private IntervaltbService intervalService;
//
//	public SwizFeedback save(Loan loan) {
//		try {
//
//			// generate payment schedule
//			loan.setCreatedBy(systemUserService.getLoggedInUser());
//			loan.setUpdatedBy(systemUserService.getLoggedInUser());
//			loan.setMEMBERID((getclient(loan)));
//			loan.setThead(theadpService.gettheadp());
//
//			List<RepaymentSchedule> repaymentSchedules = generateRepaymentSchedule(loan);
//
//			if (repaymentSchedules != null) {
//
//				if (!repaymentSchedules.isEmpty()) {
//
//					//loan.setRepaymentSchedules(repaymentSchedules);
//
//					Account receivingAccount = accountService.findByName(ProductType.Loan.getProductType());
//					loan.setReceivingAccount(receivingAccount);
//
//					Loan loan2 = loanDao.save(loan);
//
//					if (loan2 != null) {
//
//						TransGL generalLedger3 = generateGLc(loan2);
//						TransGL generalLedger4 = generateGLc(loan2, receivingAccount);
//
//						// GeneralLedger generalLedger = generateGeneralLedger(loan2);
//
//						ClientStatement clientStatement = generateClientStatement(loan2);
//						// ClientStatement clientStatement2 = generateInterests(loan2);
//						// TransML clientstm = generatememberstmc(loan2);
//						// TransML clientstm1 = generateclientint(loan2);
//
//						GeneralLedger generalLedger2 = generateGeneralLedgerDr(loan2, receivingAccount);
//
//						// generalLedgerService.save(generalLedger);
//						generalLedgerService.save(generalLedger2);
//						clientStatementService.save(clientStatement);
//						// clientStatementService.save(clientStatement2);
//						transglService.save(generalLedger4);
//						transglService.save(generalLedger3);
////						transmlService.save(clientstm);
////						transmlService.save(clientstm1);
//
//					}
//
//					return new SwizFeedback(true, "Loan disbursed successfully");
//				} else {
//					return new SwizFeedback(false, "System not able to generate Payment schedule. Please try again.");
//				}
//
//			} else {
//				return new SwizFeedback(false, "System not able to generate Payment schedule. Please try again.");
//			}
//
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//			return new SwizFeedback(false, ex.getMessage());
//
//		}
//	}
//
//	private float InterestGenerator(Loan loan3) {
//		float interest = 0;
//
//		try {
//
//			if (loan3.getInterestType() != null) {
//				float totalinterest = 0;
//
//				if (loan3.getInterestType().getInterestType().equals(InterestType.C.getInterestType())) {
//
//					totalinterest = (((loan3.getInterestRate() / 100) * loan3.getLoanAmount())
//							/ loan3.getRepaymentPeriod());
//
//					Calendar cal = Calendar.getInstance();
//					cal.setTime(loan3.getLoanDisbursementDate());
//					cal.add(Calendar.MONTH, loan3.getRepaymentPeriod());
//					Date dt = cal.getTime();
//
//					long diff = dt.getTime() - loan3.getLoanDisbursementDate().getTime();
//					float diffdays = (diff / (24 * 60 * 60 * 1000));
//
//					interest = totalinterest / diffdays;
//
//				} else if (loan3.getInterestType().getInterestType().equals(InterestType.R.getInterestType())) {
//
//					float installment = (loan3.getLoanAmount()) / loan3.getRepaymentPeriod();
//					for (int i = 0; i < loan3.getRepaymentPeriod(); i++) {
//
//						totalinterest += ((loan3.getLoanAmount() - (installment * i))
//								* (loan3.getInterestRate() / 100));
//
//						Calendar cal = Calendar.getInstance();
//						cal.setTime(loan3.getLoanDisbursementDate());
//						cal.add(Calendar.MONTH, loan3.getRepaymentPeriod());
//						Date dt = cal.getTime();
//
//						long diff = dt.getTime() - loan3.getLoanDisbursementDate().getTime();
//						float diffdays = (diff / (24 * 60 * 60 * 1000));
//
//						interest = totalinterest / diffdays;
//
//					}
//
//				}
//			}
//		}
//
//		catch (Exception ex) {
//			System.out.println(ex.getMessage());
//
//		}
//
//		return interest;
//	}
//
//	private ClientStatement generateInterests(Loan loan2) {
//		Account interest = accountService.findByName2(ProductType.LNP.getProductType());
//
//		ClientStatement clientStatement2 = new ClientStatement();
//		clientStatement2.setCustomerAccount(loan2.getCustomerAccount());
//		// clientStatement2.setPeriod(loan2.getPeriod());
//		clientStatement2.setBalanceCredit(0);
//		clientStatement2.setBalanceDebt(0);
//		clientStatement2.setCreatedBy(systemUserService.getLoggedInUser());
//		clientStatement2.setDateCreated(new Date());
//		clientStatement2.setDateUpdated(new Date());
//		clientStatement2.setYear(loan2.getYear());
//		clientStatement2.setPaymentDetails(loan2.getPaymentDetails());
//		clientStatement2.setPostReferenceNo(loan2.getLOANID());
//		clientStatement2.setProductType(interest.getProductType().getProductId());
//		clientStatement2.setRemarks(ProductType.Loan.getProductType());
//		clientStatement2.setStatus(Status.ACTIVE);
//		clientStatement2.setTransactionCredit(0);
//		clientStatement2.setTransactionDebt(InterestGenerator(loan2));
//		clientStatement2.setTransactionDescription(ProductType.LoanInterest.getProductType());
//		clientStatement2.setTtype(Ttype.D);
//		clientStatement2.setTransationDate(loan2.getLoanDisbursementDate());
//		clientStatement2.setUpdatedBy(systemUserService.getLoggedInUser());
//		clientStatement2.setThead(loan2.getThead());
//		return clientStatement2;
//	}
//
//	private TransML generatememberstmc(Loan payment) {
//		TransML gl = new TransML();
//		gl.setCreatedBy(systemUserService.getLoggedInUser());
//		gl.setUpdatedBy(systemUserService.getLoggedInUser());
//		gl.setDateCreated(new Date());
//		gl.setDateUpdated(new Date());
//		gl.setStatus(Status.ACTIVE);
//		gl.setDataSourceScreen(DataSourceScreen.Loan);
//		CustomerAccount account = customeraccountService.find(payment.getCustomerAccount().getSysid());
//
//		gl.setMEMBERID(account.getClientCode());
//		gl.setCustomeraccount(payment.getCustomerAccount());
//
//		gl.setFSAVE(Fsave.Y);
////	gl.setPRODUCTID(generateacodeproducttype(payment));
//		int loanid = Integer.parseInt(payment.getLOANID());
//
//		gl.setLOANID(loanid);
//		gl.setTDATE(payment.getLoanDisbursementDate());
//		gl.setTTYPE(Ttype.D);
//		gl.setAMOUNT(payment.getLoanAmount());
//		gl.setPAYDET(payment.getLoanPurpose() + payment.getPaymentDetails());
//		gl.setVNO(payment.getPaymentDetails());
//		// gl.setJNLID(0);
//		gl.setTHEADID(payment.getThead());
//		// gl.setLoan(payment.getLoanid());
//
//		SystemUser staff = systemUserService.find(payment.getUpdatedBy().getId());
//		String name = staff.getUsername();
//		gl.setSTAFFNAME(name);
//		gl.setCustomeraccount(payment.getCustomerAccount());
//
//		return gl;
//	}
//
//	private TransGL generateGLc(Loan payment) {
//		TransGL gl = new TransGL();
//		gl.setDateCreated(new Date());
//		gl.setDateUpdated(new Date());
//		gl.setDataSourceScreen(DataSourceScreen.Loan);
//		gl.setCreatedBy(systemUserService.getLoggedInUser());
//		gl.setUpdatedBy(systemUserService.getLoggedInUser());
//		gl.setStatus(Status.ACTIVE);
//		gl.setACODE(getrecivingaccount(payment));
//		gl.setFsave(Fsave.Y);
//		gl.setAMOUNT(payment.getLoanAmount());
//		gl.setAmtUsd(0);
//		gl.setRecAmt(0);
//		gl.setTDATE(payment.getLoanDisbursementDate());
//		gl.setTTYPE(Ttype.C);
//		gl.setPAYDET(payment.getPaymentDetails());
//		gl.setREMARKS(payment.getLoanPurpose());
//		gl.setICODE(payment.getPeriodcode());
//		gl.setYRCODE(payment.getYear());
//		gl.setPAYPERSON(payment.getLOANID());
//		gl.setMemberID(payment.getMEMBERID());
//		gl.setTheadID(payment.getThead());
//		gl.setLoanID(payment.getLOANID());
//
//		gl.setYrRec(0);
//		gl.setTrec(0);
//		gl.setAccount(payment.getPayingAccount());
//		SystemUser staff = systemUserService.find(payment.getUpdatedBy().getId());
//		String name = staff.getUsername();
//		gl.setStaffName(name);
//		if (payment.getCustomerAccount() != null) {
//
//			gl.setCustomeraccount(payment.getCustomerAccount());
//		}
//
//		return gl;
//	}
//
//	private TransGL generateGLc(Loan payment, Account account) {
//		TransGL gl = new TransGL();
//		gl.setDateCreated(new Date());
//		gl.setDateUpdated(new Date());
//		gl.setDataSourceScreen(DataSourceScreen.Loan);
//		gl.setCreatedBy(systemUserService.getLoggedInUser());
//		gl.setUpdatedBy(systemUserService.getLoggedInUser());
//		gl.setStatus(Status.ACTIVE);
//		gl.setACODE(getrecivingaccount(payment));
//		gl.setFsave(Fsave.Y);
//		gl.setAMOUNT(payment.getLoanAmount());
//		gl.setAmtUsd(0);
//		gl.setRecAmt(0);
//		gl.setTDATE(payment.getLoanDisbursementDate());
//		gl.setTTYPE(Ttype.D);
//		gl.setPAYDET(payment.getPaymentDetails());
//		gl.setREMARKS(payment.getLoanPurpose());
//		gl.setICODE(payment.getPeriodcode());
//		gl.setYRCODE(payment.getYear());
//		gl.setPAYPERSON(payment.getLOANID());
//		gl.setMemberID(payment.getMEMBERID());
//		gl.setTheadID(payment.getThead());
//		gl.setLoanID(payment.getLOANID());
//		gl.setYrRec(0);
//		gl.setTrec(0);
//		gl.setAccount(payment.getReceivingAccount());
//		SystemUser staff = systemUserService.find(payment.getUpdatedBy().getId());
//		String name = staff.getUsername();
//		gl.setStaffName(name);
//		if (payment.getCustomerAccount() != null) {
//
//			gl.setCustomeraccount(payment.getCustomerAccount());
//		}
//
//		return gl;
//	}
//
//	private GeneralLedger generateGeneralLedgerDr(Loan loan2, Account account) {
//		try {
//
//			if (account != null) {
//				GeneralLedger generalLedger = new GeneralLedger();
//				generalLedger.setAccount(account);
//				// generalLedger.setPeriod(loan2.getPeriod());
//				generalLedger.setBalanceCredit(0);
//				generalLedger.setBalanceDebt(0);
//				generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
//				generalLedger.setDateCreated(new Date());
//				generalLedger.setDateUpdated(new Date());
//				generalLedger.setYear(loan2.getYear());
//				generalLedger.setPaymentDetails(loan2.getPaymentDetails());
//				generalLedger.setPostReferenceNo(loan2.getLOANID());
//				generalLedger.setDataSourceScreen(DataSourceScreen.Loan);
//				generalLedger.setRemarks(ProductType.Loan.getProductType());
//				generalLedger.setStatus(Status.ACTIVE);
//				generalLedger.setTransactionCredit(0);
//				generalLedger.setTransactionDebt(loan2.getLoanAmount());
//				generalLedger.setTransactionDescription(ProductType.Loan.getProductType());
//				generalLedger.setTtype(Ttype.D);
//				generalLedger.setTransationDate(loan2.getLoanDisbursementDate());
//				generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
//				generalLedger.setThead(loan2.getThead());
//
//				return generalLedger;
//			}
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//		}
//		return null;
//	}
//
//	private GeneralLedger generateGeneralLedger(Loan loan2) {
//		GeneralLedger generalLedger = new GeneralLedger();
//		generalLedger.setAccount(loan2.getPayingAccount());
//		// generalLedger.setPeriod(loan2.getPeriod());
//		generalLedger.setBalanceCredit(0);
//		generalLedger.setBalanceDebt(0);
//		generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
//		generalLedger.setDateCreated(new Date());
//		generalLedger.setDateUpdated(new Date());
//		generalLedger.setYear(loan2.getYear());
//		generalLedger.setPaymentDetails(loan2.getPaymentDetails());
//		generalLedger.setPostReferenceNo(loan2.getLOANID());
//		generalLedger.setDataSourceScreen(DataSourceScreen.Loan);
//		generalLedger.setRemarks(ProductType.Loan.getProductType());
//		generalLedger.setStatus(Status.ACTIVE);
//		generalLedger.setTransactionCredit(loan2.getLoanAmount());
//		generalLedger.setTransactionDebt(0);
//		generalLedger.setTransactionDescription(ProductType.Loan.getProductType());
//		generalLedger.setTtype(Ttype.C);
//		generalLedger.setTransationDate(loan2.getLoanDisbursementDate());
//		generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
//
//		generalLedger.setThead(loan2.getThead());
//
//		return generalLedger;
//	}
//
//	private ClientStatement generateClientStatement(Loan loan2) {
//		Account interest = accountService.findByName2(ProductType.LNP.getProductType());
//
//		ClientStatement clientStatement = new ClientStatement();
//		clientStatement.setCustomerAccount(loan2.getCustomerAccount());
//		// clientStatement.setPeriod(loan2.getPeriod());
//		clientStatement.setBalanceCredit(0);
//		clientStatement.setBalanceDebt(0);
//		clientStatement.setCreatedBy(systemUserService.getLoggedInUser());
//		clientStatement.setDateCreated(new Date());
//		clientStatement.setDateUpdated(new Date());
//		clientStatement.setYear(loan2.getYear());
//		clientStatement.setPaymentDetails(loan2.getPaymentDetails());
//		clientStatement.setPostReferenceNo(loan2.getLOANID());
//		clientStatement.setProductType(interest.getProductType().getProductId());
//		clientStatement.setRemarks(ProductType.Loan.getProductType());
//		clientStatement.setStatus(Status.ACTIVE);
//		clientStatement.setTransactionCredit(0);
//		clientStatement.setTransactionDebt(loan2.getLoanAmount());
//		clientStatement.setTransactionDescription(ProductType.Loan.getProductType());
//		clientStatement.setTtype(Ttype.D);
//		clientStatement.setTransationDate(loan2.getLoanDisbursementDate());
//		clientStatement.setUpdatedBy(systemUserService.getLoggedInUser());
//		clientStatement.setThead(loan2.getThead());
//
//		return clientStatement;
//	}
//
//	private List<RepaymentSchedule> generateRepaymentSchedule(Loan loan) {
//		List<RepaymentSchedule> repaymentSchedules = new ArrayList<RepaymentSchedule>();
//		try {
//			int lot = 1;
//
//			float payableAmount = (loan.getLoanAmount() / loan.getRepaymentPeriod());
//
//			for (int i = 0; i < loan.getRepaymentPeriod(); i++) {
//
//				Calendar cal = Calendar.getInstance();
//				cal.setTime(loan.getLoanDisbursementDate());
//				cal.add(Calendar.MONTH, lot);
//				Date dt = cal.getTime();
//
//				float payableInterest = 0;
//
//				if (loan.getInterestType() != null) {
//
//					if (loan.getInterestType().getInterestType().equals(InterestType.C.getInterestType())) {
//
//						payableInterest = (((loan.getInterestRate() / 100) * loan.getLoanAmount())
//								/ loan.getRepaymentPeriod());
//
//					} else if (loan.getInterestType().getInterestType().equals(InterestType.R.getInterestType())) {
//
//						float installment = (loan.getLoanAmount()) / loan.getRepaymentPeriod();
//						payableInterest = ((loan.getLoanAmount() - (installment * i)) * (loan.getInterestRate() / 100));
//					}
//				}
//
//				float totalAmount = payableAmount + payableInterest;
//
//				RepaymentSchedule repaymentSchedule = new RepaymentSchedule();
//				repaymentSchedule.setCreatedBy(loan.getCreatedBy());
//				repaymentSchedule.setDateCreated(new Date());
//				repaymentSchedule.setDateUpdated(new Date());
//				repaymentSchedule.setInterestAmount(payableInterest);
//				repaymentSchedule.setLoan(loan);
//				repaymentSchedule.setPaymentDate(dt);
//				repaymentSchedule.setPaymentLotNumber(lot);
//				repaymentSchedule.setPrincipleAmount(payableAmount);
//				repaymentSchedule.setStatus(Status.ACTIVE);
//				repaymentSchedule.setTotalAmount(totalAmount);
//				repaymentSchedule.setUpdatedBy(loan.getUpdatedBy());
//
//				lot++;
//
//				repaymentSchedules.add(repaymentSchedule);
//			}
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//			return null;
//		}
//		return repaymentSchedules;
//	}
//
//	public SwizFeedback update(Loan loan) {
//		try {
//
//			loanDao.update(loan);
//			return new SwizFeedback(true, "Updated successfully");
//
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//			return new SwizFeedback(false, ex.getMessage());
//
//		}
//	}
//
//	public SwizFeedback delete(Loan loan) {
//		try {
//
//			loanDao.delete(loan);
//			return new SwizFeedback(true, "Deleted successfully");
//
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//			return new SwizFeedback(false, ex.getMessage());
//
//		}
//	}
//
//	@Transactional(readOnly = true)
//	public List<Loan> find() {
//		List<Loan> list = new ArrayList<Loan>();
//		try {
//
//			List<Loan> loans = loanDao.find();
//			for (Loan loan : loans) {
//				//loan.setRepaymentSchedules(null);
//				list.add(loan);
//			}
//
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//
//		}
//		return list;
//	}
//
//	public Loan find(String LOANID) {
//		try {
//
//			return loanDao.find(LOANID);
//
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//
//		}
//		return null;
//	}
//
//	@Transactional(readOnly = true)
//	public List<RepaymentSchedule> getRepaymentSchedule(Loan loan) {
//		List<RepaymentSchedule> list = new ArrayList<RepaymentSchedule>();
//		try {
//			Search search = new Search();
//			search.addFilterEqual("loan", loan);
//			List<RepaymentSchedule> schedules = repaymentScheduleDao.search(search);
//			for (RepaymentSchedule repaymentSchedule : schedules) {
//				if (repaymentSchedule.getLoan() != null) {
//					repaymentSchedule.setLoan(null);
//				}
//				list.add(repaymentSchedule);
//			}
//
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//		}
//		return list;
//	}
//
////	private String generateLoanNo(Loan loan) {
////		String loanNo = null;
////		try {
////
////			int count = getLoanCount() + 1;
////			long n = count;
////			int digits = 5;
////			char[] zeros = new char[digits];
////			Arrays.fill(zeros, '0');
////			DecimalFormat df = new DecimalFormat(String.valueOf(zeros));
////
////			String year = String.valueOf((loan.getYear()));
////
////			if (year != null) {
////				Intervaltb financialYear = intervalService.find(year);
////				if (financialYear != null) {
////					loanNo = financialYear.getFINTCODE() + "" + df.format(n);
////				}
////
////			} else {
////				loanNo = "_" + df.format(n);
////			}
////
////		} catch (Exception ex) {
////			System.out.println(ex.getMessage());
////		}
////		return loanNo;
////	}
//
////	public String loanNumber() {
////		String loannum = null;
////		try {
////			int num = getLoanCount() + 1;
////			String loan = "" + String.valueOf(num);
////			loannum = loan;
////		} catch (Exception ex) {
////
////		}
////		return loannum;
////	}
//
////	private int getLoanCount() {
////		int count = 0;
////		try {
////
////			List<Loan> loans = loanDao.findAll();
////
////			count = loans.size();
////
////		} catch (Exception ex) {
////
////		}
////
////		return count;
////	}
//
//	public Loan findBy(CustomerAccount customerAccount) {
//		try {
//
//			Search search = new Search();
//			search.addFilterEqual("customerAccount", customerAccount);
//			return loanDao.searchUnique(search);
//
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//
//		}
//		return null;
//	}
//
//	public String getrecivingaccount(Loan loan) {
//		String ans = null;
//		try {
//			Account account = accountService.find(loan.getReceivingAccount().getSysid());
//			ans = account.getACODE();
//
//		} catch (Exception ex) {
//
//		}
//		return ans;
//	}
//
//	public String getpayingaccount(Loan loan) {
//		String ans = null;
//		try {
//			Account account = accountService.find(loan.getPayingAccount().getSysid());
//			ans = account.getACODE();
//
//		} catch (Exception ex) {
//
//		}
//		return ans;
//	}
//
//	public String getclient(Loan loan) {
//		String ans = null;
//		try {
//			CustomerAccount account = customeraccountService.find(loan.getCustomerAccount().getSysid());
//			ans = account.getClientCode();
//
//		} catch (Exception ex) {
//
//		}
//		return ans;
//	}
//
//}
