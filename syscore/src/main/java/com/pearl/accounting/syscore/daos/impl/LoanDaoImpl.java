package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.LoanDao;
import com.pearl.accounting.sysmodel.Loan;

@Repository("LoanDao")
public class LoanDaoImpl extends SwiziBaseDaoImpl<Loan> implements LoanDao{

}
