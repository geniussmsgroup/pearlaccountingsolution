package com.pearl.accounting.syscore.security;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.SystemUserDao;
import com.pearl.accounting.syscore.daos.UserPermissionDao;
import com.pearl.accounting.syscore.daos.UserRoleDao;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.sysmodel.ConfigRole;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.SystemUser;
import com.pearl.accounting.sysmodel.SystemView;
import com.pearl.accounting.sysmodel.UserPermission;
import com.pearl.accounting.sysmodel.UserRole;

@Service("authenticationService")
public class AuthenticationServiceImpl implements AuthenticationService {

	@Autowired
	private SystemUserDao userDao;
	
	@Autowired
	private SystemUserService systemUserService;
	
	private UserRoleDao userRoleDao;
	@Autowired
	private UserPermissionDao userPermissionDao;

	public SystemUser authenticate(String username, String password, boolean attachUserToSecurityContext) {
		
		System.out.println("Authenticating user..");
		
		//setting initial user
				List<SystemUser> users=userDao.find();
				if(users==null) {
					UserRole userRole=new UserRole();
					userRole.setCode("ADM");
					userRole.setDateCreated(new Date());
					userRole.setDateUpdated(new Date());
					userRole.setDescription("Monintor & manage all system transactions");
					userRole.setEmployeeRole("Administrator");
					userRole.setStatus(Status.ACTIVE);
					
					UserRole role=userRoleDao.save(userRole);
					
					UserPermission permission=new UserPermission();
					permission.setDateCreated(new Date());
					permission.setDateUpdated(new Date());
					permission.setStatus(Status.ACTIVE);
					permission.setSystemView(SystemView.VIEW_SYSTEMS_ADMIN);
					permission.setUserRole(role);
					
					UserPermission userPermission=userPermissionDao.save(permission);
					
					SystemUser user = new SystemUser();
					user.setEnabled(true);
					user.setPassword("password");
					user.setUsername("admin");
					
					user.setDateCreated(new Date());
					user.setDateUpdated(new Date());
					user.setEmailAddress("kfredrick35@gmail.com");
					user.setFirstName("Kasoma");
					user.setLastName("Fredrick");
					user.setPhoneNumber("0700408387");
					//user.setProjectPartner(projectPartner);
					user.setStatus(Status.ACTIVE);
					user.setUserRole(role);
					user.setRole(ConfigRole.ROLE_USER);
					
					systemUserService.save(user);
				}else if(users.size()==0) {
					UserRole userRole=new UserRole();
					userRole.setCode("ADM");
					userRole.setDateCreated(new Date());
					userRole.setDateUpdated(new Date());
					userRole.setDescription("Monintor & manage all system transactions");
					userRole.setEmployeeRole("Administrator");
					userRole.setStatus(Status.ACTIVE);
					
					UserRole role=userRoleDao.save(userRole);
					
					UserPermission permission=new UserPermission();
					permission.setDateCreated(new Date());
					permission.setDateUpdated(new Date());
					permission.setStatus(Status.ACTIVE);
					permission.setSystemView(SystemView.VIEW_SYSTEMS_ADMIN);
					permission.setUserRole(role);
					
					UserPermission userPermission=userPermissionDao.save(permission);
					
					SystemUser user = new SystemUser();
					user.setEnabled(true);
					user.setPassword("password");
					user.setUsername("admin");
					
					user.setDateCreated(new Date());
					user.setDateUpdated(new Date());
					user.setEmailAddress("kfredrick35@gmail.com");
					user.setFirstName("Kasoma");
					user.setLastName("Fredrick");
					user.setPhoneNumber("0700408387");
					//user.setProjectPartner(projectPartner);
					user.setStatus(Status.ACTIVE);
					user.setUserRole(role);
					user.setRole(ConfigRole.ROLE_USER);
					systemUserService.save(user);
				}
				
		Search search = new Search();
		search.addFilterEqual("username", username);
		SystemUser user = userDao.searchUnique(search);

		return user;
	}

	public Boolean isValidUserPassword(SystemUser user, String password) {

		return true;
	}

	public boolean isEnabledUser(SystemUser systemUser) {
		// TODO Auto-generated method stub
		return false;
	}

}
