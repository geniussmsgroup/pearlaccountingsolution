package com.pearl.accounting.syscore.daos;

import com.pearl.accounting.sysmodel.UserPermission;

public interface UserPermissionDao extends SwiziBaseDao<UserPermission>{

}
