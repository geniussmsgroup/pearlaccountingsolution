package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.AtypeA;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface AccountService {

	public SwizFeedback save(Account account);

	public SwizFeedback update(Account account);

	public SwizFeedback delete(Account account);

	public List<Account> find();
    public Account findcbal(Float cbal  );
	public Account find(String Sysid);
	

	public Account findByName(String accountName);

	public Account findByName2(String ACODE);
	public String AtypeA(Account account);
	public String productid(Account account);
}
