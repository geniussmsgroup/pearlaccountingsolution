package com.pearl.accounting.syscore.services.impl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.AccountSDao;
import com.pearl.accounting.syscore.services.AccountSService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.sysmodel.Init;
import com.pearl.accounting.sysmodel.SwizFeedback;
@Service("AccountSService")
@Transactional
public class AccountSServiceImpl implements AccountSService {

	@Autowired
	private AccountSDao accountsDao;
	
	@Autowired
	private SystemUserService systemUserService;

	public SwizFeedback save(Init init) {
		try {
			accountsDao.save(init);
			return new SwizFeedback(true, "Saved successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}

	}

	public SwizFeedback update(Init init) {
		try {
			accountsDao.update(init);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}
	}

	public SwizFeedback delete(Init init) {
		try {
			accountsDao.delete(init);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}
	}

	public List<Init> find() {
		try {

			return accountsDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public Init find(String ACode) {
		try {

			return accountsDao.find(ACode);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}
	
	public Init findByName(String Aname) {
		try {
			
			Search search=new Search();
			search.addFilterILike("accountName", Aname); 
			return accountsDao.searchUnique(search);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}
	
	
	
	
	

}
	

