package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface LoanService {
	public SwizFeedback save(Loan loan);

	public SwizFeedback update(Loan loan);

	public SwizFeedback delete(Loan loan);

	public List<Loan> find();

	public Loan find(String LOANID);
	public Loan findBy(CustomerAccount customerAccount);
    public Loan findByMemberid(String  MEMBERID);
    public List<Loan> findByMemberids(String  MEMBERID);
}
