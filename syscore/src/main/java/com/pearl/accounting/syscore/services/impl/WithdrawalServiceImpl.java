package com.pearl.accounting.syscore.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.WithdrawalDao;
import com.pearl.accounting.syscore.services.AccountService;
import com.pearl.accounting.syscore.services.ClientStatementService;
import com.pearl.accounting.syscore.services.GeneralLedgerService;
import com.pearl.accounting.syscore.services.PeriodicDepositService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.syscore.services.WithdrawalService;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.ClientStatement;
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.DataSourceScreen;
import com.pearl.accounting.sysmodel.GeneralLedger;
import com.pearl.accounting.sysmodel.ProductType;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.Ttype;
import com.pearl.accounting.sysmodel.Withdrawal;

@Service("WithdrawalService")
@Transactional
public class WithdrawalServiceImpl implements WithdrawalService {

	@Autowired
	private WithdrawalDao withdrawalDao;

	@Autowired
	private SystemUserService systemUserService;

	@Autowired
	private GeneralLedgerService generalLedgerService;

	@Autowired
	private AccountService accountService;

	@Autowired
	private ClientStatementService clientStatementService;

	@Autowired
	private PeriodicDepositService periodicDepositService;

	public SwizFeedback save(Withdrawal withdrawal) {
		try {
			float balance = periodicDepositService.getTotalDeposits(withdrawal.getCustomerAccount()) - getTotalWithdraws(withdrawal.getCustomerAccount());
			if(withdrawal.getWithdrawalAmount() < balance) {
			withdrawal.setCreatedBy(systemUserService.getLoggedInUser());
			withdrawal.setUpdatedBy(systemUserService.getLoggedInUser());

			Account receivingAccount = accountService.findByName(ProductType.Savings.getProductType());

			if (receivingAccount != null) {
				withdrawal.setReceivingAccount(receivingAccount);

				Withdrawal withdrawal2 = withdrawalDao.save(withdrawal);

				if (withdrawal2 != null) {

					GeneralLedger generalLedger = generateGeneralLedger(withdrawal2);

					ClientStatement clientStatement = generateClientStatement(withdrawal2);

					GeneralLedger generalLedger2 = generateGeneralLedgerDr(withdrawal2, receivingAccount);

					generalLedgerService.save(generalLedger);
					generalLedgerService.save(generalLedger2);

					clientStatementService.save(clientStatement);

					return new SwizFeedback(true, "Transaction posted successfully");

				} else {

					return new SwizFeedback(false, "Transaction not posted successfully. Please try again.");
				}
			} else {
				return new SwizFeedback(false,
						"Receiving Account " + ProductType.Savings.getProductType() + " does not exist.");
			}
			}else {
				return new SwizFeedback(false, "Insufficient funds. You only have: " + balance);

			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}

	}

	// Debit the savings A/C (Receiving account)in the general ledger
	private GeneralLedger generateGeneralLedgerDr(Withdrawal withdrawal, Account loanAccount) {
		try {

			if (loanAccount != null) {
				GeneralLedger generalLedger = new GeneralLedger();
				generalLedger.setAccount(loanAccount);
				generalLedger.setPeriod(withdrawal.getPeriod());
				generalLedger.setBalanceCredit(0);
				generalLedger.setBalanceDebt(0);
				generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
				generalLedger.setDateCreated(new Date());
				generalLedger.setDateUpdated(new Date());
				generalLedger.setYear(withdrawal.getYear());
				generalLedger.setPaymentDetails(withdrawal.getPaymentDetails());
				generalLedger.setPostReferenceNo(withdrawal.getId());
				generalLedger.setDataSourceScreen(DataSourceScreen.Savings);
				generalLedger.setRemarks(ProductType.Savings.getProductType() + ":" + withdrawal.getRemarks());
				generalLedger.setStatus(Status.ACTIVE);
				generalLedger.setTransactionCredit(0);
				generalLedger.setTransactionDebt(withdrawal.getWithdrawalAmount() + withdrawal.getWithdrawalCharge());
				generalLedger.setTransactionDescription(ProductType.Savings.getProductType());
				generalLedger.setTtype(Ttype.D);
				generalLedger.setTransationDate(new Date());
				generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
				return generalLedger;
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	// Credit the paying account in the general ledger
	private GeneralLedger generateGeneralLedger(Withdrawal withdrawal) {

		GeneralLedger generalLedger = new GeneralLedger();

		generalLedger.setAccount(withdrawal.getPayingAccount());
		generalLedger.setPeriod(withdrawal.getPeriod());
		generalLedger.setBalanceCredit(0);
		generalLedger.setBalanceDebt(0);
		generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
		generalLedger.setDateCreated(new Date());
		generalLedger.setDateUpdated(new Date());
		generalLedger.setYear(withdrawal.getYear());
		generalLedger.setPaymentDetails(withdrawal.getPaymentDetails());
		generalLedger.setPostReferenceNo(withdrawal.getId());
		generalLedger.setDataSourceScreen(DataSourceScreen.Savings);
		generalLedger.setRemarks(ProductType.Savings.getProductType() + " :" + withdrawal.getRemarks());
		generalLedger.setStatus(Status.ACTIVE);
		generalLedger.setTransactionCredit(withdrawal.getWithdrawalAmount() + withdrawal.getWithdrawalCharge());
		generalLedger.setTransactionDebt(0);
		generalLedger.setTransactionDescription(ProductType.Savings.getProductType());
		generalLedger.setTtype(Ttype.C);
		generalLedger.setTransationDate(withdrawal.getWithdrawalDate());
		generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());

		return generalLedger;
	}

	private ClientStatement generateClientStatement(Withdrawal withdrawal) {

		ClientStatement clientStatement = new ClientStatement();

		System.out.println("PeriodicDeposit ClientId:==> " + withdrawal.getCustomerAccount().getSysid());

		clientStatement.setCustomerAccount(withdrawal.getCustomerAccount());

		clientStatement.setPeriod(withdrawal.getPeriod());
		clientStatement.setBalanceCredit(0);
		clientStatement.setBalanceDebt(0);
		clientStatement.setCreatedBy(systemUserService.getLoggedInUser());
		clientStatement.setDateCreated(new Date());
		clientStatement.setDateUpdated(new Date());
		clientStatement.setYear(withdrawal.getYear());
		clientStatement.setPaymentDetails(withdrawal.getPaymentDetails());
		clientStatement.setPostReferenceNo(withdrawal.getId());
		//clientStatement.setProductType(ProductType.Savings);
		clientStatement.setRemarks(ProductType.Savings.getProductType() + " :" + withdrawal.getRemarks());
		clientStatement.setStatus(Status.ACTIVE);
		clientStatement.setTransactionCredit(0);
		clientStatement.setTransactionDebt(withdrawal.getWithdrawalAmount() + withdrawal.getWithdrawalCharge());
		clientStatement.setTransactionDescription(ProductType.Savings.getProductType());
		clientStatement.setTtype(Ttype.D);
		clientStatement.setTransationDate(withdrawal.getWithdrawalDate());
		clientStatement.setUpdatedBy(systemUserService.getLoggedInUser());

		return clientStatement;

	}

	public SwizFeedback update(Withdrawal withdrawal) {
		try {
			withdrawalDao.update(withdrawal);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public SwizFeedback delete(Withdrawal withdrawal) {
		try {
			withdrawalDao.delete(withdrawal);
			return new SwizFeedback(true, "Deleted successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public List<Withdrawal> find() {
		try {
			return withdrawalDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public Withdrawal find(String id) {
		try {
			return withdrawalDao.find(id);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public float getTotalWithdraws(CustomerAccount cust) {
		float total = 0;
		try {

			Search s = new Search();
			s.addFilterEqual("customer account", cust);
			List<Withdrawal> list = withdrawalDao.search(s);
			for (Withdrawal wd : list) {
				total += wd.getWithdrawalAmount();
			}

		} catch (Exception ex) {

			System.out.println(ex.getMessage());

		}

		return total;
	}

}
