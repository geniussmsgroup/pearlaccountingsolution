package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.LoanRepaymentsDao;
import com.pearl.accounting.sysmodel.LoanRepayments;

@Repository("LoanRepaymentsDao")
public class LoanRepaymentsDaoImpl extends SwiziBaseDaoImpl<LoanRepayments> implements LoanRepaymentsDao {

}
