package com.pearl.accounting.syscore.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.AtypeADao;
import com.pearl.accounting.syscore.services.AtypeAservice;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.sysmodel.AtypeA;
import com.pearl.accounting.sysmodel.SwizFeedback;

@Service("AtypeAService")
@Transactional
public class AtypeAServiceImpl implements AtypeAservice {

	@Autowired
	private AtypeADao atypeaDao;
	
	@Autowired
	private SystemUserService systemUserService;

	public SwizFeedback save(AtypeA atypea) {
		try {
			atypeaDao.save(atypea);
			return new SwizFeedback(true, "Saved successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}

	}

	public SwizFeedback update(AtypeA atypea) {
		try {
			atypeaDao.update(atypea);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}
	}

	public SwizFeedback delete(AtypeA atypea) {
		try {
			atypeaDao.delete(atypea);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}
	}

	public List<AtypeA> find() {
		try {

			return atypeaDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public AtypeA find(String id) {
		try {

			return atypeaDao.find(id);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}
	
	public AtypeA findByName(String atypecd) {
		try {
			
			Search search=new Search();
			search.addFilterILike("atypeaName", atypecd); 
			return atypeaDao.searchUnique(search);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

}
