package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.IntervaltbDao;
import com.pearl.accounting.syscore.daos.MonthDataDao;
import com.pearl.accounting.sysmodel.Intervaltb;
import com.pearl.accounting.sysmodel.MonthData;

@Repository("MonthDataDao")
public class MonthDataDaoImpl extends SwiziBaseDaoImpl<MonthData> implements MonthDataDao {

}
