package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.UserPermission;
import com.pearl.accounting.sysmodel.UserRole;

public interface UserPermissionService {

	public SwizFeedback save(UserPermission userPermission);

	public SwizFeedback update(UserPermission userPermission);

	public SwizFeedback delete(UserPermission userPermission);

	public List<UserPermission> find();

	public UserPermission find(String id);
	
	public List<UserPermission> find(UserRole userRole);
}
