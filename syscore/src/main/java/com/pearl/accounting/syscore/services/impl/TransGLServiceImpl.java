package com.pearl.accounting.syscore.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.TransGLDao;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.syscore.services.TransGLService;
import com.pearl.accounting.sysmodel.TransGL;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.SwizFeedback;

@Service("TransGLService")
@Transactional
public class TransGLServiceImpl implements TransGLService{

	@Autowired
	private TransGLDao transglDao;

	@Autowired
	private SystemUserService systemUserService;

	public SwizFeedback save(TransGL transgl) {
		try {
			transgl.setCreatedBy(systemUserService.getLoggedInUser());
			transgl.setUpdatedBy(systemUserService.getLoggedInUser());
			transgl.setDateCreated(new Date());
			transgl.setDateUpdated(new Date());

			transglDao.save(transgl);
			return new SwizFeedback(true, "Transaction posted successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}

	}
	
	public SwizFeedback save(List<TransGL> transgl) {
		try {
			TransGL[] ledgers=new TransGL[transgl.size()];
			
			transglDao.save(transgl.toArray(ledgers));
			return new SwizFeedback(true, "Transaction posted successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}

	}

	public SwizFeedback update(TransGL transgl) {
		try {

			TransGL ledger = find(transgl.getId());
			if (ledger != null) {
				transgl.setCreatedBy(ledger.getCreatedBy());
				transgl.setUpdatedBy(systemUserService.getLoggedInUser());
				transgl.setDateCreated(ledger.getDateCreated());
				transgl.setDateUpdated(new Date());

				transglDao.update(transgl);
				return new SwizFeedback(true, "Transaction Updated successfully");
			} else {
				return new SwizFeedback(false, "Transaction does not exist in the general ledger");
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public SwizFeedback delete(TransGL transgl) {
		try {
			TransGL ledger = find(transgl.getId());
			if (ledger != null) {

				ledger.setUpdatedBy(systemUserService.getLoggedInUser());
				ledger.setDateUpdated(new Date());
				ledger.setStatus(Status.DELETED);
				transglDao.update(transgl);
				return new SwizFeedback(true, "Transaction Deleted successfully");
			} else {
				return new SwizFeedback(false, "Transaction does not exist in the general ledger");
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public List<TransGL> find() {
		try {
			Search search = new Search();
			search.addFilterEqual("status", Status.ACTIVE);
			return transglDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	public TransGL find(String id) {
		try {
			return transglDao.find(id);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

}
