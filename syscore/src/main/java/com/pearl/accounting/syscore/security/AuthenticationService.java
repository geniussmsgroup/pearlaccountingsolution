package com.pearl.accounting.syscore.security;

import com.pearl.accounting.sysmodel.SystemUser;

public interface AuthenticationService {

	SystemUser authenticate(String username, String password,
			boolean attachUserToSecurityContext);

	Boolean isValidUserPassword(SystemUser user, String loginPassword);

	boolean isEnabledUser(SystemUser systemUser);

}
