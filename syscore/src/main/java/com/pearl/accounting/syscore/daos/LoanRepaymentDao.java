package com.pearl.accounting.syscore.daos;

import com.pearl.accounting.sysmodel.LoanRepayment;

public interface LoanRepaymentDao extends SwiziBaseDao<LoanRepayment>{

}
