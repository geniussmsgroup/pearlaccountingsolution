package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.CashBookReceiptDetailDao;
import com.pearl.accounting.sysmodel.CashBookReceiptDetail;

@Repository("CashBookReceiptDetailDao")
public class CashBookReceiptDetailDaoImpl extends SwiziBaseDaoImpl<CashBookReceiptDetail>
		implements CashBookReceiptDetailDao {

}
