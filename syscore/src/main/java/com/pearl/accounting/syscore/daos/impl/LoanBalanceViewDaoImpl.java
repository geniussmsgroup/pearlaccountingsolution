package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.AccountDao;
import com.pearl.accounting.syscore.daos.LoanBalanceViewDao;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.LoanbalanceView;

@Repository("LoanBalanceViewDao")
public class LoanBalanceViewDaoImpl extends SwiziBaseDaoImpl<LoanbalanceView> implements LoanBalanceViewDao {

}
