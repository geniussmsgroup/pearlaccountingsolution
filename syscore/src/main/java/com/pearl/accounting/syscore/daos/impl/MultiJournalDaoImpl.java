package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.MultiJournalDao;
import com.pearl.accounting.sysmodel.MultiJournal;

@Repository("MultiJournalDao")
public class MultiJournalDaoImpl extends SwiziBaseDaoImpl<MultiJournal> implements MultiJournalDao {

}
