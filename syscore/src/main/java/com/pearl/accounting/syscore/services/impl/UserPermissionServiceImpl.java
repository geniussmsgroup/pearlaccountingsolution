package com.pearl.accounting.syscore.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.UserPermissionDao;
import com.pearl.accounting.syscore.services.UserPermissionService;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.UserPermission;
import com.pearl.accounting.sysmodel.UserRole;

@Service("UserPermissionService")
@Transactional
public class UserPermissionServiceImpl implements UserPermissionService {

	@Autowired
	private UserPermissionDao userPermissionDao;
 
	public SwizFeedback save(UserPermission userPermission) {
		try {
			userPermissionDao.save(userPermission);
			return new SwizFeedback(true, "Saved successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());

		}

	}

	public SwizFeedback update(UserPermission userPermission) {
		try {
			userPermissionDao.update(userPermission);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());

		}
	}

	public SwizFeedback delete(UserPermission userPermission) {
		try {
			userPermissionDao.delete(userPermission);
			return new SwizFeedback(true, "Deleted successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());

		}
	}

	public List<UserPermission> find() {
		try {
			return userPermissionDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public UserPermission find(String id) {

		try {
			return userPermissionDao.find(id);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}

		return null;
	}

	public List<UserPermission> find(UserRole userRole) {
		try {
			Search search = new Search();
			search.addFilterEqual("userRole", userRole); 
			return userPermissionDao.search(search);
			
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

}
