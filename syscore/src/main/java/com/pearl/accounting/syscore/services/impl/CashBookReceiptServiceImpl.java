package com.pearl.accounting.syscore.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.CashBookReceiptDao;
import com.pearl.accounting.syscore.daos.CashBookReceiptDetailDao;
import com.pearl.accounting.syscore.services.AccountService;
import com.pearl.accounting.syscore.services.CashBookReceiptService;
import com.pearl.accounting.syscore.services.GeneralLedgerService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.syscore.services.TheadpService;
import com.pearl.accounting.syscore.services.TransGLService;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.CashBookPayment;
import com.pearl.accounting.sysmodel.CashBookPaymentDetail;
import com.pearl.accounting.sysmodel.CashBookReceipt;
import com.pearl.accounting.sysmodel.CashBookReceiptDetail;
import com.pearl.accounting.sysmodel.DataSourceScreen;
import com.pearl.accounting.sysmodel.Fsave;
import com.pearl.accounting.sysmodel.GeneralLedger;
import com.pearl.accounting.sysmodel.ProductType;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.Theadp;
import com.pearl.accounting.sysmodel.TransGL;
import com.pearl.accounting.sysmodel.TransML;
import com.pearl.accounting.sysmodel.Ttype;

@Service("CashBookReceiptService")
@Transactional
public class CashBookReceiptServiceImpl implements CashBookReceiptService {

	@Autowired
	private CashBookReceiptDao cashBookReceiptDao;

	@Autowired
	private SystemUserService systemUserService;

	@Autowired
	private CashBookReceiptDetailDao cashBookReceiptDetailDao;
	@Autowired
	private TheadpService theadpService;
	@Autowired
	private GeneralLedgerService generalLedgerService;
	@Autowired
	private TransGLService transglService;
	@Autowired
	private AccountService accountService;

	public SwizFeedback save(CashBookReceipt cashBookReceipt) {
		try {
			cashBookReceipt.setCreatedBy(systemUserService.getLoggedInUser());
			cashBookReceipt.setUpdatedBy(systemUserService.getLoggedInUser());
			cashBookReceipt.setThead(theadpService.getthead());

			CashBookReceipt receipt = cashBookReceiptDao.save(cashBookReceipt);
			theadpService.save(savethead(receipt));
			if (receipt != null) {

				SwizFeedback feedback = generalLedgerService.save(generateGeneralLedger(receipt));
			   SwizFeedback feedback2 = transglService.save(generateGeneralleger(receipt));
				if (feedback.isResponse()&&feedback2.isResponse()) {
					return new SwizFeedback(true, "Payment successfully posted");
				} else {
					return feedback;
				}

			} else {
				return new SwizFeedback(false, "Error occured while posting transaction.");
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}

	}

	// debit and credit the receiveing account and Paying account in the general
	// ledger
	private List<GeneralLedger> generateGeneralLedger(CashBookReceipt payment) {

		List<GeneralLedger> list = new ArrayList<GeneralLedger>();

		List<CashBookReceiptDetail> details = findByPayment(payment);

		for (CashBookReceiptDetail detail : details) {

			GeneralLedger generalLedger = new GeneralLedger();

			generalLedger.setAccount(detail.getPayingAccount());
			generalLedger.setPeriod(payment.getPeriod());
			generalLedger.setBalanceCredit(0);
			generalLedger.setBalanceDebt(0);
			generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
			generalLedger.setDateCreated(new Date());
			generalLedger.setDateUpdated(new Date());
			generalLedger.setYear(payment.getYear());
			generalLedger.setPaymentDetails(detail.getRemarks());
			generalLedger.setPostReferenceNo(detail.getId());
			generalLedger.setDataSourceScreen(DataSourceScreen.CASH_BOOK_RECEIPTS);
			generalLedger
					.setRemarks(DataSourceScreen.CASH_BOOK_RECEIPTS.getDataSourceScreen() + " :" + detail.getRemarks());
			generalLedger.setStatus(Status.ACTIVE);
			generalLedger.setTransactionCredit(detail.getAmountPaid());
			generalLedger.setTransactionDebt(0);
			generalLedger.setTransactionDescription(DataSourceScreen.CASH_BOOK_RECEIPTS.getDataSourceScreen());
			generalLedger.setTtype(Ttype.C);
			generalLedger.setTransationDate(payment.getPaymentDate());
			generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
			generalLedger.setThead(payment.getThead());
			list.add(generalLedger);

		}

		// Debiting the receiving account
		GeneralLedger generalLedger = new GeneralLedger();

		generalLedger.setAccount(payment.getReceivingAccount());
		generalLedger.setPeriod(payment.getPeriod());
		generalLedger.setBalanceCredit(0);
		generalLedger.setBalanceDebt(0);
		generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
		generalLedger.setDateCreated(new Date());
		generalLedger.setDateUpdated(new Date());
		generalLedger.setYear(payment.getYear());
		generalLedger.setPaymentDetails(payment.getRemarks());
		generalLedger.setPostReferenceNo(payment.getId());
		generalLedger.setDataSourceScreen(DataSourceScreen.CASH_BOOK_RECEIPTS);
		generalLedger
				.setRemarks(DataSourceScreen.CASH_BOOK_RECEIPTS.getDataSourceScreen() + ":" + payment.getPaymentfrom());
		generalLedger.setStatus(Status.ACTIVE);
		generalLedger.setTransactionCredit(0);
		generalLedger.setTransactionDebt(payment.getAmountReceived());
		generalLedger.setTransactionDescription(DataSourceScreen.CASH_BOOK_RECEIPTS.getDataSourceScreen());
		generalLedger.setTtype(Ttype.D);
		generalLedger.setTransationDate(payment.getPaymentDate());
		generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
		generalLedger.setThead(payment.getThead());
		list.add(generalLedger);

		return list;
	}
	private List<TransGL> generateGeneralleger(CashBookReceipt payment) {

		List<TransGL> list = new ArrayList<TransGL>();

		List<CashBookReceiptDetail> details = findByPayment(payment);

		for (CashBookReceiptDetail detail : details) {
           Account account = accountService.find(detail.getPayingAccount().getSysid());
			TransGL gl = new TransGL();
			gl.setDateCreated(new Date());
			gl.setDateUpdated(new Date());
			gl.setDataSourceScreen(DataSourceScreen.CASH_BOOK_RECEIPTS);
			gl.setCreatedBy(systemUserService.getLoggedInUser());
			gl.setUpdatedBy(systemUserService.getLoggedInUser());
			gl.setStatus(Status.ACTIVE);
			 gl.setACODE(account.getACODE());
			gl.setFsave(Fsave.Y);
			gl.setAMOUNT(detail.getAmountPaid());
			gl.setAmtUsd(0);
			gl.setRecAmt(0);
			gl.setTDATE(payment.getPaymentDate());
			gl.setTTYPE(Ttype.C);
			gl.setPAYDET(detail.getRemarks());
			gl.setREMARKS(payment.getRemarks() + detail.getRemarks());
			gl.setICODE(payment.getPeriodcode());
			gl.setYRCODE(payment.getYear());
			gl.setPAYPERSON(payment.getRemarks());
			// gl.setMemberID(payment.getAcctNo());
			gl.setTheadID(payment.getThead());
			
			gl.setYrRec(0);
			gl.setTrec(0);
			gl.setAccount(detail.getPayingAccount());
			gl.setCreatedBy(systemUserService.getLoggedInUser());
			gl.setUpdatedBy(systemUserService.getLoggedInUser());

			list.add(gl);

		}

		TransGL gl = new TransGL();
		gl.setDateCreated(new Date());
		gl.setDateUpdated(new Date());
		gl.setDataSourceScreen(DataSourceScreen.CASH_BOOK_RECEIPTS);
		gl.setCreatedBy(systemUserService.getLoggedInUser());
		gl.setUpdatedBy(systemUserService.getLoggedInUser());
		gl.setStatus(Status.ACTIVE);
		 gl.setACODE(receving(payment));
		gl.setFsave(Fsave.Y);
		gl.setAMOUNT(payment.getAmountReceived());
		gl.setAmtUsd(0);
		gl.setRecAmt(0);
		gl.setTDATE(payment.getPaymentDate());
		gl.setTTYPE(Ttype.D);
		gl.setPAYDET(payment.getReceiptNumber());
		gl.setREMARKS(payment.getRemarks());
		gl.setICODE(payment.getPeriodcode());
		gl.setYRCODE(payment.getYear());
		gl.setPAYPERSON(payment.getRemarks());
		// gl.setMemberID(payment.getAcctNo());
		gl.setTheadID(payment.getThead());
		// gl.setJNLNO(0);
		gl.setYrRec(0);
		gl.setTrec(0);
		gl.setAccount(payment.getReceivingAccount());
		list.add(gl);

		return list;
	}
public Theadp savethead(CashBookReceipt payment) {
	Theadp account = new Theadp();
	account.setAccount(payment.getReceivingAccount());
	account.setPeriodcode(payment.getPeriodcode());
	account.setRemarks(payment.getRemarks());
	account.setVNO(payment.getRemarks() + " " + payment.getPaymentfrom());
	;
	///account.setRemarks(payment.);
	account.setTdates(payment.getPaymentDate());
	account.setDataSourceScreen(DataSourceScreen.CASH_BOOK_RECEIPTS);
	account.setAmount(payment.getAmountReceived());
	account.setDateCreated(new Date());
	account.setDateUpdated(new Date());
	
	
	
	return account;
}

	public SwizFeedback update(CashBookReceipt cashBookReceipt) {
		try {
			cashBookReceipt.setCreatedBy(systemUserService.getLoggedInUser());
			cashBookReceipt.setUpdatedBy(systemUserService.getLoggedInUser());

			cashBookReceiptDao.update(cashBookReceipt);
			return new SwizFeedback(true, "Payment updated successfully");

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public SwizFeedback delete(CashBookReceipt cashBookReceipt) {
		try {
			cashBookReceipt.setCreatedBy(systemUserService.getLoggedInUser());
			cashBookReceipt.setUpdatedBy(systemUserService.getLoggedInUser());

			cashBookReceiptDao.delete(cashBookReceipt);
			return new SwizFeedback(true, "Payment deleted successfully");

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	@Transactional(readOnly = true)
	public List<CashBookReceipt> find() {
		List<CashBookReceipt> list = new ArrayList<CashBookReceipt>();
		try {
			for (CashBookReceipt cashBookReceipt : cashBookReceiptDao.find()) {
				if (cashBookReceipt.getCashBookReceiptDetails() != null) {
					cashBookReceipt.setCashBookReceiptDetails(null);
				}

				list.add(cashBookReceipt);
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return list;
	}

	public CashBookReceipt find(String id) {
		try {
			return cashBookReceiptDao.find(id);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	@Transactional(readOnly = true)
	public List<CashBookReceiptDetail> find(CashBookReceipt cashBookReceipt) {

		List<CashBookReceiptDetail> list = new ArrayList<CashBookReceiptDetail>();

		try {

			Search search = new Search();
			search.addFilterEqual("cashBookReceipt", cashBookReceipt);
			List<CashBookReceiptDetail> bookReceiptDetails = cashBookReceiptDetailDao.search(search);

			for (CashBookReceiptDetail bookReceiptDetail : bookReceiptDetails) {

				if (bookReceiptDetail.getCashBookReceipt() != null) {
					bookReceiptDetail.setCashBookReceipt(null);
				}

				list.add(bookReceiptDetail);
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return list;
	}

	public List<CashBookReceiptDetail> findByPayment(CashBookReceipt cashBookReceipt) {

		List<CashBookReceiptDetail> list = new ArrayList<CashBookReceiptDetail>();

		try {

			Search search = new Search();
			search.addFilterEqual("cashBookReceipt", cashBookReceipt);
			list = cashBookReceiptDetailDao.search(search);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return list;
	}
	private String receving(CashBookReceipt cashBook) {
		String result = null;

		try {
			if (cashBook.getReceivingAccount() != null) {
				Account account = accountService.find(cashBook.getReceivingAccount().getSysid());
				result = account.getACODE();

			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return result;
	}

	private String getCashBookRecipt(CashBookReceipt cashBookDetail) {
		 List<String> list = new ArrayList<String>();
		String result = null;
		try {
			List<CashBookReceiptDetail> details = findByPayment(cashBookDetail);
			Account account = new Account();
			for (CashBookReceiptDetail detail : details) {
				CashBookReceiptDetail cashBookDetails = new CashBookReceiptDetail();
				account = accountService.find(cashBookDetails.getPayingAccount().getSysid());
				result = account.getACODE();

			}
			list.add(result);


		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return result;
	}
}
