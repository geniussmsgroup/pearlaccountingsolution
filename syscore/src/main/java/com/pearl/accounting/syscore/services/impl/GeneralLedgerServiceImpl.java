package com.pearl.accounting.syscore.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.GeneralLedgerDao;
import com.pearl.accounting.syscore.services.GeneralLedgerService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.sysmodel.DataSourceScreen;
import com.pearl.accounting.sysmodel.GeneralLedger;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.TransGL;
import com.pearl.accounting.sysmodel.TransML;

@Service("GeneralLedgerService")
@Transactional
public class GeneralLedgerServiceImpl implements GeneralLedgerService {

	@Autowired
	private GeneralLedgerDao generalLedgerDao;

	@Autowired
	private SystemUserService systemUserService;

	public SwizFeedback save(GeneralLedger generalLedger) {
		try {
			generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
			generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
			generalLedger.setDateCreated(new Date());
			generalLedger.setDateUpdated(new Date());

			generalLedgerDao.save(generalLedger);
			return new SwizFeedback(true, "Transaction posted successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}

	}
	
	public SwizFeedback save(List<GeneralLedger> generalLedgers) {
		try {
			GeneralLedger[] ledgers=new GeneralLedger[generalLedgers.size()];
			
			generalLedgerDao.save(generalLedgers.toArray(ledgers));
			return new SwizFeedback(true, "Transaction posted successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}

	}

	public SwizFeedback update(GeneralLedger generalLedger) {
		try {

			GeneralLedger ledger = find(generalLedger.getId());
			if (ledger != null) {
				generalLedger.setCreatedBy(ledger.getCreatedBy());
				generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
				generalLedger.setDateCreated(ledger.getDateCreated());
				generalLedger.setDateUpdated(new Date());

				generalLedgerDao.update(generalLedger);
				return new SwizFeedback(true, "Transaction Updated successfully");
			} else {
				return new SwizFeedback(false, "Transaction does not exist in the general ledger");
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public SwizFeedback delete(GeneralLedger generalLedger) {
		try {
			GeneralLedger ledger = find(generalLedger.getId());
			if (ledger != null) {

				ledger.setUpdatedBy(systemUserService.getLoggedInUser());
				ledger.setDateUpdated(new Date());
				ledger.setStatus(Status.DELETED);
				generalLedgerDao.update(generalLedger);
				return new SwizFeedback(true, "Transaction Deleted successfully");
			} else {
				return new SwizFeedback(false, "Transaction does not exist in the general ledger");
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public List<GeneralLedger> find() {
		try {
			Search search = new Search();
			search.addFilterEqual("status", Status.ACTIVE);
			return generalLedgerDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	public GeneralLedger find(String id) {
		try {
			return generalLedgerDao.find(id);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	

}
