package com.pearl.accounting.syscore.services;

import java.util.List;
 
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface CustomerAccountService {

	public SwizFeedback save(CustomerAccount account);

	public SwizFeedback update(CustomerAccount account);

	public SwizFeedback delete(CustomerAccount account);

	public List<CustomerAccount> find();

	public CustomerAccount find(String Sysid);
	public CustomerAccount findByName(String ClientCode);
}
