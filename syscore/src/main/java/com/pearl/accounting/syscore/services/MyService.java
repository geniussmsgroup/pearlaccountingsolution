package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.My;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface MyService {

	public SwizFeedback save(My my);

	public SwizFeedback update(My my);

	public SwizFeedback delete(My my);

	public List<My> find();

	public My find(String id);
	
	public My findByName(String accountName);

}