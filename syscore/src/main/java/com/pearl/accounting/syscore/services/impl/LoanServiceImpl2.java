package com.pearl.accounting.syscore.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.LoanDao;
import com.pearl.accounting.syscore.daos.RepaymentScheduleDao;
import com.pearl.accounting.syscore.services.AccountService;
import com.pearl.accounting.syscore.services.ClientStatementService;
import com.pearl.accounting.syscore.services.CustomerAccountService;
import com.pearl.accounting.syscore.services.FinancialYearService;
import com.pearl.accounting.syscore.services.GeneralLedgerService;
import com.pearl.accounting.syscore.services.IntervaltbService;
import com.pearl.accounting.syscore.services.LoanService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.syscore.services.TheadpService;
import com.pearl.accounting.syscore.services.TransGLService;
import com.pearl.accounting.syscore.services.TransMLService;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.AssessmentPeriod;
import com.pearl.accounting.sysmodel.ClientStatement;
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.DataSourceScreen;
import com.pearl.accounting.sysmodel.DoubleEntry;
import com.pearl.accounting.sysmodel.Fsave;
import com.pearl.accounting.sysmodel.GeneralLedger;
import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysmodel.ProductType;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.SystemUser;
import com.pearl.accounting.sysmodel.Theadp;
import com.pearl.accounting.sysmodel.TransGL;
import com.pearl.accounting.sysmodel.TransML;
import com.pearl.accounting.sysmodel.Ttype;

@Service("LoanService")
@Transactional
public class LoanServiceImpl2 implements LoanService {
	@Autowired
	private LoanDao loanDao;

	@Autowired
	private RepaymentScheduleDao repaymentScheduleDao;

	@Autowired
	private SystemUserService systemUserService;

	@Autowired
	private FinancialYearService financialYearService;

	@Autowired
	private GeneralLedgerService generalLedgerService;

	@Autowired
	private ClientStatementService clientStatementService;

	@Autowired
	private AccountService accountService;
	@Autowired
	private CustomerAccountService customeraccountService;

	@Autowired
	private TheadpService theadpService;
	@Autowired
	private TransGLService transglService;
	@Autowired
	private TransMLService transmlService;
	@Autowired
	private IntervaltbService intervalService;
	
	public SwizFeedback save(Loan loan) {
		try {
			// loan.setCreatedBy(createdBy);
			loan.setCreatedBy(systemUserService.getLoggedInUser());
			loan.setUpdatedBy(systemUserService.getLoggedInUser());
			loan.setMEMBERID((getclient(loan)));
		  loan.setThead(theadpService.getthead());
			Account account = accountService.find("7");
			loan.setReceivingAccount(account);
			loan.setTDATE2(loan.getLoanDisbursementDate());
			
			Loan loan1 = loanDao.save(loan);
			if (loan1 != null) {
				TransGL gld = generateGLD(loan1);
				TransGL glc = generateGLc(loan1);
				GeneralLedger generalLedgerd = generateGeneralLedgerD(loan1);
				GeneralLedger generalLedgerc = generateGeneralLedgerC(loan1);
				ClientStatement clientStatement = generateLoan(loan1);
				TransML clientstm = generatememberstmc(loan1);
                theadpService.save(savethead(loan1));
				transmlService.save(clientstm);
				generalLedgerService.save(generalLedgerd);
				generalLedgerService.save(generalLedgerc);
				clientStatementService.save(clientStatement);

				transglService.save(gld);
				transglService.save(glc);

			}

			return new SwizFeedback(true, "Success successfully");
		} catch (Exception message) {
		}
		return null;
	}

	private TransML generatememberstmc(Loan loan1) {
		TransML gl = new TransML();
		gl.setCreatedBy(systemUserService.getLoggedInUser());
		gl.setUpdatedBy(systemUserService.getLoggedInUser());
		gl.setDateCreated(new Date());
		gl.setDateUpdated(new Date());
		gl.setStatus(Status.ACTIVE);
		gl.setDataSourceScreen(DataSourceScreen.Loan);
		CustomerAccount account = customeraccountService.find(loan1.getCustomerAccount().getSysid());
		gl.setMEMBERID(account.getClientCode());
		gl.setCustomeraccount(loan1.getCustomerAccount());
		gl.setFSAVE(Fsave.Y);
//	gl.setPRODUCTID(generateacodeproducttype(payment));
		int loanid = Integer.parseInt(loan1.getLOANID());
		gl.setLOANID(loanid);
		gl.setTDATE(loan1.getLoanDisbursementDate());
		gl.setTTYPE(Ttype.D);
		gl.setAMOUNT(loan1.getLoanAmount());
		gl.setPAYDET(loan1.getLoanPurpose() + loan1.getPaymentDetails());
		gl.setVNO(loan1.getPaymentDetails());
		// gl.setJNLID(0);
		gl.setTHEADID(loan1.getThead());
		// gl.setLoan(payment.getLoanid());
		gl.setPRODUCTID("LNP");
		SystemUser staff = systemUserService.find(loan1.getUpdatedBy().getId());
		String name = staff.getUsername();
		gl.setSTAFFNAME(name);
		gl.setCustomeraccount(loan1.getCustomerAccount());

		return gl;
	}

	private GeneralLedger generateGeneralLedgerC(Loan loan2) {
		try {

			GeneralLedger generalLedger = new GeneralLedger();
			generalLedger.setAccount(loan2.getPayingAccount());
			// generalLedger.setPeriod(loan2.getPeriod());
			generalLedger.setBalanceCredit(0);
			generalLedger.setBalanceDebt(0);
			generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
			generalLedger.setDateCreated(new Date());
			generalLedger.setDateUpdated(new Date());
			generalLedger.setYear(loan2.getYear());
			generalLedger.setPaymentDetails(loan2.getPaymentDetails());
			generalLedger.setPostReferenceNo(loan2.getLOANID());
			generalLedger.setDataSourceScreen(DataSourceScreen.Loan);
			generalLedger.setRemarks(loan2.getLoanPurpose());
			generalLedger.setStatus(Status.ACTIVE);
			generalLedger.setTransactionCredit(loan2.getLoanAmount());
			generalLedger.setTransactionDebt(0);
			generalLedger.setTransactionDescription(ProductType.Loan.getProductType());
			generalLedger.setTtype(Ttype.C);
			generalLedger.setTransationDate(loan2.getLoanDisbursementDate());
			generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
			generalLedger.setThead(loan2.getThead());

			return generalLedger;

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	private GeneralLedger generateGeneralLedgerD(Loan loan2) {
		try {

			GeneralLedger generalLedger = new GeneralLedger();
			generalLedger.setAccount(loan2.getReceivingAccount());
			// generalLedger.setPeriod(loan2.getPeriod());
			generalLedger.setBalanceCredit(0);
			generalLedger.setBalanceDebt(0);
			generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
			generalLedger.setDateCreated(new Date());
			generalLedger.setDateUpdated(new Date());
			generalLedger.setYear(loan2.getYear());
			generalLedger.setPaymentDetails(loan2.getPaymentDetails());
			generalLedger.setPostReferenceNo(loan2.getLOANID());
			generalLedger.setDataSourceScreen(DataSourceScreen.Loan);
			generalLedger.setRemarks(loan2.getLoanPurpose());
			generalLedger.setStatus(Status.ACTIVE);
			generalLedger.setTransactionCredit(0);
			generalLedger.setTransactionDebt(loan2.getLoanAmount());
			generalLedger.setTransactionDescription(ProductType.Loan.getProductType());
			generalLedger.setTtype(Ttype.D);
			generalLedger.setTransationDate(loan2.getLoanDisbursementDate());
			generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
			generalLedger.setThead(loan2.getThead());

			return generalLedger;

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	private TransGL generateGLD(Loan loan) {
		TransGL gl = new TransGL();
		gl.setACODE(getReceiving(loan));

		gl.setDateCreated(new Date());
		gl.setDateUpdated(new Date());
		gl.setDataSourceScreen(DataSourceScreen.Loan);
		gl.setAccount(loan.getReceivingAccount());
		gl.setAMOUNT(loan.getLoanAmount());
		gl.setTheadID(loan.getThead());
		gl.setTDATE(loan.getLoanDisbursementDate());
		gl.setTTYPE(Ttype.D);
		gl.setPAYDET(loan.getPaymentDetails());
		gl.setREMARKS(loan.getLoanPurpose());
		gl.setFsave(Fsave.Y);
		gl.setCustomeraccount(loan.getCustomerAccount());
		gl.setRDATE(new Date());
		gl.setYRCODE(loan.getYear());
		gl.setICODE(loan.getPeriodcode());
		gl.setYRCODE(0);
		gl.setTrecN("0");
		gl.setTrecN("0");
		gl.setMemberID(getclient(loan));

		gl.setICODE(loan.getPeriodcode());
		return gl;
	}

	private TransGL generateGLc(Loan loan) {
		TransGL gl = new TransGL();
		gl.setACODE(getPaying(loan));

		gl.setDateCreated(new Date());
		gl.setDateUpdated(new Date());
		gl.setDataSourceScreen(DataSourceScreen.Loan);
		gl.setAccount(loan.getReceivingAccount());
		gl.setAMOUNT(loan.getLoanAmount());
		gl.setTheadID(loan.getThead());
		gl.setTDATE(loan.getLoanDisbursementDate());
		gl.setTTYPE(Ttype.C);
		gl.setPAYDET(loan.getPaymentDetails());
		gl.setREMARKS(loan.getLoanPurpose());
		gl.setFsave(Fsave.Y);
		gl.setCustomeraccount(loan.getCustomerAccount());
		gl.setRDATE(new Date());
		gl.setYRCODE(loan.getYear());
		gl.setICODE(loan.getPeriodcode());
		gl.setYRCODE(0);
		gl.setTrecN("0");
		gl.setTrecN("0");
		gl.setMemberID(getclient(loan));

		gl.setICODE(loan.getPeriodcode());
		return gl;
	}

	private ClientStatement generateLoan(Loan loan2) {
		// Account interest =
		// accountService.findByName2(ProductType.LNP.getProductType());
		ClientStatement clientStatement2 = new ClientStatement();
		clientStatement2.setCustomerAccount(loan2.getCustomerAccount());
		clientStatement2.setBalanceCredit(0);
		clientStatement2.setBalanceDebt(0);
		clientStatement2.setCreatedBy(systemUserService.getLoggedInUser());
		clientStatement2.setDateCreated(new Date());
		clientStatement2.setDateUpdated(new Date());
		clientStatement2.setYear(loan2.getYear());
		clientStatement2.setPaymentDetails(loan2.getPaymentDetails());
		clientStatement2.setPostReferenceNo(loan2.getLOANID());
		// clientStatement2.setProductType(loan2.getProductType().getProductId());
		clientStatement2.setRemarks(ProductType.Loan.getProductType() + " " + loan2.getLoanPurpose());
		clientStatement2.setStatus(Status.ACTIVE);
		clientStatement2.setTransactionCredit(0);
		clientStatement2.setTransactionDebt(loan2.getLoanAmount());
		clientStatement2.setTransactionDescription(ProductType.Loan.getProductType());
		clientStatement2.setTtype(Ttype.D);
		clientStatement2.setTransationDate(loan2.getLoanDisbursementDate());
		clientStatement2.setUpdatedBy(systemUserService.getLoggedInUser());
		clientStatement2.setThead(loan2.getThead());
		// clientStatement2.setRemarks(loan2.getLoanPurpose());
		return clientStatement2;
	}

	public Theadp savethead(Loan loan) {
		
		Theadp thead = new Theadp();

		thead.setAmount(loan.getLoanAmount());
		thead.setCustomerAccount(loan.getCustomerAccount());
		thead.setTdates(loan.getLoanDisbursementDate());
		thead.setAccount(loan.getPayingAccount());
		thead.setVNO(loan.getLoanPurpose());
		thead.setRemarks(loan.getPaymentDetails());
		thead.setDataSourceScreen(DataSourceScreen.Loan);
		thead.setDateCreated(new Date());
		thead.setDateUpdated(new Date());
		thead.setPeriodcode(loan.getPeriodcode());
		
		
		return thead;
	}
	
	
	
	
	public SwizFeedback update(Loan loan) {
		try {
			loanDao.update(loan);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}
	}

	public SwizFeedback delete(Loan loan) {
		try {
			loanDao.delete(loan);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}
	}

	public List<Loan> find() {
		try {

			return loanDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public Loan find(String LOANID) {
		try {

			return loanDao.find(LOANID);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public Loan findBy(CustomerAccount customerAccount) {
		try {

			Search search = new Search();
			search.addFilterEqual("customerAccount", customerAccount);
//			List<Loan> assessmentPeriods = loanDao.search(search);
//			if (assessmentPeriods != null) {
//				if (!assessmentPeriods.isEmpty()) {
//					return assessmentPeriods.get(assessmentPeriods.size());
//				}
//			}
			return loanDao.searchUnique(search);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	
	
//
	public List<Loan> findByMemberids(String  MEMBERID) {
		try {

			Search search = new Search();
			search.addFilterEqual("MEMBERID", MEMBERID);
			List<Loan> assessmentPeriods = loanDao.searchUnique(search);
			
			if (assessmentPeriods != null) {
				if (!assessmentPeriods.isEmpty()) {
					return   assessmentPeriods;
					
				}
			}
			return loanDao.searchUnique(search);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public Loan findByMemberid(String  MEMBERID) {
		try {

			Search search = new Search();
			search.addFilterEqual("MEMBERID", MEMBERID);
			List<Loan> assessmentPeriods = loanDao.searchUnique(search);
			
//			if (assessmentPeriods != null) {
//				if (!assessmentPeriods.isEmpty()) {
//					return   assessmentPeriods;
//					
//				}
//			}
			return loanDao.searchUnique(search);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}


	
	public String getclient(Loan loan) {
		String clientcode = null;
		try {
			CustomerAccount customer = customeraccountService.find(loan.getCustomerAccount().getSysid());
			clientcode = customer.getClientCode();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return clientcode;
	}

	public String getReceiving(Loan loan) {
		String loanaccount = null;
		try {
			Account account = accountService.find(loan.getReceivingAccount().getSysid());
			loanaccount = account.getACODE();

		} catch (Exception e) {
			// TODO: handle exception
		}
		return loanaccount;
	}

	public String getPaying(Loan loan) {
		String loanaccount = null;
		try {
			Account account = accountService.find(loan.getPayingAccount().getSysid());
			loanaccount = account.getACODE();

		} catch (Exception e) {
			// TODO: handle exception
		}
		return loanaccount;
	}

public String getloanid(Loan loan) {
	String loanw = null;
	int loanid = 0;
	try {
		List<Loan> loans = loanDao.findAll();
		loanid = loans.size();
		loanw = String.valueOf(loanid); 
	
	}catch (Exception ex) {
		// TODO: handle exception
	}
return loanw;
}

}
