package com.pearl.accounting.syscore.services.impl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
//import com.pearl.theadping.sysui.client.views.windows.DoubleEntryWindow;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.AccountDao;
import com.pearl.accounting.syscore.daos.DoubleEntryDao;
import com.pearl.accounting.syscore.daos.TheadpDao;
import com.pearl.accounting.syscore.services.AccountService;
import com.pearl.accounting.syscore.services.ClientStatementService;
import com.pearl.accounting.syscore.services.CustomerAccountService;
import com.pearl.accounting.syscore.services.DoubleEntryService;
import com.pearl.accounting.syscore.services.GeneralLedgerService;
import com.pearl.accounting.syscore.services.LoanService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.syscore.services.TheadService;
import com.pearl.accounting.syscore.services.TheadpService;
import com.pearl.accounting.syscore.services.TransGLService;
import com.pearl.accounting.syscore.services.TransMLService;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.CashBookPayment;
import com.pearl.accounting.sysmodel.CashBookPaymentDetail;
import com.pearl.accounting.sysmodel.ClientStatement;
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.DataSourceScreen;
import com.pearl.accounting.sysmodel.FinancialYear;
import com.pearl.accounting.sysmodel.Fsave;
import com.pearl.accounting.sysmodel.GeneralLedger;
import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysmodel.LoanRepayment2;
import com.pearl.accounting.sysmodel.ProductType;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.SystemUser;
import com.pearl.accounting.sysmodel.Thead;
import com.pearl.accounting.sysmodel.DoubleEntry;
import com.pearl.accounting.sysmodel.TransGL;
import com.pearl.accounting.sysmodel.TransML;
import com.pearl.accounting.sysmodel.Ttype;
import com.pearl.accounting.sysmodel.Withdrawal;

@Service("DoubleEntryService")
@Transactional
public class DoubleEntryServiceImpl implements DoubleEntryService {
	@Autowired
	private DoubleEntryDao doubleentryDao;

	@Autowired
	private LoanService loanService;
	@Autowired
	private TransGLService transglService;
	@Autowired
	private TransMLService transmlService;
	@Autowired
	private GeneralLedgerService generalLedgerService;
	@Autowired
	private SystemUserService systemUserService;
	@Autowired
	private ClientStatementService clientStatementService;
	@Autowired
	private AccountService accountService;
	@Autowired
	private CustomerAccountService customeraccountService;
	@Autowired
	private TheadpService theadpService;

	public SwizFeedback save(DoubleEntry theadp) {
		try {

			theadp.setCreatedBy(systemUserService.getLoggedInUser());
			theadp.setUpdatedBy(systemUserService.getLoggedInUser());
			theadp.setHDNO(theadpService.getthead());
			DoubleEntry doubleentry = doubleentryDao.save(theadp);

			if (doubleentry != null) {

				TransGL transdebit = generateGeneralLedgerdr(doubleentry);
				TransGL transcredit = generateGeneralLedgercr(doubleentry);
				GeneralLedger debit = generateGeneralLedgerd(doubleentry);
				GeneralLedger credit = generateGeneralLedgerc(doubleentry);

				TransML tdebit = generatememberstmd(doubleentry);
				TransML tcredit = generatememberstmc(doubleentry);
				ClientStatement cld = generateClientStatement(doubleentry);
				ClientStatement cldc = generateClientStatementc(doubleentry);

				generalLedgerService.save(debit);
				generalLedgerService.save(credit);
				transglService.save(transdebit);
				transglService.save(transcredit);
				if(doubleentry.getDebit().getAnalReq().equals("Y")) {
				transmlService.save(tdebit);
				clientStatementService.save(cld);
			}if(doubleentry.getDebit().getAnalReq().equals("Y")) {

				transmlService.save(tcredit);
				clientStatementService.save(cldc);
			}
				return new SwizFeedback(true, "Transaction posted successfully");
			} else {
				return new SwizFeedback(false, "Transaction not posted successfully");

			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}

	}

	private TransGL generateGeneralLedgercr(DoubleEntry payment) {
		TransGL gl = new TransGL();
		Account account = accountService.find(payment.getCredit().getSysid());

		gl.setACODE(account.getACODE());

		gl.setDateCreated(new Date());
		gl.setDateUpdated(new Date());
		gl.setDataSourceScreen(DataSourceScreen.DoubleEntry);
		gl.setAccount(payment.getCredit());
		gl.setAMOUNT(payment.getAmount());
		gl.setTheadID(payment.getHDNO());
		gl.setTDATE(payment.getTdates());
		gl.setTTYPE(Ttype.C);
		gl.setPAYDET(payment.getVNO());
		gl.setREMARKS(payment.getRemarks());
		gl.setFsave(Fsave.Y);
		gl.setCustomeraccount(payment.getCustomerAccount());
		gl.setRDATE(new Date());
		gl.setYRCODE(payment.getYear());
		gl.setICODE(payment.getPeriodcode());
		gl.setYRCODE(0);
		gl.setTrecN("0");
		gl.setTrecN("0");
		if (payment.getCustomerAccount() != null) {
			gl.setMemberID(generateacctNo(payment));

		} else {
			gl.setMemberID(null);

		}

		gl.setICODE(payment.getPeriodcode());
		return gl;
	}

	private TransGL generateGeneralLedgerdr(DoubleEntry recieve) {
		TransGL gl = new TransGL();
		Account account = accountService.find(recieve.getDebit().getSysid());

		gl.setACODE(account.getACODE());
		gl.setDateCreated(new Date());
		gl.setDateUpdated(new Date());
		gl.setDataSourceScreen(DataSourceScreen.DoubleEntry);
		gl.setAccount(recieve.getDebit());
		gl.setAMOUNT(recieve.getAmount());
		gl.setTheadID(recieve.getHDNO());
		gl.setTDATE(recieve.getTdates());
		gl.setTTYPE(Ttype.D);
		gl.setPAYDET(recieve.getVNO());
		gl.setREMARKS(recieve.getRemarks());
		gl.setFsave(Fsave.Y);
		gl.setCustomeraccount(recieve.getCustomerAccount());
		gl.setRDATE(new Date());
		gl.setYRCODE(recieve.getYear());
		gl.setICODE(recieve.getPeriodcode());
		gl.setYRCODE(0);
		gl.setTrecN("0");
		gl.setTrecN("0");
		gl.setAmtUsd(0);
		if (recieve.getCustomerAccount() != null) {
			gl.setMemberID(generateacctNo(recieve));
		} else {
			gl.setMemberID(null);

		}
		gl.setICODE(recieve.getPeriodcode());
		gl.setTheadID(recieve.getHDNO());
		return gl;
	}

	private GeneralLedger generateGeneralLedgerc(DoubleEntry payment) {
		GeneralLedger gl = new GeneralLedger();
		gl.setAccount(payment.getCredit());
		gl.setTransactionCredit(payment.getAmount());
		gl.setRemarks(payment.getRemarks());
		gl.setThead(payment.getHDNO());
		gl.setTtype(Ttype.C);
		gl.setTransationDate(payment.getTdates());
		gl.setCreatedBy(systemUserService.getLoggedInUser());
		gl.setUpdatedBy(systemUserService.getLoggedInUser());
		gl.setThead(payment.getHDNO());
		gl.setPostReferenceNo(payment.getId());
		gl.setTransactionCredit(payment.getAmount());
		gl.setTransactionDebt(0);
		gl.setDateCreated(new Date());
		gl.setDateUpdated(new Date());
		gl.setDataSourceScreen(DataSourceScreen.DoubleEntry);
		return gl;
	}

	private GeneralLedger generateGeneralLedgerd(DoubleEntry recieve) {
		GeneralLedger gl = new GeneralLedger();
		gl.setAccount(recieve.getDebit());
		gl.setTransactionDebt(recieve.getAmount());
		gl.setRemarks(recieve.getRemarks());
		gl.setPostReferenceNo(recieve.getId());
		gl.setTtype(Ttype.D);
		gl.setTransationDate(recieve.getTdates());
		gl.setCreatedBy(systemUserService.getLoggedInUser());
		gl.setUpdatedBy(systemUserService.getLoggedInUser());
		gl.setThead(recieve.getHDNO());
		gl.setTransationDate(recieve.getTdates());
		gl.setDateCreated(new Date());
		gl.setDateUpdated(new Date());
		gl.setDataSourceScreen(DataSourceScreen.DoubleEntry);
		gl.setTransactionDebt(recieve.getAmount());
		gl.setTransactionCredit(0);

		return gl;
	}

	private ClientStatement generateClientStatement(DoubleEntry recieve) {
		ClientStatement gl = new ClientStatement();
		if ((recieve.getCustomerAccount() != null) && (recieve.getDebit() != null)) {

			Account account = accountService.find(recieve.getDebit().getSysid());
			if (account.getAnalReq().equalsIgnoreCase("Y")) {
				gl.setCustomerAccount(recieve.getCustomerAccount());
				gl.setTransactionDebt(recieve.getAmount());
				gl.setRemarks(recieve.getRemarks());
				gl.setPostReferenceNo(recieve.getId());
				gl.setTtype(Ttype.D);
				gl.setTransationDate(recieve.getTdates());
				gl.setCreatedBy(systemUserService.getLoggedInUser());
				gl.setUpdatedBy(systemUserService.getLoggedInUser());
				gl.setThead(recieve.getHDNO());
				gl.setTransationDate(recieve.getTdates());
				gl.setDateCreated(new Date());
				gl.setDateUpdated(new Date());
				gl.setTransactionDebt(recieve.getAmount());
				gl.setTransactionCredit(0);
			} else {
				System.out.println("debit account is not track");// gl.setDataSourceScreen(DataSourceScreen.DoubleEntry);
			}
		} else {
			System.out.println("debitand customer account are empty");// gl.setDataSourceScreen(DataSourceScreen.DoubleEntry);
		}

		return gl;
	}

	private ClientStatement generateClientStatementc(DoubleEntry payment) {
		ClientStatement gl = new ClientStatement();
		// if (payment.getCustomerAccount() != null) {
		if ((payment.getCustomerAccount() != null) && (payment.getCredit() != null)) {
			Account account = accountService.find(payment.getCredit().getSysid());
			if (account.getAnalReq().equalsIgnoreCase("Y")) {

				gl.setCustomerAccount(payment.getCustomerAccount());
				gl.setTransactionCredit(payment.getAmount());
				gl.setRemarks(payment.getRemarks());
				gl.setTransationDate(payment.getTdates());
				gl.setTtype(Ttype.C);
				gl.setTransationDate(payment.getTdates());
				gl.setCreatedBy(systemUserService.getLoggedInUser());
				gl.setUpdatedBy(systemUserService.getLoggedInUser());
				gl.setThead(payment.getHDNO());
				gl.setPostReferenceNo(payment.getId());
				gl.setTransactionCredit(payment.getAmount());
				gl.setTransactionDebt(0);
				gl.setDateCreated(new Date());
				gl.setDateUpdated(new Date());
			} else {
				System.out.println(" credit account is not track");// gl.setDataSourceScreen(DataSourceScreen.DoubleEntry);
			}
		} else {
			System.out.println("debitand customer account are empty");// gl.setDataSourceScreen(DataSourceScreen.DoubleEntry);

		}
		return gl;
	}

	private TransML generatememberstmc(DoubleEntry payment) {
		TransML gl = new TransML();
		if ((payment.getCustomerAccount() != null) && (payment.getCredit() != null)) {
			Account account = accountService.find(payment.getCredit().getSysid());
			if (account.getAnalReq().equalsIgnoreCase("Y")) {
				gl.setCreatedBy(systemUserService.getLoggedInUser());
				gl.setUpdatedBy(systemUserService.getLoggedInUser());
				gl.setDateCreated(new Date());
				gl.setDateUpdated(new Date());
				gl.setStatus(Status.ACTIVE);
				gl.setDataSourceScreen(DataSourceScreen.DoubleEntry);
				gl.setCustomeraccount(payment.getCustomerAccount());
				// Loan loan = loanService.find(payment.get.getSysid());
				gl.setFSAVE(Fsave.Y);
				CustomerAccount loannumber = customeraccountService.find(payment.getCustomerAccount().getSysid());
				// int loanid = Integer.parseInt(loan.getLOANID());
				gl.setMEMBERID(loannumber.getClientCode());
				if (payment.getLoanaccount() != null) {
					String loans = payment.getLoanaccount().getLOANID();
					int loanid = Integer.parseInt(loans);
					gl.setLOANID(loanid);
				}

				gl.setTDATE(payment.getTdates());
				gl.setTTYPE(Ttype.C);
				gl.setAMOUNT(payment.getAmount());
				gl.setPAYDET(payment.getRemarks());
				gl.setVNO(payment.getVNO() + "" + DataSourceScreen.Loan);
				// gl.setJNLID(0);
				gl.setTHEADID(payment.getHDNO());
				gl.setLoan(payment.getLoanaccount());
				// gl.setPRODUCTID(account.getProductType().getProductId());
				SystemUser staff = systemUserService.find(payment.getUpdatedBy().getId());
				String name = staff.getUsername();
				gl.setSTAFFNAME(name);
				gl.setCustomeraccount(payment.getCustomerAccount());
			} else {
				System.out.println("credit account is not track");// gl.setDataSourceScreen(DataSourceScreen.DoubleEntry);
			}
		} else {
			System.out.println("debitand customer account are empty");// gl.setDataSourceScreen(DataSourceScreen.DoubleEntry);

		}
		return gl;
	}

	private TransML generatememberstmd(DoubleEntry recieve) {
		TransML gl = new TransML();
		if ((recieve.getCustomerAccount() != null) && (recieve.getDebit() != null)) {
			Account account = accountService.find(recieve.getDebit().getSysid());
			if (account.getAnalReq().equalsIgnoreCase("Y")) {
				gl.setCreatedBy(systemUserService.getLoggedInUser());
				gl.setUpdatedBy(systemUserService.getLoggedInUser());
				gl.setDateCreated(new Date());
				gl.setDateUpdated(new Date());
				gl.setStatus(Status.ACTIVE);
				gl.setDataSourceScreen(DataSourceScreen.DoubleEntry);
				gl.setCustomeraccount(recieve.getCustomerAccount());
				// Loan loan = loanService.find(recieve.getCustomerAccount().getSysid());

				gl.setFSAVE(Fsave.Y);
				if (recieve.getLoanaccount() != null) {
					String loans = recieve.getLoanaccount().getLOANID();
					int loanid = Integer.parseInt(loans);
					gl.setLOANID(loanid);
				}
				CustomerAccount loannumber = customeraccountService.find(recieve.getCustomerAccount().getSysid());

				// int loanid = Integer.parseInt(loan.getLOANID());
				gl.setMEMBERID(loannumber.getClientCode());

//			Account account = accountService.find(payment.getCredit().getId());
				// gl.setLOANID(loanid);
				gl.setTDATE(recieve.getTdates());
				gl.setTTYPE(Ttype.D);
				gl.setAMOUNT(recieve.getAmount());
				gl.setPAYDET(recieve.getRemarks());
				gl.setVNO(recieve.getVNO() + "" + DataSourceScreen.Loan);
				// gl.setJNLID(0);
				gl.setTHEADID(recieve.getHDNO());
				gl.setLoan(recieve.getLoanaccount());
				// gl.setPRODUCTID(account.getProductType().getProductId());
				SystemUser staff = systemUserService.find(recieve.getUpdatedBy().getId());
				String name = staff.getUsername();
				gl.setSTAFFNAME(name);
				gl.setCustomeraccount(recieve.getCustomerAccount());
			} else {
				System.out.println("debit account is not track ");// gl.setDataSourceScreen(DataSourceScreen.DoubleEntry);
			}
		} else {
			System.out.println("debit and customer account are empty");// gl.setDataSourceScreen(DataSourceScreen.DoubleEntry);

		}
		return gl;
	}

	public SwizFeedback update(DoubleEntry theadp) {
		try {

			DoubleEntry ledger = find(theadp.getHDNO());
			if (ledger != null) {
				theadp.setCreatedBy(ledger.getCreatedBy());
				theadp.setUpdatedBy(systemUserService.getLoggedInUser());
				theadp.setDateCreated(ledger.getDateCreated());
				theadp.setDateUpdated(new Date());

				doubleentryDao.update(theadp);
				return new SwizFeedback(true, "Transaction Updated successfully");
			} else {
				return new SwizFeedback(false, "Transaction does not exist in the general ledger");
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public SwizFeedback delete(DoubleEntry theadp) {
		try {
			DoubleEntry ledger = find(theadp.getHDNO());
			if (ledger != null) {

				ledger.setUpdatedBy(systemUserService.getLoggedInUser());
				ledger.setDateUpdated(new Date());
				ledger.setStatus(Status.DELETED);
				doubleentryDao.update(theadp);
				return new SwizFeedback(true, "Transaction Deleted successfully");
			} else {
				return new SwizFeedback(false, "Transaction does not exist in the general ledger");
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public List<DoubleEntry> find() {
		try {
//			Search search = new Search();
//			search.addFilterEqual("status", Status.ACTIVE);
			return doubleentryDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	public DoubleEntry find(String id) {
		try {
			return doubleentryDao.find(id);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	public int getLoanCount() {
		int count = 0;
		try {

			List<DoubleEntry> hdno = doubleentryDao.findAll();

			count = hdno.size();

		} catch (Exception ex) {

		}

		return count;
	}

	public String generateCode() {
		try {
			DoubleEntry thead = new DoubleEntry();
			thead.setCreatedBy(systemUserService.getLoggedInUser());
			thead.setUpdatedBy(systemUserService.getLoggedInUser());
			thead.setDateCreated(new Date());
			thead.setDateUpdated(new Date());
			thead.setStatus(Status.ACTIVE);
			thead.setHDNO(gethdnoCount());
			DoubleEntry thead2 = doubleentryDao.save(thead);
			if (thead2 != null) {
				return thead2.getHDNO();
			}
		} catch (Exception ex) {

		}
		return null;
	}

	public String gethdnoCount() {

		String s = null;

		try {

			int count = getLoanCount() + 1;
			long n = count;
			int digits = 1;
			char[] zeros = new char[digits];
			Arrays.fill(zeros, '0');
			DecimalFormat df = new DecimalFormat(String.valueOf(zeros));
			s = df.format(n);

		} catch (Exception ex) {

		}

		return s;
	}

	public String generateacctNo(DoubleEntry theadp) {
		String loanNo = null;
		try {
			if (theadp.getCustomerAccount() != null) {
				CustomerAccount acode = customeraccountService.find(theadp.getCustomerAccount().getSysid());
				if (acode != null) {
					loanNo = acode.getClientCode();
				}

			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return loanNo;
	}

//	public String generateacodeC(DoubleEntry theadp) {
//		String acode = null;
//		try {
//			//if (theadp.getCredit() != null) {
//				Account acode2 = accountService.find(theadp.getCredit().getId());
//				acode = acode2.getACODE();
//			}
//
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//		}
//		return acode;
//	}
	public String generateacodC(DoubleEntry theadp) {
		String loanNo = null;
		try {
			if (theadp.getCredit() != null) {
				Account acode = accountService.find(theadp.getCredit().getSysid());
				if (acode != null) {
					loanNo = acode.getACODE();
				}

			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return loanNo;
	}

	public String generateacodD(DoubleEntry theadp) {
		String loanNo = null;
		try {
			if (theadp.getDebit() != null) {
				Account acode = accountService.find(theadp.getDebit().getSysid());
				if (acode != null) {
					loanNo = acode.getACODE();
				}

			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return loanNo;
	}

	public Account debit(DoubleEntry theadp) {
		Account loanNo = null;
		try {
			if (theadp.getDebit() != null) {
				Account acode = accountService.find(theadp.getDebit().getSysid());
				if (acode != null) {
					loanNo = acode;
				}

			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return loanNo;
	}

	public Account credit(DoubleEntry theadp) {
		Account loanNo = null;
		try {
			if (theadp.getCredit() != null) {
				Account acode = accountService.find(theadp.getCredit().getSysid());
				if (acode != null) {
					loanNo = acode;
				}

			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return loanNo;
	}

}
//private TransML generatememberstmc(DoubleEntry payment) {
//	TransML gl = new TransML();
//	gl.setCreatedBy(systemUserService.getLoggedInUser());
//	gl.setUpdatedBy(systemUserService.getLoggedInUser());
//	gl.setDateCreated(new Date());
//	gl.setDateUpdated(new Date());
//	gl.setStatus(Status.ACTIVE);
//	gl.setDataSourceScreen(DataSourceScreen.DoubleEntry);
//	gl.setMEMBERID(payment.getAcctNo());
//	gl.setCustomeraccount(payment.getCustomerAccount());
//	
//	gl.setFSAVE(Fsave.Y);
////	gl.setPRODUCTID(generateacodeproducttype(payment));
//	int loanid = Integer.parseInt(payment.getLoanNumber());
//
//	gl.setLOANID(loanid);
//	gl.setTDATE(payment.getTdates());
//	gl.setTTYPE(Ttype.C);
//	gl.setAMOUNT(payment.getAmount());
//	gl.setPAYDET(payment.getRemarks());
//	gl.setVNO(payment.getVNO());
//	gl.setJNLID(0);
//	gl.setTHEADID(payment.getHDNO());
//	gl.setLoan(payment.getLoanid());
//
//	SystemUser staff = systemUserService.find(payment.getUpdatedBy().getId());
//	String name = staff.getUsername();
//	gl.setSTAFFNAME(name);
//	gl.setCustomeraccount(payment.getCustomerAccount());
//
//	return gl;
//}
//private TransGL generateGLc(DoubleEntry payment) {
//	TransGL gl = new TransGL();
//	gl.setDateCreated(new Date());
//	gl.setDateUpdated(new Date());
//	gl.setDataSourceScreen(DataSourceScreen.DoubleEntry);
//	gl.setCreatedBy(systemUserService.getLoggedInUser());
//	gl.setUpdatedBy(systemUserService.getLoggedInUser());
//	gl.setStatus(Status.ACTIVE);
//	gl.setACODE(payment.getAcodeC());
//	gl.setFsave(Fsave.Y);
//	gl.setAMOUNT(payment.getAmount());
//	gl.setAmtUsd(0);
//	gl.setRecAmt(0);
//	gl.setTDATE(payment.getTdates());
//	gl.setTTYPE(Ttype.C);
//	gl.setPAYDET(payment.getVNO());
//	gl.setREMARKS(payment.getRemarks());
//	gl.setICODE(payment.getPeriodcode());
//	gl.setYRCODE(payment.getYear());
//	gl.setPAYPERSON(payment.getRemarks());
//	gl.setMemberID(payment.getAcctNo());
//	gl.setTheadID(payment.getHDNO());
//	// gl.setJNLNO(0);
//	gl.setYrRec(0);
//	gl.setTrec(0);
//	gl.setAccount(payment.getDebit());
//	SystemUser staff = systemUserService.find(payment.getUpdatedBy().getId());
//	String name = staff.getUsername();
//	gl.setStaffName(name);
//	if (payment.getCustomerAccount() != null) {
//
//		gl.setCustomeraccount(payment.getCustomerAccount());
//	}
//
//	return gl;
//}
