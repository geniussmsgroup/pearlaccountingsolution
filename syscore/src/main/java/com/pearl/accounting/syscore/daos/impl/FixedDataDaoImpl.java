package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.AccountDao;
import com.pearl.accounting.syscore.daos.FixedDataDao;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.FixedData;

@Repository("FixedDataDao")
public class FixedDataDaoImpl extends SwiziBaseDaoImpl<FixedData> implements FixedDataDao {

}
