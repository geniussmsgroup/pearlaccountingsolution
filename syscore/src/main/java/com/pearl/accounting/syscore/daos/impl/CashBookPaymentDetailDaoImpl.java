package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.CashBookPaymentDetailDao;
import com.pearl.accounting.sysmodel.CashBookPaymentDetail;

@Repository("CashBookPaymentDetailDao")
public class CashBookPaymentDetailDaoImpl extends SwiziBaseDaoImpl<CashBookPaymentDetail>
		implements CashBookPaymentDetailDao {

}
