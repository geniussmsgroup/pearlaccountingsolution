package com.pearl.accounting.syscore.daos;

import com.pearl.accounting.sysmodel.Withdrawal;

public interface WithdrawalDao extends SwiziBaseDao<Withdrawal>{

}
