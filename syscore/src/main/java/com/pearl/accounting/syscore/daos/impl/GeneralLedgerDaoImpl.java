package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.GeneralLedgerDao;
import com.pearl.accounting.sysmodel.GeneralLedger;

@Repository("GeneralLedgerDao")
public class GeneralLedgerDaoImpl extends SwiziBaseDaoImpl<GeneralLedger> implements GeneralLedgerDao {

}
