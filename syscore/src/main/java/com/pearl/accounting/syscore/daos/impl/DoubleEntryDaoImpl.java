package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.DoubleEntryDao;
import com.pearl.accounting.sysmodel.DoubleEntry;
 
@Repository("DoubleEntryDao")
public class DoubleEntryDaoImpl extends SwiziBaseDaoImpl<DoubleEntry> implements DoubleEntryDao  {

}
