package com.pearl.accounting.syscore.daos;

import java.util.List;
import java.util.Map;

import com.googlecode.genericdao.dao.jpa.GenericDAO; 

public interface SwiziBaseDao<T>extends GenericDAO<T, String> {
	
	public T findOne(String id);

	public void create(T entity);

	public T update(T entity);

	public T[] update(T[] entities);

	public void delete(T entity);

	public void deleteById(String entityId);

	public List<T> find();

	public List<T> findByAttributes(Map<String, Object> attributes);
	List<T> searchByPropertyEqual(String property, Object value);
}
