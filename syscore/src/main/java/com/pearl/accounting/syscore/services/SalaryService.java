package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.Salary;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface SalaryService {


	public SwizFeedback save(Salary salary);
 

	public List<Salary> find();
}
