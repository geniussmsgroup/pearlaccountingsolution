package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.LoanbalanceView;

public interface LoanBalanceViewService {


	public List<LoanbalanceView> find();

	public LoanbalanceView find(String  MemberId );


}
