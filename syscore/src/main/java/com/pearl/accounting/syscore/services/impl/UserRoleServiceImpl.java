package com.pearl.accounting.syscore.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pearl.accounting.syscore.daos.UserRoleDao;
import com.pearl.accounting.syscore.services.UserRoleService;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.UserRole;

@Service("UserRoleService")
@Transactional
public class UserRoleServiceImpl implements UserRoleService {

	@Autowired
	private UserRoleDao userRoleDao;

	public SwizFeedback save(UserRole userRole) {
		try {
			userRoleDao.save(userRole);
			return new SwizFeedback(true, "Saved successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

			return new SwizFeedback(false, ex.getMessage());
		}

	}

	public SwizFeedback update(UserRole userRole) {
		try {
			userRoleDao.update(userRole);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}

	}

	public SwizFeedback delete(UserRole userRole) {
		try {
			userRoleDao.delete(userRole);
			return new SwizFeedback(true, "Deleted successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public List<UserRole> find() {
		try {
			return userRoleDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public UserRole find(String id) {
		try {
			return userRoleDao.find(id);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

}
