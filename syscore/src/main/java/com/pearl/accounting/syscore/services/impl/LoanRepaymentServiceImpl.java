package com.pearl.accounting.syscore.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.LoanDao;
import com.pearl.accounting.syscore.daos.LoanRepaymentDao;
import com.pearl.accounting.syscore.services.AccountService;
import com.pearl.accounting.syscore.services.ClientStatementService;
import com.pearl.accounting.syscore.services.GeneralLedgerService;
import com.pearl.accounting.syscore.services.LoanRepaymentService;
import com.pearl.accounting.syscore.services.LoanService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.ClientStatement;
import com.pearl.accounting.sysmodel.DataSourceScreen;
import com.pearl.accounting.sysmodel.GeneralLedger;
import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysmodel.LoanRepayment;
import com.pearl.accounting.sysmodel.ProductType;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.TransGL;
import com.pearl.accounting.sysmodel.TransML;
import com.pearl.accounting.sysmodel.Ttype;

@Service("LoanRepaymentService")
@Transactional
public class LoanRepaymentServiceImpl implements LoanRepaymentService {

	@Autowired
	private LoanRepaymentDao loanRepaymentDao;

	@Autowired
	private SystemUserService systemUserService;

	@Autowired
	private GeneralLedgerService generalLedgerService;

	@Autowired
	private AccountService accountService;

	@Autowired
	private ClientStatementService clientStatementService;

	@Autowired
	private LoanService loanService;

	public SwizFeedback save(LoanRepayment loanRepayment) {

		try {

			loanRepayment.setCreatedBy(systemUserService.getLoggedInUser());
			loanRepayment.setUpdatedBy(systemUserService.getLoggedInUser());

			Account payingAccount = accountService.findByName(ProductType.Loan.getProductType());
			
			loanRepayment.setPayingAccount(payingAccount);

			LoanRepayment repayment = loanRepaymentDao.save(loanRepayment);

			if (repayment != null) {

				GeneralLedger generalLedger = generateGeneralLedger(repayment);

				ClientStatement clientStatement = generateClientStatement(repayment);

				GeneralLedger generalLedger2 = generateGeneralLedgerDr(repayment, payingAccount);

				generalLedgerService.save(generalLedger);
				generalLedgerService.save(generalLedger2);

				clientStatementService.save(clientStatement);

				return new SwizFeedback(true, "Payment posted successfully");

			} else {

				return new SwizFeedback(false, "Loan repayment not posted successfully. Please try again.");
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	// credit the loan A/C in the general ledger
	private GeneralLedger generateGeneralLedgerDr(LoanRepayment repayment, Account loanAccount) {
		try {

			if (loanAccount != null) {
				GeneralLedger generalLedger = new GeneralLedger();
				generalLedger.setAccount(loanAccount);
				generalLedger.setPeriod(repayment.getPeriod());
				generalLedger.setBalanceCredit(0);
				generalLedger.setBalanceDebt(0);
				generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
				generalLedger.setDateCreated(new Date());
				generalLedger.setDateUpdated(new Date());
				generalLedger.setYear(repayment.getYear());
				generalLedger.setPaymentDetails(repayment.getPaymentDetails());
				generalLedger.setPostReferenceNo(repayment.getId());
				generalLedger.setDataSourceScreen(DataSourceScreen.Loan);
				generalLedger.setRemarks(ProductType.Loan.getProductType());
				generalLedger.setStatus(Status.ACTIVE);
				generalLedger.setTransactionCredit(repayment.getDepositedAmount());
				generalLedger.setTransactionDebt(0);
				generalLedger.setTransactionDescription(ProductType.Loan.getProductType());
				generalLedger.setTtype(Ttype.C);
				generalLedger.setTransationDate(repayment.getDepositDate());
				generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
				return generalLedger;
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	// debit the receiveing account in the general ledger
	private GeneralLedger generateGeneralLedger(LoanRepayment repayment) {

		GeneralLedger generalLedger = new GeneralLedger();

		generalLedger.setAccount(repayment.getReceiveingAccount());
		generalLedger.setPeriod(repayment.getPeriod());
		generalLedger.setBalanceCredit(0);
		generalLedger.setBalanceDebt(0);
		generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
		generalLedger.setDateCreated(new Date());
		generalLedger.setDateUpdated(new Date());
		generalLedger.setYear(repayment.getYear());
		generalLedger.setPaymentDetails(repayment.getPaymentDetails());
		generalLedger.setPostReferenceNo(repayment.getId());
		generalLedger.setDataSourceScreen(DataSourceScreen.Loan);
		generalLedger.setRemarks(ProductType.Loan.getProductType());
		generalLedger.setStatus(Status.ACTIVE);
		generalLedger.setTransactionCredit(0);
		generalLedger.setTransactionDebt(repayment.getDepositedAmount());
		generalLedger.setTransactionDescription(ProductType.Loan.getProductType());
		generalLedger.setTtype(Ttype.D);
		generalLedger.setTransationDate(repayment.getDepositDate());
		generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());

		return generalLedger;
	}

	private ClientStatement generateClientStatement(LoanRepayment repayment) {
		Account interest = accountService.findByName2(ProductType.LNP.getProductType());

		ClientStatement clientStatement = new ClientStatement();

		LoanRepayment loanRepayment = find(repayment.getId());

		if (loanRepayment != null) {

			if (repayment.getLoan() != null) {
				
				Loan loan = loanService.find(repayment.getLoan().getLOANID());

				if (loan != null) {
					System.out.println("ClientStatement:==> " + loan.getCustomerAccount().getSysid());

					clientStatement.setCustomerAccount(loan.getCustomerAccount());

					clientStatement.setPeriod(loanRepayment.getPeriod());
					clientStatement.setBalanceCredit(0);
					clientStatement.setBalanceDebt(0);
					clientStatement.setCreatedBy(systemUserService.getLoggedInUser());
					clientStatement.setDateCreated(new Date());
					clientStatement.setDateUpdated(new Date());
					clientStatement.setYear(loanRepayment.getYear());
					clientStatement.setPaymentDetails(loanRepayment.getPaymentDetails());
					clientStatement.setPostReferenceNo(loanRepayment.getId());
					clientStatement.setProductType(interest.getProductType().getProductId());
					clientStatement.setRemarks(ProductType.Loan.getProductType());
					clientStatement.setStatus(Status.ACTIVE);
					clientStatement.setTransactionCredit(loanRepayment.getDepositedAmount());
					clientStatement.setTransactionDebt(0);
					clientStatement.setTransactionDescription(ProductType.Loan.getProductType());
					clientStatement.setTtype(Ttype.C);
					clientStatement.setTransationDate(new Date());
					clientStatement.setUpdatedBy(systemUserService.getLoggedInUser());

					return clientStatement;
				}
			}

		} else {
			return null;
		}

		return null;
	}

	public SwizFeedback update(LoanRepayment loanRepayment) {
		try {
			loanRepaymentDao.update(loanRepayment);
			return new SwizFeedback(true, "Payment Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public SwizFeedback delete(LoanRepayment loanRepayment) {
		try {
			loanRepaymentDao.delete(loanRepayment);
			return new SwizFeedback(true, "Payment deleted successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public List<LoanRepayment> find() {
		try {
			return loanRepaymentDao.find();

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public LoanRepayment find(String id) {
		try {
			return loanRepaymentDao.find(id);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	@Transactional(readOnly = true)
	public List<LoanRepayment> find(Loan loan) {
		List<LoanRepayment> list = new ArrayList<LoanRepayment>();
		try {
			

			Search search = new Search();
			search.addFilterEqual("loan", loan);
			List<LoanRepayment> loanRepayments = loanRepaymentDao.search(search);
			for (LoanRepayment loanRepayment : loanRepayments) {
				if (loanRepayment.getLoan() != null) {
					 
					
					
					loanRepayment.setLoan(null);
			
				}
				list.add(loanRepayment);
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return list;
	}

	@Override
	public List<LoanRepayment> getLoanRepayment(Loan loan) {
		List<LoanRepayment> list = new ArrayList<LoanRepayment>();

	
		try {
		float tot = loan.getLoanAmount();
		float bal = tot;
		List<LoanRepayment> list1 = loanRepaymentDao.find();
		for (LoanRepayment repay : list1) {
			bal -= repay.getDepositedAmount();
		
			repay.setBalance(bal);
			list.add(repay);

		}
		}
		catch(Exception ex) {
			
		}
		
		
		return null;
	

}

//	private TransGL generateGLc(LoanRepaymentServiceImpl payment) {
//		TransGL gl = new TransGL();
//		gl.setDateCreated(new Date());
//		gl.setDateUpdated(new Date());
//		gl.setDataSourceScreen(DataSourceScreen.DoubleEntry);
//		gl.setCreatedBy(systemUserService.getLoggedInUser());
//		gl.setUpdatedBy(systemUserService.getLoggedInUser());
//		gl.setStatus(Status.ACTIVE);
//		gl.setACODE(payment.getAcodeC());
//		gl.setFsave(Fsave.Y);
//		gl.setAMOUNT(payment.getAmount());
//		gl.setAmtUsd(0);
//		gl.setRecAmt(0);
//		gl.setTDATE(payment.getTdates());
//		gl.setTTYPE(Ttype.C);
//		gl.setPAYDET(payment.getVNO());
//		gl.setREMARKS(payment.getRemarks());
//		gl.setICODE(payment.getPeriodcode());
//		gl.setYRCODE(payment.getYear());
//		gl.setPAYPERSON(payment.getRemarks());
//		gl.setMemberID(payment.getAcctNo());
//		gl.setTheadID(payment.getHDNO());
//		// gl.setJNLNO(0);
//		gl.setYrRec(0);
//		gl.setTrec(0);
//		gl.setAccount(payment.getDebit());
//		SystemUser staff = systemUserService.find(payment.getUpdatedBy().getId());
//		String name = staff.getUsername();
//		gl.setStaffName(name);
//		if (payment.getCustomerAccount() != null) {
//	
//			gl.setCustomeraccount(payment.getCustomerAccount());
//		}
//	
//		return gl;
//	}
//	private TransML generatememberstmc(LoanRepaymentServiceImpl payment) {
//	TransML gl = new TransML();
//	gl.setCreatedBy(systemUserService.getLoggedInUser());
//	gl.setUpdatedBy(systemUserService.getLoggedInUser());
//	gl.setDateCreated(new Date());
//	gl.setDateUpdated(new Date());
//	gl.setStatus(Status.ACTIVE);
//	gl.setDataSourceScreen(DataSourceScreen.DoubleEntry);
//	gl.setMEMBERID(payment.getAcctNo());
//	gl.setCustomeraccount(payment.getCustomerAccount());
//	
//	gl.setFSAVE(Fsave.Y);
////	gl.setPRODUCTID(generateacodeproducttype(payment));
//	int loanid = Integer.parseInt(payment.getLoanNumber());
//
//	gl.setLOANID(loanid);
//	gl.setTDATE(payment.getTdates());
//	gl.setTTYPE(Ttype.C);
//	gl.setAMOUNT(payment.getAmount());
//	gl.setPAYDET(payment.getRemarks());
//	gl.setVNO(payment.getVNO());
//	gl.setJNLID(0);
//	gl.setTHEADID(payment.getHDNO());
//	gl.setLoan(payment.getLoanid());
//
//	SystemUser staff = systemUserService.find(payment.getUpdatedBy().getId());
//	String name = staff.getUsername();
//	gl.setSTAFFNAME(name);
//	gl.setCustomeraccount(payment.getCustomerAccount());
//
//	return gl;
//}

	
}
