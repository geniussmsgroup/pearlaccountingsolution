//package com.pearl.accounting.syscore.services.impl;
//
//import java.util.Date;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.googlecode.genericdao.search.Search;
//import com.pearl.accounting.syscore.daos.LoantestDao;
//import com.pearl.accounting.syscore.services.AccountService;
//import com.pearl.accounting.syscore.services.AtypeAservice;
//import com.pearl.accounting.syscore.services.LoantestService;
//import com.pearl.accounting.syscore.services.ProductSetupService;
//import com.pearl.accounting.syscore.services.SystemUserService;
//import com.pearl.accounting.sysmodel.Account;
//import com.pearl.accounting.sysmodel.AtypeA;
//import com.pearl.accounting.sysmodel.Loantest;
//import com.pearl.accounting.sysmodel.ProductSetup;
//import com.pearl.accounting.sysmodel.Status;
//import com.pearl.accounting.sysmodel.SwizFeedback;
//
//@Service("LoantestService")
//@Transactional
//public class LoantestServiceImpl implements LoantestService {
//
//	@Autowired
//	private LoantestDao loantestao;
//	@Autowired
//	private AtypeAservice atypeService;
//	@Autowired
//	private ProductSetupService productsetupService;
//
//	@Autowired
//	private SystemUserService systemUserService;
//
//	public SwizFeedback save(Loantest account) {
//		try {
//
//			account.setCreatedBy(systemUserService.getLoggedInUser());
//			account.setUpdatedBy(systemUserService.getLoggedInUser());
//			account.setStatus(Status.ACTIVE);
//			account.setDateCreated(new Date());
//			account.setDateUpdated(new Date());
//			loantestao.save(account);
//			return new SwizFeedback(true, "Saved successfully");
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//			return new SwizFeedback(true, ex.getMessage());
//		}
//
//	}
//
//	public SwizFeedback update(Loantest account) {
//		try {
//			loantestao.update(account);
//			return new SwizFeedback(true, "Updated successfully");
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//			return new SwizFeedback(true, ex.getMessage());
//		}
//	}
//
//	public SwizFeedback delete(Loantest account) {
//		try {
//			loantestao.delete(account);
//			return new SwizFeedback(true, "Updated successfully");
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//			return new SwizFeedback(true, ex.getMessage());
//		}
//	}
//
//	public List<Loantest> find() {
//		try {
//
//			return loantestao.find();
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//
//		}
//		return null;
//	}
//
//	public Loantest find(String Sysid) {
//		try {
//
//			return loantestao.find(Sysid);
//
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//
//		}
//		return null;
//	}
//
//	public Loantest findByName(String accountName) {
//		try {
//
//			Search search = new Search();
//			search.addFilterILike("accountName", accountName);
//			return loantestao.searchUnique(search);
//
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//
//		}
//		return null;
//	}
//
//	public Loantest findByName2(String productType) {
//		try {
//
//			Search search = new Search();
//			search.addFilterILike("productType", productType);
//			return loantestao.searchUnique(search);
//
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//
//		}
//		return null;
//	}
//	public String AtypeA(Account account) {
//	String accountcd = null;
//		try {
//			AtypeA acode = atypeService.find(account.getAccountType().getAtypecd());
//                 accountcd = acode.getAtypecd();
//           
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//
//		}		return accountcd;	
//		
//	}
//public String productid(Account account) {
//	String productsetup = null;
//	try {
//          ProductSetup producttype = productsetupService.find(account.getProductType().getProductId());
//                     productsetup = producttype.getProductId();
//
//	} catch (Exception ex) {
//		System.out.println(ex.getMessage());
//
//	}
//	return productsetup;	
//	}
// 
//}
