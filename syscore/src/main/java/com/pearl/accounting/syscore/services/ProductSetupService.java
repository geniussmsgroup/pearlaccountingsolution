package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.ProductSetup;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface ProductSetupService {
	public SwizFeedback save(ProductSetup productsetup);

	public SwizFeedback update(ProductSetup productsetup);

	public SwizFeedback delete(ProductSetup productsetup);

	public List<ProductSetup> find();

	public ProductSetup find(String id);
	
	public ProductSetup findByName(String productDetails);

}