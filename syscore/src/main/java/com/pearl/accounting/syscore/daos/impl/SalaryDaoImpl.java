package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.SalaryDao;
import com.pearl.accounting.sysmodel.Salary;

@Repository("SalaryDao")
public class SalaryDaoImpl extends SwiziBaseDaoImpl<Salary> implements SalaryDao{

}
