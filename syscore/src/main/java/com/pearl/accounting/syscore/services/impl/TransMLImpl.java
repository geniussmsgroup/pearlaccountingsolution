package com.pearl.accounting.syscore.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pearl.accounting.syscore.daos.TransMLDao;
import com.pearl.accounting.syscore.services.TransMLService;
import com.pearl.accounting.sysmodel.TransML;
import com.pearl.accounting.sysmodel.SwizFeedback;

@Service("TransMLService")
@Transactional
public class TransMLImpl implements TransMLService {

	@Autowired
	private TransMLDao transmlDao;

	public SwizFeedback save(TransML transml) {
		try {
			transmlDao.save(transml);
			return new SwizFeedback(true, "Saved successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null; 
	}
	
	public SwizFeedback save(List<TransML> transmls) {
		try {
			TransML[] list=new TransML[transmls.size()];
			transmlDao.save(transmls.toArray(list));
			System.out.println("client statement saved successfully");
			return new SwizFeedback(true, "Saved successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	public SwizFeedback update(TransML transml) {

		try {
			transmlDao.update(transml);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	public SwizFeedback delete(TransML tranml) {
		try {
			transmlDao.delete(tranml);
			return new SwizFeedback(true, "Deleted successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	public List<TransML> find() {
		try {
			return transmlDao.find(); 
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	public TransML find(String sysid) {
		try {
			return transmlDao.find(sysid); 
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

}
