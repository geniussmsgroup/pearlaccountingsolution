package com.pearl.accounting.syscore.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.AssessmentPeriodDao;
import com.pearl.accounting.syscore.services.AssessmentPeriodService;
import com.pearl.accounting.sysmodel.AssessmentPeriod;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.SwizFeedback;

@Service("AssessmentPeriodService")
@Transactional
public class AssessmentPeriodServiceImpl implements AssessmentPeriodService {

	@Autowired
	private AssessmentPeriodDao assessmentPeriodDao;

	public SwizFeedback save(AssessmentPeriod assessmentPeriod) {
		try {
			assessmentPeriodDao.save(assessmentPeriod);
			return new SwizFeedback(true, "Saved successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}

	}

	public SwizFeedback update(AssessmentPeriod assessmentPeriod) {
		try {
			assessmentPeriodDao.update(assessmentPeriod);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public SwizFeedback delete(AssessmentPeriod assessmentPeriod) {
		try {
			assessmentPeriodDao.delete(assessmentPeriod);
			return new SwizFeedback(true, "Saved successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public List<AssessmentPeriod> find() {
		try {
			return assessmentPeriodDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public AssessmentPeriod find(String id) {
		try {
			return assessmentPeriodDao.find(id);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	//
	 
	
	public SwizFeedback activate(AssessmentPeriod assessmentPeriod) {
		try {

			AssessmentPeriod period = find(assessmentPeriod.getId());

			if (period != null) {
				period.setActivationStatus(Status.ACTIVE);
				assessmentPeriodDao.save(period);
				return new SwizFeedback(true, "Activated successfully");
			} else {
				return new SwizFeedback(true, "Assessement period does not exist.");
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}

	}
	
	

	public SwizFeedback deactivate(AssessmentPeriod assessmentPeriod) {
		try {

			AssessmentPeriod period = find(assessmentPeriod.getId());

			if (period != null) {
				period.setActivationStatus(Status.IN_ACTIVE);
				assessmentPeriodDao.save(period);
				return new SwizFeedback(true, "De-activated successfully");
			} else {
				return new SwizFeedback(true, "Assessement period does not exist.");
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}

	}

	public AssessmentPeriod findActive() {
		try {
			Search search = new Search();
			search.addFilterEqual("activationStatus", Status.ACTIVE);
			List<AssessmentPeriod> assessmentPeriods = assessmentPeriodDao.search(search);
			if (assessmentPeriods != null) {
				if (!assessmentPeriods.isEmpty()) {
					return assessmentPeriods.get(0);
				}
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}
	
	public AssessmentPeriod findByCode(String code) {
		try {
			Search search = new Search();
			search.addFilterEqual("code", code);
			List<AssessmentPeriod> assessmentPeriods = assessmentPeriodDao.search(search);
			if (assessmentPeriods != null) {
				if (!assessmentPeriods.isEmpty()) {
					return assessmentPeriods.get(0);
				}
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

}
