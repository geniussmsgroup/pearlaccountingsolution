package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.ClientStatement;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface ClientStatementService {

	public SwizFeedback save(ClientStatement clientStatement);

	public SwizFeedback update(ClientStatement clientStatement);

	public SwizFeedback delete(ClientStatement clientStatement);

	public List<ClientStatement> find();

	public ClientStatement find(String id);
	
	public SwizFeedback save(List<ClientStatement> clientStatement);

}
