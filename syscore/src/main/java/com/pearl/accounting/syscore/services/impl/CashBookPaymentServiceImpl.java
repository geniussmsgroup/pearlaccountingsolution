package com.pearl.accounting.syscore.services.impl;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.CashBookPaymentDao;
import com.pearl.accounting.syscore.daos.CashBookPaymentDetailDao;
import com.pearl.accounting.syscore.daos.TheadpDao;
import com.pearl.accounting.syscore.services.AccountService;
import com.pearl.accounting.syscore.services.CashBookPaymentService;
import com.pearl.accounting.syscore.services.GeneralLedgerService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.syscore.services.TheadService;
import com.pearl.accounting.syscore.services.TheadpService;
import com.pearl.accounting.syscore.services.TransGLService;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.CashBookPayment;
import com.pearl.accounting.sysmodel.CashBookPaymentDetail;
import com.pearl.accounting.sysmodel.DataSourceScreen;
import com.pearl.accounting.sysmodel.FinancialYear;
import com.pearl.accounting.sysmodel.Fsave;
import com.pearl.accounting.sysmodel.GeneralLedger;
import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysmodel.ProductType;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.Thead;
import com.pearl.accounting.sysmodel.Theadp;
import com.pearl.accounting.sysmodel.TransGL;
import com.pearl.accounting.sysmodel.TransML;
import com.pearl.accounting.sysmodel.Ttype;

@Service("CashBookPaymentService")
@Transactional
public class CashBookPaymentServiceImpl implements CashBookPaymentService {

	@Autowired
	private CashBookPaymentDao cashBookPaymentDao;

	@Autowired
	private SystemUserService systemUserService;
	
	@Autowired
	private TransGLService transglService;

	@Autowired
	private CashBookPaymentDetailDao cashBookPaymentDetailDao;

	@Autowired
	private GeneralLedgerService generalLedgerService;

	@Autowired
	private TheadpService theadpService;
	
	@Autowired
	private AccountService accountService;

	public SwizFeedback savetoSpring(CashBookPayment cashBookPayment) {
		try {

			cashBookPayment.setCreatedBy(systemUserService.getLoggedInUser());
			cashBookPayment.setUpdatedBy(systemUserService.getLoggedInUser());
			cashBookPayment.setThead(theadpService.getthead());
            cashBookPayment.setAcodeCredit(getpayingAccount(cashBookPayment));
            
			CashBookPayment bookPayment = cashBookPaymentDao.save(cashBookPayment);
                
			if (bookPayment != null) {
                     theadpService.save(savethead(bookPayment));
				SwizFeedback feedback = generalLedgerService.save(generateGeneralLedger(bookPayment));
				 SwizFeedback feedback2 = transglService.save(generateGeneralleger(bookPayment));

				if (feedback.isResponse()&&feedback2.isResponse()) {
					return new SwizFeedback(true, "Payment successfully posted");
				} else {
					return feedback;
				}
			} else {
				return new SwizFeedback(false, "Error while posting transaction. Please try again.");
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}

	}

	// debit and credit the receiveing account and Paying account in the general
	// ledger
	private List<GeneralLedger> generateGeneralLedger(CashBookPayment payment) {

		List<GeneralLedger> list = new ArrayList<GeneralLedger>();

		List<CashBookPaymentDetail> details = findByPayement(payment);

		for (CashBookPaymentDetail detail : details) {

			GeneralLedger generalLedger = new GeneralLedger();

			generalLedger.setAccount(detail.getReceivingAccount());
			generalLedger.setPeriod(payment.getPeriod());
			generalLedger.setBalanceCredit(0);
			generalLedger.setBalanceDebt(0);
			generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
			generalLedger.setDateCreated(new Date());
			generalLedger.setDateUpdated(new Date());
			generalLedger.setYear(payment.getYear());
			generalLedger.setPaymentDetails(detail.getRemarks());
			generalLedger.setPostReferenceNo(detail.getId());
			generalLedger.setDataSourceScreen(DataSourceScreen.CASH_BOOK_PAYMENTS);
			generalLedger
					.setRemarks(DataSourceScreen.CASH_BOOK_PAYMENTS.getDataSourceScreen() + " :" + detail.getRemarks());
			generalLedger.setStatus(Status.ACTIVE);
			generalLedger.setTransactionCredit(0);
			generalLedger.setTransactionDebt(detail.getReceivedAmount());
			generalLedger.setTransactionDescription(DataSourceScreen.CASH_BOOK_PAYMENTS.getDataSourceScreen());
			generalLedger.setTtype(Ttype.D);
			generalLedger.setTransationDate(payment.getPaymentDate());
			generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
			generalLedger.setThead(payment.getThead());

			list.add(generalLedger);

		}

		GeneralLedger generalLedger = new GeneralLedger();
		generalLedger.setAccount(payment.getPayingAccount());
		generalLedger.setPeriod(payment.getPeriod());
		generalLedger.setBalanceCredit(0);
		generalLedger.setBalanceDebt(0);
		generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
		generalLedger.setDateCreated(new Date());
		generalLedger.setDateUpdated(new Date());
		generalLedger.setYear(payment.getYear());
		generalLedger.setPaymentDetails(payment.getRemarks());
		generalLedger.setPostReferenceNo(payment.getId());
		generalLedger.setDataSourceScreen(DataSourceScreen.CASH_BOOK_PAYMENTS);
		generalLedger.setRemarks(DataSourceScreen.CASH_BOOK_PAYMENTS.getDataSourceScreen() + ":" + payment.getPayee());
		generalLedger.setStatus(Status.ACTIVE);
		generalLedger.setTransactionCredit(payment.getAmountPaid());
		generalLedger.setTransactionDebt(0);
		generalLedger.setTransactionDescription(DataSourceScreen.CASH_BOOK_PAYMENTS.getDataSourceScreen());
		generalLedger.setTtype(Ttype.C);
		generalLedger.setTransationDate(payment.getPaymentDate());
		generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
		generalLedger.setThead(payment.getThead());
		generalLedger.setTransationDate(payment.getPaymentDate());
		;

		list.add(generalLedger);

		return list;
	}

	private List<TransGL> generateGeneralleger(CashBookPayment payment) {

		List<TransGL> list = new ArrayList<TransGL>();

		List<CashBookPaymentDetail> details = findByPayement(payment);

		for (CashBookPaymentDetail detail : details) {
Account account = accountService.find(detail.getReceivingAccount().getSysid()); 
			TransGL gl = new TransGL();
			gl.setDateCreated(new Date());
			gl.setDateUpdated(new Date());
			gl.setDataSourceScreen(DataSourceScreen.CASH_BOOK_PAYMENTS);
			gl.setCreatedBy(systemUserService.getLoggedInUser());
			gl.setUpdatedBy(systemUserService.getLoggedInUser());
			gl.setStatus(Status.ACTIVE);
			 //gl.setACODE();
			gl.setFsave(Fsave.Y);
			gl.setAMOUNT(detail.getReceivedAmount());
			gl.setAmtUsd(0);
			gl.setRecAmt(0);
			gl.setTDATE(payment.getPaymentDate());
			gl.setTTYPE(Ttype.D);
			gl.setPAYDET(payment.getVoucherNumber());
			gl.setREMARKS(payment.getRemarks() + detail.getRemarks());
			gl.setICODE(payment.getPeriodcode());
			gl.setYRCODE(payment.getYear());
			gl.setPAYPERSON(payment.getRemarks());
			// gl.setMemberID(payment.getAcctNo());
			gl.setTheadID(payment.getThead());
			gl.setACODE(account.getACODE());
			gl.setYrRec(0);
			gl.setTrec(0);
			gl.setAccount(detail.getReceivingAccount());
			gl.setCreatedBy(systemUserService.getLoggedInUser());
			gl.setUpdatedBy(systemUserService.getLoggedInUser());

			list.add(gl);

		}

		TransGL gl = new TransGL();
		gl.setDateCreated(new Date());
		gl.setDateUpdated(new Date());
		gl.setDataSourceScreen(DataSourceScreen.CASH_BOOK_PAYMENTS);
		gl.setCreatedBy(systemUserService.getLoggedInUser());
		gl.setUpdatedBy(systemUserService.getLoggedInUser());
		gl.setStatus(Status.ACTIVE);
		 gl.setACODE(getpayingAccount(payment));
		gl.setFsave(Fsave.Y);
		gl.setAMOUNT(payment.getAmountPaid());
		gl.setAmtUsd(0);
		gl.setRecAmt(0);
		gl.setTDATE(payment.getPaymentDate());
		gl.setTTYPE(Ttype.C);
		gl.setPAYDET(payment.getVoucherNumber());
		gl.setREMARKS(payment.getRemarks());
		gl.setICODE(payment.getPeriodcode());
		gl.setYRCODE(payment.getYear());
		gl.setPAYPERSON(payment.getRemarks());
		// gl.setMemberID(payment.getAcctNo());
		gl.setTheadID(payment.getThead());
		// gl.setJNLNO(0);
		gl.setYrRec(0);
		gl.setTrec(0);
		gl.setAccount(payment.getPayingAccount());
		list.add(gl);

		return list;
	}

	public SwizFeedback update(CashBookPayment cashBookPayment) {
		try {
			cashBookPayment.setUpdatedBy(systemUserService.getLoggedInUser());

			cashBookPaymentDao.update(cashBookPayment);
			return new SwizFeedback(true, "Payment successfully Updated");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}
	}

	public SwizFeedback delete(CashBookPayment cashBookPayment) {
		try {
			cashBookPaymentDao.delete(cashBookPayment);
			return new SwizFeedback(true, "Payment successfully Deleted");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}

	}

	@Transactional(readOnly = true)
	public List<CashBookPayment> find() {
		List<CashBookPayment> bookPayments = new ArrayList<CashBookPayment>();
		try {

			for (CashBookPayment cashBookPayment : cashBookPaymentDao.find()) {
				if (cashBookPayment.getCashBookPaymentDetails() != null) {
					cashBookPayment.setCashBookPaymentDetails(null);
				}
				bookPayments.add(cashBookPayment);
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return bookPayments;
	}

	public CashBookPayment find(String id) {
		try {
			return cashBookPaymentDao.find(id);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	@Transactional(readOnly = true)
	public List<CashBookPaymentDetail> find(CashBookPayment cashBookPayment) {

		List<CashBookPaymentDetail> list = new ArrayList<CashBookPaymentDetail>();

		try {

			Search search = new Search();
			search.addFilterEqual("cashBookPayment", cashBookPayment);
			List<CashBookPaymentDetail> details = cashBookPaymentDetailDao.search(search);

			for (CashBookPaymentDetail detail : details) {

				if (detail.getCashBookPayment() != null) {

					detail.setCashBookPayment(null);
				}

				list.add(detail);
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return list;
	}

	public List<CashBookPaymentDetail> findByPayement(CashBookPayment cashBookPayment) {

		List<CashBookPaymentDetail> list = new ArrayList<CashBookPaymentDetail>();

		try {

			Search search = new Search();
			search.addFilterEqual("cashBookPayment", cashBookPayment);
			list = cashBookPaymentDetailDao.search(search);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return list;
	}

	private String getpayingAccount(CashBookPayment cashBook) {
		String result = null;

		try {
			if (cashBook.getPayingAccount() != null) {
				Account account = accountService.find(cashBook.getPayingAccount().getSysid());
				result = account.getACODE();

			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return result;
	}

	
	public Theadp savethead(CashBookPayment cashbookpayment) {
		Theadp account = new Theadp();
		account.setAccount(cashbookpayment.getPayingAccount());
		account.setRemarks(cashbookpayment.getRemarks());
		account.setVNO(cashbookpayment.getPayee());
		//account.setRemarks(remarks);
		account.setTdates(cashbookpayment.getPaymentDate());
		account.setDataSourceScreen(DataSourceScreen.CASH_BOOK_PAYMENTS);
		account.setAmount(cashbookpayment.getAmountPaid());
		account.setDateCreated(new Date());
		account.setDateUpdated(new Date());
		account.setPeriodcode(cashbookpayment.getPeriodcode());
		
		
		return account;
	}
	
	
	
	
	
	private String getcashbookdetailcodes(CashBookPayment cashBookDetail) {
		// List<String> list = new ArrayList<String>();
		String result = null;
		try {
			List<CashBookPaymentDetail> details = findByPayement(cashBookDetail);
			Account account = new Account();
			for (CashBookPaymentDetail detail : details) {
				CashBookPaymentDetail cashBookDetails = new CashBookPaymentDetail();
				account = accountService.find(cashBookDetails.getReceivingAccount().getSysid());
				result = account.getACODE();

			}


		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return result;
	}

}
