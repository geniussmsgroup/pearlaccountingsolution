package com.pearl.accounting.syscore.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.ProductSetupDao;
import com.pearl.accounting.syscore.services.ProductSetupService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.sysmodel.ProductSetup;
import com.pearl.accounting.sysmodel.SwizFeedback;
@Service("ProductSetupService")
@Transactional
public class ProductSetupServiceImpl implements ProductSetupService {

	@Autowired
	private ProductSetupDao productsetupDao;
	
	@Autowired
	private SystemUserService systemUserService;

	public SwizFeedback save(ProductSetup productsetup) {
		try {
			productsetupDao.save(productsetup);
			return new SwizFeedback(true, "Saved successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}

	}

	public SwizFeedback update(ProductSetup productsetup) {
		try {
			productsetupDao.update(productsetup);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}
	}

	public SwizFeedback delete(ProductSetup productsetup) {
		try {
			productsetupDao.delete(productsetup);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}
	}

	public List<ProductSetup> find() {
		try {

			return productsetupDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public ProductSetup find(String id) {
		try {

			return productsetupDao.find(id);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}
	
	public ProductSetup findByName(String productDetails) {
		try {
			
			Search search=new Search();
			search.addFilterILike("productDetails", productDetails); 
			return productsetupDao.searchUnique(search);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

}
