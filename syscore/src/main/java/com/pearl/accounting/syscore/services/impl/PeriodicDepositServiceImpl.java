package com.pearl.accounting.syscore.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.PeriodicDepositDao;
import com.pearl.accounting.syscore.services.AccountService;
import com.pearl.accounting.syscore.services.ClientStatementService;
import com.pearl.accounting.syscore.services.CustomerAccountService;
import com.pearl.accounting.syscore.services.GeneralLedgerService;
import com.pearl.accounting.syscore.services.PeriodicDepositService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.syscore.services.TheadpService;
import com.pearl.accounting.syscore.services.TransMLService;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.ClientStatement;
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.DataSourceScreen;
import com.pearl.accounting.sysmodel.Fsave;
import com.pearl.accounting.sysmodel.GeneralLedger;
import com.pearl.accounting.sysmodel.PeriodicDeposit;
import com.pearl.accounting.sysmodel.ProductType;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.SystemUser;
import com.pearl.accounting.sysmodel.TransGL;
import com.pearl.accounting.sysmodel.TransML;
import com.pearl.accounting.sysmodel.Ttype;

@Service("PeriodicDepositService")
@Transactional
public class PeriodicDepositServiceImpl implements PeriodicDepositService {

	@Autowired
	private PeriodicDepositDao periodicDepositDao;

	@Autowired
	private SystemUserService systemUserService;

	@Autowired
	private GeneralLedgerService generalLedgerService;
	@Autowired
	private TransMLService transmlService;
	@Autowired
	private TheadpService theadpService;
	@Autowired
	private AccountService accountService;
	@Autowired
	private CustomerAccountService customeraccountService;

	@Autowired
	private ClientStatementService clientStatementService;

	public SwizFeedback save(PeriodicDeposit deposit) {
		try {
            deposit.setThead(theadpService.getthead());
			deposit.setCreatedBy(systemUserService.getLoggedInUser());
			deposit.setUpdatedBy(systemUserService.getLoggedInUser());

			Account payingAccount = accountService.findByName(ProductType.Savings.getProductType());

			deposit.setPayingAccount(payingAccount);

			PeriodicDeposit periodicDeposit = periodicDepositDao.save(deposit);

			if (periodicDeposit != null) {

				GeneralLedger generalLedger = generateGeneralLedger(periodicDeposit);

				ClientStatement clientStatement = generateClientStatement(periodicDeposit);
                    TransML      transml   =generatememberstmc(periodicDeposit);
				GeneralLedger generalLedger2 = generateGeneralLedgerCr(periodicDeposit, payingAccount);
                transmlService.save(transml);
				generalLedgerService.save(generalLedger);
				generalLedgerService.save(generalLedger2);

				clientStatementService.save(clientStatement);

				return new SwizFeedback(true, "Payment posted successfully");

			} else {

				return new SwizFeedback(false, "Deposit not posted successfully. Please try again.");
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	// credit the savings A/C in the general ledger
	private GeneralLedger generateGeneralLedgerCr(PeriodicDeposit deposit, Account loanAccount) {
		try {

			if (loanAccount != null) {
				GeneralLedger generalLedger = new GeneralLedger();
				generalLedger.setAccount(loanAccount);
				generalLedger.setPeriod(deposit.getPeriod());
				generalLedger.setBalanceCredit(0);
				generalLedger.setBalanceDebt(0);
				generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
				generalLedger.setDateCreated(new Date());
				generalLedger.setDateUpdated(new Date());
				generalLedger.setYear(deposit.getYear());
				generalLedger.setPaymentDetails(deposit.getPaymentDetails());
				generalLedger.setPostReferenceNo(deposit.getId());
				generalLedger.setDataSourceScreen(DataSourceScreen.Savings);
				generalLedger.setRemarks(ProductType.Savings.getProductType() + ":" + deposit.getRemarks());
				generalLedger.setStatus(Status.ACTIVE);
				generalLedger.setTransactionCredit(Float.parseFloat(deposit.getDepositedAmount()));
				generalLedger.setTransactionDebt(0);
				generalLedger.setTransactionDescription(ProductType.Savings.getProductType());
				generalLedger.setTtype(Ttype.C);
				generalLedger.setTransationDate(deposit.getDepositDate());
				generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
				return generalLedger;
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}
	private TransGL generateGLc(PeriodicDeposit payment,Account account) {
		TransGL gl = new TransGL();
		gl.setDateCreated(new Date());
		gl.setDateUpdated(new Date());
		gl.setDataSourceScreen(DataSourceScreen.Savings);
		gl.setCreatedBy(systemUserService.getLoggedInUser());
		gl.setUpdatedBy(systemUserService.getLoggedInUser());
		gl.setStatus(Status.ACTIVE);
		gl.setACODE(receive(payment));
		gl.setFsave(Fsave.Y);
		gl.setAMOUNT(Float.parseFloat(payment.getDepositedAmount()));
		gl.setAmtUsd(0);
		gl.setRecAmt(0);
		gl.setTDATE(payment.getDepositDate());
		gl.setTTYPE(Ttype.C);
		gl.setPAYDET(payment.getPaymentDetails());
		gl.setREMARKS(payment.getRemarks());
		gl.setICODE(payment.getPeriodcode());
		gl.setYRCODE(payment.getYear());
		gl.setPAYPERSON(payment.getRemarks());
		gl.setMemberID(customer(payment));
		gl.setTheadID(payment.getThead());
		// gl.setJNLNO(0);
		gl.setYrRec(0);
		gl.setTrec(0);
		gl.setAccount(account);
		SystemUser staff = systemUserService.find(payment.getUpdatedBy().getId());
		String name = staff.getUsername();
		gl.setStaffName(name);
		if (payment.getCustomerAccount() != null) {
	
			gl.setCustomeraccount(payment.getCustomerAccount());
		}
	
		return gl;
	}
	///////////////////////////////////debit A/c
	private TransGL generateGLc(PeriodicDeposit payment) {
		TransGL gl = new TransGL();
		gl.setDateCreated(new Date());
		gl.setDateUpdated(new Date());
		gl.setDataSourceScreen(DataSourceScreen.Savings);
		gl.setCreatedBy(systemUserService.getLoggedInUser());
		gl.setUpdatedBy(systemUserService.getLoggedInUser());
		gl.setStatus(Status.ACTIVE);
		gl.setACODE(receive(payment));
		gl.setFsave(Fsave.Y);
		gl.setAMOUNT(Float.parseFloat(payment.getDepositedAmount()));
		gl.setAmtUsd(0);
		gl.setRecAmt(0);
		gl.setTDATE(payment.getDepositDate());
		gl.setTTYPE(Ttype.D);
		gl.setPAYDET(payment.getPaymentDetails());
		gl.setREMARKS(payment.getRemarks());
		gl.setICODE(payment.getPeriodcode());
		gl.setYRCODE(payment.getYear());
		gl.setPAYPERSON(payment.getRemarks());
		gl.setMemberID(customer(payment));
		gl.setTheadID(payment.getThead());
		// gl.setJNLNO(0);
		gl.setYrRec(0);
		gl.setTrec(0);
		gl.setAccount(payment.getReceiveingAccount());
		SystemUser staff = systemUserService.find(payment.getUpdatedBy().getId());
		String name = staff.getUsername();
		gl.setStaffName(name);
		if (payment.getCustomerAccount() != null) {
	
			gl.setCustomeraccount(payment.getCustomerAccount());
		}
	
		return gl;
	}

	// debit the receiveing account in the general ledger
	private GeneralLedger generateGeneralLedger(PeriodicDeposit deposit) {

		GeneralLedger generalLedger = new GeneralLedger();

		generalLedger.setAccount(deposit.getReceiveingAccount());
		generalLedger.setPeriod(deposit.getPeriod());
		generalLedger.setBalanceCredit(0);
		generalLedger.setBalanceDebt(0);
		generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
		generalLedger.setDateCreated(new Date());
		generalLedger.setDateUpdated(new Date());
		generalLedger.setYear(deposit.getYear());
		generalLedger.setPaymentDetails(deposit.getPaymentDetails());
		generalLedger.setPostReferenceNo(deposit.getId());
		generalLedger.setDataSourceScreen(DataSourceScreen.Savings);
		generalLedger.setRemarks(ProductType.Savings.getProductType() + " :" + deposit.getRemarks());
		generalLedger.setStatus(Status.ACTIVE);
		generalLedger.setTransactionCredit(0);
		generalLedger.setTransactionDebt(Float.parseFloat(deposit.getDepositedAmount()));
		generalLedger.setTransactionDescription(ProductType.Savings.getProductType());
		generalLedger.setTtype(Ttype.D);
		generalLedger.setTransationDate(deposit.getDepositDate());
		generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());

		return generalLedger;
	}

	private ClientStatement generateClientStatement(PeriodicDeposit deposit) {
		//Account account =
		ClientStatement clientStatement = new ClientStatement();

		System.out.println("PeriodicDeposit ClientId:==> " + deposit.getCustomerAccount().getSysid());

		clientStatement.setCustomerAccount(deposit.getCustomerAccount());

		clientStatement.setPeriod(deposit.getPeriod());
		clientStatement.setBalanceCredit(0);
		clientStatement.setBalanceDebt(0);
		clientStatement.setCreatedBy(systemUserService.getLoggedInUser());
		clientStatement.setDateCreated(new Date());
		clientStatement.setDateUpdated(new Date());
		clientStatement.setYear(deposit.getYear());
		clientStatement.setPaymentDetails(deposit.getPaymentDetails());
		clientStatement.setPostReferenceNo(deposit.getId());
		//clientStatement.setProductType(ProductType.Savings);
		clientStatement.setRemarks(ProductType.Savings.getProductType() + " :" + deposit.getRemarks());
		clientStatement.setStatus(Status.ACTIVE);
		clientStatement.setTransactionCredit(Float.parseFloat(deposit.getDepositedAmount()));
		clientStatement.setTransactionDebt(0);
		clientStatement.setTransactionDescription(ProductType.Savings.getProductType());
		clientStatement.setTtype(Ttype.C);
		clientStatement.setTransationDate(deposit.getDepositDate());
		clientStatement.setUpdatedBy(systemUserService.getLoggedInUser());

		return clientStatement;

	}
	private TransML generatememberstmc(PeriodicDeposit payment) {
	TransML gl = new TransML();
	gl.setCreatedBy(systemUserService.getLoggedInUser());
	gl.setUpdatedBy(systemUserService.getLoggedInUser());
	gl.setDateCreated(new Date());
	gl.setDateUpdated(new Date());
	gl.setStatus(Status.ACTIVE);
	gl.setDataSourceScreen(DataSourceScreen.Savings);
	gl.setMEMBERID(payment.getAcctNo());
	gl.setCustomeraccount(payment.getCustomerAccount());
	
	gl.setFSAVE(Fsave.Y);
//	gl.setPRODUCTID(generateacodeproducttype(payment));
	//int loanid = Integer.parseInt(payment.getLoanNumber());

	gl.setLOANID(0);
	gl.setTDATE(payment.getDepositDate());
	gl.setTTYPE(Ttype.C);
	gl.setAMOUNT(Float.parseFloat(payment.getDepositedAmount()));
	gl.setPAYDET(payment.getRemarks());
	gl.setVNO(payment.getPaymentDetails());
	//gl.setJNLID(0);
	gl.setTHEADID(payment.getThead());
	

	SystemUser staff = systemUserService.find(payment.getUpdatedBy().getId());
	String name = staff.getUsername();
	gl.setSTAFFNAME(name);
	//gl.setCustomeraccount(payment.getCustomerAccount());

	return gl;
}

	public SwizFeedback update(PeriodicDeposit deposit) {
		try {
			periodicDepositDao.update(deposit);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public SwizFeedback delete(PeriodicDeposit deposit) {
		try {
			periodicDepositDao.delete(deposit);
			return new SwizFeedback(true, "Deleted successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	public List<PeriodicDeposit> find() {
		try {
			return periodicDepositDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	public PeriodicDeposit find(String id) {
		try {
			return periodicDepositDao.find(id);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	public float getTotalDeposits(CustomerAccount Sysid) {
		float total = 0;
		try {
			
			Search s= new Search();
			s.addFilterEqual("customerAccount", Sysid);
			List<PeriodicDeposit> list = periodicDepositDao.search(s);
			for(PeriodicDeposit deposit:list) {
				total+=Float.parseFloat(deposit.getDepositedAmount());
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return total;

	}
	private String receive(PeriodicDeposit deposit) {
		String ans = null;
		
		try {
	Account account = accountService.find(deposit.getReceiveingAccount().getSysid());
         ans = account.getACODE();
		}catch(Exception ex) {
			System.out.println(ex.getMessage());

	}return ans;
	}
private String pay(PeriodicDeposit deposit) {
	String ans = null;
	try {
		Account account = accountService.find(deposit.getPayingAccount().getSysid());
        ans = account.getACODE();

		
	}catch(Exception ex) {
		System.out.println(ex.getMessage());
	}
	return ans;}
private String customer(PeriodicDeposit deposit) {
	String ans = null;
	try {
		CustomerAccount account = customeraccountService.find(deposit.getCustomerAccount().getSysid());
        ans = account.getClientCode();

		
	}catch(Exception ex) {
		System.out.println(ex.getMessage());
	}
	return ans;}




}
