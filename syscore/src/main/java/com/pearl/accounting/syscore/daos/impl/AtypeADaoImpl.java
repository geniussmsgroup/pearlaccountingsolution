package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.AccountDao;
import com.pearl.accounting.syscore.daos.AtypeADao;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.AtypeA;
@Repository("AtypeADao")

public class AtypeADaoImpl extends SwiziBaseDaoImpl<AtypeA> implements AtypeADao {


}
