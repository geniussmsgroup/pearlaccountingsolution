package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.CashBookReceiptDao;
import com.pearl.accounting.sysmodel.CashBookReceipt;

@Repository("CashBookReceiptDao")
public class CashBookReceiptDaoImpl extends SwiziBaseDaoImpl<CashBookReceipt> implements CashBookReceiptDao {

}
