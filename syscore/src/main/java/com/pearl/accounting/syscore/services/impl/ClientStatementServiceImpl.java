package com.pearl.accounting.syscore.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pearl.accounting.syscore.daos.ClientStatementDao;
import com.pearl.accounting.syscore.services.ClientStatementService;
import com.pearl.accounting.sysmodel.ClientStatement;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.TransGL;

@Service("ClientStatementService")
@Transactional
public class ClientStatementServiceImpl implements ClientStatementService {

	@Autowired
	private ClientStatementDao clientStatementDao;

	public SwizFeedback save(ClientStatement clientStatement) {
		try {
			clientStatementDao.save(clientStatement);
			return new SwizFeedback(true, "Saved successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null; 
	}
	
	public SwizFeedback save(List<ClientStatement> clientStatements) {
		try {
			ClientStatement[] list=new ClientStatement[clientStatements.size()];
			clientStatementDao.save(clientStatements.toArray(list));
			System.out.println("client statement saved successfully");
			return new SwizFeedback(true, "Saved successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	public SwizFeedback update(ClientStatement clientStatement) {

		try {
			clientStatementDao.update(clientStatement);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	public SwizFeedback delete(ClientStatement clientStatement) {
		try {
			clientStatementDao.delete(clientStatement);
			return new SwizFeedback(true, "Deleted successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	public List<ClientStatement> find() {
		try {
			return clientStatementDao.find(); 
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	public ClientStatement find(String id) {
		try {
			return clientStatementDao.find(id); 
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}
	


}
