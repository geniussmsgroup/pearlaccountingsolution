package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.TheadpDao;
import com.pearl.accounting.sysmodel.Theadp;

@Repository("TheadpDao")
public class TheadpDaoImpl extends SwiziBaseDaoImpl<Theadp> implements TheadpDao {

}
