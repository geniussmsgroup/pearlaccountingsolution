package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.ClientStatementDao;
import com.pearl.accounting.sysmodel.ClientStatement;

@Repository("ClientStatementDao")
public class ClientStatementDaoImpl extends SwiziBaseDaoImpl<ClientStatement> implements ClientStatementDao {

}
