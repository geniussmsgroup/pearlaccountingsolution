package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.FinancialYear;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface FinancialYearService {

	public SwizFeedback save(FinancialYear financialYear);

	public SwizFeedback update(FinancialYear financialYear);

	public SwizFeedback delete(FinancialYear financialYear);

	public List<FinancialYear> find();

	public FinancialYear find(String id);

	public SwizFeedback deactivate(FinancialYear financialYear);

	public SwizFeedback activate(FinancialYear financialYear);
	
	public FinancialYear findActive();

}
