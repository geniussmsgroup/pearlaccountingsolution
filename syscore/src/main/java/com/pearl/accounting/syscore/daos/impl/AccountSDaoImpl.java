package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.AccountSDao;
import com.pearl.accounting.sysmodel.Init;

@Repository("AccountSDao")
public class AccountSDaoImpl extends SwiziBaseDaoImpl<Init> implements AccountSDao {

}
