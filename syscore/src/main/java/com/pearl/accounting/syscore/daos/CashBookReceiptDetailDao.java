package com.pearl.accounting.syscore.daos;

import com.pearl.accounting.sysmodel.CashBookReceiptDetail;

public interface CashBookReceiptDetailDao extends SwiziBaseDao<CashBookReceiptDetail>{

}
