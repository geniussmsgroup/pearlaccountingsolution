package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.Loantest;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface LoantestService {
	public SwizFeedback save(Loantest loan);

	public SwizFeedback update(Loantest loan);

	public SwizFeedback delete(Loantest loan);

	public List<Loantest> find();

	public Loantest find(String LOANID);
	public Loantest findBy(CustomerAccount customerAccount);

  
}
