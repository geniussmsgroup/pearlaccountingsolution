package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.IntervaltbDao;
import com.pearl.accounting.sysmodel.Intervaltb;

@Repository("IntervaltbDao")
public class IntervaltbDaoImpl extends SwiziBaseDaoImpl<Intervaltb> implements IntervaltbDao {

}
