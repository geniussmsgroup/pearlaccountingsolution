package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.AssessmentPeriod;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface AssessmentPeriodService {

	public SwizFeedback save(AssessmentPeriod assessmentPeriod);

	public SwizFeedback update(AssessmentPeriod assessmentPeriod);

	public SwizFeedback delete(AssessmentPeriod assessmentPeriod);

	public List<AssessmentPeriod> find();

	public AssessmentPeriod find(String id);

	public AssessmentPeriod findActive();

	public SwizFeedback deactivate(AssessmentPeriod assessmentPeriod);

	public SwizFeedback activate(AssessmentPeriod assessmentPeriod);
	
	public AssessmentPeriod findByCode(String code);

}
