package com.pearl.accounting.syscore.services.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.LoanDepositDao;
import com.pearl.accounting.syscore.services.AccountService;
import com.pearl.accounting.syscore.services.ClientStatementService;
import com.pearl.accounting.syscore.services.CustomerAccountService;
import com.pearl.accounting.syscore.services.GeneralLedgerService;
import com.pearl.accounting.syscore.services.LoanDepositService;
import com.pearl.accounting.syscore.services.LoanService;
import com.pearl.accounting.syscore.services.LoaninterestViewService;
import com.pearl.accounting.syscore.services.ReceiptnoService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.syscore.services.TheadpService;
import com.pearl.accounting.syscore.services.TransGLService;
import com.pearl.accounting.syscore.services.TransMLService;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.ClientStatement;
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.DataSourceScreen;
import com.pearl.accounting.sysmodel.FixedData;
import com.pearl.accounting.sysmodel.Fsave;
import com.pearl.accounting.sysmodel.GeneralLedger;
import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysmodel.LoanDeposit;
import com.pearl.accounting.sysmodel.LoaninterestView;
import com.pearl.accounting.sysmodel.ProductType;
import com.pearl.accounting.sysmodel.Receiptno;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.SystemUser;
import com.pearl.accounting.sysmodel.Theadp;
import com.pearl.accounting.sysmodel.TransGL;
import com.pearl.accounting.sysmodel.TransML;
import com.pearl.accounting.sysmodel.Ttype;

@Service("LoanDepositService")
@Transactional
public class LoanDepositServiceImpl implements LoanDepositService {
	@Autowired
	private LoanDepositDao loandepositDao;
	@Autowired
	private LoanService loanService;
   @Autowired
   private LoaninterestViewService  loaninterestviewServce;
	@Autowired
	private SystemUserService systemUserService;

	@Autowired
	private GeneralLedgerService generalLedgerService;
	@Autowired
	private TransGLService transglService;
	@Autowired
	private TransMLService transmlService;

	@Autowired
	private AccountService accountService;
	@Autowired
	private CustomerAccountService customeraccountService;

	@Autowired
	private ClientStatementService clientStatementService;
	@Autowired
	private TheadpService theadpService;
	@Autowired
	private ReceiptnoService receiptnoService;

	public SwizFeedback save(LoanDeposit loandeposit) {
		try {
			///if(getmaxmember(loandeposit).getLoan()!=null) {
			///loandeposit.setLoanbalance(getloanbalance(loandeposit.getCustomeraccount()));
			loandeposit.setCreatedBy(systemUserService.getLoggedInUser());
			loandeposit.setUpdatedBy(systemUserService.getLoggedInUser());
			loandeposit.setThead(theadpService.getthead());
			///loandeposit.setLoan(getmaxmember(loandeposit).getLoan());
			System.out.println(" the loan is " +loandeposit.getLoan().getLOANID());
			
	LoanDeposit loanDeposits= loandepositDao.save(loandeposit);
			
			if (loanDeposits != null) {
				
				theadpService.save(Savetothead(loandeposit));
				generalLedgerService.save(generateGeneralLedgerDr(loanDeposits));
				transglService.save(generateGLD(loanDeposits));
				/// receiptnoService.save(generateReceiptno(loanDeposits));
				if (loanDeposits.getLoandeposit() > 0) {
					transglService.save(generateGLDloan(loanDeposits));
					// clientStatementService.save(generateClientStatementloan(loanDeposits));
					transmlService.save(generatememberstmcloan(loanDeposits));
					// return;
					generalLedgerService.save(generateGeneralLedgerloan(loanDeposits));
				}
				if (loanDeposits.getInterestdeposit() > 0) {
					transglService.save(generateGLDloaninterest(loanDeposits));
					transmlService.save(generatememberstmcloaninterest(loanDeposits));
					generalLedgerService.save(generateGeneralLedgerloaninterest(loanDeposits));
					generalLedgerService.save(getinterestincome(loandeposit));
					generalLedgerService.save(Creditingaccuredacct(loandeposit));
					transglService.save(getloanintincome((loanDeposits)));
					transglService.save(getcreditaccuredact(loanDeposits));

				}
//
//				// clientStatementService.save(generateClientStatementloaninterest(loanDeposits));
//
				return new SwizFeedback(true, "Saved successfully in the table");
//
			}
//			

			else {

				return new SwizFeedback(false, "Transaction not posted successfully. Please try again.");
			}
//}
			

//			else {
//
//				return new SwizFeedback(false, "The client has no loan id . Please try another one.");
//			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, ex.getMessage());
		}
	}

	private GeneralLedger generateGeneralLedgerDr(LoanDeposit loandeposit) {

		// try {

		GeneralLedger generalLedger = new GeneralLedger();
		generalLedger.setAccount(loandeposit.getAccount());
		generalLedger.setPeriod(loandeposit.getPeriod());
		generalLedger.setBalanceCredit(0);
		generalLedger.setBalanceDebt(0);
		generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
		generalLedger.setDateCreated(new Date());
		generalLedger.setDateUpdated(new Date());
		generalLedger.setPeriodcode(loandeposit.getPeriodcode());
		generalLedger.setYear(Integer.parseInt(loandeposit.getYear()));
		generalLedger.setPaymentDetails(loandeposit.getPaydetails());
		generalLedger.setPostReferenceNo(loandeposit.getId());
		generalLedger.setDataSourceScreen(DataSourceScreen.LoanDeposit);
		generalLedger.setRemarks(loandeposit.getRemarks());
		generalLedger.setStatus(Status.ACTIVE);
		generalLedger.setTransactionCredit(0);
		generalLedger.setTransactionDebt(loandeposit.getLoandeposit() + loandeposit.getInterestdeposit());
		// generalLedger.setTransactionDescription(ProductType.LNP.getProductType()+"
		// "+ProductType.LNP.getProductType());
		generalLedger.setTtype(Ttype.D);
		generalLedger.setTransationDate(loandeposit.getDepositdate());
		generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
		generalLedger.setThead(loandeposit.getThead());
		;

//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//		}
		return generalLedger;

	}

	// debit the receiveing account in the general ledger
	private GeneralLedger generateGeneralLedgerloan(LoanDeposit loandeposit) {

		GeneralLedger generalLedger = new GeneralLedger();

		Account account = accountService.find("7");
		generalLedger.setAccount(account);
		generalLedger.setPeriod(loandeposit.getPeriod());
		generalLedger.setBalanceCredit(0);
		generalLedger.setBalanceDebt(0);
		generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
		generalLedger.setDateCreated(new Date());
		generalLedger.setDateUpdated(new Date());
		generalLedger.setYear(Integer.parseInt(loandeposit.getYear()));
		generalLedger.setPaymentDetails(loandeposit.getPaydetails());
		generalLedger.setPostReferenceNo(loandeposit.getId());
		generalLedger.setDataSourceScreen(DataSourceScreen.LoanDeposit);
		generalLedger.setRemarks(loandeposit.getRemarks());
		generalLedger.setStatus(Status.ACTIVE);
		generalLedger.setTransactionCredit(loandeposit.getLoandeposit());
		generalLedger.setTransactionDebt(0);
		generalLedger.setTransactionDescription(ProductType.Loan.getProductType());
		generalLedger.setTtype(Ttype.C);
		generalLedger.setTransationDate(loandeposit.getDepositdate());
		generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
//		} else {
//			System.out.println("not paying interest");
//		}
		generalLedger.setThead(loandeposit.getThead());
		;
		return generalLedger;
	}

	private GeneralLedger generateGeneralLedgerloaninterest(LoanDeposit loandeposit) {

		GeneralLedger generalLedger = new GeneralLedger();
		// if (loandeposit.getInterestdeposit() > 0) {

		Account account = accountService.find("8");
		generalLedger.setAccount(account);
		generalLedger.setPeriod(loandeposit.getPeriod());
		generalLedger.setBalanceCredit(0);
		generalLedger.setBalanceDebt(0);
		generalLedger.setCreatedBy(systemUserService.getLoggedInUser());
		generalLedger.setDateCreated(new Date());
		generalLedger.setDateUpdated(new Date());
		generalLedger.setYear(Integer.parseInt(loandeposit.getYear()));
		generalLedger.setPaymentDetails(loandeposit.getPaydetails());
		generalLedger.setPostReferenceNo(loandeposit.getId());
		generalLedger.setDataSourceScreen(DataSourceScreen.LoanDeposit);
		generalLedger.setRemarks(loandeposit.getRemarks());
		generalLedger.setStatus(Status.ACTIVE);
		generalLedger.setTransactionCredit(loandeposit.getInterestdeposit());
		generalLedger.setTransactionDebt(0);
		generalLedger.setTransactionDescription(ProductType.LoanInterest.getProductType());
		generalLedger.setTtype(Ttype.C);
		generalLedger.setPeriodcode(loandeposit.getPeriodcode());
		generalLedger.setLoanid(loandeposit.getLoan());
		generalLedger.setTransationDate(loandeposit.getDepositdate());
		generalLedger.setUpdatedBy(systemUserService.getLoggedInUser());
//		} else {
//			System.out.println("not paying interest");
//		}
		generalLedger.setThead(loandeposit.getThead());
		;
		return generalLedger;
	}

	private ClientStatement generateClientStatementloan(LoanDeposit loandeposit) {
		Account loans = accountService.find("7");

		ClientStatement clientStatement = new ClientStatement();

		// Loan loan = loanService.find(loandeposit.getLoan().getLOANID());

		clientStatement.setCustomerAccount(loandeposit.getCustomeraccount());
		clientStatement.setPeriod(loandeposit.getPeriod());
		clientStatement.setBalanceCredit(0);
		clientStatement.setBalanceDebt(0);
		clientStatement.setCreatedBy(systemUserService.getLoggedInUser());
		clientStatement.setDateCreated(new Date());
		clientStatement.setDateUpdated(new Date());
		clientStatement.setYear(Integer.parseInt(loandeposit.getYear()));
		clientStatement.setPaymentDetails(loandeposit.getPaydetails());
		clientStatement.setPostReferenceNo(loandeposit.getId());
		clientStatement.setProductType(loans.getProductType().getProductId());
		clientStatement.setRemarks(loandeposit.getRemarks());
		clientStatement.setStatus(Status.ACTIVE);
		clientStatement.setTransactionCredit(loandeposit.getLoandeposit());
		clientStatement.setTransactionDebt(0);
		clientStatement.setTransactionDescription(ProductType.Loan.getProductType());
		clientStatement.setTtype(Ttype.C);
		clientStatement.setTransationDate(new Date());
		clientStatement.setUpdatedBy(systemUserService.getLoggedInUser());
		clientStatement.setThead(loandeposit.getThead());
		return clientStatement;
	}

	private ClientStatement generateClientStatementloaninterest(LoanDeposit loandeposit) {
		Account loans = accountService.find("8");

		ClientStatement clientStatement = new ClientStatement();

//		if (loandeposit != null) {
//
//			// if (loandeposit.getInterestdeposit() > 0) {
//
//			Loan loan = loanService.find(loandeposit.getLoan().getLOANID());
//
//			if (loan != null) {
//				System.out.println("ClientStatement:==> " + loan.getCustomerAccount().getSysid());

		clientStatement.setCustomerAccount(loandeposit.getCustomeraccount());
		clientStatement.setPeriod(loandeposit.getPeriod());
		clientStatement.setBalanceCredit(0);
		clientStatement.setBalanceDebt(0);
		clientStatement.setCreatedBy(systemUserService.getLoggedInUser());
		clientStatement.setDateCreated(new Date());
		clientStatement.setDateUpdated(new Date());
		clientStatement.setYear(Integer.parseInt(loandeposit.getYear()));
		clientStatement.setPaymentDetails(loandeposit.getPaydetails());
		clientStatement.setPostReferenceNo(loandeposit.getId());
		clientStatement.setProductType(loans.getProductType().getProductId());
		clientStatement.setRemarks(loandeposit.getRemarks());
		clientStatement.setStatus(Status.ACTIVE);
		clientStatement.setTransactionCredit(loandeposit.getInterestdeposit());
		clientStatement.setTransactionDebt(0);
		clientStatement.setTransactionDescription(ProductType.LoanInterest.getProductType());
		clientStatement.setTtype(Ttype.C);
		clientStatement.setTransationDate(new Date());
		clientStatement.setUpdatedBy(systemUserService.getLoggedInUser());
		clientStatement.setThead(loandeposit.getThead());

		return clientStatement;
	}

	private TransML generatememberstmcloan(LoanDeposit loandeposit) {
		TransML gl = new TransML();

//		if (loandeposit.getLoandeposit() > 0) {
		gl.setCreatedBy(systemUserService.getLoggedInUser());
		gl.setUpdatedBy(systemUserService.getLoggedInUser());
		gl.setDateCreated(new Date());
		gl.setDateUpdated(new Date());
		gl.setStatus(Status.ACTIVE);
		gl.setDataSourceScreen(DataSourceScreen.Loan);
		CustomerAccount account = customeraccountService.find(loandeposit.getCustomeraccount().getSysid());
		gl.setMEMBERID(account.getClientCode());
		gl.setCustomeraccount(loandeposit.getCustomeraccount());
		gl.setFSAVE(Fsave.Y);
//	gl.setPRODUCTID(generateacodeproducttype(payment));

		gl.setLOANID(Integer.parseInt(loandeposit.getLoan().getLOANID()));
		gl.setTDATE(loandeposit.getDepositdate());
		gl.setTTYPE(Ttype.C);
		gl.setAMOUNT(loandeposit.getLoandeposit());
		gl.setPAYDET(loandeposit.getPaydetails() + loandeposit.getRemarks());
		gl.setVNO(loandeposit.getPaydetails());
		// gl.setJNLID(0);
		gl.setTHEADID(loandeposit.getThead());
		// gl.setLoan(payment.getLoanid());
		gl.setPRODUCTID("LNP");
		SystemUser staff = systemUserService.find(loandeposit.getUpdatedBy().getId());
		String name = staff.getUsername();
		gl.setSTAFFNAME(name);
		gl.setCustomeraccount(loandeposit.getCustomeraccount());

//		} else {
//			System.out.println("not paying loan deposit");
//		}
		return gl;
	}

	private TransML generatememberstmcloaninterest(LoanDeposit loandeposit) {
		TransML gl = new TransML();
		// if (loandeposit.getInterestdeposit() > 0) {
		gl.setCreatedBy(systemUserService.getLoggedInUser());
		gl.setUpdatedBy(systemUserService.getLoggedInUser());
		gl.setDateCreated(new Date());
		gl.setDateUpdated(new Date());
		gl.setStatus(Status.ACTIVE);
		gl.setDataSourceScreen(DataSourceScreen.LoanInterest);
		CustomerAccount account = customeraccountService.find(loandeposit.getCustomeraccount().getSysid());
		gl.setMEMBERID(account.getClientCode());
		gl.setCustomeraccount(loandeposit.getCustomeraccount());
		gl.setFSAVE(Fsave.Y);
//	gl.setPRODUCTID(generateacodeproducttype(payment));

		gl.setLOANID(Integer.parseInt(loandeposit.getLoan().getLOANID()));
		gl.setTDATE(loandeposit.getDepositdate());
		gl.setTTYPE(Ttype.C);
		gl.setAMOUNT(loandeposit.getInterestdeposit());
		gl.setPAYDET(loandeposit.getPaydetails() + loandeposit.getRemarks());
		gl.setVNO(loandeposit.getPaydetails());
		// gl.setJNLID(0);
		gl.setTHEADID(loandeposit.getThead());
		// gl.setLoan(payment.getLoanid());
		gl.setPRODUCTID("LNIP");
		SystemUser staff = systemUserService.find(loandeposit.getUpdatedBy().getId());
		String name = staff.getUsername();
		gl.setSTAFFNAME(name);
		gl.setCustomeraccount(loandeposit.getCustomeraccount());
//		} else {
//			System.out.println("not paying interest");
//		}
		return gl;
	}

	private TransGL generateGLD(LoanDeposit loandeposit) {
		TransGL gl = new TransGL();
		Account account = accountService.find(loandeposit.getAccount().getSysid());
		gl.setACODE(account.getACODE());
		gl.setDateCreated(new Date());
		gl.setDateUpdated(new Date());
		gl.setDataSourceScreen(DataSourceScreen.LoanDeposit);
		gl.setAccount(loandeposit.getAccount());
		gl.setAMOUNT(loandeposit.getLoandeposit() + loandeposit.getInterestdeposit());
		gl.setTheadID(loandeposit.getThead());
		gl.setTDATE(loandeposit.getDepositdate());
		gl.setTTYPE(Ttype.D);
		gl.setPAYDET(loandeposit.getPaydetails());
		gl.setREMARKS(loandeposit.getRemarks());
		gl.setFsave(Fsave.Y);
		gl.setCustomeraccount(loandeposit.getCustomeraccount());
		gl.setRDATE(new Date());
		gl.setYRCODE(Integer.parseInt(loandeposit.getYear()));
		gl.setICODE(loandeposit.getPeriodcode());
		gl.setYRCODE(0);
		gl.setTrecN("0");
		gl.setTrecN("0");
		CustomerAccount actno = customeraccountService.find(loandeposit.getCustomeraccount().getSysid());
		gl.setMemberID(actno.getClientCode());

		gl.setICODE(loandeposit.getPeriodcode());
		return gl;
	}

//////////////crediting Loan Interest Receivable
	private TransGL generateGLDloaninterest(LoanDeposit loandeposit) {
		TransGL gl = new TransGL();
		// if (loandeposit.getInterestdeposit() > 0) {

		Account account = accountService.find("8");
		gl.setACODE(account.getACODE());

		gl.setDateCreated(new Date());
		gl.setDateUpdated(new Date());
		gl.setDataSourceScreen(DataSourceScreen.LoanDeposit);
		gl.setAccount(account);
		gl.setAMOUNT(loandeposit.getInterestdeposit());
		gl.setTheadID(loandeposit.getThead());
		gl.setTDATE(loandeposit.getDepositdate());
		gl.setTTYPE(Ttype.C);
		gl.setPAYDET(loandeposit.getPaydetails());
		gl.setREMARKS(loandeposit.getRemarks());
		gl.setFsave(Fsave.Y);
		gl.setCustomeraccount(loandeposit.getCustomeraccount());
		gl.setRDATE(new Date());
		gl.setYRCODE(Integer.parseInt(loandeposit.getYear()));
		gl.setICODE(loandeposit.getPeriodcode());
		gl.setYRCODE(0);
		gl.setTrecN("0");
		gl.setTrecN("0");
		CustomerAccount actno = customeraccountService.find(loandeposit.getCustomeraccount().getSysid());
		gl.setMemberID(actno.getClientCode());

		gl.setICODE(loandeposit.getPeriodcode());
//		} else {
//			System.out.println("not paying interest");
//		}
		return gl;
	}

/////////////////crediting loan
	private TransGL generateGLDloan(LoanDeposit loandeposit) {
		TransGL gl = new TransGL();
//		if (loandeposit.getLoandeposit() > 0) {

		Account account = accountService.find("7");
		gl.setACODE(account.getACODE());

		gl.setDateCreated(new Date());
		gl.setDateUpdated(new Date());
		gl.setDataSourceScreen(DataSourceScreen.LoanDeposit);
		gl.setAccount(account);
		gl.setAMOUNT(loandeposit.getLoandeposit());
		gl.setTheadID(loandeposit.getThead());
		gl.setTDATE(loandeposit.getDepositdate());
		gl.setTTYPE(Ttype.C);
		gl.setPAYDET(loandeposit.getPaydetails());
		gl.setREMARKS(loandeposit.getRemarks());
		gl.setFsave(Fsave.Y);
		gl.setCustomeraccount(loandeposit.getCustomeraccount());
		gl.setRDATE(new Date());
		gl.setYRCODE(Integer.parseInt(loandeposit.getYear()));
		gl.setICODE(loandeposit.getPeriodcode());
		gl.setYRCODE(0);
		gl.setTrecN("0");
		gl.setTrecN("0");
		CustomerAccount actno = customeraccountService.find(loandeposit.getCustomeraccount().getSysid());
		gl.setMemberID(actno.getClientCode());

		gl.setICODE(loandeposit.getPeriodcode());
//		} else {
//			System.out.println("not paying loan");
//		}
		return gl;
	}

	//////////// crediting the accured interest
	private TransGL getcreditaccuredact(LoanDeposit loandeposit) {
		TransGL gl = new TransGL();
		// if (loandeposit.getInterestdeposit() > 0) {

		Account account = accountService.find("31");
		gl.setACODE(account.getACODE());

		gl.setDateCreated(new Date());
		gl.setDateUpdated(new Date());
		gl.setDataSourceScreen(DataSourceScreen.LoanDeposit);
		gl.setAccount(account);
		gl.setAMOUNT(loandeposit.getInterestdeposit());
		gl.setTheadID(loandeposit.getThead());
		gl.setTDATE(loandeposit.getDepositdate());
		gl.setTTYPE(Ttype.C);
		gl.setPAYDET(loandeposit.getPaydetails());
		gl.setREMARKS(loandeposit.getRemarks());
		gl.setFsave(Fsave.Y);
		gl.setCustomeraccount(loandeposit.getCustomeraccount());
		gl.setRDATE(new Date());
		gl.setYRCODE(Integer.parseInt(loandeposit.getYear()));
		gl.setICODE(loandeposit.getPeriodcode());
		gl.setYRCODE(0);
		gl.setTrecN("0");
		gl.setTrecN("0");
		CustomerAccount actno = customeraccountService.find(loandeposit.getCustomeraccount().getSysid());
		gl.setMemberID(actno.getClientCode());

		gl.setICODE(loandeposit.getPeriodcode());
//		} else {
//			System.out.println("not paying interest");
//		}
		return gl;
	}

	////// crediting the loan interest incoome
	private TransGL getloanintincome(LoanDeposit loandeposit) {
		TransGL gl = new TransGL();
		// if (loandeposit.getInterestdeposit() > 0) {

		Account account = accountService.find("47");
		gl.setACODE(account.getACODE());

		gl.setDateCreated(new Date());
		gl.setDateUpdated(new Date());
		gl.setDataSourceScreen(DataSourceScreen.LoanDeposit);
		gl.setAccount(account);
		gl.setAMOUNT(loandeposit.getInterestdeposit());
		gl.setTheadID(loandeposit.getThead());
		gl.setTDATE(loandeposit.getDepositdate());
		gl.setTTYPE(Ttype.C);
		gl.setPAYDET(loandeposit.getPaydetails());
		gl.setREMARKS(loandeposit.getRemarks());
		gl.setFsave(Fsave.Y);
		gl.setCustomeraccount(loandeposit.getCustomeraccount());
		gl.setRDATE(new Date());
		gl.setYRCODE(Integer.parseInt(loandeposit.getYear()));
		gl.setICODE(loandeposit.getPeriodcode());
		gl.setYRCODE(0);
		gl.setTrecN("0");
		gl.setTrecN("0");
		CustomerAccount actno = customeraccountService.find(loandeposit.getCustomeraccount().getSysid());
		gl.setMemberID(actno.getClientCode());

		gl.setICODE(loandeposit.getPeriodcode());
//		} else {
//			System.out.println("not paying interest");
//		}
		return gl;
	}
	private GeneralLedger getinterestincome(LoanDeposit loandeposit) {
		GeneralLedger generalledger = new GeneralLedger();
		Account account = accountService.find("47");
		generalledger.setAccount(account);
		generalledger.setPeriod(loandeposit.getPeriod());
		generalledger.setBalanceCredit(0);
		generalledger.setBalanceDebt(0);
		generalledger.setCreatedBy(systemUserService.getLoggedInUser());
		generalledger.setDateCreated(new Date());
		generalledger.setDateUpdated(new Date());
		generalledger.setYear(Integer.parseInt(loandeposit.getYear()));
		generalledger.setPaymentDetails(loandeposit.getPaydetails());
		generalledger.setPostReferenceNo(loandeposit.getId());
		generalledger.setDataSourceScreen(DataSourceScreen.LoanDeposit);
		generalledger.setRemarks(loandeposit.getRemarks());
		generalledger.setStatus(Status.ACTIVE);
		generalledger.setTransactionCredit(loandeposit.getInterestdeposit());
		generalledger.setTransactionDebt(0);
		generalledger.setTransactionDescription(ProductType.LoanInterest.getProductType());
		generalledger.setTtype(Ttype.C);
		generalledger.setPeriodcode(loandeposit.getPeriodcode());
		generalledger.setLoanid(loandeposit.getLoan());
		generalledger.setTransationDate(loandeposit.getDepositdate());
		generalledger.setUpdatedBy(systemUserService.getLoggedInUser());
//		} else {
//			System.out.println("not paying interest");
//		}
		generalledger.setThead(loandeposit.getThead());	
	return generalledger;
	}
	 private GeneralLedger Creditingaccuredacct(LoanDeposit loandeposit) {
			GeneralLedger generalledger = new GeneralLedger();
			Account account = accountService.find("31");
			generalledger.setAccount(account);
			generalledger.setPeriod(loandeposit.getPeriod());
			generalledger.setBalanceCredit(0);
			generalledger.setBalanceDebt(0);
			generalledger.setCreatedBy(systemUserService.getLoggedInUser());
			generalledger.setDateCreated(new Date());
			generalledger.setDateUpdated(new Date());
			generalledger.setYear(Integer.parseInt(loandeposit.getYear()));
			generalledger.setPaymentDetails(loandeposit.getPaydetails());
			generalledger.setPostReferenceNo(loandeposit.getId());
			generalledger.setDataSourceScreen(DataSourceScreen.LoanDeposit);
			generalledger.setRemarks(loandeposit.getRemarks());
			generalledger.setStatus(Status.ACTIVE);
			generalledger.setTransactionCredit(loandeposit.getInterestdeposit());
			generalledger.setTransactionDebt(0);
			generalledger.setTransactionDescription(ProductType.LoanInterest.getProductType());
			generalledger.setTtype(Ttype.C);
			generalledger.setPeriodcode(loandeposit.getPeriodcode());
			generalledger.setLoanid(loandeposit.getLoan());
			generalledger.setTransationDate(loandeposit.getDepositdate());
			generalledger.setUpdatedBy(systemUserService.getLoggedInUser());
//			} else {
//				System.out.println("not paying interest");
//			}
			generalledger.setThead(loandeposit.getThead());	
			return generalledger;
	 }

	private Theadp Savetothead(LoanDeposit loandeposit) {

		Theadp thead1 = new Theadp();

		thead1.setAmount(loandeposit.getInterestdeposit() + loandeposit.getLoandeposit());
		thead1.setCustomerAccount(loandeposit.getCustomeraccount());
		thead1.setTdates(loandeposit.getDepositdate());
		thead1.setAccount(loandeposit.getAccount());
		thead1.setVNO(loandeposit.getPaydetails());
		thead1.setRemarks(loandeposit.getRemarks());
		thead1.setDataSourceScreen(DataSourceScreen.LoanDeposit);
		thead1.setDateCreated(new Date());
		thead1.setDateUpdated(new Date());
		thead1.setPeriodcode(loandeposit.getPeriodcode());

		return thead1;
	}

	/////// receiptno

	public SwizFeedback update(LoanDeposit loandeposit) {
		try {
			loandepositDao.update(loandeposit);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}
	}

	public SwizFeedback delete(LoanDeposit loandeposit) {
		try {
			loandepositDao.delete(loandeposit);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}
	}

	public List<LoanDeposit> find() {
		try {

			return loandepositDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}
//	public List<LoanDeposit> getloan(LoanDeposit loandeposit) {
////		
//		List<LoanDeposit> loand=new ArrayList<LoanDeposit>();
//        LoanDeposit loanex =new  LoanDeposit();
//		List<Loan>  loans = loanService.findByMemberids(loandeposit.getCustomeraccount().getClientCode());
//        for(Loan loan : loans) {
//        	String LOANME= loan.getMEMBERID();
//        	if(loandeposit.getCustomeraccount().getClientCode()==LOANME) {
//        		loanex.setLoan(loan);
//        		
//        		
//        	}else {
//        		System.out.println("not equall at all ");
//        	}
//        }
//	loand.add(loanex);	
//		
		
		
		
		
		
		
		//		List<Loan> loanints = loanService.find();
//     System.out.println("the before");
//		for (Loan loanint : loanints) {
//			 if(loandeposit.getCustomeraccount()!=null) {
//				LoanDeposit loans2 = new LoanDeposit();
//				 Loan loancust = loanService.findByMemberid(loandeposit.getCustomeraccount().getClientCode());
//				//==loanint.getCustomerAccount()) {
//			String compare = loanint.getMEMBERID();
//				   String lonme=loancust.getMEMBERID();
//			if(compare==lonme) { 
//				///List<Loan> the =loanService.
//					loans2.setLoan(loancust);
//					loand.add(loans2);
//					loand.get(loand.size());
//					 
//				 }
//				 else {
//					 System.out.println("not the same");
//				 }
//			 
//			 }else {
//				 System.out.println("customer is null");
//			 }
//			
//		} 
//               		
//				return loand;
//				
//			}	
			
//		public LoanDeposit getmaxmember(LoanDeposit loandeposit) {
//			LoanDeposit loanr = new LoanDeposit();
//			List<LoanDeposit> loans = getloan(loandeposit);
//			for(LoanDeposit loanid : loans) {
//			     
//				
//				loanr.setLoan(loanid.getLoan());
//			}
//			
//			return loanr;
//		}
//			
//			
			
	
			
			//LoaninterestView memberid = loaninterestviewServce.find(loandeposit.getCustomeraccount().getClientCode());
//			String client = loandeposit.getCustomeraccount().getClientCode();
//			System.out.println(" the client is "+client);
////			
////			if(client==loanint.getMemberId()) { 	                      
//			Loan loans=  loanService.find(c);	
//		       loand.setLoan(loans);
//		       
//             }else {
//            	 System.out.println(" the meber is null no loan");
//             }
//		
//		}
		

	public LoanDeposit find(String id) {
		try {

			return loandepositDao.find(id);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public LoanDeposit findByName(String remarks) {
		try {

			Search search = new Search();
			search.addFilterILike("remarks", remarks);
			return loandepositDao.searchUnique(search);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

//	public float getloanbalance(CustomerAccount CustomerAccount) {
//		float total1 = 0;
//		float total = 0;
//		try {
//
//			Search s = new Search();
//			s.addFilterEqual("CustomerAccount", CustomerAccount);
//			List<LoanDeposit> list = loandepositDao.search(s);
//			for (LoanDeposit deposit : list) {
//				total += deposit.getLoandeposit();
//			}
//			Loan loan = loanService.findBy(CustomerAccount);
//			float totalloan = loan.getLoanAmount();
//			float balance = totalloan - total;
//			total1 += balance;
//		} catch (Exception ex) {
//			System.out.println(ex.getMessage());
//
//		}
//		return total1;
//
//	}
}
