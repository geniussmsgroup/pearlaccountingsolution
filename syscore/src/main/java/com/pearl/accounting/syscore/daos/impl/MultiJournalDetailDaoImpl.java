package com.pearl.accounting.syscore.daos.impl;

import org.springframework.stereotype.Repository;

import com.pearl.accounting.syscore.daos.MultiJournalDetailDao;
import com.pearl.accounting.sysmodel.MultiJournalDetail;

@Repository("MultiJournalDetailDao")
public class MultiJournalDetailDaoImpl extends SwiziBaseDaoImpl<MultiJournalDetail> implements MultiJournalDetailDao{

}
