package com.pearl.accounting.syscore.services.impl;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.CustomerAccountDao;
import com.pearl.accounting.syscore.services.CustomerAccountService;
import com.pearl.accounting.syscore.services.FinancialYearService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.FinancialYear;
import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysmodel.SwizFeedback;

@Service("CustomerAccountService")
@Transactional
public class CustomerAccountServiceImpl implements CustomerAccountService {

	@Autowired
	private CustomerAccountDao customerAccountDao;

	@Autowired
	private FinancialYearService financialYearService;

	@Autowired
	private SystemUserService systemUserService;

	public SwizFeedback save(CustomerAccount account) {
		try {

			account.setCreatedBy(systemUserService.getLoggedInUser());
			account.setUpdatedBy(systemUserService.getLoggedInUser());

			customerAccountDao.save(account);
			return new SwizFeedback(true, "Saved successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, "Error " + ex.getMessage());
		}

	}

	public SwizFeedback update(CustomerAccount account) {
		try {
			customerAccountDao.update(account);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, "Error " + ex.getMessage());
		}
	}

	public SwizFeedback delete(CustomerAccount account) {
		try {
			customerAccountDao.delete(account);
			return new SwizFeedback(true, "Deleted successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(false, "Error " + ex.getMessage());
		}
	}

	public List<CustomerAccount> find() {
		try {
			return customerAccountDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public CustomerAccount find(String Sysid) {
		try {
			return customerAccountDao.find(Sysid);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}
	public CustomerAccount findByName(String ClientCode) {
		try {

//			Search search = new Search();
//			search.addFilterILike("ClientCode", ClientCode);
			return customerAccountDao.find(ClientCode);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	private String generateACCNo() {
		String loanNo = null;
		try {

			int count = getAccCount() + 1;
			long n = count;
			int digits = 3;
			char[] zeros = new char[digits];
			Arrays.fill(zeros, '0');
			DecimalFormat df = new DecimalFormat(String.valueOf(zeros));

			FinancialYear financialYear = financialYearService.findActive();
			if (financialYear != null) {
				loanNo = financialYear.getCode() + "" + df.format(n);
			}

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return loanNo;
	}

	private int getAccCount() {
		int count = 0;
		try {

			List<CustomerAccount> loans = customerAccountDao.findAll();

			count = loans.size();

		} catch (Exception ex) {

		}

		return count;
	}

}
