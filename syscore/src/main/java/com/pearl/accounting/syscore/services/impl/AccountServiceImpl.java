package com.pearl.accounting.syscore.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.AccountDao;
import com.pearl.accounting.syscore.services.AccountService;
import com.pearl.accounting.syscore.services.AtypeAservice;
import com.pearl.accounting.syscore.services.ProductSetupService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.AtypeA;
import com.pearl.accounting.sysmodel.ProductSetup;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.SwizFeedback;

@Service("AccountService")
@Transactional
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountDao accountDao;
	@Autowired
	private AtypeAservice atypeService;
	@Autowired
	private ProductSetupService productsetupService;

	@Autowired
	private SystemUserService systemUserService;

	public SwizFeedback save(Account account) {
		try {

			account.setCreatedBy(systemUserService.getLoggedInUser());
			account.setUpdatedBy(systemUserService.getLoggedInUser());
			account.setStatus(Status.ACTIVE);
			account.setDateCreated(new Date());
			account.setDateUpdated(new Date());
			account.setATYPECD(AtypeA(account));
			account.setProductid(productid(account));
			accountDao.save(account);
			return new SwizFeedback(true, "Saved successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}

	}

	public SwizFeedback update(Account account) {
		try {
			accountDao.update(account);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}
	}

	public SwizFeedback delete(Account account) {
		try {
			accountDao.delete(account);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}
	}

	public List<Account> find() {
		try {

			return accountDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public Account find(String Sysid) {
		try {

			return accountDao.find(Sysid);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public Account findByName(String accountName) {
		try {

			Search search = new Search();
			search.addFilterILike("accountName", accountName);
			return accountDao.searchUnique(search);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public Account findByName2(String ACODE) {
		try {

//			Search search = new Search();
//			search.addFilterILike("ACODE", ACODE);
			return accountDao.find(ACODE);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}
	public String AtypeA(Account account) {
	String accountcd = null;
		try {
			AtypeA acode = atypeService.find(account.getAccountType().getAtypecd());
                 accountcd = acode.getAtypecd();
           
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}		return accountcd;	
		
	}
public  String productid(Account account) {
	String productsetup = null;
	try {
          ProductSetup producttype = productsetupService.find(account.getProductType().getProductId());
                     productsetup = producttype.getProductId();

	} catch (Exception ex) {
		System.out.println(ex.getMessage());

	}
	return productsetup;	
	}

@Override
public Account findcbal(Float cbal) {
	// TODO Auto-generated method stub
	return null;
}
 
}
