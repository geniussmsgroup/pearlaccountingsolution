package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.Withdrawal;

public interface WithdrawalService {

	public SwizFeedback save(Withdrawal withdrawal);

	public SwizFeedback update(Withdrawal withdrawal);

	public SwizFeedback delete(Withdrawal withdrawal);

	public List<Withdrawal> find();

	public Withdrawal find(String id);
}
