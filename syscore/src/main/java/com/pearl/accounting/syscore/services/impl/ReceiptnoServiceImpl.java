package com.pearl.accounting.syscore.services.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.syscore.daos.AccountDao;
import com.pearl.accounting.syscore.daos.ReceiptnoDao;
import com.pearl.accounting.syscore.services.AccountService;
import com.pearl.accounting.syscore.services.AtypeAservice;
import com.pearl.accounting.syscore.services.ProductSetupService;
import com.pearl.accounting.syscore.services.ReceiptnoService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.syscore.services.TheadpService;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.AtypeA;
import com.pearl.accounting.sysmodel.ProductSetup;
import com.pearl.accounting.sysmodel.Receiptno;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.SwizFeedback;

@Service("ReceiptnoService")
@Transactional
public class ReceiptnoServiceImpl implements ReceiptnoService {

	@Autowired
	private ReceiptnoDao receiptnoDao;
	@Autowired
	private AtypeAservice atypeService;
	@Autowired
	private ProductSetupService productsetupService;

	@Autowired
	private SystemUserService systemUserService;
	@Autowired
	private TheadpService theadpService;
	public SwizFeedback save(Receiptno receipt) {
		try {
		//	receipt.setAmount(9000);
			
			
			
			
			receipt.setAMTWORDS(getamountinwords(receipt));
			receipt.setTheadID(Integer.parseInt(theadpService.getthead()));
			
			receiptnoDao.save(receipt);
			return new SwizFeedback(true, "Saved successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}

	}
	public String getamountinwords(Receiptno receipt) {
		String ans = null;
		NumberToWord did =new NumberToWord(); 
		
		ans= did.convert((int)receipt.getAmount());
		System.out.println(ans);
		
		return ans;
	}

	public SwizFeedback update(Receiptno receipt) {
		try {
			receiptnoDao.update(receipt);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}
	}

	public SwizFeedback delete(Receiptno receipt) {
		try {
			receiptnoDao.delete(receipt);
			return new SwizFeedback(true, "Updated successfully");
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			return new SwizFeedback(true, ex.getMessage());
		}
	}

	public List<Receiptno> find() {
		try {

			return receiptnoDao.find();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public Receiptno find(String Sysid) {
		try {

			return receiptnoDao.find(Sysid);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	public Receiptno findByName(String accountName) {
		try {

			Search search = new Search();
			search.addFilterILike("accountName", accountName);
			return receiptnoDao.searchUnique(search);

		} catch (Exception ex) {
			System.out.println(ex.getMessage());

		}
		return null;
	}

	
 
}
