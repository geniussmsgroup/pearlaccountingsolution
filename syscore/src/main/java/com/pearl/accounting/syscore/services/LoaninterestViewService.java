package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.LoanbalanceView;
import com.pearl.accounting.sysmodel.LoaninterestView;

public interface LoaninterestViewService {


	public List<LoaninterestView> find();

	//public LoaninterestView find(String  Lnbal );
	public LoaninterestView find(String   MemberId);

	public  float  getinterestrate(LoanbalanceView loanbalance);
	public LoaninterestView getmemberids(LoanbalanceView loanbalance);
}
