 package com.pearl.accounting.syscore.daos;

import com.pearl.accounting.sysmodel.AssessmentPeriod;

public interface AssessmentPeriodDao extends SwiziBaseDao<AssessmentPeriod>{

}
