package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.AtypeA;
import com.pearl.accounting.sysmodel.FixedData;
import com.pearl.accounting.sysmodel.SwizFeedback;

public interface FixedDataService {

	public SwizFeedback save(FixedData fixeddata);

	public SwizFeedback update(FixedData fixeddata);

	public SwizFeedback delete(FixedData fixeddata);

	public List<FixedData> find();
    public FixedData findcbal(Float cbal  );
	public FixedData find(String IDNo );

//	public Account findByName(String accountName);
//
//	public Account findByName2(String productType);
//	public String AtypeA(Account account);
//	public String productid(Account account);
}
