package com.pearl.accounting.syscore.daos;

import com.pearl.accounting.sysmodel.CashBookPayment;

public interface CashBookPaymentDao extends SwiziBaseDao<CashBookPayment>{

}
