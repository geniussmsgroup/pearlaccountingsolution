package com.pearl.accounting.syscore.daos;

import com.pearl.accounting.sysmodel.Account;

public interface AccountDao extends SwiziBaseDao<Account>{

}
