package com.pearl.accounting.syscore.daos;

import com.pearl.accounting.sysmodel.MultiJournalDetail;

public interface MultiJournalDetailDao extends SwiziBaseDao<MultiJournalDetail>{

}
