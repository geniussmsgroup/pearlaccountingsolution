package com.pearl.accounting.syscore.services;

import java.util.List;

import com.pearl.accounting.sysmodel.DoubleEntry;
import com.pearl.accounting.sysmodel.SwizFeedback;


public interface DoubleEntryService {
	public SwizFeedback save(DoubleEntry doubleentry);

	public SwizFeedback update(DoubleEntry doubleentry);

	public SwizFeedback delete(DoubleEntry doubleentry);

	public List<DoubleEntry> find();

	public DoubleEntry find(String id);
	
	public String gethdnoCount() ;
	public String generateCode();
	public int getLoanCount();

}
