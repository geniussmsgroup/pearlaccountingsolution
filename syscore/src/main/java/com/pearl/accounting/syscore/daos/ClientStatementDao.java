package com.pearl.accounting.syscore.daos;

import com.pearl.accounting.sysmodel.ClientStatement;

public interface ClientStatementDao extends SwiziBaseDao<ClientStatement>{

}
