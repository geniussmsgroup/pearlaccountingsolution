package com.pearl.accounting.syscore.daos;

import com.pearl.accounting.sysmodel.GeneralLedger;

public interface GeneralLedgerDao extends SwiziBaseDao<GeneralLedger>{

}
