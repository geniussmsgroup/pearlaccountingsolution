package com.pearl.accounting.sysui.server;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.pearl.accounting.syscore.services.CashBookPaymentService;
import com.pearl.accounting.syscore.utils.ApplicationContextProvider;
import com.pearl.accounting.sysmodel.CashBookPayment; 

public class AccDataExport extends HttpServlet {

	private static final long serialVersionUID = 1L;

	ServletOutputStream outStream;

	CashBookPaymentService cashBookPaymentService = ApplicationContextProvider.getBean(CashBookPaymentService.class);

	 

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		System.out.println("Reached The File Down load Servlet");
		try { 

			String reportRequest = request.getParameter("request");

			if (reportRequest.equalsIgnoreCase("CASHBOOK_PAYMENT_EXPORT")) {

				handleDownload(request, response);

			} 

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

		}
	}

 
	
	protected void handleDownload(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {

			File downloadFile = generatePayementExport();

			FileInputStream inStream = new FileInputStream(downloadFile);

			// if you want to use a relative path to context root:
			String relativePath = getServletContext().getRealPath("");
			System.out.println("relativePath = " + relativePath);

			// obtains ServletContext
			ServletContext context = getServletContext();

			// gets MIME type of the file
			String mimeType = context.getMimeType(downloadFile.getName());

			if (mimeType == null) {
				mimeType = "application/octet-stream";
			}
			System.out.println("MIME type: " + mimeType);

			// modifies response
			response.setContentType(mimeType);
			response.setContentLength((int) downloadFile.length());

			// forces download
			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
			response.setHeader(headerKey, headerValue);

			// obtains response's output stream
			outStream = response.getOutputStream();

			byte[] buffer = new byte[4096];
			int bytesRead = -1;

			while ((bytesRead = inStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}

			inStream.close();
			outStream.close();
		} catch (Exception e) {
			e.printStackTrace();
			StringWriter stringWriter = new StringWriter();
			PrintWriter printWriter = new PrintWriter(stringWriter);
			e.printStackTrace(printWriter);
			response.setContentType("text/plain");
			response.getOutputStream().print(stringWriter.toString());

		} finally {
			outStream.flush();
			outStream.close();
		}
	}
	
	 
	private List<CashBookPayment> getPayments() {
		try {
			  
			List<CashBookPayment> cashBookPayments = cashBookPaymentService.find(); 
			return cashBookPayments;
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return null;
	}

	private File generatePayementExport() {

		@SuppressWarnings("resource")
		HSSFWorkbook workbook = new HSSFWorkbook();

		HSSFSheet sheet = workbook.createSheet("Employees");

		HSSFFont font = workbook.createFont();
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		HSSFCellStyle style = workbook.createCellStyle();
		style.setFont(font);

		// Create a new row in current sheet
		Row header = sheet.createRow(0);

		// Create a new cell in current row
		Cell countcell = header.createCell(0);
		Cell financialYearcell = header.createCell(1);
		Cell assessmentPeriodcell = header.createCell(2);
		Cell payeecell = header.createCell(3);
		Cell amountPaidcell = header.createCell(4);
		Cell payingAccountcell = header.createCell(5);
		Cell remarkscell = header.createCell(6);
		Cell voucherNumbercell = header.createCell(7); 
		Cell paymentDatecell = header.createCell(8);  
		 
		// Set value to new value
		countcell.setCellValue("#");
		countcell.setCellStyle(style);

		financialYearcell.setCellValue("Year");
		financialYearcell.setCellStyle(style);

		assessmentPeriodcell.setCellValue("Period");
		assessmentPeriodcell.setCellStyle(style);

		payeecell.setCellValue("Payee");
		payeecell.setCellStyle(style);

		amountPaidcell.setCellValue("Amount");
		amountPaidcell.setCellStyle(style);

		payingAccountcell.setCellValue("Paying Account");
		payingAccountcell.setCellStyle(style);

		remarkscell.setCellValue("Remarks");
		remarkscell.setCellStyle(style);

		voucherNumbercell.setCellValue("Voucher No.");
		voucherNumbercell.setCellStyle(style);

		paymentDatecell.setCellValue("Date");
		paymentDatecell.setCellStyle(style);
 

		String fileName = "Cashbook payments";
		Map<String, Object[]> data = new HashMap<String, Object[]>();

		int count = 1;
		for (CashBookPayment payment : getPayments()) {
 
			  
			
			String financialYear = null;
			String assessmentPeriod = null;
			String payee = null;
			float amountPaid = 0;

			String payingAccount = null;
			String remarks = null;
			String voucherNumber = null;
			Date paymentDate = null; 
String year = String.valueOf(payment.getYear());
			if (year != null) {
				financialYear = year;
			} else {
				financialYear = "";
			}
			if (payment.getPeriod() != null) {
				assessmentPeriod = payment.getPeriod();
			} else {
				assessmentPeriod = "";
			}

			 
			payee = payment.getPayee();
			  
			 
			amountPaid = payment.getAmountPaid();
			 

			if (payment.getPayingAccount() != null) {
				payingAccount = payment.getPayingAccount().getAccountName();
			} else {
				payingAccount = "";
			}

			if (payment.getRemarks() != null) {
				remarks = payment.getRemarks();
			} else {
				remarks = "";
			}
 

			if (payment.getVoucherNumber() != null) {
				voucherNumber = payment.getVoucherNumber();
			} else {
				voucherNumber = "";
			}
			
			paymentDate=payment.getPaymentDate(); 
			
			data.put(payment.getId(), new Object[] { count, financialYear, assessmentPeriod, payee, amountPaid,
					payingAccount, remarks, voucherNumber,paymentDate });
			count++;
		}

		Set<String> keyset = data.keySet();
		int rownum = 1;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object[] objArr = data.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if (obj instanceof Date)
					cell.setCellValue((Date) obj);
				else if (obj instanceof Boolean)
					cell.setCellValue((Boolean) obj);
				else if (obj instanceof String)
					cell.setCellValue((String) obj);
				else if (obj instanceof Double)
					cell.setCellValue((Double) obj);
				else if (obj instanceof Float)
					cell.setCellValue((Float) obj);
				else if (obj instanceof Integer)
					cell.setCellValue((Integer) obj);
			}
		}

		try {
			FileOutputStream out = new FileOutputStream(new File(fileName + ".xls"));
			workbook.write(out);
			out.close();
			System.out.println("Excel written successfully..");
			return new File(fileName + ".xls");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	
	 
	 
}
