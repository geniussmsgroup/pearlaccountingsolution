package com.pearl.accounting.sysui.client.presenters;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.pearl.accounting.sysui.client.views.panes.UserPermissionPane;
import com.pearl.accounting.sysui.client.views.panes.UserRolePane;
import com.pearl.accounting.sysui.client.views.panes.UsersPane;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;

public class UsersView extends ViewImpl implements UsersPresenter.MyView {

	private static final String DEFAULT_MARGIN = "0px";
	private VLayout panel;
	//private PartnersPane partnersPane;
	private UserRolePane userRolePane;
	
	private UserPermissionPane userPermissionPane;
	private UsersPane usersPane;

	 

	@Inject
	public UsersView() {
		panel = new VLayout();
		//partnersPane = new PartnersPane();

		userRolePane = new UserRolePane();
		
		userPermissionPane = new UserPermissionPane();
		
		usersPane = new UsersPane();
 

		//Tab partnersPaneTab = new Tab();
		//partnersPaneTab.setPane(partnersPane);
		//partnersPaneTab.setTitle("Partners");

		Tab userRolePaneTab = new Tab();
		userRolePaneTab.setPane(userRolePane);
		userRolePaneTab.setTitle("User roles");

		Tab userPermissionPaneTab = new Tab();
		userPermissionPaneTab.setPane(userPermissionPane);
		userPermissionPaneTab.setTitle("User permisssions");

		Tab usersPaneTab = new Tab();
		usersPaneTab.setPane(usersPane);
		usersPaneTab.setTitle("Users");
 
		TabSet tabSet = new TabSet();
		//tabSet.addTab(partnersPaneTab);

		tabSet.addTab(userRolePaneTab);
		
		tabSet.addTab(userPermissionPaneTab);
		
		tabSet.addTab(usersPaneTab); 

		tabSet.setMargin(0);
		tabSet.setPadding(0);

		panel.addMember(tabSet);
		panel.setWidth100();
		panel.setHeight("90%");
		Window.enableScrolling(false);
		Window.setMargin(DEFAULT_MARGIN);

	}

	public Widget asWidget() {
		return panel;
	}

	

	public UserRolePane getUserRolePane() {
		return userRolePane;
	}

	public UserPermissionPane getUserPermissionPane() {
		return userPermissionPane;
	}

	public UsersPane getUsersPane() {
		return usersPane;
	}


}
