package com.pearl.accounting.sysui.client.presenters;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.pearl.accounting.sysui.client.views.panes.TransMLPane;
import com.smartgwt.client.widgets.layout.VLayout;

public class TransMLView extends ViewImpl implements TransMLPresenter.MyView {

	private static final String DEFAULT_MARGIN = "0px"; 
	private VLayout panel;
	private TransMLPane transmlPane;

	@Inject
	public TransMLView() {

		panel = new VLayout();

		transmlPane = new TransMLPane();

		panel.setMembers(transmlPane);
		panel.setWidth100();
		panel.setHeight("90%");
		Window.enableScrolling(false);
		Window.setMargin(DEFAULT_MARGIN);

	}

	public Widget asWidget() {
		return panel;
	}

	public TransMLPane getTransMLPane() {
		return transmlPane;
	}
	
	

}
