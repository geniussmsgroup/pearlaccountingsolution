package com.pearl.accounting.sysui.client.views.panes;

import com.pearl.accounting.sysui.client.views.listgrids.UserRoleListgrid;
import com.pearl.accounting.sysui.client.views.widgets.ControlsPane;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout; 

public class UserRolePane extends VLayout {

	private ControlsPane controlsPane;

	private UserRoleListgrid userRoleListgrid;

	private IButton addButton;
	private IButton editButton;
	private IButton deleteButton;
	private IButton loadButton;
	private IButton importButton;

	public UserRolePane() {

		super();
		Label header = new Label();
		header.setStyleName("crm-ContextArea-Header-Label");
		header.setContents("User Roles");
		header.setWidth("100%");
		header.setAutoHeight();
		header.setMargin(10);
		header.setAlign(Alignment.LEFT);

		controlsPane = new ControlsPane();

		userRoleListgrid = new UserRoleListgrid();

		addButton = new IButton("New");
		editButton = new IButton("Edit");
		deleteButton = new IButton("Delete");
		loadButton = new IButton("Refresh");
		importButton = new IButton("Import");

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(addButton, editButton, deleteButton, importButton, loadButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(10);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(controlsPane);
		layout.addMember(header);
		layout.addMember(userRoleListgrid);
		layout.addMember(buttonLayout);
		this.addMember(layout);

	}

	public ControlsPane getControlsPane() {
		return controlsPane;
	}
 

	public IButton getAddButton() {
		return addButton;
	}

	public IButton getEditButton() {
		return editButton;
	}

	public IButton getDeleteButton() {
		return deleteButton;
	}

	public IButton getLoadButton() {
		return loadButton;
	}

	public IButton getImportButton() {
		return importButton;
	}

	public UserRoleListgrid getUserRoleListgrid() {
		return userRoleListgrid;
	}

}
