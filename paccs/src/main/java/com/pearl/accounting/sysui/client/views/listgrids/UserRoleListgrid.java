package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.pearl.accounting.sysmodel.UserRole;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class UserRoleListgrid extends SuperListGrid {

	public static String ID = "id";
	public static String CODE = "code";
	public static String Role = "employeeRole";
	public static String Description = "description";

	public UserRoleListgrid() {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField code = new ListGridField(CODE, "Code");
		ListGridField name = new ListGridField(Role, "Role");
		ListGridField description = new ListGridField(Description, "Description");

		this.setFields(id, code, name, description);

	}

	public ListGridRecord addRowData(UserRole userRole) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute(ID, userRole.getId());
		record.setAttribute(CODE, userRole.getCode());
		record.setAttribute(Role, userRole.getEmployeeRole());
		record.setAttribute(Description, userRole.getDescription());

		return record;
	}

	public void addRecordsToGrid(List<UserRole> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (UserRole item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
	}
}
