package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.pearl.accounting.sysmodel.Account;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class AccountListgrid extends SuperListGrid {
	public static String ID = "id";
	public static String CODE = "code";
	public static String NAME = "accountName";

	public static String AccountType = "accountType";
	public static String AccountDefinition = "AccountDefinition";
	public static String TrackPData = "trackPData";

	public AccountListgrid() {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField code = new ListGridField(CODE, "Code");
		ListGridField accountName = new ListGridField(NAME, "Account name");

		ListGridField accountType = new ListGridField(AccountType, "Account type");

		ListGridField accountDefinition = new ListGridField(AccountDefinition, "Income/Expenditure");

		ListGridField trackPData = new ListGridField(TrackPData, "Track personal data");

		this.setFields(id, code, accountName, accountType, accountDefinition, trackPData);

	}

	public ListGridRecord addRowData(Account account) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute(ID, account.getSysid());
		record.setAttribute(CODE, account.getACODE());
		record.setAttribute(NAME, account.getAccountName());

		
			record.setAttribute(AccountType, account.getATYPECD());
		

		record.setAttribute(AccountDefinition, account.getAccountDefinition());

		record.setAttribute(TrackPData, account.getAnalReq());


		return record;
	}

	public void addRecordsToGrid(List<Account> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (Account item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
	}

}
