package com.pearl.accounting.sysui.client.gin;

import com.google.gwt.inject.client.AsyncProvider;
import com.google.gwt.inject.client.GinModules;
import com.google.gwt.inject.client.Ginjector;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.client.gin.DispatchAsyncModule;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.pearl.accounting.sysui.client.presenters.AccountPresenter;
//import com.pearl.accounting.sysui.client.presenters.AccountSPresenter;
import com.pearl.accounting.sysui.client.presenters.AssessmentPeriodPresenter;
import com.pearl.accounting.sysui.client.presenters.CashBookPaymentPresenter;
import com.pearl.accounting.sysui.client.presenters.CashBookReceiptPresenter;
import com.pearl.accounting.sysui.client.presenters.ClientStatementPresenter;
import com.pearl.accounting.sysui.client.presenters.ComputeinterestPresenter;
//import com.pearl.accounting.sysui.client.presenters.ComputeinterestPresenter;
import com.pearl.accounting.sysui.client.presenters.CustomerAccountPresenter;
import com.pearl.accounting.sysui.client.presenters.DoubleEntryPresenter;
import com.pearl.accounting.sysui.client.presenters.FinancialYearPresenter;
import com.pearl.accounting.sysui.client.presenters.GeneralLedgerPresenter;
import com.pearl.accounting.sysui.client.presenters.IntervalPresenter;
import com.pearl.accounting.sysui.client.presenters.LoanDepositPresenter;
import com.pearl.accounting.sysui.client.presenters.LoanPresenter;
import com.pearl.accounting.sysui.client.presenters.LoanRepaymentsPresenter;
import com.pearl.accounting.sysui.client.presenters.MainPagePresenter;
import com.pearl.accounting.sysui.client.presenters.MultiJournalPresenter;
import com.pearl.accounting.sysui.client.presenters.MultiPresenter;
//import com.pearl.accounting.sysui.client.presenters.MultiPresenter;
//import com.pearl.accounting.sysui.client.presenters.MyPresenter;
import com.pearl.accounting.sysui.client.presenters.NotificationsPresenter;
import com.pearl.accounting.sysui.client.presenters.PeriodicDepositPresenter;
import com.pearl.accounting.sysui.client.presenters.ProductSetupPresenter;
import com.pearl.accounting.sysui.client.presenters.TransGLPresenter;
import com.pearl.accounting.sysui.client.presenters.TransMLPresenter;
///import com.pearl.accounting.sysui.client.presenters.ProductSetupPresenter;
import com.pearl.accounting.sysui.client.presenters.UsersPresenter;
import com.pearl.accounting.sysui.client.presenters.WithdrawalPresenter;

@GinModules({ DispatchAsyncModule.class, ClientModule.class })
public interface ClientGinjector extends Ginjector {
	PlaceManager getPlaceManager();

//LoanDeposit
	EventBus getEventBus();
	
	
	AsyncProvider<ComputeinterestPresenter> getComputeinterestPresenter();

	AsyncProvider<LoanDepositPresenter> getLoanDepositPresenter();

	AsyncProvider<DoubleEntryPresenter> getDoubleEntryPresenter();

	AsyncProvider<LoanRepaymentsPresenter> getLoanRepaymentsPresenter();

	AsyncProvider<IntervalPresenter> getIntervalPresenter();

	AsyncProvider<MainPagePresenter> getMainPagePresenter();

	AsyncProvider<TransGLPresenter> getTransGLPresenter();

	AsyncProvider<TransMLPresenter> getTransMLPresenter();

	AsyncProvider<ProductSetupPresenter> getProductSetupPresenter();

	AsyncProvider<NotificationsPresenter> getNotificationsPresenter();

	AsyncProvider<MultiPresenter> getMultiPresenter();

	AsyncProvider<AssessmentPeriodPresenter> getAssessmentPeriodPresenter();

	AsyncProvider<FinancialYearPresenter> getFinancialYearPresenter();

	AsyncProvider<UsersPresenter> getUsersPresenter();

	AsyncProvider<AccountPresenter> getAccountPresenter();

	AsyncProvider<CustomerAccountPresenter> getCustomerAccountPresenter();

	AsyncProvider<PeriodicDepositPresenter> getPeriodicDepositPresenter();

	AsyncProvider<LoanPresenter> getLoanPresenter();

	AsyncProvider<WithdrawalPresenter> getWithdrawalPresenter();

	AsyncProvider<GeneralLedgerPresenter> getGeneralLedgerPresenter();

	AsyncProvider<ClientStatementPresenter> getClientStatementPresenter();

	AsyncProvider<CashBookPaymentPresenter> getCashBookPaymentPresenter();

	AsyncProvider<CashBookReceiptPresenter> getCashBookReceiptPresenter();

	AsyncProvider<MultiJournalPresenter> getMultiJournalPresenter();

}
