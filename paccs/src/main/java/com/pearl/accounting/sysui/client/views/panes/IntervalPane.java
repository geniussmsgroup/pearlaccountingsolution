package com.pearl.accounting.sysui.client.views.panes;

import com.pearl.accounting.sysui.client.views.listgrids.GeneralLedgerListgrid;
import com.pearl.accounting.sysui.client.views.listgrids.IntervalListgrid;
import com.pearl.accounting.sysui.client.views.widgets.ControlsPane;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class IntervalPane extends VLayout {

	private ControlsPane controlsPane;

	private IntervalListgrid IntervalListgrid;

	private IButton loadButton;
	private IButton NextInterval;
	private IButton PreviousButton;
	private IButton Current;
	private IButton AddInterval;



	public IntervalPane() {

		super();
		Label header = new Label();
		header.setStyleName("crm-ContextArea-Header-Label");
		header.setContents("General Ledger");
		header.setWidth("100%");
		header.setAutoHeight();
		header.setMargin(10);
		header.setAlign(Alignment.LEFT);

		controlsPane = new ControlsPane();

		IntervalListgrid = new IntervalListgrid();

		loadButton = new IButton("Load");
		NextInterval = new IButton("Add Interval");
		PreviousButton = new IButton("Previous Button");
		Current = new IButton("Current Interval");
		AddInterval = new IButton("Next Button");


		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(loadButton,NextInterval,PreviousButton,Current,AddInterval);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(10);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(controlsPane);
		layout.addMember(header);
		layout.addMember(IntervalListgrid);
		layout.addMember(buttonLayout);
		this.addMember(layout);

	}

	public ControlsPane getControlsPane() {
		return controlsPane;
	}

	public IntervalListgrid getIntervalListgrid() {
		return IntervalListgrid;
	}

	public IButton getLoadButton() {
		return loadButton;
	}

	public IButton getNextInterval() {
		return NextInterval;
	}

	public void setNextInterval(IButton nextInterval) {
		NextInterval = nextInterval;
	}

	public IButton getPreviousButton() {
		return PreviousButton;
	}

	public void setPreviousButton(IButton previousButton) {
		PreviousButton = previousButton;
	}

	public IButton getCurrent() {
		return Current;
	}

	public void setCurrent(IButton current) {
		Current = current;
	}

	public IButton getAddInterval() {
		return AddInterval;
	}

	public void setAddInterval(IButton addInterval) {
		AddInterval = addInterval;
	}
	
	

}
