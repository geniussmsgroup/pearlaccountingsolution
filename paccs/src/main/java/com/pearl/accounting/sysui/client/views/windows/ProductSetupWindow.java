package com.pearl.accounting.sysui.client.views.windows;

import com.pearl.accounting.sysui.client.views.widgets.TextField;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class ProductSetupWindow extends Window {

	private TextField productId;
	private TextField productDetails;
	private TextField Remarks;
	private IButton saveButton;
	private IButton saveButtonx;

	public ProductSetupWindow() {
		productId = new TextField();
		productId.setTitle("Account Code");

		productDetails = new TextField();
		productDetails.setTitle(" Details");
		
		Remarks = new TextField();
		Remarks.setTitle("Remarks");
    
	
		
		
		
		saveButton = new IButton("Save");
	saveButtonx = new IButton("SAVE");
	
	DynamicForm form = new DynamicForm();
	form.setFields(productId,productDetails,Remarks);
	form.setWrapItemTitles(true);
	form.setMargin(10);

	HLayout buttonLayout = new HLayout();
	buttonLayout.setMembers(saveButton,saveButtonx);
	buttonLayout.setAutoHeight();
	buttonLayout.setWidth100();
	buttonLayout.setMargin(5);
	buttonLayout.setMembersMargin(4);

	VLayout layout = new VLayout();
	layout.addMember(form);
	layout.addMember(buttonLayout);

	layout.setMargin(10);
	this.addItem(layout);
	this.setWidth("40%");
	this.setHeight("40%");
	this.setAutoCenter(true);
	this.setTitle("Product Setup");
	this.setIsModal(true);
	this.setShowModalMask(true);
}

	public TextField getProductId() {
		return productId;
	}

	

	public TextField getProductDetails() {
		return productDetails;
	}

	
	public TextField getRemarks() {
		return Remarks;
	}

	
	public IButton getSaveButton() {
		return saveButton;
	}

	

	public IButton getSaveButtonx() {
		return saveButtonx;
	}

	
	
	}

