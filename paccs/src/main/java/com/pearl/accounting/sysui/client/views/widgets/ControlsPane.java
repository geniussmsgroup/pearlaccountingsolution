package com.pearl.accounting.sysui.client.views.widgets;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;


public class ControlsPane extends ToolStrip {
	ToolStripButton backButton;
	ToolStripButton newCategoryButton;
	//ToolStripButton saveButton;
	ToolStripButton editButton;
	ToolStripButton DeleteButton;
	ToolStripButton importButton;
	ToolStripButton ReportButton;
	ToolStripButton RefreshButton;
	ToolStripButton printButton;
	ToolStripButton closeButton;

	public ControlsPane() {
		
		this.setWidth100();
//		saveButton=new ToolStripButton();
//		saveButton.setIcon("saveas1.png");
//		saveButton.setShadowSoftness(50);		
//		saveButton.setTitle("Save");
		//this.addButton(saveButton);
				
		DeleteButton = new ToolStripButton();
		importButton = new ToolStripButton();
		ReportButton = new ToolStripButton();
		RefreshButton = new ToolStripButton();
		printButton = new ToolStripButton();
		closeButton = new ToolStripButton();
		
		newCategoryButton = new ToolStripButton();
		newCategoryButton.setIcon("addnew.png");
		newCategoryButton.setShadowSoftness(50);
		newCategoryButton.setTitle("New");
		this.addButton(newCategoryButton);

		this.addSeparator();

		/*
		 * newCategoryButton.addClickHandler(new ClickHandler() {
		 * 
		 * public void onClick(ClickEvent event) { addAssetCategoryForm = new
		 * AddAssetCategoryForm();
		 * 
		 * if(addAssetCategoryForm.dynamicForm.validate()){ SC.say("Sucess!");
		 * 
		 * } else { SC.say("Oops! Not Saved!");
		 * 
		 * } } });
		 */

		editButton = new ToolStripButton();
		editButton.setIcon("editfile.png");
		editButton.setTitle("Edit");
		this.addButton(editButton);
		this.addSeparator();

		DeleteButton.setIcon("deleteNew1.png");
		DeleteButton.setTitle("Delete");
		this.addButton(DeleteButton);
		this.addResizer();

		importButton.setIcon("import.png");
		importButton.setTitle("Import");
		this.addButton(importButton);
		this.addResizer();

		ReportButton.setIcon("reportnew.png");
		ReportButton.setTitle("Report");
		this.addButton(ReportButton);
		this.addResizer();

		printButton.setIcon("print.png");
		printButton.setTitle("Print");
		this.addButton(printButton);
		this.addResizer();
		/*
		 * printButton.addClickHandler(new ClickHandler() {
		 * 
		 * @Override public void onClick(ClickEvent event) {
		 * 
		 * ListGrid.showPrintPreview(AssetsGrid.myGrid); //engallanmusembya@ }
		 * });
		 */
		closeButton.setTitle("Clear");
		this.addButton(closeButton);
		this.addResizer();
		
		
		
		RefreshButton.setIcon("refresh.png");
		RefreshButton.setTitle("Refresh");
		this.addButton(RefreshButton);

	}

	public ToolStripButton getNewAssetButton() {
		return newCategoryButton;
	}

	public ToolStripButton getEditButton() {
		return editButton;
	}

	public ToolStripButton getDeleteButton() {
		return DeleteButton;
	}

	public ToolStripButton getImportButton() {
		return importButton;
	}

	public ToolStripButton getReportButton() {
		return ReportButton;
	}

	public ToolStripButton getRefreshButton() {
		return RefreshButton;
	}

	public ToolStripButton getPrintButton() {
		return printButton;
	}

	public ToolStripButton getClearButton() {
		return closeButton;
	}

	public ToolStripButton getBackButton() {
		return backButton;
	}

}