/**
 * 
 */
package com.pearl.accounting.sysui.client.views.widgets;
import com.smartgwt.client.types.Alignment; 
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class Masthead extends HLayout {

	private static final int MASTHEAD_HEIGHT = 20;
	private static final String WEST_WIDTH = "50%";
	private static final String EAST_WIDTH = "50%";
	private static final String CENTER_WIDTH = "50%";

	private static final String NAME_LABEL = "PEARL";
	private static final String CLIENT_LABEL = "SIG ";
	
	//private static final String NAME_LABEL = "Yunga";

	private static final String LOGO = "telalogo.png";

	private Label logedInUser;
	private Label logedInUserRole;

	public Masthead() {
		super();
		
	/*	Img logo=new Img(LOGO, 120, 45);
		logo.setMargin(5);*/

		Label name = new Label();
		name.setStyleName("crm-Masthead-Name");
		name.setContents(NAME_LABEL);
		name.setAutoHeight();
		
		Label Cname = new Label();
		Cname.setContents(CLIENT_LABEL);
		Cname.setStyleName("crm-Masthead-Name");
		Cname.setAutoHeight();

		//name.setIcon(LOGO);

		Label slogan = new Label(); 
		slogan.setContents("Accounting Solutions");
		
		//slogan.setContents("Soft lock");
		slogan.setAutoHeight();
		slogan.setStyleName("crm-Masthead-slogan");
		Label nlogan = new Label(); 
		nlogan.setContents("Data");
		
		//slogan.setContents("Soft lock");
		nlogan.setAutoHeight();
		nlogan.setStyleName("crm-Masthead-slogan");
		// initialize the West layout container
		VLayout westLayout = new VLayout();
		// westLayout.setHeight(MASTHEAD_HEIGHT);
//		westLayout.setWidth(WEST_WIDTH);
		westLayout.setHeight(MASTHEAD_HEIGHT);
		// westLayout.setAutoWidth();
		//westLayout.addMember(logo);
		westLayout.addMember(name);
		westLayout.addMember(slogan);
		westLayout.setBackgroundColor("green");

		// initialize the East layout container

		logedInUser = new Label();
		logedInUser.setContents("");
		logedInUser.setAutoHeight();
		logedInUser.setMargin(15);
		logedInUser.setStyleName("crm-Masthead-loginedUser");
		logedInUser.setAlign(Alignment.RIGHT);
		
		logedInUserRole= new Label();
		logedInUserRole.setContents("--");
		logedInUserRole.setAutoHeight();
		logedInUserRole.setMargin(15);
		logedInUserRole.setStyleName("crm-Masthead-loginedUser");
		logedInUserRole.setAlign(Alignment.RIGHT);

		VLayout eastLayout = new VLayout();
		eastLayout.setAlign(Alignment.RIGHT);
//		eastLayout.setWidth(EAST_WIDTH);
		eastLayout.setHeight(MASTHEAD_HEIGHT);
		eastLayout.setAutoHeight();
//		eastLayout.addMember(Cname);
		eastLayout.addMember(logedInUser);
		//eastLayout.addMember(logedInUserRole);
		

		HLayout centerLayout = new HLayout();
//		centerLayout.setAlign(Alignment.CENTER);
		centerLayout.setHeight(MASTHEAD_HEIGHT);
		centerLayout.setWidth(CENTER_WIDTH);

		this.addMember(westLayout);
		this.addMember(centerLayout);
		this.addMember(eastLayout);
		this.setStyleName("crm-Masthead");
		this.setAutoHeight();
		this.setHeight(MASTHEAD_HEIGHT);

	}

	public Label getLogedInUser() {
		return logedInUser;
	}

	public void setLogedInUser(Label logedInUser) {
		this.logedInUser = logedInUser;
	}

	public Label getLogedInUserRole() {
		return logedInUserRole;
	}
	
	

}
