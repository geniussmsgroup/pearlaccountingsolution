package com.pearl.accounting.sysui.client.views.windows;

import com.pearl.accounting.sysui.client.views.widgets.ComboBox;
import com.pearl.accounting.sysui.client.views.widgets.TextField;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.PasswordItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout; 

public class SystemUserWindow extends Window {
 
	private TextField username;
	private TextField firstName;
	private TextField lastName; 
	private TextField phoneNumber;
	private TextField emailAddress; 
	private ComboBox userRole;
	private ComboBox partner;
    private PasswordItem password;

	private IButton saveButton;
 

	public SystemUserWindow() {
		super();
		
		username = new TextField();
		username.setTitle("Username");
		
		firstName = new TextField();
		firstName.setTitle("First Name");

		lastName = new TextField();
		lastName.setTitle("Last Name");
 
		phoneNumber = new TextField();
		phoneNumber.setTitle("Phone Number");

		emailAddress = new TextField();
		emailAddress.setTitle("Email Address");
 
		userRole = new ComboBox();
		userRole.setTitle("Role");
		
		partner = new ComboBox();
		partner.setTitle("Organisation / Partner");
       
		password = new PasswordItem();
		password.setTitle("Password ");
		

		saveButton = new IButton("Save");

		DynamicForm form = new DynamicForm();
		form.setFields(firstName, lastName,username, phoneNumber, emailAddress,
				userRole, partner,password);
		form.setWrapItemTitles(true);
		form.setMargin(10);
		form.setNumCols(2);

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(saveButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(form);
 

		layout.addMember(buttonLayout);

		layout.setMargin(10);
		this.addItem(layout);
		this.setWidth("40%");
		this.setHeight("40%");
		this.setAutoCenter(true);
		this.setTitle("Add system user");
		this.setIsModal(true);
		this.setShowModalMask(true);
	}


	public TextField getUsername() {
		return username;
	}


	public TextField getFirstName() {
		return firstName;
	}


	public TextField getLastName() {
		return lastName;
	}


	public TextField getPhoneNumber() {
		return phoneNumber;
	}


	public TextField getEmailAddress() {
		return emailAddress;
	}


	public ComboBox getUserRole() {
		return userRole;
	}


	public ComboBox getPartner() {
		return partner;
	}


	public IButton getSaveButton() {
		return saveButton;
	}


	public PasswordItem getPassword() {
		return password;
	}

	 
}
