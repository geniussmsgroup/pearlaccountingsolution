package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.google.gwt.i18n.client.NumberFormat;
import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysui.client.utils.PAManager;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;

public class LoanListgrid extends SuperListGrid {
	public static String ID = "id";
	public static String AssessmentPeriodId = "assessmentPeriodId";
	public static String AssessmentPeriod = "AssessmentPeriod";
	public static String FinancialYearId = "financialYearId";
	public static String FinancialYear = "financialYear";
	public static String CustomerAccountId = "customerAccountId";
	public static String CustomerAccountNo = "customerAccountNo";
	public static String CustomerAccount = "customerAccount";
	public static String LoanDisbursementDate = "loanDisbursementDate";
	public static String RepaymentPeriod = "repaymentPeriod";
	public static String LoanCategory = "loanCategory";
	public static String LoanAmount = "loanAmount";
	public static String InterestRate = "interestRate";
	public static String PaymentDetails = "paymentDetails";
	public static String LoanPurpose = "loanPurpose";
	public static String LoanFee = "loanFee";
	public static String PayingAccountId = "payingAccountId";
	public static String PayingAccount = "payingAccount";
	public static String LoanInsuranceFee = "loanInsuranceFee";
	public static String LoanFormFee = "loanFormFee";
	public static String LoanPhotoFee = "loanPhotoFee";

	public static String RepaymentCategory = "repaymentCategory";
	public static String InterestType = "interestType";
	
	public static String LoanNumber = "loanNumber";

	private RepaymentScheduleListgrid repaymentScheduleListgrid;
	private LoanRepaymentListgrid loanRepaymentListgrid;
	
	NumberFormat nf = NumberFormat.getFormat("#,##0.00");

	public LoanListgrid() {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField assessmentPeriodId = new ListGridField(AssessmentPeriodId, "PeriodId");
		assessmentPeriodId.setHidden(true);
		ListGridField assessmentPeriod = new ListGridField(AssessmentPeriod, "Period");
		assessmentPeriod.setHidden(true);

		ListGridField financialYearId = new ListGridField(FinancialYearId, "FinancialYearId");
		financialYearId.setHidden(true);
		ListGridField financialYear = new ListGridField(FinancialYear, "Year");
		financialYear.setHidden(true);

		ListGridField customerAccountId = new ListGridField(CustomerAccountId, "AccountId");
		customerAccountId.setHidden(true);
		ListGridField customerAccountNo = new ListGridField(CustomerAccountNo, "Account No");
		ListGridField customerAccount = new ListGridField(CustomerAccount, "Account Name");

		ListGridField loanDisbursementDate = new ListGridField(LoanDisbursementDate, "Disbursement Date");
		ListGridField repaymentPeriod = new ListGridField(RepaymentPeriod, "Repayment Period (Months)");
		ListGridField loanCategory = new ListGridField(LoanCategory, "Loan Category");
		ListGridField loanAmount = new ListGridField(LoanAmount, "Loan Amount");
		
		loanAmount.setShowGridSummary(true);

		loanAmount.setSummaryFunction(new SummaryFunction() {
			public Object getSummaryValue(Record[] records, ListGridField field) {

				long totalAmount = 0;
				for (Record record : records) {

					totalAmount += Float
							.parseFloat(PAManager.getInstance().unformatCash(record.getAttribute(LoanAmount)));

				}
				return "" + PAManager.getInstance().formatCash(totalAmount);
			}
		});
		
		ListGridField interestRate = new ListGridField(InterestRate, "Interest Rate(%)");
		ListGridField paymentDetails = new ListGridField(PaymentDetails, "Payment Details");
		ListGridField loanPurpose = new ListGridField(LoanPurpose, "Loan Purpose");
		ListGridField loanFee = new ListGridField(LoanFee, "Loan Fee");

		ListGridField payingAccountId = new ListGridField(PayingAccountId, "PayingAccountId");
		payingAccountId.setHidden(true);
		ListGridField payingAccount = new ListGridField(PayingAccount, "Paying Account");
		payingAccount.setHidden(true);

		ListGridField loanInsuranceFee = new ListGridField(LoanInsuranceFee, "Loan Insurance Fee");
		loanInsuranceFee.setHidden(true);
		ListGridField loanFormFee = new ListGridField(LoanFormFee, "Loan Form Fee");
		loanFormFee.setHidden(true);

		ListGridField loanPhotoFee = new ListGridField(LoanPhotoFee, "Loan Photo Fee");
		loanPhotoFee.setHidden(true);

		ListGridField repaymentCategory = new ListGridField(RepaymentCategory, "Repayment category");

		ListGridField interestType = new ListGridField(InterestType, "Interest type");
		
		ListGridField loanNumber = new ListGridField(LoanNumber, "No.");
		 

		this.setFields(id, loanNumber,assessmentPeriodId, assessmentPeriod, financialYearId, financialYear, customerAccountId,
				customerAccountNo, customerAccount, loanDisbursementDate, repaymentPeriod, loanCategory, loanAmount,
				interestRate, paymentDetails, loanPurpose, loanFee, payingAccountId, payingAccount, loanInsuranceFee,
				loanFormFee, loanPhotoFee, repaymentCategory, interestType);
		
		this.setCanExpandRecords(true);
		this.setShowGridSummary(true);
		
		this.setWrapHeaderTitles(true);
		this.setHeaderHeight(55);

	}

	public ListGridRecord addRowData(Loan loan) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute(ID, loan.getLOANID());
		record.setAttribute(LoanNumber, loan.getLOANID());

		
		//if (loan.getYear() != null) {
		//	record.setAttribute(FinancialYearId, loan.getFinancialYear().getId());
			record.setAttribute(FinancialYear, loan.getYear());
	//	}

		if (loan.getCustomerAccount() != null) {
			record.setAttribute(CustomerAccountId, loan.getCustomerAccount().getSysid());
			record.setAttribute(CustomerAccountNo,   loan.getMEMBERID());
			record.setAttribute(CustomerAccount,
					loan.getMEMBERID()+" " +loan.getCustomerAccount().getSurname() + " " + loan.getCustomerAccount().getOtherNames());
		}

		record.setAttribute(LoanDisbursementDate, loan.getLoanDisbursementDate());
		record.setAttribute(RepaymentPeriod, loan.getRepaymentPeriod());

		if (loan.getLoanCategory() != null) {
			record.setAttribute(LoanCategory, loan.getLoanCategory().getLoanCategory());
		}

		record.setAttribute(LoanAmount, nf.format(loan.getLoanAmount()));
		record.setAttribute(InterestRate, loan.getInterestRate());
		record.setAttribute(PaymentDetails, loan.getPaymentDetails());
		record.setAttribute(LoanPurpose, loan.getLoanPurpose());
		record.setAttribute(LoanFee, loan.getLoanFee());

		if (loan.getPayingAccount() != null) {
			record.setAttribute(PayingAccountId, loan.getPayingAccount().getSysid());
			record.setAttribute(PayingAccount, loan.getPayingAccount().getAccountName());
		}

		record.setAttribute(LoanInsuranceFee, nf.format(loan.getLoanInsuranceFee()));
		record.setAttribute(LoanFormFee, nf.format(loan.getLoanFormFee()));
		record.setAttribute(LoanPhotoFee, nf.format(loan.getLoanPhotoFee()));

		if (loan.getRepaymentCategory() != null) {
			record.setAttribute(RepaymentCategory, loan.getRepaymentCategory());
		}

		if (loan.getInterestType() != null) {
			record.setAttribute(InterestType, loan.getInterestType().getInterestType());
		}

		return record;
	}

	public void addRecordsToGrid(List<Loan> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (Loan item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
	}

	@Override
	protected Canvas getExpansionComponent(final ListGridRecord record) {

		VLayout layout = new VLayout(5);

		if (repaymentScheduleListgrid != null&loanRepaymentListgrid!=null) {
			final ListGrid grid = this;
			for (ListGridRecord gr : grid.getRecords()) {
				if (gr != record) {
					grid.collapseRecord(gr);
				}
			}
			grid.selectRecord(record);

			layout.setPadding(5);

			repaymentScheduleListgrid.setWidth100();
			repaymentScheduleListgrid.setHeight(224);
			repaymentScheduleListgrid.setCellHeight(22);
			
			loanRepaymentListgrid.setWidth100();
			loanRepaymentListgrid.setHeight(224);
			loanRepaymentListgrid.setCellHeight(22);
			
			
   
			
			VLayout schedulePane = new VLayout(); 
			schedulePane.setMembers(repaymentScheduleListgrid); 
			
			VLayout paymentsPane = new VLayout();
			paymentsPane.setMembers(loanRepaymentListgrid);
			
			Tab paymentsTab = new Tab();
			paymentsTab.setPane(paymentsPane);
			paymentsTab.setTitle("Loan Repayments");
			
			Tab repaymentScheduleTab = new Tab();
			repaymentScheduleTab.setPane(schedulePane);
			repaymentScheduleTab.setTitle("Repayment schedule");

			TabSet tabSet=new TabSet();
			tabSet.addTab(repaymentScheduleTab);
			tabSet.addTab(paymentsTab); 
			

			layout.addMember(tabSet); 
			layout.setWidth100();
			layout.setHeight(270);

		}
		return layout;
	}

	public RepaymentScheduleListgrid getRepaymentScheduleListgrid() {
		return repaymentScheduleListgrid;
	}

	public void setRepaymentScheduleListgrid(RepaymentScheduleListgrid repaymentScheduleListgrid) {
		this.repaymentScheduleListgrid = repaymentScheduleListgrid;
	}

	public LoanRepaymentListgrid getLoanRepaymentListgrid() {
		return loanRepaymentListgrid;
	}

	public void setLoanRepaymentListgrid(LoanRepaymentListgrid loanRepaymentListgrid) {
		this.loanRepaymentListgrid = loanRepaymentListgrid;
	}

}
