package com.pearl.accounting.sysui.client.views.windows;

import java.util.List;
import com.pearl.accounting.sysui.client.views.widgets.ComboBox;
import com.pearl.accounting.sysui.client.views.widgets.TextField;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class DoubleEntryWindow extends Window {

	private DateItem Transdate;
	private TextField paydet;
	private TextField amount;
	private TextField remarks;
	private ComboBox Memcode;
	private ComboBox Acdebit;
	private ComboBox accredit;
	private TextField Searchbar;
	private TextField year;
	private TextField period;
	private ComboBox loanaccount;

	private DateItem startdate;
	private DateItem enddate;
	private IButton save;
	private TextField periodcode;
	private IButton Search;

	public DoubleEntryWindow() {
		super();

		Searchbar = new TextField();
		Searchbar.setTitle("Searchbar");
		year = new TextField();
		year.setTitle("Year");
		period = new TextField();
		period.setTitle("Period");
		periodcode = new TextField();
		periodcode.setTitle("periodcode");

		Transdate = new DateItem();
		Transdate.setTitle("Transactiondate");
		Transdate.setTitle("Payment Date");
		Transdate.setUseTextField(true);
		Transdate.setWidth("10px");
		Transdate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
		///
		startdate = new DateItem();
		startdate.setTitle("Transactiondate");
		startdate.setTitle("from tartdate");
		startdate.setUseTextField(true);
		startdate.setWidth("10px");
		startdate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
		//////
		enddate = new DateItem();
		enddate.setTitle("Transactiondate");
		enddate.setTitle("to enddate");
		enddate.setUseTextField(true);
		enddate.setWidth("10px");
		enddate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
		/////

		paydet = new TextField();
		paydet.setTitle("paymentdetails");
		paydet.setWidth("50px");
		amount = new TextField();
		amount.setTitle("Amount");
		amount.setKeyPressFilter("[0-9]");
		amount.setWidth("50px");
		remarks = new TextField();
		remarks.setTitle("REmarks");
		remarks.setWidth("50px");
		Memcode = new ComboBox();
		Memcode.setTitle("select client code");
		Memcode.setWidth("20px");
		Memcode.setWidth("50px");
		Acdebit = new ComboBox();
		Acdebit.setTitle("AC/DEBIT");
		Acdebit.setWidth("20px");
		loanaccount = new ComboBox();
		loanaccount.setTitle("LoanNumber");
		loanaccount.setWidth("20px");
		accredit = new ComboBox();
		accredit.setTitle("AcCredit");
		accredit.setWidth("20px");
		save = new IButton("Save");
		Search = new IButton("Search");
		DynamicForm form3 = new DynamicForm();
		form3.setFields(Transdate, paydet, amount, remarks);
		form3.setWrapItemTitles(true);
		form3.setMargin(10);

		DynamicForm form1 = new DynamicForm();
////	Aform.setFields(period);
		form1.setFields(year, periodcode, period, startdate, enddate);
		form1.setWrapItemTitles(true);
		form1.setMargin(10);
		form1.setNumCols(8);
		DynamicForm form2 = new DynamicForm();
////Aform.setFields(period);
		form2.setFields(Acdebit, accredit, Memcode, loanaccount);
		form2.setWrapItemTitles(true);
		form2.setMargin(10);
		form2.setNumCols(4);

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(save, Search);

		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMembers(form1, form3, form2);
		layout.addMember(buttonLayout);

		layout.setMargin(10);
		this.addItem(layout);
		this.setWidth("70%");
		this.setHeight("70%");
		this.setAutoCenter(true);
		this.setTitle("DoubleEntryScreen");
		this.setIsModal(true);
		this.setShowModalMask(true);

	}

	public ComboBox getLoanaccount() {
		return loanaccount;
	}

	public TextField getYear() {
		return year;
	}

	public TextField getPeriod() {
		return period;
	}

	public DateItem getStartdate() {
		return startdate;
	}

	public DateItem getEnddate() {
		return enddate;
	}

	public IButton getSave() {
		return save;
	}

	public DateItem getTransdate() {
		return Transdate;
	}

	public TextField getPaydet() {
		return paydet;
	}

	public TextField getAmount() {
		return amount;
	}

	public TextField getPeriodcode() {
		return periodcode;
	}

	public TextField getRemarks() {
		return remarks;
	}

	public ComboBox getMemcode() {
		return Memcode;
	}

	public ComboBox getAcdebit() {
		return Acdebit;
	}

	public ComboBox getAccredit() {
		return accredit;
	}

	public TextField getSearchbar() {
		return Searchbar;
	}

	public IButton getSearch() {
		return Search;
	}

}
