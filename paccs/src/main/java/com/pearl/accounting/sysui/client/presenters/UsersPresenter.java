package com.pearl.accounting.sysui.client.presenters;

import java.util.Date;
import java.util.LinkedHashMap;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.shared.DispatchAsync;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.SystemUser;
import com.pearl.accounting.sysmodel.SystemView;
import com.pearl.accounting.sysmodel.UserPermission;
import com.pearl.accounting.sysmodel.UserRole;
import com.pearl.accounting.sysui.client.place.NameTokens;
import com.pearl.accounting.sysui.client.utils.PAConstant;
import com.pearl.accounting.sysui.client.views.listgrids.UserListgrid;
import com.pearl.accounting.sysui.client.views.listgrids.UserPermissionListgrid;
import com.pearl.accounting.sysui.client.views.listgrids.UserRoleListgrid;
import com.pearl.accounting.sysui.client.views.panes.UserPermissionPane;
import com.pearl.accounting.sysui.client.views.panes.UserRolePane;
import com.pearl.accounting.sysui.client.views.panes.UsersPane;
import com.pearl.accounting.sysui.client.views.widgets.SwizimaLoader;
import com.pearl.accounting.sysui.client.views.windows.SystemUserWindow;
import com.pearl.accounting.sysui.client.views.windows.UserPermissionWindow;
import com.pearl.accounting.sysui.client.views.windows.UserRoleWindow;
import com.pearl.accounting.sysui.shared.SysAction;
import com.pearl.accounting.sysui.shared.SysResult;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler; 

public class UsersPresenter extends Presenter<UsersPresenter.MyView, UsersPresenter.MyProxy> {

	private final PlaceManager placeManager;
	private final DispatchAsync dispatcher;

	public interface MyView extends View {
  
		public UserRolePane getUserRolePane();

		public UserPermissionPane getUserPermissionPane();

		public UsersPane getUsersPane();

	}

	@ProxyCodeSplit
	@NameToken(NameTokens.users)
	public interface MyProxy extends ProxyPlace<UsersPresenter> {
	}

	@Inject
	public UsersPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
			final DispatchAsync dispatcher, final PlaceManager placeManager) {
		super(eventBus, view, proxy);
		this.dispatcher = dispatcher;
		this.placeManager = placeManager;
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this, MainPagePresenter.TYPE_SetContextAreaContent, this);
	}

	@Override
	protected void onBind() {
		super.onBind();

		onNewButtonClicked(); 
		onRefreshButtonClicked();
		onDeleteButtonClicked();

		loadRoles();
		loadPermissions();
		loadUsers();
		
		onEditButtonClicked();
		
		onResetPasswordButtonClicked();

	}

	private void onRefreshButtonClicked() {
		  
		getView().getUserRolePane().getLoadButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadRoles();

			}
		});

		getView().getUserRolePane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadRoles();

			}
		});

		getView().getUserPermissionPane().getLoadButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadPermissions();

			}
		});

		getView().getUserPermissionPane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadPermissions();

			}
		});


		getView().getUsersPane().getRefreshButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				loadUsers();

			}
		});

		getView().getUsersPane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadUsers();

			}
		});
	}

	private void onNewButtonClicked() {
		 

		getView().getUserRolePane().getAddButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				createUserRole();

			}
		});

		getView().getUserRolePane().getControlsPane().getNewAssetButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				createUserRole();

			}
		});

		getView().getUserPermissionPane().getAddButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				createPermission();

			}
		});

		getView().getUserPermissionPane().getControlsPane().getNewAssetButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				createPermission();

			}
		});

		getView().getUsersPane().getAddButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				createUser();

			}
		});

		getView().getUsersPane().getControlsPane().getNewAssetButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				createUser();

			}
		});
	}

	private void onDeleteButtonClicked() {
		 

		getView().getUserRolePane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				deleteRole();

			}
		});

		getView().getUserRolePane().getControlsPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				deleteRole();

			}
		});

		getView().getUserPermissionPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				deletePermission();

			}
		});

		getView().getUserPermissionPane().getControlsPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				deletePermission();

			}
		});

		getView().getUsersPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				deleteUser();

			}
		});

		getView().getUsersPane().getControlsPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				deleteUser();

			}
		});
	}

	
	private void onEditButtonClicked(){
		getView().getUserRolePane().getEditButton().addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				
				editUserRole();
				
			}
		});
		
		getView().getUserRolePane().getControlsPane().getEditButton().addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				
				editUserRole();
				
			}
		});
		
		getView().getUsersPane().getEditButton().addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				
				editUser();
				
			}
		});
		
		getView().getUsersPane().getControlsPane().getEditButton().addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				
				editUser();
				
			}
		});
	}
	
	//// User roles

	private void createUserRole() {
		UserRoleWindow roleWindow = new UserRoleWindow();
		onSaveButtonClicked(roleWindow);
		roleWindow.show();

	}

	private void onSaveButtonClicked(final UserRoleWindow roleWindow) {
		roleWindow.getSaveButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				UserRole userRole = new UserRole();

				userRole.setCode(roleWindow.getRoleCode().getValueAsString());
				userRole.setDateCreated(new Date());
				userRole.setDateUpdated(new Date());
				userRole.setDescription(roleWindow.getDescription().getValueAsString());
				userRole.setEmployeeRole(roleWindow.getRoleName().getValueAsString());
				userRole.setStatus(Status.ACTIVE);

				SC.showPrompt("", "", new SwizimaLoader());

				dispatcher.execute(new SysAction(PAConstant.SAVE_ROLE, userRole), new AsyncCallback<SysResult>() {
					public void onFailure(Throwable caught) {
						System.out.println(caught.getMessage());
					}

					public void onSuccess(SysResult result) {

						SC.clearPrompt();

						if (result != null) {
							if (result.getSwizFeedback().isResponse()) {

								SC.say("Message", result.getSwizFeedback().getMessage());

								getView().getUserRolePane().getUserRoleListgrid()
										.addRecordsToGrid(result.getUserRoles());
							} else {
								SC.warn("ERROR", result.getSwizFeedback().getMessage());
							}

						} else {
							SC.say("ERROR", "Unknow error");
						}

					}
				});

			}
		});
	}
	
	
	private void editUserRole() {
		if(getView().getUserRolePane().getUserRoleListgrid().anySelected()){
			UserRoleWindow roleWindow = new UserRoleWindow();
			onUpdateButtonClicked(roleWindow);
			loadRecordToEdit(roleWindow);
			roleWindow.show();
		}else{
			SC.warn("ERROR","Please select record to edit");
		}

	}
	
	private void loadRecordToEdit(final UserRoleWindow roleWindow){
		 roleWindow.getRoleCode().setValue(getView().getUserRolePane().getUserRoleListgrid().getSelectedRecord()
					.getAttribute(UserRoleListgrid.CODE)); 
		 roleWindow.getDescription().setValue(getView().getUserRolePane().getUserRoleListgrid().getSelectedRecord()
					.getAttribute(UserRoleListgrid.Description));
		 roleWindow.getRoleName().setValue(getView().getUserRolePane().getUserRoleListgrid().getSelectedRecord()
					.getAttribute(UserRoleListgrid.Role));
	}

	private void onUpdateButtonClicked(final UserRoleWindow roleWindow) {
		roleWindow.getSaveButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				UserRole userRole = new UserRole();
				
				userRole.setId(getView().getUserRolePane().getUserRoleListgrid().getSelectedRecord()
						.getAttribute(UserRoleListgrid.ID));

				userRole.setCode(roleWindow.getRoleCode().getValueAsString());
				userRole.setDateCreated(new Date());
				userRole.setDateUpdated(new Date());
				userRole.setDescription(roleWindow.getDescription().getValueAsString());
				userRole.setEmployeeRole(roleWindow.getRoleName().getValueAsString());
				userRole.setStatus(Status.ACTIVE);

				SC.showPrompt("", "", new SwizimaLoader());

				dispatcher.execute(new SysAction(PAConstant.EDIT_ROLE, userRole), new AsyncCallback<SysResult>() {
					public void onFailure(Throwable caught) {
						System.out.println(caught.getMessage());
					}

					public void onSuccess(SysResult result) {

						SC.clearPrompt();

						if (result != null) {
							if (result.getSwizFeedback().isResponse()) {

								SC.say("Message", result.getSwizFeedback().getMessage());

								getView().getUserRolePane().getUserRoleListgrid()
										.addRecordsToGrid(result.getUserRoles());
								roleWindow.close();
							} else {
								SC.warn("ERROR", result.getSwizFeedback().getMessage());
							}

						} else {
							SC.say("ERROR", "Unknow error");
						}

					}
				});

			}
		});
	}


	private void loadRoles() {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.RETRIVE_ROLE), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					getView().getUserRolePane().getUserRoleListgrid().addRecordsToGrid(result.getUserRoles());

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void deleteRole() {
		if (getView().getUserRolePane().getUserRoleListgrid().anySelected()) {
			SC.ask("Confirm", "Are you sure you want to delete the selected record?", new BooleanCallback() {

				public void execute(Boolean value) {

					if (value) {

						UserRole userRole = new UserRole();

						userRole.setId(getView().getUserRolePane().getUserRoleListgrid().getSelectedRecord()
								.getAttribute(UserRoleListgrid.ID));

						SC.showPrompt("", "", new SwizimaLoader());

						dispatcher.execute(new SysAction(PAConstant.DELETE_ROLE, userRole),
								new AsyncCallback<SysResult>() {
									public void onFailure(Throwable caught) {
										System.out.println(caught.getMessage());
									}

									public void onSuccess(SysResult result) {

										SC.clearPrompt();

										if (result != null) {
											if (result.getSwizFeedback().isResponse()) {
												SC.say("Message", result.getSwizFeedback().getMessage());

												getView().getUserRolePane().getUserRoleListgrid()
														.addRecordsToGrid(result.getUserRoles());

											} else {
												SC.warn("ERROR", result.getSwizFeedback().getMessage());
											}

										} else {
											SC.say("ERROR", "Unknow error");
										}

									}
								});
					}

				}
			});
		} else {
			SC.warn("ERROR", "ERROR: Please select record to delete");
		}

	}

	// User permissions
	private void createPermission() {
		UserPermissionWindow window = new UserPermissionWindow();
		onSaveButtonClicked(window);
		loadRoles(window);
		loadViews(window);
		window.show();

	}

	private void loadViews(UserPermissionWindow window) {
		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

		for (SystemView systemView : SystemView.values()) {
			valueMap.put(systemView.getSystemView(), systemView.getSystemView());
		}

		window.getSystemView().setValueMap(valueMap);
	}

	private void loadRoles(final UserPermissionWindow window) {

		dispatcher.execute(new SysAction(PAConstant.RETRIVE_ROLE), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

					for (UserRole userRole : result.getUserRoles()) {
						valueMap.put(userRole.getId(), userRole.getEmployeeRole());
					}

					window.getUserRole().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void onSaveButtonClicked(final UserPermissionWindow window) {
		window.getSaveButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				UserPermission userPermission = new UserPermission();

				userPermission.setDateCreated(new Date());
				userPermission.setDateUpdated(new Date());
				userPermission.setStatus(Status.ACTIVE);

				UserRole userRole = new UserRole();
				userRole.setId(window.getUserRole().getValueAsString());

				userPermission.setUserRole(userRole);
				userPermission.setSystemView(SystemView.getSystemView(window.getSystemView().getValueAsString()));

				SC.showPrompt("", "", new SwizimaLoader());

				dispatcher.execute(new SysAction(PAConstant.SAVE_UserPermission, userPermission), 
						new AsyncCallback<SysResult>() {
							public void onFailure(Throwable caught) {
								System.out.println(caught.getMessage());
							}

							public void onSuccess(SysResult result) {

								SC.clearPrompt();

								if (result != null) {

									if (result.getSwizFeedback().isResponse()) {

										SC.say("Message", result.getSwizFeedback().getMessage());

										getView().getUserPermissionPane().getUserPermissionListgrid()
												.addRecordsToGrid(result.getUserPermissions());

									} else {
										SC.warn("ERROR", result.getSwizFeedback().getMessage());
									}

								} else {
									SC.say("ERROR", "Unknow error");
								}

							}
						});

			}
		});
	}

	private void loadPermissions() {

		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_UserPermission), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					getView().getUserPermissionPane().getUserPermissionListgrid()
							.addRecordsToGrid(result.getUserPermissions());

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

	}

	private void deletePermission() {
		if (getView().getUserPermissionPane().getUserPermissionListgrid().anySelected()) {
			SC.ask("Confirm", "Are you sure you want to delete the selected record?", new BooleanCallback() {

				public void execute(Boolean value) {

					if (value) {

						UserPermission userPermission = new UserPermission();

						userPermission.setId(getView().getUserPermissionPane().getUserPermissionListgrid()
								.getSelectedRecord().getAttribute(UserPermissionListgrid.ID));

						SC.showPrompt("", "", new SwizimaLoader());

						dispatcher.execute(new SysAction(PAConstant.DELETE_UserPermission, userPermission),
								new AsyncCallback<SysResult>() {
									public void onFailure(Throwable caught) {
										System.out.println(caught.getMessage());
									}

									public void onSuccess(SysResult result) {

										SC.clearPrompt();

										if (result != null) {
											if (result.getSwizFeedback().isResponse()) {
												SC.say("Message", result.getSwizFeedback().getMessage());

												getView().getUserPermissionPane().getUserPermissionListgrid()
														.addRecordsToGrid(result.getUserPermissions());

											} else {
												SC.warn("ERROR", result.getSwizFeedback().getMessage());
											}

										} else {
											SC.say("ERROR", "Unknow error");
										}

									}
								});
					}

				}
			});
		} else {
			SC.warn("ERROR", "ERROR: Please select record to delete");
		}

	}

	// system users

	private void createUser() {
		SystemUserWindow systemUserWindow = new SystemUserWindow();

		onSaveButtonClicked(systemUserWindow);
		loadRoles(systemUserWindow); 
		systemUserWindow.show();

	}

	private void loadRoles(final SystemUserWindow window) {

		dispatcher.execute(new SysAction(PAConstant.RETRIVE_ROLE), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

					for (UserRole userRole : result.getUserRoles()) {
						valueMap.put(userRole.getId(), userRole.getEmployeeRole());
					}

					window.getUserRole().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}
 
	
	private void onSaveButtonClicked(final SystemUserWindow systemUserWindow) {
		systemUserWindow.getSaveButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				SystemUser systemUser = new SystemUser();

				systemUser.setDateCreated(new Date());
				systemUser.setDateUpdated(new Date());
				systemUser.setEmailAddress(systemUserWindow.getEmailAddress().getValueAsString());
				systemUser.setEnabled(true);
				systemUser.setFirstName(systemUserWindow.getFirstName().getValueAsString());
				systemUser.setLastName(systemUserWindow.getLastName().getValueAsString());
				systemUser.setPassword(systemUserWindow.getPassword().getValueAsString());
				systemUser.setPhoneNumber(systemUserWindow.getPhoneNumber().getValueAsString());
   
				systemUser.setStatus(Status.ACTIVE);
				systemUser.setUsername(systemUserWindow.getUsername().getValueAsString());

				UserRole userRole = new UserRole();
				userRole.setId(systemUserWindow.getUserRole().getValueAsString());

				systemUser.setUserRole(userRole);

				SC.showPrompt("", "", new SwizimaLoader());

				dispatcher.execute(new SysAction(PAConstant.SAVE_SystemUser, systemUser),
						new AsyncCallback<SysResult>() {
							public void onFailure(Throwable caught) { 
								System.out.println(caught.getMessage());
							}

							public void onSuccess(SysResult result) {

								SC.clearPrompt();

								if (result != null) {
									if (result.getSwizFeedback().isResponse()) {
										SC.say("Message", result.getSwizFeedback().getMessage());

										getView().getUsersPane().getUserListgrid()
												.addRecordsToGrid(result.getSystemUsers());

									} else {
										SC.warn("ERROR", result.getSwizFeedback().getMessage());
									}

								} else {
									SC.say("ERROR", "Unknow error");
								}

							}
						});

			}
		});
	}

	private void loadUsers() {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_SystemUser), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					getView().getUsersPane().getUserListgrid().addRecordsToGrid(result.getSystemUsers());

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void deleteUser() {
		if (getView().getUsersPane().getUserListgrid().anySelected()) {
			SC.ask("Confirm", "Are you sure you want to delete the selected record?", new BooleanCallback() {

				public void execute(Boolean value) {

					if (value) {

						SystemUser systemUser = new SystemUser();

						systemUser.setId(getView().getUsersPane().getUserListgrid().getSelectedRecord()
								.getAttribute(UserListgrid.ID));

						SC.showPrompt("", "", new SwizimaLoader());

						dispatcher.execute(new SysAction(PAConstant.DELETE_SystemUser, systemUser),
								new AsyncCallback<SysResult>() {
									public void onFailure(Throwable caught) {
										System.out.println(caught.getMessage()); 
									}

									public void onSuccess(SysResult result) {

										SC.clearPrompt();

										if (result != null) {

											if (result.getSwizFeedback().isResponse()) {

												SC.say("Message", result.getSwizFeedback().getMessage());

												getView().getUsersPane().getUserListgrid()
														.addRecordsToGrid(result.getSystemUsers());

											} else {
												SC.warn("ERROR", result.getSwizFeedback().getMessage());
											}

										} else {
											SC.say("ERROR", "Unknow error");
										}

									}
								});
					}

				}
			});
		} else {
			SC.warn("ERROR", "ERROR: Please select record to delete");
		}

	}

	
	private void editUser() {
		
		if(getView().getUsersPane().getUserListgrid().anySelected()){
			SystemUserWindow systemUserWindow = new SystemUserWindow();
			 
			loadRoles(systemUserWindow);
			  
			onUpdateButtonClicked(systemUserWindow);
			loadRecordToEdit(systemUserWindow);
			systemUserWindow.setTitle("Update");
			systemUserWindow.show();
		}else{
			SC.warn("ERROR","Please select record to edit");
		}
		

	}
	
	private void loadRecordToEdit(final SystemUserWindow systemUserWindow){
		
		 
		 systemUserWindow.getEmailAddress().setValue(getView().getUsersPane().getUserListgrid().getSelectedRecord()
					.getAttribute(UserListgrid.EMAIL));
		 
		 systemUserWindow.getFirstName().setValue(getView().getUsersPane().getUserListgrid().getSelectedRecord()
					.getAttribute(UserListgrid.FIRST_NAME));
		 
		 systemUserWindow.getLastName().setValue(getView().getUsersPane().getUserListgrid().getSelectedRecord()
					.getAttribute(UserListgrid.LAST_NAME));
		 
		 systemUserWindow.getPhoneNumber().setValue(getView().getUsersPane().getUserListgrid().getSelectedRecord()
					.getAttribute(UserListgrid.PHONE_NUMBER));

		 
		 systemUserWindow.getPartner().setValue(getView().getUsersPane().getUserListgrid().getSelectedRecord()
					.getAttribute(UserListgrid.PARTNER_ID));
 
		 systemUserWindow.getUsername().setValue(getView().getUsersPane().getUserListgrid().getSelectedRecord()
					.getAttribute(UserListgrid.USER_NAME)); 
	 
		 systemUserWindow.getUserRole().setValue(getView().getUsersPane().getUserListgrid().getSelectedRecord()
					.getAttribute(UserListgrid.ROLE_ID));
		
	}
	
	private void onUpdateButtonClicked(final SystemUserWindow systemUserWindow) {
		systemUserWindow.getSaveButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				SystemUser systemUser = new SystemUser();
				
				systemUser.setId(getView().getUsersPane().getUserListgrid().getSelectedRecord()
						.getAttribute(UserListgrid.ID));

				systemUser.setDateCreated(new Date());
				systemUser.setDateUpdated(new Date());
				systemUser.setEmailAddress(systemUserWindow.getEmailAddress().getValueAsString());
				systemUser.setEnabled(true);
				systemUser.setFirstName(systemUserWindow.getFirstName().getValueAsString());
				systemUser.setLastName(systemUserWindow.getLastName().getValueAsString());
				systemUser.setPassword("password");
				systemUser.setPhoneNumber(systemUserWindow.getPhoneNumber().getValueAsString());
  

				systemUser.setStatus(Status.ACTIVE);
				systemUser.setUsername(systemUserWindow.getUsername().getValueAsString());

				UserRole userRole = new UserRole();
				userRole.setId(systemUserWindow.getUserRole().getValueAsString());

				systemUser.setUserRole(userRole);

				SC.showPrompt("", "", new SwizimaLoader());

				dispatcher.execute(new SysAction(PAConstant.EDIT_SystemUser, systemUser),
						new AsyncCallback<SysResult>() {
							public void onFailure(Throwable caught) {
								System.out.println(caught.getMessage());
							}

							public void onSuccess(SysResult result) {

								SC.clearPrompt();

								if (result != null) {
									if (result.getSwizFeedback().isResponse()) {
										SC.say("Message", result.getSwizFeedback().getMessage());

										getView().getUsersPane().getUserListgrid()
												.addRecordsToGrid(result.getSystemUsers());
										
										systemUserWindow.close();

									} else {
										SC.warn("ERROR", result.getSwizFeedback().getMessage());
									}

								} else {
									SC.say("ERROR", "Unknow error");
								}

							}
						});

			}
		});
	}
	
	
	private void onResetPasswordButtonClicked(){
		getView().getUsersPane().getPasswordResetButton().addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				
				passwordReset();
				
			}
		});
	}
	private void passwordReset(){

		if (getView().getUsersPane().getUserListgrid().anySelected()) {
			SC.ask("Confirm", "Are you sure you want to reset password for the selected user?", new BooleanCallback() {

				public void execute(Boolean value) {

					if (value) {

						SystemUser systemUser = new SystemUser();

						systemUser.setId(getView().getUsersPane().getUserListgrid().getSelectedRecord()
								.getAttribute(UserListgrid.ID));

						SC.showPrompt("", "", new SwizimaLoader());

						dispatcher.execute(new SysAction(PAConstant.RESET_PASSWORD, systemUser),
								new AsyncCallback<SysResult>() {
									public void onFailure(Throwable caught) { 
										System.out.println(caught.getMessage());
									}

									public void onSuccess(SysResult result) {

										SC.clearPrompt();

										if (result != null) {

											if (result.getSwizFeedback().isResponse()) {

												SC.say("Message", result.getSwizFeedback().getMessage());

												getView().getUsersPane().getUserListgrid()
														.addRecordsToGrid(result.getSystemUsers());

											} else {
												SC.warn("ERROR", result.getSwizFeedback().getMessage());
											}

										} else {
											SC.say("ERROR", "Unknow error");
										}

									}
								});
					}

				}
			});
		} else {
			SC.warn("ERROR", "ERROR: Please select record to delete");
		}

	
	}
}
