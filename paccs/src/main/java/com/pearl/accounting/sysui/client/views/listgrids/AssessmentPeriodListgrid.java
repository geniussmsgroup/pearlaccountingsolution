package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.pearl.accounting.sysmodel.AssessmentPeriod;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;  

public class AssessmentPeriodListgrid extends SuperListGrid {
	public static String ID = "id";
	public static String CODE = "code";
	public static String NAME = "assessmentPeriod";
	public static String DESCRIPTION = "description";
	public static String START_DATE = "startDate";
	public static String END_DATE = "endDate";
	
	public static String FINANCIAL_YEAR_ID = "financialYearId";
	public static String FINANCIAL_YEAR = "financialYear";
	
	public static String STATUS = "status";

	public AssessmentPeriodListgrid() {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField code = new ListGridField(CODE, "Code");
		ListGridField assessmentPeriod = new ListGridField(NAME, "Assessment Period");
		
		ListGridField description = new ListGridField(DESCRIPTION, "Description");
		
		
		ListGridField startDate = new ListGridField(START_DATE, "Start date");
		ListGridField endDate = new ListGridField(END_DATE, "End date");
		
		ListGridField financialYearId = new ListGridField(FINANCIAL_YEAR_ID, "Financial year Id");
		financialYearId.setHidden(true);
		
		ListGridField financialYear = new ListGridField(FINANCIAL_YEAR, "Financial year");
		financialYear.setHidden(true);
		
		ListGridField status = new ListGridField(STATUS, "Status");
		

		this.setFields(id, financialYearId,financialYear,code, assessmentPeriod, description,startDate, endDate,status);

	}

	public ListGridRecord addRowData(AssessmentPeriod assessmentPeriod) {
		ListGridRecord record = new ListGridRecord(); 
		record.setAttribute(ID, assessmentPeriod.getId());
		record.setAttribute(CODE, assessmentPeriod.getCode());
		record.setAttribute(NAME, assessmentPeriod.getPeriodName());
		record.setAttribute(DESCRIPTION, assessmentPeriod.getDescription());
		record.setAttribute(START_DATE, assessmentPeriod.getStartDate());
		record.setAttribute(END_DATE, assessmentPeriod.getEndDate());
		
		if(assessmentPeriod.getFinancialYear()!=null){
			
			record.setAttribute(FINANCIAL_YEAR_ID, assessmentPeriod.getFinancialYear().getId());
			record.setAttribute(FINANCIAL_YEAR, assessmentPeriod.getFinancialYear().getFinancialYear());
		}
		
		if(assessmentPeriod.getActivationStatus()!=null){
			
			record.setAttribute(STATUS, assessmentPeriod.getActivationStatus().getStatus());
		}
		

		return record;
	}

	public void addRecordsToGrid(List<AssessmentPeriod> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (AssessmentPeriod item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
	}


}
