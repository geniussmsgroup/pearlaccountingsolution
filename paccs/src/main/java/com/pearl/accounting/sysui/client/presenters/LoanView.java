package com.pearl.accounting.sysui.client.presenters;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.pearl.accounting.sysui.client.views.panes.LoanPane;
import com.smartgwt.client.widgets.layout.VLayout;

public class LoanView extends ViewImpl implements LoanPresenter.MyView {

	private static final String DEFAULT_MARGIN = "0px";
	private VLayout panel;
	private LoanPane loanPane;

	@Inject
	public LoanView() {

		panel = new VLayout();

		loanPane = new LoanPane();

		panel.setMembers(loanPane);
		panel.setWidth100();
		panel.setHeight("90%");
		Window.enableScrolling(false);
		Window.setMargin(DEFAULT_MARGIN);

	}

	public Widget asWidget() {
		return panel;
	}

	public LoanPane getLoanPane() {
		return loanPane;
	}
	
	

}
