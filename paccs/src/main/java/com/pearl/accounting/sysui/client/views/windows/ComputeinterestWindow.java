
package com.pearl.accounting.sysui.client.views.windows;


import com.pearl.accounting.sysui.client.views.widgets.ComboBox;
import com.pearl.accounting.sysui.client.views.widgets.TextField;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

//ComputeinterestWindow

public class ComputeinterestWindow extends Window {

	private ComboBox financialYear;
	private ComboBox assessmentPeriod;
	//private ComboBox customerAccount;
	//private ComboBox payingAccount;
	private TextField periodcode;

	
	private DateItem fixeddate;
	private DateItem startDate;
	private DateItem endDate;
	private TextField period;
	private TextField year;
	private DateItem lastintdate;
	private TextField fsave;

	private IButton saveButton;

	public ComputeinterestWindow() {
		super();

		financialYear = new ComboBox();
		financialYear.setTitle("Year");

//		assessmentPeriod = new ComboBox();
//		assessmentPeriod.setTitle("Period");

//		customerAccount = new ComboBox();
//		customerAccount.setTitle("Customer");
//
//		payingAccount = new ComboBox();
//		payingAccount.setTitle("Paying Account");
		period = new TextField();
		period.setTitle("period");
		period.setWidth("10px");
		periodcode = new TextField();
		periodcode.setTitle("periodcode");
		periodcode.setWidth("10px");

		year = new TextField();
		year.setTitle("year");
		year.setWidth("10px");

		fixeddate = new DateItem();
		fixeddate.setTitle("interset for Date");
		fixeddate.setUseTextField(true);
		fixeddate.setWidth("10px");
		fixeddate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		lastintdate = new DateItem();
		lastintdate.setTitle("Last interset  Date");
		lastintdate.setUseTextField(true);
		lastintdate.setWidth("10px");
		lastintdate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		
		
		
		startDate = new DateItem();
		startDate.setTitle("startDate ");
		startDate.setUseTextField(true);
		startDate.setWidth("*");
		startDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
		endDate = new DateItem();
		endDate.setTitle("endDate");
		endDate.setUseTextField(true);
		endDate.setWidth("*");
		endDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

//		paymentDetails = new TextField();
//		paymentDetails.setTitle("Payment Details");

		fsave = new TextField();
		fsave.setTitle("saved");

		saveButton = new IButton("Save");

		DynamicForm form = new DynamicForm();
		form.setFields(period, periodcode, year, startDate, endDate);
		form.setWrapItemTitles(true);
		form.setMargin(10);
		form.setNumCols(4);

		DynamicForm form2 = new DynamicForm();
		form2.setFields( fixeddate, lastintdate,fsave);
		form2.setWrapItemTitles(true);
		form2.setMargin(10);
		form2.setNumCols(4);

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(saveButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(form);
		layout.addMember(form2);
		layout.addMember(buttonLayout);

		layout.setMargin(10);
		this.addItem(layout);
		this.setWidth("50%");
		this.setHeight("50%");
		this.setAutoCenter(true);
		this.setTitle("Compute interest day interest");
		this.setIsModal(true);
		this.setShowModalMask(true);
	}

	public ComboBox getFinancialYear() {
		return financialYear;
	}

	public ComboBox getAssessmentPeriod() {
		return assessmentPeriod;
	}

//	public ComboBox getCustomerAccount() {
//		return customerAccount;
//	}

	public DateItem getStartDate() {
		return startDate;
	}

	public DateItem getEndDate() {
		return endDate;
	}

	public TextField getPeriod() {
		return period;
	}

	public TextField getPeriodcode() {
		return periodcode;
	}

	public TextField getYear() {
		return year;
	}

//	public ComboBox getPayingAccount() {
//		return payingAccount;
//	}


	public IButton getSaveButton() {
		return saveButton;
	}

	public DateItem getFixeddate() {
		return fixeddate;
	}

	public DateItem getLastintdate() {
		return lastintdate;
	}

	public TextField getFsave() {
		return fsave;
	}

	
}
