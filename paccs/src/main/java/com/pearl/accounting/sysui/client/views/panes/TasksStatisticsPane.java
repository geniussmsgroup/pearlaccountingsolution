package com.pearl.accounting.sysui.client.views.panes;

import org.moxieapps.gwt.highcharts.client.Chart;
import org.moxieapps.gwt.highcharts.client.ChartSubtitle;
import org.moxieapps.gwt.highcharts.client.ChartTitle;
import org.moxieapps.gwt.highcharts.client.ToolTip;
import org.moxieapps.gwt.highcharts.client.labels.DataLabels;
import org.moxieapps.gwt.highcharts.client.plotOptions.LinePlotOptions;
import org.moxieapps.gwt.highcharts.client.*;

import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

import org.moxieapps.gwt.highcharts.client.labels.*;
import org.moxieapps.gwt.highcharts.client.plotOptions.*;

public class TasksStatisticsPane extends VLayout {
 

	//private ControlsPane controlsPane;

	private Label paneHeader;

	// private Chart pieChart;

	// private Chart columnChart;

	// private Chart lineChart;

	public TasksStatisticsPane() {
		super();

		paneHeader = new Label();
		paneHeader = new Label();
		paneHeader.setStyleName("crm-ContextArea-Header-Label");
		paneHeader.setContents("Statistics");
		paneHeader.setWidth("100%");
		paneHeader.setAutoHeight();
		paneHeader.setMargin(2);
		paneHeader.setAlign(Alignment.LEFT);

		//controlsPane = new ControlsPane();


		final VLayout layout = new VLayout();
		//layout.addMember(controlsPane);
		//layout.addMember(paneHeader);
		//layout.addMember(buttonLayout);

		final HLayout vPanel2 = new HLayout();
		vPanel2.setWidth100();
		//vPanel2.setHeight(250);

		VLayout v1 = new VLayout();
		//v1.setHeight(250);
		v1.addMember(createPieChart());

		VLayout v2 = new VLayout();
		//v2.setHeight(250);
		v2.addMember(createColumnChart());

		vPanel2.setMembers(v1, v2);

		VLayout vPanel3 = new VLayout();
		//vPanel3.setHeight(400);
		vPanel3.addMember(createLineChart());
		
		layout.addMember(vPanel2);
		layout.addMember(vPanel3);
		this.setMembers(layout);  
	}
 

	public Chart createPieChart() {
		Chart pieChart = new Chart().setType(Series.Type.PIE)
				.setChartTitleText("Sector partners contribution")
				.setPlotBackgroundColor((String) null).setPlotBorderWidth(null).setPlotShadow(false)
				.setPiePlotOptions(new PiePlotOptions().setAllowPointSelect(true).setCursor(PlotOptions.Cursor.POINTER)
						.setPieDataLabels(new PieDataLabels().setConnectorColor("#000000").setEnabled(true)
								.setColor("#000000").setFormatter(new DataLabelsFormatter() {
									public String format(DataLabelsData dataLabelsData) {
										return "<b>" + dataLabelsData.getPointName() + "</b>: "
												+ dataLabelsData.getYAsDouble() + " %";
									}
								})))
				.setLegend(new Legend().setLayout(Legend.Layout.VERTICAL).setAlign(Legend.Align.RIGHT)
						.setVerticalAlign(Legend.VerticalAlign.TOP).setX(-100).setY(100).setFloating(true)
						.setBorderWidth(1).setBackgroundColor("#FFFFFF").setShadow(true))
				.setToolTip(new ToolTip().setFormatter(new ToolTipFormatter() {
					public String format(ToolTipData toolTipData) {
						return "<b>" + toolTipData.getPointName() + "</b>: " + toolTipData.getYAsDouble() + " %";
					}
				}));

		pieChart.addSeries(pieChart.createSeries().setName("Partners Vs sector")
				.setPoints(new Point[] { new Point("Education", 20.0), new Point("Health", 26.8),
						new Point("Environment", 12.8).setSliced(true).setSelected(true), new Point("Livelihood", 8.5),
						new Point("Refugee coordination and management",60),new Point("Wash", 21),new Point("Works", 30) }));
		return pieChart;
	}

	public Chart createColumnChart() {
		Chart columnChart = new Chart().setType(Series.Type.COLUMN).setChartTitleText("Partner outputs per sector")
				.setChartSubtitleText("Numbers")
				.setColumnPlotOptions(new ColumnPlotOptions().setPointPadding(0.2).setBorderWidth(0))
				.setLegend(new Legend().setLayout(Legend.Layout.VERTICAL).setAlign(Legend.Align.LEFT)
						.setVerticalAlign(Legend.VerticalAlign.TOP).setX(100).setY(70).setFloating(true)
						.setBackgroundColor("#FFFFFF").setShadow(true))
				.setToolTip(new ToolTip().setFormatter(new ToolTipFormatter() {
					public String format(ToolTipData toolTipData) {
						return toolTipData.getXAsString() + ": " + toolTipData.getYAsLong() + "";
					}
				}));

		columnChart.getXAxis().setCategories("Education", "Environment", "Health", "Livelihood", "Refugee coordination and management", "Works");

		columnChart.getYAxis().setAxisTitleText("Attendance over time").setMin(0);

		columnChart.addSeries(columnChart.createSeries().setName("Planned").setPoints(
				new Number[] { 49, 71, 106, 129, 144, 176}));
		columnChart.addSeries(columnChart.createSeries().setName("Actuals").setPoints(
				new Number[] { 83.6, 78.8, 98.5, 93.4, 106.0, 84.5}));
		
		

		return columnChart;
	}

	public Chart createLineChart() {
		Chart lineChart = new Chart().setType(Series.Type.LINE)
				.setChartTitle(new ChartTitle().setText("Cummulative Outputs"))
				.setChartSubtitle(new ChartSubtitle().setText("Numbers"))
				.setToolTip(new ToolTip().setEnabled(false));

		lineChart.getXAxis().setCategories("Education", "Environment", "Health", "Livelihood", "Refugee coordination and management", "Works");

		lineChart.getYAxis().setAxisTitleText("Numbers");

		lineChart.setLinePlotOptions(
				new LinePlotOptions().setEnableMouseTracking(true).setDataLabels(new DataLabels().setEnabled(true)));

		lineChart.addSeries(lineChart.createSeries().setName("Cummulative Actuals")
				.setPoints(new Number[] {20,30, 28, 49, 100, 56}));
		 
		return lineChart;
	}
	

}
