package com.pearl.accounting.sysui.client.views.windows;

import com.pearl.accounting.sysui.client.views.widgets.ComboBox;
import com.pearl.accounting.sysui.client.views.widgets.TextField;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class LoanWindow extends Window {

	private ComboBox assessmentPeriod;
	private TextField periodcode;
	  private TextField currentperiod;
	    private DateItem currentstartdate;
	    private DateItem currentenddate;
	private ComboBox financialYear;
	private DateItem startdate;
	private DateItem enddate;
	private ComboBox customerAccount;
	private DateItem loanDisbursementDate;
	private TextField repaymentPeriod;
	private ComboBox loanCategory;
	private TextField loanAmount;
	private TextField interestRate;
	private TextField paymentDetails;
	private TextField loanPurpose;
	private TextField loanFee;
	private ComboBox payingAccount;
	private TextField loanInsuranceFee;
	private TextField loanFormFee;
	private TextField loanPhotoFee;
	private TextField test;

	private ComboBox repaymentCategory;
	private ComboBox interestType;

	private IButton saveButton;

	public LoanWindow() {
		super();
		
		
		currentperiod = new TextField();
		currentperiod.setTitle("currentperiod");
		currentperiod.setWidth("10px");

		currentstartdate = new DateItem();
		currentstartdate.setTitle("Transaction Date");
		currentstartdate.setUseTextField(true);
		currentstartdate.setWidth("*");
		currentstartdate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		currentenddate = new DateItem();
		currentenddate.setTitle("Transaction Date");
		currentenddate.setUseTextField(true);
		currentstartdate.setWidth("*");
		currentstartdate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
		
		assessmentPeriod = new ComboBox();
		assessmentPeriod.setTitle("Period");

		financialYear = new ComboBox();
		financialYear.setTitle("Year");

		customerAccount = new ComboBox();
		customerAccount.setTitle("Customer/member");

		loanDisbursementDate = new DateItem();
		loanDisbursementDate.setTitle("Disbursement Date");
		loanDisbursementDate.setUseTextField(true);
		loanDisbursementDate.setWidth("*");
		loanDisbursementDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		startdate = new DateItem();
		startdate.setTitle("startdate");
		startdate.setUseTextField(true);
		startdate.setWidth("*");
		startdate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		enddate = new DateItem();
		enddate.setTitle("enddate");
		enddate.setUseTextField(true);
		enddate.setWidth("*");
		enddate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
		periodcode = new TextField();
		periodcode.setTitle("periodcode");
		periodcode.setWidth("10px");

		repaymentPeriod = new TextField();
		repaymentPeriod.setTitle("Repayment Period (Months)");
		test = new TextField();
		test .setTitle("number");
		test.setWidth("5px");
		loanCategory = new ComboBox();
		loanCategory.setTitle("Loan Category");

		loanAmount = new TextField();
		loanAmount.setTitle("Loan Amount");
		loanAmount.setKeyPressFilter("[0-9.]");

		interestRate = new TextField();
		interestRate.setTitle("Interest Rate (% per Month)");

		paymentDetails = new TextField();
		paymentDetails.setTitle("Payment Details");

		loanPurpose = new TextField();
		loanPurpose.setTitle("LoanPurpose");

		loanFee = new TextField();
		loanFee.setTitle("Loan Fee");

		payingAccount = new ComboBox();
		payingAccount.setTitle("Paying Account");

		loanInsuranceFee = new TextField();
		loanInsuranceFee.setTitle("Loan Insurance Fee");

		loanFormFee = new TextField();
		loanFormFee.setTitle("Loan Form Fee");

		loanPhotoFee = new TextField();
		loanPhotoFee.setTitle("Loan Photo Fee");

		repaymentCategory = new ComboBox();
		repaymentCategory.setTitle("Repayment Category");

		interestType = new ComboBox();
		interestType.setTitle("Interest Type");

		saveButton = new IButton("Save");

		DynamicForm form = new DynamicForm();
		form.setFields(test,financialYear, periodcode, assessmentPeriod, startdate, enddate);
		form.setWrapItemTitles(true);
		form.setMargin(10);
		form.setNumCols(4);

		DynamicForm form2 = new DynamicForm();
		form2.setFields(customerAccount, loanDisbursementDate, loanAmount, interestRate, repaymentPeriod,
				repaymentCategory, interestType, loanCategory, loanPurpose);
		form2.setWrapItemTitles(true);
		form2.setMargin(10);
		form2.setNumCols(4);

		DynamicForm form3 = new DynamicForm();
		form3.setFields(loanFee, loanInsuranceFee, loanFormFee, loanPhotoFee, payingAccount, paymentDetails);
		form3.setWrapItemTitles(true);
		form3.setMargin(10);
		form3.setNumCols(4);
		DynamicForm form4 = new DynamicForm();
		form4.setFields(currentstartdate, currentperiod,currentenddate);
		form4.setWrapItemTitles(true);
		form4.setMargin(10);
		form4.setNumCols(6);
		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(saveButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(form4);
		layout.addMember(form);
		layout.addMember(form2);
		layout.addMember(form3);
		layout.addMember(buttonLayout);

		layout.setMargin(10);
		this.addItem(layout);
		this.setWidth("60%");
		this.setHeight("60%");
		this.setAutoCenter(true);
		this.setTitle("Loan disbursement");
		this.setIsModal(true);
		this.setShowModalMask(true);
	}

	public TextField getTest() {
		return test;
	}

	public ComboBox getAssessmentPeriod() {
		return assessmentPeriod;
	}

	public ComboBox getFinancialYear() {
		return financialYear;
	}

	public ComboBox getCustomerAccount() {
		return customerAccount;
	}

	public TextField getPeriodcode() {
		return periodcode;
	}

	public DateItem getLoanDisbursementDate() {
		return loanDisbursementDate;
	}

	public TextField getRepaymentPeriod() {
		return repaymentPeriod;
	}

	public ComboBox getLoanCategory() {
		return loanCategory;
	}

	public TextField getLoanAmount() {
		return loanAmount;
	}

	public TextField getInterestRate() {
		return interestRate;
	}

	public TextField getPaymentDetails() {
		return paymentDetails;
	}

	public TextField getLoanPurpose() {
		return loanPurpose;
	}

	public TextField getLoanFee() {
		return loanFee;
	}

	public ComboBox getPayingAccount() {
		return payingAccount;
	}

	public TextField getLoanInsuranceFee() {
		return loanInsuranceFee;
	}

	public TextField getLoanFormFee() {
		return loanFormFee;
	}

	public TextField getLoanPhotoFee() {
		return loanPhotoFee;
	}

	public IButton getSaveButton() {
		return saveButton;
	}

	public ComboBox getRepaymentCategory() {
		return repaymentCategory;
	}

	public ComboBox getInterestType() {
		return interestType;
	}

	public DateItem getStartdate() {
		return startdate;
	}

	public DateItem getEnddate() {
		return enddate;
	}

	public TextField getCurrentperiod() {
		return currentperiod;
	}

	public DateItem getCurrentstartdate() {
		return currentstartdate;
	}

	public DateItem getCurrentenddate() {
		return currentenddate;
	}

}

//
//private DateItem fixeddata;
//private TextItem periodcode;
//private TextItem yearcode;
//private TextItem fsave;
//private DateItem lastintdate;
//private DateItem startdate;
//private DateItem enddate;
