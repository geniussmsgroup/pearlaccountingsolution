package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;
 
import com.pearl.accounting.sysmodel.Salary;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class SalaryListgrid extends SuperListGrid {
	public static String ID = "id"; 
	public static String NAME = "name";
	
	public static String AMOUT = "amount";
	public static String DATE = "date"; 
	  
	public SalaryListgrid() {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		 
		ListGridField name = new ListGridField(NAME, "Name"); 
		 
		ListGridField amount = new ListGridField(AMOUT, "Amount");
		
		ListGridField date = new ListGridField(DATE, "Date"); 
		
		this.setFields(id, name,amount,date);

	}

	public ListGridRecord addRowData(Salary account) {
		ListGridRecord record = new ListGridRecord(); 
		record.setAttribute(ID, account.getId());
		record.setAttribute(AMOUT, account.getAmount());
		record.setAttribute(NAME, account.getFirstName());
		record.setAttribute(DATE, account.getTransactionDate()); 

		return record;
	}

	public void addRecordsToGrid(List<Salary> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (Salary item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
	}


}
