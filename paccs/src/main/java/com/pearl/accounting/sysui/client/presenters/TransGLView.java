package com.pearl.accounting.sysui.client.presenters;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.pearl.accounting.sysui.client.views.panes.TransGLPane;
import com.smartgwt.client.widgets.layout.VLayout;

public class TransGLView extends ViewImpl implements TransGLPresenter.MyView {

	private static final String DEFAULT_MARGIN = "0px"; 
	private VLayout panel;
	private TransGLPane tranglPane;

	@Inject
	public TransGLView() {

		panel = new VLayout();

		tranglPane = new TransGLPane();

		panel.setMembers(tranglPane);
		panel.setWidth100();
		panel.setHeight("90%");
		Window.enableScrolling(false);
		Window.setMargin(DEFAULT_MARGIN);

	}

	public Widget asWidget() {
		return panel;
	}

	public TransGLPane getTransGLPane() {
		return tranglPane;
	}
	
	

}
