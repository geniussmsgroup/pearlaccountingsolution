package com.pearl.accounting.sysui.client.presenters;

import java.util.Date;
import java.util.LinkedHashMap;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.shared.DispatchAsync;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.AccountDefinition;
import com.pearl.accounting.sysmodel.AccountType;
import com.pearl.accounting.sysmodel.AtypeA;
import com.pearl.accounting.sysmodel.Fsave;
import com.pearl.accounting.sysmodel.ProductSetup;
import com.pearl.accounting.sysmodel.ProductType;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.Ttype;
import com.pearl.accounting.sysui.client.place.NameTokens;
import com.pearl.accounting.sysui.client.utils.PAConstant;
import com.pearl.accounting.sysui.client.views.listgrids.AccountListgrid;
import com.pearl.accounting.sysui.client.views.panes.AccountPane;
import com.pearl.accounting.sysui.client.views.widgets.SwizimaLoader;
import com.pearl.accounting.sysui.client.views.windows.AccountWindow;
import com.pearl.accounting.sysui.shared.SysAction;
import com.pearl.accounting.sysui.shared.SysResult;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;

public class AccountPresenter extends Presenter<AccountPresenter.MyView, AccountPresenter.MyProxy> {

	private final PlaceManager placeManager;
	private final DispatchAsync dispatcher;

	public interface MyView extends View {
		public AccountPane getAccountPane();

	}

	@ProxyCodeSplit
	@NameToken(NameTokens.accounts)
	public interface MyProxy extends ProxyPlace<AccountPresenter> {
	}

	@Inject
	public AccountPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
			final DispatchAsync dispatcher, final PlaceManager placeManager) {
		super(eventBus, view, proxy);

		this.dispatcher = dispatcher;
		this.placeManager = placeManager;
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this, MainPagePresenter.TYPE_SetContextAreaContent, this);
	}

	@Override
	protected void onBind() {
		super.onBind();

		onNewButtonClicked();
		onDeletebuttonClicked();
		loadAccounts();
		onLoadButtonClicked();

	}

	private void onLoadButtonClicked() {
		getView().getAccountPane().getLoadButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				loadAccounts();

			}
		});

		getView().getAccountPane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadAccounts();

			}
		});
	}

	private void onNewButtonClicked() {
		getView().getAccountPane().getAddButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				createAccount();

			}
		});

		getView().getAccountPane().getControlsPane().getNewAssetButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				createAccount();

			}
		});

	}

	private void createAccount() {
		AccountWindow window = new AccountWindow();
		loadACTYPEA(window);
		loadcurrency(window);
		loadTrackle(window);
		loadProductTypes(window);
		onSavebuttonClicked(window);
		onloadaccounttypes(window);
//		onSavebuttonClickedx(window);
		loadaccountdefinition(window);
		window.show();

	}

	private void onloadaccounttypes(AccountWindow window) {

		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
		valueMap.put("C", "C");
		valueMap.put("D", "D");

		window.getTtype().setValueMap(valueMap);

	}

///////prodcuctid
	private void loadProductTypes(final AccountWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_PRODUCTSETUP), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
					for (ProductSetup account : result.getProductSetups()) {
						valueMap.put(account.getProductId(), account.getProductDetails());
					}

					window.getProductType().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

	}

	private void loadACTYPEA(final AccountWindow window) {

		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_AtypeA), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
					for (AtypeA account : result.getAtypeAs()) {
						valueMap.put(account.getAtypecd(), account.getAtypecd() + " " + account.getAtypenme());
					}

					window.getAccountType().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

	}

//	private void loadAccountCategories(AccountWindow window) {
//
//		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
//
//		for (AccountDefinition accountDefinition : AccountDefinition.values()) {
//			valueMap.put(accountDefinition.getDefinition(), accountDefinition.getDefinition());
//		}
//
//		window.getCategory().setValueMap(valueMap);
//
//	}

	private void loadTrackle(AccountWindow window) {

		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

		valueMap.put("Yes", "Yes");
		valueMap.put("No", "No");

		window.getAnalReq().setValueMap(valueMap);

	}

	private void loadaccountdefinition(AccountWindow window) {

		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

		valueMap.put("I", "I");
		valueMap.put("E", "E");
		valueMap.put("N", "N");

		window.getCategory().setValueMap(valueMap);

	}
	private void loadcurrency(AccountWindow window) {

		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

		valueMap.put("UGX", "UGX");
		valueMap.put("USD", "USD");
		valueMap.put("GBP", "GBP");

		window.getCurrencyCd().setValueMap(valueMap);

	}

	private boolean FormValid(final AccountWindow window) {
		if (window.getAccountCode().getValueAsString() != null) {
			if (window.getAccountName().getValueAsString() != null) {
				if (window.getAccountType().getValueAsString() != null) {
					if (window.getCategory().getValueAsString() != null) {
						return true;
					} else {
						SC.warn("ERROR", "Please Select paying category");

					}
				} else {
					SC.warn("ERROR", "Please Select account type");
				}
			} else {
				SC.warn("ERROR", "Please enter account name");
			}
		} else {
			SC.warn("ERROR", "Please enter account code");
		}
		return false;
	}

	private void ClearForm(final AccountWindow window) {
		window.getAccountCode().clearValue();
		window.getAccountName().clearValue();
		window.getAccountType().clearValue();
		window.getCategory().clearValue();

	}

	private void onSavebuttonClicked(final AccountWindow window) {
		window.getSaveButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				if (FormValid(window)) {
					ProductSetup productid = new ProductSetup();
					productid.setProductId(window.getProductType().getValueAsString());
					AtypeA accounttype = new AtypeA();
					accounttype.setAtypecd(window.getAccountType().getValueAsString());

					Account account = new Account();
					account.setACODE(window.getAccountCode().getValueAsString());
					account.setAccountDefinition(window.getCategory().getValueAsString());
//							AccountDefinition.getDefinition(window.getCategory().getValueAsString()));
					account.setAccountName(window.getAccountName().getValueAsString());
					account.setProductType(productid);

					account.setAccountType(accounttype);
					account.setCBAL(0);
					account.setSysBal(0);
					account.setSysid("0");
					account.setPSTAT(0);
					account.setPSTATOC(0);
					account.setGBUDGET("0");
					account.setFsave(Fsave.Y);
					account.setRDATE(new Date());
					account.setCTYPE(window.getTtype().getValueAsString());
					account.setPSTYPE(window.getTtype().getValueAsString());
					// account.setITYPE(window.getAccount_type().getValueAsString());
					if (window.getAnalReq().getValueAsString() != null) {
						if (window.getAnalReq().getValueAsString().equalsIgnoreCase("Yes")) {
							account.setAnalReq("Y");

						} else if (window.getAnalReq().getValueAsString().equalsIgnoreCase("No")) {
							account.setAnalReq("N");
						}
					} else {
						account.setAnalReq("N");
					}
                     account.setCurrencyCd(window.getCurrencyCd().getValueAsString());
					// account.setUpdatedBy(updatedBy);

					SC.showPrompt("", "", new SwizimaLoader());

					dispatcher.execute(new SysAction(PAConstant.SAVE_ACCOUNT, account), new AsyncCallback<SysResult>() {
						public void onFailure(Throwable caught) {
							System.out.println(caught.getMessage());
						}

						public void onSuccess(SysResult result) {

							SC.clearPrompt();

							if (result != null) {

								if (result.getSwizFeedback().isResponse()) {
									ClearForm(window);
									SC.say("SUCCESS", result.getSwizFeedback().getMessage());

									getView().getAccountPane().getAccountListgrid()
											.addRecordsToGrid(result.getAccounts());
								} else {
									SC.warn("ERROR", result.getSwizFeedback().getMessage());
								}

							} else {
								SC.say("ERROR", "Unknow error");
							}

						}
					});
				}
			}
		});
	}

//	private void onSavebuttonClickedx(final AccountWindow window) {
//		window.getSaveButtonx().addClickHandler(new ClickHandler() {
//
//			public void onClick(ClickEvent event) {
//				if (FormValid(window)) {
//					Account account = new Account();
//					account.setAccountCode(window.getAccountCode().getValueAsString());
//					account.setAccountDefinition(
//							AccountDefinition.getDefinition(window.getCategory().getValueAsString()));
//					account.setAccountName(window.getAccountName().getValueAsString());
//					account.setAccountType(AccountType.getAccountType(window.getAccountType().getValueAsString()));
//					// account.setCreatedBy(createdBy);
//					account.setDateCreated(new Date());
//					account.setDateUpdated(new Date());
//					account.setStatus(Status.ACTIVE);
//					
//					account.setProductType(ProductType.getProductType(window.getProductType().getValueAsString()));
//
//					if (window.getTackable().getValueAsString() != null) {
//						if (window.getTackable().getValueAsString().equalsIgnoreCase("Yes")) {
//							account.setTrackPData(true);
//
//						} else if (window.getTackable().getValueAsString().equalsIgnoreCase("No")) {
//							account.setTrackPData(false);
//						}
//					} else {
//						account.setTrackPData(false);
//					}
//
//					// account.setUpdatedBy(updatedBy);
//
//					SC.showPrompt("", "", new SwizimaLoader());
//
//					dispatcher.execute(new SysAction(PAConstant.SAVE_ACCOUNT, account), new AsyncCallback<SysResult>() {
//						public void onFailure(Throwable caught) {
//							System.out.println(caught.getMessage());
//						}
//
//						public void onSuccess(SysResult result) {
//
//							SC.clearPrompt();
//
//							if (result != null) {
//
//								if (result.getSwizFeedback().isResponse()) {
//									ClearForm(window);
//									SC.say("SUCCESS", result.getSwizFeedback().getMessage());
//
//									getView().getAccountPane().getAccountListgrid()
//											.addRecordsToGrid(result.getAccounts());
//								} else {
//									SC.warn("ERROR", result.getSwizFeedback().getMessage());
//								}
//
//							} else {
//								SC.say("ERROR", "Unknow error");
//							}
//
//						}
//					});
//				}
//			}
//		});
//	}

	private void loadAccounts() {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_ACCOUNT), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					getView().getAccountPane().getAccountListgrid().addRecordsToGrid(result.getAccounts());

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void onDeletebuttonClicked() {
		getView().getAccountPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				delete();

			}
		});

		getView().getAccountPane().getControlsPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				delete();

			}
		});
	}

	private void delete() {
		if (getView().getAccountPane().getAccountListgrid().anySelected()) {

			SC.ask("Confirm", "Are you sure you want to delete the selected record?", new BooleanCallback() {

				public void execute(Boolean value) {

					if (value) {

						Account account = new Account();
						account.setSysid(getView().getAccountPane().getAccountListgrid().getSelectedRecord()
								.getAttribute(AccountListgrid.ID));

						SC.showPrompt("", "", new SwizimaLoader());

						dispatcher.execute(new SysAction(PAConstant.DELETE_ACCOUNT, account),
								new AsyncCallback<SysResult>() {
									public void onFailure(Throwable caught) {
										System.out.println(caught.getMessage());
									}

									public void onSuccess(SysResult result) {

										SC.clearPrompt();

										if (result != null) {

											if (result.getSwizFeedback().isResponse()) {

												SC.say("SUCCESS", result.getSwizFeedback().getMessage());

												getView().getAccountPane().getAccountListgrid()
														.addRecordsToGrid(result.getAccounts());
											} else {
												SC.warn("ERROR", result.getSwizFeedback().getMessage());
											}

										} else {
											SC.say("ERROR", "Unknow error");
										}

									}
								});

					}

				}
			});

		} else {
			SC.say("ERROR", "ERROR: Please select record to delete?");
		}
	}

}
