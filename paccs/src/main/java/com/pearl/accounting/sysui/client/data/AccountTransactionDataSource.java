package com.pearl.accounting.sysui.client.data;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class AccountTransactionDataSource extends DataSource {

	private static AccountTransactionDataSource instance = null;

	public static AccountTransactionDataSource getInstance(ListGridRecord[] data) {

		if (instance == null) {
			instance = new AccountTransactionDataSource("AccountTransactionDataSource",data);
		}
		return instance;
	}

	public AccountTransactionDataSource(String id,ListGridRecord[] data) {

		DataSourceTextField pk = new DataSourceTextField("pk", "Primary Key");
		DataSourceTextField icon = new DataSourceTextField("icon", "ICON");
		DataSourceTextField name = new DataSourceTextField("name", "Name");
		setFields(pk, icon, name);

		setTestData(data);
		setClientOnly(true);
	}

}
