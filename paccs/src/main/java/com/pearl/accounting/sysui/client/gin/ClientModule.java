package com.pearl.accounting.sysui.client.gin;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;
import com.gwtplatform.mvp.client.gin.DefaultModule;
//import com.pearl.accounting.sysmodel.My;
import com.pearl.accounting.sysui.client.place.ClientPlaceManager;
import com.pearl.accounting.sysui.client.place.DefaultPlace;
import com.pearl.accounting.sysui.client.place.NameTokens;
import com.pearl.accounting.sysui.client.presenters.AccountPresenter;
//import com.pearl.accounting.sysui.client.presenters.AccountSPresenter;
//import com.pearl.accounting.sysui.client.presenters.AccountSView;
import com.pearl.accounting.sysui.client.presenters.AccountView;
import com.pearl.accounting.sysui.client.presenters.AssessmentPeriodPresenter;
import com.pearl.accounting.sysui.client.presenters.AssessmentPeriodView;
import com.pearl.accounting.sysui.client.presenters.CashBookPaymentPresenter;
import com.pearl.accounting.sysui.client.presenters.CashBookPaymentView;
import com.pearl.accounting.sysui.client.presenters.CashBookReceiptPresenter;
import com.pearl.accounting.sysui.client.presenters.CashBookReceiptView;
import com.pearl.accounting.sysui.client.presenters.ClientStatementPresenter;
import com.pearl.accounting.sysui.client.presenters.ClientStatementView;
import com.pearl.accounting.sysui.client.presenters.ComputeinterestPresenter;
import com.pearl.accounting.sysui.client.presenters.ComputeinterestView;
import com.pearl.accounting.sysui.client.presenters.CustomerAccountPresenter;
//import com.pearl.accounting.sysui.client.presenters.ComputeinterestView;
///import com.pearl.accounting.sysui.client.presenters.CustomerAccountPresenter;
import com.pearl.accounting.sysui.client.presenters.CustomerAccountView;
import com.pearl.accounting.sysui.client.presenters.DoubleEntryPresenter;
import com.pearl.accounting.sysui.client.presenters.DoubleEntryView;
import  com.pearl.accounting.sysui.client.presenters.FinancialYearPresenter;
import com.pearl.accounting.sysui.client.presenters.FinancialYearView;
import com.pearl.accounting.sysui.client.presenters.GeneralLedgerPresenter;
import com.pearl.accounting.sysui.client.presenters.GeneralLedgerView;
import com.pearl.accounting.sysui.client.presenters.IntervalPresenter;
import com.pearl.accounting.sysui.client.presenters.IntervalView;
import com.pearl.accounting.sysui.client.presenters.LoanDepositPresenter;
import com.pearl.accounting.sysui.client.presenters.LoanDepositView;
import com.pearl.accounting.sysui.client.presenters.LoanPresenter;
import com.pearl.accounting.sysui.client.presenters.LoanRepaymentsPresenter;
import com.pearl.accounting.sysui.client.presenters.LoanRepaymentsView;
import com.pearl.accounting.sysui.client.presenters.LoanView;
import com.pearl.accounting.sysui.client.presenters.MainPagePresenter;
import com.pearl.accounting.sysui.client.presenters.MainPageView;
import com.pearl.accounting.sysui.client.presenters.MultiJournalPresenter;
import com.pearl.accounting.sysui.client.presenters.MultiJournalView;
import com.pearl.accounting.sysui.client.presenters.MultiPresenter;
import com.pearl.accounting.sysui.client.presenters.MultiView;
//import com.pearl.accounting.sysui.client.presenters.MultiPresenter;
//import com.pearl.accounting.sysui.client.presenters.MultiView;
//import com.pearl.accounting.sysui.client.presenters.MyPresenter;
//import com.pearl.accounting.sysui.client.presenters.MyView;
import com.pearl.accounting.sysui.client.presenters.NotificationsPresenter;
import com.pearl.accounting.sysui.client.presenters.NotificationsView;
import com.pearl.accounting.sysui.client.presenters.PeriodicDepositPresenter;
import com.pearl.accounting.sysui.client.presenters.PeriodicDepositView;
import com.pearl.accounting.sysui.client.presenters.ProductSetupPresenter;
import com.pearl.accounting.sysui.client.presenters.ProductSetupView;
import com.pearl.accounting.sysui.client.presenters.TransGLPresenter;
import com.pearl.accounting.sysui.client.presenters.TransGLView;
import com.pearl.accounting.sysui.client.presenters.TransMLPresenter;
import com.pearl.accounting.sysui.client.presenters.TransMLView;
//import com.pearl.accounting.sysui.client.presenters.ProductSetupPresenter;
//import com.pearl.accounting.sysui.client.presenters.ProductSetupView;
import com.pearl.accounting.sysui.client.presenters.UsersPresenter;
import com.pearl.accounting.sysui.client.presenters.UsersView;
import com.pearl.accounting.sysui.client.presenters.WithdrawalPresenter;
import com.pearl.accounting.sysui.client.presenters.WithdrawalView;

public class ClientModule extends AbstractPresenterModule {

	@Override
	protected void configure() {
		// TODO Auto-generated method stub 
		install(new DefaultModule(ClientPlaceManager.class));

		bindConstant().annotatedWith(DefaultPlace.class).to(NameTokens.notifications);

		bindPresenter(LoanDepositPresenter.class, LoanDepositPresenter.MyView.class, LoanDepositView.class,
				LoanDepositPresenter.MyProxy.class);
		bindPresenter(LoanRepaymentsPresenter.class, LoanRepaymentsPresenter.MyView.class, LoanRepaymentsView.class,
				LoanRepaymentsPresenter.MyProxy.class);
		
		
		bindPresenter(ComputeinterestPresenter.class, ComputeinterestPresenter.MyView.class, ComputeinterestView.class,
				ComputeinterestPresenter.MyProxy.class);
		
		
		
		bindPresenter(TransGLPresenter.class, TransGLPresenter.MyView.class, TransGLView.class,
				TransGLPresenter.MyProxy.class);
		bindPresenter(IntervalPresenter.class, IntervalPresenter.MyView.class, IntervalView.class, IntervalPresenter.MyProxy.class);
//		
		bindPresenter(DoubleEntryPresenter.class,DoubleEntryPresenter.MyView.class, DoubleEntryView.class,
				DoubleEntryPresenter.MyProxy.class);
		bindPresenter(MultiPresenter.class,MultiPresenter.MyView.class, MultiView.class,
				MultiPresenter.MyProxy.class);
//

		bindPresenter(TransMLPresenter.class, TransMLPresenter.MyView.class, TransMLView.class,
				TransMLPresenter.MyProxy.class);

		
	
//		
		bindPresenter(ProductSetupPresenter.class, ProductSetupPresenter.MyView.class, ProductSetupView.class,
				ProductSetupPresenter.MyProxy.class);
		
		bindPresenter(MainPagePresenter.class, MainPagePresenter.MyView.class, MainPageView.class,
				MainPagePresenter.MyProxy.class);

		bindPresenter(NotificationsPresenter.class, NotificationsPresenter.MyView.class, NotificationsView.class,
				NotificationsPresenter.MyProxy.class);

		bindPresenter(AssessmentPeriodPresenter.class, AssessmentPeriodPresenter.MyView.class,
				AssessmentPeriodView.class, AssessmentPeriodPresenter.MyProxy.class);

		bindPresenter(FinancialYearPresenter.class, FinancialYearPresenter.MyView.class, FinancialYearView.class,
				FinancialYearPresenter.MyProxy.class);

		bindPresenter(UsersPresenter.class, UsersPresenter.MyView.class, UsersView.class, UsersPresenter.MyProxy.class);

		bindPresenter(AccountPresenter.class, AccountPresenter.MyView.class, AccountView.class,
				AccountPresenter.MyProxy.class);

		bindPresenter(CustomerAccountPresenter.class, CustomerAccountPresenter.MyView.class, CustomerAccountView.class,
				CustomerAccountPresenter.MyProxy.class);

		bindPresenter(PeriodicDepositPresenter.class, PeriodicDepositPresenter.MyView.class, PeriodicDepositView.class,
				PeriodicDepositPresenter.MyProxy.class);

		bindPresenter(LoanPresenter.class, LoanPresenter.MyView.class, LoanView.class, LoanPresenter.MyProxy.class);

		bindPresenter(WithdrawalPresenter.class, WithdrawalPresenter.MyView.class, WithdrawalView.class,
				WithdrawalPresenter.MyProxy.class);

		bindPresenter(GeneralLedgerPresenter.class, GeneralLedgerPresenter.MyView.class, GeneralLedgerView.class,
				GeneralLedgerPresenter.MyProxy.class);
		
		bindPresenter(ClientStatementPresenter.class, ClientStatementPresenter.MyView.class, ClientStatementView.class,
				ClientStatementPresenter.MyProxy.class);

		bindPresenter(CashBookPaymentPresenter.class, CashBookPaymentPresenter.MyView.class, CashBookPaymentView.class,
				CashBookPaymentPresenter.MyProxy.class);

		bindPresenter(CashBookReceiptPresenter.class, CashBookReceiptPresenter.MyView.class, CashBookReceiptView.class,
				CashBookReceiptPresenter.MyProxy.class);

		bindPresenter(MultiJournalPresenter.class, MultiJournalPresenter.MyView.class, MultiJournalView.class,
				MultiJournalPresenter.MyProxy.class);
		
		

	}

}