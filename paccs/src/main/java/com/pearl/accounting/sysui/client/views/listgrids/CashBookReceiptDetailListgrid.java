package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.LinkedHashMap;
import java.util.List;

import com.google.gwt.i18n.client.NumberFormat;
import com.pearl.accounting.sysmodel.CashBookReceiptDetail;
import com.pearl.accounting.sysui.client.utils.PAManager;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;

public class CashBookReceiptDetailListgrid extends SuperListGrid {
	public static String ID = "id";

	public static String AmountPaid = "amountPaid";
	public static String PaymentDate = "paymentDate";

	public static String AccountId = "accountId";
	public static String Account = "account";

	public static String Remarks = "Remarks";

	NumberFormat nf = NumberFormat.getFormat("#,##0.00");

	public CashBookReceiptDetailListgrid(List<com.pearl.accounting.sysmodel.Account> accounts) {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField amountPaid = new ListGridField(AmountPaid, "Amount");
		amountPaid.setCanEdit(true);

		amountPaid.setShowGridSummary(true);

		amountPaid.setSummaryFunction(new SummaryFunction() {
			public Object getSummaryValue(Record[] records, ListGridField field) {

				long totalAmount = 0;
				for (Record record : records) {

					totalAmount += Float
							.parseFloat(PAManager.getInstance().unformatCash(record.getAttribute(AmountPaid)));

				}
				return "" + PAManager.getInstance().formatCash(totalAmount);
			}
		});

		ListGridField accountId = new ListGridField(AccountId, "Paying A/C Id");
		accountId.setHidden(true);

		ListGridField account = new ListGridField(Account, "Paying A/C");

		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

		for (com.pearl.accounting.sysmodel.Account acc : accounts) {
			valueMap.put(acc.getSysid(), acc.getAccountName());

		}
		account.setValueMap(valueMap);
		account.setCanEdit(true);

		ListGridField paymentDate = new ListGridField(PaymentDate, "Date");
		paymentDate.setHidden(true);

		ListGridField remarks = new ListGridField(Remarks, "Remarks");
		remarks.setCanEdit(true);

		this.setFields(id, accountId, account, amountPaid, paymentDate, remarks);

		this.setCanExpandRecords(true);
		this.setShowGridSummary(true);

	}

	public CashBookReceiptDetailListgrid() {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField amountPaid = new ListGridField(AmountPaid, "Amount");

		amountPaid.setShowGridSummary(true);

		amountPaid.setSummaryFunction(new SummaryFunction() {
			public Object getSummaryValue(Record[] records, ListGridField field) {

				long totalAmount = 0;
				for (Record record : records) {

					totalAmount += Float
							.parseFloat(PAManager.getInstance().unformatCash(record.getAttribute(AmountPaid)));

				}
				return "" + PAManager.getInstance().formatCash(totalAmount);
			}
		});

		ListGridField accountId = new ListGridField(AccountId, "Paying A/C Id");
		accountId.setHidden(true);

		ListGridField account = new ListGridField(Account, "Paying A/C");

		ListGridField paymentDate = new ListGridField(PaymentDate, "Date");

		ListGridField remarks = new ListGridField(Remarks, "Remarks");

		this.setFields(id, accountId, account, amountPaid, paymentDate, remarks);

		this.setCanExpandRecords(true);
		this.setShowGridSummary(true);

	}

	public ListGridRecord addRowData(CashBookReceiptDetail payment) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute(ID, payment.getId());

		record.setAttribute(AmountPaid, nf.format(payment.getAmountPaid()));

		if (payment.getPayingAccount() != null) {
			record.setAttribute(AccountId, payment.getPayingAccount().getSysid());
			record.setAttribute(Account, payment.getPayingAccount().getAccountName());
		}

		record.setAttribute(PaymentDate, payment.getPaymentDate());

		record.setAttribute(Remarks, payment.getRemarks());

		return record;
	}

	public void addRecordsToGrid(List<CashBookReceiptDetail> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (CashBookReceiptDetail item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
	}

	public ListGridRecord editRowData(CashBookReceiptDetail payment) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute(ID, payment.getId());

		record.setAttribute(AmountPaid, nf.format(payment.getAmountPaid()));

		if (payment.getPayingAccount() != null) {
			record.setAttribute(AccountId, payment.getPayingAccount().getSysid());
			record.setAttribute(Account, payment.getPayingAccount().getSysid());
		}

		record.setAttribute(PaymentDate, payment.getPaymentDate());

		record.setAttribute(Remarks, payment.getRemarks());

		return record;
	}

	public void addRecordToGrid(CashBookReceiptDetail item) {
		ListGridRecord record = editRowData(item);
		this.addData(record);
	}

}
