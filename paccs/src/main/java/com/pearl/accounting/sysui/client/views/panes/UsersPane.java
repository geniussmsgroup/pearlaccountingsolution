package com.pearl.accounting.sysui.client.views.panes;

import com.pearl.accounting.sysui.client.views.listgrids.UserListgrid;
import com.pearl.accounting.sysui.client.views.widgets.ComboBox;
import com.pearl.accounting.sysui.client.views.widgets.ControlsPane;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout; 

public class UsersPane extends VLayout {
	private ControlsPane controlsPane;

	private ComboBox deploymentSite;

	private UserListgrid userListgrid;

	private IButton addButton;
	private IButton editButton;
	private IButton deleteButton;
	private IButton loadButton;
	private IButton importButton;

	private IButton refreshButton;

	private IButton passwordResetButton;

	public UsersPane() {

		super();
		Label header = new Label();
		header.setStyleName("crm-ContextArea-Header-Label");
		header.setContents("System users ");
		header.setWidth("100%");
		header.setAutoHeight();
		header.setMargin(10);
		header.setAlign(Alignment.LEFT);

		controlsPane = new ControlsPane();

		deploymentSite = new ComboBox();
		deploymentSite.setTitle("Role");
		
		loadButton = new IButton("load");

		DynamicForm dynamicForm = new DynamicForm();
		dynamicForm.setFields(deploymentSite);
		dynamicForm.setNumCols(6);

		HLayout formLayout = new HLayout();
		formLayout.setMembers(dynamicForm, loadButton);
		formLayout.setAutoHeight();
		formLayout.setWidth100();
		formLayout.setMargin(8);
		formLayout.setMembersMargin(2);

		userListgrid = new UserListgrid();

		addButton = new IButton("New");
		editButton = new IButton("Edit");
		deleteButton = new IButton("Delete");
		importButton = new IButton("Import");
		refreshButton = new IButton("Refresh");

		passwordResetButton = new IButton();
		passwordResetButton.setTitle("Reset password");

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(addButton, editButton, deleteButton, importButton, refreshButton, passwordResetButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(10);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(controlsPane);
		layout.addMember(header);
		//layout.addMember(formLayout);
		layout.addMember(userListgrid);
		layout.addMember(buttonLayout);
		this.addMember(layout);

	}

	public ControlsPane getControlsPane() {
		return controlsPane;
	}

	public ComboBox getDeploymentSite() {
		return deploymentSite;
	}

	  
	public IButton getAddButton() {
		return addButton;
	}

	public IButton getEditButton() {
		return editButton;
	}

	public IButton getDeleteButton() {
		return deleteButton;
	}

	public IButton getLoadButton() {
		return loadButton;
	}

	public IButton getImportButton() {
		return importButton;
	}

	public IButton getRefreshButton() {
		return refreshButton;
	}

	public IButton getPasswordResetButton() {
		return passwordResetButton;
	}

	public UserListgrid getUserListgrid() {
		return userListgrid;
	}

}
