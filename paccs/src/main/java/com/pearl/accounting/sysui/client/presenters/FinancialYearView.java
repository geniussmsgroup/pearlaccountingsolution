package com.pearl.accounting.sysui.client.presenters;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.pearl.accounting.sysui.client.views.panes.FinancialYearPane;
import com.smartgwt.client.widgets.layout.VLayout; 

public class FinancialYearView  extends ViewImpl implements FinancialYearPresenter.MyView {

	private static final String DEFAULT_MARGIN = "0px";
	private VLayout panel;
	private FinancialYearPane financialYearPane;

	@Inject
	public FinancialYearView() {
		
		panel = new VLayout();
		
		financialYearPane = new FinancialYearPane();

		panel.setMembers(financialYearPane);
		panel.setWidth100();
		panel.setHeight("90%");
		Window.enableScrolling(false);
		Window.setMargin(DEFAULT_MARGIN);

	}

	public Widget asWidget() {
		return panel;
	}

	public FinancialYearPane getFinancialYearPane() {
		return financialYearPane;
	}
	
	


}
