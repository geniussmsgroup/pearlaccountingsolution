package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.pearl.accounting.sysmodel.UserPermission;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;  

public class UserPermissionListgrid extends SuperListGrid {
	public static String ID = "id";
	public static String Role_ID = "employeeRoleId";
	public static String Role = "employeeRole";
	public static String SystemView = "systemView";

	public UserPermissionListgrid() {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField employeeRoleId = new ListGridField(Role_ID, "Role Id");
		employeeRoleId.setHidden(true);
		
		ListGridField employeeRole = new ListGridField(Role, "Role");
		ListGridField systemView = new ListGridField(SystemView, "System View");

		this.setFields(id, employeeRoleId,employeeRole, systemView);

	}

	public ListGridRecord addRowData(UserPermission userPermission) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute(ID, userPermission.getId());

		if (userPermission.getUserRole() != null) {
			record.setAttribute(Role_ID, userPermission.getUserRole().getId());
			record.setAttribute(Role, userPermission.getUserRole().getEmployeeRole());
		}

		if (userPermission.getSystemView() != null) {
			record.setAttribute(SystemView, userPermission.getSystemView().getSystemView());
		}

		return record;
	}

	public void addRecordsToGrid(List<UserPermission> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (UserPermission item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
	}
}
