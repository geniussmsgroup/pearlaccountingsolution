package com.pearl.accounting.sysui.client.presenters;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.pearl.accounting.sysui.client.views.panes.AccountPane;
import com.pearl.accounting.sysui.client.views.panes.AccountSPane;
import com.pearl.accounting.sysui.client.views.panes.LoanBalancePane;
import com.pearl.accounting.sysui.client.views.panes.LoanratePane;
import com.pearl.accounting.sysui.client.views.panes.UsersPane;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;

public class ComputeinterestView extends ViewImpl implements ComputeinterestPresenter.MyView {

	private static final String DEFAULT_MARGIN = "0px";
	private VLayout panel;
	// private PartnersPane partnersPane;
	private LoanBalancePane loanbalancePane;

	private LoanratePane loanratePane;
//	private UsersPane usersPane;

	@Inject
	public ComputeinterestView() {
		panel = new VLayout();
		// partnersPane = new PartnersPane();

		loanbalancePane = new LoanBalancePane();

		loanratePane = new LoanratePane();

		/// usersPane = new UsersPane();

		// Tab partnersPaneTab = new Tab();
		// partnersPaneTab.setPane(partnersPane);
		// partnersPaneTab.setTitle("Partners");

		Tab userRolePaneTab = new Tab();
		userRolePaneTab.setPane(loanbalancePane);
		userRolePaneTab.setTitle("Loan Balance");

		Tab userPermissionPaneTab = new Tab();
		userPermissionPaneTab.setPane(loanratePane);
		userPermissionPaneTab.setTitle("Loan Rates");

//		Tab usersPaneTab = new Tab();
//		usersPaneTab.setPane(usersPane);
//		usersPaneTab.setTitle("Users");

		TabSet tabSet = new TabSet();
		// tabSet.addTab(partnersPaneTab);

		tabSet.addTab(userRolePaneTab);

		tabSet.addTab(userPermissionPaneTab);

		// tabSet.addTab(usersPaneTab);

		tabSet.setMargin(0);
		tabSet.setPadding(0);

		panel.addMember(tabSet);
		panel.setWidth100();
		panel.setHeight("90%");
		Window.enableScrolling(false);
		Window.setMargin(DEFAULT_MARGIN);

	}

	public Widget asWidget() {
		return panel;
	}

	public VLayout getPanel() {
		return panel;
	}

	public LoanBalancePane getLoanbalancePane() {
		// TODO Auto-generated method stub
		return loanbalancePane;
	}

	public LoanratePane getloanratePane() {
		// TODO Auto-generated method stub
		return loanratePane;
	}

}
