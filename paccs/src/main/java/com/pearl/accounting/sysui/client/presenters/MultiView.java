package com.pearl.accounting.sysui.client.presenters;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.pearl.accounting.sysui.client.views.panes.CashBookReceiptPane;
import com.pearl.accounting.sysui.client.views.panes.MultiPane;
import com.smartgwt.client.widgets.layout.VLayout;

public class MultiView extends ViewImpl implements MultiPresenter.MyView {

	private static final String DEFAULT_MARGIN = "0px";
	private VLayout panel;
	private MultiPane multipane;

	@Inject
	public MultiView() {

		panel = new VLayout();

		multipane = new MultiPane();

		panel.setMembers(multipane); 
		panel.setWidth100();
		panel.setHeight("90%");
		Window.enableScrolling(false);
		Window.setMargin(DEFAULT_MARGIN);

	}

	public Widget asWidget() {
		return panel;
	}

	public MultiPane getMultiPane() {
		return multipane;
	}

}
