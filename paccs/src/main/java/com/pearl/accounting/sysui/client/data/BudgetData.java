package com.pearl.accounting.sysui.client.data;

import com.pearl.accounting.sysui.client.place.NameTokens;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class BudgetData {
 
	private static ListGridRecord[] records;

	public static ListGridRecord[] getRecords() {
		if (records == null) {
			records = getNewRecords();

		}
		return records;

	}

	public static ListGridRecord createRecord(String pk, String icon, String name) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute("pk", pk);
		record.setAttribute("icon", icon);
		record.setAttribute("name", name);
		return record;
	}

	public static ListGridRecord[] getNewRecords() {
		 

		return new ListGridRecord[] {
				createRecord("", "application_form",NameTokens.notifications),
				createRecord("", "application_form","Budget tractions"),
				createRecord("", "application_form","Budget data generate"),
				createRecord("", "application_form","Budget data edit"),
				createRecord("", "application_form","Function budget"),
				createRecord("", "application_form","Budget by budgetline")
				  
		};

	}

}
