package com.pearl.accounting.sysui.client.presenters;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.pearl.accounting.sysui.client.views.panes.IntervalPane;
import com.smartgwt.client.widgets.layout.VLayout;

public class IntervalView extends ViewImpl implements IntervalPresenter.MyView {

	private static final String DEFAULT_MARGIN = "0px"; 
	private VLayout panel;
	private IntervalPane intervalPane;

	@Inject
	public IntervalView() {

		panel = new VLayout();

		intervalPane = new IntervalPane();

		panel.setMembers(intervalPane);
		panel.setWidth100();
		panel.setHeight("90%");
		Window.enableScrolling(false);
		Window.setMargin(DEFAULT_MARGIN);

	}

	public Widget asWidget() {
		return panel;
	}

	public IntervalPane getIntervalPane() {
		return intervalPane;
	}
	
	

}
