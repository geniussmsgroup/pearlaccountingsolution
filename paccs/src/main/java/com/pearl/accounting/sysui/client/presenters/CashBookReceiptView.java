package com.pearl.accounting.sysui.client.presenters;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.pearl.accounting.sysui.client.views.panes.CashBookReceiptPane;
import com.smartgwt.client.widgets.layout.VLayout;

public class CashBookReceiptView extends ViewImpl implements CashBookReceiptPresenter.MyView {

	private static final String DEFAULT_MARGIN = "0px";
	private VLayout panel;
	private CashBookReceiptPane cashBookReceiptPane;

	@Inject
	public CashBookReceiptView() {

		panel = new VLayout();

		cashBookReceiptPane = new CashBookReceiptPane();

		panel.setMembers(cashBookReceiptPane); 
		panel.setWidth100();
		panel.setHeight("90%");
		Window.enableScrolling(false);
		Window.setMargin(DEFAULT_MARGIN);

	}

	public Widget asWidget() {
		return panel;
	}

	public CashBookReceiptPane getCashBookReceiptPane() {
		return cashBookReceiptPane;
	}

}
