package com.pearl.accounting.sysui.client.data;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class SavingsCreditDataSource extends DataSource {

	private static SavingsCreditDataSource instance = null;

	public static SavingsCreditDataSource getInstance(ListGridRecord[] data) {

		if (instance == null) {
			instance = new SavingsCreditDataSource("SavingsCreditDataSource",data);
		}
		return instance;
	}

	public SavingsCreditDataSource(String id,ListGridRecord[] data) {

		DataSourceTextField pk = new DataSourceTextField("pk", "Primary Key");
		DataSourceTextField icon = new DataSourceTextField("icon", "ICON");
		DataSourceTextField name = new DataSourceTextField("name", "Name");
		setFields(pk, icon, name);

		setTestData(data);
		setClientOnly(true);
	}

}
