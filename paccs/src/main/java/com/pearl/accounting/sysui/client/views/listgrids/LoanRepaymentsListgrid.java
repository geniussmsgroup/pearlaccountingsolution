package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.google.gwt.i18n.client.NumberFormat;
//import com.pearl.accounting.sysmodel.GeneralLedger;
import com.pearl.accounting.sysmodel.LoanRepayments;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class LoanRepaymentsListgrid extends SuperListGrid {

	public static String ID = "id";

	public static String TransDate = "TransDate";
	public static String ClientCode = "ClientCode";
	public static String Period = "Period";
	public static String Year = "Year";
	public static String LoanID = "LoanID";
	public static String PrincipalPay = "PrincipalPay";
	public static String InterestPay = "InterestPay";
	public static String TotalPay = "TotalPay";
	public static String LoanBal = "LoanBal";
	NumberFormat nf = NumberFormat.getFormat("#,##0.00");

	public LoanRepaymentsListgrid() {

		super();

		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField tdate = new ListGridField(TransDate, "TransDate");
		ListGridField client = new ListGridField(ClientCode, "ClientCode");

		ListGridField period = new ListGridField(Period, "Period");
		ListGridField year = new ListGridField(Year, "Year");
		// ListGridField financialYear = new ListGridField(TransDate, "Year");
		ListGridField loanID = new ListGridField(LoanID, "LoanID");
		ListGridField principalPay = new ListGridField(PrincipalPay, "PrincipalPay");
		ListGridField interestPay = new ListGridField(InterestPay, "InterestPay");
		ListGridField totalPay = new ListGridField(TotalPay, "TotalPay");
		this.setFields(id, tdate, client, period, year, loanID, principalPay, interestPay, totalPay);
		// ListGridField LoanBal = new ListGridField(LoanBal, "Period");

	}

	public ListGridRecord addRowData(LoanRepayments ledger) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute(ID, ledger.getSysId());
		record.setAttribute(ClientCode, ledger.getClientCode());
		record.setAttribute(Period, ledger.getIcode());
		record.setAttribute(Year, ledger.getYrCode());
		record.setAttribute(LoanID, ledger.getLoanId());
		record.setAttribute(PrincipalPay, ledger.getPrincipalPay());
		record.setAttribute(InterestPay, ledger.getInterestPay());
		record.setAttribute(TotalPay, ledger.getTotalPay());

		// if (ledger.getYear() != null) {
		record.setAttribute(TransDate, ledger.getTDate());

		return record;

	}

	public void addRecordsToGrid(List<LoanRepayments> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (LoanRepayments item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
	}
}
