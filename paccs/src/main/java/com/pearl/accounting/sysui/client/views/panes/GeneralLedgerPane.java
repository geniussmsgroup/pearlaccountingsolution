package com.pearl.accounting.sysui.client.views.panes;

import com.pearl.accounting.sysui.client.views.listgrids.GeneralLedgerListgrid; 
import com.pearl.accounting.sysui.client.views.widgets.ControlsPane;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class GeneralLedgerPane extends VLayout {

	private ControlsPane controlsPane;

	private GeneralLedgerListgrid generalLedgerListgrid;

	private IButton loadButton;

	public GeneralLedgerPane() {

		super();
		Label header = new Label();
		header.setStyleName("crm-ContextArea-Header-Label");
		header.setContents("General Ledger");
		header.setWidth("100%");
		header.setAutoHeight();
		header.setMargin(10);
		header.setAlign(Alignment.LEFT);

		controlsPane = new ControlsPane();

		generalLedgerListgrid = new GeneralLedgerListgrid();

		loadButton = new IButton("Load");

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(loadButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(10);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(controlsPane);
		layout.addMember(header);
		layout.addMember(generalLedgerListgrid);
		layout.addMember(buttonLayout);
		this.addMember(layout);

	}

	public ControlsPane getControlsPane() {
		return controlsPane;
	}

	public GeneralLedgerListgrid getGeneralLedgerListgrid() {
		return generalLedgerListgrid;
	}

	public IButton getLoadButton() {
		return loadButton;
	}
	
	

}
