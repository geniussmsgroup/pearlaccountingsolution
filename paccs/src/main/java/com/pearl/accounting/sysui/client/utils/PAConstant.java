package com.pearl.accounting.sysui.client.utils;

public class PAConstant {

	public static final String SYSTEM_CONFIGURATION = "System Setup & Configurations";
	public static final String Account_transactions = "Account Transactions";

	public static final String SAVE_PRODUCTSETUP = "SAVE_PRODUCTSETUP";
	public static final String GET_PRODUCTSETUP = "GET_PRODUCTSETUP";
	public static final String EDIT_PRODUCTSETUP = "EDIT_PRODUCTSETUP";
	public static final String DELETE_PRODUCTSETUP = "DELETE_PRODUCTSETUP";
	public static final String GET_TRANSML = "GET_TRANML";
	public static final String GET_Totalinterest = "GET_Totalinterest";
	public static final String SAVE_Totalinterest = "SAVE_Totalinterest";
	public static final String GET_FixedData = "GET_FixedData";
	public static final String GET_LoanRateQry = "GET_LoanRateQry";
	public static final String GET_LoanBlanceQry = "GET_LoanbalanceQry";
	public static final String GET_CurrentPeriod = "GET_CurrentPeriod";

	public static final String GET_CustomersLoanid = "GET_CustomersLoanid";
	
	
	public static final String GET_LOANREPAYMENTS = "GET_LOANREPAYMENTS";
	public static final String ACTIVE_INTERVAL_PERIOD = "ACTIVE_ACTIVE_INTERVAL_PERIOD";
	
	
	
	
	public static final String SAVE_RECEIPT = "SAVE_RECEIPT";
	public static final String GET_RECEIPT = "GET_RECEIPT";

//	public static final String SAVE_LoanDeposit = "SAVE_PRODUCTSETUP";
////	public static final String GET_LoanDeposit = "GET_PRODUCTSETUP";
//	public static final String EDIT_LoanDeposit = "EDIT_LoanDeposit";
//	public static final String DELETE_LoanDeposit = "DELETE_LoanDeposit";
//	public static final String GET_LoanDeposit= "GET_LoanDeposit";

	public static final String GET_Interval = "GET_Interval";
	public static final String SAVE_Interval = "SAVE_Interval";
	public static final String SAVE_Thead = "SAVE_Thead";
	public static final String GET_Thead = "GET_Thead";
	public static final String DELETE_Thead = "DELETE_Thead";
	public static final String SAVE_AtypeA = "SAVE_AtypeA";
	public static final String GET_AtypeA = "GET_AtypeA";
	public static final String EDIT_AtypeA = "EDIT_AtypeA";
	public static final String DELETE_AtypeA = "DELETE_AtypeA";

	public static final String SAVE_MY = "SAVE_MY";
	public static final String GET_MY = "GET_MY";
	public static final String EDIT_MY = "EDIT_MY";
	public static final String DELETE_MY = "DELETE_MY";
	public static final String DELETE_DoubleEntry = "DELETE_DoubleEntry";

	public static final String SAVE_LoanRepayment = "SAVE_LoanRepayment";
	public static final String GET_LoanRepayment = "GET_LoanRepayment";
	public static final String EDIT_LoanRepayment = "EDIT_LoanRepayment";
	public static final String DELETE_LoanRepayment = "DELETE_LoanRepayment";

	public static final String SAVE_ACCOUNTS = "SAVE_ACCOUNTS";
	public static final String GET_ACCOUNTS = "GET_ACCOUNTS";
	public static final String EDIT_ACCOUNTS = "EDIT_ACCOUNTS";
	public static final String DELETE_ACCOUNTS = "DELETE_ACCOUNTS";

	public static final String Advance_Management = "Advance Management";

	public static final String Budgets_Management = "Budgets Management";

	public static final String Loans_Management = "Savings & Loans";

	public static final String Utility_Maintenance = "Utility Maintenance";
	public static final String SAVE_PROUCTID = "SAVE_PROUCTID";

	public static final String SAVE_FinancialYear = "SAVE_FinancialYear";
	public static final String UPDATE_FinancialYear = "UPDATE_FinancialYear";
	public static final String DELETE_FinancialYear = "DELETE_FinancialYear";
	public static final String ACTIVATE_FinancialYear = "ACTIVATE_FinancialYear";
	public static final String DEACTIVATE_FinancialYear = "DEACTIVATE_FinancialYear";
	public static final String GET_FinancialYear = "GET_FinancialYear";

	public static final String SAVE_AssessmentPeriod = "SAVE_AssessmentPeriod";
	public static final String EDIT_AssessmentPeriod = "EDIT_AssessmentPeriod";
	public static final String DELETE_AssessmentPeriod = "DELETE_AssessmentPeriod";
	public static final String GET_AssessmentPeriod = "GET_AssessmentPeriod";

	public static final String ACTIVATE_AssessmentPeriod = "ACTIVATE_AssessmentPeriod";
	public static final String DEACTIVATE_AssessmentPeriod = "DEACTIVATE_AssessmentPeriod";

	public static final String SAVE_ROLE = "SAVE_ROLE";
	public static final String EDIT_ROLE = "EDIT_ROLE";
	public static final String DELETE_ROLE = "DELETE_ROLE";
	public static final String RETRIVE_ROLE = "RETRIVE_ROLE";

	public static final String SAVE_UserPermission = "SAVE_UserPermission";
	public static final String EDIT_UserPermission = "EDIT_UserPermission";
	public static final String DELETE_UserPermission = "DELETE_UserPermission";
	public static final String GET_UserPermission = "GET_UserPermission";

	public static final String SAVE_SystemUser = "SAVE_SystemUser";
	public static final String EDIT_SystemUser = "EDIT_SystemUser";
	public static final String DELETE_SystemUser = "DELETE_SystemUser";
	public static final String GET_SystemUser = "GET_SystemUser";

	public static final String RESET_PASSWORD = "RESET_PASSWORD";

	public static final String SAVE_ACCOUNT = "SAVE_ACCOUNT";
	public static final String EDIT_ACCOUNT = "EDIT_ACCOUNT";
	public static final String DELETE_ACCOUNT = "DELETE_ACCOUNT";
	public static final String GET_ACCOUNT = "GET_ACCOUNT";

	public static final String SAVE_PRODUCTID = "SAVE_PRODUCTID";
	public static final String EDIT_PRODUCTTYPE = "EDIT_PRODUCTTYPE";
	public static final String DELETE_PRODUCTTYPE = "DELETE_PRODUCTTYPE";
	public static final String GET_PRODUCTTYPE = "GET_PRODUCTTYPE";

	public static final String SAVE_customerAccount = "SAVE_customerAccount";
	public static final String EDIT_customerAccount = "EDIT_customerAccount";
	public static final String DELETE_customerAccount = "DELETE_customerAccount";
	public static final String GET_customerAccount = "GET_customerAccount";

	public static final String SAVE_PeriodicDeposit = "SAVE_PeriodicDeposit";
	public static final String EDIT_PeriodicDeposit = "EDIT_PeriodicDeposit";
	public static final String DELETE_PeriodicDeposit = "DELETE_PeriodicDeposit";
	public static final String GET_PeriodicDeposit = "GET_PeriodicDeposit";

	public static final String SAVE_LOAN = "SAVE_LOAN";
	public static final String EDIT_LOAN = "EDIT_LOAN";
	public static final String DELETE_LOAN = "DELETE_LOAN";
	public static final String GET_LOAN = "GET_LOAN";

	public static final String SAVE_WITHDRAWAL = "SAVE_WITHDRAWAL";
	public static final String EDIT_WITHDRAWAL = "EDIT_WITHDRAWAL";
	public static final String DELETE_WITHDRAWAL = "DELETE_WITHDRAWAL";
	public static final String GET_WITHDRAWAL = "GET_WITHDRAWAL";

	public static final String GET_RepaymentSchedules = "GET_RepaymentSchedules";

	public static final String SAVE_LOAN_REPAYMENT = "SAVE_LOAN_REPAYMENT";
	public static final String EDIT_LOAN_REPAYMENT = "EDIT_LOAN_REPAYMENT";
	public static final String DELETE_LOAN_REPAYMENT = "DELETE_LOAN_REPAYMENT";
	public static final String GET_LOAN_REPAYMENT = "GET_LOAN_REPAYMENT";

	public static final String GET_GENERAL_LEDGER = "GET_GENERAL_LEDGER";
	public static final String DELETE_GENERAL_LEDGER = "DELETE_GENERAL_LEDGER";

	public static final String GET_CLIENT_STATEMENT = "GET_CLIENT_STATEMENT";
	public static final String DELETE_CLIENT_STATEMENT = "DELETE_CLIENT_STATEMENT";

	public static final String SAVE_CASHBOOK_PAYMENT = "SAVE_CASHBOOK_PAYMENT";
	public static final String EDIT_CASHBOOK_PAYMENT = "EDIT_CASHBOOK_PAYMENTT";
	public static final String DELETE_CASHBOOK_PAYMENT = "DELETE_CASHBOOK_PAYMENT";
	public static final String GET_CASHBOOK_PAYMENT = "GET_CASHBOOK_PAYMENT";

	public static final String GET_CASHBOOK_PAYMENT_DETAILS = "GET_CASHBOOK_PAYMENT_DETAILS";

	public static final String SAVE_CASHBOOK_RECEIPT = "SAVE_CASHBOOK_RECEIPT";
	public static final String EDIT_CASHBOOK_RECEIPT = "EDIT_CASHBOOK_RECEIPT";
	public static final String DELETE_CASHBOOK_RECEIPT = "DELETE_CASHBOOK_RECEIPT";
	public static final String GET_CASHBOOK_RECEIPT = "GET_CASHBOOK_RECEIPT";

	public static final String GET_CASHBOOK_RECEIPT_DETAILS = "GET_CASHBOOK_RECEIPT_DETAILS";

	public static final String SAVE_Multi = "SAVE_Multi";
	public static final String EDIT_Multi = "EDIT_Multi";
	public static final String DELETE_Multi = "DELETE_Multi";
	public static final String GET_Multi = "GET__Multi";

	public static final String GET_Multi_DETAILS = "GET__Multi_DETAILS";
	public static final String GET_MULTI_DETAILS_INPUT = "GET_MULTI_JOURNAL_INPUT";

	public static final String SAVE_LOANDEPOSITS = "SAVE_MULTI_LOANDEPOSITS";
	public static final String EDIT_LOANDEPOSITS = "EDIT_LOANDEPOSITS";
	public static final String DELETE_LOANDEPOSITS = "DELETE_LOANDEPOSITS";
	public static final String GET_LOANDEPOSITS = "GET_LOANDEPOSITS";

	public static final String SAVE_MULTI_JOURNAL = "SAVE_MULTI_JOURNAL";
	public static final String EDIT_MULTI_JOURNAL = "EDIT_MULTI_JOURNAL";
	public static final String DELETE_MULTI_JOURNAL = "DELETE_MULTI_JOURNAL";
	public static final String GET_MULTI_JOURNAL = "GET_MULTI_JOURNAL";

	public static final String GET_MULTI_JOURNAL_DETAILS = "GET_MULTI_JOURNAL_DETAILS";

	public static final String SAVE_SALARY = "SAVE_SALARY";
	public static final String GET_SALARY = "GET_SALARY";

	public static final String SAVE_DOUBLEENTRY = "SAVE_DOUBLEENTRY";
	public static final String UPDATE_DOUBLEENTRY = "UPDATE_DOUBLEENTRY";
	public static final String DELETE_DOUBLEENTRY = "DELETE_DOUBLEENTRY";

	public static final String GET_MULTI_JOURNAL_DETAILS_INPUT_DATA = "GET_MULTI_JOURNAL_DETAILS_INPUT_DATA";
	public static final String EDIT_DOUBLEENTRY = "EDIT_DOUBLEENTRY";
	public static final String GET_DOUBLEENTRY = "GET_DOUBLEENTRY";
	public static final String GET_TRANSGL = "GET_TRANSGL";
	// public static final String GET_CustomerAccount = null;
	public static final String NEXT_Interval = "NEXT_Interval";

}
