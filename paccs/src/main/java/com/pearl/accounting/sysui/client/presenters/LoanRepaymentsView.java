package com.pearl.accounting.sysui.client.presenters;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.pearl.accounting.sysui.client.views.panes.LoanRepaymentsPane;
import com.smartgwt.client.widgets.layout.VLayout;

public class LoanRepaymentsView extends ViewImpl implements LoanRepaymentsPresenter.MyView {

	private static final String DEFAULT_MARGIN = "0px";
	private VLayout panel;
	private LoanRepaymentsPane LoanRepaymentsPane;

	@Inject
	public LoanRepaymentsView() {

		panel = new VLayout();

		LoanRepaymentsPane = new LoanRepaymentsPane();

		panel.setMembers(LoanRepaymentsPane);
		panel.setWidth100();
		panel.setHeight("90%");
		Window.enableScrolling(false);
		Window.setMargin(DEFAULT_MARGIN);

	}

	public Widget asWidget() {
		return panel;
	}

	public LoanRepaymentsPane getLoanRepaymentsPane() {
		return LoanRepaymentsPane;
	}

}
