package com.pearl.accounting.sysui.client.presenters;

import java.util.Date;
import java.util.LinkedHashMap;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.shared.DispatchAsync;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.AccountDefinition;
import com.pearl.accounting.sysmodel.AccountType;
import com.pearl.accounting.sysmodel.ProductSetup;
import com.pearl.accounting.sysmodel.ProductType;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysui.client.place.NameTokens;
import com.pearl.accounting.sysui.client.presenters.AccountPresenter.MyProxy;
import com.pearl.accounting.sysui.client.presenters.AccountPresenter.MyView;
import com.pearl.accounting.sysui.client.utils.PAConstant;
import com.pearl.accounting.sysui.client.views.listgrids.AccountListgrid;
import com.pearl.accounting.sysui.client.views.listgrids.ProductSetupListgrid;
import com.pearl.accounting.sysui.client.views.panes.AccountPane;
import com.pearl.accounting.sysui.client.views.panes.ProductSetupPane;
import com.pearl.accounting.sysui.client.views.widgets.SwizimaLoader;
import com.pearl.accounting.sysui.client.views.windows.AccountWindow;
import com.pearl.accounting.sysui.client.views.windows.ProductSetupWindow;
import com.pearl.accounting.sysui.shared.SysAction;
import com.pearl.accounting.sysui.shared.SysResult;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;

public class ProductSetupPresenter extends Presenter<ProductSetupPresenter.MyView, ProductSetupPresenter.MyProxy> {

	private final PlaceManager placeManager;
	private final DispatchAsync dispatcher;

	public interface MyView extends View {
		public ProductSetupPane getProductSetupPane();

	}

	@ProxyCodeSplit
	@NameToken(NameTokens.ProductSetup)
	public interface MyProxy extends ProxyPlace<ProductSetupPresenter> {
	}

	@Inject
	public ProductSetupPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
			final DispatchAsync dispatcher, final PlaceManager placeManager) {
		super(eventBus, view, proxy);

		this.dispatcher = dispatcher;
		this.placeManager = placeManager;
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this, MainPagePresenter.TYPE_SetContextAreaContent, this);
	}

	@Override
	protected void onBind() {
		super.onBind();

		onNewButtonClicked();
		onDeletebuttonClicked();
		loadAccounts();
		onLoadButtonClicked();

	}

	private void onLoadButtonClicked() {
		getView().getProductSetupPane().getLoadButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				loadAccounts();

			}
		});

		getView().getProductSetupPane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadAccounts();

			}
		});
	}

	private void onNewButtonClicked() {
		getView().getProductSetupPane().getAddButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				createAccount();

			}
		});

		getView().getProductSetupPane().getControlsPane().getNewAssetButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				createAccount();

			}
		});

	}

	private void createAccount() {
		ProductSetupWindow window = new ProductSetupWindow();		
		onSavebuttonClicked(window);		
		onSavebuttonClickedx(window);
		window.show();

	}
	private boolean FormValid(final ProductSetupWindow window) {
			if (window.getProductId().getValueAsString() != null) {
				if (window.getProductDetails().getValueAsString() != null) {
					if (window.getRemarks().getValueAsString() != null) {
						return true;
					} else {
						SC.warn("ERROR", "Please enter Remarks");

					}
				} else {
					SC.warn("ERROR", "Please enter ProductDetails");
				}
			} else {
				SC.warn("ERROR", "Please enter productId");
			}
		
		return false;
	}

	private void ClearForm(final ProductSetupWindow window) {
		window.getProductId().clearValue();
		window.getProductDetails().clearValue();
		window.getRemarks().clearValue();

	}

	private void onSavebuttonClicked(final ProductSetupWindow window) {
		window.getSaveButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				if (FormValid(window)) {
					ProductSetup account = new ProductSetup();
					account.setProductId(window.getProductId().getValueAsString());
					
					account.setProductDetails(window.getProductDetails().getValueAsString());
					account.setRemarks(window.getRemarks().getValueAsString());

//					account.setDateCreated(new Date());
//					account.setDateUpdated(new Date());
//					account.setStatus(Status.ACTIVE);
//					


					// account.setUpdatedBy(updatedBy);

					SC.showPrompt("", "", new SwizimaLoader());

					dispatcher.execute(new SysAction(PAConstant.SAVE_PRODUCTSETUP, account), new AsyncCallback<SysResult>() {
						public void onFailure(Throwable caught) {
							System.out.println(caught.getMessage());
						}

						public void onSuccess(SysResult result) {

							SC.clearPrompt();

							if (result != null) {

								if (result.getSwizFeedback().isResponse()) {
									ClearForm(window);
									SC.say("SUCCESS", result.getSwizFeedback().getMessage());

									getView().getProductSetupPane().getProductSetupListgrid()
											.addRecordsToGrid(result.getProductSetups());
								} else {
									SC.warn("ERROR", result.getSwizFeedback().getMessage());
								}

							} else {
								SC.say("ERROR", "Unknow error");
							}

						}
					});
				}
			}
		});
	}

	
	private void onSavebuttonClickedx(final ProductSetupWindow window) {
		window.getSaveButtonx().addClickHandler(new ClickHandler() {


			public void onClick(ClickEvent event) {
				if (FormValid(window)) {
					ProductSetup account = new ProductSetup();
					account.setProductId(window.getProductId().getValueAsString());
					
					account.setProductDetails(window.getProductDetails().getValueAsString());
					account.setRemarks(window.getRemarks().getValueAsString());

//					account.setDateCreated(new Date());
//					account.setDateUpdated(new Date());
//					account.setStatus(Status.ACTIVE);
//					


					// account.setUpdatedBy(updatedBy);

					SC.showPrompt("", "", new SwizimaLoader());

					dispatcher.execute(new SysAction(PAConstant.SAVE_PRODUCTSETUP, account), new AsyncCallback<SysResult>() {
						public void onFailure(Throwable caught) {
							System.out.println(caught.getMessage());
						}

						public void onSuccess(SysResult result) {

							SC.clearPrompt();

							if (result != null) {

								if (result.getSwizFeedback().isResponse()) {
									ClearForm(window);
									SC.say("SUCCESS", result.getSwizFeedback().getMessage());

									getView().getProductSetupPane().getProductSetupListgrid()
											.addRecordsToGrid(result.getProductSetups());
								} else {
									SC.warn("ERROR", result.getSwizFeedback().getMessage());
								}

							} else {
								SC.say("ERROR", "Unknow error");
							}

						}
					});
				}
			}
		});
	}

	private void loadAccounts() {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_PRODUCTSETUP), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					getView().getProductSetupPane().getProductSetupListgrid().addRecordsToGrid(result.getProductSetups());

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void onDeletebuttonClicked() {
		getView().getProductSetupPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				delete();

			}
		});

		getView().getProductSetupPane().getControlsPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				delete();

			}
		});
	}

	private void delete() {
		if (getView().getProductSetupPane().getProductSetupListgrid().anySelected()) {

			SC.ask("Confirm", "Are you sure you want to delete the selected record?", new BooleanCallback() {

				public void execute(Boolean value) {

					if (value) {

						ProductSetup account = new ProductSetup();
						account.setProductId(getView().getProductSetupPane().getProductSetupListgrid().getSelectedRecord()
								.getAttribute(ProductSetupListgrid.ID));

						SC.showPrompt("", "", new SwizimaLoader());

						dispatcher.execute(new SysAction(PAConstant.DELETE_PRODUCTSETUP, account),
								new AsyncCallback<SysResult>() {
									public void onFailure(Throwable caught) {
										System.out.println(caught.getMessage());
									}

									public void onSuccess(SysResult result) {

										SC.clearPrompt();

										if (result != null) {

											if (result.getSwizFeedback().isResponse()) {

												SC.say("SUCCESS", result.getSwizFeedback().getMessage());

												getView().getProductSetupPane().getProductSetupListgrid()
														.addRecordsToGrid(result.getProductSetups());
											} else {
												SC.warn("ERROR", result.getSwizFeedback().getMessage());
											}

										} else {
											SC.say("ERROR", "Unknow error");
										}

									}
								});

					}

				}
			});

		} else {
			SC.say("ERROR", "ERROR: Please select record to delete?");
		}
	}

}
