package com.pearl.accounting.sysui.client.presenters;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.pearl.accounting.sysui.client.views.panes.AccountPane;
import com.pearl.accounting.sysui.client.views.panes.ProductSetupPane;
import com.smartgwt.client.widgets.layout.VLayout;

public class ProductSetupView extends ViewImpl implements ProductSetupPresenter.MyView {

	private static final String DEFAULT_MARGIN = "0px";
	private VLayout panel;
	private ProductSetupPane productsetupPane;

	@Inject
	public ProductSetupView() {

		panel = new VLayout();

		productsetupPane = new ProductSetupPane();

		panel.setMembers(productsetupPane);
		panel.setWidth100();
		panel.setHeight("90%");
		Window.enableScrolling(false);
		Window.setMargin(DEFAULT_MARGIN);

	}

	public Widget asWidget() {
		return panel;
	}

	public ProductSetupPane getProductSetupPane() {
		return productsetupPane;
	}
	
	

}
