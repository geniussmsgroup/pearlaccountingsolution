package com.pearl.accounting.sysui.client.presenters;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.shared.DispatchAsync;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.AssessmentPeriod;
import com.pearl.accounting.sysmodel.CashBookPayment;
import com.pearl.accounting.sysmodel.CashBookReceipt;
import com.pearl.accounting.sysmodel.CashBookReceiptDetail;
import com.pearl.accounting.sysmodel.DataSourceScreen;
import com.pearl.accounting.sysmodel.FinancialYear;
import com.pearl.accounting.sysmodel.Intervaltb;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.Theadp;
import com.pearl.accounting.sysui.client.place.NameTokens;
import com.pearl.accounting.sysui.client.utils.PAConstant;
import com.pearl.accounting.sysui.client.utils.PAManager;
import com.pearl.accounting.sysui.client.views.listgrids.CashBookReceiptDetailListgrid;
import com.pearl.accounting.sysui.client.views.listgrids.CashBookReceiptListgrid;
import com.pearl.accounting.sysui.client.views.panes.CashBookReceiptPane;
import com.pearl.accounting.sysui.client.views.widgets.SwizimaLoader;
import com.pearl.accounting.sysui.client.views.windows.CashBookPaymentWindow;
import com.pearl.accounting.sysui.client.views.windows.CashBookReceiptWindow;
import com.pearl.accounting.sysui.shared.SysAction;
import com.pearl.accounting.sysui.shared.SysResult;
import com.smartgwt.client.docs.DateInputFormat;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.EditorExitEvent;
import com.smartgwt.client.widgets.form.fields.events.EditorExitHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordExpandEvent;
import com.smartgwt.client.widgets.grid.events.RecordExpandHandler;

public class CashBookReceiptPresenter
		extends Presenter<CashBookReceiptPresenter.MyView, CashBookReceiptPresenter.MyProxy> {

	private final PlaceManager placeManager;
	private final DispatchAsync dispatcher;

	public interface MyView extends View {

		public CashBookReceiptPane getCashBookReceiptPane();

	}

	@ProxyCodeSplit
	@NameToken(NameTokens.cashbookreceipts)
	public interface MyProxy extends ProxyPlace<CashBookReceiptPresenter> {
	}

	@Inject
	public CashBookReceiptPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
			final DispatchAsync dispatcher, final PlaceManager placeManager) {
		super(eventBus, view, proxy);

		this.dispatcher = dispatcher;
		this.placeManager = placeManager;
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this, MainPagePresenter.TYPE_SetContextAreaContent, this);
	}

	@Override
	protected void onBind() {
		super.onBind();

		onNewButtonClicked();

		loadReceipts();

		onLoadButtonaclicked();

		onRecordExpanded();
		onExportButtonClicked();

	}

	private void onDateLoaded(final CashBookReceiptWindow window) {

		window.getPaymentDate().addChangedHandler(new ChangedHandler() {

			public void onChanged(ChangedEvent event) {

				dispatcher.execute(new SysAction(PAConstant.GET_Interval), new AsyncCallback<SysResult>() {
					public void onFailure(Throwable caught) {
						System.out.println(caught.getMessage());
					}

					public void onSuccess(SysResult result) {
						// TODO Auto-generated method stub
						if (result != null) {
//								
							for (Intervaltb period : result.getIntervaltbs()) {

								final Date dn = window.getPaymentDate().getValueAsDate();
								if (dn.before(period.getFEND()) && dn.after(period.getFSTART())) {
									window.getAssessmentPeriod().setValue(period.getFINTNAME());
									window.getFinancialYear().setValue(period.getYRCODE());
									window.getFrom().setValue(period.getFSTART());
									window.getTo().setValue(period.getFEND());
									window.getPeriodcode().setValue(period.getFINTCODE());

								}
							}
						}
					}
				});
			}
		});
	}

	private void onNewButtonClicked() {
		getView().getCashBookReceiptPane().getAddButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				addNewCashBookReceipt();

			}
		});

		getView().getCashBookReceiptPane().getControlsPane().getNewAssetButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				addNewCashBookReceipt();

			}
		});
	}

	private void addNewCashBookReceipt() {

		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_ACCOUNT), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					CashBookReceiptWindow window = new CashBookReceiptWindow(result.getAccounts());

					loadAccounts(window);
//					loadPeriods(window);
//					loadFinancialYears(window);

					addPaymentDetails(window);
					onloadcurrentperiod(window);
					onSaveButtonClicked(window);
					onDeleteButtonPressed(window);
					onDateLoaded(window);
					// onActive(window);

					window.animateShow();

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

	}

	private void onloadcurrentperiod(final CashBookReceiptWindow window) {
		dispatcher.execute(new SysAction(PAConstant.GET_CurrentPeriod), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {
				// TODO Auto-generated method stub
				if (result != null) {
//						
					for (Intervaltb period : result.getIntervaltbs()) {

							window.getCurrentperiod().setValue(period.getFINTNAME());
							//window.getYear().setValue(period.getYRCODE());
							window.getCurrentstartdate().setValue(period.getFSTART());
							
							window.getCurrentenddate().setValue(period.getFEND());
							//period.window.getPeriodcode().setValue(period.getFINTCODE());
					
				}}
			}
		});
	}

	private void loadFinancialYears(final CashBookReceiptWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_FinancialYear), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

					for (FinancialYear financialYear : result.getFinancialYears()) {

						valueMap.put(financialYear.getId(), financialYear.getFinancialYear());
					}

					window.getFinancialYear().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void loadAccounts(final CashBookReceiptWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_ACCOUNT), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
					for (Account account : result.getAccounts()) {
						valueMap.put(account.getSysid(), account.getAccountName());
					}
					window.getReceivingAccount().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void onDeleteButtonPressed(final CashBookReceiptWindow window) {
		window.getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if (window.getCashBookReceiptDetailListgrid().anySelected()) {
					SC.ask("Are you sure you want to delete the selected record", new BooleanCallback() {

						public void execute(Boolean value) {
							// TODO Auto-generated method stub
							if (value == true) {
								for (ListGridRecord record : window.getCashBookReceiptDetailListgrid()
										.getSelectedRecords()) {
									window.getCashBookReceiptDetailListgrid().removeData(record);
								}

							}
						}
					});

				} else {
					SC.say("ERROR", "please select record to delete");
				}

			}
		});

	}

	private void addPaymentDetails(final CashBookReceiptWindow window) {
		window.getAddButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				CashBookReceiptDetail receiptDetail = new CashBookReceiptDetail();

				window.getCashBookReceiptDetailListgrid().addRecordToGrid(receiptDetail);

			}
		});

	}

	private boolean FormValid(final CashBookReceiptWindow window) {
		if (window.getAssessmentPeriod().getValueAsString() != null) {

			if (window.getFinancialYear().getValueAsString() != null) {

				if (window.getPaidAmount().getValueAsString() != null) {
					if (window.getRemarks().getValueAsString() != null) {
						if (window.getPayee().getValueAsString() != null) {
							if (window.getReceivingAccount().getValueAsString() != null) {

								if (window.getCashBookReceiptDetailListgrid().getRecords().length > 0) {

									float OverallTotal = 0;

									for (ListGridRecord record : window.getCashBookReceiptDetailListgrid()
											.getRecords()) {
										OverallTotal += Float.parseFloat(
												record.getAttributeAsString(CashBookReceiptDetailListgrid.AmountPaid));
									}
									float total = Float.parseFloat(window.getPaidAmount().getValueAsString());

									if (OverallTotal == total) {
										return true;
									} else {
										SC.warn("Error", "Please amount values dont match");

									}
								} else {
									SC.warn("Error", "Please enter atleast one paying account");

								}
							} else {
								SC.warn("Error", "Please enter receiving account");
							}
						} else {
							SC.warn("Error", "Please enter Payee");
						}

					} else {
						SC.warn("Error", "Please enter Remarks");

					}
				} else {
					SC.warn("Error", "Please enter Amount");
				}

			} else {
				SC.warn("Error", "Please select Financial Year");

			}
		} else {
			SC.warn("Error", "Please Select Period");

		}
		return false;
	}

	private void onSaveButtonClicked(final CashBookReceiptWindow window) {
		window.getSaveButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				if (FormValid(window)) {

					FinancialYear financialYear = new FinancialYear();
					financialYear.setId(window.getFinancialYear().getValueAsString());

					AssessmentPeriod assessmentPeriod = new AssessmentPeriod();
					assessmentPeriod.setId(window.getAssessmentPeriod().getValueAsString());

					String paymentFrom = window.getPayee().getValueAsString();

					float amountReceived = Float.parseFloat(
							PAManager.getInstance().unformatCash(window.getPaidAmount().getValueAsString()));

//					DateInputFormat dateFormatter = DateInputFormat("dd.MM.yyyy");
//			        String format = dateFormatter.format(date);
//			        return format;

					Date paymentDate = window.getPaymentDate().getValueAsDate();

					Account receivingAccount = new Account();
					receivingAccount.setSysid(window.getReceivingAccount().getValueAsString());

					String remarks = window.getRemarks().getValueAsString();
					String receiptNumber = window.getPaymentDetails().getValueAsString();
					// Save to theadp;
					
					// String transactionNumber;

					CashBookReceipt cashBookReceipt = new CashBookReceipt();
					cashBookReceipt.setAmountReceived(amountReceived);
					cashBookReceipt.setPeriod(window.getAssessmentPeriod().getValueAsString());
					int year = Integer.parseInt(window.getFinancialYear().getValueAsString());

					 cashBookReceipt.setPeriodcode(window.getPeriodcode().getValueAsString());;
					cashBookReceipt.setDateCreated(new Date());
					cashBookReceipt.setDateUpdated(new Date());
					cashBookReceipt.setYear(year);
					cashBookReceipt.setPaymentfrom(paymentFrom);
					cashBookReceipt.setReceivingAccount(receivingAccount);
					cashBookReceipt.setPaymentDate(paymentDate);
					cashBookReceipt.setRemarks(remarks);
					cashBookReceipt.setStatus(Status.ACTIVE);
					// cashBookPayment.setTransactionNumber(transactionNumber);
					// cashBookPayment.setUpdatedBy(updatedBy);
					cashBookReceipt.setReceiptNumber(receiptNumber);

					List<CashBookReceiptDetail> cashBookReceiptDetails = new ArrayList<CashBookReceiptDetail>();

					for (ListGridRecord record : window.getCashBookReceiptDetailListgrid().getRecords()) {

						Account payingAccount = new Account();
						payingAccount.setSysid(record.getAttribute(CashBookReceiptDetailListgrid.Account));

						float paidAmount = Float.parseFloat(PAManager.getInstance()
								.unformatCash(record.getAttribute(CashBookReceiptDetailListgrid.AmountPaid)));
						Date payDate = window.getPaymentDate().getValueAsDate();

						String details = window.getRemarks().getValueAsString() + " "
								+ record.getAttribute(CashBookReceiptDetailListgrid.Remarks);

						CashBookReceiptDetail cashBookPaymentDetail = new CashBookReceiptDetail();

						cashBookPaymentDetail.setCashBookReceipt(cashBookReceipt);
						 cashBookPaymentDetail.setPeriodcode(window.getPeriodcode().getValueAsString());;
						cashBookPaymentDetail.setDateCreated(new Date());
						cashBookPaymentDetail.setDateUpdated(new Date());

						cashBookPaymentDetail.setPaymentDate(payDate);
						cashBookPaymentDetail.setAmountPaid(paidAmount);
						cashBookPaymentDetail.setPayingAccount(payingAccount);
						cashBookPaymentDetail.setRemarks(details);
						cashBookPaymentDetail.setStatus(Status.ACTIVE);

						cashBookReceiptDetails.add(cashBookPaymentDetail);

					}
					cashBookReceipt.setCashBookReceiptDetails(cashBookReceiptDetails);
					SC.showPrompt("", "", new SwizimaLoader());

					dispatcher.execute(new SysAction(PAConstant.SAVE_CASHBOOK_RECEIPT, cashBookReceipt),
							new AsyncCallback<SysResult>() {
								public void onFailure(Throwable caught) {
									System.out.println(caught.getMessage());
								}

								public void onSuccess(SysResult result) {

									ClearForm(window);

									SC.clearPrompt();

									if (result != null) {

										if (result.getSwizFeedback().isResponse()) {

											SC.say("SUCCESS", result.getSwizFeedback().getMessage());

											getView().getCashBookReceiptPane().getCashBookReceiptListgrid()
													.addRecordsToGrid(result.getCashBookReceipts());
										} else {
											SC.warn("ERROR", result.getSwizFeedback().getMessage());
										}
									} else {
										SC.say("ERROR", "Unknow error");
									}
								}
							});
				}
			}
		});
	}

	private void ClearForm(final CashBookReceiptWindow window) {
		window.getPaidAmount().clearValue();
		window.getRemarks().clearValue();
		window.getPayee().clearValue();
		window.getReceivingAccount().clearValue();

	}

	private void onLoadButtonaclicked() {
		getView().getCashBookReceiptPane().getLoadButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadReceipts();

			}
		});

		getView().getCashBookReceiptPane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadReceipts();

			}
		});

	}

	private void loadReceipts() {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_CASHBOOK_RECEIPT), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					getView().getCashBookReceiptPane().getCashBookReceiptListgrid()
							.addRecordsToGrid(result.getCashBookReceipts());

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void onRecordExpanded() {
		getView().getCashBookReceiptPane().getCashBookReceiptListgrid()
				.addRecordExpandHandler(new RecordExpandHandler() {

					public void onRecordExpand(RecordExpandEvent event) {

						getView().getCashBookReceiptPane().getCashBookReceiptListgrid().deselectAllRecords();
						getView().getCashBookReceiptPane().getCashBookReceiptListgrid().selectRecord(event.getRecord());

						CashBookReceiptDetailListgrid listgrid = new CashBookReceiptDetailListgrid();

						getView().getCashBookReceiptPane().getCashBookReceiptListgrid().setListgrid(listgrid);

						CashBookReceipt cashBookReceipt = new CashBookReceipt();
						cashBookReceipt.setId(getView().getCashBookReceiptPane().getCashBookReceiptListgrid()
								.getSelectedRecord().getAttribute(CashBookReceiptListgrid.ID));

						SC.showPrompt("", "", new SwizimaLoader());

						dispatcher.execute(new SysAction(PAConstant.GET_CASHBOOK_RECEIPT_DETAILS, cashBookReceipt),
								new AsyncCallback<SysResult>() {
									public void onFailure(Throwable caught) {
										System.out.println(caught.getMessage());
									}

									public void onSuccess(SysResult result) {

										SC.clearPrompt();

										if (result != null) {

											getView().getCashBookReceiptPane().getCashBookReceiptListgrid()
													.getListgrid().addRecordsToGrid(result.getBookReceiptDetails());

										} else {
											SC.say("ERROR", "Unknow error");
										}

									}
								});

					}
				});
	}

	
	
	private void onExportButtonClicked() {
		getView().getCashBookReceiptPane().getControlsPane().getPrintButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				String request = "CASHBOOK_Receipt_EXPORT";

				String NAME = "_blank";
				String FEATURES = "width=760, height=480";

				StringBuilder url = new StringBuilder();
				url.append("dataExport").append("?");
				url.append("&" + "request" + "=" + request);
				Window.open(url.toString(), NAME, FEATURES);
				

			}
		});
	}
}
