package com.pearl.accounting.sysui.client.data;

import com.pearl.accounting.sysui.client.place.NameTokens;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class SavingsCreditData {


	private static ListGridRecord[] records;

	public static ListGridRecord[] getRecords() {
		if (records == null) {
			records = getNewRecords();

		}
		return records;

	}

	public static ListGridRecord createRecord(String pk, String icon, String name) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute("pk", pk);
		record.setAttribute("icon", icon);
		record.setAttribute("name", name);
		return record;
	}

	public static ListGridRecord[] getNewRecords() {

		return new ListGridRecord[] {
				createRecord("", "application_form",NameTokens.notifications),
				createRecord("", "application_form",NameTokens.customers),
				//createRecord("", "application_form",NameTokens.periodicdeposits),
			//	createRecord("", "application_form",NameTokens.savingswithdrawals),
				createRecord("", "application_form",NameTokens.loans),
				createRecord("", "application_form",NameTokens.LoanDeposit),
				createRecord("", "application_form",NameTokens.LoanRepayments),
				createRecord("", "application_form",NameTokens.TransML),

			//	createRecord("", "application_form",NameTokens.loanRepayment2),
			//	createRecord("","application_form",NameTokens.multi),
				createRecord("", "application_form",NameTokens.generalledger),
				createRecord("", "application_form",NameTokens.clientstatement)
				
		};

	}
}
