package com.pearl.accounting.sysui.client.views.panes;

import com.pearl.accounting.sysui.client.views.listgrids.LoanRepaymentsListgrid;
import com.pearl.accounting.sysui.client.views.widgets.ControlsPane;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class LoanRepaymentsPane extends VLayout {

	private ControlsPane controlsPane;

	private LoanRepaymentsListgrid LoanRepaymentsListgrid;

	private IButton loadButton;

	public LoanRepaymentsPane() {

		super();
		Label header = new Label();
		header.setStyleName("crm-ContextArea-Header-Label");
		header.setContents("Loan Repayments");
		header.setWidth("100%");
		header.setAutoHeight();
		header.setMargin(10);
		header.setAlign(Alignment.LEFT);

		controlsPane = new ControlsPane();

		LoanRepaymentsListgrid = new LoanRepaymentsListgrid();

		loadButton = new IButton("Load");

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(loadButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(10);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(controlsPane);
		layout.addMember(header);
		layout.addMember(LoanRepaymentsListgrid);
		layout.addMember(buttonLayout);
		this.addMember(layout);

	}

	public ControlsPane getControlsPane() {
		return controlsPane;
	}

	public LoanRepaymentsListgrid getLoanRepaymentsListgrid() {
		return LoanRepaymentsListgrid;
	}

	public IButton getLoadButton() {
		return loadButton;
	}
	
	

}
