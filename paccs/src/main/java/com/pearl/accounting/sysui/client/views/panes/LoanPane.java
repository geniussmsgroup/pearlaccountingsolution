package com.pearl.accounting.sysui.client.views.panes;

import com.pearl.accounting.sysui.client.views.listgrids.LoanListgrid;
import com.pearl.accounting.sysui.client.views.widgets.ControlsPane;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class LoanPane extends VLayout {

	private ControlsPane controlsPane;

	private LoanListgrid loanListgrid;
	//private IButton interest;

	private IButton addButton;
	private IButton editButton;
	private IButton deleteButton;
	private IButton loadButton;
	private IButton LoadInterest;

	private IButton paymentButton;

	public LoanPane() {

		super();
		Label header = new Label();
		header.setStyleName("crm-ContextArea-Header-Label");
		header.setContents("Loan disbursement");
		header.setWidth("100%");
		header.setAutoHeight();
		header.setMargin(10);
		header.setAlign(Alignment.LEFT);

		controlsPane = new ControlsPane();

		loanListgrid = new LoanListgrid();

		addButton = new IButton("New");
		editButton = new IButton("Edit");
		deleteButton = new IButton("Delete");
		loadButton = new IButton("Refresh");
	//	interest = new IButton("Interest");
		LoadInterest =new IButton("Day Compute interest");

		paymentButton = new IButton("Payments");

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(addButton, editButton, deleteButton, paymentButton, loadButton,LoadInterest);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(10);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(controlsPane);
		layout.addMember(header);
		layout.addMember(loanListgrid);
		layout.addMember(buttonLayout);
		this.addMember(layout);

	}

	public ControlsPane getControlsPane() {
		return controlsPane;
	}

	public LoanListgrid getLoanListgrid() {
		return loanListgrid;
	}

	public IButton getAddButton() {
		return addButton;
	}
	public IButton getLoadInterest() {
		return LoadInterest;
	}
	public IButton getEditButton() {
		return editButton;
	}

	public IButton getDeleteButton() {
		return deleteButton;
	}

	public IButton getLoadButton() {
		return loadButton;
	}

	public IButton getPaymentButton() {
		return paymentButton;
	}

//	public IButton getInterest() {
//		return interest;
//	}
//
//	public void setInterest(IButton interest) {
//		this.interest = interest;
//	}

}
