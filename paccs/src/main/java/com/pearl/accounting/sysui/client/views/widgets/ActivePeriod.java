package com.pearl.accounting.sysui.client.views.widgets;

import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class ActivePeriod extends DynamicForm {
	TextItem period;
	TextItem fro;
	TextItem t;
	TextItem y;
	public ActivePeriod(){
		super();
		 period = new TextItem();
		 fro = new TextItem();
		 t = new TextItem();
		 y = new TextItem();
		
		period.setTitle("Active Period");
		fro.setTitle("From");
		t.setTitle("to");
		y.setTitle("Financial Year");

		this.setFields(period,y,fro,t);
		this.setNumCols(8);
		
	}
	public TextItem getPeriod() {
		return period;
	}
	public void setPeriod(TextItem period) {
		this.period = period;
	}
	public TextItem getFro() {
		return fro;
	}
	public void setFro(TextItem fro) {
		this.fro = fro;
	}
	public TextItem getT() {
		return t;
	}
	public void setT(TextItem t) {
		this.t = t;
	}
	public TextItem getY() {
		return y;
	}
	public void setY(TextItem y) {
		this.y = y;
	}
	

}