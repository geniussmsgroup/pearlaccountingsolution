package com.pearl.accounting.sysui.client.views.panes;

import com.pearl.accounting.sysui.client.views.listgrids.LoanbalanceListgrid;
import com.pearl.accounting.sysui.client.views.widgets.ControlsPane;
import com.pearl.accounting.sysui.client.views.widgets.TextField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class LoanBalancePane extends VLayout {

	private ControlsPane controlsPane;

	private LoanbalanceListgrid loanbalanceListgrid;
	private IButton addButton;
//	private IButton editButton;
//	private IButton deleteButton;

	private IButton loadButton;
	

//	private IButton activateButton;
//	private IButton deactivaeButton;

	public LoanBalancePane() {

		super();
		Label header = new Label();
		header.setStyleName("crm-ContextArea-Header-Label");
		header.setContents("Loanbalance");
		header.setWidth("100%");
		header.setAutoHeight();
		header.setMargin(10);
		header.setAlign(Alignment.LEFT);

		controlsPane = new ControlsPane();

		loanbalanceListgrid = new LoanbalanceListgrid();
		addButton = new IButton("compute interest");
		

//		deleteButton = new IButton("Delete");
		loadButton = new IButton("Refresh");
		
		HLayout buttonLayout = new HLayout();
       // buttonLayout.addMembers(form4,form3);	
		buttonLayout.setMembers(loadButton, addButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(10);
		buttonLayout.setMembersMargin(4);

//		HLayout buttonLayout2 = new HLayout();
//		///buttonLayout2.setMembers();
//		buttonLayout2.addMembers(form3, form4);
//		buttonLayout2.setAutoHeight();
//		buttonLayout2.setWidth100();
//		buttonLayout2.setMargin(10);
//		buttonLayout2.setMembersMargin(4);
		VLayout layout = new VLayout();
		layout.addMember(controlsPane);
		layout.addMember(header);
		layout.addMember(loanbalanceListgrid);
		///layout.addMembers();
		layout.addMember(buttonLayout);
		this.addMember(layout);

	}

	public ControlsPane getControlsPane() {
		return controlsPane;
	}

	public LoanbalanceListgrid getLoanbalanceListgrid() {
		return loanbalanceListgrid;
	}

	public IButton getLoadButton() {
		return loadButton;
	}

	public IButton getAddButton() {
		return addButton;
	}



}
