//package com.pearl.accounting.sysui.client.presenters;
//
//import java.util.Date;
//import java.util.LinkedHashMap;
//
//import com.google.gwt.user.client.rpc.AsyncCallback;
//import com.google.inject.Inject;
//import com.google.web.bindery.event.shared.EventBus;
//import com.gwtplatform.dispatch.shared.DispatchAsync;
//import com.gwtplatform.mvp.client.Presenter;
//import com.gwtplatform.mvp.client.View;
//import com.gwtplatform.mvp.client.annotations.NameToken;
//import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
//import com.gwtplatform.mvp.client.proxy.PlaceManager;
//import com.gwtplatform.mvp.client.proxy.ProxyPlace;
//import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
//import com.pearl.accounting.sysmodel.Account;
//import com.pearl.accounting.sysmodel.AccountDefinition;
//import com.pearl.accounting.sysmodel.AccountType;
//import com.pearl.accounting.sysmodel.AtypeA;
//import com.pearl.accounting.sysmodel.Init;
//import com.pearl.accounting.sysmodel.ProductID;
//import com.pearl.accounting.sysmodel.ProductSetup;
//import com.pearl.accounting.sysmodel.ProductType;
//import com.pearl.accounting.sysmodel.Status;
//import com.pearl.accounting.sysui.client.place.NameTokens;
//import com.pearl.accounting.sysui.client.presenters.AccountPresenter.MyProxy;
//import com.pearl.accounting.sysui.client.presenters.AccountPresenter.MyView;
//import com.pearl.accounting.sysui.client.utils.PAConstant;
//import com.pearl.accounting.sysui.client.views.listgrids.AccountListgrid;
//import com.pearl.accounting.sysui.client.views.listgrids.AccountSListgrid;
//import com.pearl.accounting.sysui.client.views.panes.AccountPane;
//import com.pearl.accounting.sysui.client.views.panes.AccountSPane;
//import com.pearl.accounting.sysui.client.views.widgets.SwizimaLoader;
//import com.pearl.accounting.sysui.client.views.windows.AccountSWindow;
//import com.pearl.accounting.sysui.client.views.windows.AccountWindow;
//import com.pearl.accounting.sysui.client.views.windows.DoubleEntryWindow;
//import com.pearl.accounting.sysui.shared.SysAction;
//import com.pearl.accounting.sysui.shared.SysResult;
//import com.smartgwt.client.util.BooleanCallback;
//import com.smartgwt.client.util.SC;
//import com.smartgwt.client.widgets.events.ClickEvent;
//import com.smartgwt.client.widgets.events.ClickHandler;
//
//public class AccountSPresenter extends Presenter<AccountSPresenter.MyView, AccountSPresenter.MyProxy> {
//
//	private final PlaceManager placeManager;
//	private final DispatchAsync dispatcher;
//
//	public interface MyView extends View {
//		public AccountSPane getAccountSPane();
//
//	}
//
//	@ProxyCodeSplit
//	@NameToken(NameTokens.ACCOUNTS)
//	public interface MyProxy extends ProxyPlace<AccountSPresenter> {
//	}
//
//	@Inject
//	public AccountSPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
//			final DispatchAsync dispatcher, final PlaceManager placeManager) {
//		super(eventBus, view, proxy);
//
//		this.dispatcher = dispatcher;
//		this.placeManager = placeManager;
//	}
//
//	@Override
//	protected void revealInParent() {
//		RevealContentEvent.fire(this, MainPagePresenter.TYPE_SetContextAreaContent, this);
//	}
//
//	@Override
//	protected void onBind() {
//		super.onBind();
//
//		onNewButtonClicked();
//		onDeletebuttonClicked();
//		loadAccounts();
//		onLoadButtonClicked();
//
//	}
//
//	private void onLoadButtonClicked() {
//		getView().getAccountSPane().getLoadButton().addClickHandler(new ClickHandler() {
//
//			public void onClick(ClickEvent event) {
//				loadAccounts();
//
//			}
//		});
//
//		getView().getAccountSPane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {
//
//			public void onClick(ClickEvent event) {
//
//				loadAccounts();
//
//			}
//		});
//	}
//
//	private void onNewButtonClicked() {
//		getView().getAccountSPane().getAddButton().addClickHandler(new ClickHandler() {
//
//			public void onClick(ClickEvent event) {
//
//				createAccount();
//
//			}
//		});
//
//		getView().getAccountSPane().getControlsPane().getNewAssetButton().addClickHandler(new ClickHandler() {
//
//			public void onClick(ClickEvent event) {
//
//				createAccount();
//
//			}
//		});
//
//	}
//
//	private void createAccount() {
//		AccountSWindow window = new AccountSWindow();
//		loadAccountTypes(window);
//		loadAccountCategories(window);
//		loadAnalReq(window);
//		loadPdata(window);
//		loadProductID(window);
//		onSavebuttonClicked(window);
//		loadAccountTypes2(window);
//		window.show();
//
//	}
//
//	private void loadAccountCategories(AccountSWindow window) {
//
//		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
//		for (ProductID productid : ProductID.values()) {
//			valueMap.put(productid.getProduct(), productid.getProduct());
//		}
//
//		window.getCategory().setValueMap(valueMap);
//
//	}
//
//	private void loadAnalReq(AccountSWindow window) {
//		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
//
//		valueMap.put("Y", "Y");
//		valueMap.put("N", "N");
//
//		window.getAnalReq().setValueMap(valueMap);
//
//	}
//
//	private void loadPdata(AccountSWindow window) {
//		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
//
//		valueMap.put("Y", "Y");
//		valueMap.put("N", "N");
//
//		window.getPdata().setValueMap(valueMap);
//	}
//
//	private void loadAccountTypes(final AccountSWindow window) {
//		SC.showPrompt("", "", new SwizimaLoader());
//
//		dispatcher.execute(new SysAction(PAConstant.GET_AtypeA), new AsyncCallback<SysResult>() {
//			public void onFailure(Throwable caught) {
//				System.out.println(caught.getMessage());
//			}
//
//			public void onSuccess(SysResult result) {
//
//				SC.clearPrompt();
//
//				if (result != null) {
//
//					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
//					for (AtypeA account : result.getAtypeAs()) {
//						valueMap.put(account.getId(), account.getAtypecd());
//					}
//
//					window.getAccountType().setValueMap(valueMap);
//
//				} else {
//					SC.say("ERROR", "Unknow error");
//				}
//
//			}
//		});
//
//	}
//
//	private void loadAccountTypes2(final AccountSWindow window) {
//		SC.showPrompt("", "", new SwizimaLoader());
//
//		dispatcher.execute(new SysAction(PAConstant.GET_AtypeA), new AsyncCallback<SysResult>() {
//			public void onFailure(Throwable caught) {
//				System.out.println(caught.getMessage());
//			}
//
//			public void onSuccess(SysResult result) {
//
//				SC.clearPrompt();
//
//				if (result != null) {
//
//					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
//					for (AtypeA account : result.getAtypeAs()) {
//						valueMap.put(account.getId(), account.getAtypenme());
//					}
//
//					window.getAccountType().setValueMap(valueMap);
//
//				} else {
//					SC.say("ERROR", "Unknow error");
//				}
//
//			}
//		});
//
//	}
//
//	private void loadProductID(final AccountSWindow window) {
//		SC.showPrompt("", "", new SwizimaLoader());
//
//		dispatcher.execute(new SysAction(PAConstant.GET_PRODUCTSETUP), new AsyncCallback<SysResult>() {
//			public void onFailure(Throwable caught) {
//				System.out.println(caught.getMessage());
//			}
//
//			public void onSuccess(SysResult result) {
//
//				SC.clearPrompt();
//
//				if (result != null) {
//
//					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
//					for (ProductSetup account : result.getProductSetups()) {
//						valueMap.put(account.getId(), account.getProductDetails());
//					}
//
//					window.getProductType().setValueMap(valueMap);
//
//				} else {
//					SC.say("ERROR", "Unknow error");
//				}
//
//			}
//		});
//
//	}
//
//	private boolean FormValid(final AccountSWindow window) {
//		if (window.getAccountCode().getValueAsString() != null) {
//			if (window.getAccountName().getValueAsString() != null) {
//				if (window.getAccountType().getValueAsString() != null) {
//					if (window.getCategory().getValueAsString() != null) {
//						if (window.getPdata().getValueAsString().equalsIgnoreCase("N")
//								&& window.getProductType().getValueAsString() == "n/a") {
//							return true;
//						} else {
//							SC.warn("ERROR", "Product id needs to be defined");
//							return false;
//
//						}
//					} else {
//						SC.warn("ERROR", "Please Select paying category");
//
//					}
//				} else {
//					SC.warn("ERROR", "Please Select account type");
//				}
//			} else {
//				SC.warn("ERROR", "Please enter account name");
//			}
//		} else {
//			SC.warn("ERROR", "Please enter account code");
//		}
//		return false;
//	}
//
//	private void ClearForm(final AccountSWindow window) {
//		window.getAccountCode().clearValue();
//		window.getAccountName().clearValue();
//		window.getAccountType().clearValue();
//		window.getCategory().clearValue();
//		window.getPdata().clearValue();
//		window.getAnalReq().clearValue();
//		window.getProductType().clearValue();
//	}
//
//	private void onSavebuttonClicked(final AccountSWindow window) {
//		window.getSaveButton().addClickHandler(new ClickHandler() {
//
//			public void onClick(ClickEvent event) {
//				if (FormValid(window)) {
//					AtypeA act = new AtypeA();
//					act.setId(window.getAccountType().getValueAsString());
//					ProductSetup ps = new ProductSetup();
//					ps.setId(window.getProductType().getValueAsString());
//
//					Init account = new Init();
//					account.setAcode(window.getAccountCode().getValueAsString());
//					account.setAname(window.getAccountName().getValueAsString());
//
//					account.setTtypeCd(act);
//					account.setIncExp(ProductID.getProduct(window.getCategory().getValueAsString()));
//					account.setCurrencyYcd(window.getCurrencyCd().getValueAsString());
//					account.setProductId(ps);
//					// account.setCreatedBy(createdBy);
//					account.setDateCreated(new Date());
//					account.setDateUpdated(new Date());
//					account.setStatus(Status.ACTIVE);
//					if (window.getAnalReq().getValueAsString() != null) {
//						if (window.getAnalReq().getValueAsString().equalsIgnoreCase("Y")) {
//							account.setAnalRequired(true);
//
//						} else if (window.getAnalReq().getValueAsString().equalsIgnoreCase("N")) {
//							account.setAnalRequired(false);
//						}
//					} else {
//						account.setAnalRequired(false);
//					}
//					if (window.getPdata().getValueAsString() != null) {
//						if (window.getPdata().getValueAsString().equalsIgnoreCase("Y")) {
//							account.setPdata(true);
//
//						} else if (window.getPdata().getValueAsString().equalsIgnoreCase("N")) {
//							account.setPdata(false);
//						}
//					} else {
//						account.setPdata(false);
//					}
//
//					// account.setUpdatedBy(updatedBy);
//
//					SC.showPrompt("", "", new SwizimaLoader());
//
//					dispatcher.execute(new SysAction(PAConstant.SAVE_ACCOUNTS, account),
//							new AsyncCallback<SysResult>() {
//								public void onFailure(Throwable caught) {
//									System.out.println(caught.getMessage());
//								}
//
//								public void onSuccess(SysResult result) {
//
//									SC.clearPrompt();
//
//									if (result != null) {
//
//										if (result.getSwizFeedback().isResponse()) {
//											ClearForm(window);
//											SC.say("SUCCESS", result.getSwizFeedback().getMessage());
//
//											getView().getAccountSPane().getAccountSListgrid()
//													.addRecordsToGrid(result.getInits());
//										} else {
//											SC.warn("ERROR", result.getSwizFeedback().getMessage());
//										}
//
//									} else {
//										SC.say("ERROR", "Unknow error");
//									}
//
//								}
//							});
//				}
//			}
//		});
//	}
//
//	private void loadAccounts() {
//		SC.showPrompt("", "", new SwizimaLoader());
//
//		dispatcher.execute(new SysAction(PAConstant.GET_ACCOUNTS), new AsyncCallback<SysResult>() {
//			public void onFailure(Throwable caught) {
//				System.out.println(caught.getMessage());
//			}
//
//			public void onSuccess(SysResult result) {
//
//				SC.clearPrompt();
//
//				if (result != null) {
//
//					getView().getAccountSPane().getAccountSListgrid().addRecordsToGrid(result.getInits());
//
//				} else {
//					SC.say("ERROR", "Unknow error");
//				}
//
//			}
//		});
//	}
//
//	private void onDeletebuttonClicked() {
//		getView().getAccountSPane().getDeleteButton().addClickHandler(new ClickHandler() {
//
//			public void onClick(ClickEvent event) {
//				delete();
//
//			}
//		});
//
//		getView().getAccountSPane().getControlsPane().getDeleteButton().addClickHandler(new ClickHandler() {
//
//			public void onClick(ClickEvent event) {
//				delete();
//
//			}
//		});
//	}
//
//	private void delete() {
//		if (getView().getAccountSPane().getAccountSListgrid().anySelected()) {
//
//			SC.ask("Confirm", "Are you sure you want to delete the selected record?", new BooleanCallback() {
//
//				public void execute(Boolean value) {
//
//					if (value) {
//
//						Init account = new Init();
//						account.setId(getView().getAccountSPane().getAccountSListgrid().getSelectedRecord()
//								.getAttribute(AccountSListgrid.ID));
//
//						SC.showPrompt("", "", new SwizimaLoader());
//
//						dispatcher.execute(new SysAction(PAConstant.DELETE_ACCOUNTS, account),
//								new AsyncCallback<SysResult>() {
//									public void onFailure(Throwable caught) {
//										System.out.println(caught.getMessage());
//									}
//
//									public void onSuccess(SysResult result) {
//
//										SC.clearPrompt();
//
//										if (result != null) {
//
//											if (result.getSwizFeedback().isResponse()) {
//
//												SC.say("SUCCESS", result.getSwizFeedback().getMessage());
//
//												getView().getAccountSPane().getAccountSListgrid()
//														.addRecordsToGrid(result.getInits());
//											} else {
//												SC.warn("ERROR", result.getSwizFeedback().getMessage());
//											}
//
//										} else {
//											SC.say("ERROR", "Unknow error");
//										}
//
//									}
//								});
//
//					}
//
//				}
//			});
//
//		} else {
//			SC.say("ERROR", "ERROR: Please select record to delete?");
//		}
//	}
//
//}
