package com.pearl.accounting.sysui.client.views.windows;

import java.util.List;

import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysui.client.views.listgrids.MultiJournalDetailListgrid;
import com.pearl.accounting.sysui.client.views.widgets.ComboBox;
import com.pearl.accounting.sysui.client.views.widgets.TextField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class MultiJournalWindow extends Window {

//private ComboBox accountname;
	
	
    private TextField currentperiod;
    private DateItem currentstartdate;
    private DateItem currentenddate;
	private TextField paidAmount;
	private TextField assessmentPeriod;
	private TextField financialYear;
	private DateItem paymentDate;
	private ComboBox receivingAccount;
	private TextField paymentDetails;
	private TextField remarks;
	private DateItem startdate;
	private DateItem enddate;
	private TextField periodcode;

	private MultiJournalDetailListgrid multiJournalDetailListgrid;
	private IButton saveButton;
	private IButton addButton;
	private IButton delete;

	public MultiJournalWindow(List<Account> accounts, List<CustomerAccount> custacct, List<Loan> Lnacct) {
		super();

		
		
		currentperiod = new TextField();
		currentperiod.setTitle("currentperiod");
		currentperiod.setWidth("10px");

		currentstartdate = new DateItem();
		currentstartdate.setTitle("Transaction Date");
		currentstartdate.setUseTextField(true);
		currentstartdate.setWidth("*");
		currentstartdate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		currentenddate = new DateItem();
		currentenddate.setTitle("Transaction Date");
		currentenddate.setUseTextField(true);
		currentstartdate.setWidth("*");
		currentstartdate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
		
		financialYear = new TextField();
		financialYear.setTitle("Year");
		financialYear.setWidth("10px");
		assessmentPeriod = new TextField();
		assessmentPeriod.setTitle("Period");
		assessmentPeriod.setWidth("10px");

		// accountname = new ComboBox();
		// accountname.setTitle("Account Name");

		paidAmount = new TextField();
		paidAmount.setTitle("Amount");
		paidAmount.setKeyPressFilter("[0-9.]");

		paymentDate = new DateItem();
		paymentDate.setTitle("Transaction Date");
		paymentDate.setUseTextField(true);
		paymentDate.setWidth("*");
		paymentDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		periodcode = new TextField();
		periodcode.setTitle("priodcode");
		periodcode.setWidth("10px");

		startdate = new DateItem();
		startdate.setTitle("startdate");
		startdate.setUseTextField(true);
		startdate.setWidth("*");
		startdate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		enddate = new DateItem();
		enddate.setTitle("	enddate");
		enddate.setUseTextField(true);
		enddate.setWidth("*");
		enddate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		receivingAccount = new ComboBox();
		receivingAccount.setTitle("Account");

		paymentDetails = new TextField();
		paymentDetails.setTitle("Payment details");

		remarks = new TextField();
		remarks.setTitle("Remarks");

		saveButton = new IButton("Save");
		addButton = new IButton("Add");
		delete = new IButton("Delete");

		multiJournalDetailListgrid = new MultiJournalDetailListgrid(accounts, custacct, Lnacct);

		DynamicForm form = new DynamicForm();
		form.setFields(financialYear, periodcode, assessmentPeriod, startdate, enddate);
		form.setWrapItemTitles(true);
		form.setMargin(10);
		form.setNumCols(4);

		DynamicForm form2 = new DynamicForm();
		form2.setFields(paidAmount, paymentDate, paymentDetails, remarks);
		form2.setWrapItemTitles(true);
		form2.setMargin(10);
		form2.setNumCols(4);
		DynamicForm form4 = new DynamicForm();
		form4.setFields(currentstartdate, currentperiod,currentenddate);
		form4.setWrapItemTitles(true);
		form4.setMargin(10);
		form4.setNumCols(6);
		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(saveButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(4);

		Label subheader = new Label();
		subheader.setStyleName("crm-ContextArea-Header-Label");
		subheader.setContents("Accounts details");
		subheader.setWidth("1%");
		subheader.setAutoHeight();
		// subheader.setMargin(10);
		subheader.setAlign(Alignment.LEFT);

		HLayout subbuttonLayout = new HLayout();
		subbuttonLayout.setMembers(addButton, delete, subheader);
		subbuttonLayout.setAutoHeight();
		subbuttonLayout.setWidth100();
		subbuttonLayout.setMargin(5);
		subbuttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(form4);
		layout.addMember(form);
		layout.addMember(form2);
		layout.addMember(subbuttonLayout);
		layout.addMember(multiJournalDetailListgrid);
		layout.addMember(buttonLayout);
		layout.setMargin(10);

		this.addItem(layout);
		this.setWidth("70%");
		this.setHeight("70%");
		this.setAutoCenter(true);
		this.setTitle("Multi-Journal definition");
		this.setIsModal(true);
		this.setShowModalMask(true);
	}


	public TextField getPaidAmount() {
		return paidAmount;
	}

	public TextField getAssessmentPeriod() {
		return assessmentPeriod;
	}

	public TextField getFinancialYear() {
		return financialYear;
	}

	public DateItem getPaymentDate() {
		return paymentDate;
	}

	public ComboBox getReceivingAccount() {
		return receivingAccount;
	}

	public TextField getPaymentDetails() {
		return paymentDetails;
	}

	public TextField getRemarks() {
		return remarks;
	}

	public MultiJournalDetailListgrid getMultiJournalDetailListgrid() {
		return multiJournalDetailListgrid;
	}

	public IButton getSaveButton() {
		return saveButton;
	}

	public IButton getAddButton() {
		return addButton;
	}

	public IButton getDelete() {
		return delete;
	}

	public void setDelete(IButton delete) {
		this.delete = delete;
	}

	public DateItem getStartdate() {
		return startdate;
	}

	public DateItem getEnddate() {
		return enddate;
	}

	public TextField getPeriodcode() {
		return periodcode;
	}


	public TextField getCurrentperiod() {
		return currentperiod;
	}


	public DateItem getCurrentstartdate() {
		return currentstartdate;
	}


	public DateItem getCurrentenddate() {
		return currentenddate;
	}

}
