package com.pearl.accounting.sysui.client.views.windows;

import com.pearl.accounting.sysui.client.views.widgets.ComboBox;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class UserPermissionWindow extends Window {

	private ComboBox userRole;
	private ComboBox systemView;

	private IButton saveButton;

	public UserPermissionWindow() {
		super();

		userRole = new ComboBox();
		userRole.setTitle("Role");

		systemView = new ComboBox();
		systemView.setTitle("System View");

		saveButton = new IButton("Save");

		DynamicForm form = new DynamicForm();
		form.setFields(userRole, systemView);
		form.setWrapItemTitles(false);
		form.setMargin(10);

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(saveButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(form);
		layout.addMember(buttonLayout);

		layout.setMargin(10);
		this.addItem(layout);
		this.setWidth("40%");
		this.setHeight("28%");
		this.setAutoCenter(true);
		this.setTitle("System permissions");
		this.setIsModal(true);
		this.setShowModalMask(true);
	}

	public ComboBox getUserRole() {
		return userRole;
	}

	public ComboBox getSystemView() {
		return systemView;
	}

	public IButton getSaveButton() {
		return saveButton;
	}

}
