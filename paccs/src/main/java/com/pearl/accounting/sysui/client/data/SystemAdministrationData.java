/**
 * 
 */
package com.pearl.accounting.sysui.client.data;

import com.pearl.accounting.sysui.client.place.NameTokens;
import com.smartgwt.client.widgets.grid.ListGridRecord; 

/**
 * @author Planet Developer 001
 * 
 */
public class SystemAdministrationData { 

	private static ListGridRecord[] records;

	public static ListGridRecord[] getRecords() {
		if (records == null) {
			records = getNewRecords();

		}
		return records;

	}

	public static ListGridRecord createRecord(String pk, String icon, String name) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute("pk", pk);
		record.setAttribute("icon", icon);
		record.setAttribute("name", name);
		return record;
	}

	public static ListGridRecord[] getNewRecords() {

		return new ListGridRecord[] {
//				createRecord("", "application_form",NameTokens.notifications),
				createRecord("", "application_form",NameTokens.users),
				createRecord("", "application_form",NameTokens.Interval),
			//	createRecord("", "application_form",NameTokens.periods),
				createRecord("", "application_form",NameTokens.accounts),
//				createRecord("", "application_form","Currencies"),
//				createRecord("", "application_form","Exchange rates"),
				createRecord("", "application_form",NameTokens.ProductSetup),
			//	createRecord("", "application_form",NameTokens.ACCOUNTS),

			//	createRecord("", "application_form",NameTokens.DoubleEntry),

			//	createRecord("", "application_form",NameTokens.MY),

				
		};

	}
	 
}
