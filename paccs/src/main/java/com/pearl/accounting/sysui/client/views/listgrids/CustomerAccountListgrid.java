package com.pearl.accounting.sysui.client.views.listgrids;


import java.util.List;

import com.pearl.accounting.sysmodel.CustomerAccount;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class CustomerAccountListgrid extends SuperListGrid {
	public static String ID = "id";
	public static String CODE = "code";
	public static String Surname = "Surname";
    public static String ClientCode = "Clientcode";
	public static String Othername = "othername";
	public static String dob = "dob";
	public static String Gender = "gender";
	public static String companycode = "companycode";
	public static String companyaddress = "companyaddress";
	public static String profession = "profession";
	public static String email = "email";
	public static String location = "location";
	public static String phonenumber = "phonenumber";
	public static String recomder = "recomder";

	public CustomerAccountListgrid() {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField Clientcode = new ListGridField(ClientCode, "ClientCode");
		
		ListGridField surname  = new ListGridField(Surname, "Surname name");

		ListGridField othername = new ListGridField(Othername, "Othername");

		ListGridField  Email= new ListGridField(email, "Email");

		ListGridField Phonenumber= new ListGridField(phonenumber, "phone number");
		ListGridField Companycode= new ListGridField(companycode, "company code");
		ListGridField CompanyAddress= new ListGridField(companyaddress, "Company Adderss");
		ListGridField gender= new ListGridField(Gender, "gender");
		ListGridField dob1= new ListGridField(dob, "Date of birth");

		this.setFields(id,Clientcode,Companycode,CompanyAddress,surname,othername,gender,Email,Phonenumber,dob1);

	}

	public ListGridRecord addRowData(CustomerAccount account) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute(ID, account.getSysid());
		record.setAttribute(ClientCode, account.getClientCode());
		record.setAttribute(companycode, account.getCompanyCode());
		record.setAttribute(companyaddress, account.getCompanyAddress());
		record.setAttribute(Surname, account.getSurname());
		record.setAttribute(Othername, account.getOtherNames());
		record.setAttribute(Gender, account.getGender());
		record.setAttribute(email, account.getEmail());
		record.setAttribute(phonenumber, account.getPhoneNumber());
		record.setAttribute(dob, account.getBirthDate());



		return record;
	}

	public void addRecordsToGrid(List<CustomerAccount> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (CustomerAccount item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
	}

}
