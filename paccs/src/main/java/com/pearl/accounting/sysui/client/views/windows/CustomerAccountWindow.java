package com.pearl.accounting.sysui.client.views.windows;

import com.googlecode.genericdao.search.Search;
import com.pearl.accounting.sysui.client.views.widgets.ComboBox;
import com.pearl.accounting.sysui.client.views.widgets.TextField;
import com.smartgwt.client.types.CharacterCasing;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class CustomerAccountWindow extends Window {
    private TextField clientCode;
	private TextField nationalId;
	private TextField firstName;
	private TextField lastName;
	private TextField phoneNumber;
	private TextField emailAddress;
    private TextField Searchbar;
	private IButton saveButton;
	private IButton searchButton;
	private DateItem dob;
	private ComboBox gender;
    private TextField Age;
    private TextField Recomender;
	private TextField nextofKin;
	private TextField relationShip;
	private TextField relationShipContact;
	private TextField residence;
	private TextField workPlace;
	private TextField Address;
	private TextField Address1;
	private ComboBox MaritalStatus;
	private  TextField Kinresidence;
    private TextField CompanyCode;
    private TextField CompanyAddress;
	private TextField profession;
	private TextField Title;
	private TextField Department;
    private TextField OfficePhone;
	public CustomerAccountWindow() {
		super();
		firstName = new TextField();
		firstName.setTitle("First Name");
		firstName.setCharacterCasing(CharacterCasing.UPPER);
		Searchbar = new TextField();
		Searchbar.setTitle("Search");
		Department = new TextField();
		Department.setTitle("Department");
		
		CompanyAddress = new TextField();
		CompanyAddress.setTitle("CompanyAddress");
	
			
		CompanyCode = new TextField();
		CompanyCode.setTitle("CompanyCode");
		
		
		
		
		Kinresidence = new TextField();
		Kinresidence.setTitle("Kinresidence");
		
		Recomender = new TextField();
		Recomender.setTitle("Recomender");
		OfficePhone = new TextField();
		OfficePhone.setTitle("OfficePhone");
		OfficePhone.setKeyPressFilter("[0-9]");

		Title = new TextField();
		Title.setTitle("Title");
		
		lastName = new TextField();
		lastName.setTitle("Last Name");
		lastName.setCharacterCasing(CharacterCasing.UPPER);


		nationalId = new TextField();
		nationalId.setTitle("National Id");
		clientCode = new TextField();
		clientCode.setTitle("Client CodeSetup");

		phoneNumber = new TextField();
		phoneNumber.setTitle("Phone Number");
		phoneNumber.setKeyPressFilter("[0-9]");
//
//		phoneNumber = new TextField();
//		phoneNumber.setTitle("Phone Number");
		OfficePhone.setKeyPressFilter("[0-9]");
		
		
		emailAddress = new TextField();
		emailAddress.setTitle("Email Address");

		dob = new DateItem();
		dob.setTitle("D.O.B");
		dob.setUseTextField(true);
		dob.setWidth("*");
		dob.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);


		gender = new ComboBox();
		gender.setTitle("Gender");
		MaritalStatus = new ComboBox();
		MaritalStatus.setTitle("MaritalStatus");

		nextofKin = new TextField();
		nextofKin.setTitle("Next of Kin");

		relationShip = new TextField();
		relationShip.setTitle("RelationShip");

		relationShipContact = new TextField();
		relationShipContact.setTitle("Next of Kin Contact");
		relationShipContact.setKeyPressFilter("[0-9]");


		residence = new TextField();
		residence.setTitle("Residence");

		workPlace = new TextField();
		workPlace.setTitle("WorkPlace");

		Address = new TextField();
		Address.setTitle("Address");
		Address1 = new TextField();
		Address1.setTitle("Address1");

		profession = new TextField();
		profession.setTitle("Profession");

		saveButton = new IButton("Save");
		searchButton = new IButton("Search");

		DynamicForm form = new DynamicForm();
		form.setFields(firstName, lastName,Title,clientCode, gender, dob, nationalId,MaritalStatus);
		form.setWrapItemTitles(true);
		form.setMargin(10);
		form.setNumCols(4);
		// form.setBorder("1px solid black");
		// form.setTitle("Customer details");
				DynamicForm form4 = new DynamicForm();
				form4.setFields(CompanyCode, CompanyAddress);
				form4.setMargin(8);
				form4.setNumCols(2);
// form.setTitle("Customer details");
		DynamicForm form1 = new DynamicForm();
		form1.setFields( phoneNumber, emailAddress);
		form1.setMargin(8);
		form1.setNumCols(2);

		DynamicForm form2 = new DynamicForm();
		form2.setFields(profession,Department, workPlace, residence, Address,Address1,OfficePhone);
		form2.setWrapItemTitles(true);
		form2.setMargin(10);
		form2.setNumCols(4);

		DynamicForm form3 = new DynamicForm();
		form3.setFields(Recomender,nextofKin, relationShip, relationShipContact,Kinresidence);
		form3.setWrapItemTitles(true);
		form3.setMargin(10);
		form3.setNumCols(4);
		// form2.setBorder("1px solid black");
		// form2.setTitle("Next of kin");

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(saveButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(form);
		layout.addMember(form4);
		layout.addMember(form2);
		layout.addMember(form1);
		layout.addMember(form3);

		layout.addMember(buttonLayout);

		layout.setMargin(15);
		this.addItem(layout);
		this.setWidth("60%");
		this.setHeight("60%");
		this.setAutoCenter(true);
		this.setTitle("Customer account opening");
		this.setIsModal(true);
		this.setShowModalMask(true);
	}
	public TextField getClientCode() {
		return clientCode;
	}
	public TextField getNationalId() {
		return nationalId;
	}
	public TextField getFirstName() {
		return firstName;
	}
	public TextField getLastName() {
		return lastName;
	}
	public TextField getPhoneNumber() {
		return phoneNumber;
	}
	public TextField getEmailAddress() {
		return emailAddress;
	}
	public TextField getSearchbar() {
		return Searchbar;
	}
	public IButton getSaveButton() {
		return saveButton;
	}
	public IButton getSearchButton() {
		return searchButton;
	}
	public DateItem getDob() {
		return dob;
	}
	public ComboBox getGender() {
		return gender;
	}
	public TextField getAge() {
		return Age;
	}
	public TextField getRecomender() {
		return Recomender;
	}
	public TextField getNextofKin() {
		return nextofKin;
	}
	public TextField getRelationShip() {
		return relationShip;
	}
	public TextField getRelationShipContact() {
		return relationShipContact;
	}
	public TextField getResidence() {
		return residence;
	}
	public TextField getWorkPlace() {
		return workPlace;
	}
	public TextField getAddress() {
		return Address;
	}
	public TextField getAddress1() {
		return Address1;
	}
	public ComboBox getMaritalStatus() {
		return MaritalStatus;
	}
	public TextField getKinresidence() {
		return Kinresidence;
	}
	public TextField getCompanyCode() {
		return CompanyCode;
	}
	public TextField getCompanyAddress() {
		return CompanyAddress;
	}
	public TextField getProfession() {
		return profession;
	}
	public TextField getTitles() {
		return Title;
	}
	public TextField getDepartment() {
		return Department;
	}
	public TextField getOfficePhone() {
		return OfficePhone;
	}

	


	
	

}
