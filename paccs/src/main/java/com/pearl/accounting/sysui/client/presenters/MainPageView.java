package com.pearl.accounting.sysui.client.presenters;

import com.gwtplatform.mvp.client.ViewImpl;
import com.pearl.accounting.sysui.client.data.AccountTransactionData;
import com.pearl.accounting.sysui.client.data.AccountTransactionDataSource;
//import com.pearl.accounting.sysui.client.data.AdvanceManagementData;
//import com.pearl.accounting.sysui.client.data.AdvanceManagementDataSource;
//import com.pearl.accounting.sysui.client.data.BudgetData;
//import com.pearl.accounting.sysui.client.data.BudgetDataSource;
import com.pearl.accounting.sysui.client.data.SavingsCreditData;
import com.pearl.accounting.sysui.client.data.SavingsCreditDataSource;
import com.pearl.accounting.sysui.client.data.SystemAdministrationData;
import com.pearl.accounting.sysui.client.data.SystemAdministrationDataSource;
//import com.pearl.accounting.sysui.client.data.UtilityMaintenanceData;
//import com.pearl.accounting.sysui.client.data.UtilityMaintenanceDataSource;
import com.pearl.accounting.sysui.client.utils.PAConstant;
import com.pearl.accounting.sysui.client.views.widgets.ApplicationMenu;
import com.pearl.accounting.sysui.client.views.widgets.MainStatusBar;
import com.pearl.accounting.sysui.client.views.widgets.Masthead;
import com.pearl.accounting.sysui.client.views.widgets.NavigationPane;
import com.pearl.accounting.sysui.client.views.widgets.NavigationPaneHeader;
import com.pearl.accounting.sysui.client.views.widgets.StatusBar;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.MenuItemSeparator; 

public class MainPageView extends ViewImpl implements MainPagePresenter.MyView {

	private static final int NORTH_HEIGHT = 20;
	private static final String DEFAULT_MARGIN = "0px";
	private static final int DEFAULT_MENU_WIDTH = 70;

	private ApplicationMenu applicationMenu;
	private Masthead mastHead;
	private NavigationPaneHeader navigationPaneHeader;
	private NavigationPane navigationPane;
	private StatusBar statusBar;
	private MainStatusBar mainStatusBar;

	private VLayout panel;

	private HLayout southLayout;
	public static VLayout westLayout;
	private VLayout centerLayout;

	private MenuItem logoutItem;
	private MenuItem updateProfileItem;
	private MenuItem changePasswordItem;
	private MenuItem deligateItem;
	private MenuItem claimItem;  

	@Inject
	public MainPageView() {
		super();
		applicationMenu = new ApplicationMenu();
		mastHead = new Masthead();
		navigationPaneHeader = new NavigationPaneHeader();
		navigationPane = new NavigationPane();
		statusBar = new StatusBar();
		mainStatusBar = new MainStatusBar();

		Window.enableScrolling(false);
		Window.setMargin(DEFAULT_MARGIN);

		panel = new VLayout();
		panel.setWidth100();
		panel.setHeight100();

		centerLayout = new VLayout();
		centerLayout.setWidth100();
		centerLayout.setHeight100();
		centerLayout.setBackgroundColor("white");

		applicationMenu = new ApplicationMenu();

		HLayout northLayout = new HLayout();
		northLayout.setHeight(NORTH_HEIGHT);

		initialiseApplicationMenu();

		VLayout vLayout = new VLayout();
		vLayout.addMember(mastHead);
		vLayout.addMember(applicationMenu);

		northLayout.addMember(vLayout);

		initNavigationPane();

		navigationPane.setCanDragResize(true);
		navigationPane.setShowResizeBar(true);
		westLayout = this.navigationPane;

		southLayout = new HLayout();
		southLayout.setMembers(westLayout, centerLayout);

		panel.addMember(northLayout);
		panel.addMember(southLayout);
		panel.addMember(mainStatusBar);

	}

	public Widget asWidget() {
		return panel;
	}

	@Override
	public void setInSlot(Object slot, Widget content) {
		if (slot == MainPagePresenter.TYPE_SetContextAreaContent) {

			centerLayout.setMembers((VLayout) content);
		} else {
			super.setInSlot(slot, content);
		}
	}

	public void initialiseApplicationMenu() {
		// Add the file menu
		MenuItem newItem = new MenuItem("New", "icons/16/document_plain_new.png", "Ctrl+N");
		MenuItem openItem = new MenuItem("Open", "icons/16/folder_out.png", "Ctrl+O");
		MenuItem saveItem = new MenuItem("Save", "icons/16/disk_blue.png", "Ctrl+S");
		MenuItem saveAsItem = new MenuItem("Save As", "icons/16/save_as.png");
		MenuItem recentDocItem = new MenuItem("Recent Documents", "icons/16/folder_document");
		MenuItem exportItem = new MenuItem("Export as...", "icons/16/export1.png");
		MenuItemSeparator separator = new MenuItemSeparator();
		MenuItem printItem = new MenuItem("Print", "icons/16/printer3.png", "Ctrl+P");

		applicationMenu.addMenu("File", DEFAULT_MENU_WIDTH).setItems(newItem, openItem, separator, saveItem, saveAsItem,
				separator, recentDocItem, separator, exportItem, separator, printItem);

		applicationMenu.addMenu("Go To", DEFAULT_MENU_WIDTH);
		applicationMenu.addMenu("Tools", DEFAULT_MENU_WIDTH);
		applicationMenu.addMenu("Help", DEFAULT_MENU_WIDTH);

		logoutItem = new MenuItem("Logout");
		updateProfileItem = new MenuItem("Update Account");
		changePasswordItem = new MenuItem("Change Password");

		deligateItem = new MenuItem("Delegate your roles");
		claimItem = new MenuItem("Claim delegated roles");

		applicationMenu.addMenu("My Account", DEFAULT_MENU_WIDTH).setItems(logoutItem, updateProfileItem,
				changePasswordItem, deligateItem, claimItem);

	}

	
	private void initNavigationPane() {
		
		navigationPane.addSection(PAConstant.SYSTEM_CONFIGURATION, SystemAdministrationDataSource.getInstance(SystemAdministrationData.getNewRecords())); 
		
		navigationPane.addSection(PAConstant.Account_transactions, AccountTransactionDataSource.getInstance(AccountTransactionData.getNewRecords())); 
		
	//	navigationPane.addSection(PAConstant.Advance_Management, AdvanceManagementDataSource.getInstance(AdvanceManagementData.getNewRecords()));
		
	//	navigationPane.addSection(PAConstant.Budgets_Management, BudgetDataSource.getInstance(BudgetData.getNewRecords())); 
		
		navigationPane.addSection(PAConstant.Loans_Management, SavingsCreditDataSource.getInstance(SavingsCreditData.getNewRecords()));
		
	//	navigationPane.addSection(PAConstant.Utility_Maintenance, UtilityMaintenanceDataSource.getInstance(UtilityMaintenanceData.getNewRecords()));
		 
		 
		
	}
	
	

	public ApplicationMenu getApplicationMenu() {
		return applicationMenu;
	}

	public Masthead getMastHead() {
		return mastHead;
	}

	public NavigationPaneHeader getNavigationPaneHeader() {
		return navigationPaneHeader;
	}

	public NavigationPane getNavigationPane() {
		return navigationPane; 
	}

	public StatusBar getStatusBar() {
		return statusBar;
	}

	public MainStatusBar getMainStatusBar() {
		return mainStatusBar;
	}

	public VLayout getPanel() {
		return panel;
	}

	public HLayout getSouthLayout() {
		return southLayout;
	}

	public static VLayout getWestLayout() {
		return westLayout;
	}

	public VLayout getCenterLayout() {
		return centerLayout;
	}

	public MenuItem getLogoutItem() {
		return logoutItem;
	}

	public MenuItem getUpdateProfileItem() {
		return updateProfileItem;
	}

	public MenuItem getChangePasswordItem() {
		return changePasswordItem;
	}

	public MenuItem getDeligateItem() {
		return deligateItem;
	}

	public MenuItem getClaimItem() {
		return claimItem;
	}

}
