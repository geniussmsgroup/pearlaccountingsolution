package com.pearl.accounting.sysui.client.data;

import com.pearl.accounting.sysui.client.place.NameTokens;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class AdvanceManagementData {


	private static ListGridRecord[] records;

	public static ListGridRecord[] getRecords() {
		if (records == null) {
			records = getNewRecords();

		}
		return records;

	}

	public static ListGridRecord createRecord(String pk, String icon, String name) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute("pk", pk);
		record.setAttribute("icon", icon);
		record.setAttribute("name", name);
		return record;
	}

	public static ListGridRecord[] getNewRecords() {
		

		return new ListGridRecord[] {
				createRecord("", "application_form",NameTokens.notifications),
				createRecord("", "application_form","New advance/individual"),
				createRecord("", "application_form","New advance/Multi Jnl"),
				createRecord("", "application_form","Advance Accountability"),
				createRecord("", "application_form","Advance Claim Refund"),
				createRecord("", "application_form","Advance Journal Adjustment"),
				createRecord("", "application_form","Advance Account Config"),
				createRecord("", "application_form","Advance Report")
				 
				
		};

	}
}
