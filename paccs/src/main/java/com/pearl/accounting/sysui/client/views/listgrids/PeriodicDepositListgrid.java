package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.google.gwt.i18n.client.NumberFormat;
import com.pearl.accounting.sysmodel.PeriodicDeposit;
import com.pearl.accounting.sysui.client.utils.PAManager;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;

public class PeriodicDepositListgrid extends SuperListGrid {
	public static String ID = "id";

	public static String CustomerAccountId = "CustomerAccountId";
	public static String CustomerAccountNo = "CustomerAccountNo";
	public static String CustomerAccount = "CustomerAccount";
	public static String DepositedAmount = "DepositedAmount";
	public static String AssessmentPeriodId = "AssessmentPeriodId";
	public static String AssessmentPeriod = "AssessmentPeriod";
	public static String FinancialYearId = "FinancialYearId";
	public static String FinancialYear = "FinancialYearId";
	public static String DepositDate = "DepositDate";
	public static String ReceiveingAccountId = "ReceiveingAccountId";
	public static String ReceiveingAccount = "ReceiveingAccount";
	public static String PaymentDetails = "PaymentDetails";
	public static String Remarks = "Remarks";
	public static String ExchangeRate = "ExchangeRate";

	
	NumberFormat nf = NumberFormat.getFormat("#,##0.00");
	
	public PeriodicDepositListgrid() {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField customerAccountId = new ListGridField(CustomerAccountId, "Account Id");
		customerAccountId.setHidden(true);
		ListGridField customerAccountNo = new ListGridField(CustomerAccountNo, "Account No");
		ListGridField customerAccount = new ListGridField(CustomerAccount, "Name");
		
		ListGridField depositedAmount = new ListGridField(DepositedAmount, "Amount deposited");
		
		depositedAmount.setShowGridSummary(true);

		depositedAmount.setSummaryFunction(new SummaryFunction() {
			public Object getSummaryValue(Record[] records, ListGridField field) {

				long totalAmount = 0;
				for (Record record : records) {

					totalAmount += Float
							.parseFloat(PAManager.getInstance().unformatCash(record.getAttribute(DepositedAmount)));

				}
				return "" + PAManager.getInstance().formatCash(totalAmount);
			}
		});
		
		ListGridField receiveingAccountId = new ListGridField(ReceiveingAccountId, "Receiveing Account Id");
		receiveingAccountId.setHidden(true);
		
		ListGridField receiveingAccount = new ListGridField(ReceiveingAccount, "Receiveing Account");
		ListGridField depositDate = new ListGridField(DepositDate, "Deposit Date");

		ListGridField assessmentPeriodId = new ListGridField(AssessmentPeriodId, "PeriodId");
		assessmentPeriodId.setHidden(true);
		
		ListGridField assessmentPeriod = new ListGridField(AssessmentPeriod, "Period");
		
		ListGridField financialYearId = new ListGridField(FinancialYearId, "YearId");
		financialYearId.setHidden(true);
		
		ListGridField financialYear = new ListGridField(FinancialYear, "Year");

		ListGridField paymentDetails = new ListGridField(PaymentDetails, "Payment Details");
		ListGridField remarks = new ListGridField(Remarks, "Remarks");
		ListGridField exchangeRate = new ListGridField(ExchangeRate, "Ex.Rate");

		this.setFields(id, customerAccountId, customerAccountNo, customerAccount, depositedAmount, receiveingAccountId,
				receiveingAccount, depositDate, paymentDetails, remarks, exchangeRate, assessmentPeriodId,
				assessmentPeriod, financialYearId, financialYear);
		
		this.setCanExpandRecords(true);
		this.setShowGridSummary(true);

	}

	public ListGridRecord addRowData(PeriodicDeposit deposit) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute(ID, deposit.getId());

		if (deposit.getCustomerAccount() != null) {

			record.setAttribute(CustomerAccountId, deposit.getCustomerAccount().getSysid());
			record.setAttribute(CustomerAccountNo, deposit.getCustomerAccount().getClientCode());
			record.setAttribute(CustomerAccount,
					deposit.getCustomerAccount().getSurname() + " " + deposit.getCustomerAccount().getOtherNames());
		}

		record.setAttribute(DepositedAmount,  nf.format(Float.parseFloat(deposit.getDepositedAmount())));

		if (deposit.getReceiveingAccount() != null) {
			record.setAttribute(ReceiveingAccountId, deposit.getReceiveingAccount().getSysid());
			record.setAttribute(ReceiveingAccount, deposit.getReceiveingAccount().getAccountName());
		}

		record.setAttribute(DepositDate, deposit.getDepositDate());

		if (deposit.getPeriod() != null) {
			record.setAttribute(AssessmentPeriod, deposit.getPeriod());
		}

		//if (deposit.getYear() != null) {

			record.setAttribute(FinancialYear, deposit.getYear());
		//}

		record.setAttribute(PaymentDetails, deposit.getPaymentDetails());
		record.setAttribute(Remarks, deposit.getRemarks());
		record.setAttribute(ExchangeRate, deposit.getExchangeRate());

		return record;
	}

	public void addRecordsToGrid(List<PeriodicDeposit> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (PeriodicDeposit item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
	}

}
