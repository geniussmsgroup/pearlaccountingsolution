package com.pearl.accounting.sysui.client.data;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class AdvanceManagementDataSource extends DataSource {

	private static AdvanceManagementDataSource instance = null;

	public static AdvanceManagementDataSource getInstance(ListGridRecord[] data) {

		if (instance == null) {
			instance = new AdvanceManagementDataSource("AdvanceManagementDataSource",data);
		}
		return instance;
	}

	public AdvanceManagementDataSource(String id,ListGridRecord[] data) {

		DataSourceTextField pk = new DataSourceTextField("pk", "Primary Key");
		DataSourceTextField icon = new DataSourceTextField("icon", "ICON");
		DataSourceTextField name = new DataSourceTextField("name", "Name");
		setFields(pk, icon, name);

		setTestData(data);
		setClientOnly(true);
	}


}
