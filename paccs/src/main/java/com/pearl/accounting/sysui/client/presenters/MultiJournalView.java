package com.pearl.accounting.sysui.client.presenters;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.pearl.accounting.sysui.client.views.panes.MultiJournalPane;
import com.smartgwt.client.widgets.layout.VLayout;

public class MultiJournalView extends ViewImpl implements MultiJournalPresenter.MyView {

	private static final String DEFAULT_MARGIN = "0px";
	private VLayout panel;
	private MultiJournalPane multiJournalPane;

	@Inject
	public MultiJournalView() {

		panel = new VLayout();

		multiJournalPane = new MultiJournalPane();

		panel.setMembers(multiJournalPane);
		panel.setWidth100();
		panel.setHeight("90%");
		Window.enableScrolling(false);
		Window.setMargin(DEFAULT_MARGIN);

	}

	public Widget asWidget() {
		return panel;
	}

	public MultiJournalPane getMultiJournalPane() {
		return multiJournalPane;
	}

}
