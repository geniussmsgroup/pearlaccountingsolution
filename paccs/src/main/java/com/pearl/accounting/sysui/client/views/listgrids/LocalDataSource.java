package com.pearl.accounting.sysui.client.views.listgrids;
 
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.DataBoundComponent;

public class LocalDataSource extends DataSource {

	public DataBoundComponent dataComp;

	public LocalDataSource(DataBoundComponent dbc) {
		this.dataComp = dbc;
		setTestData(new Record[] {}); 
		setClientOnly(true);

	}

	public void reloadRecords(Record[] data) {

		setTestData(data);
		dataComp.fetchData();// without these two lines,
		dataComp.filterData(); // live filter didn't work properly
	}
}
