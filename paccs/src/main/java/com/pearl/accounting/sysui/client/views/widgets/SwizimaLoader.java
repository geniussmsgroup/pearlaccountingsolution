package com.pearl.accounting.sysui.client.views.widgets;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Dialog;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.VLayout;

public class SwizimaLoader extends Dialog {

	public SwizimaLoader() {
		super();
		this.setAutoCenter(true);
		this.setShowTitle(false);
		this.setShowCloseButton(false);
		this.setShowMaximizeButton(false);
		this.setWidth(120);
		this.setHeight(70);
		this.setMargin(0);

		Img image = new Img("Spinner_16.gif", 64, 64);
		image.setAlign(Alignment.CENTER);

		Label message = new Label();
		message.setContents("Please wait..");
		message.setHeight(4);
		message.setAlign(Alignment.CENTER);

		VLayout layout = new VLayout();
		layout.addMember(image);
		layout.addMember(message);

		this.setMargin(0);
		this.setAlign(Alignment.CENTER); 
		this.addItem(layout);
		
		
		
	}
}
