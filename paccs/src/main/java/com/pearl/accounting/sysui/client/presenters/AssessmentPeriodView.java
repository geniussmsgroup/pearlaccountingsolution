package com.pearl.accounting.sysui.client.presenters;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.pearl.accounting.sysui.client.views.panes.AssessmentPeriodPane;
import com.smartgwt.client.widgets.layout.VLayout;

public class AssessmentPeriodView extends ViewImpl implements AssessmentPeriodPresenter.MyView {

	private static final String DEFAULT_MARGIN = "0px";
	private VLayout panel;
	private AssessmentPeriodPane assessmentPeriodPane;

	@Inject
	public AssessmentPeriodView() {

		panel = new VLayout();

		assessmentPeriodPane = new AssessmentPeriodPane();

		panel.setMembers(assessmentPeriodPane);
		panel.setWidth100();
		panel.setHeight("90%");
		Window.enableScrolling(false);
		Window.setMargin(DEFAULT_MARGIN);

	}

	public Widget asWidget() {
		return panel;
	}

	public AssessmentPeriodPane getAssessmentPeriodPane() {
		return assessmentPeriodPane;
	}

}
