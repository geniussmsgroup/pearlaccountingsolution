package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.google.gwt.i18n.client.NumberFormat;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.LoanDeposit;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class LoanDepositListgrid extends SuperListGrid {
	public static String ID = "id";
	public static String Loanaccount = "Loanaccount";
	public static String Customeraccount = "Customeraccount";
	public static String Remarks = "Remarks";
	public static String account = "account";
	public static String loanDeposit = "loandeposit";
	public static String Loaninterestamount = "Loaninterestamount";
	public static String Paydetails = "Paydetails";
	public static String transdate = "transdate";
	NumberFormat nf = NumberFormat.getFormat("#,##0.00");
	NumberFormat nf1 = NumberFormat.getFormat("#,##0.00");

	public LoanDepositListgrid() {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField loannumber = new ListGridField(Loanaccount, "loanId");
		ListGridField customeraccount = new ListGridField(Customeraccount, "CustomerAccount");
		ListGridField remarks = new ListGridField(Remarks, "Remarks");
		ListGridField Account = new ListGridField(account, "account");
		ListGridField Loandeposit = new ListGridField(loanDeposit, "loan deposit");
//		ListGridField loaninterestamount = new ListGridField(Loaninterestamount, "loan interest amount");
		ListGridField paydetails = new ListGridField(Paydetails, "Paymentdetails");
		ListGridField Transdate = new ListGridField(transdate, "Transactiondate");
        ListGridField loanint = new ListGridField(Loaninterestamount,"Loan Interest") ;
		this.setFields(id,  Transdate, customeraccount, Account,loannumber, loanint, Loandeposit, 
				remarks,paydetails);

	}

	public ListGridRecord addRowData(LoanDeposit loandeposit) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute(ID, loandeposit.getId());
		record.setAttribute(Loanaccount, loandeposit.getLoan().getLOANID());
		record.setAttribute(loanDeposit, nf1.format(loandeposit.getLoandeposit()));
        record.setAttribute(Loaninterestamount,nf.format(loandeposit.getInterestdeposit()));
		//Loaninterestamount
		record.setAttribute(Customeraccount, loandeposit.getCustomeraccount().getClientCode());
		record.setAttribute(Remarks, loandeposit.getRemarks());
		record.setAttribute(account, loandeposit.getAccount().getACODE());

		
		record.setAttribute(loanDeposit, nf1.format(loandeposit.getLoandeposit()));
		record.setAttribute(Paydetails, loandeposit.getPaydetails());
		record.setAttribute(transdate, loandeposit.getDepositdate());

		return record;
	}
	public void addRecordsToGrid(List<LoanDeposit> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (LoanDeposit item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
	}

}