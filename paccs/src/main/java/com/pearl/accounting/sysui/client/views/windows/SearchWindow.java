package com.pearl.accounting.sysui.client.views.windows;

import com.pearl.accounting.sysui.client.views.widgets.TextField;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.layout.VLayout;

public class SearchWindow extends Window{

private TextField searchbar;
private IButton go;

public SearchWindow() {


go = new IButton();
searchbar = new TextField();
go.setTitle("Search");
searchbar.setTitle("search");
VLayout hl = new VLayout();
DynamicForm fl = new DynamicForm();
fl.setFields(searchbar);

hl.addMembers(fl,go);
this.addMember(hl);
this.setWidth("70%");
this.setHeight("100%");
this.setAutoCenter(true);
this.setTitle("Search window");
this.setIsModal(true);
this.setShowModalMask(true);
}

public TextField getSearchbar() {
return searchbar;
}

public void setSearchbar(TextField searchbar) {
this.searchbar = searchbar;
}

public IButton getGo() {
return go;
}

public void setGo(IButton go) {
this.go = go;
}

}
