//package com.pearl.accounting.sysui.client.presenters;
//
//import java.util.Date;
//import java.util.LinkedHashMap;
//
//import com.google.gwt.user.client.rpc.AsyncCallback;
//import com.google.inject.Inject;
//import com.google.web.bindery.event.shared.EventBus;
//import com.gwtplatform.dispatch.shared.DispatchAsync;
//import com.gwtplatform.mvp.client.Presenter;
//import com.gwtplatform.mvp.client.View;
//import com.gwtplatform.mvp.client.annotations.NameToken;
//import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
//import com.gwtplatform.mvp.client.proxy.PlaceManager;
//import com.gwtplatform.mvp.client.proxy.ProxyPlace;
//import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
//import com.pearl.accounting.sysmodel.AccountDefinition;
//import com.pearl.accounting.sysmodel.AccountType;
//import com.pearl.accounting.sysmodel.My;
//import com.pearl.accounting.sysmodel.ProductType;
//import com.pearl.accounting.sysmodel.Status;
//import com.pearl.accounting.sysui.client.place.NameTokens;
//import com.pearl.accounting.sysui.client.utils.PAConstant;
//import com.pearl.accounting.sysui.client.views.listgrids.MyListgrid;
//import com.pearl.accounting.sysui.client.views.panes.MyPane;
//import com.pearl.accounting.sysui.client.views.widgets.SwizimaLoader;
//import com.pearl.accounting.sysui.client.views.windows.MyWindow;
//import com.pearl.accounting.sysui.shared.SysAction;
//import com.pearl.accounting.sysui.shared.SysResult;
//import com.smartgwt.client.util.BooleanCallback;
//import com.smartgwt.client.util.SC;
//import com.smartgwt.client.widgets.events.ClickEvent;
//import com.smartgwt.client.widgets.events.ClickHandler;
//
//public class MyPresenter extends Presenter<MyPresenter.MyView, MyPresenter.MyProxy> {
//
//	private final PlaceManager placeManager;
//	private final DispatchAsync dispatcher;
//
//	public interface MyView extends View {
//		public MyPane getMyPane();
//
//	}
//
//	@ProxyCodeSplit
//	@NameToken(NameTokens.MY)
//	public interface MyProxy extends ProxyPlace<MyPresenter> {
//	}
//
//	@Inject
//	public MyPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
//			final DispatchAsync dispatcher, final PlaceManager placeManager) {
//		super(eventBus, view, proxy);
//
//		this.dispatcher = dispatcher;
//		this.placeManager = placeManager;
//	}
//
//	@Override
//	protected void revealInParent() {
//		RevealContentEvent.fire(this, MainPagePresenter.TYPE_SetContextAreaContent, this);
//	}
//
//	@Override
//	protected void onBind() {
//		super.onBind();
//
//		onNewButtonClicked();
//		onDeletebuttonClicked();
//		loadAccounts();
//		onLoadButtonClicked();
//
//	}
//
//	private void onLoadButtonClicked() {
//		getView().getMyPane().getLoadButton().addClickHandler(new ClickHandler() {
//
//			public void onClick(ClickEvent event) {
//				loadAccounts();
//
//			}
//		});
//
//		getView().getMyPane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {
//
//			public void onClick(ClickEvent event) {
//
//				loadAccounts();
//
//			}
//		});
//	}
//
//	private void onNewButtonClicked() {
//		getView().getMyPane().getAddButton().addClickHandler(new ClickHandler() {
//
//			public void onClick(ClickEvent event) {
//
//				createAccount();
//
//			}
//		});
//
//		getView().getMyPane().getControlsPane().getNewAssetButton().addClickHandler(new ClickHandler() {
//
//			public void onClick(ClickEvent event) {
//
//				createAccount();
//
//			}
//		});
//
//	}
//
//	private void createAccount() {
//		MyWindow window = new MyWindow();
//		loadAccountTypes(window);
//		loadAccountCategories(window);
//		loadTrackle(window);
//		loadProductTypes(window);
//		onSavebuttonClicked(window);
//		
//		onSavebuttonClickedx(window);
//		window.show();
//
//	}
//
//	private void loadProductTypes(MyWindow window) {
//
//		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
//		for (ProductType productType : ProductType.values()) {
//			valueMap.put(productType.getProductType(), productType.getProductType());
//		}
//
//		window.getProductType().setValueMap(valueMap);
//
//	}
//
//	private void loadAccountTypes(MyWindow window) {
//
//		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
//		for (AccountType accountType : AccountType.values()) {
//			valueMap.put(accountType.getAccountType(), accountType.getAccountType());
//		}
//
//		window.getAccountType().setValueMap(valueMap);
//
//	}
//
//	private void loadAccountCategories(MyWindow window) {
//
//		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
//
//		for (AccountDefinition accountDefinition : AccountDefinition.values()) {
//			valueMap.put(accountDefinition.getDefinition(), accountDefinition.getDefinition());
//		}
//
//		window.getCategory().setValueMap(valueMap);
//
//	}
//
//	private void loadTrackle(MyWindow window) {
//
//		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
//
//		valueMap.put("Yes", "Yes");
//		valueMap.put("No", "No");
//
//		window.getTackable().setValueMap(valueMap);
//
//	}
//
//	private boolean FormValid(final MyWindow window) {
//		if (window.getAccountCode().getValueAsString() != null) {
//			if (window.getAccountName().getValueAsString() != null) {
//				if (window.getAccountType().getValueAsString() != null) {
//					if (window.getCategory().getValueAsString() != null) {
//						return true;
//					} else {
//						SC.warn("ERROR", "Please Select paying category");
//
//					}
//				} else {
//					SC.warn("ERROR", "Please Select account type");
//				}
//			} else {
//				SC.warn("ERROR", "Please enter account name");
//			}
//		} else {
//			SC.warn("ERROR", "Please enter account code");
//		}
//		return false;
//	}
//
//	private void ClearForm(final MyWindow window) {
//		window.getAccountCode().clearValue();
//		window.getAccountName().clearValue();
//		window.getAccountType().clearValue();
//		window.getCategory().clearValue();
//
//	}
//
//	private void onSavebuttonClicked(final MyWindow window) {
//		window.getSaveButton().addClickHandler(new ClickHandler() {
//
//			public void onClick(ClickEvent event) {
//				if (FormValid(window)) {
//					My account = new My();
//					//private string AccountName =window.getAccountName().getValueAsString();
//					
//					account.setAccountCode(window.getAccountCode().getValueAsString());
//					account.setAccountDefinition(
//							AccountDefinition.getDefinition(window.getCategory().getValueAsString()));
//					account.setAccountName(window.getAccountName().getValueAsString());
//					account.setAccountType(AccountType.getAccountType(window.getAccountType().getValueAsString()));
//					// account.setCreatedBy(createdBy);
//					account.setDateCreated(new Date());
//					account.setDateUpdated(new Date());
//					account.setStatus(Status.ACTIVE);
//					
//					account.setProductType(ProductType.getProductType(window.getProductType().getValueAsString()));
//
//					if (window.getTackable().getValueAsString() != null) {
//						if (window.getTackable().getValueAsString().equalsIgnoreCase("Yes")) {
//							account.setTrackPData(true);
//
//						} else if (window.getTackable().getValueAsString().equalsIgnoreCase("No")) {
//							account.setTrackPData(false);
//						}
//					} else {
//						account.setTrackPData(false);
//					}
//
//					// account.setUpdatedBy(updatedBy);
//
//					SC.showPrompt("", "", new SwizimaLoader());
//
//					dispatcher.execute(new SysAction(PAConstant.SAVE_MY, account), new AsyncCallback<SysResult>() {
//						public void onFailure(Throwable caught) {
//							System.out.println(caught.getMessage());
//						}
//
//						public void onSuccess(SysResult result) {
//
//							SC.clearPrompt();
//
//							if (result != null) {
//
//								if (result.getSwizFeedback().isResponse()) {
//									ClearForm(window);
//									SC.say("SUCCESS", result.getSwizFeedback().getMessage());
//
//									getView().getMyPane().getMyListgrid()
//											.addRecordsToGrid(result.getMys());
//								} else {
//									SC.warn("ERROR", result.getSwizFeedback().getMessage());
//								}
//
//							} else {
//								SC.say("ERROR", "Unknow error");
//							}
//
//						}
//					});
//				}
//			}
//		});
//	}
//
//	
//	private void onSavebuttonClickedx(final MyWindow window) {
//		window.getSaveButtonx().addClickHandler(new ClickHandler() {
//
//			public void onClick(ClickEvent event) {
//				if (FormValid(window)) {
//					My account = new My();
//					account.setAccountCode(window.getAccountCode().getValueAsString());
//					account.setAccountDefinition(
//							AccountDefinition.getDefinition(window.getCategory().getValueAsString()));
//					account.setAccountName(window.getAccountName().getValueAsString());
//					account.setAccountType(AccountType.getAccountType(window.getAccountType().getValueAsString()));
//					// account.setCreatedBy(createdBy);
//					account.setDateCreated(new Date());
//					account.setDateUpdated(new Date());
//					account.setStatus(Status.ACTIVE);
//					
//					account.setProductType(ProductType.getProductType(window.getProductType().getValueAsString()));
//
//					if (window.getTackable().getValueAsString() != null) {
//						if (window.getTackable().getValueAsString().equalsIgnoreCase("Yes")) {
//							account.setTrackPData(true);
//
//						} else if (window.getTackable().getValueAsString().equalsIgnoreCase("No")) {
//							account.setTrackPData(false);
//						}
//					} else {
//						account.setTrackPData(false);
//					}
//
//					// account.setUpdatedBy(updatedBy);
//
//					SC.showPrompt("", "", new SwizimaLoader());
//
//					dispatcher.execute(new SysAction(PAConstant.SAVE_MY, account), new AsyncCallback<SysResult>() {
//						public void onFailure(Throwable caught) {
//							System.out.println(caught.getMessage());
//						}
//
//						public void onSuccess(SysResult result) {
//
//							SC.clearPrompt();
//
//							if (result != null) {
//
//								if (result.getSwizFeedback().isResponse()) {
//									ClearForm(window);
//									SC.say("SUCCESS", result.getSwizFeedback().getMessage());
//
//									getView().getMyPane().getMyListgrid()
//											.addRecordsToGrid(result.getMys());
//								} else {
//									SC.warn("ERROR", result.getSwizFeedback().getMessage());
//								}
//
//							} else {
//								SC.say("ERROR", "Unknow error");
//							}
//
//						}
//					});
//				}
//			}
//		});
//	}
//	
//	private void loadAccounts() {
//		SC.showPrompt("", "", new SwizimaLoader());
//
//		dispatcher.execute(new SysAction(PAConstant.GET_MY), new AsyncCallback<SysResult>() {
//			public void onFailure(Throwable caught) {
//				System.out.println(caught.getMessage());
//			}
//
//			public void onSuccess(SysResult result) {
//
//				SC.clearPrompt();
//
//				if (result != null) {
//
//					getView().getMyPane().getMyListgrid().addRecordsToGrid(result.getMys());
//
//				} else {
//					SC.say("ERROR", "Unknow error");
//				}
//
//			}
//		});
//	}
//
//	private void onDeletebuttonClicked() {
//		getView().getMyPane().getDeleteButton().addClickHandler(new ClickHandler() {
//
//			public void onClick(ClickEvent event) {
//				delete();
//
//			}
//		});
//
//		getView().getMyPane().getControlsPane().getDeleteButton().addClickHandler(new ClickHandler() {
//
//			public void onClick(ClickEvent event) {
//				delete();
//
//			}
//		});
//	}
//
//	private void delete() {
//		if (getView().getMyPane().getMyListgrid().anySelected()) {
//
//			SC.ask("Confirm", "Are you sure you want to delete the selected record?", new BooleanCallback() {
//
//				public void execute(Boolean value) {
//
//					if (value) {
//
//						My account = new My();
//						account.setId(getView().getMyPane().getMyListgrid().getSelectedRecord()
//								.getAttribute(MyListgrid.ID));
//
//						SC.showPrompt("", "", new SwizimaLoader());
//
//						dispatcher.execute(new SysAction(PAConstant.DELETE_MY, account),
//								new AsyncCallback<SysResult>() {
//									public void onFailure(Throwable caught) {
//										System.out.println(caught.getMessage());
//									}
//
//									public void onSuccess(SysResult result) {
//
//										SC.clearPrompt();
//
//										if (result != null) {
//
//											if (result.getSwizFeedback().isResponse()) {
//
//												SC.say("SUCCESS", result.getSwizFeedback().getMessage());
//
//												getView().getMyPane().getMyListgrid()
//														.addRecordsToGrid(result.getMys());
//											} else {
//												SC.warn("ERROR", result.getSwizFeedback().getMessage());
//											}
//
//										} else {
//											SC.say("ERROR", "Unknow error");
//										}
//
//									}
//								});
//
//					}
//
//				}
//			});
//
//		} else {
//			SC.say("ERROR", "ERROR: Please select record to delete?");
//		}
//	}
//
//}
