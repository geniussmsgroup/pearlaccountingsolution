package com.pearl.accounting.sysui.client.views.windows;

import com.pearl.accounting.sysui.client.views.widgets.ComboBox;
import com.pearl.accounting.sysui.client.views.widgets.TextField;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class AssessmentPeriodWindow extends Window {

	private ComboBox financialYear;
	private TextField periodCode;
	private TextField periodName;
	private TextField periodDescription;
	private DateItem startDate;
	private DateItem endDate;
	
	

	private IButton saveButton;

	public AssessmentPeriodWindow() {
		super();
		financialYear = new ComboBox();
		financialYear.setTitle("financial Year");
		
		periodCode = new TextField();
		periodCode.setTitle("Code");

		periodName = new TextField();
		periodName.setTitle("Period");
		
		periodDescription = new TextField();
		periodDescription.setTitle("Description");

		startDate = new DateItem();
		startDate.setTitle("Start date");
		startDate.setUseTextField(true);
		startDate.setWidth("*");
		startDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);


		endDate = new DateItem();
		endDate.setTitle("End date");
		endDate.setUseTextField(true);
		endDate.setWidth("*");
		endDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);


		saveButton = new IButton("Save");

		DynamicForm form = new DynamicForm();
		form.setFields(periodCode, periodName,periodDescription,startDate,endDate);
		form.setWrapItemTitles(false);
		form.setMargin(10);

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(saveButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(form);
		layout.addMember(buttonLayout);

		layout.setMargin(10);
		this.addItem(layout);
		this.setWidth("40%");
		this.setHeight("40%");
		this.setAutoCenter(true);
		this.setTitle("Add assement period");
		this.setIsModal(true);
		this.setShowModalMask(true);
	}

	public ComboBox getFinancialYear() {
		return financialYear;
	}

	public TextField getPeriodCode() {
		return periodCode;
	}

	public TextField getPeriodName() {
		return periodName;
	}

	public TextField getPeriodDescription() {
		return periodDescription;
	}

	public DateItem getStartDate() {
		return startDate;
	}

	public DateItem getEndDate() {
		return endDate;
	}

	public IButton getSaveButton() {
		return saveButton;
	}
	
	

}
