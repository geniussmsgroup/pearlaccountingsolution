package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.google.gwt.i18n.client.NumberFormat;
import com.pearl.accounting.sysmodel.MultiJournal;
import com.pearl.accounting.sysui.client.utils.PAManager;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;

public class MultiJournalListgrid extends SuperListGrid {
	
	public static String ID = "id";

	public static String AmountPaid = "amountPaid";

	public static String AssessmentPeriodId = "AssessmentPeriodId";
	public static String AssessmentPeriod = "AssessmentPeriod";

	public static String FinancialYearId = "FinancialYearId";
	public static String FinancialYear = "FinancialYearId";

	public static String PaymentDate = "paymentDate";

	public static String AccountId = "payingAccountId";
	public static String Account = "payingAccount";

	public static String PaymentDetails = "PaymentDetails";

	public static String Remarks = "Remarks";
	

	public static String TransactionNumber = "transactionNumber";

	NumberFormat nf = NumberFormat.getFormat("#,##0.00");

	private MultiJournalDetailListgrid listgrid;

	public MultiJournalListgrid() {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField amountPaid = new ListGridField(AmountPaid, "Amount");

		amountPaid.setShowGridSummary(true);

		amountPaid.setSummaryFunction(new SummaryFunction() {
			public Object getSummaryValue(Record[] records, ListGridField field) {

				long totalAmount = 0;
				for (Record record : records) {

					totalAmount += Float
							.parseFloat(PAManager.getInstance().unformatCash(record.getAttribute(AmountPaid)));

				}
				return "" + PAManager.getInstance().formatCash(totalAmount);
			}
		});

		ListGridField payingAccountId = new ListGridField(AccountId, "Account Id");
		payingAccountId.setHidden(true);

		ListGridField payingAccount = new ListGridField(Account, "Account");

		ListGridField paymentDate = new ListGridField(PaymentDate, "Date");

		ListGridField assessmentPeriodId = new ListGridField(AssessmentPeriodId, "PeriodId");
		assessmentPeriodId.setHidden(true);

		ListGridField assessmentPeriod = new ListGridField(AssessmentPeriod, "Period");

		ListGridField financialYearId = new ListGridField(FinancialYearId, "YearId");
		financialYearId.setHidden(true);

		ListGridField financialYear = new ListGridField(FinancialYear, "Year");

		ListGridField paymentDetails = new ListGridField(PaymentDetails, "Details");
		ListGridField remarks = new ListGridField(Remarks, "Remarks");
		ListGridField transactionNumber = new ListGridField(TransactionNumber, "Transaction Id");

		this.setFields(id, transactionNumber, amountPaid, payingAccountId, payingAccount, paymentDate, paymentDetails,
				remarks, assessmentPeriodId, assessmentPeriod, financialYearId, financialYear);

		this.setCanExpandRecords(true);
		this.setShowGridSummary(true);

	}

	public ListGridRecord addRowData(MultiJournal payment) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute(ID, payment.getId());

		record.setAttribute(AmountPaid, nf.format(payment.getAmountPosted()));

//		if (payment.getTransactionAccount() != null) {
//			record.setAttribute(AccountId, payment.getTransactionAccount().getId());
//			record.setAttribute(Account, payment.getTransactionAccount().getAccountName());
//		}

		record.setAttribute(PaymentDate, payment.getTransactionDate());

		if (payment.getPeriod() != null) {
			//record.setAttribute(AssessmentPeriodId, payment.getAssessmentPeriod().getId());
			record.setAttribute(AssessmentPeriod, payment.getPeriod());
		}

		//if (payment.getYear() != null) {

		//	record.setAttribute(FinancialYearId, payment.getFinancialYear().getId());
			record.setAttribute(FinancialYear, payment.getYear());
		//}

		record.setAttribute(PaymentDetails, payment.getTransactionDetails());
		record.setAttribute(Remarks, payment.getRemarks());
		record.setAttribute(TransactionNumber, payment.getTransactionNumber());

		return record;
	}

	public void addRecordsToGrid(List<MultiJournal> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (MultiJournal item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
	}

	@Override
	protected Canvas getExpansionComponent(final ListGridRecord record) {

		VLayout layout = new VLayout(5);

		if (listgrid != null) {
			final ListGrid grid = this;
			for (ListGridRecord gr : grid.getRecords()) {
				if (gr != record) {
					grid.collapseRecord(gr);
				}
			}
			grid.selectRecord(record);

			layout.setPadding(5);

			listgrid.setWidth100();
			listgrid.setHeight(224);
			listgrid.setCellHeight(22);

			VLayout schedulePane = new VLayout();
			schedulePane.setMembers(listgrid);

			Tab accounttab = new Tab();
			accounttab.setPane(schedulePane);
			accounttab.setTitle("Accounts");

			TabSet tabSet = new TabSet();
			tabSet.addTab(accounttab);

			layout.addMember(tabSet);
			layout.setWidth100();
			layout.setHeight(270);

		}
		return layout;
	}

	public MultiJournalDetailListgrid getListgrid() {
		return listgrid;
	}

	public void setListgrid(MultiJournalDetailListgrid listgrid) {
		this.listgrid = listgrid;
	}

}
