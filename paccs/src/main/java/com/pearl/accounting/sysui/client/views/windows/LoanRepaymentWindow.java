package com.pearl.accounting.sysui.client.views.windows;

import com.pearl.accounting.sysui.client.views.widgets.ComboBox;
import com.pearl.accounting.sysui.client.views.widgets.TextField;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class LoanRepaymentWindow extends Window {
 
	private TextField depositedAmount;
	private ComboBox assessmentPeriod;
	private ComboBox financialYear;
	private DateItem depositDate;
	private ComboBox receiveingAccount;
	private TextField paymentDetails;
	private TextField remarks;
	// private TextField exchangeRate;

	private IButton saveButton;

	public LoanRepaymentWindow() {
		super();

		financialYear = new ComboBox();
		financialYear.setTitle("Year");

		assessmentPeriod = new ComboBox();
		assessmentPeriod.setTitle("Period");

		depositedAmount = new TextField();
		depositedAmount.setTitle("Amount deposited");
		depositedAmount.setKeyPressFilter("[0-9.]");


		depositDate = new DateItem();
		depositDate.setTitle("Deposit Date");
		depositDate.setUseTextField(true);
		depositDate.setWidth("*");
		depositDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);


		receiveingAccount = new ComboBox();
		receiveingAccount.setTitle("Receiveing Account");

		paymentDetails = new TextField();
		paymentDetails.setTitle("Payment Details");

		remarks = new TextField();
		remarks.setTitle("Remarks");

		saveButton = new IButton("Save");

		DynamicForm form = new DynamicForm();
		form.setFields(financialYear, assessmentPeriod);
		form.setWrapItemTitles(true);
		form.setMargin(10);
		form.setNumCols(4);

		DynamicForm form2 = new DynamicForm();
		form2.setFields( depositedAmount, depositDate, receiveingAccount, paymentDetails, remarks);
		form2.setWrapItemTitles(true);
		form2.setMargin(10);
		form2.setNumCols(4);

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(saveButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(form);
		layout.addMember(form2);
		layout.addMember(buttonLayout);

		layout.setMargin(10);
		this.addItem(layout);
		this.setWidth("60%");
		this.setHeight("40%");
		this.setAutoCenter(true);
		this.setTitle("Loan Repayments");
		this.setIsModal(true);
		this.setShowModalMask(true);
	}
 

	public TextField getDepositedAmount() {
		return depositedAmount;
	}

	public ComboBox getAssessmentPeriod() {
		return assessmentPeriod;
	}

	public ComboBox getFinancialYear() {
		return financialYear;
	}

	public DateItem getDepositDate() {
		return depositDate;
	}

	public ComboBox getReceiveingAccount() {
		return receiveingAccount;
	}

	public TextField getPaymentDetails() {
		return paymentDetails;
	}

	public TextField getRemarks() {
		return remarks;
	}

	public IButton getSaveButton() {
		return saveButton;
	}

}
