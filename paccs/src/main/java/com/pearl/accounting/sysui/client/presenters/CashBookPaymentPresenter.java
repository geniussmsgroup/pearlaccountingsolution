package com.pearl.accounting.sysui.client.presenters;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.shared.DispatchAsync;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.AssessmentPeriod;
import com.pearl.accounting.sysmodel.CashBookPayment;
import com.pearl.accounting.sysmodel.CashBookPaymentDetail;
import com.pearl.accounting.sysmodel.DataSourceScreen;
import com.pearl.accounting.sysmodel.FinancialYear;
import com.pearl.accounting.sysmodel.Intervaltb;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.Theadp;
import com.pearl.accounting.sysui.client.place.NameTokens;
import com.pearl.accounting.sysui.client.utils.PAConstant;
import com.pearl.accounting.sysui.client.utils.PAManager;
import com.pearl.accounting.sysui.client.views.listgrids.CashBookPaymentDetailListgrid;
import com.pearl.accounting.sysui.client.views.listgrids.CashBookPaymentListgrid;
import com.pearl.accounting.sysui.client.views.panes.CashBookPaymentPane;
import com.pearl.accounting.sysui.client.views.widgets.ReportDisplayWindow;
import com.pearl.accounting.sysui.client.views.widgets.SwizimaLoader;
import com.pearl.accounting.sysui.client.views.windows.CashBookPaymentWindow;
import com.pearl.accounting.sysui.client.views.windows.DoubleEntryWindow;
import com.pearl.accounting.sysui.shared.SysAction;
import com.pearl.accounting.sysui.shared.SysResult;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.EditorExitEvent;
import com.smartgwt.client.widgets.form.fields.events.EditorExitHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordExpandEvent;
import com.smartgwt.client.widgets.grid.events.RecordExpandHandler;

public class CashBookPaymentPresenter
		extends Presenter<CashBookPaymentPresenter.MyView, CashBookPaymentPresenter.MyProxy> {

	private final PlaceManager placeManager;
	private final DispatchAsync dispatcher;

	public interface MyView extends View {
		public CashBookPaymentPane getCashBookPaymentPane();

	}

	@ProxyCodeSplit
	@NameToken(NameTokens.cashbookpayments)
	public interface MyProxy extends ProxyPlace<CashBookPaymentPresenter> {
	}

	@Inject
	public CashBookPaymentPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
			final DispatchAsync dispatcher, final PlaceManager placeManager) {
		super(eventBus, view, proxy);

		this.dispatcher = dispatcher;
		this.placeManager = placeManager;
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this, MainPagePresenter.TYPE_SetContextAreaContent, this);
	}

	@Override
	protected void onBind() {
		super.onBind();

		onNewButtonClicked();
		
		loadCashBookPayments();

		onLoadButtonClicked();

		onRecordExpanded();

		onExportButtonClicked();
	}

	// TODO Auto-generated method stub

//						SC.clearPrompt();
//
//						
//								// TODO Auto-generated method stub
//								while 
//									window.getPeri().setTitle(period.getPeriodName());

	private void onNewButtonClicked() {
		getView().getCashBookPaymentPane().getAddButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				loadNewCashbookPayment();

			}
		});

		getView().getCashBookPaymentPane().getControlsPane().getNewAssetButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadNewCashbookPayment();

			}
		});
	}

//	private void loadPeriods(final CashBookPaymentWindow window) {
//		SC.showPrompt("", "", new SwizimaLoader());
//
//		dispatcher.execute(new SysAction(PAConstant.GET_AssessmentPeriod), new AsyncCallback<SysResult>() {
//			public void onFailure(Throwable caught) {
//				System.out.println(caught.getMessage());
//			}
//
//			public void onSuccess(SysResult result) {
//
//				SC.clearPrompt();
//
//				if (result != null) {
//
//					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
//
//					for (AssessmentPeriod period : result.getAssessmentPeriods()) {
//
//						valueMap.put(period.getId(), period.getPeriodName());
//					}
//
//					window.getAssessmentPeriod().setValueMap(valueMap);
//
//				} else {
//					SC.say("ERROR", "Unknow error");
//				}
//
//			}
//		});
//
//	}
	private void onDateLoaded(final CashBookPaymentWindow window) {

		window.getPaymentDate().addChangedHandler(new ChangedHandler() {

			public void onChanged(ChangedEvent event) {
				
				dispatcher.execute(new SysAction(PAConstant.GET_Interval), new AsyncCallback<SysResult>() {
					public void onFailure(Throwable caught) {
						System.out.println(caught.getMessage());
					}

					public void onSuccess(SysResult result) {
						// TODO Auto-generated method stub
						if (result != null) {
//								
							for (Intervaltb period : result.getIntervaltbs()) {

								final Date dn = window.getPaymentDate().getValueAsDate();
								if (dn.before(period.getFEND()) && dn.after(period.getFSTART())) {
									window.getAssessmentPeriod().setValue(period.getFINTNAME());
									window.getFinancialYear().setValue(period.getYRCODE());
									window.getFrom().setValue(period.getFSTART());
									window.getTo().setValue(period.getFEND());
									window.getPeriodcode().setValue(period.getFINTCODE());

								}
							}
						}
					}
				});
			}

			//@Override
				// TODO Auto-generated method stub
				
			//}
		});
	}

	private void currentperiods(final CashBookPaymentWindow window) {
		dispatcher.execute(new SysAction(PAConstant.GET_CurrentPeriod), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {
				// TODO Auto-generated method stub
				if (result != null) {
//						
					for (Intervaltb period : result.getIntervaltbs()) {

							window.getCurrentperiod().setValue(period.getFINTNAME());
							//window.getYear().setValue(period.getYRCODE());
							window.getCurrentstartdate().setValue(period.getFSTART());
							
							window.getCurrentenddate().setValue(period.getFEND());
							//period.window.getPeriodcode().setValue(period.getFINTCODE());
					
				}}
			}
		});
	}
	private void loadAccounts(final CashBookPaymentWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_ACCOUNT), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
					for (Account account : result.getAccounts()) {
						valueMap.put(account.getSysid(), account.getAccountName());
					}

					window.getPayingAccount().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void loadNewCashbookPayment() {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_ACCOUNT), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					CashBookPaymentWindow window = new CashBookPaymentWindow(result.getAccounts());

					loadAccounts(window);
//					loadPeriods(window);
					currentperiods(window);
					onDeleteButtonPressed(window);

					addPaymentDetails(window);

					onSaveButtonClicked(window);
					onDateLoaded(window);
//					newfunc(window);
					// onActive(window);

					window.animateShow();

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}

			private void newfunc(final CashBookPaymentWindow window) {
				// TODO Auto-generated method stub
				window.getPaidAmount().setTitle("Amount2");
			}
		});

	}

	private void onDeleteButtonPressed(final CashBookPaymentWindow window) {
		window.getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if (window.getCashBookPaymentDetailListgrid().anySelected()) {
					SC.ask("Are you sure you want to delete the selected record", new BooleanCallback() {

						public void execute(Boolean value) {
							// TODO Auto-generated method stub
							if (value == true) {
//								String nm;
//								String nm2;
//								String nm3;

								for (ListGridRecord record : window.getCashBookPaymentDetailListgrid()
										.getSelectedRecords()) {
//									
									window.getCashBookPaymentDetailListgrid().removeData(record);

//									dispatcher.execute(new SysAction(PAConstant.DELETE_WITHDRAWAL, withdrawal),

								}

							}
						}
					});

				} else {
					SC.say("ERROR", "please select record to delete");
				}

			}
		});

	}

	private void addPaymentDetails(final CashBookPaymentWindow window) {
		window.getAddButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				CashBookPaymentDetail paymentDetail = new CashBookPaymentDetail();

				window.getCashBookPaymentDetailListgrid().addRecordToGrid(paymentDetail);

			}
		});

	}

	private void onSaveButtonClicked(final CashBookPaymentWindow window) {
		window.getSaveButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				if (isFormValid(window)) {

//					FinancialYear financialYear = new FinancialYear();
//					financialYear.setId(window.getFinancialYear().getValueAsString());
//
//					AssessmentPeriod assessmentPeriod = new AssessmentPeriod();
//					assessmentPeriod.setId(window.getAssessmentPeriod().getValueAsString());

					String payee = window.getPayee().getValueAsString();

					float amountPaid = Float.parseFloat(
							PAManager.getInstance().unformatCash(window.getPaidAmount().getValueAsString()));

					Date paymentDate = window.getPaymentDate().getValueAsDate();

					Account payingAccount = new Account();
					payingAccount.setSysid(window.getPayingAccount().getValueAsString());

					String remarks = window.getRemarks().getValueAsString();
					String voucherNumber = window.getPaymentDetails().getValueAsString();

					// Save to theadp;
					

					CashBookPayment cashBookPayment = new CashBookPayment();
					cashBookPayment.setAmountPaid(amountPaid);
					cashBookPayment.setPeriod(window.getAssessmentPeriod().getValueAsString());
					int year = Integer.parseInt(window.getFinancialYear().getValueAsString());
					// cashBookPayment.setCreatedBy(createdBy);
					cashBookPayment.setDateCreated(new Date());
					cashBookPayment.setDateUpdated(new Date());
					cashBookPayment.setYear(year);
					cashBookPayment.setPayee(payee);
					cashBookPayment.setPayingAccount(payingAccount);
					cashBookPayment.setPaymentDate(paymentDate);
					cashBookPayment.setRemarks(remarks);
					cashBookPayment.setStatus(Status.ACTIVE);
					cashBookPayment.setPeriodcode(window.getPeriodcode().getValueAsString());
					// cashBookPayment.setUpdatedBy(updatedBy);
					cashBookPayment.setVoucherNumber(voucherNumber);

					List<CashBookPaymentDetail> cashBookPaymentDetails = new ArrayList<CashBookPaymentDetail>();

					for (ListGridRecord record : window.getCashBookPaymentDetailListgrid().getRecords()) {

						Account receivingAccount = new Account();
						receivingAccount.setSysid(record.getAttribute(CashBookPaymentDetailListgrid.Account));

						float receivedAmount = Float.parseFloat(PAManager.getInstance()
								.unformatCash(record.getAttribute(CashBookPaymentDetailListgrid.AmountPaid)));
						Date payDate = window.getPaymentDate().getValueAsDate();

						String details = record.getAttribute(CashBookPaymentDetailListgrid.Remarks);

						CashBookPaymentDetail cashBookPaymentDetail = new CashBookPaymentDetail();

						cashBookPaymentDetail.setCashBookPayment(cashBookPayment);
						cashBookPaymentDetail.setPeriodcode(window.getPeriodcode().getValueAsString());
						cashBookPaymentDetail.setDateCreated(new Date());
						cashBookPaymentDetail.setDateUpdated(new Date());

						cashBookPaymentDetail.setPaymentDate(payDate);
						cashBookPaymentDetail.setReceivedAmount(receivedAmount);
						cashBookPaymentDetail.setReceivingAccount(receivingAccount);
						cashBookPaymentDetail.setRemarks(details);
						cashBookPaymentDetail.setStatus(Status.ACTIVE);

						cashBookPaymentDetails.add(cashBookPaymentDetail);

					}
					cashBookPayment.setCashBookPaymentDetails(cashBookPaymentDetails);
					
					SC.showPrompt("", "", new SwizimaLoader());

					dispatcher.execute(new SysAction(PAConstant.SAVE_CASHBOOK_PAYMENT, cashBookPayment),
							new AsyncCallback<SysResult>() {
								public void onFailure(Throwable caught) {
									System.out.println(caught.getMessage());
								}

								public void onSuccess(SysResult result) {
									clearForm(window);
									SC.clearPrompt();

									if (result != null) {

										if (result.getSwizFeedback().isResponse()) {

											SC.say("SUCCESS", result.getSwizFeedback().getMessage());

											getView().getCashBookPaymentPane().getCashBookPaymentListgrid()
													.addRecordsToGrid(result.getCashBookPayments());
										} else {
											SC.warn("ERROR", result.getSwizFeedback().getMessage());
										}
									} else {
										SC.say("ERROR", "Unknow error");
									}
								}
							});
				}
			}
		});
	}

	private boolean isFormValid(final CashBookPaymentWindow window) {
		if (window.getFinancialYear().getValueAsString() != null) {
			if (window.getAssessmentPeriod().getValueAsString() != null) {
				if (window.getPayee().getValueAsString() != null) {
					if (window.getPaidAmount().getValueAsString() != null) {
						if (window.getRemarks().getValueAsString() != null) {
							if (window.getPayingAccount().getValueAsString() != null) {
								if (window.getCashBookPaymentDetailListgrid().getRecords().length > 0) {

									float total = 0;
									for (ListGridRecord record : window.getCashBookPaymentDetailListgrid()
											.getRecords()) {
										total += Float.parseFloat(
												record.getAttributeAsString(CashBookPaymentDetailListgrid.AmountPaid));
									}

									float overallTotal = Float.parseFloat(window.getPaidAmount().getValueAsString());

									if (total == overallTotal) {
										return true;
									} else {
										SC.warn("Error",
												"The amount values dont match T= " + total + " OT= " + overallTotal);
									}

									//
								} else {
									SC.warn("Error", "Please Add atleast one receiving Account");

								}
							} else {
								SC.warn("Error", "Please Select Paying Account");

							}
						} else {
							SC.warn("Error", "Please enter Remarks");
						}

					} else {
						SC.warn("Error", "Please enter Amount");

					}
				} else {
					SC.warn("Error", "Please enter Payee");
				}

			} else {
				SC.warn("Error", "Please select Period");

			}

		} else

		{
			SC.warn("Error", "Please select financial Year");
		}

		return false;
	}

	private void clearForm(final CashBookPaymentWindow window) {
		window.getPayee().clearValue();
		window.getPaidAmount().clearValue();
		window.getRemarks().clearValue();
		window.getPayingAccount().clearValue();
		window.getPaymentDate().clearValue();
		window.getFinancialYear().clearValue();
		window.getAssessmentPeriod().clearValue();
	}

	private void onLoadButtonClicked() {
		getView().getCashBookPaymentPane().getLoadButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadCashBookPayments();

			}
		});

		getView().getCashBookPaymentPane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadCashBookPayments();

			}
		});
	}

	private void loadCashBookPayments() {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_CASHBOOK_PAYMENT), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					getView().getCashBookPaymentPane().getCashBookPaymentListgrid()
							.addRecordsToGrid(result.getCashBookPayments());

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void onRecordExpanded() {
		getView().getCashBookPaymentPane().getCashBookPaymentListgrid()
				.addRecordExpandHandler(new RecordExpandHandler() {

					public void onRecordExpand(RecordExpandEvent event) {

						getView().getCashBookPaymentPane().getCashBookPaymentListgrid().deselectAllRecords();
						getView().getCashBookPaymentPane().getCashBookPaymentListgrid().selectRecord(event.getRecord());

						CashBookPaymentDetailListgrid listgrid = new CashBookPaymentDetailListgrid();

						getView().getCashBookPaymentPane().getCashBookPaymentListgrid().setListgrid(listgrid);

						CashBookPayment cashBookPayment = new CashBookPayment();
						cashBookPayment.setId(getView().getCashBookPaymentPane().getCashBookPaymentListgrid()
								.getSelectedRecord().getAttribute(CashBookPaymentListgrid.ID));

						SC.showPrompt("", "", new SwizimaLoader());

						dispatcher.execute(new SysAction(PAConstant.GET_CASHBOOK_PAYMENT_DETAILS, cashBookPayment),
								new AsyncCallback<SysResult>() {
									public void onFailure(Throwable caught) {
										System.out.println(caught.getMessage());
									}

									public void onSuccess(SysResult result) {

										SC.clearPrompt();

										if (result != null) {

											getView().getCashBookPaymentPane().getCashBookPaymentListgrid()
													.getListgrid().addRecordsToGrid(result.getCashBookPaymentDetails());

										} else {
											SC.say("ERROR", "Unknow error");
										}

									}
								});

					}
				});
	}

	private void onExportButtonClicked() {
		getView().getCashBookPaymentPane().getControlsPane().getPrintButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				String request = "CASHBOOK_PAYMENT_EXPORT";

				String NAME = "_blank";
				String FEATURES = "width=760, height=480";

				StringBuilder url = new StringBuilder();
				url.append("dataExport").append("?");
				url.append("&" + "request" + "=" + request);
				Window.open(url.toString(), NAME, FEATURES);
				

			}
		});
	}

}
