package com.pearl.accounting.sysui.client.presenters;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.shared.DispatchAsync;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.pearl.accounting.sysui.client.place.NameTokens;
import com.pearl.accounting.sysui.client.utils.PAConstant;
import com.pearl.accounting.sysui.client.views.listgrids.IntervalListgrid;
import com.pearl.accounting.sysui.client.views.panes.GeneralLedgerPane;
import com.pearl.accounting.sysui.client.views.panes.IntervalPane;
import com.pearl.accounting.sysui.client.views.widgets.SwizimaLoader;
import com.pearl.accounting.sysui.client.views.windows.AccountWindow;
import com.pearl.accounting.sysui.shared.SysAction;
import com.pearl.accounting.sysui.shared.SysResult;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class IntervalPresenter extends Presenter<IntervalPresenter.MyView, IntervalPresenter.MyProxy> {

	private final PlaceManager placeManager;
	private final DispatchAsync dispatcher;

	public interface MyView extends View {
		public IntervalPane getIntervalPane();

	}

	@ProxyCodeSplit
	@NameToken(NameTokens.Interval)
	public interface MyProxy extends ProxyPlace<IntervalPresenter> {
	}

	@Inject
	public IntervalPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
			final DispatchAsync dispatcher, final PlaceManager placeManager) {
		super(eventBus, view, proxy);

		this.dispatcher = dispatcher;
		this.placeManager = placeManager;
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this, MainPagePresenter.TYPE_SetContextAreaContent, this);
	}

	@Override
	protected void onBind() {
		super.onBind();
		load();
//		onNextIntervalClicked();
//		onPreviousIntervalClicked();
		onCurrent();
		onLoadButonClicked();
		onAddInterval();

	}

	

	private void onCurrent() {
		// TODO Auto-generated method stub
		getView().getIntervalPane().getCurrent().addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub

				if (getView().getIntervalPane().getIntervalListgrid().anySelected()) {
					for (ListGridRecord record1 : getView().getIntervalPane().getIntervalListgrid().getRecords()) {
						if (record1.getAttribute(IntervalListgrid.Current) == "CURRENT") {
							record1.setAttribute(IntervalListgrid.Current, " ");

						}
					}

					SC.ask("Make current Interval?", new BooleanCallback() {

						public void execute(Boolean value) {
							// TODO Auto-generated method stub
							if (value == true) {

								ListGridRecord ls = getView().getIntervalPane().getIntervalListgrid()
										.getSelectedRecord();
								ls.setAttribute(IntervalListgrid.Current, "CURRENT");

							}
						}
					});

					SC.say("Current interval Selected");
				} else {
					SC.say("Please select interval");
				}

			}
		});

	}

	private void onAddInterval() {
		// TODO Auto-generated method stub
		getView().getIntervalPane().getNextInterval().addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub

				dispatcher.execute(new SysAction(PAConstant.NEXT_Interval), new AsyncCallback<SysResult>() {
					public void onFailure(Throwable caught) {
						System.out.println(caught.getMessage());
					}

					public void onSuccess(SysResult result) {

						SC.say(result.getSwizFeedback().getMessage());

					}
				});

			}
		});
	}

	

	private void onLoadButonClicked() {
		getView().getIntervalPane().getLoadButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				load();

			}
		});

		getView().getIntervalPane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				load();

			}
		});
	}

	private void load() {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_Interval), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					getView().getIntervalPane().getIntervalListgrid().addRecordsToGrid(result.getIntervaltbs());

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

}
