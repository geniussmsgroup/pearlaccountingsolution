package com.pearl.accounting.sysui.client.views.listgrids;

import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.widgets.DataBoundComponent; 

public class UserDS extends LocalDataSource {

	public static String ID = "id";
	public static String USER_NAME = "userName";
	public static String FIRST_NAME = "firstName";
	public static String LAST_NAME = "lastName";
	public static String PHONE_NUMBER = "phoneNo";
	public static String EMAIL = "email";
	public static String ROLE_ID = "roleId";
	public static String ROLE = "role";
	public static String PARTNER = "partner";
	
	public static String PARTNER_ID = "partnerId";
 
 
	public UserDS(DataBoundComponent dbc) {
		super(dbc);
    
		DataSourceTextField id = new DataSourceTextField(ID, "Id");
		id.setHidden(true);

		DataSourceTextField userName = new DataSourceTextField(USER_NAME, "User Name");

		DataSourceTextField firstName = new DataSourceTextField(FIRST_NAME, "First Name");
		DataSourceTextField lastName = new DataSourceTextField(LAST_NAME, "Last Name");

		DataSourceTextField phoneNo = new DataSourceTextField(PHONE_NUMBER, "Phone number");
		DataSourceTextField role = new DataSourceTextField(ROLE, "Role");

		DataSourceTextField roleId = new DataSourceTextField(ROLE_ID, "Role Id");
		roleId.setHidden(true);

		DataSourceTextField email = new DataSourceTextField(EMAIL, "Email");
		
		DataSourceTextField partner = new DataSourceTextField(PARTNER, "Partner");
		DataSourceTextField partnerId = new DataSourceTextField(PARTNER_ID, "Partner Id");
		partnerId.setHidden(true);

		this.setFields(id, userName, firstName, lastName, phoneNo, roleId, role, email, partner,partnerId);
 
		 setClientOnly(true);  

	}

}
