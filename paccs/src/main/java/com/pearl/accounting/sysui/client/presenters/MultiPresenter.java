package com.pearl.accounting.sysui.client.presenters;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.shared.DispatchAsync;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.AssessmentPeriod;
import com.pearl.accounting.sysmodel.CashBookReceipt;
import com.pearl.accounting.sysmodel.CashBookReceiptDetail;
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.DataSourceScreen;
import com.pearl.accounting.sysmodel.FinancialYear;
import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysmodel.Multi;
import com.pearl.accounting.sysmodel.MultiDetail;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.Theadp;
import com.pearl.accounting.sysmodel.Ttype;
import com.pearl.accounting.sysui.client.place.NameTokens;
import com.pearl.accounting.sysui.client.utils.PAConstant;
import com.pearl.accounting.sysui.client.utils.PAManager;
import com.pearl.accounting.sysui.client.views.listgrids.CashBookReceiptDetailListgrid;
import com.pearl.accounting.sysui.client.views.listgrids.CashBookReceiptListgrid;
import com.pearl.accounting.sysui.client.views.listgrids.MultiDetailListgrid;
import com.pearl.accounting.sysui.client.views.listgrids.MultiJournalDetailListgrid;
import com.pearl.accounting.sysui.client.views.listgrids.MultiListgrid;
import com.pearl.accounting.sysui.client.views.panes.CashBookReceiptPane;
import com.pearl.accounting.sysui.client.views.panes.MultiPane;
import com.pearl.accounting.sysui.client.views.widgets.SwizimaLoader;
import com.pearl.accounting.sysui.client.views.windows.CashBookPaymentWindow;
import com.pearl.accounting.sysui.client.views.windows.CashBookReceiptWindow;
import com.pearl.accounting.sysui.client.views.windows.MultiWindow;
import com.pearl.accounting.sysui.shared.SysAction;
import com.pearl.accounting.sysui.shared.SysResult;
import com.smartgwt.client.docs.DateInputFormat;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.EditorExitEvent;
import com.smartgwt.client.widgets.form.fields.events.EditorExitHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordExpandEvent;
import com.smartgwt.client.widgets.grid.events.RecordExpandHandler;

public class MultiPresenter
		extends Presenter<MultiPresenter.MyView, MultiPresenter.MyProxy> {

	private final PlaceManager placeManager;
	private final DispatchAsync dispatcher;

	public interface MyView extends View {

		public MultiPane getMultiPane();

	}

	@ProxyCodeSplit
	@NameToken(NameTokens.multi)
	public interface MyProxy extends ProxyPlace<MultiPresenter> {
	}

	@Inject
	public MultiPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
			final DispatchAsync dispatcher, final PlaceManager placeManager) {
		super(eventBus, view, proxy);

		this.dispatcher = dispatcher;
		this.placeManager = placeManager;
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this, MainPagePresenter.TYPE_SetContextAreaContent, this);
	}

	@Override
	protected void onBind() {
		super.onBind();

		onNewButtonClicked();

		loadReceipts();

		onLoadButtonaclicked();

		onRecordExpanded();

	}
	
	private void onActive(final MultiWindow window) {
		dispatcher.execute(new SysAction(PAConstant.GET_AssessmentPeriod), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {
				// TODO Auto-generated method stub
				if (result != null) {
//					
					for (AssessmentPeriod pr : result.getAssessmentPeriods()) {
						
					String act = pr.getActivationStatus().getStatus();
						if((act=="Active")||(act=="ACTIVE")){
							window.getActi().getPeriod().setValue(pr.getPeriodName());
							window.getActi().getFro().setValue(pr.getStartDate());;
							window.getActi().getT().setValue(pr.getEndDate());
							window.getActi().getY().setValue(pr.getFinancialYear().getFinancialYear());
						}
					
					}
					}}});}
	private void onDateLoaded(final MultiWindow window) {

window.getPaymentDate().addEditorExitHandler(new EditorExitHandler() {
	
	public void onEditorExit(EditorExitEvent event) {
		
			
			dispatcher.execute(new SysAction(PAConstant.GET_AssessmentPeriod), new AsyncCallback<SysResult>() {
				public void onFailure(Throwable caught) {
					System.out.println(caught.getMessage());
				}

				public void onSuccess(SysResult result) {
					// TODO Auto-generated method stub
					if (result != null) {
//						
						for (AssessmentPeriod period : result.getAssessmentPeriods()) {

							final Date dn = window.getPaymentDate().getValueAsDate();
							if (dn.before(period.getEndDate()) && dn.after(period.getStartDate())) {
								window.getAssessmentPeriod().setValue(period.getPeriodName());
								 window.getFinancialYear().setValue(period.getFinancialYear().getFinancialYear());
								 window.getFrom().setValue(period.getStartDate());
								 window.getTo().setValue(period.getEndDate());}}}}});}});}
		

	private void onNewButtonClicked() {
		getView().getMultiPane().getAddButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				addNewCashBookReceipt();

			}
		});

		getView().getMultiPane().getControlsPane().getNewAssetButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				addNewCashBookReceipt();

			}
		});
	}

	private void addNewCashBookReceipt() {

		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_MULTI_DETAILS_INPUT), new AsyncCallback<SysResult>() {
				public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}
					public void onSuccess(SysResult result) {

				SC.clearPrompt();
				if (result != null) {
					MultiWindow window = new MultiWindow(result.getAccounts(),result.getCustomerAccounts(),result.getLoans());
					loadAccounts(window);
					loadPeriods(window);
					loadFinancialYears(window);
					addPaymentDetails(window);
					onSaveButtonClicked(window);
					onDeleteButtonPressed(window);
					onDateLoaded(window);
					onActive(window);
					window.show();

				} else {
					SC.say("ERROR", "Unknow error");
				}
				}
		});
		
	
	}

	private void loadPeriods(final MultiWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_AssessmentPeriod), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

					for (AssessmentPeriod period : result.getAssessmentPeriods()) {

						valueMap.put(period.getId(), period.getPeriodName());
					}

					window.getAssessmentPeriod().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

	}

	private void loadFinancialYears(final MultiWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_FinancialYear), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

					for (FinancialYear financialYear : result.getFinancialYears()) {

						valueMap.put(financialYear.getId(), financialYear.getFinancialYear());
					}

					window.getFinancialYear().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void loadAccounts(final MultiWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_ACCOUNT), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
					for (Account account : result.getAccounts()) {
						valueMap.put(account.getSysid(), account.getAccountName());
					}

					window.getReceivingAccount().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void onDeleteButtonPressed(final MultiWindow window){
		window.getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if(window.getMultiDetailListgrid().anySelected()) {
					SC.ask("Are you sure you want to delete the selected record", new BooleanCallback() {
						
						public void execute(Boolean value) {
							// TODO Auto-generated method stub
							if(value==true) {
								for(ListGridRecord record:window.getMultiDetailListgrid().getSelectedRecords()) {
									window.getMultiDetailListgrid().removeData(record);
								}
								
							}
						}
					});
					
					
				}else {
					SC.say("ERROR","please select record to delete");
				}
				

			}
		});
		
	}
	private void addPaymentDetails(final MultiWindow window) {
		window.getAddButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				MultiDetail receiptDetail = new MultiDetail();

				window.getMultiDetailListgrid().addRecordToGrid(receiptDetail);

			}
		});

	}

	private boolean FormValid(final MultiWindow window) {
		if (window.getFinancialYear().getValueAsString() != null) {

			if (window.getAssessmentPeriod().getValueAsString() != null) {

				if (window.getPaidAmount().getValueAsString() != null) {

					
						if (window.getRemarks().getValueAsString() != null) {

							if (window.getMultiDetailListgrid().getRecords().length > 0) {
								for (ListGridRecord record1 : window.getMultiDetailListgrid().getRecords()) {
									if ((record1
											.getAttributeAsString(MultiDetailListgrid.TransactionType) == "Cr")
											|| (record1.getAttributeAsString(
													MultiDetailListgrid.Account) == "Loan")) {
										

									}
								}

								float totaldebits = 0;
								float totalcredits = 0;
								for (ListGridRecord record : window.getMultiDetailListgrid().getRecords()) {
									if (record
											.getAttributeAsString(MultiDetailListgrid.TransactionType) == "Dr") {
										totaldebits += Float.parseFloat(
												record.getAttributeAsString(MultiDetailListgrid.AmountPaid));

									} else if (record
											.getAttributeAsString(MultiDetailListgrid.TransactionType) == "Cr") {
										totalcredits += Float.parseFloat(
												record.getAttributeAsString(MultiDetailListgrid.AmountPaid));

									}

								}
								if (totaldebits == totalcredits) {
									float overalltotal = Float.parseFloat(window.getPaidAmount().getValueAsString());
									if ((totaldebits == overalltotal) || (totalcredits == overalltotal)) {
										//if(window.)
										
										
							
										return true;
									} else {
										SC.warn("ERROR", "Amount values do not match");

									}

								} else {
									SC.warn("ERROR", "The Credits are not equal to the Debits");
								}
							} else {
								SC.warn("ERROR", "add account details");
							}
						} else {
							SC.warn("ERROR", "Please enter Remarks");

						}
					

				} else {
					SC.warn("ERROR", "Please enter Amount");

				}
			} else {
				SC.warn("ERROR", "Please select Period");
			}
		} else {
			SC.warn("ERROR", "Please select Financial Year");

		}

		return false; 
	}

	private void onSaveButtonClicked(final MultiWindow window) {
		window.getSaveButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				if (FormValid(window)) {

					FinancialYear financialYear = new FinancialYear();
					financialYear.setId(window.getFinancialYear().getValueAsString());

					AssessmentPeriod assessmentPeriod = new AssessmentPeriod();
					assessmentPeriod.setId(window.getAssessmentPeriod().getValueAsString());

					String paymentFrom = window.getPayee().getValueAsString();

					float amountReceived = Float.parseFloat(
							PAManager.getInstance().unformatCash(window.getPaidAmount().getValueAsString()));

//					DateInputFormat dateFormatter = DateInputFormat("dd.MM.yyyy");
//			        String format = dateFormatter.format(date);
//			        return format;

					Date paymentDate = window.getPaymentDate().getValueAsDate();

					String remarks = window.getRemarks().getValueAsString();
					String receiptNumber = window.getPaymentDetails().getValueAsString();
					// Save to theadp;
                    Theadp account = new Theadp();
                    account.setRemarks(remarks);
                    account.setVNO(receiptNumber+" "+paymentFrom);;
                    account.setRemarks(remarks);
                    account.setTdates(paymentDate);
                    account.setDataSourceScreen(DataSourceScreen.Multi);
                    account.setAmount(amountReceived);
                	account.setDateCreated(new Date());
					account.setDateUpdated(new Date());
					// String transactionNumber;

					Multi multi = new Multi();
					multi.setAmountRecieved(amountReceived);
					multi.setPeriod(window.getAssessmentPeriod().getValueAsString());

					int year =Integer.parseInt(window.getFinancialYear().getValueAsString());
					
					// cashBookPayment.setCreatedBy(createdBy);
					multi.setDateCreated(new Date());
					multi.setDateUpdated(new Date());
					multi.setYear(year);
					//cashBookReceipt.setPaymentfrom(paymentFrom);
					//cashBookReceipt.setAccount(receivingAccount);
					multi.setPaymentDate(paymentDate);
					multi.setRemarks(remarks+paymentFrom);
					multi.setStatus(Status.ACTIVE);
					// cashBookPayment.setTransactionNumber(transactionNumber);
					// cashBookPayment.setUpdatedBy(updatedBy);
					multi.setReceiptNumber(receiptNumber);

					List<MultiDetail> multidetails = new ArrayList<MultiDetail>();

					for (ListGridRecord record : window.getMultiDetailListgrid().getRecords()) {

						Account payingAccount = new Account();
						payingAccount.setSysid(record.getAttribute(MultiDetailListgrid.Account));

						float paidAmount = Float.parseFloat(PAManager.getInstance()
								.unformatCash(record.getAttribute(MultiDetailListgrid.AmountPaid)));
						Date payDate = window.getPaymentDate().getValueAsDate();

						String details = window.getRemarks().getValueAsString()+" "+record.getAttribute(MultiDetailListgrid.Remarks);

						CustomerAccount Customer = new CustomerAccount();
						Customer.setSysid(record.getAttribute(MultiDetailListgrid.CustomerAccount));
						Loan loan= new Loan();
						loan.setLOANID(record.getAttribute(MultiDetailListgrid.Loan));
						
						MultiDetail multidetail = new MultiDetail();
						if(payingAccount!=null) {
						multidetail.setMulti(multi);
						// cashBookPaymentDetail.setCreatedBy(createdBy);
						multidetail.setDateCreated(new Date());
						multidetail.setDateUpdated(new Date());
						multidetail.setPaymentDate(payDate);
						multidetail.setAmountPaid(paidAmount);
						multidetail.setAccount(payingAccount);
						multidetail.setRemarks(details);
						multidetail.setStatus(Status.ACTIVE);                    
                        multidetail.setTtype(Ttype
		.getTransactionType((record.getAttribute(MultiDetailListgrid.TransactionType))));
//					multidetail.setTtype(ttype);
						
				if(payingAccount.getAnalReq().equalsIgnoreCase("Y")) {
						multidetail.setCustomerAccount(Customer);
							multidetail.setLoan(loan);
							GWT.log("customerAccount Id: "+Customer.getSysid());
							GWT.log("Loan id :"+ loan.getLOANID());
					}else {
						multidetail.setCustomerAccount(null);
						multidetail.setLoan(null);
						GWT.log("customerAccount Id: "+Customer.getSysid());
						GWT.log("Loan id :"+ loan.getLOANID());
					}
						} 
                   
						multidetails.add(multidetail);
						

					}
					

					multi.setMultiDetails(multidetails);
										SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.SAVE_Thead, account), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
							System.out.println(caught.getMessage());
						}

						public void onSuccess(SysResult result) {

							SC.clearPrompt();

							if (result != null) {

								if (result.getSwizFeedback().isResponse()) {
									ClearForm(window);
									SC.say("SUCCESS", result.getSwizFeedback().getMessage());

								} else {
									SC.warn("ERROR", result.getSwizFeedback().getMessage());
								}

							} else {
								SC.say("ERROR", "Unknow error");
							}

						}
		});
					SC.showPrompt("", "", new SwizimaLoader());

					dispatcher.execute(new SysAction(PAConstant.SAVE_Multi, multi),
							new AsyncCallback<SysResult>() {
								public void onFailure(Throwable caught) {
									System.out.println(caught.getMessage());
								}

								public void onSuccess(SysResult result) {

									ClearForm(window);

									SC.clearPrompt();

									if (result != null) {

										if (result.getSwizFeedback().isResponse()) {

											SC.say("SUCCESS", result.getSwizFeedback().getMessage());

											getView().getMultiPane().getMultiListgrid()
													.addRecordsToGrid(result.getMultis());
										} else {
											SC.warn("ERROR", result.getSwizFeedback().getMessage());
										}
									} else {
										SC.say("ERROR", "Unknow error");
									}
								}});}}});}

	private void ClearForm(final MultiWindow window) {
		window.getPaidAmount().clearValue();
		window.getRemarks().clearValue();
		window.getPayee().clearValue();
		window.getReceivingAccount().clearValue();
		

	}

	private void onLoadButtonaclicked() {
		getView().getMultiPane().getLoadButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadReceipts();

			}
		});

		getView().getMultiPane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadReceipts();

			}
		});

	}

	private void loadReceipts() {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_Multi), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					getView().getMultiPane().getMultiListgrid()
							.addRecordsToGrid(result.getMultis());

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void onRecordExpanded() {
		getView().getMultiPane().getMultiListgrid()
				.addRecordExpandHandler(new RecordExpandHandler() {

					public void onRecordExpand(RecordExpandEvent event) {

						getView().getMultiPane().getMultiListgrid().deselectAllRecords();
						getView().getMultiPane().getMultiListgrid().selectRecord(event.getRecord());

						MultiDetailListgrid listgrid = new MultiDetailListgrid();

						getView().getMultiPane().getMultiListgrid().setListgrid(listgrid);

						Multi cashBookReceipt = new Multi();
						cashBookReceipt.setId(getView().getMultiPane().getMultiListgrid()
								.getSelectedRecord().getAttribute(MultiListgrid.ID));

						SC.showPrompt("", "", new SwizimaLoader());

						dispatcher.execute(new SysAction(PAConstant.GET_Multi_DETAILS, cashBookReceipt),
								new AsyncCallback<SysResult>() {
									public void onFailure(Throwable caught) {
										System.out.println(caught.getMessage());
									}

									public void onSuccess(SysResult result) {

										SC.clearPrompt();

										if (result != null) {

											getView().getMultiPane().getMultiListgrid()
													.getListgrid().addRecordsToGrid(result.getMultiDetails());

										} else {
											SC.say("ERROR", "Unknow error");
										}

									}
								});

					}
				});
	}
	
	
	
	
	
}
