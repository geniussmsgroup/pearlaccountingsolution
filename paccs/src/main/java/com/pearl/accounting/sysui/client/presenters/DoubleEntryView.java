package com.pearl.accounting.sysui.client.presenters;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.pearl.accounting.sysui.client.views.panes.AccountPane;
import com.pearl.accounting.sysui.client.views.panes.DoubleEntryPane;
import com.smartgwt.client.widgets.layout.VLayout;

public class DoubleEntryView extends ViewImpl implements DoubleEntryPresenter.MyView {

	private static final String DEFAULT_MARGIN = "0px";
	private VLayout panel;
	private DoubleEntryPane doubleentryPane;

	@Inject
	public DoubleEntryView() {

		panel = new VLayout();

		doubleentryPane = new DoubleEntryPane();

		panel.setMembers(doubleentryPane);
		panel.setWidth100();
		panel.setHeight("90%");
		Window.enableScrolling(false);
		Window.setMargin(DEFAULT_MARGIN);

	}

	public Widget asWidget() {
		return panel;
	}

	public DoubleEntryPane getDoubleEntryPane() {
		return doubleentryPane;
	}
	
	

}
