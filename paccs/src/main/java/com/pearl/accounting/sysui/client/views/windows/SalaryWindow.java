package com.pearl.accounting.sysui.client.views.windows;
 
import com.pearl.accounting.sysui.client.views.listgrids.SalaryListgrid;
import com.pearl.accounting.sysui.client.views.widgets.TextField;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class SalaryWindow extends Window {

	 
	private TextField accountName;
	private TextField amount; 
	private DateItem date; 

	private IButton saveButton;
	
	private SalaryListgrid salaryListgrid;

	public SalaryWindow() {
		super();  
		accountName = new TextField();
		accountName.setTitle("First Name");

		amount = new TextField();
		amount.setTitle("Amount");
		amount.setKeyPressFilter("[0-9.]");


		date = new DateItem();
		date.setTitle("Date"); 
		date.setUseTextField(true);
		date.setWidth("*");
		date.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);


		saveButton = new IButton("Save");
		
		salaryListgrid =new SalaryListgrid();

		DynamicForm form = new DynamicForm();
		form.setFields(accountName, amount, date);
		form.setWrapItemTitles(true);
		form.setMargin(10);

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(saveButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(form);
		layout.addMember(buttonLayout);
		layout.addMember(salaryListgrid); 
		layout.setMargin(10);
		this.addItem(layout);
		this.setWidth("40%");
		this.setHeight("40%");
		this.setAutoCenter(true);
		this.setTitle("Account Definition");
		this.setIsModal(true);
		this.setShowModalMask(true);
	}

	public TextField getAccountName() {
		return accountName;
	}

	public TextField getAmount() {
		return amount;
	}

	public DateItem getDate() {
		return date;
	}

	public IButton getSaveButton() {
		return saveButton;
	}

	public SalaryListgrid getSalaryListgrid() {
		return salaryListgrid;
	}


}
