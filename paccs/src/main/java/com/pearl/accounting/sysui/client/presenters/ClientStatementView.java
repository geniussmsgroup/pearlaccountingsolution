package com.pearl.accounting.sysui.client.presenters;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.pearl.accounting.sysui.client.views.panes.AccountPane;
import com.pearl.accounting.sysui.client.views.panes.ClientStatementPane;
import com.smartgwt.client.widgets.layout.VLayout;

public class ClientStatementView extends ViewImpl implements ClientStatementPresenter.MyView {

	private static final String DEFAULT_MARGIN = "0px";
	private VLayout panel;
	private ClientStatementPane clientStatementPane;

	@Inject
	public ClientStatementView() {

		panel = new VLayout();

		clientStatementPane = new ClientStatementPane();

		panel.setMembers(clientStatementPane);
		panel.setWidth100();
		panel.setHeight("90%");
		Window.enableScrolling(false);
		Window.setMargin(DEFAULT_MARGIN);

	}

	public Widget asWidget() {
		return panel;
	}

	public ClientStatementPane getClientStatementPane() {
		return clientStatementPane;
	}

	 

 

}
