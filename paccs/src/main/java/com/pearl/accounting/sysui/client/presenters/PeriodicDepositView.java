package com.pearl.accounting.sysui.client.presenters;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.pearl.accounting.sysui.client.views.panes.PeriodicDepositPane;
import com.smartgwt.client.widgets.layout.VLayout;

public class PeriodicDepositView extends ViewImpl implements PeriodicDepositPresenter.MyView {

	private static final String DEFAULT_MARGIN = "0px";
	private VLayout panel;
	private PeriodicDepositPane periodicDepositPane;

	@Inject
	public PeriodicDepositView() {

		panel = new VLayout();

		periodicDepositPane = new PeriodicDepositPane();

		panel.setMembers(periodicDepositPane);
		panel.setWidth100();
		panel.setHeight("90%");
		Window.enableScrolling(false);
		Window.setMargin(DEFAULT_MARGIN);

	}

	public Widget asWidget() {
		return panel;
	}

	public PeriodicDepositPane getPeriodicDepositPane() {
		return periodicDepositPane;
	}

}
