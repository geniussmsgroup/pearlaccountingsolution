package com.pearl.accounting.sysui.shared;

import com.gwtplatform.dispatch.shared.UnsecuredActionImpl;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.AssessmentPeriod;
import com.pearl.accounting.sysmodel.AtypeA;
import com.pearl.accounting.sysmodel.CashBookPayment;
import com.pearl.accounting.sysmodel.CashBookReceipt;
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.DoubleEntry;
import com.pearl.accounting.sysmodel.FinancialYear;
import com.pearl.accounting.sysmodel.GeneralLedger;
import com.pearl.accounting.sysmodel.Intervaltb;
import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysmodel.LoanDeposit;
//import com.pearl.accounting.sysmodel.LoanDeposit;
import com.pearl.accounting.sysmodel.LoanRepayment;
import com.pearl.accounting.sysmodel.LoanRepayments;
import com.pearl.accounting.sysmodel.LoanbalanceView;
import com.pearl.accounting.sysmodel.LoaninterestView;
import com.pearl.accounting.sysmodel.Multi;
import com.pearl.accounting.sysmodel.MultiJournal;
import com.pearl.accounting.sysmodel.PeriodicDeposit;
import com.pearl.accounting.sysmodel.ProductSetup;
import com.pearl.accounting.sysmodel.Receiptno;
//import com.pearl.accounting.sysmodel.ProductTest;
import com.pearl.accounting.sysmodel.Salary;
import com.pearl.accounting.sysmodel.SystemUser;
import com.pearl.accounting.sysmodel.Theadp;
import com.pearl.accounting.sysmodel.Totalinterest;
import com.pearl.accounting.sysmodel.TransGL;
import com.pearl.accounting.sysmodel.TransML;
import com.pearl.accounting.sysmodel.UserPermission;
import com.pearl.accounting.sysmodel.UserRole;
import com.pearl.accounting.sysmodel.Withdrawal;
//import com.pearl.accounting.sysmodel.productdetails;

public class SysAction extends UnsecuredActionImpl<SysResult> {

	private String operation;

	private AssessmentPeriod assessmentPeriod;

	private FinancialYear financialYear;

	private UserRole userRole;
	private Intervaltb interval;
	private TransGL transgl;
	private TransML transml;
	private LoanRepayments loanrepayment;
	private Receiptno receiptno;
	private Totalinterest totalinterest;
	private LoanbalanceView loanbalance;
	private LoaninterestView loaninterest;

	private UserPermission userPermission;
	private DoubleEntry doubleentry;

	private SystemUser systemUser;
	private ProductSetup productsetup;
	private Account account;

	private Multi multi;

	private CustomerAccount customerAccount;

	private PeriodicDeposit periodicDeposit;

	private Loan loan;
	private LoanDeposit loandeposit;

	private Withdrawal withdrawal;

	private LoanRepayment loanRepayment;

	private GeneralLedger generalLedger;

	private CashBookPayment cashBookPayment;

	private CashBookReceipt cashBookReceipt;

	private MultiJournal multiJournal;

	private Salary salary;
	private Theadp theadp;
	private AtypeA atypea;
	// private ProductTest productTest;

	public SysAction(String operation, Salary salary) {
		this.operation = operation;
		this.salary = salary;
	}

	public SysAction(String operation, LoanbalanceView loanbalance) {
		this.operation = operation;
		this.loanbalance = loanbalance;
	}

	public SysAction(String operation, LoaninterestView loaninterest) {
		this.operation = operation;
		this.loaninterest = loaninterest;
	}


	

	public SysAction(String operation, Receiptno receiptno) {
		this.operation = operation;
		this.receiptno = receiptno;
	}

	
	public SysAction(String operation, AtypeA atypea) {
		this.operation = operation;
		this.atypea = atypea;
	}
//	public SysAction(String operation, LoanDeposit loanDeposit) {
//		this.operation = operation;
//		this.loanDeposit = loanDeposit;
//	}totalinterest

	public SysAction(String operation, Totalinterest totalinterest) {
		this.operation = operation;
		this.totalinterest = totalinterest;
	}

	public SysAction(String operation, MultiJournal multiJournal) {
		this.operation = operation;
		this.multiJournal = multiJournal;
	}

	public SysAction(String operation, Theadp theadp) {
		this.operation = operation;
		this.theadp = theadp;
	}

	public SysAction(String operation, ProductSetup productsetup) {
		this.operation = operation;
		this.productsetup = productsetup;
	}

	public SysAction(String operation, Intervaltb interval) {
		this.operation = operation;
		this.interval = interval;
	}

	public SysAction(String operation, DoubleEntry doubleentry) {
		this.operation = operation;
		this.doubleentry = doubleentry;
	}

	public SysAction(String operation, LoanRepayments loanrepayment) {
		this.operation = operation;
		this.loanrepayment = loanrepayment;
	}

	public SysAction(String operation, TransGL transgl) {
		this.operation = operation;
		this.transgl = transgl;
	}

	public SysAction(String operation, TransML transml) {
		this.operation = operation;
		this.transml = transml;
	}

	public SysAction(String operation, CashBookReceipt cashBookReceipt) {
		this.operation = operation;
		this.cashBookReceipt = cashBookReceipt;
	}

	public SysAction(String operation, CashBookPayment cashBookPayment) {
		this.operation = operation;
		this.cashBookPayment = cashBookPayment;
	}

	public SysAction(String operation, GeneralLedger generalLedger) {
		this.operation = operation;
		this.generalLedger = generalLedger;
	}

	public SysAction(String operation, Multi multi) {
		this.operation = operation;
		this.multi = multi;
	}

	public SysAction(String operation, LoanRepayment loanRepayment) {
		this.operation = operation;
		this.loanRepayment = loanRepayment;
	}

	public SysAction(String operation, Withdrawal withdrawal) {
		this.operation = operation;
		this.withdrawal = withdrawal;
	}

	public SysAction(String operation, Loan loan) {
		this.operation = operation;
		this.loan = loan;
	}

	public SysAction(String operation, LoanDeposit loandeposit) {
		this.operation = operation;
		this.loandeposit = loandeposit;
	}

	public SysAction(String operation, PeriodicDeposit periodicDeposit) {
		this.operation = operation;
		this.periodicDeposit = periodicDeposit;
	}

	public SysAction(String operation, CustomerAccount customerAccount) {
		this.operation = operation;
		this.customerAccount = customerAccount;
	}

	public SysAction(String operation, Account account) {
		this.operation = operation;
		this.account = account;
	}

	public SysAction(String operation, SystemUser systemUser) {
		this.operation = operation;
		this.systemUser = systemUser;
	}

	public SysAction(String operation, UserPermission userPermission) {
		this.operation = operation;
		this.userPermission = userPermission;
	}

	public SysAction(String operation, UserRole userRole) {
		this.operation = operation;
		this.userRole = userRole;
	}

//public SysAction(String operation, productdetails Productdetails) {
//	this.operation = operation;
//	this.Productdetails = Productdetails;
//}
	public SysAction(String operation, FinancialYear financialYear) {
		this.operation = operation;
		this.financialYear = financialYear;
	}

	public SysAction(String operation, AssessmentPeriod assessmentPeriod) {
		this.operation = operation;
		this.assessmentPeriod = assessmentPeriod;
	}

//	public SysAction(String operation, ProductTest productTest) {
//		this.operation = operation;
//		this.productTest = productTest;
//	}

	public SysAction() {

	}

	public SysAction(String operation) {
		this.operation = operation;
	}

	public String getOperation() {
		return operation;
	}

	public AssessmentPeriod getAssessmentPeriod() {
		return assessmentPeriod;
	}

	public TransML getTransML() {
		return transml;
	}

	public AtypeA getAtypeA() {
		return atypea;
	}

	public FinancialYear getFinancialYear() {
		return financialYear;
	}

	public LoanDeposit getLoandeposit() {
		return loandeposit;
	}

	public Multi getMulti() {
		return multi;
	}

	public LoanRepayments getLoanrepayment() {
		return loanrepayment;
	}

	public DoubleEntry getDoubleEntry() {
		return doubleentry;
	}

	public TransGL getTransGL() {
		return transgl;
	}

	public Totalinterest getTotalinterest() {
		return totalinterest;
	}

	public ProductSetup getProductSetup() {
		return productsetup;
	}

	public UserRole getUserRole() {
		return userRole;
	}

	public UserPermission getUserPermission() {
		return userPermission;
	}

	public SystemUser getSystemUser() {
		return systemUser;
	}

	public Account getAccount() {
		return account;
	}

	public Intervaltb getIntervaltb() {
		return interval;
	}

	public CustomerAccount getCustomerAccount() {
		return customerAccount;
	}

	public PeriodicDeposit getPeriodicDeposit() {
		return periodicDeposit;
	}

	public Loan getLoan() {
		return loan;
	}

	public Withdrawal getWithdrawal() {
		return withdrawal;
	}

	public Theadp getTheadp() {
		return theadp;
	}

	public LoanRepayment getLoanRepayment() {
		return loanRepayment;
	}

	public GeneralLedger getGeneralLedger() {
		return generalLedger;
	}

//	public LoanDeposit getLoanDeposit() {
//		return loanDeposit;
//	}

	public LoanbalanceView getLoanbalance() {
		return loanbalance;
	}

	public LoaninterestView getLoaninterest() {
		return loaninterest;
	}

	public CashBookPayment getCashBookPayment() {
		return cashBookPayment;
	}

	public CashBookReceipt getCashBookReceipt() {
		return cashBookReceipt;
	}

	public MultiJournal getMultiJournal() {
		return multiJournal;
	}

	public Salary getSalary() {
		return salary;
	}

	public Receiptno getReceiptno() {
		return receiptno;
	}

}
