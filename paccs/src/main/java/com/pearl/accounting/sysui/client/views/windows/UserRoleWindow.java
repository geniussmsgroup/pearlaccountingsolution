package com.pearl.accounting.sysui.client.views.windows;

import com.pearl.accounting.sysui.client.views.widgets.TextField;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout; 

public class UserRoleWindow extends Window {

	private TextField roleCode;
	private TextField roleName;
	private TextField description;

	private IButton saveButton;

	public UserRoleWindow() {
		super();
		roleCode = new TextField();
		roleCode.setTitle("Code");

		roleName = new TextField();
		roleName.setTitle("Role");

		description = new TextField();
		description.setTitle("Description");

		saveButton = new IButton("Save");

		DynamicForm form = new DynamicForm();
		form.setFields(roleCode, roleName, description);
		form.setWrapItemTitles(false);
		form.setMargin(10);

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(saveButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(form);
		layout.addMember(buttonLayout);

		layout.setMargin(10);
		this.addItem(layout);
		this.setWidth("40%");
		this.setHeight("28%");
		this.setAutoCenter(true);
		this.setTitle("Roles");
		this.setIsModal(true);
		this.setShowModalMask(true);
	}

	public TextField getRoleCode() {
		return roleCode;
	}

	public TextField getRoleName() {
		return roleName;
	}

	public TextField getDescription() {
		return description;
	}

	public IButton getSaveButton() {
		return saveButton;
	}

}
