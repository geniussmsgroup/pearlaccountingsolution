package com.pearl.accounting.sysui.client.views.windows;

import java.util.List;

import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysui.client.views.listgrids.CashBookReceiptDetailListgrid;
import com.pearl.accounting.sysui.client.views.listgrids.MultiDetailListgrid;
import com.pearl.accounting.sysui.client.views.listgrids.MultiListgrid;
import com.pearl.accounting.sysui.client.views.widgets.ActivePeriod;
import com.pearl.accounting.sysui.client.views.widgets.ComboBox;
import com.pearl.accounting.sysui.client.views.widgets.TextField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class MultiWindow extends Window {

	private TextField payee;
	private TextField paidAmount;
	private ComboBox assessmentPeriod;
	private TextField periodcode;
	private ComboBox financialYear;
	private DateItem paymentDate;
	private ComboBox receivingAccount;
	private TextField paymentDetails;
	private TextField remarks;
	private static DateItem from;
	private static DateItem to;
	private MultiDetailListgrid multiListgrid;

	private IButton saveButton;
	private IButton addButton;
	private IButton delete;
	private ActivePeriod acti;

	public MultiWindow(List<Account> accounts, List<CustomerAccount> custacct, List<Loan> loanaccts) {
		super();

		financialYear = new ComboBox();
		financialYear.setTitle("Year");
		periodcode = new TextField();
		periodcode.setTitle("periodcode");
		periodcode.setWidth("periodcode");
		acti = new ActivePeriod();

		assessmentPeriod = new ComboBox();
		assessmentPeriod.setTitle("Selected Period");

		payee = new TextField();
		payee.setTitle("From");

		paidAmount = new TextField();
		paidAmount.setTitle("Amount");
		paidAmount.setKeyPressFilter("[0-9.]");

		paymentDate = new DateItem();
		paymentDate.setTitle("Payment Date");
		paymentDate.setUseTextField(true);
		paymentDate.setWidth("*");
		paymentDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		receivingAccount = new ComboBox();
		receivingAccount.setTitle("Receiving A/C");

		paymentDetails = new TextField();
		paymentDetails.setTitle("Receipt No.");

		remarks = new TextField();
		remarks.setTitle("Remarks");

		from = new DateItem();
		from.setTitle("From");
		from.setUseTextField(true);
		from.setWidth("*");
		from.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		to = new DateItem();
		to.setTitle("To");
		to.setUseTextField(true);
		to.setWidth("*");
		to.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		saveButton = new IButton("Save");
		addButton = new IButton("Add");
		delete = new IButton("delete");

		multiListgrid = new MultiDetailListgrid(accounts, custacct, loanaccts);

		DynamicForm form = new DynamicForm();
		form.setFields(assessmentPeriod, periodcode, financialYear, from, to);
		form.setWrapItemTitles(true);
		form.setMargin(10);
		form.setNumCols(8);

		DynamicForm form2 = new DynamicForm();
		form2.setFields(paidAmount, paymentDate, paymentDetails, remarks);
		form2.setWrapItemTitles(true);
		form2.setMargin(10);
		form2.setNumCols(4);

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(saveButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(4);

		Label subheader = new Label();
		subheader.setStyleName("crm-ContextArea-Header-Label");
		subheader.setContents("Accounts details");
		subheader.setWidth("1%");
		subheader.setAutoHeight();
		// subheader.setMargin(10);
		subheader.setAlign(Alignment.LEFT);

		HLayout subbuttonLayout = new HLayout();
		subbuttonLayout.setMembers(addButton, delete, subheader);
		subbuttonLayout.setAutoHeight();
		subbuttonLayout.setWidth100();
		subbuttonLayout.setMargin(5);
		subbuttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(form);
		layout.addMember(form2);
		layout.addMember(subbuttonLayout);
		layout.addMember(multiListgrid);
		layout.addMember(buttonLayout);
		layout.setMargin(10);

		this.addItem(layout);
		this.setWidth("70%");
		this.setHeight("70%");
		this.setAutoCenter(true);
		this.setTitle("Multi-Journal definition");
		this.setIsModal(true);
		this.setShowModalMask(true);
	}

	public TextField getPayee() {
		return payee;
	}

	public TextField getPaidAmount() {
		return paidAmount;
	}

	public TextField getPeriodcode() {
		return periodcode;
	}

	public ComboBox getAssessmentPeriod() {
		return assessmentPeriod;
	}

	public ComboBox getFinancialYear() {
		return financialYear;
	}

	public DateItem getPaymentDate() {
		return paymentDate;
	}

	public ComboBox getReceivingAccount() {
		return receivingAccount;
	}

	public TextField getPaymentDetails() {
		return paymentDetails;
	}

	public TextField getRemarks() {
		return remarks;
	}

	public MultiDetailListgrid getMultiDetailListgrid() {
		return multiListgrid;
	}

	public IButton getSaveButton() {
		return saveButton;
	}

	public IButton getAddButton() {
		return addButton;
	}

	public IButton getDeleteButton() {
		return delete;
	}

	public ActivePeriod getActi() {
		return acti;
	}

	public void setActi(ActivePeriod acti) {
		this.acti = acti;
	}

	public static DateItem getFrom() {
		return from;
	}

	public static void setFrom(DateItem from) {
		MultiWindow.from = from;
	}

	public static DateItem getTo() {
		return to;
	}

	public static void setTo(DateItem to) {
		MultiWindow.to = to;
	}

	public void setDelete(IButton delete) {
		this.delete = delete;
	}

}
