package com.pearl.accounting.sysui.client.presenters;

import java.util.Date;
import java.util.LinkedHashMap;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.shared.DispatchAsync;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.pearl.accounting.sysmodel.AssessmentPeriod;
import com.pearl.accounting.sysmodel.FinancialYear;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysui.client.place.NameTokens;
import com.pearl.accounting.sysui.client.utils.PAConstant;
import com.pearl.accounting.sysui.client.views.listgrids.AssessmentPeriodListgrid;
import com.pearl.accounting.sysui.client.views.panes.AssessmentPeriodPane;
import com.pearl.accounting.sysui.client.views.widgets.SwizimaLoader;
import com.pearl.accounting.sysui.client.views.windows.AssessmentPeriodWindow;
import com.pearl.accounting.sysui.shared.SysAction;
import com.pearl.accounting.sysui.shared.SysResult;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class AssessmentPeriodPresenter
		extends Presenter<AssessmentPeriodPresenter.MyView, AssessmentPeriodPresenter.MyProxy> {

	private final PlaceManager placeManager;
	private final DispatchAsync dispatcher;

	public interface MyView extends View {

		public AssessmentPeriodPane getAssessmentPeriodPane();

	}

	@ProxyCodeSplit
	@NameToken(NameTokens.periods)
	public interface MyProxy extends ProxyPlace<AssessmentPeriodPresenter> {
	}

	@Inject
	public AssessmentPeriodPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
			final DispatchAsync dispatcher, final PlaceManager placeManager) {
		super(eventBus, view, proxy);

		this.dispatcher = dispatcher;
		this.placeManager = placeManager;
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this, MainPagePresenter.TYPE_SetContextAreaContent, this);
	}

	@Override
	protected void onBind() {
		super.onBind();

		onNewButtonClicked();
		loadPeriods();
		onLoadButtonClicked();
		onLoadButtonClicked();
		onDeleteButtonClicked();

		onEditButtonClicked();

		onActivateButtonClicked();

	}

	private void onLoadButtonClicked() {
		getView().getAssessmentPeriodPane().getLoadButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadPeriods();

			}
		});

		getView().getAssessmentPeriodPane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadPeriods();

			}
		});
	}

	private void onNewButtonClicked() {
		getView().getAssessmentPeriodPane().getAddButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				createAssesmentPeriod();

			}
		});

		getView().getAssessmentPeriodPane().getControlsPane().getNewAssetButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				createAssesmentPeriod();

			}
		});
	}

	private void createAssesmentPeriod() {
		AssessmentPeriodWindow window = new AssessmentPeriodWindow();
		onSaveButtonClicked(window);
		// loadFinancialYears(window);
		window.show();
	}

	private void loadFinancialYears(final AssessmentPeriodWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_FinancialYear), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

					for (FinancialYear financialYear : result.getFinancialYears()) {

						valueMap.put(financialYear.getId(), financialYear.getFinancialYear());
					}

					window.getFinancialYear().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private boolean FormValid(final AssessmentPeriodWindow window) {
		if (window.getPeriodName().getValueAsString() != null) {
			if (window.getPeriodDescription().getValueAsString() != null) {
				if (window.getPeriodCode().getValueAsString() != null) {
					return true;
				} else {
					SC.warn("ERROR", "Please enter period code");
				}
			} else {
				SC.warn("ERROR", "Please enter Period description");

			}
		} else {
			SC.warn("ERROR", "Please enter Period name");

		}
		return false;
	}

	private void onSaveButtonClicked(final AssessmentPeriodWindow window) {
		window.getSaveButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				if (FormValid(window)) {
					AssessmentPeriod assessmentPeriod = new AssessmentPeriod();

					assessmentPeriod.setCode(window.getPeriodCode().getValueAsString());
					assessmentPeriod.setDateCreated(new Date());
					assessmentPeriod.setDateUpdated(new Date());
					assessmentPeriod.setDescription(window.getPeriodDescription().getValueAsString());
					assessmentPeriod.setEndDate(window.getEndDate().getValueAsDate());

					// FinancialYear financialYear = new FinancialYear();
					// financialYear.setId(window.getFinancialYear().getValueAsString());

					// assessmentPeriod.setFinancialYear(financialYear);
					assessmentPeriod.setPeriodName(window.getPeriodName().getValueAsString());
					assessmentPeriod.setStartDate(window.getStartDate().getValueAsDate());
					assessmentPeriod.setStatus(Status.ACTIVE);

					assessmentPeriod.setActivationStatus(Status.IN_ACTIVE);

					SC.showPrompt("", "", new SwizimaLoader());

					dispatcher.execute(new SysAction(PAConstant.SAVE_AssessmentPeriod, assessmentPeriod),
							new AsyncCallback<SysResult>() {
								public void onFailure(Throwable caught) {
									System.out.println(caught.getMessage());
								}

								public void onSuccess(SysResult result) {

									SC.clearPrompt();

									if (result != null) {

										if (result.getSwizFeedback().isResponse()) {

											SC.say("SUCCESS", result.getSwizFeedback().getMessage());
											ClearForm(window);
											getView().getAssessmentPeriodPane().getAssessmentPeriodListgrid()
													.addRecordsToGrid(result.getAssessmentPeriods());
										} else {
											SC.warn("ERROR", result.getSwizFeedback().getMessage());
										}

									} else {
										SC.say("ERROR", "Unknow error");
									}

								}
							});

				}
			}
		});
	}

	private void ClearForm(final AssessmentPeriodWindow window) {
		window.getPeriodCode().clearValue();
		window.getPeriodDescription().clearValue();
		window.getPeriodName().clearValue();
	}

	private void onEditButtonClicked() {
		getView().getAssessmentPeriodPane().getEditButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				edit();

			}
		});

		getView().getAssessmentPeriodPane().getControlsPane().getEditButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				edit();

			}
		});
	}

	private void edit() {
		if (getView().getAssessmentPeriodPane().getAssessmentPeriodListgrid().anySelected()) {
			AssessmentPeriodWindow window = new AssessmentPeriodWindow();
			onUpdateButtonClicked(window);
			loadRecordToEdit(window);
			window.setTitle("Update assessment period");
			window.show();
		} else {
			SC.warn("ERROR", "Please select record to edit");
		}

	}

	private void loadRecordToEdit(final AssessmentPeriodWindow window) {
		ListGridRecord record = getView().getAssessmentPeriodPane().getAssessmentPeriodListgrid().getSelectedRecord();

		window.getPeriodCode().setValue(record.getAttribute(AssessmentPeriodListgrid.CODE));
		window.getPeriodName().setValue(record.getAttribute(AssessmentPeriodListgrid.NAME));
		window.getPeriodDescription().setValue(record.getAttribute(AssessmentPeriodListgrid.DESCRIPTION));

		if (record.getAttribute(AssessmentPeriodListgrid.END_DATE) != null) {
			window.getEndDate().setValue(new Date(record.getAttribute(AssessmentPeriodListgrid.END_DATE)));
		}
		if (record.getAttribute(AssessmentPeriodListgrid.START_DATE) != null) {
			window.getStartDate().setValue(new Date(record.getAttribute(AssessmentPeriodListgrid.START_DATE)));
		}

	}

	private void onUpdateButtonClicked(final AssessmentPeriodWindow window) {
		window.getSaveButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				ListGridRecord record = getView().getAssessmentPeriodPane().getAssessmentPeriodListgrid()
						.getSelectedRecord();

				AssessmentPeriod assessmentPeriod = new AssessmentPeriod();

				assessmentPeriod.setId(record.getAttribute(AssessmentPeriodListgrid.ID));

				assessmentPeriod.setCode(window.getPeriodCode().getValueAsString());
				assessmentPeriod.setDateCreated(new Date());
				assessmentPeriod.setDateUpdated(new Date());
				assessmentPeriod.setDescription(window.getPeriodDescription().getValueAsString());
				assessmentPeriod.setEndDate(window.getEndDate().getValueAsDate());

				// FinancialYear financialYear = new FinancialYear();
				// financialYear.setId(window.getFinancialYear().getValueAsString());

				// assessmentPeriod.setFinancialYear(financialYear);
				assessmentPeriod.setPeriodName(window.getPeriodName().getValueAsString());
				assessmentPeriod.setStartDate(window.getStartDate().getValueAsDate());
				assessmentPeriod.setStatus(Status.ACTIVE);

				assessmentPeriod.setActivationStatus(Status.IN_ACTIVE);

				SC.showPrompt("", "", new SwizimaLoader());

				dispatcher.execute(new SysAction(PAConstant.EDIT_AssessmentPeriod, assessmentPeriod),
						new AsyncCallback<SysResult>() {
							public void onFailure(Throwable caught) {
								System.out.println(caught.getMessage());
							}

							public void onSuccess(SysResult result) {

								SC.clearPrompt();

								if (result != null) {
									if (result.getSwizFeedback().isResponse()) {

										SC.say("SUCCESS", result.getSwizFeedback().getMessage());

										getView().getAssessmentPeriodPane().getAssessmentPeriodListgrid()
												.addRecordsToGrid(result.getAssessmentPeriods());
										window.close();
									} else {
										SC.warn("ERROR", result.getSwizFeedback().getMessage());
									}

								} else {
									SC.say("ERROR", "Unknow error");
								}

							}
						});

			}
		});
	}

	private void loadPeriods() {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_AssessmentPeriod), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					getView().getAssessmentPeriodPane().getAssessmentPeriodListgrid()
							.addRecordsToGrid(result.getAssessmentPeriods());

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

	}

	private void onDeleteButtonClicked() {
		getView().getAssessmentPeriodPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				delete();

			}
		});

		getView().getAssessmentPeriodPane().getControlsPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				delete();

			}
		});
	}

	private void delete() {

		if (getView().getAssessmentPeriodPane().getAssessmentPeriodListgrid().anySelected()) {
			SC.ask("Confirm", "Are you sure you want to delete the selected record?", new BooleanCallback() {

				public void execute(Boolean value) {

					if (value) {

						AssessmentPeriod assessmentPeriod = new AssessmentPeriod();

						assessmentPeriod.setId(getView().getAssessmentPeriodPane().getAssessmentPeriodListgrid()
								.getSelectedRecord().getAttribute(AssessmentPeriodListgrid.ID));

						SC.showPrompt("", "", new SwizimaLoader());

						dispatcher.execute(new SysAction(PAConstant.DELETE_AssessmentPeriod, assessmentPeriod),
								new AsyncCallback<SysResult>() {
									public void onFailure(Throwable caught) {
										System.out.println(caught.getMessage());
									}

									public void onSuccess(SysResult result) {

										SC.clearPrompt();

										if (result != null) {
											if (result.getSwizFeedback().isResponse()) {

												SC.say("SUCCESS", result.getSwizFeedback().getMessage());

												getView().getAssessmentPeriodPane().getAssessmentPeriodListgrid()
														.addRecordsToGrid(result.getAssessmentPeriods());

											} else {
												SC.warn("ERROR", result.getSwizFeedback().getMessage());
											}

										} else {
											SC.say("ERROR", "Unknow error");
										}

									}
								});
					}

				}
			});
		} else {
			SC.warn("ERROR", "ERROR: Please select record to delete");
		}

	}

	private void onActivateButtonClicked() {
		getView().getAssessmentPeriodPane().getActivateButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				activate();

			}
		});

		getView().getAssessmentPeriodPane().getDeactivaeButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				deactivate();

			}
		});
	}

	private void activate() {

		if (getView().getAssessmentPeriodPane().getAssessmentPeriodListgrid().anySelected()) {
			SC.ask("Confirm", "Are you sure you want to activate the selected record?", new BooleanCallback() {

				public void execute(Boolean value) {

					if (value) {

						AssessmentPeriod assessmentPeriod = new AssessmentPeriod();

						assessmentPeriod.setId(getView().getAssessmentPeriodPane().getAssessmentPeriodListgrid()
								.getSelectedRecord().getAttribute(AssessmentPeriodListgrid.ID));

						SC.showPrompt("", "", new SwizimaLoader());

						dispatcher.execute(new SysAction(PAConstant.ACTIVATE_AssessmentPeriod, assessmentPeriod),
								new AsyncCallback<SysResult>() {
									public void onFailure(Throwable caught) {
										System.out.println(caught.getMessage());
									}

									public void onSuccess(SysResult result) {

										SC.clearPrompt();

										if (result != null) {
											if (result.getSwizFeedback().isResponse()) {

												SC.say("SUCCESS", result.getSwizFeedback().getMessage());

												getView().getAssessmentPeriodPane().getAssessmentPeriodListgrid()
														.addRecordsToGrid(result.getAssessmentPeriods());

											} else {
												SC.warn("ERROR", result.getSwizFeedback().getMessage());
											}

										} else {
											SC.say("ERROR", "Unknow error");
										}

									}
								});
					}

				}
			});
		} else {
			SC.warn("ERROR", "ERROR: Please select record to delete");
		}

	}

	private void deactivate() {

		if (getView().getAssessmentPeriodPane().getAssessmentPeriodListgrid().anySelected()) {
			SC.ask("Confirm", "Are you sure you want to deactivate the selected record?", new BooleanCallback() {

				public void execute(Boolean value) {

					if (value) {

						AssessmentPeriod assessmentPeriod = new AssessmentPeriod();

						assessmentPeriod.setId(getView().getAssessmentPeriodPane().getAssessmentPeriodListgrid()
								.getSelectedRecord().getAttribute(AssessmentPeriodListgrid.ID));

						SC.showPrompt("", "", new SwizimaLoader());

						dispatcher.execute(new SysAction(PAConstant.DEACTIVATE_AssessmentPeriod, assessmentPeriod),
								new AsyncCallback<SysResult>() {
									public void onFailure(Throwable caught) {
										System.out.println(caught.getMessage());
									}

									public void onSuccess(SysResult result) {

										SC.clearPrompt();

										if (result != null) {

											if (result.getSwizFeedback().isResponse()) {

												SC.say("SUCCESS", result.getSwizFeedback().getMessage());

												getView().getAssessmentPeriodPane().getAssessmentPeriodListgrid()
														.addRecordsToGrid(result.getAssessmentPeriods());

											} else {
												SC.warn("ERROR", result.getSwizFeedback().getMessage());
											}

										} else {
											SC.say("ERROR", "Unknow error");
										}

									}
								});
					}

				}
			});
		} else {
			SC.warn("ERROR", "ERROR: Please select record to delete");
		}

	}

}
