package com.pearl.accounting.sysui.client.views.windows;

import com.pearl.accounting.sysui.client.views.widgets.TextField;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class FinancialYearWindow extends Window {

	private TextField fyearCode;
	private TextField fyearName;
	private DateItem startDate;
	private DateItem endDate;

	private IButton saveButton;

	public FinancialYearWindow() {
		super();
		fyearCode = new TextField();
		fyearCode.setTitle("Code");

		fyearName = new TextField();
		fyearName.setTitle("Financial year");

		startDate = new DateItem();
		startDate.setTitle("Start date");
		startDate.setUseTextField(true);
		startDate.setWidth("*");	
		startDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		

		endDate = new DateItem();
		endDate.setTitle("End date");
		endDate.setUseTextField(true);
		endDate.setWidth("*");
		endDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);


		saveButton = new IButton("Save");

		DynamicForm form = new DynamicForm();
		form.setFields(fyearCode, fyearName, startDate, endDate);
		form.setWrapItemTitles(false);
		form.setMargin(10);

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(saveButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(form);
		layout.addMember(buttonLayout);

		layout.setMargin(10);
		this.addItem(layout);
		this.setWidth("30%");
		this.setHeight("35%");
		this.setAutoCenter(true);
		this.setTitle("Create new financial year");
		this.setIsModal(true);
		this.setShowModalMask(true);
	}

	public TextField getFyearCode() {
		return fyearCode;
	}

	public TextField getFyearName() {
		return fyearName;
	}

	public DateItem getStartDate() {
		return startDate;
	}

	public DateItem getEndDate() {
		return endDate;
	}

	public IButton getSaveButton() {
		return saveButton;
	}

}
