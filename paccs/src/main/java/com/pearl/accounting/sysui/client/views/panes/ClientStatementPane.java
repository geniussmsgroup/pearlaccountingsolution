package com.pearl.accounting.sysui.client.views.panes;

import com.pearl.accounting.sysui.client.views.listgrids.ClientStatementListgrid;
import com.pearl.accounting.sysui.client.views.widgets.ControlsPane;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class ClientStatementPane extends VLayout {

	private ControlsPane controlsPane;

	private ClientStatementListgrid clientStatementListgrid;

	private IButton loadButton;

	public ClientStatementPane() {

		super();
		Label header = new Label();
		header.setStyleName("crm-ContextArea-Header-Label");
		header.setContents("Customer statements");
		header.setWidth("100%");
		header.setAutoHeight();
		header.setMargin(10);
		header.setAlign(Alignment.LEFT);

		controlsPane = new ControlsPane();

		clientStatementListgrid = new ClientStatementListgrid();

		loadButton = new IButton("Load");

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(loadButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(10);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(controlsPane);
		layout.addMember(header);
		layout.addMember(clientStatementListgrid);
		layout.addMember(buttonLayout);
		this.addMember(layout);

	}

	public ControlsPane getControlsPane() {
		return controlsPane;
	}

	public ClientStatementListgrid getClientStatementListgrid() {
		return clientStatementListgrid;
	}

	public IButton getLoadButton() {
		return loadButton;
	}

}
