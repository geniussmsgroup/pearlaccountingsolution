package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.LoaninterestView;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class LoanRateListgrid extends SuperListGrid {
	public static String ID = "id";
	public static String MemberId = "code";
	public static String LoanID = "accountName";

	public static String InterestRate = "accountType";

	public LoanRateListgrid() {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField member = new ListGridField(MemberId, "member code");
		ListGridField loan = new ListGridField(LoanID, "loan id ");

		ListGridField interest = new ListGridField(InterestRate, "interest Rate ");

		this.setFields(id, member, loan, interest);

	}

	public ListGridRecord addRowData(LoaninterestView account) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute(ID, account.getMemberId());
		record.setAttribute(MemberId, account.getMemberId());
		record.setAttribute(LoanID, account.getLoanID());

		record.setAttribute(InterestRate, account.getInterestRate());

		return record;
	}

	public void addRecordsToGrid(List<LoaninterestView> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (LoaninterestView item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
	}

}
