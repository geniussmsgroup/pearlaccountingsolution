package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.LinkedHashMap;
import java.util.List;

import com.google.gwt.i18n.client.NumberFormat;
import com.pearl.accounting.sysmodel.MultiJournalDetail;
import com.pearl.accounting.sysui.client.utils.PAManager;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;

public class MultiJournalDetailListgrid extends SuperListGrid {
	public static String ID = "id";

	public static String AmountPaid = "amountPaid";
	public static String PaymentDate = "paymentDate";

	public static String AccountId = "payingAccountId";
	public static String Account = "payingAccount";

	public static String CustomerAccountID = "customerAccountId";
	public static String CustomerAccount = "AccountName";
	public static String LoanID = "LoanID";
	public static String Loan = "Loan";
	public static String Trackpdata = "trackpdata";

	public static String Remarks = "Remarks";

	public static String TransactionType = "transactionType";

	NumberFormat nf = NumberFormat.getFormat("#,##0.00");

	public MultiJournalDetailListgrid(List<com.pearl.accounting.sysmodel.Account> accounts,
			List<com.pearl.accounting.sysmodel.CustomerAccount> custacct,
			List<com.pearl.accounting.sysmodel.Loan> Lnacct) {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField amountPaid = new ListGridField(AmountPaid, "Amount");
		amountPaid.setCanEdit(true);

		amountPaid.setShowGridSummary(true);

		amountPaid.setSummaryFunction(new SummaryFunction() {
			public Object getSummaryValue(Record[] records, ListGridField field) {

				long totalAmount = 0;
				for (Record record : records) {

					totalAmount += Float
							.parseFloat(PAManager.getInstance().unformatCash(record.getAttribute(AmountPaid)));

				}
				return "" + PAManager.getInstance().formatCash(totalAmount);
			}
		});

		ListGridField accountId = new ListGridField(AccountId, "Account Id");
		accountId.setHidden(true);

		ListGridField account = new ListGridField(Account, "Account");

		LinkedHashMap<String, String> valueMap1 = new LinkedHashMap<String, String>();

		for (com.pearl.accounting.sysmodel.Account acc : accounts) {
			valueMap1.put(acc.getSysid(), acc.getAccountName());
		}
		account.setValueMap(valueMap1);
		account.setCanEdit(true);

		ListGridField paymentDate = new ListGridField(PaymentDate, "Date");
		paymentDate.setHidden(true);

		ListGridField remarks = new ListGridField(Remarks, "Remarks");
		remarks.setCanEdit(true);

		ListGridField transactionType = new ListGridField(TransactionType, "Trans. Type");

		LinkedHashMap<String, String> valueMap2 = new LinkedHashMap<String, String>();
		for (com.pearl.accounting.sysmodel.Ttype type : com.pearl.accounting.sysmodel.Ttype
				.values()) {
			valueMap2.put(type.getTransactionType(), type.getTransactionType());

		}

		transactionType.setValueMap(valueMap2);
		transactionType.setCanEdit(true);

		ListGridField customerAccountId = new ListGridField(CustomerAccountID, "Customer No.");
		customerAccountId.setHidden(true);

		ListGridField customerAccount = new ListGridField(CustomerAccount, "Customer");

		LinkedHashMap<String, String> valueMap3 = new LinkedHashMap<String, String>();
		valueMap3.put("None", "None");
		for (com.pearl.accounting.sysmodel.CustomerAccount cacc : custacct) {
			valueMap3.put(cacc.getSysid(), cacc.getSurname() + " " + cacc.getOtherNames());

		}
		String shows = "None";
		
		
      
        customerAccount.setDefaultFilterValue(shows);
		customerAccount.setValueMap(valueMap3);
		customerAccount.setCanEdit(true);

		ListGridField trackpdata = new ListGridField(Trackpdata, "track account");

		LinkedHashMap<String, String> valueMap9 = new LinkedHashMap<String, String>();
		valueMap9.put("No", "No");
		valueMap9.put("Yes", "Yes");
		trackpdata.setValueMap(valueMap9);
		trackpdata.setCanEdit(true);


		ListGridField loan = new ListGridField(Loan, "Loan");
		ListGridField loanid = new ListGridField(LoanID, "loan id");
		loanid.setHidden(true);

		LinkedHashMap<String, String> valueMap4 = new LinkedHashMap<String, String>();
		valueMap4.put("None", "None");
		for (com.pearl.accounting.sysmodel.Loan lon : Lnacct) {
			valueMap4.put(lon.getLOANID(), lon.getLOANID());

		}
		//String shows = "None";
		
		
	      
        loan.setDefaultFilterValue(shows);
		loan.setValueMap(valueMap4);
		loan.setCanEdit(true);

		this.setFields(id, accountId, account,trackpdata, amountPaid, customerAccountId, customerAccount, loan, loanid,
				transactionType, paymentDate, remarks);

		this.setCanExpandRecords(true);
		this.setShowGridSummary(true);

	}
	

	public MultiJournalDetailListgrid() {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField amountPaid = new ListGridField(AmountPaid, "Amount");

		amountPaid.setShowGridSummary(true);

		amountPaid.setSummaryFunction(new SummaryFunction() {
			public Object getSummaryValue(Record[] records, ListGridField field) {

				long totalAmount = 0;
				for (Record record : records) {

					totalAmount += Float
							.parseFloat(PAManager.getInstance().unformatCash(record.getAttribute(AmountPaid)));

				}
				return "" + PAManager.getInstance().formatCash(totalAmount);
			}
		});

		ListGridField accountId = new ListGridField(AccountId, "AccountId");
		accountId.setHidden(true);

		ListGridField account = new ListGridField(Account, "Account");

		ListGridField paymentDate = new ListGridField(PaymentDate, "Date");

		ListGridField remarks = new ListGridField(Remarks, "Remarks");

		ListGridField transactionType = new ListGridField(TransactionType, "Trans. Type");

		ListGridField customerAccountId = new ListGridField(CustomerAccountID, "Customer No.");
		customerAccountId.setHidden(true);

		ListGridField customerAccount = new ListGridField(CustomerAccount, "Customer");
		ListGridField trackpdata  =new ListGridField(Trackpdata,"trackpersonal");
		ListGridField loanid = new ListGridField(LoanID, "loan id");
		loanid.setHidden(true);

		ListGridField loan = new ListGridField(Loan, "loan ");

		this.setFields(id, accountId, account,trackpdata ,amountPaid, customerAccountId, customerAccount, loan, loanid,
				transactionType, paymentDate, remarks);

		this.setCanExpandRecords(true);
		this.setShowGridSummary(true);

	}

	public ListGridRecord addRowData(MultiJournalDetail payment) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute(ID, payment.getId());

		record.setAttribute(AmountPaid, nf.format(payment.getAmountPosted()));

		if (payment.getTransactionAccount() != null) {
			record.setAttribute(AccountId, "NULL");
			record.setAttribute(Account, payment.getTransactionAccount().getACODE());
		}

		if (payment.getCustomerAccount() != null) {
			record.setAttribute(CustomerAccount, payment.getCustomerAccount().getClientCode());
		}
			///record.setAttribute(CustomerAccount,"NULL");}
		if(payment.getLoan()!=null) {
			record.setAttribute(LoanID, "NULL");
			
			record.setAttribute(Loan, payment.getLoan().getLOANID());
		}

		record.setAttribute(PaymentDate, payment.getTransactionDate());

		record.setAttribute(Remarks, payment.getRemarks());

		if (payment.getTtype() != null) {
			record.setAttribute(TransactionType, payment.getTtype().getTransactionType());
		}

		return record;
	}

	public void addRecordsToGrid(List<MultiJournalDetail> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (MultiJournalDetail item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
	}

	public ListGridRecord editRowData(MultiJournalDetail payment) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute(ID, payment.getId());

		record.setAttribute(AmountPaid, nf.format(payment.getAmountPosted()));
		
		

		if (payment.getTransactionAccount() != null) {
			record.setAttribute(AccountId, "NULL");
			record.setAttribute(Account, "NULL");
		}

		if (payment.getCustomerAccount() != null) {
			record.setAttribute(CustomerAccountID, "NULL");
			record.setAttribute(CustomerAccount, "NULL");
		}
		
		if(payment.getLoan()!=null) {
			record.setAttribute(LoanID, "NULL");
			
			record.setAttribute(Loan, "NULL");
		}
//

		record.setAttribute(PaymentDate, payment.getTransactionDate());

		record.setAttribute(Remarks, payment.getRemarks());

		if (payment.getTtype() != null) {
			record.setAttribute(TransactionType, payment.getTtype().getTransactionType());
		}
		
		

		return record;
	}

	public void addRecordToGrid(MultiJournalDetail item) {
		ListGridRecord record = editRowData(item);
		this.addData(record);
	}

}
