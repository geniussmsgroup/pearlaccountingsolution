package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.google.gwt.i18n.client.NumberFormat;
import com.pearl.accounting.sysmodel.Withdrawal;
import com.pearl.accounting.sysui.client.utils.PAManager;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;

public class WithdrawalListgrid extends SuperListGrid {

	public static String ID = "id";

	public static String CustomerAccountId = "customerAccountId";
	public static String CustomerAccountNo = "customerAccountNo";
	public static String CustomerAccount = "customerAccount";

	public static String WithdrawalAmount = "withdrawalAmount";
	public static String WithdrawalCharge = "withdrawalCharge";
	public static String WithdrawalDate = "withdrawalDate";

	public static String FinancialYear = "financialYear";
	public static String FinancialYearId = "financialYearId";

	public static String AssessmentPeriod = "assessmentPeriod";
	public static String AssessmentPeriodId = "assessmentPeriodId";

	public static String PayingAccountId = "payingAccountId";
	public static String PayingAccount = "payingAccount";

	public static String PaymentDetails = "paymentDetails";
	public static String Remarks = "remarks";

	NumberFormat nf = NumberFormat.getFormat("#,##0.00");

	public WithdrawalListgrid() {
		super();

		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField customerAccountId = new ListGridField(CustomerAccountId, "AccountId");
		customerAccountId.setHidden(true);
		ListGridField customerAccountNo = new ListGridField(CustomerAccountNo, "AccountNo");
		ListGridField customerAccount = new ListGridField(CustomerAccount, "Account Name");

		ListGridField withdrawalAmount = new ListGridField(WithdrawalAmount, "Withdrawal Amount");
		withdrawalAmount.setShowGridSummary(true);

		withdrawalAmount.setSummaryFunction(new SummaryFunction() {
			public Object getSummaryValue(Record[] records, ListGridField field) {

				long totalAmount = 0;
				for (Record record : records) {

					totalAmount += Float
							.parseFloat(PAManager.getInstance().unformatCash(record.getAttribute(WithdrawalAmount)));

				}
				return "" + PAManager.getInstance().formatCash(totalAmount);
			}
		});

		ListGridField withdrawalCharge = new ListGridField(WithdrawalCharge, "Withdrawal Charges");

		withdrawalCharge.setShowGridSummary(true);

		withdrawalCharge.setSummaryFunction(new SummaryFunction() {
			public Object getSummaryValue(Record[] records, ListGridField field) {

				long totalAmount = 0;
				for (Record record : records) {

					totalAmount += Float
							.parseFloat(PAManager.getInstance().unformatCash(record.getAttribute(WithdrawalCharge)));

				}
				return "" + PAManager.getInstance().formatCash(totalAmount);
			}
		});

		ListGridField withdrawalDate = new ListGridField(WithdrawalDate, "Withdrawal Date");

		ListGridField financialYear = new ListGridField(FinancialYear, "Year");
		financialYear.setHidden(true);
		ListGridField financialYearId = new ListGridField(FinancialYearId, "YearId");
		financialYearId.setHidden(true);

		ListGridField assessmentPeriod = new ListGridField(AssessmentPeriod, "Period");
		assessmentPeriod.setHidden(true);
		ListGridField assessmentPeriodId = new ListGridField(AssessmentPeriodId, "PeriodId");
		assessmentPeriodId.setHidden(true);

		ListGridField payingAccountId = new ListGridField(PayingAccountId, "Paying Account Id");
		payingAccountId.setHidden(true);
		ListGridField payingAccount = new ListGridField(PayingAccount, "Paying Account");

		ListGridField paymentDetails = new ListGridField(PaymentDetails, "Payment Details");
		ListGridField remarks = new ListGridField(Remarks, "Remarks");

		this.setFields(id, customerAccountId, customerAccountNo, customerAccount, withdrawalAmount, withdrawalCharge,
				withdrawalDate, financialYear, financialYearId, assessmentPeriod, assessmentPeriodId, payingAccountId,
				payingAccount, paymentDetails, remarks);

		this.setCanExpandRecords(true);
		this.setShowGridSummary(true);

	}

	public ListGridRecord addRowData(Withdrawal withdrawal) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute(ID, withdrawal.getId());

		if (withdrawal.getCustomerAccount() != null) {
			record.setAttribute(CustomerAccountId, withdrawal.getCustomerAccount().getSysid());
			record.setAttribute(CustomerAccountNo, withdrawal.getCustomerAccount().getClientCode());
			record.setAttribute(CustomerAccount, withdrawal.getCustomerAccount().getSurname() + " "
					+ withdrawal.getCustomerAccount().getOtherNames());
		}

		record.setAttribute(WithdrawalAmount, nf.format(withdrawal.getWithdrawalAmount()));
		record.setAttribute(WithdrawalCharge, nf.format(withdrawal.getWithdrawalCharge()));
		record.setAttribute(WithdrawalDate, withdrawal.getWithdrawalDate());

//		if(withdrawal.getYear()!=null){
		// record.setAttribute(FinancialYear,
		// withdrawal.getFinancialYear().getFinancialYear());
		record.setAttribute(FinancialYearId, withdrawal.getYear());
		// }

		if (withdrawal.getPeriod() != null) {
			// record.setAttribute(AssessmentPeriod,
			// withdrawal.getAssessmentPeriod().getPeriodName());
			record.setAttribute(AssessmentPeriodId, withdrawal.getPeriod());
		}

		if (withdrawal.getPayingAccount() != null) {
			record.setAttribute(PayingAccountId, withdrawal.getPayingAccount().getSysid());
			record.setAttribute(PayingAccount, withdrawal.getPayingAccount().getAccountName());
		}

		record.setAttribute(PaymentDetails, withdrawal.getPaymentDetails());
		record.setAttribute(Remarks, withdrawal.getRemarks());

		return record;
	}

	public void addRecordsToGrid(List<Withdrawal> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (Withdrawal item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
	}

}
