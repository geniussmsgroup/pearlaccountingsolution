package com.pearl.accounting.sysui.client.presenters;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.google.gwt.user.client.Window;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.shared.DispatchAsync;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.ContentSlot;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.PlaceRequest;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentHandler;
import com.gwtplatform.mvp.client.proxy.RevealRootContentEvent;
import com.pearl.accounting.sysui.client.place.NameTokens;
import com.pearl.accounting.sysui.client.utils.PAConstant;
import com.pearl.accounting.sysui.client.views.widgets.ApplicationMenu;
import com.pearl.accounting.sysui.client.views.widgets.Masthead;
import com.pearl.accounting.sysui.client.views.widgets.NavigationPane;
import com.pearl.accounting.sysui.client.views.widgets.NavigationPaneHeader;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.events.ClickHandler;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;

public class MainPagePresenter extends Presenter<MainPagePresenter.MyView, MainPagePresenter.MyProxy> {

	private final PlaceManager placeManager;
	private final DispatchAsync dispatcher;

	@ContentSlot
	public static Type<RevealContentHandler<?>> TYPE_SetContextAreaContent = new Type<RevealContentHandler<?>>();

	public interface MyView extends View {

		public ApplicationMenu getApplicationMenu();

		public NavigationPaneHeader getNavigationPaneHeader();

		public NavigationPane getNavigationPane();

		public Masthead getMastHead();

		public MenuItem getLogoutItem();

		public MenuItem getChangePasswordItem();

		public MenuItem getDeligateItem();

		public MenuItem getClaimItem();

	}

	@ProxyCodeSplit
	@NameToken(NameTokens.mainpage)
	public interface MyProxy extends ProxyPlace<MainPagePresenter> {
	}

	@Inject
	public MainPagePresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
			final PlaceManager placeManager, final DispatchAsync dispatcher) {
		super(eventBus, view, proxy);
		this.dispatcher = dispatcher;
		this.placeManager = placeManager;
	}

	@Override
	protected void revealInParent() {
		RevealRootContentEvent.fire(this, this);
	}

	@Override
	protected void onBind() {
		super.onBind();

		myAccountMenuControl();
		displayLogedInUser();
		loadNavigationViews();

		loadNavigationViews();

	}

	protected void onReset() {
		super.onReset();
	}

	private class NavigationPaneClickHandler implements RecordClickHandler {
		public void onRecordClick(RecordClickEvent event) {

			Record record = event.getRecord();
			String name = record.getAttributeAsString("name");

			PlaceRequest myRequest = new PlaceRequest(name);

			placeManager.revealPlace(myRequest);

		}

	}

	private void myAccountMenuControl() {
		getView().getLogoutItem().addClickHandler(new ClickHandler() {

			public void onClick(MenuItemClickEvent event) {
				String relativeURL = "j_spring_security_logout";
				Window.Location.assign(GWT.getHostPageBaseURL() + relativeURL);

			}
		});

		getView().getChangePasswordItem().addClickHandler(new ClickHandler() {

			public void onClick(MenuItemClickEvent event) {
				// UtilsUserManager.loadChangePasswordWindow(dispatcher);

			}
		});

		getView().getDeligateItem().addClickHandler(new ClickHandler() {

			public void onClick(MenuItemClickEvent event) {

				// UtilsUserManager.loadUsersListWindow(dispatcher);

			}
		});

		getView().getClaimItem().addClickHandler(new ClickHandler() {

			public void onClick(MenuItemClickEvent event) {

				// UtilsUserManager.loadChangePasswordWindow(dispatcher);

			}
		});

	}

	private void displayLogedInUser() {
	}

	private void loadNavigationViews() {

		getView().getNavigationPane().addRecordClickHandler(PAConstant.SYSTEM_CONFIGURATION,
				new NavigationPaneClickHandler());
		
		getView().getNavigationPane().addRecordClickHandler(PAConstant.Loans_Management,
				new NavigationPaneClickHandler());
		
		getView().getNavigationPane().addRecordClickHandler(PAConstant.Account_transactions,
				new NavigationPaneClickHandler());
	}
}
