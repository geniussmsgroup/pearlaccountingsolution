package com.pearl.accounting.sysui.client.views.windows;

import com.pearl.accounting.sysui.client.views.widgets.ComboBox;
import com.pearl.accounting.sysui.client.views.widgets.TextField;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class AccountWindow extends Window {
	private TextField accountCode;
	private TextField accountName;
	private TextField periodstartamount;
	private ComboBox CurrencyCd;
	private TextField cbal;

	private ComboBox accountType;
	private ComboBox category;
	private ComboBox productType;
	private ComboBox AnalReq;
	 private ComboBox Ttype;

	// private ComboBox account_type;

	private IButton saveButton;

	public AccountWindow() {
		super();
		accountCode = new TextField();
		accountCode.setTitle("Account Code");
		cbal = new TextField();
		cbal.setTitle("carried balance");
		cbal.setDefaultValue(0);
		periodstartamount = new TextField();
		periodstartamount.setTitle("periodstartamount");
		periodstartamount.setDefaultValue(0);
		CurrencyCd = new ComboBox();
		CurrencyCd.setTitle("Currency");
		// CurrencyCd.setDefaultValue("UGX");
		accountName = new TextField();
		accountName.setTitle("Account Name");

		accountType = new ComboBox();
		accountType.setTitle("Account Type");

		category = new ComboBox();
		category.setTitle("Account Category");

		AnalReq = new ComboBox();
		AnalReq.setTitle("Track personal data?");
		Ttype = new ComboBox();
		Ttype.setTitle("Account Debit /Credit");

		productType = new ComboBox();
		productType.setTitle("Product Type");

		saveButton = new IButton("Save");
		// saveButtonx = new IButton("Saveagain");

		DynamicForm form = new DynamicForm();
		form.setFields(accountCode, accountName, accountType, category, productType,Ttype);
		form.setWrapItemTitles(true);
		form.setMargin(10);
		form.setNumCols(4);
		DynamicForm form2 = new DynamicForm();
		form2.setFields(AnalReq, CurrencyCd);
		form2.setWrapItemTitles(true);
		form2.setMargin(10);
		form2.setNumCols(4);

		DynamicForm form3 = new DynamicForm();
		form3.setFields(periodstartamount, cbal);
		form3.setWrapItemTitles(true);
		form3.setMargin(10);
		form3.setNumCols(4);

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(saveButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMembers(form, form2,form3);
		layout.addMember(buttonLayout);

		layout.setMargin(10);
		this.addItem(layout);
		this.setWidth("40%");
		this.setHeight("40%");
		this.setAutoCenter(true);
		this.setTitle("Account Setup");
		this.setIsModal(true);
		this.setShowModalMask(true);
	}
	public TextField getAccountCode() {
		return accountCode;
	}

	public TextField getAccountName() {
		return accountName;
	}

	public ComboBox getAccountType() {
		return accountType;
	}

	public ComboBox getTtype() {
		return Ttype;
	}
	public ComboBox getCategory() {
		return category;
	}

//	public ComboBox getTackable() {
//		return tackable;
//	}

	public IButton getSaveButton() {
		return saveButton;
	}

//	public IButton getSaveButtonx() {
//		return saveButtonx;
//	}

	public ComboBox getCurrencyCd() {
		return CurrencyCd;
	}
	public ComboBox getAnalReq() {
		return AnalReq;
	}
	public ComboBox getProductType() {
		return productType;
	}

	public TextField getCbal() {
		return cbal;
	}

//	public ComboBox getAccount_type() {
//		return account_type;
//	}

	public TextField getPeriodstartamount() {
		return periodstartamount;
	}

}
