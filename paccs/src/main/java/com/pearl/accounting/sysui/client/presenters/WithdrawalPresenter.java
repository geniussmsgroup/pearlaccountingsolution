package com.pearl.accounting.sysui.client.presenters;

import java.util.Date;
import java.util.LinkedHashMap;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.shared.DispatchAsync;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.AssessmentPeriod;
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.DataSourceScreen;
import com.pearl.accounting.sysmodel.FinancialYear;
import com.pearl.accounting.sysmodel.FixedData;
import com.pearl.accounting.sysmodel.Intervaltb;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.Theadp;
import com.pearl.accounting.sysmodel.Withdrawal;
import com.pearl.accounting.sysui.client.place.NameTokens;
import com.pearl.accounting.sysui.client.utils.PAConstant;
import com.pearl.accounting.sysui.client.views.listgrids.WithdrawalListgrid;
import com.pearl.accounting.sysui.client.views.panes.WithdrawalPane;
import com.pearl.accounting.sysui.client.views.widgets.SwizimaLoader;
import com.pearl.accounting.sysui.client.views.windows.DoubleEntryWindow;
import com.pearl.accounting.sysui.client.views.windows.LoanWindow;
import com.pearl.accounting.sysui.client.views.windows.SavingsWithdrawalWindow;
import com.pearl.accounting.sysui.shared.SysAction;
import com.pearl.accounting.sysui.shared.SysResult;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.EditorExitEvent;
import com.smartgwt.client.widgets.form.fields.events.EditorExitHandler;

public class WithdrawalPresenter extends Presenter<WithdrawalPresenter.MyView, WithdrawalPresenter.MyProxy> {

	private final PlaceManager placeManager;
	private final DispatchAsync dispatcher;

	public interface MyView extends View {

		public WithdrawalPane getWithdrawalPane();

	}

	@ProxyCodeSplit
	@NameToken(NameTokens.savingswithdrawals)
	public interface MyProxy extends ProxyPlace<WithdrawalPresenter> {
	}

	@Inject
	public WithdrawalPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
			final DispatchAsync dispatcher, final PlaceManager placeManager) {
		super(eventBus, view, proxy);

		this.dispatcher = dispatcher;
		this.placeManager = placeManager;
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this, MainPagePresenter.TYPE_SetContextAreaContent, this);
	}

	@Override
	protected void onBind() {
		super.onBind();
		onNewButtonClicked();
		loadWithDraws();
		onDeleteButtonClicked();
		onLoadButtonClicked();

	}

	private void onLoadButtonClicked() {
		getView().getWithdrawalPane().getLoadButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadWithDraws();

			}
		});

		getView().getWithdrawalPane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadWithDraws();

			}
		});
	}

	private void onNewButtonClicked() {
		getView().getWithdrawalPane().getAddButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				create();

			}
		});

		getView().getWithdrawalPane().getControlsPane().getNewAssetButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				create();

			}
		});
	}
//
	private void create() {

		SavingsWithdrawalWindow window = new SavingsWithdrawalWindow();

		loadAccounts(window);
		loadPeriods(window);
		loadFinancialYears(window);
		loadCustomers(window);
		onSaveButtonClicked(window);
		onDateLoaded(window);
		window.show();
	}

	private void onDateLoaded(final SavingsWithdrawalWindow window) {

		window.getWithdrawalDate().addChangedHandler(new ChangedHandler() {

			public void onChanged(ChangedEvent event) {
				dispatcher.execute(new SysAction(PAConstant.GET_Interval), new AsyncCallback<SysResult>() {
					public void onFailure(Throwable caught) {
						System.out.println(caught.getMessage());
					}

					public void onSuccess(SysResult result) {
						// TODO Auto-generated method stub
						if (result != null) {
//								
							for (Intervaltb period : result.getIntervaltbs()) {

								final Date dn = window.getWithdrawalDate().getValueAsDate();
								if (dn.before(period.getFEND()) && dn.after(period.getFSTART())) {
									window.getPeriod().setValue(period.getFINTNAME());
									window.getYear().setValue(period.getYRCODE());
									window.getStartDate().setValue(period.getFSTART());
									window.getEndDate().setValue(period.getFEND());
									window.getPeriodcode().setValue(period.getFINTCODE());

								}
							}
						}
					}
				});
			}
		});
	}

	private void loadPeriods(final SavingsWithdrawalWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_FixedData), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();
               //   Date tdate = null;
				if (result != null) {
					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

					for(FixedData fixeddata : result.getFixeddatas()) {

						valueMap.put(fixeddata.getIDNo(), String.valueOf(fixeddata.getBFToDate()));
					}

					window.getAssessmentPeriod().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

	}

	private void loadFinancialYears(final SavingsWithdrawalWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_FinancialYear), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

					for (FinancialYear financialYear : result.getFinancialYears()) {

						valueMap.put(financialYear.getId(), financialYear.getFinancialYear());
					}

					window.getFinancialYear().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void loadAccounts(final SavingsWithdrawalWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_ACCOUNT), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
					for (Account account : result.getAccounts()) {
						valueMap.put(account.getSysid(), account.getAccountName());
					}

					window.getPayingAccount().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void loadCustomers(final SavingsWithdrawalWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_customerAccount), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

					for (CustomerAccount account : result.getCustomerAccounts()) {
						valueMap.put(account.getSysid(), account.getSurname() + " " + account.getOtherNames());
					}

					window.getCustomerAccount().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private boolean FormValid(final SavingsWithdrawalWindow window) {
		if (window.getCustomerAccount().getValueAsString() != null) {
			if (window.getPayingAccount().getValueAsString() != null) {
				if (window.getWithdrawalAmount().getValueAsString() != null) {
					if (window.getWithdrawalCharge().getValueAsString() != null) {
						if (window.getPaymentDetails().getValueAsString() != null) {
							if (window.getRemarks().getValueAsString() != null) {
								return true;

							} else {
								SC.warn("ERROR", "Please enter Remarks");

							}
						} else {
							SC.warn("ERROR", "Please enter Payment Details");

						}
					} else {
						SC.warn("ERROR", "Please enter Withdraw charge");

					}
				} else {
					SC.warn("ERROR", "Please enter withdraw amount");

				}

			} else {
				SC.warn("ERROR", "Please enter Paying account");
			}
		} else {
			SC.warn("ERROR", "Please enter customer account");
		}
		return false;
	}

	private void onSaveButtonClicked(final SavingsWithdrawalWindow window) {

		window.getSaveButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				if (FormValid(window)) {

					AssessmentPeriod assessmentPeriod = new AssessmentPeriod();
					assessmentPeriod.setId(window.getAssessmentPeriod().getValueAsString());

					FinancialYear financialYear = new FinancialYear();
					financialYear.setId(window.getFinancialYear().getValueAsString());

					CustomerAccount customerAccount = new CustomerAccount();
					customerAccount.setSysid(window.getCustomerAccount().getValueAsString());

					Account payingAccount = new Account();
					payingAccount.setSysid(window.getPayingAccount().getValueAsString());

					float withdrawalAmount = Float.parseFloat(window.getWithdrawalAmount().getValueAsString());
					float withdrawalCharge = Float.parseFloat(window.getWithdrawalCharge().getValueAsString());
					Date withdrawalDate = window.getWithdrawalDate().getValueAsDate();

					String paymentDetails = window.getPaymentDetails().getValueAsString();
					String remarks = window.getRemarks().getValueAsString();
					int year = Integer.parseInt(window.getFinancialYear().getValueAsString());

					Withdrawal withdrawal = new Withdrawal();
					withdrawal.setPeriod(window.getAssessmentPeriod().getValueAsString());
					// withdrawal.setCreatedBy(createdBy);
					withdrawal.setCustomerAccount(customerAccount);
					withdrawal.setDateCreated(new Date());
					withdrawal.setDateUpdated(new Date());
					withdrawal.setYear(year);
					withdrawal.setPayingAccount(payingAccount);
					withdrawal.setPaymentDetails(paymentDetails);
					withdrawal.setRemarks(remarks);
					withdrawal.setStatus(Status.ACTIVE);
					// withdrawal.setUpdatedBy(updatedBy);
					withdrawal.setWithdrawalAmount(withdrawalAmount);
					withdrawal.setWithdrawalCharge(withdrawalCharge);
					withdrawal.setWithdrawalDate(withdrawalDate);
					SC.say("it workks");
					Theadp thead =new Theadp();
					thead.setPeriod(window.getPeriodcode().getValueAsString());
                     thead.setPeriodcode(window.getPeriodcode().getValueAsString());					
					 thead.setPeriod(window.getPeriod().getValueAsString());
					 thead.setYear(year);
                     thead.setDataSourceScreen(DataSourceScreen.MULTI_JOURNAL);
                     thead.setStatus(Status.ACTIVE);
                     thead.setRemarks(remarks);
                     thead.setVNO(paymentDetails);
                     thead.setAmount(withdrawalAmount+withdrawalCharge);
             	thead.setDateCreated(new Date());
					thead.setDateUpdated(new Date());
					thead.setAccount(payingAccount);
					dispatcher.execute(new SysAction(PAConstant.SAVE_Thead, thead),
							new AsyncCallback<SysResult>() {
								public void onFailure(Throwable caught) {
									System.out.println(caught.getMessage());
								}

								public void onSuccess(SysResult result) {

									SC.clearPrompt();

									if (result != null) {

										if (result.getSwizFeedback().isResponse()) {
											ClearForms(window);
											SC.say("SUCCESS", result.getSwizFeedback().getMessage());

										} else {
											SC.warn("ERROR", result.getSwizFeedback().getMessage());
										}

									} else {
										SC.say("ERROR", "Unknow error");
									}

								}
							});
					
					
					SC.showPrompt("", "", new SwizimaLoader());

					dispatcher.execute(new SysAction(PAConstant.SAVE_WITHDRAWAL, withdrawal),
							new AsyncCallback<SysResult>() {
								public void onFailure(Throwable caught) {
									System.out.println(caught.getMessage());
								}

								public void onSuccess(SysResult result) {

									SC.clearPrompt();

									if (result != null) {

										if (result.getSwizFeedback().isResponse()) {

											SC.say("SUCCESS", result.getSwizFeedback().getMessage());

											getView().getWithdrawalPane().getWithdrawalListgrid()
													.addRecordsToGrid(result.getWithdrawals());
											ClearForms(window);

										} else {
											SC.warn("ERROR", result.getSwizFeedback().getMessage());
										}

									} else {
										SC.say("ERROR", "Unknow error");
									}

								}
							});
				}

			}
		});
	}

	private void ClearForms(final SavingsWithdrawalWindow window) {
		window.getCustomerAccount().clearValue();
		window.getWithdrawalAmount().clearValue();
		window.getPaymentDetails().clearValue();
		window.getAssessmentPeriod().clearValue();
		window.getRemarks().clearValue();
		window.getPayingAccount().clearValue();
		window.getWithdrawalCharge().clearValue();

	}

	private void loadWithDraws() {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_WITHDRAWAL), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					getView().getWithdrawalPane().getWithdrawalListgrid().addRecordsToGrid(result.getWithdrawals());

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

	}

	private void onDeleteButtonClicked() {
		getView().getWithdrawalPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				delete();

			}
		});
		getView().getWithdrawalPane().getControlsPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				delete();

			}
		});
	}

	private void delete() {
		if (getView().getWithdrawalPane().getWithdrawalListgrid().anySelected()) {
			SC.say("Confirm", "Are you sure you want to delete the selected record?", new BooleanCallback() {

				public void execute(Boolean value) {
					if (value) {
						Withdrawal withdrawal = new Withdrawal();
						withdrawal.setId(getView().getWithdrawalPane().getWithdrawalListgrid().getSelectedRecord()
								.getAttribute(WithdrawalListgrid.ID));

						SC.showPrompt("", "", new SwizimaLoader());

						dispatcher.execute(new SysAction(PAConstant.DELETE_WITHDRAWAL, withdrawal),
								new AsyncCallback<SysResult>() {
									public void onFailure(Throwable caught) {
										System.out.println(caught.getMessage());
									}

									public void onSuccess(SysResult result) {

										SC.clearPrompt();

										if (result != null) {

											if (result.getSwizFeedback().isResponse()) {

												SC.say("SUCCESS", result.getSwizFeedback().getMessage());

												getView().getWithdrawalPane().getWithdrawalListgrid()
														.addRecordsToGrid(result.getWithdrawals());

											} else {
												SC.warn("ERROR", result.getSwizFeedback().getMessage());
											}

										} else {
											SC.say("ERROR", "Unknow error");
										}

									}
								});
					}

				}
			});
		} else {
			SC.say("ERROR", "Please select record to delete");
		}
	}

}
