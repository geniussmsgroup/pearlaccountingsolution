package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.pearl.accounting.sysmodel.EmailNotificationConfiguration;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord; 

public class EmailNotificationConfigurationListGrid extends SuperListGrid {
	public static String ID = "id";
	public static String HOST = "hostConfiguration";
	public static String PORT = "portNumberConfiguration";
	public static String USER = "userConfiguration";

	public static String PASSWORD = "passwordConfiguration";

	public static String SUBJECT = "subjectConfiguration";
	public static String MESSAGE = "messageBodyConfinguration";
	public static String LINK = "applicationLinkConfiguration";
 

	public EmailNotificationConfigurationListGrid() {
		super();

		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField hostConfiguration = new ListGridField(HOST, "Host");

		ListGridField portNumberConfiguration = new ListGridField(PORT, "Port");

		ListGridField userConfiguration = new ListGridField(USER, "User");
		ListGridField passwordConfiguration = new ListGridField(PASSWORD, "Password");
		ListGridField subjectConfiguration = new ListGridField(SUBJECT, "Subject");
		ListGridField messageBodyConfinguration = new ListGridField(MESSAGE, "Message Body");
		ListGridField applicationLinkConfiguration = new ListGridField(LINK, "Application link"); 

		this.setFields(id, hostConfiguration, portNumberConfiguration, userConfiguration, passwordConfiguration,
				subjectConfiguration, messageBodyConfinguration, applicationLinkConfiguration);
	}

	public ListGridRecord addRowData(EmailNotificationConfiguration configuration) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute(ID, configuration.getId());
		record.setAttribute(HOST, configuration.getHostConfiguration());
		record.setAttribute(PORT, configuration.getPortNumberConfiguration());

		record.setAttribute(USER, configuration.getUserConfiguration());
		record.setAttribute(PASSWORD, configuration.getPasswordConfiguration());
		record.setAttribute(SUBJECT, configuration.getSubjectConfiguration());
		record.setAttribute(MESSAGE, configuration.getMessageBodyConfinguration());
		record.setAttribute(LINK, configuration.getApplicationLinkConfiguration());

		 

		return record;
	}

	public void addRecordsToGrid(List<EmailNotificationConfiguration> configurations) {
		if (configurations != null) {
			ListGridRecord[] records = new ListGridRecord[configurations.size()];
			int row = 0;
			for (EmailNotificationConfiguration item : configurations) {
				records[row] = addRowData(item);
				row++;
			}
			this.setData(records);
		}

	}
}
