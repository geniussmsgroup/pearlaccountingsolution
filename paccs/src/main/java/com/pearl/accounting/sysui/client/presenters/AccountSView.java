//package com.pearl.accounting.sysui.client.presenters;
//
//import com.google.gwt.user.client.Window;
//import com.google.gwt.user.client.ui.Widget;
//import com.google.inject.Inject;
//import com.gwtplatform.mvp.client.ViewImpl;
//import com.pearl.accounting.sysui.client.views.panes.AccountPane;
//import com.pearl.accounting.sysui.client.views.panes.AccountSPane;
//import com.smartgwt.client.widgets.layout.VLayout;
//
//public class AccountSView extends ViewImpl implements AccountSPresenter.MyView {
//
//	private static final String DEFAULT_MARGIN = "0px";
//	private VLayout panel;
//	private AccountSPane accountsPane;
//
//	@Inject
//	public AccountSView() {
//
//		panel = new VLayout();
//
//		accountsPane = new AccountSPane();
//
//		panel.setMembers(accountsPane);
//		panel.setWidth100();
//		panel.setHeight("90%");
//		Window.enableScrolling(false);
//		Window.setMargin(DEFAULT_MARGIN);
//
//	}
//
//	public Widget asWidget() {
//		return panel;
//	}
//
//	public AccountSPane getAccountSPane() {
//		return accountsPane;
//	}
//	
//	
//
//}
