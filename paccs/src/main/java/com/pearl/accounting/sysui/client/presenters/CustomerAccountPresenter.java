package com.pearl.accounting.sysui.client.presenters;

import java.util.Date;
import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.shared.DispatchAsync;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.pearl.accounting.syscore.services.TheadService;
import com.pearl.accounting.syscore.services.impl.CustomerAccountServiceImpl;
import com.pearl.accounting.syscore.services.impl.TheadServiceImpl;
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.Fsave;
import com.pearl.accounting.sysmodel.Gender;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.Theadp;
import com.pearl.accounting.sysui.client.place.NameTokens;
import com.pearl.accounting.sysui.client.utils.PAConstant;
import com.pearl.accounting.sysui.client.views.listgrids.CustomerAccountListgrid;
import com.pearl.accounting.sysui.client.views.panes.CustomerAccountPane;
import com.pearl.accounting.sysui.client.views.widgets.SwizimaLoader;
import com.pearl.accounting.sysui.client.views.windows.AccountWindow;
import com.pearl.accounting.sysui.client.views.windows.CustomerAccountWindow;
import com.pearl.accounting.sysui.client.views.windows.DoubleEntryWindow;
import com.pearl.accounting.sysui.shared.SysAction;
import com.pearl.accounting.sysui.shared.SysResult;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;

public class CustomerAccountPresenter
		extends Presenter<CustomerAccountPresenter.MyView, CustomerAccountPresenter.MyProxy> {

	private final PlaceManager placeManager;
	private final DispatchAsync dispatcher;

	public interface MyView extends View {
		public CustomerAccountPane getCustomerAccountPane();
	}

	@ProxyCodeSplit
	@NameToken(NameTokens.customers)
	public interface MyProxy extends ProxyPlace<CustomerAccountPresenter> {
	}

	@Inject
	public CustomerAccountPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
			final DispatchAsync dispatcher, final PlaceManager placeManager) {
		super(eventBus, view, proxy);

		this.dispatcher = dispatcher;
		this.placeManager = placeManager;
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this, MainPagePresenter.TYPE_SetContextAreaContent, this);
	}

	@Override
	protected void onBind() {
		super.onBind();
		onNewbuttonClicked();
		load();
		onDeleteButtonClicked();
		onLoadButtonClicked();

	}

	private void onLoadButtonClicked() {
		getView().getCustomerAccountPane().getLoadButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				load();

			}
		});

		getView().getCustomerAccountPane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				load();

			}
		});
	}

	private void onNewbuttonClicked() {
		getView().getCustomerAccountPane().getAddButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				create();

			}
		});

		getView().getCustomerAccountPane().getControlsPane().getNewAssetButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				create();

			}
		});
	}

	private void create() {
		CustomerAccountWindow window = new CustomerAccountWindow();
		loadGender(window);
		onSaveButtonClicked(window);
		// searchbar(window);
		loadStatus(window);
		window.show();

	}

	private void loadGender(CustomerAccountWindow window) {

		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
		valueMap.put("M", "M");
		
		valueMap.put("F", "F");
		valueMap.put("O", "O");

		window.getGender().setValueMap(valueMap);

	}

	private void loadStatus(CustomerAccountWindow window) {

		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

		valueMap.put("Single", "Single");
		valueMap.put("Married", "Married");
		valueMap.put("Divorced", "Divorced");

		window.getMaritalStatus().setValueMap(valueMap);

	}

	private boolean IsFormValid(final CustomerAccountWindow window) {
		if (window.getFirstName().getValueAsString() != null) {
			if (window.getLastName().getValueAsString() != null) {

				return true;

			} else {
				SC.warn("Error", "please enter last name");
			}

		} else {

			SC.warn("Error", "please enter first name");
		}

		return false;

	}

	private void onSaveButtonClicked(final CustomerAccountWindow window) {
		window.getSaveButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				// String accountNo=window.
				if (IsFormValid(window)) {

					String firstName = window.getFirstName().getValueAsString();
					String lastName = window.getLastName().getValueAsString();
					Date dob = window.getDob().getValueAsDate();
					Gender gender = Gender.getGender(window.getGender().getValueAsString());

					String nextofKin = window.getNextofKin().getValueAsString();
					String relationShip = window.getRelationShip().getValueAsString();
					String relationShipContact = window.getRelationShipContact().getValueAsString();
					String residence = window.getResidence().getValueAsString();

					String workPlace = window.getWorkPlace().getValueAsString();
					String Address = window.getAddress().getValueAsString();
					String Address1 = window.getAddress1().getValueAsString();

					String profession = window.getProfession().getValueAsString();

					String phoneNumber = window.getPhoneNumber().getValueAsString();
					String emailAddress = window.getEmailAddress().getValueAsString();
					String nationalId = window.getNationalId().getValueAsString();

					CustomerAccount customerAccount = new CustomerAccount();
					customerAccount.setKinRelation(relationShip);
					customerAccount.setKinresidence(window.getKinresidence().getValueAsString());
					customerAccount.setKinContact(relationShipContact);
					customerAccount.setNextofKin(nextofKin);
					customerAccount.setAddress2(Address1);
					customerAccount.setAddress1(Address);
				    customerAccount.setKinContact(window.getRelationShipContact().getValueAsString());
					customerAccount.setMaritalStatus(window.getMaritalStatus().getValueAsString());
					customerAccount.setDateCreated(new Date());
					customerAccount.setDateUpdated(new Date());
					customerAccount.setBirthDate(dob);
					customerAccount.setEmail(emailAddress);
					customerAccount.setSurname(firstName);
					customerAccount.setGender(window.getGender().getValueAsString());
					customerAccount.setOtherNames(lastName);
					customerAccount.setNationalId(nationalId);
					customerAccount.setPhoneNumber(phoneNumber);
					customerAccount.setOccupation(profession);
					customerAccount.setAge(0);
					customerAccount.setKinresidence(residence);
					customerAccount.setStatus(Status.ACTIVE);
					customerAccount.setClientCode(window.getClientCode().getValueAsString());
					customerAccount.setWorkPlace(workPlace);
					customerAccount.setRLnID(0);
					customerAccount.setApproved("Y");
                    customerAccount.setFsave(Fsave.Y);
                    customerAccount.setTitle(window.getTitles().getValueAsString());
                    customerAccount.setOfficePhone(window.getOfficePhone().getValueAsString());
                    customerAccount.setSysid("0");
                    SC.showPrompt("", "", new SwizimaLoader());

					dispatcher.execute(new SysAction(PAConstant.SAVE_customerAccount, customerAccount),
							new AsyncCallback<SysResult>() {
								public void onFailure(Throwable caught) {
									System.out.println(caught.getMessage());
								}

								public void onSuccess(SysResult result) {

									SC.clearPrompt();

									if (result != null) {

										if (result.getSwizFeedback().isResponse()) {

											SC.say("SUCCESS", result.getSwizFeedback().getMessage());

											getView().getCustomerAccountPane().getCustomerAccountListgrid()
													.addRecordsToGrid(result.getCustomerAccounts());
										} else {
											SC.warn("ERROR", result.getSwizFeedback().getMessage());
										}

									} else {
										SC.say("ERROR", "Unknow error");
									}

								}
							});

				}
			}
		});
	}

	private void load() {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_customerAccount), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					getView().getCustomerAccountPane().getCustomerAccountListgrid()
							.addRecordsToGrid(result.getCustomerAccounts());

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void delete() {
		if (getView().getCustomerAccountPane().getCustomerAccountListgrid().anySelected()) {
			SC.ask("Confirm", "Are you sure you want to delete the selected record?", new BooleanCallback() {

				public void execute(Boolean value) {
					if (value) {

						CustomerAccount customerAccount = new CustomerAccount();

						customerAccount.setClientCode(getView().getCustomerAccountPane().getCustomerAccountListgrid()
								.getSelectedRecord().getAttribute(CustomerAccountListgrid.ID));

						SC.showPrompt("", "", new SwizimaLoader());

						dispatcher.execute(new SysAction(PAConstant.DELETE_customerAccount, customerAccount),
								new AsyncCallback<SysResult>() {
									public void onFailure(Throwable caught) {
										System.out.println(caught.getMessage());
									}

									public void onSuccess(SysResult result) {

										SC.clearPrompt();

										if (result != null) {

											if (result.getSwizFeedback().isResponse()) {

												SC.say("SUCCESS", result.getSwizFeedback().getMessage());

												getView().getCustomerAccountPane().getCustomerAccountListgrid()
														.addRecordsToGrid(result.getCustomerAccounts());
											} else {
												SC.warn("ERROR", result.getSwizFeedback().getMessage());
											}

										} else {
											SC.say("ERROR", "Unknow error");
										}

									}
								});
					}

				}
			});
		} else {
			SC.say("ERROR", "ERROR:Please select record to delete");
		}
	}

	private void onDeleteButtonClicked() {

		getView().getCustomerAccountPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				delete();

			}
		});

		getView().getCustomerAccountPane().getControlsPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				delete();

			}
		});
	}
//	private void searchbar(final CustomerAccountWindow window) {
//		window.getSearchButton().addClickHandler(new ClickHandler() {
//		
//			
//			public void onClick(ClickEvent event) {
////							SC.say("worked");
//
//			dispatcher.execute(new SysAction(PAConstant.GET_customerAccount), new AsyncCallback<SysResult>() {
//			public void onFailure(Throwable caught) {
//			System.out.println(caught.getMessage());
//			}
//
//		
//			public void onSuccess(SysResult result) {
//			// TODO Auto-generated method stub
//			SC.clearPrompt();
//		
//			if (result != null) {
//			
//				String enteredString = window.getSearchbar().getValueAsString();
//				
//				
//				//CustomerAccount thead = customerAccountServiceImpl.find(enteredString);
//				for ( CustomerAccount thead : result.getCustomerAccounts()) {
//			
//						
//			String id = thead.getAccountNo();
////			if (enteredString == id) {
////            //String tdate =thead.getTdates().toString().toUpperCase();
////			String vno = thead.getFirstName().toUpperCase();
////			String remarks = thead.getLastName().toUpperCase();
////			//float amt = thead.getAmount();
//			
//							//SC.say(vno + " " + remarks + " ");
//							SC.say("First Name $vno, Remarks $remarks");
////		window.setTransdate(thead.getTdates());
////		window.setPaydet(thead.getVNO().getBytes());
////		window.setAmount(thead.getAmount());
////		window.setRemarks(thead.getRemarks());
////			window.setAccredit(thead.getCredit());
////			window.setAcdebit(thead.getDebit());
////			
//			
//			
//			}
//			}
//				}}});}});
//		}
}
