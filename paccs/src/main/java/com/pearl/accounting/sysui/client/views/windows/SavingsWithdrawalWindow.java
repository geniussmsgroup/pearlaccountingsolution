package com.pearl.accounting.sysui.client.views.windows;

import com.pearl.accounting.sysui.client.views.widgets.ComboBox;
import com.pearl.accounting.sysui.client.views.widgets.TextField;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class SavingsWithdrawalWindow extends Window {

	private ComboBox financialYear;
	private ComboBox assessmentPeriod;
	private ComboBox customerAccount;
	private ComboBox payingAccount;
	private TextField periodcode;

	private TextField withdrawalAmount;
	private TextField withdrawalCharge;
	private DateItem withdrawalDate;
	private DateItem startDate;
	private DateItem endDate;
	private TextField period;
	private TextField year;
	private TextField paymentDetails;
	private TextField remarks;

	private IButton saveButton;

	public SavingsWithdrawalWindow() {
		super();

		financialYear = new ComboBox();
		financialYear.setTitle("Year");

		assessmentPeriod = new ComboBox();
		assessmentPeriod.setTitle("Period");

		customerAccount = new ComboBox();
		customerAccount.setTitle("Customer");

		payingAccount = new ComboBox();
		payingAccount.setTitle("Paying Account");

		withdrawalAmount = new TextField();
		withdrawalAmount.setTitle("Withdrawal Amount");
		withdrawalAmount.setKeyPressFilter("[0-9.]");

		withdrawalCharge = new TextField();
		withdrawalCharge.setTitle("Withdrawal Charge");
		period = new TextField();
		period.setTitle("period");
		period.setWidth("10px");
		periodcode = new TextField();
		periodcode.setTitle("periodcode");
		periodcode.setWidth("10px");

		year = new TextField();
		year.setTitle("year");
		year.setWidth("10px");

		withdrawalDate = new DateItem();
		withdrawalDate.setTitle("Withdrawal Date");
		withdrawalDate.setUseTextField(true);
		withdrawalDate.setWidth("*");
		withdrawalDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		startDate = new DateItem();
		startDate.setTitle("startDate ");
		startDate.setUseTextField(true);
		startDate.setWidth("*");
		startDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
		endDate = new DateItem();
		endDate.setTitle("endDate");
		endDate.setUseTextField(true);
		endDate.setWidth("*");
		endDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		paymentDetails = new TextField();
		paymentDetails.setTitle("Payment Details");

		remarks = new TextField();
		remarks.setTitle("Remarks");

		saveButton = new IButton("Save");

		DynamicForm form = new DynamicForm();
		form.setFields(assessmentPeriod,period, periodcode, year, startDate, endDate);
		form.setWrapItemTitles(true);
		form.setMargin(10);
		form.setNumCols(4);

		DynamicForm form2 = new DynamicForm();
		form2.setFields(customerAccount, withdrawalAmount, withdrawalCharge, withdrawalDate, payingAccount,
				paymentDetails, remarks);
		form2.setWrapItemTitles(true);
		form2.setMargin(10);
		form2.setNumCols(4);

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(saveButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(form);
		layout.addMember(form2);
		layout.addMember(buttonLayout);

		layout.setMargin(10);
		this.addItem(layout);
		this.setWidth("50%");
		this.setHeight("50%");
		this.setAutoCenter(true);
		this.setTitle("Savings withdrawal");
		this.setIsModal(true);
		this.setShowModalMask(true);
	}

	public ComboBox getFinancialYear() {
		return financialYear;
	}

	public ComboBox getAssessmentPeriod() {
		return assessmentPeriod;
	}

	public ComboBox getCustomerAccount() {
		return customerAccount;
	}

	public DateItem getStartDate() {
		return startDate;
	}

	public DateItem getEndDate() {
		return endDate;
	}

	public TextField getPeriod() {
		return period;
	}

	public TextField getPeriodcode() {
		return periodcode;
	}

	public TextField getYear() {
		return year;
	}

	public ComboBox getPayingAccount() {
		return payingAccount;
	}

	public TextField getWithdrawalAmount() {
		return withdrawalAmount;
	}

	public TextField getWithdrawalCharge() {
		return withdrawalCharge;
	}

	public DateItem getWithdrawalDate() {
		return withdrawalDate;
	}

	public TextField getPaymentDetails() {
		return paymentDetails;
	}

	public TextField getRemarks() {
		return remarks;
	}

	public IButton getSaveButton() {
		return saveButton;
	}

}
