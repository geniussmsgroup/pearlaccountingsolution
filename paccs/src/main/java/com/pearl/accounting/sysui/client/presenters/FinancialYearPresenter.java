package com.pearl.accounting.sysui.client.presenters;

import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.shared.DispatchAsync;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.pearl.accounting.sysmodel.FinancialYear;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysui.client.place.NameTokens;
import com.pearl.accounting.sysui.client.utils.PAConstant;
import com.pearl.accounting.sysui.client.views.listgrids.FinancialYearListgrid;
import com.pearl.accounting.sysui.client.views.panes.FinancialYearPane;
import com.pearl.accounting.sysui.client.views.widgets.SwizimaLoader;
import com.pearl.accounting.sysui.client.views.windows.FinancialYearWindow;
import com.pearl.accounting.sysui.shared.SysAction;
import com.pearl.accounting.sysui.shared.SysResult;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class FinancialYearPresenter extends Presenter<FinancialYearPresenter.MyView, FinancialYearPresenter.MyProxy> {

	private final PlaceManager placeManager;
	private final DispatchAsync dispatcher;

	public interface MyView extends View {
		public FinancialYearPane getFinancialYearPane();
	}

	@ProxyCodeSplit
	@NameToken(NameTokens.years)
	public interface MyProxy extends ProxyPlace<FinancialYearPresenter> {
	}

	@Inject
	public FinancialYearPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
			final DispatchAsync dispatcher, final PlaceManager placeManager) {
		super(eventBus, view, proxy);

		this.dispatcher = dispatcher;
		this.placeManager = placeManager;
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this, MainPagePresenter.TYPE_SetContextAreaContent, this);
	}

	@Override
	protected void onBind() {
		super.onBind();

		loadFinancialYears();

		onNewButtonClicked();

		onLoadButtonCLicked();

		onDeleteButtonClicked();

		onEditButtonClicked();

		onActivateButtonClicked();
	}

	private void onNewButtonClicked() {
		getView().getFinancialYearPane().getAddButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				createNewFinancialYear();

			}
		});

		getView().getFinancialYearPane().getControlsPane().getNewAssetButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				createNewFinancialYear();

			}
		});
	}

	private void createNewFinancialYear() {
		FinancialYearWindow window = new FinancialYearWindow();
		onSaveButtonClicked(window);
		window.show();
	}

	private boolean FormValid(final FinancialYearWindow window) {
		if (window.getStartDate().getValueAsDate() != null) {
			if (window.getEndDate().getValueAsDate() != null) {
				if (window.getFyearCode().getValueAsString() != null) {
					if (window.getFyearName().getValueAsString() != null) {
						return true;
					} else {
						SC.warn("ERROR", "Please enter year name");

					}
				} else {
					SC.warn("ERROR", "Please enter year code");

				}
			} else {
				SC.warn("ERROR", "Please enter end date");

			}
		} else {
			SC.warn("ERROR", "Please enter start date");

		}
		return false;
	}

	private void onSaveButtonClicked(final FinancialYearWindow window) {
		window.getSaveButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				if(FormValid(window)) {

				FinancialYear financialYear = new FinancialYear();

				financialYear.setCode(window.getFyearCode().getValueAsString());
				financialYear.setDateCreated(new Date());
				financialYear.setDateUpdated(new Date());
				financialYear.setEndDate(window.getEndDate().getValueAsDate());
				financialYear.setFinancialYear(window.getFyearName().getValueAsString());
				financialYear.setStartDate(window.getStartDate().getValueAsDate());
				financialYear.setStatus(Status.ACTIVE);
				financialYear.setActivationStatus(Status.IN_ACTIVE);

				SC.showPrompt("", "", new SwizimaLoader());

				dispatcher.execute(new SysAction(PAConstant.SAVE_FinancialYear, financialYear),
						new AsyncCallback<SysResult>() {
							public void onFailure(Throwable caught) {
								System.out.println(caught.getMessage());
							}

							public void onSuccess(SysResult result) {

								SC.clearPrompt();

								if (result != null) {
									if (result.getSwizFeedback().isResponse()) {

										SC.say("SUCCESS", result.getSwizFeedback().getMessage());

										ClearForm(window);
										
										getView().getFinancialYearPane().getFinancialYearListgrid()
												.addRecordsToGrid(result.getFinancialYears());
									} else {
										SC.warn("ERROR", result.getSwizFeedback().getMessage());
									}

								} else {
									SC.say("ERROR", "Unknow error");
								}

							}
						});
			}}
		});
	}

	private void ClearForm(final FinancialYearWindow window) {
		window.getFyearCode().clearValue();
		window.getFyearCode().clearValue();
		window.getStartDate().clearValue();
		window.getEndDate().clearValue();
		window.getFyearName().clearValue();
		window.getFyearCode().clearValue();
	}
	private void onEditButtonClicked() {
		getView().getFinancialYearPane().getEditButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				edit();

			}
		});

		getView().getFinancialYearPane().getControlsPane().getEditButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				edit();

			}
		});
	}

	private void edit() {
		if (getView().getFinancialYearPane().getFinancialYearListgrid().anySelected()) {
			FinancialYearWindow window = new FinancialYearWindow();
			onUpdatedButtonClicked(window);
			loadRecordToEdit(window);
			window.setTitle("Update financial year");
			window.show();
		} else {
			SC.warn("ERROR", "Please select record to delete");
		}

	}

	private void loadRecordToEdit(final FinancialYearWindow window) {

		ListGridRecord record = getView().getFinancialYearPane().getFinancialYearListgrid().getSelectedRecord();

		window.getFyearCode().setValue(record.getAttribute(FinancialYearListgrid.CODE));
		window.getFyearName().setValue(record.getAttribute(FinancialYearListgrid.NAME));

		if (record.getAttribute(FinancialYearListgrid.END_DATE) != null) {
			window.getEndDate().setValue(new Date(record.getAttribute(FinancialYearListgrid.END_DATE)));
		}

		if (record.getAttribute(FinancialYearListgrid.START_DATE) != null) {
			window.getStartDate().setValue(new Date(record.getAttribute(FinancialYearListgrid.START_DATE)));
		}

	}

	private void onUpdatedButtonClicked(final FinancialYearWindow window) {

		window.getSaveButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				ListGridRecord record = getView().getFinancialYearPane().getFinancialYearListgrid().getSelectedRecord();

				FinancialYear financialYear = new FinancialYear();

				financialYear.setId(record.getAttribute(FinancialYearListgrid.ID));

				financialYear.setCode(window.getFyearCode().getValueAsString());
				financialYear.setDateCreated(new Date());
				financialYear.setDateUpdated(new Date());
				financialYear.setEndDate(window.getEndDate().getValueAsDate());
				financialYear.setFinancialYear(window.getFyearName().getValueAsString());
				financialYear.setStartDate(window.getStartDate().getValueAsDate());
				financialYear.setStatus(Status.ACTIVE);
				financialYear.setActivationStatus(Status.IN_ACTIVE);

				SC.showPrompt("", "", new SwizimaLoader());

				dispatcher.execute(new SysAction(PAConstant.UPDATE_FinancialYear, financialYear),
						new AsyncCallback<SysResult>() {
							public void onFailure(Throwable caught) {
								System.out.println(caught.getMessage());
							}

							public void onSuccess(SysResult result) {

								SC.clearPrompt();

								if (result != null) {
									if (result.getSwizFeedback().isResponse()) {

										SC.say("SUCCESS", result.getSwizFeedback().getMessage());

										getView().getFinancialYearPane().getFinancialYearListgrid()
												.addRecordsToGrid(result.getFinancialYears());
										window.close();
									} else {
										SC.warn("ERROR", result.getSwizFeedback().getMessage());
									}

								} else {
									SC.say("ERROR", "Unknow error");
								}

							}
						});
			}
		});

	}

	private void onLoadButtonCLicked() {
		getView().getFinancialYearPane().getLoadButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadFinancialYears();

			}
		});

		getView().getFinancialYearPane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadFinancialYears();

			}
		});
	}

	private void loadFinancialYears() {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_FinancialYear), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					getView().getFinancialYearPane().getFinancialYearListgrid()
							.addRecordsToGrid(result.getFinancialYears());

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void onDeleteButtonClicked() {
		getView().getFinancialYearPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				deleteFinancialYear();

			}
		});

		getView().getFinancialYearPane().getControlsPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				deleteFinancialYear();

			}
		});
	}

	private void deleteFinancialYear() {

		if (getView().getFinancialYearPane().getFinancialYearListgrid().anySelected()) {
			SC.ask("Confirm", "Are you sure you want to delete the selected record?", new BooleanCallback() {

				public void execute(Boolean value) {

					if (value) {
						FinancialYear financialYear = new FinancialYear();

						financialYear.setId(getView().getFinancialYearPane().getFinancialYearListgrid()
								.getSelectedRecord().getAttribute(FinancialYearListgrid.ID));

						SC.showPrompt("", "", new SwizimaLoader());

						dispatcher.execute(new SysAction(PAConstant.DELETE_FinancialYear, financialYear),
								new AsyncCallback<SysResult>() {
									public void onFailure(Throwable caught) {
										System.out.println(caught.getMessage());
									}

									public void onSuccess(SysResult result) {

										SC.clearPrompt();

										if (result != null) {
											if (result.getSwizFeedback().isResponse()) {

												SC.say("SUCCESS", result.getSwizFeedback().getMessage());

												getView().getFinancialYearPane().getFinancialYearListgrid()
														.addRecordsToGrid(result.getFinancialYears());

											} else {
												SC.warn("ERROR", result.getSwizFeedback().getMessage());
											}

										} else {
											SC.say("ERROR", "Unknow error");
										}

									}
								});
					}

				}
			});
		} else {
			SC.warn("ERROR", "ERROR: Please select record to delete");
		}

	}

	private void onActivateButtonClicked() {
		getView().getFinancialYearPane().getActivateButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				activateFinancialYear();

			}
		});

		getView().getFinancialYearPane().getDeActivateButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				deactivateFinancialYear();

			}
		});
	}

	private void activateFinancialYear() {

		if (getView().getFinancialYearPane().getFinancialYearListgrid().anySelected()) {
			SC.ask("Confirm", "Are you sure you want to Activate the selected record?", new BooleanCallback() {

				public void execute(Boolean value) {

					if (value) {
						FinancialYear financialYear = new FinancialYear();

						financialYear.setId(getView().getFinancialYearPane().getFinancialYearListgrid()
								.getSelectedRecord().getAttribute(FinancialYearListgrid.ID));

						SC.showPrompt("", "", new SwizimaLoader());

						dispatcher.execute(new SysAction(PAConstant.ACTIVATE_FinancialYear, financialYear),
								new AsyncCallback<SysResult>() {
									public void onFailure(Throwable caught) {
										System.out.println(caught.getMessage());
									}

									public void onSuccess(SysResult result) {

										SC.clearPrompt();

										if (result != null) {
											if (result.getSwizFeedback().isResponse()) {

												SC.say("SUCCESS", result.getSwizFeedback().getMessage());

												getView().getFinancialYearPane().getFinancialYearListgrid()
														.addRecordsToGrid(result.getFinancialYears());

											} else {
												SC.warn("ERROR", result.getSwizFeedback().getMessage());
											}

										} else {
											SC.say("ERROR", "Unknow error");
										}

									}
								});
					}

				}
			});
		} else {
			SC.warn("ERROR", "ERROR: Please select record to delete");
		}

	}

	private void deactivateFinancialYear() {

		if (getView().getFinancialYearPane().getFinancialYearListgrid().anySelected()) {
			SC.ask("Confirm", "Are you sure you want to deactivate the selected record?", new BooleanCallback() {

				public void execute(Boolean value) {

					if (value) {
						FinancialYear financialYear = new FinancialYear();

						financialYear.setId(getView().getFinancialYearPane().getFinancialYearListgrid()
								.getSelectedRecord().getAttribute(FinancialYearListgrid.ID));

						SC.showPrompt("", "", new SwizimaLoader());

						dispatcher.execute(new SysAction(PAConstant.DEACTIVATE_FinancialYear, financialYear),
								new AsyncCallback<SysResult>() {
									public void onFailure(Throwable caught) {
										System.out.println(caught.getMessage());
									}

									public void onSuccess(SysResult result) {

										SC.clearPrompt();

										if (result != null) {
											if (result.getSwizFeedback().isResponse()) {

												SC.say("SUCCESS", result.getSwizFeedback().getMessage());

												getView().getFinancialYearPane().getFinancialYearListgrid()
														.addRecordsToGrid(result.getFinancialYears());

											} else {
												SC.warn("ERROR", result.getSwizFeedback().getMessage());
											}

										} else {
											SC.say("ERROR", "Unknow error");
										}

									}
								});
					}

				}
			});
		} else {
			SC.warn("ERROR", "ERROR: Please select record to delete");
		}

	}

}
