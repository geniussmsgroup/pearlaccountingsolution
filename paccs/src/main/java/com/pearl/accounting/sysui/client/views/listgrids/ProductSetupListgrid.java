package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.ProductSetup;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class ProductSetupListgrid extends SuperListGrid {
	
	
	
	public static String ID= "id";
	public static String pid = "productId";

	public static String pd = "productDetails";
	public static String R = "Remarks";
	  
	public ProductSetupListgrid() {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField productId = new ListGridField(pid, "ProductId");
		ListGridField productDetails = new ListGridField(pd, "ProductDetails");
		
		ListGridField Remarks = new ListGridField(R, "Remarks");
		 
	

		this.setFields(id,productId,productDetails,Remarks);

	}

	public ListGridRecord addRowData(ProductSetup account) {
		ListGridRecord record = new ListGridRecord(); 
		record.setAttribute(ID, account.getProductId());
		record.setAttribute(pid, account.getProductId());
		record.setAttribute(pd, account.getProductDetails());
		record.setAttribute(R, account.getRemarks());

		 

		return record;
	}

	public void addRecordsToGrid(List<ProductSetup> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (ProductSetup item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
	}


}
