package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.google.gwt.i18n.client.NumberFormat;
import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysmodel.LoanRepayment;
import com.pearl.accounting.sysmodel.RepaymentSchedule;
import com.pearl.accounting.sysui.client.utils.PAManager;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;

public class LoanRepaymentListgrid extends SuperListGrid {
	public static String ID = "id";

	public static String TotalAmount="totalAmount";

	public static String DepositedAmount = "DepositedAmount";
	public static String AssessmentPeriodId = "AssessmentPeriodId";
	public static String AssessmentPeriod = "AssessmentPeriod";
	public static String FinancialYearId = "FinancialYearId";
	public static String FinancialYear = "FinancialYearId";
	public static String DepositDate = "DepositDate";
	public static String ReceiveingAccountId = "ReceiveingAccountId";
	public static String ReceiveingAccount = "ReceiveingAccount";
	public static String PaymentDetails = "PaymentDetails";
	public static String Remarks = "Remarks";
	public static String ExchangeRate = "ExchangeRate";
	public static String LoanBalance = "LoanBalance";
	public static String TotalLoanAmount = "totalLoanAmount";

	
	NumberFormat nf = NumberFormat.getFormat("#,##0.00");

	public LoanRepaymentListgrid() {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true); 
		
		ListGridField totalAmount = new ListGridField(TotalAmount, "");
		totalAmount.setShowGridSummary(true);
		
		totalAmount.setSummaryFunction(new SummaryFunction() {
			public Object getSummaryValue(Record[] records, ListGridField field) {

				long totalAmount = 0;
				for (Record record : records) {

					totalAmount += Float
							.parseFloat(PAManager.getInstance().unformatCash(record.getAttribute(TotalAmount)));

				}
				return "" + PAManager.getInstance().formatCash(totalAmount);
			}
		});
		
		ListGridField depositedAmount = new ListGridField(DepositedAmount, "Amount deposited");
		depositedAmount.setShowGridSummary(true);
		
		depositedAmount.setSummaryFunction(new SummaryFunction() {
			public Object getSummaryValue(Record[] records, ListGridField field) {

				long totalAmount = 0;
				for (Record record : records) {

					totalAmount += Float
							.parseFloat(PAManager.getInstance().unformatCash(record.getAttribute(DepositedAmount)));

				}
				return "" + PAManager.getInstance().formatCash(totalAmount);
			}
		});

		
		ListGridField Loanbal = new ListGridField(LoanBalance, "Loan Balance");
		
		ListGridField receiveingAccountId = new ListGridField(ReceiveingAccountId, "Receiveing Account Id");
		receiveingAccountId.setHidden(true);
		
		ListGridField receiveingAccount = new ListGridField(ReceiveingAccount, "Receiveing Account");
		ListGridField depositDate = new ListGridField(DepositDate, "Deposit Date");

		ListGridField assessmentPeriodId = new ListGridField(AssessmentPeriodId, "PeriodId");
		assessmentPeriodId.setHidden(true);
		
		ListGridField assessmentPeriod = new ListGridField(AssessmentPeriod, "Period");
		
		ListGridField financialYearId = new ListGridField(FinancialYearId, "YearId");
		financialYearId.setHidden(true);
		
		ListGridField financialYear = new ListGridField(FinancialYear, "Year");

		ListGridField paymentDetails = new ListGridField(PaymentDetails, "Payment Details");
		ListGridField remarks = new ListGridField(Remarks, "Remarks");
		ListGridField exchangeRate = new ListGridField(ExchangeRate, "Ex.Rate");
		ListGridField totalamt = new ListGridField(TotalLoanAmount, "Total Loan");


		this.setFields(id, depositedAmount,totalamt, Loanbal, receiveingAccountId,
				receiveingAccount, depositDate, paymentDetails, remarks, exchangeRate, assessmentPeriodId,
				assessmentPeriod, financialYearId, financialYear);
		this.setShowGridSummary(true);

	}

	public ListGridRecord addRowData(LoanRepayment deposit) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute(ID, deposit.getId());
		record.setAttribute(LoanBalance, deposit.getBalance());
		record.setAttribute(TotalLoanAmount, deposit.getTotalAmount());


 
		record.setAttribute(DepositedAmount, nf.format(deposit.getDepositedAmount()));

		//if (deposit.getReceiveingAccount() != null) {
			record.setAttribute(ReceiveingAccountId, deposit.getReceiveingAccount().getSysid());
			record.setAttribute(ReceiveingAccount, deposit.getReceiveingAccount().getAccountName());
		//}

		record.setAttribute(DepositDate, deposit.getDepositDate());

			//record.setAttribute(AssessmentPeriodId, deposit.getAssessmentPeriod().getId());
			record.setAttribute(AssessmentPeriod, deposit.getPeriod());
		

	//	if (deposit.getYear() != null) {

			//record.setAttribute(FinancialYearId, deposit.getFinancialYear().getId());
			record.setAttribute(FinancialYear, deposit.getYear());
	//	}

//		LoanRepaymentListgrid grd = new LoanRepaymentListgrid();
//		RepaymentScheduleListgrid slg1 = new RepaymentScheduleListgrid();
		
		record.setAttribute(PaymentDetails, deposit.getPaymentDetails());
		record.setAttribute(Remarks, deposit.getRemarks());
		record.setAttribute(ExchangeRate, deposit.getExchangeRate());
//		record.setAttribute(LoanBalance, getBal(grd, slg1));
//		record.setAttribute(TotalAmount,  nf.format(schedule.getTotalAmount()));


		return record;
	}
//	private float getBal(LoanRepaymentListgrid grd,RepaymentScheduleListgrid slg) {
//	
//		float z = 0;
//		for (ListGridRecord record : grd.getRecords()) {
//
//				float x = record.getAttributeAsFloat(LoanRepaymentListgrid.DepositedAmount);
//				float y = record.getAttributeAsFloat(RepaymentScheduleListgrid.TotalAmount);
//				z=x-y;
//		}
//		
//		return z;
//		
//		
//		
//	}
//	
	public void addRecordsToGrid(List<LoanRepayment> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (LoanRepayment item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
	}

}
