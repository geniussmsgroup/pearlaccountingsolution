package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.google.gwt.i18n.client.NumberFormat;
import com.pearl.accounting.sysmodel.TransGL;
import com.pearl.accounting.sysui.client.utils.PAManager;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.grid.HeaderSpan;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;

public class TransGLListgrid extends SuperListGrid{
//	public class GeneralLedgerListgrid extends SuperListGrid {

		public static String ID = "id";

		public static String FinancialYear = "financialYear";
		public static String AssessmentPeriod = "assessmentPeriod";

		public static String TransationDate = "transationDate";
		public static String PostReferenceNo = "postReferenceNo";
		public static String Account = "account";
		public static String AccountCode = "accountCode";
		public static String TransactionType = "transactionType";
	    public static String Amount ="amount";
		public static String PaymentDetails = "paymentDetails";
		public static String Remarks = "remarks";
		public static String ProductType = "productType";
		
		NumberFormat nf = NumberFormat.getFormat("#,##0.00");

		public TransGLListgrid() {

			super();
			ListGridField id = new ListGridField(ID, "Id");
			id.setHidden(true);

			ListGridField financialYear = new ListGridField(FinancialYear, "Year");
			ListGridField assessmentPeriod = new ListGridField(AssessmentPeriod, "Period");

			ListGridField transationDate = new ListGridField(TransationDate, "Date");
			ListGridField amount = new ListGridField(Amount, "Amount");

			ListGridField postReferenceNo = new ListGridField(PostReferenceNo, "Ref.");
			postReferenceNo.setHidden(true);
			
			ListGridField account = new ListGridField(Account, "Account");
			ListGridField accountCode = new ListGridField(AccountCode, "Acc. Code");

			ListGridField transactionType = new ListGridField(TransactionType, "Type");
			
//			ListGridField transactionDebt = new ListGridField(TransactionDebt, "Debt");
//			transactionDebt.setShowGridSummary(true);
//
//			transactionDebt.setSummaryFunction(new SummaryFunction() {
//				public Object getSummaryValue(Record[] records, ListGridField field) {
//
//					long totalAmount = 0;
//					for (Record record : records) {
//
//						totalAmount += Float
//								.parseFloat(PAManager.getInstance().unformatCash(record.getAttribute(TransactionDebt)));
//
//					}
//					return "" + PAManager.getInstance().formatCash(totalAmount);
//				}
//			});
//			
//			ListGridField transactionCredit = new ListGridField(TransactionCredit, "Credit");
//			transactionCredit.setShowGridSummary(true);
//
//			transactionCredit.setSummaryFunction(new SummaryFunction() {
//				public Object getSummaryValue(Record[] records, ListGridField field) {
//
//					long totalAmount = 0;
//					for (Record record : records) {
//
//						totalAmount += Float
//								.parseFloat(PAManager.getInstance().unformatCash(record.getAttribute(TransactionCredit)));
//
//					}
//					return "" + PAManager.getInstance().formatCash(totalAmount);
//				}
//			});
//
//			ListGridField balanceDebt = new ListGridField(BalanceDebt, "Debt");
//			balanceDebt.setShowGridSummary(true);
//			balanceDebt.setHidden(true);
//
//			balanceDebt.setSummaryFunction(new SummaryFunction() {
//				public Object getSummaryValue(Record[] records, ListGridField field) {
//
//					long totalAmount = 0;
//					for (Record record : records) {
//
//						totalAmount += Float
//								.parseFloat(PAManager.getInstance().unformatCash(record.getAttribute(BalanceDebt)));
//
//					}
//					return "" + PAManager.getInstance().formatCash(totalAmount);
//				}
//			});
//			
//			
//			ListGridField balanceCredit = new ListGridField(BalanceCredit, "Credit");
//			balanceCredit.setShowGridSummary(true);
//			balanceCredit.setHidden(true);
//
//			balanceCredit.setSummaryFunction(new SummaryFunction() {
//				public Object getSummaryValue(Record[] records, ListGridField field) {
//
//					long totalAmount = 0;
//					for (Record record : records) {
//
//						totalAmount += Float
//								.parseFloat(PAManager.getInstance().unformatCash(record.getAttribute(BalanceCredit)));
//
//					}
//					return "" + PAManager.getInstance().formatCash(totalAmount);
//				}
//			});

			ListGridField paymentDetails = new ListGridField(PaymentDetails, "Payment Details");

			ListGridField remarks = new ListGridField(Remarks, "Remarks");
			ListGridField productType = new ListGridField(ProductType, "product");
			productType.setHidden(true);

			this.setFields(id, financialYear, assessmentPeriod, transationDate, amount, postReferenceNo,
					account, accountCode, transactionType,
					paymentDetails, remarks, productType);
			
			
			
			this.setLeaveHeaderMenuButtonSpace(false);
			this.setWrapHeaderTitles(true);
			this.setHeaderHeight(55);
			
			this.setCanExpandRecords(true);
			this.setShowGridSummary(true);

		}

		public ListGridRecord addRowData(TransGL ledger) {
			ListGridRecord record = new ListGridRecord();
			record.setAttribute(ID, ledger.getEPOS());
		record.setAttribute(TransationDate, ledger.getTDATE());

			if (ledger.getACODE() != null) {
				record.setAttribute(Account, ledger.getACODE());
				record.setAttribute(AccountCode, ledger.getACODE());
			}

			if (ledger.getTTYPE()!= null) {
				record.setAttribute(TransactionType, ledger.getTTYPE().getTransactionType());
			}

			record.setAttribute(Amount, nf.format(ledger.getAMOUNT()));

			record.setAttribute(PaymentDetails, ledger.getPAYDET());
			record.setAttribute(Remarks, ledger.getREMARKS());


			return record;
		}

		public void addRecordsToGrid(List<TransGL> list) {
			ListGridRecord[] records = new ListGridRecord[list.size()];
			int row = 0;
			for (TransGL item : list) {
				records[row] = addRowData(item);
				row++;
			}
			this.setData(records);
		}

	}
