package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.My;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class MyListgrid extends SuperListGrid {
	public static String ID = "id";
	public static String CODE = "code";
	public static String NAME = "accountName";
	
	public static String AccountType = "accountType";
	public static String AccountDefinition = "AccountDefinition";
	public static String TrackPData = "trackPData";
	  
	public MyListgrid() {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField code = new ListGridField(CODE, "Code");
		ListGridField accountName = new ListGridField(NAME, "Account name");
		
		ListGridField accountType = new ListGridField(AccountType, "Account type");
		 
		ListGridField accountDefinition = new ListGridField(AccountDefinition, "Income/Expenditure");
		
		ListGridField trackPData = new ListGridField(TrackPData, "Track personal data");
		  

		this.setFields(id, code,accountName,accountType, accountDefinition, trackPData);

	}

	public ListGridRecord addRowData(My my) {
		ListGridRecord record = new ListGridRecord(); 
		record.setAttribute(ID, my.getId());
		record.setAttribute(CODE, my.getAccountCode());
		record.setAttribute(NAME, my.getAccountName());
		
		
		if(my.getAccountType()!=null){
			record.setAttribute(AccountType, my.getAccountType().getAccountType());
		}
		
		if(my.getAccountDefinition()!=null){
			record.setAttribute(AccountDefinition, my.getAccountDefinition().getDefinition());
		}
		
		if(my.isTrackPData()){
			record.setAttribute(TrackPData, "Yes");
		}else{
			record.setAttribute(TrackPData, "No");
		}
		
		 

		return record;
	}

	public void addRecordsToGrid(List<My> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (My my : list) {
			records[row] = addRowData(my);
			row++;
		}
		this.setData(records);
	}


}
