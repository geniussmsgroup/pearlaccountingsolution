package com.pearl.accounting.sysui.server;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.gwtplatform.dispatch.server.actionvalidator.ActionValidator;
import com.gwtplatform.dispatch.server.spring.HandlerModule;
import com.gwtplatform.dispatch.server.spring.actionvalidator.DefaultActionValidator;
import com.gwtplatform.dispatch.server.spring.configuration.DefaultModule;
import com.pearl.accounting.sysui.shared.SysAction;

@Configuration
@Import(DefaultModule.class)
public class ServerModule extends HandlerModule {
	// Required
	public ServerModule() {

	}

	// Required
	@Bean
	public ActionValidator getDefaultActionValidator() {
		return new DefaultActionValidator();
	}

	@Override
	protected void configureHandlers() {

		bindHandler(SysAction.class, SysActionHandler.class);

	}

	@Bean
	public SysActionHandler getRmaeActionHandler() {
		return new SysActionHandler();
	}

}
