package com.pearl.accounting.sysui.client.views.windows;

import com.pearl.accounting.sysui.client.views.widgets.ComboBox;
import com.pearl.accounting.sysui.client.views.widgets.TextField;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class AccountSWindow extends Window {

	private TextField accountCode;
	private TextField accountName;
	private TextField periodstartamount;
	private TextField CurrencyCd;
	private TextField cbal;

	private ComboBox accountType;
	private ComboBox category;
	private ComboBox productType;
	private ComboBox AnalReq;
	//private ComboBox Pdata;

	//private ComboBox account_type;

	private IButton saveButton;

	public AccountSWindow() {
		super();
		accountCode = new TextField();
		accountCode.setTitle("Account Code");
		cbal = new TextField();
		cbal.setTitle("carried balance");
		periodstartamount = new TextField();
		periodstartamount.setTitle("periodstartamount");

//		account_type = new ComboBox();
//		account_type.setTitle("Account Type");

		accountName = new TextField();
		accountName.setTitle("Account Name");

		accountType = new ComboBox();
		accountType.setTitle("Account Type");

		category = new ComboBox();
		category.setTitle("Account Category");

//		tackable = new ComboBox();
//		tackable.setTitle("Track personal data?");

		productType = new ComboBox();
		productType.setTitle("Product Type");

		AnalReq = new ComboBox();
		AnalReq.setTitle("AnalReq");
	//	Pdata = new ComboBox();
	//	Pdata.setTitle("Product DATA");
		CurrencyCd = new TextField();
		CurrencyCd.setTitle("CurrencyCd");

		saveButton = new IButton("Save");

		DynamicForm form = new DynamicForm();
		form.setFields(accountCode, accountName, accountType, category, CurrencyCd, productType, AnalReq, periodstartamount, cbal);
		form.setWrapItemTitles(true);
		form.setMargin(10);

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(saveButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(form);
		layout.addMember(buttonLayout);

		layout.setMargin(10);
		this.addItem(layout);
		this.setWidth("60%");
		this.setHeight("60%");
		this.setAutoCenter(true);
		this.setTitle("Accounts");
		this.setIsModal(true);
		this.setShowModalMask(true);

	}

	public TextField getAccountCode() {
		return accountCode;
	}

	public TextField getAccountName() {
		return accountName;
	}

	public ComboBox getAccountType() {
		return accountType;
	}

	public ComboBox getCategory() {
		return category;
	}

	public TextField getCurrencyCd() {
		return CurrencyCd;
	}

	public ComboBox getProductType() {
		return productType;
	}

	public ComboBox getAnalReq() {
		return AnalReq;
	}

//	public ComboBox getPdata() {
//		return Pdata;
//	}

	public IButton getSaveButton() {
		return saveButton;
	}

	public TextField getCbal() {
		return cbal;
	}

//	public ComboBox getAccount_type() {
//		return account_type;
//	}

	public TextField getPeriodstartamount() {
		return periodstartamount;
	}

}
