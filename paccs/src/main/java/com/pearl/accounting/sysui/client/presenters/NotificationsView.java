package com.pearl.accounting.sysui.client.presenters;

import com.gwtplatform.mvp.client.ViewImpl;
import com.pearl.accounting.sysui.client.views.panes.TasksStatisticsPane;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet; 

public class NotificationsView extends ViewImpl implements NotificationsPresenter.MyView {

	private static final String DEFAULT_MARGIN = "0px";
	private VLayout panel;
	private TasksStatisticsPane tasksStatisticsPane;

	@Inject
	public NotificationsView() {
		panel = new VLayout();
		tasksStatisticsPane = new TasksStatisticsPane(); 

		Tab statisticsPaneTab = new Tab();
		statisticsPaneTab.setPane(tasksStatisticsPane);
		statisticsPaneTab.setTitle("Statistics"); 
		 
		TabSet tabSet = new TabSet();
		tabSet.addTab(statisticsPaneTab); 

		tabSet.setMargin(0);
		tabSet.setPadding(0);
		
		panel.addMember(tabSet);
		//panel.setMembers(tasksStatisticsPane);
		panel.setWidth100();
		panel.setHeight("90%");
		Window.enableScrolling(false);
		Window.setMargin(DEFAULT_MARGIN);

	}

	public Widget asWidget() {
		return panel;
	}

	public TasksStatisticsPane getTasksStatisticsPane() {
		return tasksStatisticsPane;
	}

}
