package com.pearl.accounting.sysui.client;
 
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.gwtplatform.mvp.client.DelayedBindRegistry;
import com.pearl.accounting.sysui.client.gin.ClientGinjector;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class AccSys implements EntryPoint {
	
	private final ClientGinjector ginjector = GWT.create(ClientGinjector.class);

	public void onModuleLoad() {
		
		DelayedBindRegistry.bind(ginjector);

		ginjector.getPlaceManager().revealCurrentPlace();
	}}
