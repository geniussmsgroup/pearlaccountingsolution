package com.pearl.accounting.sysui.client.views.panes;
 
import com.pearl.accounting.sysui.client.views.listgrids.CustomerAccountListgrid;
import com.pearl.accounting.sysui.client.views.widgets.ControlsPane;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class CustomerAccountPane  extends VLayout {

	private ControlsPane controlsPane;

	private CustomerAccountListgrid customerAccountListgrid;

	private IButton addButton;
	private IButton editButton;
	private IButton deleteButton;
	private IButton loadButton;
 

	public CustomerAccountPane() {

		super();
		Label header = new Label();
		header.setStyleName("crm-ContextArea-Header-Label");
		header.setContents("Customer Accounts");
		header.setWidth("100%");
		header.setAutoHeight();
		header.setMargin(10);
		header.setAlign(Alignment.LEFT);

		controlsPane = new ControlsPane();

		customerAccountListgrid = new CustomerAccountListgrid();

		addButton = new IButton("New");
		editButton = new IButton("Edit");
		deleteButton = new IButton("Delete");
		loadButton = new IButton("Refresh");

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(addButton, editButton, deleteButton,loadButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(10);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(controlsPane);
		layout.addMember(header);
		layout.addMember(customerAccountListgrid);
		layout.addMember(buttonLayout);
		this.addMember(layout);

	}


	public ControlsPane getControlsPane() {
		return controlsPane;
	}


	public CustomerAccountListgrid getCustomerAccountListgrid() {
		return customerAccountListgrid;
	}


	public IButton getAddButton() {
		return addButton;
	}


	public IButton getEditButton() {
		return editButton;
	}


	public IButton getDeleteButton() {
		return deleteButton;
	}


	public IButton getLoadButton() {
		return loadButton;
	}

	
	

}
