package com.pearl.accounting.sysui.client.data;

import com.pearl.accounting.sysui.client.place.NameTokens;
import com.smartgwt.client.widgets.grid.ListGridRecord; 

public class AccountTransactionData { 

	private static ListGridRecord[] records;

	public static ListGridRecord[] getRecords() {
		if (records == null) {
			records = getNewRecords();

		}
		return records;

	}

	public static ListGridRecord createRecord(String pk, String icon, String name) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute("pk", pk);
		record.setAttribute("icon", icon);
		record.setAttribute("name", name);
		return record;
	}

	public static ListGridRecord[] getNewRecords() {

		return new ListGridRecord[] {
//				createRecord("", "application_form",NameTokens.notifications),
				createRecord("", "application_form",NameTokens.cashbookpayments),
				createRecord("", "application_form",NameTokens.cashbookreceipts),
				createRecord("", "application_form",NameTokens.multijournal),
				//createRecord("", "application_form","Accounts Reports"),
				//createRecord("", "application_form","Bank Reconciliation"), 
				createRecord("", "application_form",NameTokens.generalledger),
				createRecord("", "application_form",NameTokens.DayComputeinterest),
				createRecord("", "application_form",NameTokens.TransGL),
				createRecord("", "application_form",NameTokens.TransML),
			//	createRecord("", "application_form",NameTokens.savingswithdrawals),

				//createRecord("", "application_form",NameTokens.),

				 
				
		};

	}
	
	public static ListGridRecord[] getPatrnerRecords() {

		return new ListGridRecord[] {
				createRecord("", "application_form",NameTokens.notifications)
				 
				
		};

	}

}
