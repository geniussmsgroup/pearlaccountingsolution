//package com.pearl.accounting.sysui.client.presenters;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.LinkedHashMap;
//import java.util.List;
//
//import com.google.gwt.user.client.Window;
//import com.google.gwt.user.client.rpc.AsyncCallback;
//import com.google.inject.Inject;
//import com.google.web.bindery.event.shared.EventBus;
//import com.gwtplatform.dispatch.shared.DispatchAsync;
//import com.gwtplatform.mvp.client.Presenter;
//import com.gwtplatform.mvp.client.View;
//import com.gwtplatform.mvp.client.annotations.NameToken;
//import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
//import com.gwtplatform.mvp.client.proxy.PlaceManager;
//import com.gwtplatform.mvp.client.proxy.ProxyPlace;
//import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
//import com.pearl.accounting.sysmodel.Account;
//import com.pearl.accounting.sysmodel.AssessmentPeriod;
//import com.pearl.accounting.sysmodel.CashBookPayment;
//import com.pearl.accounting.sysmodel.CashBookPaymentDetail;
//import com.pearl.accounting.sysmodel.CustomerAccount;
//import com.pearl.accounting.sysmodel.DataSourceScreen;
//import com.pearl.accounting.sysmodel.FinancialYear;
//import com.pearl.accounting.sysmodel.Loan;
//import com.pearl.accounting.sysmodel.LoanRepayment2;
//import com.pearl.accounting.sysmodel.Status;
//import com.pearl.accounting.sysmodel.Theadp;
//import com.pearl.accounting.sysmodel.Withdrawal;
//import com.pearl.accounting.sysui.client.place.NameTokens;
//import com.pearl.accounting.sysui.client.utils.PAConstant;
//import com.pearl.accounting.sysui.client.utils.PAManager;
//import com.pearl.accounting.sysui.client.views.listgrids.CashBookPaymentDetailListgrid;
//import com.pearl.accounting.sysui.client.views.listgrids.CashBookPaymentListgrid;
//import com.pearl.accounting.sysui.client.views.listgrids.WithdrawalListgrid;
//import com.pearl.accounting.sysui.client.views.panes.CashBookPaymentPane;
//import com.pearl.accounting.sysui.client.views.panes.CashBookPaymentWindow;
//import com.pearl.accounting.sysui.client.views.panes.LoanRepayment2Pane;
//import com.pearl.accounting.sysui.client.views.widgets.SwizimaLoader;
//import com.pearl.accounting.sysui.client.views.windows.LoanRepayment2Window;
//import com.pearl.accounting.sysui.shared.SysAction;
//import com.pearl.accounting.sysui.shared.SysResult;
//import com.smartgwt.client.util.BooleanCallback;
//import com.smartgwt.client.util.SC;
//import com.smartgwt.client.widgets.events.ClickEvent;
//import com.smartgwt.client.widgets.events.ClickHandler;
//import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;
//import com.smartgwt.client.widgets.form.fields.events.ChangeHandler;
//import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
//import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
//import com.smartgwt.client.widgets.form.fields.events.EditorExitEvent;
//import com.smartgwt.client.widgets.form.fields.events.EditorExitHandler;
//import com.smartgwt.client.widgets.form.fields.events.FocusEvent;
//import com.smartgwt.client.widgets.form.fields.events.FocusHandler;
//import com.smartgwt.client.widgets.grid.ListGridRecord;
//import com.smartgwt.client.widgets.grid.events.RecordExpandEvent;
//import com.smartgwt.client.widgets.grid.events.RecordExpandHandler;
//
//public class LoanRepPresenter{
//		//extends Presenter<LoanRepayment2Presenter.MyView, LoanRepayment2Presenter.MyProxy> {
//
//	private final PlaceManager placeManager;
//	private final DispatchAsync dispatcher;
//
//	public interface MyView extends View {
//
//		public LoanRepayment2Pane getLoanRepayment2Pane();
//
//	}
//
//	@ProxyCodeSplit
//	@NameToken(NameTokens.loanRepayment2)
//	public interface MyProxy extends ProxyPlace<LoanRepayment2Presenter> {
//	}
//
//	@Inject
//	public LoanRepayment2Presenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
//			final DispatchAsync dispatcher, final PlaceManager placeManager) {
//		super(eventBus, view, proxy);
//
//		this.dispatcher = dispatcher;
//		this.placeManager = placeManager;
//	}
//
//	@Override
//	protected void revealInParent() {
//		RevealContentEvent.fire(this, MainPagePresenter.TYPE_SetContextAreaContent, this);
//	}
//
//	@Override
//	protected void onBind() {
//		super.onBind();
//		onNewButtonClicked();
//		//onDeleteButtonClicked();
//		//load();
//		//onLoadButonClicked();
//
//	}
//
////	private void onLoadButtonClicked() {
////		getView().getLoanRepayment2Pane().getLoadButton().addClickHandler(new ClickHandler() {
////
////			public void onClick(ClickEvent event) {
////
////				loadDeposits();
////
////			}
////		});
////
////		getView().getLoanRepayment2Pane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {
////
////			public void onClick(ClickEvent event) {
////
////				loadDeposits();
////
////			}
////		});
////	}
////	private void onLoadButtonClicked() {
////		getView().getLoanRepayment2Pane().getLoadButton().addClickHandler(new ClickHandler() {
////
////			public void onClick(ClickEvent event) {
////				loadDeposits();
////
////			}
////		});
////
////		getView().getLoanRepayment2Pane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {
////
////			public void onClick(ClickEvent event) {
////
////				loadDeposits();
////
////			}
////		});
////	}
//	private void onNewButtonClicked() {
//		getView().getLoanRepayment2Pane().getAddButton().addClickHandler(new ClickHandler() {
//
//			public void onClick(ClickEvent event) {
//				create();
//
//			}
//		});
//
//		getView().getLoanRepayment2Pane().getControlsPane().getNewAssetButton().addClickHandler(new ClickHandler() {
//
//			public void onClick(ClickEvent event) {
//				create();
//
//			}
//		});
//	}
//
//	private void create() {
//		LoanRepayment2Window window = new LoanRepayment2Window();
//		loadAccounts(window);
//		loadPeriods(window);
//		loadFinancialYears(window);
//		loadCustomers(window);
//		total(window);
//		loanfind(window);
//		onSaveButtonClicked(window);
//
//		window.show();
//	}
//
//	private void loadPeriods(final LoanRepayment2Window window) {
//		SC.showPrompt("", "", new SwizimaLoader());
//
//		dispatcher.execute(new SysAction(PAConstant.GET_AssessmentPeriod), new AsyncCallback<SysResult>() {
//			public void onFailure(Throwable caught) {
//				System.out.println(caught.getMessage());
//			}
//
//			public void onSuccess(SysResult result) {
//
//				SC.clearPrompt();
//
//				if (result != null) {
//
//					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
//
//					for (AssessmentPeriod period : result.getAssessmentPeriods()) {
//
//						valueMap.put(period.getId(), period.getPeriodName());
//					}
//
//					window.getAssessmentPeriod().setValueMap(valueMap);
//
//				} else {
//					SC.say("ERROR", "Unknow error");
//				}
//
//			}
//		});
//
//	}
//
//	private void total(final LoanRepayment2Window window) {
//		window.getLoanrepay().addFocusHandler(new FocusHandler() {
//
//			@Override
//			public void onFocus(FocusEvent event) {
//				LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
//				String m = window.getLoanrepay().getValueAsString();
//				String a = window.getInterest_pay().getValueAsString();
//				float s = Float.parseFloat(m) + Float.parseFloat(a);
//				String ans = String.valueOf(s);
//				valueMap.put(ans, ans);
//				window.getTotal().setValue(s);
//
//			}
//		});
//	}
//
//	private void loanfind(final LoanRepayment2Window window) {
//		window.getCustomeraccount().addChangeHandler(new ChangeHandler() {
//
//			@Override
//			public void onChange(ChangeEvent event) {
//
//				dispatcher.execute(new SysAction(PAConstant.GET_LOAN), new AsyncCallback<SysResult>() {
//					public void onFailure(Throwable caught) {
//						System.out.println(caught.getMessage());
//					}
//
//					public void onSuccess(SysResult result) {
//						// TODO Auto-generated method stub
//						SC.clearPrompt();
//
//						if (result != null) {
//							// LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
//
//							String ft = null;
//							String name = null;
//							for (Loan loan : result.getLoans()) {
//
//								if (loan.getCustomerAccount().getId() == window.getCustomeraccount()
//										.getValueAsString()) {
//
//									ft = (loan.getLoanNumber());
//									name = (loan.getCustomerAccount().getAccountNo() + " "
//											+ loan.getCustomerAccount().getFirstName() + " "
//											+ loan.getCustomerAccount().getLastName());
//								} else {
//									SC.warn("has no loan id");
//								}
//
//							}
//							// window.getTick().setValueMap(valueMap);
//							window.getTick().setValue(ft);
//							window.getFl().setValue(name);
//						} else {
//							SC.say("ERROR", "Unknow error");
//						}
//					}
//				});
//			}
//		});
//	}
//
//	private void loadFinancialYears(final LoanRepayment2Window window) {
//		SC.showPrompt("", "", new SwizimaLoader());
//
//		dispatcher.execute(new SysAction(PAConstant.GET_FinancialYear), new AsyncCallback<SysResult>() {
//			public void onFailure(Throwable caught) {
//				System.out.println(caught.getMessage());
//			}
//
//			public void onSuccess(SysResult result) {
//
//				SC.clearPrompt();
//
//				if (result != null) {
//
//					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
//
//					for (FinancialYear financialYear : result.getFinancialYears()) {
//
//						valueMap.put(financialYear.getId(), financialYear.getFinancialYear());
//					}
//
//					window.getFinancialYear().setValueMap(valueMap);
//
//				} else {
//					SC.say("ERROR", "Unknow error");
//				}
//
//			}
//		});
//	}
//
//	private void loadAccounts(final LoanRepayment2Window window) {
//		SC.showPrompt("", "", new SwizimaLoader());
//
//		dispatcher.execute(new SysAction(PAConstant.GET_ACCOUNT), new AsyncCallback<SysResult>() {
//			public void onFailure(Throwable caught) {
//				System.out.println(caught.getMessage());
//			}
//
//			public void onSuccess(SysResult result) {
//
//				SC.clearPrompt();
//
//				if (result != null) {
//
//					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
//					for (Account account : result.getAccounts()) {
//						valueMap.put(account.getId(), account.getAccountName());
//					}
//
//					window.getReceiveingAccount().setValueMap(valueMap);
//
//				} else {
//					SC.say("ERROR", "Unknow error");
//				}
//
//			}
//		});
//	}
//
//	private void loadCustomers(final LoanRepayment2Window window) {
//		SC.showPrompt("", "", new SwizimaLoader());
//
//		dispatcher.execute(new SysAction(PAConstant.GET_customerAccount), new AsyncCallback<SysResult>() {
//			public void onFailure(Throwable caught) {
//				System.out.println(caught.getMessage());
//			}
//
//			public void onSuccess(SysResult result) {
//
//				SC.clearPrompt();
//
//				if (result != null) {
//
//					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
//
//					for (CustomerAccount account : result.getCustomerAccounts()) {
//						valueMap.put(account.getId(),
//								account.getAccountNo() + " " + account.getFirstName() + " " + account.getLastName());
//
//					}
//					window.getCustomeraccount().setValueMap(valueMap);
//				}
//				// else if(window.getCustomeraccount().getValueAsString()!=null) {
//
////	
//				else {
//					SC.say("ERROR", "Unknow error");
//				}
//
//			}
//		});
//
//	}
//
//	private void ClearForm(final LoanRepayment2Window window) {
//		window.getRemarks().clearValue();
//		window.getPaymentDetails().clearValue();
//		window.getReceiveingAccount().clearValue();
//		window.getDepositedAmount().clearValue();
//		window.getCustomeraccount().clearValue();
//		window.getInterest_pay().clearValue();
//	}
//
//	private boolean FormValid(final LoanRepayment2Window window) {
//		// if (window.getCustomeraccount().getValueAsString() != null) {
//		if (window.getDepositedAmount().getValueAsString() != null
//				|| window.getInterest_pay().getValueAsString() != null) {
//			if (window.getAssessmentPeriod().getValueAsString() != null) {
//				if (window.getReceiveingAccount().getValueAsString() != null) {
//					if (window.getCustomeraccount().getValueAsString() != null) {
////							
//						return true;
//					} else {
//						SC.warn("Error", "select client");
//
//					}
//				} else {
//					SC.warn("Error", "Please enter receiving account");
//
//				}
//
//			} else {
//				SC.warn("Error", "Please enter assessment period");
//			}
//
//		} else {
//			SC.warn("Error", "Please enter deposited amount or Interest Amount");
//
//		}
//
//		return false;
//
//	}
//
//	private void onSaveButtonClicked(final LoanRepayment2Window window) {
//		window.getSaveButton().addClickHandler(new ClickHandler() {
//
//			public void onClick(ClickEvent event) {
//
//				if (FormValid(window)) {
//					CustomerAccount customerAccount = new CustomerAccount();
//					customerAccount.setId(window.getCustomeraccount().getValueAsString());
//
//					float amtdeposited = Float.parseFloat(
//							PAManager.getInstance().unformatCash(window.getDepositedAmount().getValueAsString()));
//
//					float amtint = Float.parseFloat(
//							PAManager.getInstance().unformatCash(window.getInterest_pay().getValueAsString()));
//
//					AssessmentPeriod assessmentPeriod = new AssessmentPeriod();
//					assessmentPeriod.setId(window.getAssessmentPeriod().getValueAsString());
//
//					FinancialYear financialYear = new FinancialYear();
//					financialYear.setId(window.getFinancialYear().getValueAsString());
//
//					Date depositDate = window.getDepositDate().getValueAsDate();
//
//					Account receiveingAccount = new Account();
//					receiveingAccount.setId(window.getReceiveingAccount().getValueAsString());
//
//					String paymentDetails = window.getPaymentDetails().getValueAsString();
//					String remarks = window.getRemarks().getValueAsString();
//					// Loan loan=new Loan();
//
//					LoanRepayment2 periodicDeposit = new LoanRepayment2();
//
//					periodicDeposit.setAssessmentPeriod(assessmentPeriod);
//					periodicDeposit.setInterest(amtint);
//					periodicDeposit.setCustomerAccount(customerAccount);
//					periodicDeposit.setDateCreated(new Date());
//					periodicDeposit.setDateUpdated(new Date());
//					periodicDeposit.setDepositDate(depositDate);
//					periodicDeposit.setDepositedAmount(amtdeposited);
//					periodicDeposit.setLoannumber(window.getTick().getValueAsString());
//
//					periodicDeposit.setFinancialYear(financialYear);
//					periodicDeposit.setPaymentDetails(paymentDetails);
//					periodicDeposit.setReceiveingAccount(receiveingAccount);
//					periodicDeposit.setRemarks(remarks);
//					periodicDeposit.setStatus(Status.ACTIVE);
////					 if(periodicDeposit.getExchangeRate()==1) {
////							dispatcher.execute(new SysAction(PAConstant.GET_LOAN), new AsyncCallback<SysResult>() {
//                    Theadp        theadp = new Theadp();
//					theadp.setAmount(amtdeposited + amtint);
//					theadp.setTdates(depositDate);
//					theadp.setInterest(amtint);
//					theadp.setDataSourceScreen(DataSourceScreen.LoanRepayment);
//					theadp.setRemarks(remarks);
//					theadp.setVNO(paymentDetails);
//					theadp.setDebit(receiveingAccount);
//					theadp.setCustomerAccount(customerAccount);
//					theadp.setLoanaccount(window.getTick().getValueAsString());
//					theadp.setStatus(Status.ACTIVE);
//					theadp.setLoanNumber(window.getTick().getValueAsString());
//
//					SC.showPrompt("", "", new SwizimaLoader());
//
//					dispatcher.execute(new SysAction(PAConstant.SAVE_DOUBLEENTRY, theadp),
//							new AsyncCallback<SysResult>() {
//								public void onFailure(Throwable caught) {
//									System.out.println(caught.getMessage());
//								}
//
//								public void onSuccess(SysResult result) {
//
//									SC.clearPrompt();
//
//									if (result != null) {
//
//										if (result.getSwizFeedback().isResponse()) {
//											ClearForm(window);
//											SC.say("SUCCESS", result.getSwizFeedback().getMessage());
//
//										} else {
//											SC.warn("ERROR", result.getSwizFeedback().getMessage());
//										}
//
//									} else {
//										SC.say("ERROR", "Unknow error");
//									}
//
//								}
//							});
//
//					SC.showPrompt("", "", new SwizimaLoader());
//
//					dispatcher.execute(new SysAction(PAConstant.SAVE_LoanRepayment, periodicDeposit),
//							new AsyncCallback<SysResult>() {
//								public void onFailure(Throwable caught) {
//									System.out.println(caught.getMessage());
//								}
//
//								public void onSuccess(SysResult result) {
//									ClearForm(window);
//									SC.clearPrompt();
//
//									if (result != null) {
//
//										if (result.getSwizFeedback().isResponse()) {
//
//											SC.say("SUCCESS", result.getSwizFeedback().getMessage());
//
//											//getView().getLoanRepayment2Pane().getLoanRepayment2Listgrid()
//												//	.addRecordsToGrid(result.getLoanRepayment2s());
//										} else {
//											SC.warn("ERROR", result.getSwizFeedback().getMessage());
//										}
//									} else {
//										SC.say("ERROR", "Unknow error");
//									}
//								}
//							});
//				}
//			}
//		});
//	}
//	
////
////	private void onDeleteButtonClicked() {
////		getView().getLoanRepayment2Pane().getDeleteButton().addClickHandler(new ClickHandler() {
////
////			public void onClick(ClickEvent event) {
////				delete();
////
////			}
////		});
////		getView().getLoanRepayment2Pane().getControlsPane().getDeleteButton().addClickHandler(new ClickHandler() {
////
////			public void onClick(ClickEvent event) {
////				delete();
////
////			}
////		});
////	}
////
////	private void delete() {
////		if (getView().getLoanRepayment2Pane().getLoanRepayment2Listgrid().anySelected()) {
////			SC.say("Confirm", "Are you sure you want to delete the selected record?", new BooleanCallback() {
////
////				public void execute(Boolean value) {
////					if (value) {
////						LoanRepayment2 withdrawal = new LoanRepayment2();
////						withdrawal.setId(getView().getLoanRepayment2Pane().getLoanRepayment2Listgrid()
////								.getSelectedRecord().getAttribute(LoanRepayment2Listgrid.ID));
////
////						SC.showPrompt("", "", new SwizimaLoader());
////
////						dispatcher.execute(new SysAction(PAConstant.DELETE_LoanRepayment, withdrawal),
////								new AsyncCallback<SysResult>() {
////									public void onFailure(Throwable caught) {
////										System.out.println(caught.getMessage());
////									}
////
////									public void onSuccess(SysResult result) {
////
////										SC.clearPrompt();
////
////										if (result != null) {
////
////											if (result.getSwizFeedback().isResponse()) {
////
////												SC.say("SUCCESS", result.getSwizFeedback().getMessage());
////
////												getView().getLoanRepayment2Pane().getLoanRepayment2Listgrid()
////														.addRecordsToGrid(result.getLoanRepayment2s());
////
////											} else {
////												SC.warn("ERROR", result.getSwizFeedback().getMessage());
////											}
////
////										} else {
////											SC.say("ERROR", "Unknow error");
////										}
////
////									}
////								});
////					}
////
////				}
////			});
////		} else {
////			SC.say("ERROR", "Please select record to delete");
////		}
////	}
//////	private void onLoadButonClicked(){
//////		getView().getLoanRepayment2Pane().getLoadButton().addClickHandler(new ClickHandler() {
//////			
//////			public void onClick(ClickEvent event) {
//////				load();
//////				
//////			}
//////		});
//////		
//////		getView().getLoanRepayment2Pane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {
//////			
//////			public void onClick(ClickEvent event) {
//////				load();
//////				
//////			}
//////		});
//////	}
//////
//////	private void load() {
//////		SC.showPrompt("", "", new SwizimaLoader());
//////
//////		dispatcher.execute(new SysAction(PAConstant.GET_LoanRepayment), new AsyncCallback<SysResult>() {
//////			public void onFailure(Throwable caught) {
//////				System.out.println(caught.getMessage());
//////			}
//////
//////			public void onSuccess(SysResult result) {
//////
//////				SC.clearPrompt();
//////
//////				if (result != null) {
//////
//////					getView().getLoanRepayment2Pane().getLoanRepayment2Listgrid()
//////							.addRecordsToGrid(result.getLoanRepayment2s());
//////
//////				} else {
//////					SC.say("ERROR", "Unknow error");
//////				}
//////
//////			}
//////		});
//////	}
//
//}
//
//
