
package com.pearl.accounting.sysui.client.presenters;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.shared.DispatchAsync;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.AssessmentPeriod;
import com.pearl.accounting.sysmodel.CashBookReceipt;
import com.pearl.accounting.sysmodel.CashBookReceiptDetail;
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.DataSourceScreen;
import com.pearl.accounting.sysmodel.FinancialYear;
import com.pearl.accounting.sysmodel.Intervaltb;
import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysmodel.Multi;
import com.pearl.accounting.sysmodel.MultiDetail;
import com.pearl.accounting.sysmodel.MultiJournal;
import com.pearl.accounting.sysmodel.MultiJournalDetail;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.Theadp;
import com.pearl.accounting.sysmodel.Ttype;
import com.pearl.accounting.sysui.client.place.NameTokens;
import com.pearl.accounting.sysui.client.utils.PAConstant;
import com.pearl.accounting.sysui.client.utils.PAManager;
import com.pearl.accounting.sysui.client.views.listgrids.CashBookReceiptDetailListgrid;
import com.pearl.accounting.sysui.client.views.listgrids.CashBookReceiptListgrid;
import com.pearl.accounting.sysui.client.views.listgrids.MultiDetailListgrid;
import com.pearl.accounting.sysui.client.views.listgrids.MultiJournalDetailListgrid;
import com.pearl.accounting.sysui.client.views.listgrids.MultiJournalListgrid;
import com.pearl.accounting.sysui.client.views.listgrids.MultiListgrid;
import com.pearl.accounting.sysui.client.views.panes.CashBookReceiptPane;
import com.pearl.accounting.sysui.client.views.panes.MultiJournalPane;
import com.pearl.accounting.sysui.client.views.panes.MultiPane;
import com.pearl.accounting.sysui.client.views.widgets.SwizimaLoader;
import com.pearl.accounting.sysui.client.views.windows.CashBookPaymentWindow;
import com.pearl.accounting.sysui.client.views.windows.CashBookReceiptWindow;
import com.pearl.accounting.sysui.client.views.windows.MultiJournalWindow;
import com.pearl.accounting.sysui.client.views.windows.MultiWindow;
import com.pearl.accounting.sysui.shared.SysAction;
import com.pearl.accounting.sysui.shared.SysResult;
import com.smartgwt.client.docs.DateInputFormat;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.EditorExitEvent;
import com.smartgwt.client.widgets.form.fields.events.EditorExitHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordExpandEvent;
import com.smartgwt.client.widgets.grid.events.RecordExpandHandler;

public class MultiJournalPresenter extends Presenter<MultiJournalPresenter.MyView, MultiJournalPresenter.MyProxy> {

	private final PlaceManager placeManager;
	private final DispatchAsync dispatcher;

	public interface MyView extends View {
		public MultiJournalPane getMultiJournalPane();

	}

	@ProxyCodeSplit
	@NameToken(NameTokens.multijournal)
	public interface MyProxy extends ProxyPlace<MultiJournalPresenter> {
	}

	@Inject
	public MultiJournalPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
			final DispatchAsync dispatcher, final PlaceManager placeManager) {
		super(eventBus, view, proxy);

		this.dispatcher = dispatcher;
		this.placeManager = placeManager;
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this, MainPagePresenter.TYPE_SetContextAreaContent, this);
	}

	@Override
	protected void onBind() {
		super.onBind();

		onNewButtonClicked();

		onLoadButtonClicked();

		onRecordExpanded();

	}

	
	private void currentperiods(final MultiJournalWindow window) {
		dispatcher.execute(new SysAction(PAConstant.GET_CurrentPeriod), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {
				// TODO Auto-generated method stub
				if (result != null) {
//						
					for (Intervaltb period : result.getIntervaltbs()) {

							window.getCurrentperiod().setValue(period.getFINTNAME());
							//window.getYear().setValue(period.getYRCODE());
							window.getCurrentstartdate().setValue(period.getFSTART());
							
							window.getCurrentenddate().setValue(period.getFEND());
							//period.window.getPeriodcode().setValue(period.getFINTCODE());
					
				}}
			}
		});
	}
	
	
	//private void pickdP(final MultiJournalWindow window) {
//
//		window.getPaymentDate().addChangedHandler(new ChangedHandler() {
//			@Override
//			public void onChanged(ChangedEvent event) {
//				// TODO Auto-generated method stub
//				
//				dispatcher.execute(new SysAction(PAConstant.GET_Interval), new AsyncCallback<SysResult>() {
//					public void onFailure(Throwable caught) {
//						System.out.println(caught.getMessage());
//					}
//
//					public void onSuccess(SysResult result) {
//						// TODO Auto-generated method stub
//						if (result != null) {
//							//window.getTrial().setValue("trial");
//
////							
//							for(Intervaltb pr : result.getIntervaltbs()) {
//								final Date dt = window.getPaymentDate().getValueAsDate();
//								if(dt.before(pr.getFEND()) && dt.after(pr.getFSTART())) {
//									window.getAssessmentPeriod().setValue(pr.getFINTNAME());
//									window.getFinancialYear().setValue(pr.getFINTCODE());
//
//
//
//								}}}}});
//				
//				
//			}
//		});}

	private void onDateLoaded(final MultiJournalWindow window) {

		window.getPaymentDate().addChangedHandler(new ChangedHandler() {

			public void onChanged(ChangedEvent event) {

				dispatcher.execute(new SysAction(PAConstant.GET_Interval), new AsyncCallback<SysResult>() {
					public void onFailure(Throwable caught) {
						System.out.println(caught.getMessage());
					}

					public void onSuccess(SysResult result) {
						// TODO Auto-generated method stub
						if (result != null) {
//								
							for (Intervaltb period : result.getIntervaltbs()) {

								final Date dn = window.getPaymentDate().getValueAsDate();
								if (dn.before(period.getFEND()) && dn.after(period.getFSTART())) {
									window.getAssessmentPeriod().setValue(period.getFINTNAME());
									window.getFinancialYear().setValue(period.getYRCODE());
									window.getStartdate().setValue(period.getFSTART());
									window.getEnddate().setValue(period.getFEND());
									window.getPeriodcode().setValue(period.getFINTCODE());

								}
							}
						}
					}
				});
			}
		});
	}

//			@Override
//			public void onChanged(ChangedEvent event) {
//				// TODO Auto-generated method stub
//				
//				dispatcher.execute(new SysAction(PAConstant.GET_AssessmentPeriod), new AsyncCallback<SysResult>() {
//					public void onFailure(Throwable caught) {
//						System.out.println(caught.getMessage());
//					}
//
//					public void onSuccess(SysResult result) {
//						// TODO Auto-generated method stub
//						if (result != null) {
////							
//							for (AssessmentPeriod period : result.getAssessmentPeriods()) {	
//								
//								final Date dn = window.getPaymentDate().getValueAsDate();
//								if (dn.before(period.getEndDate()) && dn.after(period.getStartDate())) {
//									window.getAssessmentPeriod().setValue(period.getPeriodName());
//									 window.getFinancialYear().setValue(period.getFinancialYear());
//
//
//								}
//
//							}}}});}});
//				
//
//		

	private void onNewButtonClicked() {
		getView().getMultiJournalPane().getAddButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				addNewCashBookReceipt();
			}
		});

		getView().getMultiJournalPane().getControlsPane().getNewAssetButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				addNewCashBookReceipt();

			}
		});
	}

	private void addNewCashBookReceipt() {

		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_MULTI_JOURNAL_DETAILS_INPUT_DATA),
				new AsyncCallback<SysResult>() {
					public void onFailure(Throwable caught) {
						System.out.println(caught.getMessage());
					}

					public void onSuccess(SysResult result) {

						SC.clearPrompt();

						if (result != null) {

							MultiJournalWindow window = new MultiJournalWindow(result.getAccounts(),
									result.getCustomerAccounts(), result.getLoans());

							loadAccounts(window);
							//loadPeriods(window);
							// loadFinancialYears(window);
							addPaymentDetails(window);
							// pickdP(window);
							ondeletebuttonClicked(window);
							onDateLoaded(window);
							onSaveButtonClicked(window);
							currentperiods(window);
							
							window.show();

						} else {
							SC.say("ERROR", "Unknow error");
						}

					}
				});

	}

//	private void loadPeriods(final MultiJournalWindow window) {
//		SC.showPrompt("", "", new SwizimaLoader());
//
//		dispatcher.execute(new SysAction(PAConstant.GET_AssessmentPeriod), new AsyncCallback<SysResult>() {
//			public void onFailure(Throwable caught) {
//				System.out.println(caught.getMessage());
//			}
//
//			public void onSuccess(SysResult result) {
//
//				SC.clearPrompt();
//
//				if (result != null) {
//
//					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
//
//					for (AssessmentPeriod period : result.getAssessmentPeriods()) {
//
//						valueMap.put(period.getId(), period.getPeriodName());
//					}
//
//					window.getAssessmentPeriod().setValueMap(valueMap);
//
//				} else {
//					SC.say("ERROR", "Unknow error");
//				}
//
//			}
//		});
//
//	}

	private void loadFinancialYears(final MultiJournalWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_FinancialYear), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

					for (FinancialYear financialYear : result.getFinancialYears()) {

						valueMap.put(financialYear.getId(), financialYear.getFinancialYear());
					}

					window.getFinancialYear().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void loadAccounts(final MultiJournalWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_ACCOUNT), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
					for (Account account : result.getAccounts()) {
						valueMap.put(account.getSysid(), account.getAccountName());
					}

					window.getReceivingAccount().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void onputdefaulton(final MultiJournalWindow window) {
	      
		for (ListGridRecord record1 : window.getMultiJournalDetailListgrid().getRecords()) {
		//re
			//CustomerAccount customerAccount = new CustomerAccount();
//			Loan loan = new Loan();
//           
//			String name = record1.getAttribute(MultiJournalDetailListgrid.CustomerAccount);
//			String ln = record1.getAttribute(MultiJournalDetailListgrid.Loan);
//			customerAccount.setSysid(record1.getAttribute(MultiJournalDetailListgrid.CustomerAccount));
          
			if(record1.getAttribute(MultiJournalDetailListgrid.CustomerAccount)==null) {
		
				record1.setAttribute(MultiJournalDetailListgrid.CustomerAccount, "None");
            }
            ///loan.setLOANID(record1.getAttribute(MultiJournalDetailListgrid.Loan));
		}
		
	}
	
	
	
	
	private void ondeletebuttonClicked(final MultiJournalWindow window) {
		window.getDelete().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
//				MultiJournalDetail detail = new MultiJournalDetail();
				if (window.getMultiJournalDetailListgrid().anySelected()) {

					SC.ask("Are you sure you want to delete the record", new BooleanCallback() {

						public void execute(Boolean value) {

							if (value == true) {
								for (ListGridRecord record : window.getMultiJournalDetailListgrid()
										.getSelectedRecords()) {
  
									window.getMultiJournalDetailListgrid().removeData(record);
								}

							}

						}
					});

				} else {
					SC.say("ERROR", "select record to delete");
				}
			}
		});

	}

	private void addPaymentDetails(final MultiJournalWindow window) {
		window.getAddButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				MultiJournalDetail detail = new MultiJournalDetail();

				window.getMultiJournalDetailListgrid().addRecordToGrid(detail);

			}
		});

	}

	private void clearForm(final MultiJournalWindow window) {

		window.getPaidAmount().clearValue();
		window.getReceivingAccount().clearValue();
		window.getRemarks().clearValue();
		window.getPaymentDate().clearValue();
		window.getPaymentDetails().clearValue();
	}

	private boolean IsFormValid(final MultiJournalWindow window) {

		if (window.getFinancialYear().getValueAsString() != null) {

			if (window.getAssessmentPeriod().getValueAsString() != null) {

				if (window.getPaidAmount().getValueAsString() != null) {

					if (window.getRemarks().getValueAsString() != null) {

						if (window.getMultiJournalDetailListgrid().getRecords().length > 0) {
							for (ListGridRecord record1 : window.getMultiJournalDetailListgrid().getRecords()) {
								if ((record1.getAttributeAsString(MultiJournalDetailListgrid.TransactionType) == "Cr")
										|| (record1
												.getAttributeAsString(MultiJournalDetailListgrid.Account) == "Loan")) {

								}
							}

							float totaldebits = 0;
							float totalcredits = 0;
							for (ListGridRecord record : window.getMultiJournalDetailListgrid().getRecords()) {
								if (record.getAttributeAsString(MultiJournalDetailListgrid.TransactionType) == "D") {
									totaldebits += Float.parseFloat(
											record.getAttributeAsString(MultiJournalDetailListgrid.AmountPaid));

								} else if (record
										.getAttributeAsString(MultiJournalDetailListgrid.TransactionType) == "C") {
									totalcredits += Float.parseFloat(
											record.getAttributeAsString(MultiJournalDetailListgrid.AmountPaid));

								}

							}
							if (totaldebits == totalcredits) {

								float overalltotal = Float.parseFloat(window.getPaidAmount().getValueAsString());
								if ((totaldebits == overalltotal) && (totalcredits == overalltotal)) {
									return true;
								} else {
									SC.warn("ERROR", "Amount values do not match");

								}

							} else {
								SC.warn("ERROR", "The Credits are not equal to the Debits");
							}
						} else {
							SC.warn("ERROR", "add account details");
						}
					} else {
						SC.warn("ERROR", "Please enter Remarks");

					}

				} else {
					SC.warn("ERROR", "Please enter Amount");

				}
			} else {
				SC.warn("ERROR", "Please select Period");
			}
		} else {
			SC.warn("ERROR", "Please select Financial Year");

		}

		return false;
	}

	private void onSaveButtonClicked(final MultiJournalWindow window) {
		window.getSaveButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				if (IsFormValid(window)) {

					FinancialYear financialYear = new FinancialYear();
					financialYear.setId(window.getFinancialYear().getValueAsString());

					AssessmentPeriod assessmentPeriod = new AssessmentPeriod();
					assessmentPeriod.setId(window.getAssessmentPeriod().getValueAsString());

					float amountReceived = Float.parseFloat(
							PAManager.getInstance().unformatCash(window.getPaidAmount().getValueAsString()));

					Date paymentDate = window.getPaymentDate().getValueAsDate();

					/*
					 * CustomerAccount accountname = new CustomerAccount();
					 * accountname.setId(window.getAccountname().getValueAsString());
					 */

					Account receivingAccount = new Account();
					receivingAccount.setSysid(window.getReceivingAccount().getValueAsString());

					String remarks = window.getRemarks().getValueAsString();
					String receiptNumber = window.getPaymentDetails().getValueAsString();

					// String transactionNumber;

					MultiJournal multiJournal = new MultiJournal();

					multiJournal.setAmountPosted(amountReceived);
					multiJournal.setPeriod(window.getAssessmentPeriod().getValueAsString());
					// multiJournal.setAccountname(accountname);
					int year = Integer.parseInt(window.getFinancialYear().getValueAsString());

					// cashBookPayment.setCreatedBy(createdBy);
					multiJournal.setDateCreated(new Date());
					multiJournal.setDateUpdated(new Date());
					multiJournal.setYear(year);
					multiJournal.setPeriodcode(window.getPeriodcode().getValueAsString());
					// multiJournal.setTransactionAccount(receivingAccount);
					multiJournal.setTransactionDate(paymentDate);
					multiJournal.setRemarks(remarks);
					multiJournal.setStatus(Status.ACTIVE);
					// cashBookPayment.setTransactionNumber(transactionNumber);
					// cashBookPayment.setUpdatedBy(updatedBy);
					multiJournal.setTransactionDetails(receiptNumber);

					
					List<MultiJournalDetail> journalDetails = new ArrayList<MultiJournalDetail>();
					for (ListGridRecord record1 : window.getMultiJournalDetailListgrid().getRecords()) {
						CustomerAccount customerAccount = new CustomerAccount();
						Loan loan = new Loan();

						String name = record1.getAttribute(MultiJournalDetailListgrid.CustomerAccount);
						String ln = record1.getAttribute(MultiJournalDetailListgrid.Loan);
						customerAccount.setSysid(record1.getAttribute(MultiJournalDetailListgrid.CustomerAccount));

						loan.setLOANID(record1.getAttribute(MultiJournalDetailListgrid.Loan));
//						
						Account payingAccount = new Account();
						payingAccount.setSysid(record1.getAttribute(MultiJournalDetailListgrid.Account));

						float paidAmount = Float.parseFloat(PAManager.getInstance()
								.unformatCash(record1.getAttribute(MultiJournalDetailListgrid.AmountPaid)));
						Date payDate = window.getPaymentDate().getValueAsDate();

						String details = record1.getAttribute(MultiJournalDetailListgrid.Remarks);
						MultiJournalDetail journalDetail = new MultiJournalDetail();

						journalDetail.setMultiJournal(multiJournal);
						journalDetail.setDateCreated(new Date());
						journalDetail.setDateUpdated(new Date());
						journalDetail.setTransactionDate(payDate);
						journalDetail.setAmountPosted(paidAmount);
						journalDetail.setTransactionAccount(payingAccount);
						journalDetail.setRemarks(details);
						journalDetail.setStatus(Status.ACTIVE);
						journalDetail.setPeriodcode(window.getPeriodcode().getValueAsString());
						journalDetail.setTtype(Ttype.getTransactionType(
								(record1.getAttribute(MultiJournalDetailListgrid.TransactionType))));
						if (record1.getAttribute(MultiJournalDetailListgrid.Trackpdata) == "Yes") {
							journalDetail.setCustomerAccount(customerAccount);
							journalDetail.setLoan(loan);
						} else if (record1.getAttribute(MultiJournalDetailListgrid.Trackpdata) == "No") {
							journalDetail.setCustomerAccount(null);
							journalDetail.setLoan(null);
						} else {
							journalDetail.setCustomerAccount(null);
							journalDetail.setLoan(null);
						}
						
//                             
						GWT.log("customerAccount Id: " + customerAccount.getSysid());
						GWT.log("Loan id :" + loan.getLOANID());
						journalDetails.add(journalDetail);
					}

					multiJournal.setMultiJournalDetails(journalDetails);

					SC.showPrompt("", "", new SwizimaLoader());

					dispatcher.execute(new SysAction(PAConstant.SAVE_MULTI_JOURNAL, multiJournal),
							new AsyncCallback<SysResult>() {
								public void onFailure(Throwable caught) {
									System.out.println(caught.getMessage());
								}

								public void onSuccess(SysResult result) {

									SC.clearPrompt();
									if (result != null) {

										if (result.getSwizFeedback().isResponse()) {

											SC.say("SUCCESS", result.getSwizFeedback().getMessage());

											getView().getMultiJournalPane().getMultiJournalListgrid()
													.addRecordsToGrid(result.getMultiJournals());

											clearForm(window);

										} else {
											SC.warn("ERROR", result.getSwizFeedback().getMessage());
										}
									} else {
										SC.say("ERROR", "Unknow error");
									}

								}
							});

				}
			}
		});
	}

	private void onLoadButtonClicked() {
		getView().getMultiJournalPane().getLoadButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadJournals();

			}
		});

		getView().getMultiJournalPane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadJournals();

			}
		});
	}

	private void loadJournals() {

		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_MULTI_JOURNAL), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					getView().getMultiJournalPane().getMultiJournalListgrid()
							.addRecordsToGrid(result.getMultiJournals());

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

	}

	private void onRecordExpanded() {
		getView().getMultiJournalPane().getMultiJournalListgrid().addRecordExpandHandler(new RecordExpandHandler() {

			public void onRecordExpand(RecordExpandEvent event) {

				getView().getMultiJournalPane().getMultiJournalListgrid().deselectAllRecords();
				getView().getMultiJournalPane().getMultiJournalListgrid().selectRecord(event.getRecord());

				MultiJournalDetailListgrid listgrid = new MultiJournalDetailListgrid();

				getView().getMultiJournalPane().getMultiJournalListgrid().setListgrid(listgrid);

				MultiJournal multiJournal = new MultiJournal();
				multiJournal.setId(getView().getMultiJournalPane().getMultiJournalListgrid().getSelectedRecord()
						.getAttribute(MultiJournalListgrid.ID));

				SC.showPrompt("", "", new SwizimaLoader());

				dispatcher.execute(new SysAction(PAConstant.GET_MULTI_JOURNAL_DETAILS, multiJournal),
						new AsyncCallback<SysResult>() {
							public void onFailure(Throwable caught) {
								System.out.println(caught.getMessage());
							}

							public void onSuccess(SysResult result) {

								SC.clearPrompt();

								if (result != null) {

									getView().getMultiJournalPane().getMultiJournalListgrid().getListgrid()
											.addRecordsToGrid(result.getMultiJournalDetails());

								} else {
									SC.say("ERROR", "Unknow error");
								}

							}
						});

			}
		});
	}

}
