package com.pearl.accounting.sysui.client.views.panes;

import com.pearl.accounting.sysui.client.views.listgrids.AccountListgrid;
import com.pearl.accounting.sysui.client.views.listgrids.DoubleEntryListgrid;
import com.pearl.accounting.sysui.client.views.widgets.ControlsPane;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class DoubleEntryPane extends VLayout {

	private ControlsPane controlsPane;

	private DoubleEntryListgrid doubleentryListgrid;

	private IButton addButton;
	private IButton editButton;
	private IButton deleteButton;
	private IButton loadButton;
    private IButton searchButton;
	private IButton activateButton;
	private IButton deactivaeButton;

	public DoubleEntryPane() {

		super();
		Label header = new Label();
		header.setStyleName("crm-ContextArea-Header-Label");
		header.setContents("DoubleEntry");
		header.setWidth("100%");
		header.setAutoHeight();
		header.setMargin(10);
		header.setAlign(Alignment.LEFT);

		controlsPane = new ControlsPane();

		doubleentryListgrid = new DoubleEntryListgrid();

		addButton = new IButton("New");
		editButton = new IButton("Edit");
		deleteButton = new IButton("Delete");
		loadButton = new IButton("Refresh");

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(addButton, editButton, deleteButton, activateButton, deactivaeButton, loadButton,searchButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(10);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(controlsPane);
		layout.addMember(header);
		layout.addMember(doubleentryListgrid);
		layout.addMember(buttonLayout);
		this.addMember(layout);

	}

	public ControlsPane getControlsPane() {
		return controlsPane;
	}

	public DoubleEntryListgrid getDoubleEntryListgrid() {
		return doubleentryListgrid;
	}

	public IButton getAddButton() {
		return addButton;
	}

	public IButton getEditButton() {
		return editButton;
	}

	public IButton getDeleteButton() {
		return deleteButton;
	}

	public IButton getLoadButton() {
		return loadButton;
	}

	public IButton getActivateButton() {
		return activateButton;
	}

	public IButton getDeactivaeButton() {
		return deactivaeButton;
	}

	public IButton getSearchButton() {
		return searchButton;
	}

	
}