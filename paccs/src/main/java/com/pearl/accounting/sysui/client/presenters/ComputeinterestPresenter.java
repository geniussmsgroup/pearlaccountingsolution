package com.pearl.accounting.sysui.client.presenters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Formatter;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.shared.DispatchAsync;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.pearl.accounting.sysmodel.FixedData;
import com.pearl.accounting.sysmodel.Intervaltb;
import com.pearl.accounting.sysmodel.LoaninterestView;
import com.pearl.accounting.sysmodel.Totalinterest;
import com.pearl.accounting.sysui.client.place.NameTokens;
import com.pearl.accounting.sysui.client.utils.PAConstant;
import com.pearl.accounting.sysui.client.views.panes.LoanBalancePane;
import com.pearl.accounting.sysui.client.views.panes.LoanratePane;
import com.pearl.accounting.sysui.client.views.widgets.SwizimaLoader;
import com.pearl.accounting.sysui.client.views.windows.AccountWindow;
import com.pearl.accounting.sysui.client.views.windows.CashBookPaymentWindow;
import com.pearl.accounting.sysui.client.views.windows.ComputeinterestWindow;
import com.pearl.accounting.sysui.client.views.windows.MultiJournalWindow;
import com.pearl.accounting.sysui.shared.SysAction;
import com.pearl.accounting.sysui.shared.SysResult;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.BlurEvent;
import com.smartgwt.client.widgets.form.fields.events.BlurHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.EditorExitEvent;
import com.smartgwt.client.widgets.form.fields.events.EditorExitHandler;

public class ComputeinterestPresenter
		extends Presenter<ComputeinterestPresenter.MyView, ComputeinterestPresenter.MyProxy> {

	private final PlaceManager placeManager;
	private final DispatchAsync dispatcher;

	public interface MyView extends View {
		// public GeneralLedgerPane getGeneralLedgerPane();
		public LoanBalancePane getLoanbalancePane();

		public LoanratePane getloanratePane();
	}

	@ProxyCodeSplit
	@NameToken(NameTokens.DayComputeinterest)
	public interface MyProxy extends ProxyPlace<ComputeinterestPresenter> {
	}

	@Inject
	public ComputeinterestPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
			final DispatchAsync dispatcher, final PlaceManager placeManager) {
		super(eventBus, view, proxy);

		this.dispatcher = dispatcher;
		this.placeManager = placeManager;
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this, MainPagePresenter.TYPE_SetContextAreaContent, this);
	}

	@Override
	protected void onBind() {
		super.onBind();
		load();
		onLoadButonClicked();
		onLoadClickedloanrate();
		loadrates();
		onclickcompute();
		//// onloaddatefromfixeddata();
///		onfixe();
	}

	private void onLoadButonClicked() {
		getView().getLoanbalancePane().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				load();
				/// shows();
//				onfixeddate();
//				onDateLoaded();
//				ongetlastinterest();
			}
		});

		getView().getLoanbalancePane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				load();
//				
//				onfixeddate();
//				onDateLoaded();
//				ongetlastinterest();
			}
		});
	}

	private void load() {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_LoanBlanceQry), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					getView().getLoanbalancePane().getLoanbalanceListgrid().addRecordsToGrid(result.getLoanbalances());

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void onclickcompute() {
		getView().getLoanbalancePane().getAddButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				create();
				/// shows();
//				onfixeddate();
//				onDateLoaded();
//				ongetlastinterest();
			}
		});

		getView().getLoanbalancePane().getControlsPane().getNewAssetButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				create();
//				
//				onfixeddate();
//				onDateLoaded();
//				ongetlastinterest();
			}
		});
	}

	private void create() {
		ComputeinterestWindow window = new ComputeinterestWindow();

		ongetlastinterest(window);
		ongetlastinterest(	window);	
				onloaddatefromfixeddata(window);
		
		onsavebuttonClicked(window);
		///// window.animateFade(10);
///		currentperiods(window);
		
		window.show();
	}

	private void onDateLoaded(final ComputeinterestWindow window) {
//		window.getFixeddate().addChangedHandler(new ChangedHandler() {
//			public void onChanged(ChangedEvent event) {
		SC.showPrompt("", "", new SwizimaLoader());
		dispatcher.execute(new SysAction(PAConstant.GET_Interval), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {
				// TODO Auto-generated method stub
				SC.clearPrompt();
				if (result != null) {
//								
					for (Intervaltb period : result.getIntervaltbs()) {

						final Date dn = window.getFixeddate().getValueAsDate();
						if (dn.before(period.getFEND()) && dn.after(period.getFSTART())) {
							window.getPeriodcode().setValue(period.getFINTCODE());
							window.getYear().setValue(period.getYRCODE());

//									window.getAssessmentPeriod().setValue(period.getFINTNAME());
//									window.getFinancialYear().setValue();
									window.getStartDate().setValue(period.getFSTART());
									window.getEndDate().setValue(period.getFEND());
									window.getPeriod().setValue(period.getFINTNAME());

						}
					}
				}
//			}
//		});
        	}

//			@Override
//			public void onChanged(ChangedEvent event) {
//				// TODO Auto-generated method stub
//				
//			}

//			@Override
//			public void onEditorExit(EditorExitEvent event) {
//				// TODO Auto-generated method stub
//				
//			}
		});
        	}


//	private void currentperiods(final ComputeinterestWindow window) {
//		dispatcher.execute(new SysAction(PAConstant.GET_CurrentPeriod), new AsyncCallback<SysResult>() {
//			public void onFailure(Throwable caught) {
//				System.out.println(caught.getMessage());
//			}
//
//			public void onSuccess(SysResult result) {
//				// TODO Auto-generated method stub
//				if (result != null) {
////						
//					for (Intervaltb period : result.getIntervaltbs()) {
//
//							window.getCurrentperiod().setValue(period.getFINTNAME());
//							//window.getYear().setValue(period.getYRCODE());
//							window.getCurrentstartdate().setValue(period.getFSTART());
//							
//							window.getCurrentenddate().setValue(period.getFEND());
//							//period.window.getPeriodcode().setValue(period.getFINTCODE());
//					
//				}}
//			}
//		});
//	}
//	

	// @Override
	// TODO Auto-generated method stub

	// }

	private void ongetlastinterest(final ComputeinterestWindow window) {
///**/
//*here we are geting the last interest date to display to the client 
//\**

		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_Totalinterest), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();
				// Date tdate = null;
				if (result != null) {

					for (Totalinterest totalint : result.getTotalinterests()) {
						// Date tdate = totalint.getTdate();
						window.getFsave().setValue(totalint.getFsave());
						window.getLastintdate().setValue(totalint.getTdate());

					}

					// getView().getLoanbalancePane().getFixeddata().setValueMap(valueMap);
				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

		// }
		// });
	}

	private void onloaddatefromfixeddata(final ComputeinterestWindow window) {
		//// here we are picking the interest date to be computed

		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_FixedData), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();
				// Date tdate = null;
				if (result != null) {
					// LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
					for (FixedData fixeddata : result.getFixeddatas()) {
						Date tdate = fixeddata.getNextIntDte();
						window.getFixeddate().setValue(tdate);
					}
					onDateLoaded(window);
					// getView().getLoanbalancePane().getFixeddata().setValueMap(valueMap);
				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

		// }
		// });
	}

	private boolean FormValid(final ComputeinterestWindow window) {
		if (window.getYear().getValueAsString() != null) {
			if (window.getPeriodcode().getValueAsString() != null) {
				if (window.getFixeddate().getValueAsDate() != null) {
					return true;
				} else {
					SC.warn("ERROR", "Please adjust the system date ");

				}
			} else {
				SC.warn("ERROR", "Please adjust the system date ");

			}
		} else {
			SC.warn("ERROR", "Please adjust the system date");

		}
		return false;
	}

	private void onsavebuttonClicked(final ComputeinterestWindow window) {
		window.getSaveButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				if (FormValid(window)) {

					Totalinterest dayinterest = new Totalinterest();

					dayinterest.setPeriodcode(window.getPeriodcode().getValueAsString());
					dayinterest.setYearcode(Integer.parseInt(window.getYear().getValueAsString()));
					dayinterest.setFsave("Y");
					dayinterest.setTdate(window.getFixeddate().getValueAsDate());
//				
					SC.showPrompt("", "", new SwizimaLoader());

					dispatcher.execute(new SysAction(PAConstant.SAVE_Totalinterest, dayinterest),
							new AsyncCallback<SysResult>() {
								public void onFailure(Throwable caught) {
									System.out.println(caught.getMessage());
								}

								public void onSuccess(SysResult result) {

									SC.clearPrompt();

									if (result != null) {

										if (result.getSwizFeedback().isResponse()) {

											SC.say("SUCCESS", result.getSwizFeedback().getMessage());
											/// SC.say("wait for the fixed date ");
											load();
											/// shows();
											onloaddatefromfixeddata(window);
											ongetlastinterest(window);
											//// .addRecordsToGrid(result.getCustomerAccounts());
										} else {
											SC.warn("ERROR", result.getSwizFeedback().getMessage());
										}

									} else {
										SC.say("ERROR", "Unknow error");
									}

								}
							});

				}
			}
		});
	}

	private void onLoadClickedloanrate() {
		getView().getloanratePane().addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				loadrates();

			}
		});

		getView().getloanratePane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				loadrates();

			}
		});
	}

	private void loadrates() {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_LoanRateQry), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					getView().getloanratePane().getLoanRateListgrid().addRecordsToGrid(result.getLoaninterestviews());

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

	}
//
//	private void shows() {
//		SC.showPrompt("", "", new SwizimaLoader());
//
//		dispatcher.execute(new SysAction(PAConstant.GET_LoanRateQry), new AsyncCallback<SysResult>() {
//			public void onFailure(Throwable caught) {
//				System.out.println(caught.getMessage());
//			}
//
//			public void onSuccess(SysResult result) {
//
//				SC.clearPrompt();
//
//				if (result != null) {
//					float sum = 0;
//					for (LoaninterestView qry : result.getLoaninterestviews()) {
//						float ans = qry.getInterestRate();
//						sum = sum + ans;
//						System.out.println(sum);
//					}
//					SC.say(String.valueOf(sum));
//
//				} else {
//					SC.say("ERROR", "Unknow error");
//				}
//
//			}
//		});
//
//	}

}
