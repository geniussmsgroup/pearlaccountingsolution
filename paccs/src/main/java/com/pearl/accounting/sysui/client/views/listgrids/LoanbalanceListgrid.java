package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.LoanbalanceView;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class LoanbalanceListgrid extends SuperListGrid {
	public static String ID = "id";
	public static String Lnbal = "Lnbal";
	public static String ProductID = "ProductID";

	public static String MemberId = "MemberId";

//	
	public LoanbalanceListgrid() {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField loan = new ListGridField(Lnbal, "loan balance");
		
		ListGridField product = new ListGridField(ProductID, "product id");

		ListGridField member = new ListGridField(MemberId, "member Code");
		this.setFields(id, member, product,loan );

	}

	public ListGridRecord addRowData(LoanbalanceView account) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute(ID, account.getMemberId());
		record.setAttribute(Lnbal, account.getLnbal());
		record.setAttribute(ProductID, account.getProductID());

		record.setAttribute(MemberId, account.getMemberId());

//		record.setAttribute(AccountDefinition, account.getAccountDefinition());
//
//		record.setAttribute(TrackPData, account.getAnalReq());

		return record;
	}

	public void addRecordsToGrid(List<LoanbalanceView> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (LoanbalanceView item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
	}

}
