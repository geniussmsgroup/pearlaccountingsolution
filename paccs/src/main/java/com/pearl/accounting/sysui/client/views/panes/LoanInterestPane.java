package com.pearl.accounting.sysui.client.views.panes;

import com.pearl.accounting.sysui.client.views.listgrids.ClientStatementListgrid;
import com.pearl.accounting.sysui.client.views.widgets.ControlsPane;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class LoanInterestPane extends VLayout {
	private ControlsPane controlsPane;
	private IButton loadButton;
	private IButton interest;

	public LoanInterestPane() {
		super();
		Label header = new Label();
		header.setStyleName("crm-ContextArea-Header-Label");
		header.setContents("Customer statements");
		header.setWidth("100%");
		header.setAutoHeight();
		header.setMargin(10);
		header.setAlign(Alignment.LEFT);

		loadButton = new IButton("Load");
		interest = new IButton("Interest");

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(loadButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(10);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(controlsPane);
		layout.addMember(header);
		layout.addMember(buttonLayout);
		this.addMember(layout);

	}

	public ControlsPane getControlsPane() {
		return controlsPane;
	}

	public IButton getInterest() {
		return interest;
	}

	public void setInterest(IButton interest) {
		this.interest = interest;
	}

	public IButton getLoadButton() {
		return loadButton;
	}

}
