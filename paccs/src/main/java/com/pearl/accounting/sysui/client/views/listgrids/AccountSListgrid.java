package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.Init;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class AccountSListgrid extends SuperListGrid{
	public static String ID = "id";
	public static String ACODE = "Acode";
	public static String ANAME = "accountName";
	
	public static String AccountType = "accountType";
	public static String INC_EXP = "inc-exp"	;	
	public static String CurrencyCD = "CurrencyCd";
	
	
	public static String PRoductID = "ProductID";
	public static String AnalReQ = "AnalReq";
	public static String PData = "pdata";


	public AccountSListgrid() {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField Acode = new ListGridField(ACODE, "ACode");
		ListGridField AName = new ListGridField(ANAME, "Account name");
		
		ListGridField accountType = new ListGridField(AccountType, "Account type");
		 
		ListGridField inc_exp =new ListGridField(INC_EXP, "Income/Expenditure");
		
		ListGridField CurrencyCd = new ListGridField(CurrencyCD, "currency");
		ListGridField ProductID = new ListGridField(PRoductID, "product ID");
		ListGridField AnalReq = new ListGridField(AnalReQ, "AnalReQ");
		ListGridField Pdata = new ListGridField(PData, "PData");
		 
		

		this.setFields(id, Acode,AName,accountType, inc_exp, CurrencyCd,ProductID,AnalReq,Pdata);

	}
	public ListGridRecord addRowData(Init init) {
		ListGridRecord record = new ListGridRecord(); 
		record.setAttribute(ID, init.getId());
		record.setAttribute(ACODE, init.getAcode());
		record.setAttribute(ANAME, init.getAname());		
		if(init.isPdata()==true||init.getProductId()!=null){
			record.setAttribute(PData, "Yes");
			record.setAttribute(PRoductID, init.getProductId());
			record.setAttribute(PRoductID, init.getProductId(). getProductDetails());		}
		else{
			//record.setAttribute(AnalReQ, "No");
			record.setAttribute(PData, "No");
		}
	if(init.isAnalRequired()) {
		record.setAttribute(AnalReQ, "Yes");
	} else{
		record.setAttribute(AnalReQ, "NO");
	}
	if(init.getTtypeCd()!=null){
		record.setAttribute(AccountType, init.getTtypeCd().getAtypecd());
	}
if(init.getIncExp()!=null) {
	record.setAttribute(INC_EXP, init.getIncExp().getProduct());
	
	
	
	
}		return record;

	}
public void addRecordsToGrid(List<Init> list) {
	ListGridRecord[] records = new ListGridRecord[list.size()];
	int row = 0;
	for (Init item : list) {
		records[row] = addRowData(item);
		row++;
	}
	this.setData(records);
}

	
	}
		 

