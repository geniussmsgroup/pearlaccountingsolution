//package com.pearl.accounting.sysui.client.presenters;
//
//import com.google.gwt.user.client.Window;
//import com.google.gwt.user.client.ui.Widget;
//import com.google.inject.Inject;
//import com.gwtplatform.mvp.client.ViewImpl;
//import com.pearl.accounting.sysui.client.views.panes.MyPane;
//import com.smartgwt.client.widgets.layout.VLayout;
//
//public class MyView extends ViewImpl implements MyPresenter.MyView {
//
//	private static final String DEFAULT_MARGIN = "0px";
//	private VLayout panel;
//	private MyPane myPane;
//
//	@Inject
//	public MyView() {
//
//		panel = new VLayout();
//
//		myPane = new MyPane();
//
//		panel.setMembers(myPane);
//		panel.setWidth100();
//		panel.setHeight("90%");
//		Window.enableScrolling(false);
//		Window.setMargin(DEFAULT_MARGIN);
//
//	}
//
//	public Widget asWidget() {
//		return panel;
//	}
//
//	public MyPane getMyPane() {
//		return myPane;
//	}
//	
//	
//
//}
