package com.pearl.accounting.sysui.client.presenters;

import java.util.Date;
import java.util.LinkedHashMap;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.shared.DispatchAsync;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.AssessmentPeriod;
import com.pearl.accounting.sysmodel.CashBookPayment;
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.DataSourceScreen;
import com.pearl.accounting.sysmodel.FinancialYear;
import com.pearl.accounting.sysmodel.FixedData;
import com.pearl.accounting.sysmodel.Fsave;
import com.pearl.accounting.sysmodel.InterestType;
import com.pearl.accounting.sysmodel.Intervaltb;
import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysmodel.LoanCategory;
import com.pearl.accounting.sysmodel.LoanDeposit;
import com.pearl.accounting.sysmodel.LoanRepayment;
import com.pearl.accounting.sysmodel.PeriodicDeposit;
import com.pearl.accounting.sysmodel.Receiptno;
import com.pearl.accounting.sysmodel.RepaymentCategory;
import com.pearl.accounting.sysmodel.RepaymentSchedule;
import com.pearl.accounting.sysmodel.Salary;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.Theadp;
import com.pearl.accounting.sysmodel.Totalinterest;
import com.pearl.accounting.sysui.client.place.NameTokens;
import com.pearl.accounting.sysui.client.utils.PAConstant;
import com.pearl.accounting.sysui.client.utils.PAManager;
import com.pearl.accounting.sysui.client.views.listgrids.LoanListgrid;
import com.pearl.accounting.sysui.client.views.listgrids.LoanRepaymentListgrid;
import com.pearl.accounting.sysui.client.views.listgrids.RepaymentScheduleListgrid;
import com.pearl.accounting.sysui.client.views.panes.LoanPane;
import com.pearl.accounting.sysui.client.views.widgets.SwizimaLoader;
import com.pearl.accounting.sysui.client.views.windows.ComputeinterestWindow;
import com.pearl.accounting.sysui.client.views.windows.LoanDepositWindow;
import com.pearl.accounting.sysui.client.views.windows.LoanRepaymentWindow;
import com.pearl.accounting.sysui.client.views.windows.LoanWindow;
import com.pearl.accounting.sysui.client.views.windows.MultiJournalWindow;
import com.pearl.accounting.sysui.client.views.windows.PeriodicDepositWindow;
import com.pearl.accounting.sysui.client.views.windows.SalaryWindow;
import com.pearl.accounting.sysui.shared.SysAction;
import com.pearl.accounting.sysui.shared.SysResult;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.BlurEvent;
import com.smartgwt.client.widgets.form.fields.events.BlurHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.EditorExitEvent;
import com.smartgwt.client.widgets.form.fields.events.EditorExitHandler;
import com.smartgwt.client.widgets.grid.events.RecordExpandEvent;
import com.smartgwt.client.widgets.grid.events.RecordExpandHandler;

public class LoanPresenter extends Presenter<LoanPresenter.MyView, LoanPresenter.MyProxy> {

	private final PlaceManager placeManager;
	private final DispatchAsync dispatcher;

	public interface MyView extends View {
		public LoanPane getLoanPane();
	}

	@ProxyCodeSplit
	@NameToken(NameTokens.loans)
	public interface MyProxy extends ProxyPlace<LoanPresenter> {
	}

	@Inject
	public LoanPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
			final DispatchAsync dispatcher, final PlaceManager placeManager) {
		super(eventBus, view, proxy);

		this.dispatcher = dispatcher;
		this.placeManager = placeManager;
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this, MainPagePresenter.TYPE_SetContextAreaContent, this);
	}

	@Override
	protected void onBind() {
		super.onBind();

		onNewButtonClicked();
		loadLoans();
		onDeleteButtonClicked();
		onRecordExpanded();
		onPaymentButtonClicked();
		onLoadButtonClicked();
		onComputeButtonClicked() ;
		onEditButtonClicked();

	}

	private void onLoadButtonClicked() {
		getView().getLoanPane().getLoadButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				loadLoans();

			}
		});

		getView().getLoanPane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadLoans();

			}
		});
	}

	private void onNewButtonClicked() {
		getView().getLoanPane().getAddButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				create();

			}
		});

		getView().getLoanPane().getControlsPane().getNewAssetButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				create();

			}
		});

	}

	private void onComputeButtonClicked() {
		getView().getLoanPane().getLoadInterest().addClickHandler(new ClickHandler() {
			///public void onClick(ClickEvent event) {
public void onClick(ClickEvent event) {
				Loaninterestwindow();
			}
		});
	}
	
	
	
	
	
	
	
	
	private void create() {
		LoanWindow window = new LoanWindow();
		loadAccounts(window);
//		loadPeriods(window);
//		loadFinancialYears(window);
	
		loadCustomers(window);
		// loadLoanCategories(window);
		loadRepaymentCategory(window);
		loadInterestType(window);
		loadtest(window);
		onSaveButtonClicked(window);
		onloadcurrentperiod(window);
		window.animateShow();
	}

//	private void loadLoanCategories(LoanWindow window) {
//
//		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
//
//		for (LoanCategory category : LoanCategory.values()) {
//			valueMap.put(category.getLoanCategory(), category.getLoanCategory());
//		}
//
//		window.getLoanCategory().setValueMap(valueMap);
//
//	}

	private void loadRepaymentCategory(LoanWindow window) {

		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
		valueMap.put("Mon", "Mon");
		valueMap.put("WK", "WK");
		valueMap.put("BW", "BW");
		valueMap.put("Qtz", "Qtz");

		window.getRepaymentCategory().setValueMap(valueMap);

	}

	private void loadInterestType(LoanWindow window) {

		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

		for (InterestType category : InterestType.values()) {
			valueMap.put(category.getInterestType(), category.getInterestType());
		}

		window.getInterestType().setValueMap(valueMap);

	}

	private void onDateLoaded(final LoanWindow window) {

		window.getLoanDisbursementDate().addEditorExitHandler(new EditorExitHandler() {

			public void onEditorExit(EditorExitEvent event) {

				dispatcher.execute(new SysAction(PAConstant.GET_Interval), new AsyncCallback<SysResult>() {
					public void onFailure(Throwable caught) {
						System.out.println(caught.getMessage());
					}

					public void onSuccess(SysResult result) {
						// TODO Auto-generated method stub
						if (result != null) {
//								
							for (Intervaltb period : result.getIntervaltbs()) {

								final Date dn = window.getLoanDisbursementDate().getValueAsDate();
								if (dn.before(period.getFEND()) && dn.after(period.getFSTART())) {
									window.getAssessmentPeriod().setValue(period.getFINTNAME());
									window.getFinancialYear().setValue(period.getYRCODE());
									window.getStartdate().setValue(period.getFSTART());
									window.getEnddate().setValue(period.getFEND());
									window.getPeriodcode().setValue(period.getFINTCODE());

								}
							}
						}
					}
				});
			}
		});
	}

	private void loadAccounts(final LoanWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_ACCOUNT), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
					for (Account account : result.getAccounts()) {
						valueMap.put(account.getSysid(), account.getAccountName() + " " + account.getACODE());
					}

					window.getPayingAccount().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void onloadcurrentperiod(final LoanWindow window) {
		dispatcher.execute(new SysAction(PAConstant.GET_CurrentPeriod), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {
				// TODO Auto-generated method stub
				if (result != null) {
//						
					for (Intervaltb period : result.getIntervaltbs()) {

							window.getCurrentperiod().setValue(period.getFINTNAME());
							//window.getYear().setValue(period.getYRCODE());
							window.getCurrentstartdate().setValue(period.getFSTART());
							
							window.getCurrentenddate().setValue(period.getFEND());
							//period.window.getPeriodcode().setValue(period.getFINTCODE());
					
				}}
			}
		});
	}
	
	
	
	private void loadtest(final LoanWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_LOAN), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

				for (Loan account : result.getLoans()) {
//						max(account.getHDNO());
//						valueMap.put(account.getHDNO(), account.getHDNO());
					String getlon= account.getLOANID();
					int generatelonaid = Integer.parseInt(getlon);
					String loanmax = String.valueOf(generatelonaid);
					window.getTest().setValue(loanmax);

				}

			}
		});
	}

	private void loadCustomers(final LoanWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_customerAccount), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

					for (CustomerAccount account : result.getCustomerAccounts()) {
						valueMap.put(account.getSysid(),
								account.getClientCode() + " " + account.getSurname() + " " + account.getOtherNames());
					}

					window.getCustomerAccount().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private boolean FormValid(final LoanWindow window) {
		if (window.getCustomerAccount().getValueAsString() != null) {
			if (window.getPayingAccount().getValueAsString() != null) {
				if (window.getLoanFee().getValueAsString() != null) {
					// if (window.getLoanCategory().getValueAsString() != null) {
					if (window.getLoanPurpose().getValueAsString() != null) {
						if (window.getPaymentDetails().getValueAsString() != null) {
							if (window.getLoanAmount().getValueAsString() != null) {

								if (window.getPayingAccount().getValueAsString() != null) {
									return true;

								} else {
									SC.warn("ERROR", "Please Select paying account");

								}

							} else {
								SC.warn("ERROR", "Please Select loan amount");

							}

						} else {
							SC.warn("ERROR", "Please Select payments details");

						}

					} else {
						SC.warn("ERROR", "Please enter loan purpose");

					}

//					} else {
//						SC.warn("ERROR", "Please Select Loan Category");
//
//					}
				} else {
					SC.warn("ERROR", "Please enter loan fee");

				}
			}
		} else {
			SC.warn("ERROR", "Please enter");

		}

		return false;

	}

	private void onSaveButtonClicked(final LoanWindow window) {
		window.getSaveButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				if (FormValid(window)) {

					CustomerAccount customerAccount = new CustomerAccount();
					customerAccount.setSysid(window.getCustomerAccount().getValueAsString());

					Account payingAccount = new Account();
					payingAccount.setSysid(window.getPayingAccount().getValueAsString());

					Date loanDisbursementDate = window.getLoanDisbursementDate().getValueAsDate();
					int repaymentPeriod = Integer.parseInt(window.getRepaymentPeriod().getValueAsString());

//					LoanCategory loanCategory = LoanCategory
//							.getLoanCategory(window.getLoanCategory().getValueAsString());

					float loanAmount = Float.parseFloat(window.getLoanAmount().getValueAsString());
					float interestRate = Float.parseFloat(window.getInterestRate().getValueAsString());
					String paymentDetails = window.getPaymentDetails().getValueAsString();
					String loanPurpose = window.getLoanPurpose().getValueAsString();
					float loanFee = Float.parseFloat(window.getLoanFee().getValueAsString());

					float loanInsuranceFee = Float.parseFloat(window.getLoanInsuranceFee().getValueAsString());
					float loanFormFee = Float.parseFloat(window.getLoanFormFee().getValueAsString());
					float loanPhotoFee = Float.parseFloat(window.getLoanPhotoFee().getValueAsString());

					// RepaymentCategory repaymentCategory = RepaymentCategory
					// .getLoanCategory(window.getRepaymentCategory().getValueAsString());
					InterestType interestType = InterestType
							.getLoanCategory(window.getInterestType().getValueAsString());

					Loan loan = new Loan();
					int year = Integer.parseInt(window.getFinancialYear().getValueAsString());
					loan.setCustomerAccount(customerAccount);
					loan.setDateCreated(new Date());
					loan.setDateUpdated(new Date());
					loan.setYear(year);
					loan.setInterestRate(interestRate);
					loan.setLoanAmount(loanAmount);
					// loan.setLoanCategory(loanCategory);
					loan.setLoanDisbursementDate(loanDisbursementDate);
					loan.setLoanFee(loanFee);
					loan.setLoanFormFee(loanFormFee);
					loan.setLoanInsuranceFee(loanInsuranceFee);
					loan.setLoanPhotoFee(loanPhotoFee);
					loan.setLoanPurpose(loanPurpose);
					loan.setPayingAccount(payingAccount);
					loan.setPaymentDetails(paymentDetails);
					loan.setRepaymentPeriod(repaymentPeriod);
					loan.setStatus(Status.ACTIVE);
					loan.setPeriodcode(window.getPeriodcode().getValueAsString());
					loan.setRepaymentCategory(window.getRepaymentCategory().getValueAsString());
					////////////////////
					String theadmax = window.getTest().getValueAsString();
					int theadnew = Integer.parseInt(theadmax) ;
					loan.setLOANID(String.valueOf(theadnew));
					loan.setInterestType(interestType);
					loan.setApproveAmt(loanAmount);
					loan.setPAYAMOUNT(loanAmount);
					loan.setFsave(Fsave.Y);
					loan.setAPPROVED("Y");
					loan.setLastIntDate(loanDisbursementDate);
					loan.setPayDate(loanDisbursementDate);
					loan.setStatus(Status.ACTIVE);
					loan.setClientCat("BUSL");
//					LoanDeposit ld = new LoanDeposit();
//					ld.setRemarks(paymentDetails);          
//					ld.setLoandeposit(loanAmount);
//					ld.setInterestdeposit(interestRate);
//                    ld.setDateCreated(new Date());					
//					ld.setDateUpdated(new Date());
//				    ld.setAccount(payingAccount);
//                    ld.setLoan(null);					
//					ld.setCustomeraccount(customerAccount);
//                    ld.setDepositdate(loanDisbursementDate);
//					// loan.setLOANID("0");
//
//					
//
//					
//                    SC.showPrompt("", "", new SwizimaLoader());
//
//					dispatcher.execute(new SysAction(PAConstant.SAVE_LOANDEPOSITS, ld),
//							new AsyncCallback<SysResult>() {
//								public void onFailure(Throwable caught) {
//									System.out.println(caught.getMessage());
//								}
//
//								public void onSuccess(SysResult result) {
//
//									SC.clearPrompt();
//								
//									if (result != null) {
//
//										if (result.getSwizFeedback().isResponse()) {
//											ClearForm(window);
//											SC.say("SUCCESS", result.getSwizFeedback().getMessage());
//
//											
//
//										} else {
//											SC.warn("ERROR", result.getSwizFeedback().getMessage());
//										}
//
//									} else {
//										SC.say("ERROR", "Unknow error");
//									}
//
//								}
//							});
//
                    
                    
                    SC.showPrompt("", "", new SwizimaLoader());

					dispatcher.execute(new SysAction(PAConstant.SAVE_LOAN, loan), new AsyncCallback<SysResult>() {
						public void onFailure(Throwable caught) {
							System.out.println(caught.getMessage());
						}

						public void onSuccess(SysResult result) {

							SC.clearPrompt();
							ClearForm(window);
							if (result != null) {

								if (result.getSwizFeedback().isResponse()) {

									SC.say("SUCCESS", result.getSwizFeedback().getMessage());

									getView().getLoanPane().getLoanListgrid().addRecordsToGrid(result.getLoans());

								} else {
									SC.warn("ERROR", result.getSwizFeedback().getMessage());
								}

							} else {
								SC.say("ERROR", "Unknow error");
							}

						}
					});

				}
			}
		});
	}

	private void ClearForm(final LoanWindow window) {
		window.getLoanPhotoFee().clearValue();
		window.getLoanFormFee().clearValue();
		window.getLoanInsuranceFee().clearValue();
		window.getPayingAccount().clearValue();
		window.getLoanFee().clearValue();
		window.getLoanPurpose().clearValue();
		window.getPaymentDetails().clearValue();
		window.getInterestRate().clearValue();
		window.getLoanAmount().clearValue();
		window.getLoanCategory().clearValue();
		window.getCustomerAccount().clearValue();
		window.getInterestType().clearValue();
		window.getRepaymentPeriod().clearValue();
		window.getRepaymentCategory().clearValue();

	}

	private void loadLoans() {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_LOAN), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					getView().getLoanPane().getLoanListgrid().addRecordsToGrid(result.getLoans());

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void onDeleteButtonClicked() {
		getView().getLoanPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				delete();

			}
		});

		getView().getLoanPane().getControlsPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				delete();

			}
		});
	}

	private void delete() {
		if (getView().getLoanPane().getLoanListgrid().anySelected()) {

			SC.ask("Confirm", "Are you sure you want to delete the selected record? ", new BooleanCallback() {

				public void execute(Boolean value) {
					if (value) {

						Loan loan = new Loan();
						loan.setLOANID(getView().getLoanPane().getLoanListgrid().getSelectedRecord()
								.getAttribute(LoanListgrid.ID));

						SC.showPrompt("", "", new SwizimaLoader());

						dispatcher.execute(new SysAction(PAConstant.DELETE_LOAN, loan), new AsyncCallback<SysResult>() {
							public void onFailure(Throwable caught) {
								System.out.println(caught.getMessage());
							}

							public void onSuccess(SysResult result) {

								SC.clearPrompt();

								if (result != null) {

									if (result.getSwizFeedback().isResponse()) {

										SC.say("SUCCESS", result.getSwizFeedback().getMessage());

										getView().getLoanPane().getLoanListgrid().addRecordsToGrid(result.getLoans());

									} else {
										SC.warn("ERROR", result.getSwizFeedback().getMessage());
									}

								} else {
									SC.say("ERROR", "Unknow error");
								}

							}
						});
					}

				}
			});

		} else {
			SC.say("ERROR", "Please select record to delete");
		}
	}

	private void onRecordExpanded() {
		getView().getLoanPane().getLoanListgrid().addRecordExpandHandler(new RecordExpandHandler() {

			public void onRecordExpand(RecordExpandEvent event) {

				getView().getLoanPane().getLoanListgrid().deselectAllRecords();
				getView().getLoanPane().getLoanListgrid().selectRecord(event.getRecord());

				RepaymentScheduleListgrid repaymentScheduleListgrid = new RepaymentScheduleListgrid();
				getView().getLoanPane().getLoanListgrid().setRepaymentScheduleListgrid(repaymentScheduleListgrid);

				LoanRepaymentListgrid loanRepaymentListgrid = new LoanRepaymentListgrid();

				getView().getLoanPane().getLoanListgrid().setLoanRepaymentListgrid(loanRepaymentListgrid);

				Loan loan = new Loan();
				loan.setLOANID(
						getView().getLoanPane().getLoanListgrid().getSelectedRecord().getAttribute(LoanListgrid.ID));

				SC.showPrompt("", "", new SwizimaLoader());

				dispatcher.execute(new SysAction(PAConstant.GET_RepaymentSchedules, loan),
						new AsyncCallback<SysResult>() {
							public void onFailure(Throwable caught) {
								System.out.println(caught.getMessage());
							}

							public void onSuccess(SysResult result) {

								SC.clearPrompt();

								if (result != null) {

									getView().getLoanPane().getLoanListgrid().getRepaymentScheduleListgrid()
											.addRecordsToGrid(result.getRepaymentSchedules());

									getView().getLoanPane().getLoanListgrid().getLoanRepaymentListgrid()
											.addRecordsToGrid(result.getLoanRepayments());

								} else {
									SC.say("ERROR", "Unknow error");
								}

							}
						});

			}
		});
	}

	private void onPaymentButtonClicked() {
		getView().getLoanPane().getPaymentButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				makePayment();

			}
		});

	}

	private void makePayment() {
		LoanDepositWindow window = new LoanDepositWindow();
		onSaveButtonClicked(window);
		onloadcurrentinterval(window);
		onDateLoaded(window);
		loadcustomers(window);
		// loadloans(window);
		loadAccounts(window);
///		loadthead(window);
		loadReceipts(window);
		loadremarks(window);
		
		loadcustloans(window);
		loadloan(window);
		// window.setAnimateFadeTime(1);

		window.animateShow();

	}

	private void onloadcurrentinterval(final LoanDepositWindow window) {
		dispatcher.execute(new SysAction(PAConstant.GET_CurrentPeriod), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {
				// TODO Auto-generated method stub
				if (result != null) {
//						
					for (Intervaltb period : result.getIntervaltbs()) {

						window.getCurrentperiod().setValue(period.getFINTNAME());
						// window.getYear().setValue(period.getYRCODE());
						window.getCurrentstartdate().setValue(period.getFSTART());

						window.getCurrentenddate().setValue(period.getFEND());
						// period.window.getPeriodcode().setValue(period.getFINTCODE());

					}
				}
			}
		});
	}

	private void onDateLoaded(final LoanDepositWindow window) {

		window.getDepositdate().addChangedHandler(new ChangedHandler() {

			public void onChanged(ChangedEvent event) {//
				// SC.showPrompt("", "", new SwizimaLoader());

				dispatcher.execute(new SysAction(PAConstant.GET_Interval), new AsyncCallback<SysResult>() {
					public void onFailure(Throwable caught) {
						System.out.println(caught.getMessage());
					}

					public void onSuccess(SysResult result) {
						// TODO Auto-generated method stub
						if (result != null) {
//								
							for (Intervaltb period : result.getIntervaltbs()) {

								final Date dn = window.getDepositdate().getValueAsDate();
								if (dn.before(period.getFEND()) && dn.after(period.getFSTART())) {
									window.getPeriod().setValue(period.getFINTNAME());
									window.getYear().setValue(period.getYRCODE());
//									window.getStartdate().setValue(period.getFSTART());
//									window.getEnddate().setValue(period.getFEND());
									window.getPeriodcode().setValue(period.getFINTCODE());
								}
							}
						}
					}
				});
			}
		});
	}

///////prodcuctid
	private void loadcustomers(final LoanDepositWindow window) {
//		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_customerAccount), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				// SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
					for (CustomerAccount account : result.getCustomerAccounts()) {
						 
						valueMap.put(account.getSysid(), account.getClientCode());
					}

					window.getCustomeraccount().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

	}
//private void clientwithaloan(final LoanDepositWindow window) {
////	window.getLoandisplay().
//}

	private void loadloan(final LoanDepositWindow window) {


		window.getCustomeraccount().addChangedHandler(new ChangedHandler() {

			public void onChanged(ChangedEvent event) {//

		dispatcher.execute(new SysAction(PAConstant.GET_LOAN), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				// SC.clearPrompt();

				if (result != null) {
         
					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
	                 window.getLoan().clearValue();
					String member = window.getLoandisplay().getValueAsString();
  
					for (Loan account : result.getLoans()) {
                         if(member!=null) {
						if(member==account.getMEMBERID()) {
                            if(account.getLOANID()!=null) {
                    	window.getLoan().setValue(account.getLOANID());
                            }  else {
                            	SC.say("the client doesnt have a loan");
                            	String nulls = null;
                            	window.getLoan().setValue(nulls);
                            }
                    }
                    
                         }
					}

				//window.getLoan().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
			}
		});
	}
		
	
		
	
	private void loadcustloans(final LoanDepositWindow window) {


		window.getCustomeraccount().addChangedHandler(new ChangedHandler() {

			public void onChanged(ChangedEvent event) {//

		dispatcher.execute(new SysAction(PAConstant.GET_customerAccount), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				// SC.clearPrompt();

				if (result != null) {
         
					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
					   String member = window.getCustomeraccount().getValueAsString();
                      
					for (CustomerAccount account : result.getCustomerAccounts()) {
                    if(account.getSysid()==member) {
                    	String members= account.getClientCode();
                    	window.getLoandisplay().setValue(members);
                    	
                    }
				    
					}

				//window.getLoan().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
			}
		});
	}
		
	private void loadAccounts(final LoanDepositWindow window) {

		// SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_ACCOUNT), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				// SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
					for (Account account : result.getAccounts()) {
						valueMap.put(account.getSysid(), account.getACODE() + "  " + account.getAccountName());
					}

					window.getAccount().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

//
//	private void loadthead(final LoanDepositWindow window) {
//
//		dispatcher.execute(new SysAction(PAConstant.GET_Thead), new AsyncCallback<SysResult>() {
//			public void onFailure(Throwable caught) {
//				System.out.println(caught.getMessage());
//			}
//
//			public void onSuccess(SysResult result) {
//
//				////SC.clearPrompt();
//
//				if (result != null) {
//
//					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
//					for (Theadp account : result.getTheadps()) {
//						window.getTest().setValue(account.getHDNO());
//					}
//
//				} else {
//					SC.say("ERROR", "Unknow error");
//				}
//
//			}
//		});
//	}
	private void loadremarks(final LoanDepositWindow window) {
		window.getReceipts().addChangedHandler(new ChangedHandler() {

			public void onChanged(ChangedEvent event) {
				dispatcher.execute(new SysAction(PAConstant.GET_RECEIPT), new AsyncCallback<SysResult>() {
					public void onFailure(Throwable caught) {
						System.out.println(caught.getMessage());
					}

					public void onSuccess(SysResult result) {
						// TODO Auto-generated method stub
						if (result != null) {
////        
							String amount = window.getReceipts().getValueAsString();

							String answer = null;
							for (Receiptno period : result.getReceiptnos()) {
								String rt = String.valueOf(period.getTransactionid());
								if (amount == rt) {
									String ans = period.getAMTWORDS();
									window.getAmountinwords().setValue(ans);
//								SC.say("wait");
								}

							}
						}
					}
				});
			}
		});
	}
//	
//	

	private void loadReceipts(final LoanDepositWindow window) {

		dispatcher.execute(new SysAction(PAConstant.GET_RECEIPT), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
					for (Receiptno account : result.getReceiptnos()) {
						/// window.getTest().setValue(account.getHDNO());

						valueMap.put(String.valueOf(account.getTransactionid()), account.getRTVRNO());

					}
					window.getReceipts().setValueMap(valueMap);
				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

	}

//
	private boolean FormValid(final LoanDepositWindow window) {
		if (window.getLoan().getValueAsString() != null) {
			if (window.getCustomeraccount().getValueAsString() != null) {
				if (window.getAccount().getValueAsString() != null) {
					if (window.getLoan().getValueAsString() != null) {
						if ((window.getLoanDeposit().getValueAsString() != null)
								|| (window.getLoaninterestdeposit().getValueAsString() != null)) {
							if ((window.getRemarks().getValueAsString() != null)
									|| (window.getPaydetails().getValueAsString() != null)) {
								return true;

							} else {
								SC.warn("ERROR", "Please  enter remarks and paydetails");

							}

						} else {
							SC.warn("ERROR", "Please  enter either loan deposit and interest amount");

						}
					} else {
						SC.warn("ERROR", "Please Select paying category");

					}
				} else {
					SC.warn("ERROR", "Please Select account type");
				}
			} else {
				SC.warn("ERROR", "Please enter account name");
			}
		} else {
			SC.warn("ERROR", "Please enter loan code");
		}
		return false;
	}

	private void ClearForm(final LoanDepositWindow window) {
		window.getCustomeraccount().clearValue();
		window.getLoan().clearValue();
		window.getAccount().clearValue();
		window.getLoanDeposit().clearValue();
		window.getDepositdate().clearValue();
		window.getPaydetails().clearValue();
		window.getRemarks().clearValue();

	}

	private void onSaveButtonClicked(final LoanDepositWindow window) {
		window.getSaveButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				if (FormValid(window)) {

					Loan loans = new Loan();
					loans.setLOANID(window.getLoan().getValueAsString());
					
					
					
					CustomerAccount customeraccounts = new CustomerAccount();
					customeraccounts.setSysid(window.getCustomeraccount().getValueAsString());
					Account accounts = new Account();
					accounts.setSysid(window.getAccount().getValueAsString());
					/// String thead = window.getTest().getValueAsString();
					//
					// here is were am making the thead count
					// 
					// int newthead = Integer.parseInt(thead) + 1;
					 

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					LoanDeposit loandeposit = new LoanDeposit();
					loandeposit.setAccount(accounts);
					////////////////// loandeposit.setThead(String.valueOf(newthead));
					if(loans!=null) {
					loandeposit.setLoan(loans);
					}
					else {
						loandeposit.setLoan(null);
	
					}
					loandeposit.setCustomeraccount(customeraccounts);
					loandeposit.setStatus(Status.ACTIVE);
					loandeposit.setDateCreated(new Date());
					loandeposit.setDateUpdated(new Date());
					loandeposit.setPaydetails(window.getPaydetails().getValueAsString());
					loandeposit.setRemarks(window.getRemarks().getValueAsString());
					float amountReceived = Float.parseFloat(
							PAManager.getInstance().unformatCash(window.getLoanDeposit().getValueAsString()));
					float interestReceived = Float.parseFloat(
							PAManager.getInstance().unformatCash(window.getLoaninterestdeposit().getValueAsString()));
					// account.setUpdatedBy(updatedBy);
					loandeposit.setPeriod(window.getPeriod().getValueAsString());
					loandeposit.setYear(window.getYear().getValueAsString());
					loandeposit.setPeriodcode(window.getPeriodcode().getValueAsString());
					loandeposit.setLoandeposit(amountReceived);
					loandeposit.setInterestdeposit(interestReceived);
					loandeposit.setDepositdate(window.getDepositdate().getValueAsDate());

//					
//					SC.showPrompt("", "", new SwizimaLoader());
//
//					dispatcher.execute(new SysAction(PAConstant.SAVE_Thead, thead1), new AsyncCallback<SysResult>() {
//						public void onFailure(Throwable caught) {
//							System.out.println(caught.getMessage());
//						}
//
//						public void onSuccess(SysResult result) {
//
//							SC.clearPrompt();
//
//							if (result != null) {
//
//								if (result.getSwizFeedback().isResponse()) {
//									ClearForm(window);
//									SC.say("SUCCESS", result.getSwizFeedback().getMessage());
//
//								} else {
//									SC.warn("ERROR", result.getSwizFeedback().getMessage());
//								}
//
//							} else {
//								SC.say("ERROR", "Unknow error");
//							}
//
//						}
//					});
////
					SC.showPrompt("", "", new SwizimaLoader());

					dispatcher.execute(new SysAction(PAConstant.SAVE_LOANDEPOSITS, loandeposit),
							new AsyncCallback<SysResult>() {
								public void onFailure(Throwable caught) {
									System.out.println(caught.getMessage());
								}

								public void onSuccess(SysResult result) {

									SC.clearPrompt();
									ClearForm(window);
									if (result != null) {

										if (result.getSwizFeedback().isResponse()) {

											SC.say("SUCCESS", result.getSwizFeedback().getMessage());

//											getView().getLoanDepositPane().getLoanDepositListgrid()
//													.addRecordsToGrid(result.getLoandeposits());

										} else {
											SC.warn("ERROR", result.getSwizFeedback().getMessage());
										}

									} else {
										SC.say("ERROR", "Unknow error");
									}

								}
							});

				}
			}
		});
	}
	/// }
//			}
//		});
	// }
	
	private void Loaninterestwindow() {
		ComputeinterestWindow window = new ComputeinterestWindow();
		
		ongetlastinterest(window);
		ongetlastinterest(	window);	
				onloaddatefromfixeddata(window);
////		onDateLoaded(window);
	///	ongetlastinterest(window);
		//onloaddatefromfixeddata(window);
		onsavebuttonClicked(window);
		///// window.animateFade(10);
		window.show();
	}

	private void onDateLoaded(final ComputeinterestWindow window) {
//		window.getFixeddate().addChangedHandler(new ChangedHandler() {
//			public void onChanged(ChangedEvent event) {
		SC.showPrompt("", "", new SwizimaLoader());
		dispatcher.execute(new SysAction(PAConstant.GET_Interval), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {
				// TODO Auto-generated method stub
				SC.clearPrompt();
				if (result != null) {
//								
					for (Intervaltb period : result.getIntervaltbs()) {

						final Date dn = window.getFixeddate().getValueAsDate();
						if (dn.before(period.getFEND()) && dn.after(period.getFSTART())) {
							window.getPeriodcode().setValue(period.getFINTCODE());
							window.getYear().setValue(period.getYRCODE());

//									window.getAssessmentPeriod().setValue(period.getFINTNAME());
//									window.getFinancialYear().setValue();
									window.getStartDate().setValue(period.getFSTART());
									window.getEndDate().setValue(period.getFEND());
									window.getPeriod().setValue(period.getFINTNAME());


//									window.getAssessmentPeriod().setValue(period.getFINTNAME());
//									window.getFinancialYear().setValue();
//									window.getFrom().setValue(period.getFSTART());
//									window.getTo().setValue(period.getF END());
//									window.getPeriodcode().setValue(period.getFINTCODE());

						}
					}
				}
			}

		//	@Override
			//public void onBlur(BlurEvent event) {
				// TODO Auto-generated method stub
				
			//}
		});
//            }
//            });
            }

	// @Override
	// TODO Auto-generated method stub

	// }

	private void ongetlastinterest(final ComputeinterestWindow window) {
///**/
//*here we are geting the last interest date to display to the client 
//\**

		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_Totalinterest), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();
				// Date tdate = null;
				if (result != null) {

					for (Totalinterest totalint : result.getTotalinterests()) {
						// Date tdate = totalint.getTdate();
						window.getFsave().setValue(totalint.getFsave());
						window.getLastintdate().setValue(totalint.getTdate());

					}

					// getView().getLoanbalancePane().getFixeddata().setValueMap(valueMap);
				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

		// }
		// });
	}
	private void load() {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_LoanBlanceQry), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

				///	getView().getLoanbalancePane().getLoanbalanceListgrid().addRecordsToGrid(result.getLoanbalances());

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}
	private void onloaddatefromfixeddata(final ComputeinterestWindow window) {
		//// here we are picking the interest date to be computed

		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_FixedData), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();
				// Date tdate = null;
				if (result != null) {
					// LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
					for (FixedData fixeddata : result.getFixeddatas()) {
						Date tdate = fixeddata.getNextIntDte();
						window.getFixeddate().setValue(tdate);
					}
					onDateLoaded(window);
					// getView().getLoanbalancePane().getFixeddata().setValueMap(valueMap);
				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

		// }
		// });
	}

	private boolean FormValid(final ComputeinterestWindow window) {
		if (window.getYear().getValueAsString() != null) {
			if (window.getPeriodcode().getValueAsString() != null) {
				if (window.getFixeddate().getValueAsDate() != null) {
					return true;
				} else {
					SC.warn("ERROR", "Please adjust the system date ");

				}
			} else {
				SC.warn("ERROR", "Please adjust the system date ");

			}
		} else {
			SC.warn("ERROR", "Please adjust the system date");

		}
		return false;
	}

	private void onsavebuttonClicked(final ComputeinterestWindow window) {
		window.getSaveButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				if (FormValid(window)) {

					Totalinterest dayinterest = new Totalinterest();

					dayinterest.setPeriodcode(window.getPeriodcode().getValueAsString());
					dayinterest.setYearcode(Integer.parseInt(window.getYear().getValueAsString()));
					dayinterest.setFsave("Y");
					dayinterest.setTdate(window.getFixeddate().getValueAsDate());
//				
					SC.showPrompt("", "", new SwizimaLoader());

					dispatcher.execute(new SysAction(PAConstant.SAVE_Totalinterest, dayinterest),
							new AsyncCallback<SysResult>() {
								public void onFailure(Throwable caught) {
									System.out.println(caught.getMessage());
								}

								public void onSuccess(SysResult result) {

									SC.clearPrompt();

									if (result != null) {

										if (result.getSwizFeedback().isResponse()) {

											SC.say("SUCCESS", result.getSwizFeedback().getMessage());
											/// SC.say("wait for the fixed date ");
											load();
											/// shows();
											onloaddatefromfixeddata(window);
											ongetlastinterest(window);
											//// .addRecordsToGrid(result.getCustomerAccounts());
										} else {
											SC.warn("ERROR", result.getSwizFeedback().getMessage());
										}

									} else {
										SC.say("ERROR", "Unknow error");
									}

								}
							});

				}
			}
		});
	}

	
	
	

	private void onEditButtonClicked() {
		getView().getLoanPane().getEditButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				final SalaryWindow window = new SalaryWindow();
				window.getSaveButton().addClickHandler(new ClickHandler() {

					public void onClick(ClickEvent event) {

						Salary salary = new Salary();
						salary.setAmount(Float.parseFloat(window.getAmount().getValueAsString()));
						salary.setFirstName(window.getAccountName().getValueAsString());
						salary.setTransactionDate(window.getDate().getValueAsDate());

						SC.showPrompt("", "", new SwizimaLoader());

						dispatcher.execute(new SysAction(PAConstant.SAVE_SALARY, salary),
								new AsyncCallback<SysResult>() {
									public void onFailure(Throwable caught) {
										System.out.println(caught.getMessage());
									}

									public void onSuccess(SysResult result) {

										SC.clearPrompt();

										if (result != null) {

											if (result.getSwizFeedback().isResponse()) {

												SC.say("SUCCESS", result.getSwizFeedback().getMessage());

												window.getSalaryListgrid().addRecordsToGrid(result.getSalaries());
											} else {
												SC.warn("ERROR", result.getSwizFeedback().getMessage());
											}

										} else {
											SC.say("ERROR", "Unknow error");
										}

									}
								});

					}
				});
				window.show();
			}
		});
	}

}
