package com.pearl.accounting.sysui.client.views.windows;

import com.pearl.accounting.sysui.client.views.widgets.ComboBox;
import com.pearl.accounting.sysui.client.views.widgets.TextField;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class MyWindow extends Window {

	private TextField accountCode;
	private TextField accountName;

	private ComboBox accountType;

	private ComboBox category;
	private ComboBox tackable;
	
	private ComboBox productType;

	private IButton saveButton;
	private IButton saveButtonx;

	public MyWindow() {
		super();
		accountCode = new TextField();
		accountCode.setTitle("Account Code");

		accountName = new TextField();
		accountName.setTitle("Account Name");

		accountType = new ComboBox();
		accountType.setTitle("Account Type");

		category = new ComboBox();
		category.setTitle("Account Category");

		tackable = new ComboBox();
		tackable.setTitle("Track personal data?");
		
		productType=new ComboBox();
		productType.setTitle("Product Type");

		saveButton = new IButton("Save");
		saveButtonx = new IButton("Saveagain");

		DynamicForm form = new DynamicForm();
		form.setFields(accountCode, accountName, accountType, category, tackable,productType);
		form.setWrapItemTitles(true);
		form.setMargin(10);

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(saveButton,saveButtonx);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(form);
		layout.addMember(buttonLayout);

		layout.setMargin(10);
		this.addItem(layout);
		this.setWidth("40%");
		this.setHeight("40%");
		this.setAutoCenter(true);
		this.setTitle("Account Definition");
		this.setIsModal(true);
		this.setShowModalMask(true);
	}

	public TextField getAccountCode() {
		return accountCode;
	}

	public TextField getAccountName() {
		return accountName;
	}

	public ComboBox getAccountType() {
		return accountType;
	}

	public ComboBox getCategory() {
		return category;
	}

	public ComboBox getTackable() {
		return tackable;
	}

	public IButton getSaveButton() {
		return saveButton;
	}
	public IButton getSaveButtonx() {
		return saveButtonx;
	}

	public ComboBox getProductType() {
		return productType;
	}

}
