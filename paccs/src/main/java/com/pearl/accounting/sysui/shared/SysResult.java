package com.pearl.accounting.sysui.shared;

import java.util.List;

import com.gwtplatform.dispatch.shared.Result;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.AssessmentPeriod;
import com.pearl.accounting.sysmodel.AtypeA;
import com.pearl.accounting.sysmodel.CashBookPayment;
import com.pearl.accounting.sysmodel.CashBookPaymentDetail;
import com.pearl.accounting.sysmodel.CashBookReceipt;
import com.pearl.accounting.sysmodel.CashBookReceiptDetail;
import com.pearl.accounting.sysmodel.ClientStatement;
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.DoubleEntry;
import com.pearl.accounting.sysmodel.FinancialYear;
import com.pearl.accounting.sysmodel.FixedData;
import com.pearl.accounting.sysmodel.GeneralLedger;
import com.pearl.accounting.sysmodel.Intervaltb;
import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysmodel.LoanDeposit;
import com.pearl.accounting.sysmodel.LoanRepayment;
import com.pearl.accounting.sysmodel.LoanRepayments;
import com.pearl.accounting.sysmodel.LoanbalanceView;
import com.pearl.accounting.sysmodel.LoaninterestView;
import com.pearl.accounting.sysmodel.Multi;
import com.pearl.accounting.sysmodel.MultiDetail;
import com.pearl.accounting.sysmodel.MultiJournal;
import com.pearl.accounting.sysmodel.MultiJournalDetail;
import com.pearl.accounting.sysmodel.PeriodicDeposit;
import com.pearl.accounting.sysmodel.ProductSetup;
import com.pearl.accounting.sysmodel.Receiptno;
//import com.pearl.accounting.sysmodel.ProductTest;
import com.pearl.accounting.sysmodel.RepaymentSchedule;
import com.pearl.accounting.sysmodel.Salary;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.SystemUser;
import com.pearl.accounting.sysmodel.Theadp;
import com.pearl.accounting.sysmodel.Totalinterest;
import com.pearl.accounting.sysmodel.TransGL;
import com.pearl.accounting.sysmodel.TransML;
import com.pearl.accounting.sysmodel.UserPermission;
import com.pearl.accounting.sysmodel.UserRole;
import com.pearl.accounting.sysmodel.Withdrawal;
//import com.pearl.accounting.sysmodel.productdetails;

public class SysResult implements Result {

	private boolean response;
	private SwizFeedback swizFeedback;

	private List<AssessmentPeriod> assessmentPeriods;
	private List<Receiptno> receiptnos;

	private List<FinancialYear> financialYears;
	private List<UserRole> userRoles;
	private List<UserPermission> userPermissions;
	private List<SystemUser> systemUsers;
	private List<Intervaltb> intervals;
	private List<DoubleEntry> doubleentrys;
	private List<LoanRepayments> loanrepayments;
	private List<LoanbalanceView> loanbalances;
	private List<LoaninterestView> loaninterestviews;
	private List<FixedData> fixeddatas;
	private List<Totalinterest> totalinterests;

	private List<Account> accounts;
	private List<Multi> multis;
	private List<TransML> transmls;
	private List<AtypeA> atypeas;
	private List<CustomerAccount> customerAccounts;

	private List<PeriodicDeposit> periodicDeposits;
	private List<LoanDeposit> loandeposits;

	private List<Loan> loans;

	private List<Withdrawal> withdrawals;

	private List<RepaymentSchedule> repaymentSchedules;

	private List<LoanRepayment> loanRepayments;

	private List<GeneralLedger> generalLedgers;

	private List<ClientStatement> clientStatements;

	private List<CashBookPayment> cashBookPayments;

	private List<CashBookPaymentDetail> cashBookPaymentDetails;

	private List<CashBookReceipt> cashBookReceipts;

	private List<CashBookReceiptDetail> bookReceiptDetails;

	private List<MultiJournal> multiJournals;

	private List<MultiJournalDetail> multiJournalDetails;
	private List<MultiDetail> multiDetails;

	private List<Salary> salaries;
	private List<TransGL> transgls;

	private List<Theadp> theadps;

	private List<ProductSetup> productsetups;

	public SysResult(SwizFeedback swizFeedback, List<Account> accounts, List<CustomerAccount> customerAccounts,
			List<Loan> loans, CustomerAccount account) {
		this.swizFeedback = swizFeedback;
		this.accounts = accounts;
		this.customerAccounts = customerAccounts;
		this.loans = loans;
		// this.Productdetails=Productdetails;

	}

	public SysResult(SwizFeedback swizFeedback, List<LoanbalanceView> loanbalances, LoanbalanceView loanbalance) {
		this.swizFeedback = swizFeedback;
		this.loanbalances = loanbalances;

	}

	

	public SysResult(SwizFeedback swizFeedback, List<Receiptno> receiptnos, Receiptno receiptno) {
		this.swizFeedback = swizFeedback;
		this.receiptnos = receiptnos;

	}

	
	public SysResult(SwizFeedback swizFeedback, List<Totalinterest> totalinterests, Totalinterest totalinterest) {
		this.swizFeedback = swizFeedback;
		this.totalinterests = totalinterests;

	}

	public SysResult(SwizFeedback swizFeedback, List<LoaninterestView> loaninterestviews,
			LoaninterestView loaninterestview) {
		this.swizFeedback = swizFeedback;
		this.loaninterestviews = loaninterestviews;

	}

	public SysResult(SwizFeedback swizFeedback, List<FixedData> fixeddatas, FixedData fixeddata) {
		this.swizFeedback = swizFeedback;
		this.fixeddatas = fixeddatas;

	}

	public SysResult(SwizFeedback swizFeedback, List<ProductSetup> productsetups, ProductSetup productsetup) {
		this.swizFeedback = swizFeedback;
		this.productsetups = productsetups;

	}

	public SysResult(SwizFeedback swizFeedback, List<LoanRepayments> loanrepayments, LoanRepayments loanrepayment) {
		this.swizFeedback = swizFeedback;
		this.loanrepayments = loanrepayments;

	}

	public SysResult(SwizFeedback swizFeedback, List<Salary> salaries, Salary salary) {
		this.swizFeedback = swizFeedback;
		this.salaries = salaries;

	}

	public SysResult(SwizFeedback swizFeedback, List<TransML> transmls, TransML transml) {
		this.swizFeedback = swizFeedback;
		this.transmls = transmls;

	}

	public SysResult(SwizFeedback swizFeedback, List<AtypeA> atypeas, AtypeA atypea) {
		this.swizFeedback = swizFeedback;
		this.atypeas = atypeas;

	}

	public SysResult(SwizFeedback swizFeedback, List<DoubleEntry> doubleentrys, DoubleEntry doubleentry) {
		this.swizFeedback = swizFeedback;
		this.doubleentrys = doubleentrys;

	}

	public SysResult(SwizFeedback swizFeedback, List<TransGL> transgls, TransGL transgl) {
		this.swizFeedback = swizFeedback;
		this.transgls = transgls;

	}

	public SysResult(SwizFeedback swizFeedback, List<Theadp> theadps, Theadp theadp) {
		this.swizFeedback = swizFeedback;
		this.theadps = theadps;
	}

	public SysResult(SwizFeedback swizFeedback, List<Multi> multis, Multi multi) {
		this.swizFeedback = swizFeedback;
		this.multis = multis;
	}

	public SysResult(SwizFeedback swizFeedback, List<Intervaltb> intervals, Intervaltb interval) {
		this.swizFeedback = swizFeedback;
		this.intervals = intervals;
	}

	public SysResult(SwizFeedback swizFeedback, List<MultiJournalDetail> multiJournalDetails,
			MultiJournalDetail journalDetail) {
		this.swizFeedback = swizFeedback;
		this.multiJournalDetails = multiJournalDetails;

	}

	public SysResult(SwizFeedback swizFeedback, List<MultiDetail> multiDetails, MultiDetail Detail) {
		this.swizFeedback = swizFeedback;
		this.multiDetails = multiDetails;

	}

	public SysResult(SwizFeedback swizFeedback, List<MultiJournal> multiJournals, MultiJournal multiJournal) {
		this.swizFeedback = swizFeedback;
		this.multiJournals = multiJournals;

	}

	public SysResult(SwizFeedback swizFeedback, List<CashBookReceiptDetail> bookReceiptDetails,
			CashBookReceiptDetail bookReceiptDetail) {
		this.swizFeedback = swizFeedback;
		this.bookReceiptDetails = bookReceiptDetails;

	}

	public SysResult(SwizFeedback swizFeedback, List<CashBookReceipt> cashBookReceipts,
			CashBookReceipt cashBookReceipt) {
		this.swizFeedback = swizFeedback;
		this.cashBookReceipts = cashBookReceipts;

	}

	public SysResult(SwizFeedback swizFeedback, List<CashBookPaymentDetail> cashBookPaymentDetails,
			CashBookPaymentDetail bookPaymentDetail) {
		this.swizFeedback = swizFeedback;
		this.cashBookPaymentDetails = cashBookPaymentDetails;

	}

	public SysResult(SwizFeedback swizFeedback, List<CashBookPayment> cashBookPayments,
			CashBookPayment cashBookPayment) {
		this.swizFeedback = swizFeedback;
		this.cashBookPayments = cashBookPayments;

	}

	public SysResult(SwizFeedback swizFeedback, List<ClientStatement> clientStatements,
			ClientStatement clientStatement) {
		this.swizFeedback = swizFeedback;
		this.clientStatements = clientStatements;

	}

	public SysResult(SwizFeedback swizFeedback, List<GeneralLedger> generalLedgers, GeneralLedger generalLedger) {
		this.swizFeedback = swizFeedback;
		this.generalLedgers = generalLedgers;

	}

	public SysResult(SwizFeedback swizFeedback, List<RepaymentSchedule> repaymentSchedules,
			List<LoanRepayment> loanRepayments, RepaymentSchedule repaymentSchedule) {
		this.swizFeedback = swizFeedback;
		this.repaymentSchedules = repaymentSchedules;
		this.loanRepayments = loanRepayments;

	}

	public SysResult(SwizFeedback swizFeedback, List<RepaymentSchedule> repaymentSchedules,
			RepaymentSchedule repaymentSchedule) {
		this.swizFeedback = swizFeedback;
		this.repaymentSchedules = repaymentSchedules;

	}

	public SysResult(SwizFeedback swizFeedback, List<Withdrawal> withdrawals, Withdrawal withdrawal) {
		this.swizFeedback = swizFeedback;
		this.withdrawals = withdrawals;

	}

	public SysResult(SwizFeedback swizFeedback, List<Loan> loans, Loan loan) {
		this.swizFeedback = swizFeedback;
		this.loans = loans;

	}

	public SysResult(SwizFeedback swizFeedback, List<LoanDeposit> loandeposits, LoanDeposit loandeposit) {
		this.swizFeedback = swizFeedback;
		this.loandeposits = loandeposits;

	}

	public SysResult(SwizFeedback swizFeedback, List<PeriodicDeposit> periodicDeposits,
			PeriodicDeposit PeriodicDeposit) {
		this.swizFeedback = swizFeedback;
		this.periodicDeposits = periodicDeposits;

	}

	public SysResult(SwizFeedback swizFeedback, List<CustomerAccount> customerAccounts,
			CustomerAccount customerAccount) {
		this.swizFeedback = swizFeedback;
		this.customerAccounts = customerAccounts;

	}

	public SysResult(SwizFeedback swizFeedback, List<Account> accounts, Account account) {
		this.swizFeedback = swizFeedback;
		this.accounts = accounts;

	}

	public SysResult(SwizFeedback swizFeedback, List<SystemUser> systemUsers, SystemUser systemUser) {
		this.swizFeedback = swizFeedback;
		this.systemUsers = systemUsers;

	}

	public SysResult(SwizFeedback swizFeedback, List<UserPermission> userPermissions, UserPermission userPermission) {
		this.swizFeedback = swizFeedback;
		this.userPermissions = userPermissions;

	}

	public SysResult(SwizFeedback swizFeedback, List<UserRole> userRoles, UserRole userRole) {
		this.swizFeedback = swizFeedback;
		this.userRoles = userRoles;

	}

	public SysResult(SwizFeedback swizFeedback, List<AssessmentPeriod> assessmentPeriods,
			AssessmentPeriod assessmentPeriod) {
		this.swizFeedback = swizFeedback;
		this.assessmentPeriods = assessmentPeriods;

	}

	public SysResult(SwizFeedback swizFeedback, List<FinancialYear> financialYears, FinancialYear assessmentPeriod) {
		this.swizFeedback = swizFeedback;
		this.financialYears = financialYears;

	}
//	public SysResult(SwizFeedback swizFeedback, List<productdetails> Productdetails,
//			productdetails ProductDetail) {
//		this.swizFeedback = swizFeedback;
//		this.Productdetails=Productdetails;
//	}
//	public SysResult(SwizFeedback swizFeedback, List<ProductTest> productTest,
//			ProductTest productTests) {
//		this.swizFeedback = swizFeedback;
//		this.productTest=productTest;
//	}

	public SysResult() {

	}

	public boolean isResponse() {
		return response;
	}

	public SwizFeedback getSwizFeedback() {
		return swizFeedback;
	}

	public List<AssessmentPeriod> getAssessmentPeriods() {
		return assessmentPeriods;
	}

	public List<LoanDeposit> getLoandeposits() {
		return loandeposits;
	}

	public List<LoanRepayments> getLoanRepaymentss() {
		return loanrepayments;
	}

	public List<AtypeA> getAtypeAs() {
		return atypeas;
	}

	public List<TransGL> getTransGLs() {
		return transgls;
	}

	public List<Theadp> getTheadps() {
		return theadps;
	}

	public List<Multi> getMultis() {
		return multis;
	}

	public List<FinancialYear> getFinancialYears() {
		return financialYears;
	}

	public List<UserRole> getUserRoles() {
		return userRoles;
	}

	public List<UserPermission> getUserPermissions() {
		return userPermissions;
	}

	public List<SystemUser> getSystemUsers() {
		return systemUsers;
	}

	public List<Account> getAccounts() {
		return accounts;
	}

	public List<Intervaltb> getIntervaltbs() {
		return intervals;
	}

	public List<Totalinterest> getTotalinterests() {
		return totalinterests;
	}

	public List<CustomerAccount> getCustomerAccounts() {
		return customerAccounts;
	}

	public List<PeriodicDeposit> getPeriodicDeposits() {
		return periodicDeposits;
	}

	public List<Loan> getLoans() {
		return loans;
	}

	public List<Withdrawal> getWithdrawals() {
		return withdrawals;
	}

	public List<RepaymentSchedule> getRepaymentSchedules() {
		return repaymentSchedules;
	}

	public List<LoanRepayment> getLoanRepayments() {
		return loanRepayments;
	}

	public List<LoanbalanceView> getLoanbalances() {
		return loanbalances;
	}

	public List<LoaninterestView> getLoaninterestviews() {
		return loaninterestviews;
	}

	public List<FixedData> getFixeddatas() {
		return fixeddatas;
	}

	public List<GeneralLedger> getGeneralLedgers() {
		return generalLedgers;
	}

	public List<TransML> getTransMLs() {
		return transmls;
	}

	public List<DoubleEntry> getDoubleEntrys() {
		return doubleentrys;
	}

	public List<ClientStatement> getClientStatements() {
		return clientStatements;
	}

	public List<CashBookPayment> getCashBookPayments() {
		return cashBookPayments;
	}

	public List<CashBookPaymentDetail> getCashBookPaymentDetails() {
		return cashBookPaymentDetails;
	}

	public List<CashBookReceipt> getCashBookReceipts() {
		return cashBookReceipts;
	}

	public List<CashBookReceiptDetail> getBookReceiptDetails() {
		return bookReceiptDetails;
	}

	public List<MultiJournalDetail> getMultiJournalDetails() {
		return multiJournalDetails;
	}

	public List<MultiJournal> getMultiJournals() {
		return multiJournals;
	}

	public List<MultiDetail> getMultiDetails() {
		return multiDetails;
	}
//	public List<MultiDetail> getMultiJournalDetails() {
//		return multiJournalDetails;
//	}

	public List<Salary> getSalaries() {
		return salaries;
	}

	public List<ProductSetup> getProductSetups() {
		return productsetups;
	}
//
//	public List<ProductTest> getProductTest() {
//		return productTest;
//	}

	public List<Receiptno> getReceiptnos() {
		return receiptnos;
	}

}
