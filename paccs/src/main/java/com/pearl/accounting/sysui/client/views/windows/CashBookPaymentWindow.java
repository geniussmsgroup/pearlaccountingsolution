package com.pearl.accounting.sysui.client.views.windows;

import java.util.List;

import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysui.client.views.listgrids.CashBookPaymentDetailListgrid;
import com.pearl.accounting.sysui.client.views.widgets.ComboBox;
import com.pearl.accounting.sysui.client.views.widgets.TextField;
import com.pearl.accounting.sysui.client.views.widgets.ActivePeriod;

import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.util.DateDisplayFormatter;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.NavigationBar;
import com.smartgwt.client.widgets.layout.VLayout;

public class CashBookPaymentWindow extends Window {
	  private TextField currentperiod;
	    private DateItem currentstartdate;
	    private DateItem currentenddate;
	private TextField payee;
	private TextField paidAmount;
	private ComboBox assessmentPeriod;
	private ComboBox financialYear;
	private TextField periodcode;
	private DateItem paymentDate;
	private ComboBox payingAccount;
	private TextField paymentDetails;
	private TextField remarks;
	private static DateItem from;
	private static DateItem to;
	private ActivePeriod acti;

	public String tilt;
//	private DateItem startPeriod;
//	private DateItem endPeriod;
//	private FinancialYearWindow fwin;

	private CashBookPaymentDetailListgrid cashBookPaymentDetailListgrid;

	private IButton saveButton;
	private IButton addButton;
	private IButton deleteButton;
	private IButton prd;

	public CashBookPaymentWindow(List<Account> accounts) {
		super();

		currentperiod = new TextField();
		currentperiod.setTitle("currentperiod");
		currentperiod.setWidth("10px");

		currentstartdate = new DateItem();
		currentstartdate.setTitle("Transaction Date");
		currentstartdate.setUseTextField(true);
		currentstartdate.setWidth("*");
		currentstartdate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		currentenddate = new DateItem();
		currentenddate.setTitle("Transaction Date");
		currentenddate.setUseTextField(true);
		currentstartdate.setWidth("*");
		currentstartdate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
		
		
		
		acti = new ActivePeriod();
		financialYear = new ComboBox();
		financialYear.setTitle("Year");

		assessmentPeriod = new ComboBox();
		assessmentPeriod.setTitle("Period");
		periodcode = new TextField();
		periodcode.setTitle("Periodcode");
		periodcode.setWidth("10px");
		payee = new TextField();
		payee.setTitle("Payee");

		from = new DateItem();
		from.setTitle("From");
		from.setUseTextField(true);
		from.setWidth("10px");
		from.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
		///

		to = new DateItem();
		to.setTitle("To");
		to.setUseTextField(true);
		to.setWidth("10px");
		to.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
		///

		paidAmount = new TextField();
		paidAmount.setTitle("Amount");
		paidAmount.setKeyPressFilter("[0-9]");

		paymentDate = new DateItem();
		paymentDate.setTitle("Payment Date");
		paymentDate.setUseTextField(true);
		paymentDate.setWidth("*");
		paymentDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		payingAccount = new ComboBox();
		payingAccount.setTitle("Paying A/C");

		paymentDetails = new TextField();
		paymentDetails.setTitle("Voucher No.");

		remarks = new TextField();
		remarks.setTitle("Remarks");

		saveButton = new IButton("Save");
		addButton = new IButton("Add");
		deleteButton = new IButton("Delete");
		prd = new IButton("prd");

		TextField den;

		cashBookPaymentDetailListgrid = new CashBookPaymentDetailListgrid(accounts);

		DynamicForm form = new DynamicForm();
		DynamicForm Aform = new DynamicForm();
//		Aform.setFields(period);
		form.setFields(from, to, assessmentPeriod, periodcode, financialYear);
		form.setWrapItemTitles(true);
		form.setMargin(10);
		form.setNumCols(8);

		DynamicForm form2 = new DynamicForm();
		form2.setFields(payee, paidAmount, paymentDate, payingAccount, paymentDetails, remarks);
		form2.setWrapItemTitles(true);
		form2.setMargin(10);
		form2.setNumCols(4);
		DynamicForm form4 = new DynamicForm();
		form4.setFields(currentstartdate, currentperiod,currentenddate);
		form4.setWrapItemTitles(true);
		form4.setMargin(10);
		form4.setNumCols(6);

//		startPeriod.setValue(fwin.getStartDate());
//		endPeriod.setValue(fwin.getEndDate());
//		DynamicForm period = new DynamicForm();
//		period.setFields(startPeriod,endPeriod);
//		period.setTitle("Period");
//		
		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(saveButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(4);

		Label subheader = new Label();
		subheader.setStyleName("crm-ContextArea-Header-Label");
		subheader.setContents("Receiving accounts details");
		subheader.setWidth("1%");
		subheader.setAutoHeight();
		// subheader.setMargin(10);
		subheader.setAlign(Alignment.LEFT);

		HLayout subbuttonLayout = new HLayout();
		subbuttonLayout.setMembers(addButton, deleteButton, subheader);
		subbuttonLayout.setAutoHeight();
		subbuttonLayout.setWidth100();
		subbuttonLayout.setMargin(5);
		// subbuttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMembers(form4, form, form2);
	//	layout.addMembers(acti, form, form2);

		layout.addMember(subbuttonLayout);
		layout.addMember(cashBookPaymentDetailListgrid);
		layout.addMember(buttonLayout);
		layout.setMargin(10);

		this.addItem(layout);
		this.setWidth("70%");
		this.setHeight("70%");
		this.setAutoCenter(true);
		this.setTitle("Cashbook payement definition");
		this.setIsModal(true);
		this.setShowModalMask(true);
	}

	
	
	
	
	
	
	
	public TextField getCurrentperiod() {
		return currentperiod;
	}








	public DateItem getCurrentstartdate() {
		return currentstartdate;
	}








	public DateItem getCurrentenddate() {
		return currentenddate;
	}








	public TextField getPayee() {
		return payee;
	}

	public TextField getPaidAmount() {
		return paidAmount;
	}

	public ComboBox getAssessmentPeriod() {
		return assessmentPeriod;
	}

	public ComboBox getFinancialYear() {
		return financialYear;
	}

	public DateItem getPaymentDate() {
		return paymentDate;
	}

	public ComboBox getPayingAccount() {
		return payingAccount;
	}

	public TextField getPaymentDetails() {
		return paymentDetails;
	}

	public TextField getRemarks() {
		return remarks;
	}

	public CashBookPaymentDetailListgrid getCashBookPaymentDetailListgrid() {
		return cashBookPaymentDetailListgrid;
	}

	public IButton getSaveButton() {
		return saveButton;
	}

	public IButton getAddButton() {
		return addButton;
	}

	public IButton getDeleteButton() {
		return deleteButton;
	}

	public void setDeleteButton(IButton deleteButton) {
		this.deleteButton = deleteButton;
	}

	public TextField getPeriodcode() {
		return periodcode;
	}

	public DateItem getFrom() {
		return from;
	}

	public static void setFrom(DateItem from) {
		CashBookPaymentWindow.from = from;
	}

	public DateItem getTo() {
		return to;
	}

	public static void setTo(DateItem to) {
		CashBookPaymentWindow.to = to;
	}

	public IButton getPrd() {
		return prd;
	}

	public void setPrd(IButton prd) {
		this.prd = prd;
	}

	public ActivePeriod getActi() {
		return acti;
	}

	public void setActi(ActivePeriod acti) {
		this.acti = acti;
	}

}
