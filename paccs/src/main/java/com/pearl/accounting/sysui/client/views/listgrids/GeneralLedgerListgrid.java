package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.google.gwt.i18n.client.NumberFormat;
import com.pearl.accounting.sysmodel.GeneralLedger;
import com.pearl.accounting.sysui.client.utils.PAManager;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.grid.HeaderSpan;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;

public class GeneralLedgerListgrid extends SuperListGrid {

	public static String ID = "id";

	public static String FinancialYear = "financialYear";
	public static String AssessmentPeriod = "assessmentPeriod";

	public static String TransationDate = "transationDate";
	public static String TransactionDescription = "transactionDescription";
	public static String PostReferenceNo = "postReferenceNo";
	public static String Account = "account";
	public static String AccountCode = "accountCode";
	public static String TransactionType = "transactionType";
	public static String TransactionDebt = "transactionDebt";
	public static String TransactionCredit = "transactionCredit";
	public static String BalanceDebt = "balanceDebt";
	public static String BalanceCredit = "balanceCredit";

	public static String PaymentDetails = "paymentDetails";
	public static String Remarks = "remarks";
	public static String ProductType = "productType";
	
	NumberFormat nf = NumberFormat.getFormat("#,##0.00");

	public GeneralLedgerListgrid() {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField financialYear = new ListGridField(FinancialYear, "Year");
		ListGridField assessmentPeriod = new ListGridField(AssessmentPeriod, "Period");

		ListGridField transationDate = new ListGridField(TransationDate, "Date");
		ListGridField transactionDescription = new ListGridField(TransactionDescription, "Description");

		ListGridField postReferenceNo = new ListGridField(PostReferenceNo, "Ref.");
		postReferenceNo.setHidden(true);
		
		ListGridField account = new ListGridField(Account, "Account");
		ListGridField accountCode = new ListGridField(AccountCode, "Acc. Code");

		ListGridField transactionType = new ListGridField(TransactionType, "Type");
		
		ListGridField transactionDebt = new ListGridField(TransactionDebt, "Debt");
		transactionDebt.setShowGridSummary(true);

		transactionDebt.setSummaryFunction(new SummaryFunction() {
			public Object getSummaryValue(Record[] records, ListGridField field) {

				long totalAmount = 0;
				for (Record record : records) {

					totalAmount += Float
							.parseFloat(PAManager.getInstance().unformatCash(record.getAttribute(TransactionDebt)));

				}
				return "" + PAManager.getInstance().formatCash(totalAmount);
			}
		});
		
		ListGridField transactionCredit = new ListGridField(TransactionCredit, "Credit");
		transactionCredit.setShowGridSummary(true);

		transactionCredit.setSummaryFunction(new SummaryFunction() {
			public Object getSummaryValue(Record[] records, ListGridField field) {

				long totalAmount = 0;
				for (Record record : records) {

					totalAmount += Float
							.parseFloat(PAManager.getInstance().unformatCash(record.getAttribute(TransactionCredit)));

				}
				return "" + PAManager.getInstance().formatCash(totalAmount);
			}
		});

		ListGridField balanceDebt = new ListGridField(BalanceDebt, "Debt");
		balanceDebt.setShowGridSummary(true);
		balanceDebt.setHidden(true);

		balanceDebt.setSummaryFunction(new SummaryFunction() {
			public Object getSummaryValue(Record[] records, ListGridField field) {

				long totalAmount = 0;
				for (Record record : records) {

					totalAmount += Float
							.parseFloat(PAManager.getInstance().unformatCash(record.getAttribute(BalanceDebt)));

				}
				return "" + PAManager.getInstance().formatCash(totalAmount);
			}
		});
		
		
		ListGridField balanceCredit = new ListGridField(BalanceCredit, "Credit");
		balanceCredit.setShowGridSummary(true);
		balanceCredit.setHidden(true);

		balanceCredit.setSummaryFunction(new SummaryFunction() {
			public Object getSummaryValue(Record[] records, ListGridField field) {

				long totalAmount = 0;
				for (Record record : records) {

					totalAmount += Float
							.parseFloat(PAManager.getInstance().unformatCash(record.getAttribute(BalanceCredit)));

				}
				return "" + PAManager.getInstance().formatCash(totalAmount);
			}
		});

		ListGridField paymentDetails = new ListGridField(PaymentDetails, "Payment Details");

		ListGridField remarks = new ListGridField(Remarks, "Remarks");
		ListGridField productType = new ListGridField(ProductType, "product");
		productType.setHidden(true);

		this.setFields(id, financialYear, assessmentPeriod, transationDate, transactionDescription, postReferenceNo,
				account, accountCode, transactionType, transactionDebt, transactionCredit, balanceDebt, balanceCredit,
				paymentDetails, remarks, productType);
		
		this.setHeaderSpans(
				new HeaderSpan("Transaction", new String[] { TransactionDebt, TransactionCredit }), 
				new HeaderSpan("Balance", new String[] { BalanceDebt, BalanceCredit }));
		
		this.setLeaveHeaderMenuButtonSpace(false);
		this.setWrapHeaderTitles(true);
		this.setHeaderHeight(55);
		
		this.setCanExpandRecords(true);
		this.setShowGridSummary(true);

	}

	public ListGridRecord addRowData(GeneralLedger ledger) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute(ID, ledger.getId());

		//if (ledger.getYear() != null) {
			record.setAttribute(FinancialYear, ledger.getYear());
		//}

		if (ledger.getPeriod() != null) {
			record.setAttribute(AssessmentPeriod, ledger.getPeriod());
		}

		record.setAttribute(TransationDate, ledger.getTransationDate());

		record.setAttribute(TransactionDescription, ledger.getTransactionDescription());
		record.setAttribute(PostReferenceNo, ledger.getPostReferenceNo());

		if (ledger.getAccount() != null) {
			record.setAttribute(Account, ledger.getAccount().getAccountName());
			record.setAttribute(AccountCode, ledger.getAccount().getACODE());
		}

		if (ledger.getTtype() != null) {
			record.setAttribute(TransactionType, ledger.getTtype().getTransactionType());
		}

		record.setAttribute(TransactionDebt, nf.format(ledger.getTransactionDebt()));
		record.setAttribute(TransactionCredit, nf.format(ledger.getTransactionCredit()));

		record.setAttribute(BalanceDebt, nf.format(ledger.getBalanceDebt()));
		record.setAttribute(BalanceCredit, nf.format(ledger.getBalanceCredit()));

		record.setAttribute(PaymentDetails, ledger.getPaymentDetails());
		record.setAttribute(Remarks, ledger.getRemarks());

		if (ledger.getDataSourceScreen() != null) {
			record.setAttribute(ProductType, ledger.getDataSourceScreen().getDataSourceScreen());
		}

		return record;
	}

	public void addRecordsToGrid(List<GeneralLedger> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (GeneralLedger item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
	}

}
