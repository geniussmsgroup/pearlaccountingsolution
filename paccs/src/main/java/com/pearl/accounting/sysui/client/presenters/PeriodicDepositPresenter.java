package com.pearl.accounting.sysui.client.presenters;

import java.util.Date;
import java.util.LinkedHashMap;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.shared.DispatchAsync;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.AssessmentPeriod;
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.DataSourceScreen;
import com.pearl.accounting.sysmodel.FinancialYear;
import com.pearl.accounting.sysmodel.Intervaltb;
import com.pearl.accounting.sysmodel.PeriodicDeposit;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.Theadp;
import com.pearl.accounting.sysui.client.place.NameTokens;
import com.pearl.accounting.sysui.client.utils.PAConstant;
import com.pearl.accounting.sysui.client.views.listgrids.PeriodicDepositListgrid;
import com.pearl.accounting.sysui.client.views.panes.PeriodicDepositPane;
import com.pearl.accounting.sysui.client.views.widgets.SwizimaLoader;
import com.pearl.accounting.sysui.client.views.windows.PeriodicDepositWindow;
import com.pearl.accounting.sysui.client.views.windows.SavingsWithdrawalWindow;
import com.pearl.accounting.sysui.shared.SysAction;
import com.pearl.accounting.sysui.shared.SysResult;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.EditorExitEvent;
import com.smartgwt.client.widgets.form.fields.events.EditorExitHandler;

public class PeriodicDepositPresenter
		extends Presenter<PeriodicDepositPresenter.MyView, PeriodicDepositPresenter.MyProxy> {

	private final PlaceManager placeManager;
	private final DispatchAsync dispatcher;

	public interface MyView extends View {

		public PeriodicDepositPane getPeriodicDepositPane();

	}

	@ProxyCodeSplit
	@NameToken(NameTokens.periodicdeposits)
	public interface MyProxy extends ProxyPlace<PeriodicDepositPresenter> {
	}

	@Inject
	public PeriodicDepositPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
			final DispatchAsync dispatcher, final PlaceManager placeManager) {
		super(eventBus, view, proxy);

		this.dispatcher = dispatcher;
		this.placeManager = placeManager;
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this, MainPagePresenter.TYPE_SetContextAreaContent, this);
	}

	@Override
	protected void onBind() {
		super.onBind();
		onNewButtonClicked();
		loadDeposits();
		onDeleteButtonClicked();
		onLoadButtonClicked();

	}

	private void onLoadButtonClicked() {
		getView().getPeriodicDepositPane().getLoadButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadDeposits();

			}
		});

		getView().getPeriodicDepositPane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadDeposits();

			}
		});
	}

	private void onNewButtonClicked() {
		getView().getPeriodicDepositPane().getAddButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				create();

			}
		});

		getView().getPeriodicDepositPane().getControlsPane().getNewAssetButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				create();

			}
		});
	}

	private void create() {
		PeriodicDepositWindow window = new PeriodicDepositWindow();
		loadAccounts(window);
		// loadPeriods(window);
		// loadFinancialYears(window);
		loadCustomers(window);
		onSaveButtonClicked(window);
		onDateLoaded(window);
		window.show();
	}

	private void loadPeriods(final PeriodicDepositWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_AssessmentPeriod), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

					for (AssessmentPeriod period : result.getAssessmentPeriods()) {

						valueMap.put(period.getId(), period.getPeriodName());
					}

					window.getAssessmentPeriod().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

	}

	private void onDateLoaded(final PeriodicDepositWindow window) {

		window.getDepositDate().addChangedHandler(new ChangedHandler() {

			public void onChanged(ChangedEvent event) {

				dispatcher.execute(new SysAction(PAConstant.GET_Interval), new AsyncCallback<SysResult>() {
					public void onFailure(Throwable caught) {
						System.out.println(caught.getMessage());
					}

					public void onSuccess(SysResult result) {
						// TODO Auto-generated method stub
						if (result != null) {
//								
							for (Intervaltb period : result.getIntervaltbs()) {

								final Date dn = window.getDepositDate().getValueAsDate();
								if (dn.before(period.getFEND()) && dn.after(period.getFSTART())) {
									window.getPeriod().setValue(period.getFINTNAME());
									window.getYear().setValue(period.getYRCODE());
									window.getStartDate().setValue(period.getFSTART());
									window.getEndDate().setValue(period.getFEND());
									window.getPeriodcode().setValue(period.getFINTCODE());

								}
							}
						}
					}
				});
			}

			//@Override
				// TODO Auto-generated method stub
				
//			/}
		});
	}

	private void loadFinancialYears(final PeriodicDepositWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_FinancialYear), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

					for (FinancialYear financialYear : result.getFinancialYears()) {

						valueMap.put(financialYear.getId(), financialYear.getFinancialYear());
					}

					window.getFinancialYear().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void loadAccounts(final PeriodicDepositWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_ACCOUNT), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
					for (Account account : result.getAccounts()) {
						valueMap.put(account.getSysid(), account.getAccountName());
					}

					window.getReceiveingAccount().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void loadCustomers(final PeriodicDepositWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_customerAccount), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

					for (CustomerAccount account : result.getCustomerAccounts()) {
						valueMap.put(account.getSysid(), account.getSurname() + " " + account.getOtherNames());
					}

					window.getCustomerAccount().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void ClearForm(final PeriodicDepositWindow window) {
		window.getRemarks().clearValue();
		window.getPaymentDetails().clearValue();
		window.getReceiveingAccount().clearValue();
		window.getDepositedAmount().clearValue();
		window.getCustomerAccount().clearValue();
	}

	private boolean FormValid(final PeriodicDepositWindow window) {
		if (window.getCustomerAccount().getValueAsString() != null) {
			if (window.getDepositedAmount().getValueAsString() != null) {
				if (window.getAssessmentPeriod().getValueAsString() != null) {
					if (window.getReceiveingAccount().getValueAsString() != null) {
						return true;
					} else {
						SC.warn("Error", "Please enter receiving account");

					}

				} else {
					SC.warn("Error", "Please enter assessment period");
				}

			} else {
				SC.warn("Error", "Please enter deposited amount");

			}
		} else {
			SC.warn("Error", "Please enter Customer Account");
		}
		return false;

	}

	private void onSaveButtonClicked(final PeriodicDepositWindow window) {
		window.getSaveButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				if (FormValid(window)) {
					CustomerAccount customerAccount = new CustomerAccount();
					customerAccount.setSysid(window.getCustomerAccount().getValueAsString());

					String depositedAmount = window.getDepositedAmount().getValueAsString();

					AssessmentPeriod assessmentPeriod = new AssessmentPeriod();
					assessmentPeriod.setId(window.getAssessmentPeriod().getValueAsString());

					FinancialYear financialYear = new FinancialYear();
					financialYear.setId(window.getFinancialYear().getValueAsString());

					Date depositDate = window.getDepositDate().getValueAsDate();

					Account receiveingAccount = new Account();
					receiveingAccount.setSysid(window.getReceiveingAccount().getValueAsString());

					String paymentDetails = window.getPaymentDetails().getValueAsString();
					String remarks = window.getRemarks().getValueAsString();
					String exchangeRate = "1";

					PeriodicDeposit periodicDeposit = new PeriodicDeposit();
					int year = Integer.parseInt(window.getFinancialYear().getValueAsString());

					periodicDeposit.setPeriod(window.getAssessmentPeriod().getValueAsString());
					// periodicDeposit.setCreatedBy(createdBy);
					periodicDeposit.setCustomerAccount(customerAccount);
					periodicDeposit.setDateCreated(new Date());
					periodicDeposit.setDateUpdated(new Date());
					periodicDeposit.setDepositDate(depositDate);
					periodicDeposit.setDepositedAmount(depositedAmount);
					periodicDeposit.setExchangeRate(exchangeRate);
					periodicDeposit.setYear(year);
					periodicDeposit.setPaymentDetails(paymentDetails);
					periodicDeposit.setReceiveingAccount(receiveingAccount);
					periodicDeposit.setRemarks(remarks);
					periodicDeposit.setStatus(Status.ACTIVE);

					periodicDeposit.setPeriodcode(window.getPeriodcode().getValueAsString());

					Theadp thead = new Theadp();
					thead.setYear(year);
					thead.setDataSourceScreen(DataSourceScreen.MULTI_JOURNAL);
					thead.setStatus(Status.ACTIVE);
					float amount = Float.parseFloat(depositedAmount);
					thead.setAmount(amount);
					thead.setRemarks(remarks);
					thead.setVNO(paymentDetails);
					thead.setPeriodcode(window.getPeriodcode().getValueAsString());
					thead.setAccount(receiveingAccount);

					dispatcher.execute(new SysAction(PAConstant.SAVE_Thead, thead),
							new AsyncCallback<SysResult>() {
								public void onFailure(Throwable caught) {
									System.out.println(caught.getMessage());
								}

								public void onSuccess(SysResult result) {

									SC.clearPrompt();

									if (result != null) {

										if (result.getSwizFeedback().isResponse()) {
											ClearForm(window);
											SC.say("SUCCESS", result.getSwizFeedback().getMessage());

										} else {
											SC.warn("ERROR", result.getSwizFeedback().getMessage());
										}

									} else {
										SC.say("ERROR", "Unknow error");
									}

								}
							});

					SC.showPrompt("", "", new SwizimaLoader());

					dispatcher.execute(new SysAction(PAConstant.SAVE_PeriodicDeposit, periodicDeposit),
							new AsyncCallback<SysResult>() {
								public void onFailure(Throwable caught) {
									System.out.println(caught.getMessage());
								}

								public void onSuccess(SysResult result) {
									ClearForm(window);
									SC.clearPrompt();

									if (result != null) {

										if (result.getSwizFeedback().isResponse()) {

											SC.say("SUCCESS", result.getSwizFeedback().getMessage());

											getView().getPeriodicDepositPane().getPeriodicDepositListgrid()
													.addRecordsToGrid(result.getPeriodicDeposits());
										} else {
											SC.warn("ERROR", result.getSwizFeedback().getMessage());
										}

									} else {
										SC.say("ERROR", "Unknow error");
									}

								}
							});

				}
			}
		});
	}

	private void loadDeposits() {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_PeriodicDeposit), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					getView().getPeriodicDepositPane().getPeriodicDepositListgrid()
							.addRecordsToGrid(result.getPeriodicDeposits());

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void onDeleteButtonClicked() {
		getView().getPeriodicDepositPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				delete();

			}
		});

		getView().getPeriodicDepositPane().getControlsPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				delete();

			}
		});
	}

	private void delete() {
		if (getView().getPeriodicDepositPane().getPeriodicDepositListgrid().anySelected()) {
			SC.ask("Confirm", "Are you sure you want to delete the selected record?", new BooleanCallback() {

				public void execute(Boolean value) {
					if (value) {
						PeriodicDeposit periodicDeposit = new PeriodicDeposit();

						periodicDeposit.setId(getView().getPeriodicDepositPane().getPeriodicDepositListgrid()
								.getSelectedRecord().getAttribute(PeriodicDepositListgrid.ID));

						SC.showPrompt("", "", new SwizimaLoader());

						dispatcher.execute(new SysAction(PAConstant.DELETE_PeriodicDeposit, periodicDeposit),
								new AsyncCallback<SysResult>() {
									public void onFailure(Throwable caught) {
										System.out.println(caught.getMessage());
									}

									public void onSuccess(SysResult result) {

										SC.clearPrompt();

										if (result != null) {

											if (result.getSwizFeedback().isResponse()) {

												SC.say("SUCCESS", result.getSwizFeedback().getMessage());

												getView().getPeriodicDepositPane().getPeriodicDepositListgrid()
														.addRecordsToGrid(result.getPeriodicDeposits());
											} else {
												SC.warn("ERROR", result.getSwizFeedback().getMessage());
											}

										} else {
											SC.say("ERROR", "Unknow error");
										}

									}
								});
					}

				}
			});
		} else {
			SC.warn("ERROR", "ERROR: Please select record to delete");
		}
	}

}
