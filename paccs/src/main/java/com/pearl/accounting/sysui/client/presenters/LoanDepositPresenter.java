package com.pearl.accounting.sysui.client.presenters;

import java.time.Period;
import java.util.Date;
import java.util.LinkedHashMap;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.shared.DispatchAsync;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.DataSourceScreen;
import com.pearl.accounting.sysmodel.Intervaltb;
import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysmodel.LoanDeposit;
import com.pearl.accounting.sysmodel.Receiptno;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.Theadp;
import com.pearl.accounting.sysui.client.place.NameTokens;
import com.pearl.accounting.sysui.client.utils.PAConstant;
import com.pearl.accounting.sysui.client.utils.PAManager;
import com.pearl.accounting.sysui.client.views.listgrids.LoanDepositListgrid;
import com.pearl.accounting.sysui.client.views.panes.LoanDepositPane;
import com.pearl.accounting.sysui.client.views.widgets.SwizimaLoader;
import com.pearl.accounting.sysui.client.views.windows.LoanDepositWindow;
import com.pearl.accounting.sysui.shared.SysAction;
import com.pearl.accounting.sysui.shared.SysResult;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;

public class LoanDepositPresenter extends Presenter<LoanDepositPresenter.MyView, LoanDepositPresenter.MyProxy> {

	private final PlaceManager placeManager;
	private final DispatchAsync dispatcher;

	public interface MyView extends View {
		public LoanDepositPane getLoanDepositPane();

	}

	@ProxyCodeSplit
	@NameToken(NameTokens.LoanDeposit)
	public interface MyProxy extends ProxyPlace<LoanDepositPresenter> {
	}

	@Inject
	public LoanDepositPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
			final DispatchAsync dispatcher, final PlaceManager placeManager) {
		super(eventBus, view, proxy);

		this.dispatcher = dispatcher;
		this.placeManager = placeManager;
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this, MainPagePresenter.TYPE_SetContextAreaContent, this);
	}

	@Override
	protected void onBind() {
		super.onBind();

		onNewButtonClicked();
		onDeletebuttonClicked();
		loadAccounts();
		onLoadButtonClicked();

	}

	private void onLoadButtonClicked() {
		getView().getLoanDepositPane().getLoadButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				loadAccounts();

			}
		});

		getView().getLoanDepositPane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadAccounts();

			}
		});
	}

	private void onNewButtonClicked() {
		getView().getLoanDepositPane().getAddButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				createAccount();

			}
		});

		getView().getLoanDepositPane().getControlsPane().getNewAssetButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				createAccount();

			}
		});

	}

	private void createAccount() {
		LoanDepositWindow window = new LoanDepositWindow();
		onSaveButtonClicked(window);
		onloadcurrentinterval(window);
		onDateLoaded(window);
		loadcustomers(window);
		// loadloans(window);
		loadAccounts(window);
///		loadthead(window);
		loadReceipts(window);
		loadremarks(window);
		
		loadcustloans(window);
		loadloan(window);
		// window.setAnimateFadeTime(1);

		window.animateShow();

	}

	private void onloadcurrentinterval(final LoanDepositWindow window) {
		dispatcher.execute(new SysAction(PAConstant.GET_CurrentPeriod), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {
				// TODO Auto-generated method stub
				if (result != null) {
//						
					for (Intervaltb period : result.getIntervaltbs()) {

						window.getCurrentperiod().setValue(period.getFINTNAME());
						// window.getYear().setValue(period.getYRCODE());
						window.getCurrentstartdate().setValue(period.getFSTART());

						window.getCurrentenddate().setValue(period.getFEND());
						// period.window.getPeriodcode().setValue(period.getFINTCODE());

					}
				}
			}
		});
	}

	private void onDateLoaded(final LoanDepositWindow window) {

		window.getDepositdate().addChangedHandler(new ChangedHandler() {

			public void onChanged(ChangedEvent event) {//
				// SC.showPrompt("", "", new SwizimaLoader());

				dispatcher.execute(new SysAction(PAConstant.GET_Interval), new AsyncCallback<SysResult>() {
					public void onFailure(Throwable caught) {
						System.out.println(caught.getMessage());
					}

					public void onSuccess(SysResult result) {
						// TODO Auto-generated method stub
						if (result != null) {
//								
							for (Intervaltb period : result.getIntervaltbs()) {

								final Date dn = window.getDepositdate().getValueAsDate();
								if (dn.before(period.getFEND()) && dn.after(period.getFSTART())) {
									window.getPeriod().setValue(period.getFINTNAME());
									window.getYear().setValue(period.getYRCODE());
//									window.getStartdate().setValue(period.getFSTART());
//									window.getEnddate().setValue(period.getFEND());
									window.getPeriodcode().setValue(period.getFINTCODE());
								}
							}
						}
					}
				});
			}
		});
	}

///////prodcuctid
	private void loadcustomers(final LoanDepositWindow window) {
//		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_customerAccount), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				// SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
					for (CustomerAccount account : result.getCustomerAccounts()) {
						 
						valueMap.put(account.getSysid(), account.getClientCode());
					}

					window.getCustomeraccount().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

	}
//private void clientwithaloan(final LoanDepositWindow window) {
////	window.getLoandisplay().
//}

	private void loadloan(final LoanDepositWindow window) {


		window.getCustomeraccount().addChangedHandler(new ChangedHandler() {

			public void onChanged(ChangedEvent event) {//

		dispatcher.execute(new SysAction(PAConstant.GET_LOAN), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				// SC.clearPrompt();

				if (result != null) {
         
					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
					   String member = window.getLoandisplay().getValueAsString();
					   window.getLoan().clearValue();  
					for (Loan account : result.getLoans()) {
                         if(member!=null) {
						if(member==account.getMEMBERID()) {
                            if(account.getLOANID()!=null) {
                    	window.getLoan().setValue(account.getLOANID());
                            }  else {
                            	SC.say("the client doesnt have a loan");
                            	String nulls = null;
                            	window.getLoan().setValue(nulls);
                            }
                    }
                    
                         }
					}

				//window.getLoan().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
			}
		});
	}
		
	
		
	
	private void loadcustloans(final LoanDepositWindow window) {


		window.getCustomeraccount().addChangedHandler(new ChangedHandler() {

			public void onChanged(ChangedEvent event) {//

		dispatcher.execute(new SysAction(PAConstant.GET_customerAccount), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				// SC.clearPrompt();

				if (result != null) {
         
					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
					   String member = window.getCustomeraccount().getValueAsString();

					for (CustomerAccount account : result.getCustomerAccounts()) {
                    if(account.getSysid()==member) {
                    	String members= account.getClientCode();
                    	window.getLoandisplay().setValue(members);
                    	
                    }
				    
					}

				//window.getLoan().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
			}
		});
	}
		
	private void loadAccounts(final LoanDepositWindow window) {

		// SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_ACCOUNT), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				// SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
					for (Account account : result.getAccounts()) {
						valueMap.put(account.getSysid(), account.getACODE() + "  " + account.getAccountName());
					}

					window.getAccount().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

//
//	private void loadthead(final LoanDepositWindow window) {
//
//		dispatcher.execute(new SysAction(PAConstant.GET_Thead), new AsyncCallback<SysResult>() {
//			public void onFailure(Throwable caught) {
//				System.out.println(caught.getMessage());
//			}
//
//			public void onSuccess(SysResult result) {
//
//				////SC.clearPrompt();
//
//				if (result != null) {
//
//					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
//					for (Theadp account : result.getTheadps()) {
//						window.getTest().setValue(account.getHDNO());
//					}
//
//				} else {
//					SC.say("ERROR", "Unknow error");
//				}
//
//			}
//		});
//	}
	private void loadremarks(final LoanDepositWindow window) {
		window.getReceipts().addChangedHandler(new ChangedHandler() {

			public void onChanged(ChangedEvent event) {
				dispatcher.execute(new SysAction(PAConstant.GET_RECEIPT), new AsyncCallback<SysResult>() {
					public void onFailure(Throwable caught) {
						System.out.println(caught.getMessage());
					}

					public void onSuccess(SysResult result) {
						// TODO Auto-generated method stub
						if (result != null) {
////        
							String amount = window.getReceipts().getValueAsString();

							String answer = null;
							for (Receiptno period : result.getReceiptnos()) {
								String rt = String.valueOf(period.getTransactionid());
								if (amount == rt) {
									String ans = period.getAMTWORDS();
									window.getAmountinwords().setValue(ans);
//								SC.say("wait");
								}

							}
						}
					}
				});
			}
		});
	}
//	
//	

	private void loadReceipts(final LoanDepositWindow window) {

		dispatcher.execute(new SysAction(PAConstant.GET_RECEIPT), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
					for (Receiptno account : result.getReceiptnos()) {
						/// window.getTest().setValue(account.getHDNO());

						valueMap.put(String.valueOf(account.getTransactionid()), account.getRTVRNO());

					}
					window.getReceipts().setValueMap(valueMap);
				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

	}

//
	private boolean FormValid(final LoanDepositWindow window) {
		if (window.getLoan().getValueAsString() != null) {
			if (window.getCustomeraccount().getValueAsString() != null) {
				if (window.getAccount().getValueAsString() != null) {
					if (window.getLoan().getValueAsString() != null) {
						if ((window.getLoanDeposit().getValueAsString() != null)
								|| (window.getLoaninterestdeposit().getValueAsString() != null)) {
							if ((window.getRemarks().getValueAsString() != null)
									|| (window.getPaydetails().getValueAsString() != null)) {
								return true;

							} else {
								SC.warn("ERROR", "Please  enter remarks and paydetails");

							}

						} else {
							SC.warn("ERROR", "Please  enter either loan deposit and interest amount");

						}
					} else {
						SC.warn("ERROR", "Please Select paying category");

					}
				} else {
					SC.warn("ERROR", "Please Select account type");
				}
			} else {
				SC.warn("ERROR", "Please enter account name");
			}
		} else {
			SC.warn("ERROR", "Please enter loan code");
		}
		return false;
	}

	private void ClearForm(final LoanDepositWindow window) {
		window.getCustomeraccount().clearValue();
		window.getLoan().clearValue();
		window.getAccount().clearValue();
		window.getLoanDeposit().clearValue();
		window.getDepositdate().clearValue();
		window.getPaydetails().clearValue();
		window.getRemarks().clearValue();

	}

	private void onSaveButtonClicked(final LoanDepositWindow window) {
		window.getSaveButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				if (FormValid(window)) {

					Loan loans = new Loan();
					loans.setLOANID(window.getLoan().getValueAsString());
					
					
					
					CustomerAccount customeraccounts = new CustomerAccount();
					customeraccounts.setSysid(window.getCustomeraccount().getValueAsString());
					Account accounts = new Account();
					accounts.setSysid(window.getAccount().getValueAsString());
					/// String thead = window.getTest().getValueAsString();
					//
					// here is were am making the thead count
					// 
					// int newthead = Integer.parseInt(thead) + 1;
					 

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					LoanDeposit loandeposit = new LoanDeposit();
					loandeposit.setAccount(accounts);
					////////////////// loandeposit.setThead(String.valueOf(newthead));
					if(loans!=null) {
					loandeposit.setLoan(loans);
					}
					else {
						loandeposit.setLoan(null);
	
					}
					loandeposit.setCustomeraccount(customeraccounts);
					loandeposit.setStatus(Status.ACTIVE);
					loandeposit.setDateCreated(new Date());
					loandeposit.setDateUpdated(new Date());
					loandeposit.setPaydetails(window.getPaydetails().getValueAsString());
					loandeposit.setRemarks(window.getRemarks().getValueAsString());
					float amountReceived = Float.parseFloat(
							PAManager.getInstance().unformatCash(window.getLoanDeposit().getValueAsString()));
					float interestReceived = Float.parseFloat(
							PAManager.getInstance().unformatCash(window.getLoaninterestdeposit().getValueAsString()));
					// account.setUpdatedBy(updatedBy);
					loandeposit.setPeriod(window.getPeriod().getValueAsString());
					loandeposit.setYear(window.getYear().getValueAsString());
					loandeposit.setPeriodcode(window.getPeriodcode().getValueAsString());
					loandeposit.setLoandeposit(amountReceived);
					loandeposit.setInterestdeposit(interestReceived);
					loandeposit.setDepositdate(window.getDepositdate().getValueAsDate());

//					
//					SC.showPrompt("", "", new SwizimaLoader());
//
//					dispatcher.execute(new SysAction(PAConstant.SAVE_Thead, thead1), new AsyncCallback<SysResult>() {
//						public void onFailure(Throwable caught) {
//							System.out.println(caught.getMessage());
//						}
//
//						public void onSuccess(SysResult result) {
//
//							SC.clearPrompt();
//
//							if (result != null) {
//
//								if (result.getSwizFeedback().isResponse()) {
//									ClearForm(window);
//									SC.say("SUCCESS", result.getSwizFeedback().getMessage());
//
//								} else {
//									SC.warn("ERROR", result.getSwizFeedback().getMessage());
//								}
//
//							} else {
//								SC.say("ERROR", "Unknow error");
//							}
//
//						}
//					});
////
					SC.showPrompt("", "", new SwizimaLoader());

					dispatcher.execute(new SysAction(PAConstant.SAVE_LOANDEPOSITS, loandeposit),
							new AsyncCallback<SysResult>() {
								public void onFailure(Throwable caught) {
									System.out.println(caught.getMessage());
								}

								public void onSuccess(SysResult result) {

									SC.clearPrompt();
									ClearForm(window);
									if (result != null) {

										if (result.getSwizFeedback().isResponse()) {

											SC.say("SUCCESS", result.getSwizFeedback().getMessage());

											getView().getLoanDepositPane().getLoanDepositListgrid()
													.addRecordsToGrid(result.getLoandeposits());

										} else {
											SC.warn("ERROR", result.getSwizFeedback().getMessage());
										}

									} else {
										SC.say("ERROR", "Unknow error");
									}

								}
							});

				}
			}
		});
	}
	/// }
//			}
//		});
	// }

	private void loadAccounts() {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_LOANDEPOSITS), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					getView().getLoanDepositPane().getLoanDepositListgrid().addRecordsToGrid(result.getLoandeposits());

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void onDeletebuttonClicked() {
		getView().getLoanDepositPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				delete();

			}
		});

		getView().getLoanDepositPane().getControlsPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				delete();

			}
		});
	}

	private void delete() {
		if (getView().getLoanDepositPane().getLoanDepositListgrid().anySelected()) {

			SC.ask("Confirm", "Are you sure you want to delete the selected record?", new BooleanCallback() {

				public void execute(Boolean value) {

					if (value) {

						LoanDeposit account = new LoanDeposit();
						account.setId(getView().getLoanDepositPane().getLoanDepositListgrid().getSelectedRecord()
								.getAttribute(LoanDepositListgrid.ID));

						SC.showPrompt("", "", new SwizimaLoader());

						dispatcher.execute(new SysAction(PAConstant.DELETE_LOANDEPOSITS, account),
								new AsyncCallback<SysResult>() {
									public void onFailure(Throwable caught) {
										System.out.println(caught.getMessage());
									}

									public void onSuccess(SysResult result) {

										SC.clearPrompt();

										if (result != null) {

											if (result.getSwizFeedback().isResponse()) {

												SC.say("SUCCESS", result.getSwizFeedback().getMessage());

												getView().getLoanDepositPane().getLoanDepositListgrid()
														.addRecordsToGrid(result.getLoandeposits());
											} else {
												SC.warn("ERROR", result.getSwizFeedback().getMessage());
											}

										} else {
											SC.say("ERROR", "Unknow error");
										}

									}
								});

					}

				}
			});

		} else {
			SC.say("ERROR", "ERROR: Please select record to delete?");
		}
	}

}