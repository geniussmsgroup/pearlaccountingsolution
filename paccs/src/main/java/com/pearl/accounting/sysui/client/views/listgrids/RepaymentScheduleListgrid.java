package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.google.gwt.i18n.client.NumberFormat;
import com.pearl.accounting.sysmodel.RepaymentSchedule;
import com.pearl.accounting.sysui.client.utils.PAManager;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;

public class RepaymentScheduleListgrid extends SuperListGrid {
	public static String ID = "id";

	public static String PrincipleAmount="principleAmount";
	public static String InterestAmount="interestAmount";
	public static String TotalAmount="totalAmount";
	public static String PaymentDate="paymentDate";
	public static String PaymentLotNumber="paymentLotNumber";
	
	NumberFormat nf = NumberFormat.getFormat("#,##0.00");

	public RepaymentScheduleListgrid() {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);
		
		ListGridField principleAmount = new ListGridField(PrincipleAmount, "Amount");
		principleAmount.setShowGridSummary(true);

		principleAmount.setSummaryFunction(new SummaryFunction() {
			public Object getSummaryValue(Record[] records, ListGridField field) {

				long totalAmount = 0;
				for (Record record : records) {

					totalAmount += Float
							.parseFloat(PAManager.getInstance().unformatCash(record.getAttribute(PrincipleAmount)));

				}
				return "" + PAManager.getInstance().formatCash(totalAmount);
			}
		});
		
		ListGridField interestAmount = new ListGridField(InterestAmount, "Interest");
		interestAmount.setShowGridSummary(true);

		interestAmount.setSummaryFunction(new SummaryFunction() {
			public Object getSummaryValue(Record[] records, ListGridField field) {

				long totalAmount = 0;
				for (Record record : records) {

					totalAmount += Float
							.parseFloat(PAManager.getInstance().unformatCash(record.getAttribute(InterestAmount)));

				}
				return "" + PAManager.getInstance().formatCash(totalAmount);
			}
		});
		
		ListGridField totalAmount = new ListGridField(TotalAmount, "");
		totalAmount.setShowGridSummary(true);

		totalAmount.setSummaryFunction(new SummaryFunction() {
			public Object getSummaryValue(Record[] records, ListGridField field) {

				long totalAmount = 0;
				for (Record record : records) {

					totalAmount += Float
							.parseFloat(PAManager.getInstance().unformatCash(record.getAttribute(TotalAmount)));

				}
				return "" + PAManager.getInstance().formatCash(totalAmount);
			}
		});
		ListGridField paymentDate = new ListGridField(PaymentDate, "Due date");
		ListGridField paymentLotNumber = new ListGridField(PaymentLotNumber, "#");

		this.setFields(id, paymentLotNumber, principleAmount,interestAmount,totalAmount,paymentDate);
		this.setShowGridSummary(true);
		this.setShowRowNumbers(false);

	}

	public ListGridRecord addRowData(RepaymentSchedule schedule) {
		ListGridRecord record = new ListGridRecord(); 
	//	record.setAttribute(ID, schedule.getId());
		record.setAttribute(PaymentLotNumber, schedule.getPaymentLotNumber()); 
		record.setAttribute(PrincipleAmount,  nf.format(schedule.getPrincipleAmount())); 
		record.setAttribute(InterestAmount,  nf.format(schedule.getInterestAmount())); 
		record.setAttribute(TotalAmount,  nf.format(schedule.getTotalAmount()));
		record.setAttribute(PaymentDate, schedule.getPaymentDate());
		 
		return record;
	}

	public void addRecordsToGrid(List<RepaymentSchedule> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (RepaymentSchedule item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
	}

	public static String getTotalAmount() {
		return TotalAmount;
	}

	public static void setTotalAmount(String totalAmount) {
		TotalAmount = totalAmount;
	}

}
