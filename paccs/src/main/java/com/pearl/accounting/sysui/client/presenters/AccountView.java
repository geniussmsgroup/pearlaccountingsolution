package com.pearl.accounting.sysui.client.presenters;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.pearl.accounting.sysui.client.views.panes.AccountPane;
import com.smartgwt.client.widgets.layout.VLayout;

public class AccountView extends ViewImpl implements AccountPresenter.MyView {

	private static final String DEFAULT_MARGIN = "0px";
	private VLayout panel;
	private AccountPane accountPane;

	@Inject
	public AccountView() {

		panel = new VLayout();

		accountPane = new AccountPane();

		panel.setMembers(accountPane);
		panel.setWidth100();
		panel.setHeight("90%");
		Window.enableScrolling(false);
		Window.setMargin(DEFAULT_MARGIN);

	}

	public Widget asWidget() {
		return panel;
	}

	public AccountPane getAccountPane() {
		return accountPane;
	}
	
	

}
