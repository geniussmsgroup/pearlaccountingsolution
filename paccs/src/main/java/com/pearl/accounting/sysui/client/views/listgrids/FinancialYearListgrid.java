package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.pearl.accounting.sysmodel.FinancialYear;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord; 

public class FinancialYearListgrid extends SuperListGrid {
	public static String ID = "id";
	public static String CODE = "code";
	public static String NAME = "financialYear";
	public static String START_DATE = "startDate";
	public static String END_DATE = "endDate";
	
	public static String STATUS = "status";

	public FinancialYearListgrid() {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField code = new ListGridField(CODE, "Code");
		ListGridField financialYear = new ListGridField(NAME, "Financialyear");
		ListGridField startDate = new ListGridField(START_DATE, "Start date");
		ListGridField endDate = new ListGridField(END_DATE, "End date");
		
		ListGridField status = new ListGridField(STATUS, "Status");

		this.setFields(id, code, financialYear, startDate, endDate,status);

	}

	public ListGridRecord addRowData(FinancialYear financialYear) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute(ID, financialYear.getId());
		record.setAttribute(CODE, financialYear.getCode());
		record.setAttribute(NAME, financialYear.getFinancialYear());
		record.setAttribute(START_DATE, financialYear.getStartDate());
		record.setAttribute(END_DATE, financialYear.getEndDate());
		
		if(financialYear.getActivationStatus()!=null){
			record.setAttribute(STATUS, financialYear.getActivationStatus().getStatus());
		}
		

		return record;
	}

	public void addRecordsToGrid(List<FinancialYear> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (FinancialYear item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
	}

}
