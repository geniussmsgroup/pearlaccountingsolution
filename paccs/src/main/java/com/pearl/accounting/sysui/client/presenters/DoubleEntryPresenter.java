
package com.pearl.accounting.sysui.client.presenters;

import com.pearl.accounting.sysui.client.views.windows.CashBookPaymentWindow;
import com.smartgwt.client.widgets.form.fields.events.EditorExitEvent;
import com.smartgwt.client.widgets.form.fields.events.EditorExitHandler;

import java.util.Date;
import java.util.LinkedHashMap;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.shared.DispatchAsync;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.AccountDefinition;
import com.pearl.accounting.sysmodel.AccountType;
import com.pearl.accounting.sysmodel.AssessmentPeriod;
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.DataSourceScreen;
import com.pearl.accounting.sysmodel.DoubleEntry;
import com.pearl.accounting.sysmodel.Init;
import com.pearl.accounting.sysmodel.Intervaltb;
import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysmodel.ProductType;
import com.pearl.accounting.sysmodel.Status;
import com.pearl.accounting.sysmodel.Theadp;
import com.pearl.accounting.sysui.client.place.NameTokens;
import com.pearl.accounting.sysui.client.utils.PAConstant;
import com.pearl.accounting.sysui.client.utils.PAManager;
import com.pearl.accounting.sysui.client.views.listgrids.AccountListgrid;
import com.pearl.accounting.sysui.client.views.listgrids.DoubleEntryListgrid;
import com.pearl.accounting.sysui.client.views.panes.AccountPane;
import com.pearl.accounting.sysui.client.views.panes.DoubleEntryPane;
import com.pearl.accounting.sysui.client.views.widgets.SwizimaLoader;
import com.pearl.accounting.sysui.client.views.windows.AccountWindow;
import com.pearl.accounting.sysui.client.views.windows.DoubleEntryWindow;
import com.pearl.accounting.sysui.client.views.windows.SearchWindow;
import com.pearl.accounting.sysui.shared.SysAction;
import com.pearl.accounting.sysui.shared.SysResult;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangeHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;

public class DoubleEntryPresenter extends Presenter<DoubleEntryPresenter.MyView, DoubleEntryPresenter.MyProxy> {

	private final PlaceManager placeManager;
	private final DispatchAsync dispatcher;

	public interface MyView extends View {
		public DoubleEntryPane getDoubleEntryPane();

	}

	@ProxyCodeSplit
	@NameToken(NameTokens.DoubleEntry)
	public interface MyProxy extends ProxyPlace<DoubleEntryPresenter> {
	}

	@Inject
	public DoubleEntryPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
			final DispatchAsync dispatcher, final PlaceManager placeManager) {
		super(eventBus, view, proxy);

		this.dispatcher = dispatcher;
		this.placeManager = placeManager;
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this, MainPagePresenter.TYPE_SetContextAreaContent, this);
	}

	@Override
	protected void onBind() {
		super.onBind();

		onNewButtonClicked();
		onDeletebuttonClicked();
		loadAccounts();
		onLoadButtonClicked();
		// onSearchbuttonClicked();

	}

	private void onLoadButtonClicked() {
		getView().getDoubleEntryPane().getLoadButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				loadAccounts();

			}
		});

		getView().getDoubleEntryPane().getControlsPane().getRefreshButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				loadAccounts();

			}
		});
	}

	private void onNewButtonClicked() {
		getView().getDoubleEntryPane().getAddButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				createAccount();

			}
		});

		getView().getDoubleEntryPane().getControlsPane().getNewAssetButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {

				createAccount();

			}
		});

	}

	private void createAccount() {
		DoubleEntryWindow window = new DoubleEntryWindow();

		loadAccountCategories(window);
		loadTrackle(window);
		onSavebuttonClicked(window);
		loadCustomerAccounts(window);
		onDateLoaded(window);
		window.show();
		loadLoans(window);
	}

	private void onDateLoaded(final DoubleEntryWindow window) {

		window.getTransdate().addChangedHandler(new ChangedHandler() {

			public void onChanged(ChangedEvent event) {

				dispatcher.execute(new SysAction(PAConstant.GET_Interval), new AsyncCallback<SysResult>() {
					public void onFailure(Throwable caught) {
						System.out.println(caught.getMessage());
					}

					public void onSuccess(SysResult result) {
						// TODO Auto-generated method stub
						if (result != null) {
//								
							for (Intervaltb period : result.getIntervaltbs()) {

								final Date dn = window.getTransdate().getValueAsDate();
								if (dn.before(period.getFEND()) && dn.after(period.getFSTART())) {
									window.getPeriod().setValue(period.getFINTNAME());
									window.getYear().setValue(period.getYRCODE());
									window.getStartdate().setValue(period.getFSTART());
									window.getEnddate().setValue(period.getFEND());
									window.getPeriodcode().setValue(period.getFINTCODE());
								}
							}
						}
					}
				});
			}
		});
	}

	private void loadLoans(final DoubleEntryWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_LOAN), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

					for (Loan period : result.getLoans()) {

						valueMap.put(period.getLOANID(),
								period.getLOANID() + "" + period.getLOANID());
					}

					window.getLoanaccount().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

	}

	private void loadCustomerAccounts(final DoubleEntryWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_customerAccount), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

					for (CustomerAccount period : result.getCustomerAccounts()) {

						valueMap.put(period.getSysid(),
								period.getClientCode() + " " + period.getSurname() + " " + period.getOtherNames());
					}

					window.getMemcode().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

	}

	private void loadAccountCategories(final DoubleEntryWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_ACCOUNT), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

					for (Account period : result.getAccounts()) {

						valueMap.put(period.getSysid(), period.getAccountName());
					}

					window.getAccredit().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

	}

	private void loadTrackle(final DoubleEntryWindow window) {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_ACCOUNT), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();

					for (Account period : result.getAccounts()) {

						valueMap.put(period.getSysid(), period.getAccountName());
					}

					window.getAcdebit().setValueMap(valueMap);

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

	}

	private boolean FormValid(final DoubleEntryWindow window) {

		if (window.getPaydet().getValueAsString() != null) {
			if (window.getAmount().getValueAsString() != null) {
				if (window.getRemarks().getValueAsString() != null) {
					if (window.getAccredit().getValueAsString() != null) {
						if (window.getAcdebit().getValueAsString() != null) {

							return true;
						} else {
							SC.warn("ERROR", "Please Select debitaccount");

						}
					} else {
						SC.warn("ERROR", "Please Select creditaccount");

					}
				} else {
					SC.warn("ERROR", "Please enter REmarks");
				}
			} else {
				SC.warn("ERROR", "Please enter Amount");
			}
		} else {
			SC.warn("ERROR", "Please enter pay details");
		}
		return false;
	}

	private void ClearForm(final DoubleEntryWindow window) {
		window.getAmount().clearValue();
		window.getPaydet().clearValue();
		window.getRemarks().clearValue();
		window.getAccredit().clearValue();
		window.getAcdebit().clearValue();

	}

	private void onSavebuttonClicked(final DoubleEntryWindow window) {
		window.getSave().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				if (FormValid(window)) {
					Account credit = new Account();
					credit.setSysid(window.getAccredit().getValueAsString());

					Account debit = new Account();
					debit.setSysid(window.getAcdebit().getValueAsString());
					
					CustomerAccount customer = new CustomerAccount();
					Loan loan = new Loan();
					customer.setSysid(window.getMemcode().getValueAsString());
					loan.setLOANID(window.getLoanaccount().getValueAsString());

//////////////////////////////intizing the table
					DoubleEntry doubleentry = new DoubleEntry();
                    doubleentry.setCredit(credit);
                  //  doubleentry.setd
					doubleentry.setRemarks(window.getRemarks().getValueAsString());
					doubleentry.setVNO(window.getPaydet().getValueAsString());
					float amount = Float
							.parseFloat(PAManager.getInstance().unformatCash(window.getAmount().getValueAsString()));
					doubleentry.setAmount(amount);
					doubleentry.setTdates(window.getTransdate().getValueAsDate());
					// account.setCreatedBy(createdBy);
					doubleentry.setDateCreated(new Date());
					doubleentry.setDateUpdated(new Date());
					doubleentry.setPeriod(window.getPeriod().getValueAsString());
					int Year = Integer.parseInt(window.getYear().getValueAsString());
					doubleentry.setYear(Year);
					//Account credit =new Account();
					//credit.setSysid(window.getAccredit().getValueAsString());
					 
					//account.setCredit(accounts2);
					doubleentry.setDebit(debit);
					doubleentry.setDataSourceScreen(DataSourceScreen.DoubleEntry);
					doubleentry.setPeriodcode(window.getPeriodcode().getValueAsString());
					doubleentry.setStatus(Status.ACTIVE);
					if (window.getMemcode().getValueAsString() != null
							|| window.getLoanaccount().getValueAsString() != null) {
						doubleentry.setCustomerAccount(customer);
						doubleentry.setLoanaccount(loan);
					} else if (window.getMemcode().getValueAsString() == null
							|| window.getLoanaccount().getValueAsString() == null) {
						doubleentry.setCustomerAccount(null);
						doubleentry.setLoanaccount(null);
					}

//					////////////////////// saving to theadp
//					Theadp account1 = new Theadp();
//					account1.setRemarks(window.getRemarks().getValueAsString());
//					account1.setVNO(window.getPaydet().getValueAsString());
//					float amount2 = Float
//							.parseFloat(PAManager.getInstance().unformatCash(window.getAmount().getValueAsString()));
//					account1.setAmount(amount2);
//					account1.setTdates(window.getTransdate().getValueAsDate());
//					// account.setCreatedBy(createdBy);
//					account1.setDateCreated(new Date());
//					account1.setDateUpdated(new Date());
//					account1.setPeriod(window.getPeriod().getValueAsString());
//					// int Year = Integer.parseInt(window.getYear().getValueAsString());
//					account1.setYear(Year);
//					account1.setAccount(accounts);
//					account1.setOCODE(accounts2);
//					account1.setDataSourceScreen(DataSourceScreen.DoubleEntry);
//					account1.setPeriodcode(window.getPeriodcode().getValueAsString());
//					account1.setStatus(Status.ACTIVE);
//					if (window.getMemcode().getValueAsString() != null
//							|| window.getLoanaccount().getValueAsString() != null) {
//
//						account1.setCustomerAccount(customer);
//						account1.setLoanaccount(loan);
//					} else if (window.getMemcode().getValueAsString() == null
//							|| window.getLoanaccount().getValueAsString() == null) {
//						account1.setCustomerAccount(null);
//						account1.setLoanaccount(null);
//					}

//					SC.showPrompt("", "", new SwizimaLoader());
//
//					dispatcher.execute(new SysAction(PAConstant.SAVE_Thead, account1), new AsyncCallback<SysResult>() {
//						public void onFailure(Throwable caught) {
//							System.out.println(caught.getMessage());
//						}
//
//						public void onSuccess(SysResult result) {
//
//							SC.clearPrompt();
//
//							if (result != null) {
//
//								if (result.getSwizFeedback().isResponse()) {
//									ClearForm(window);
//									SC.say("SUCCESS", result.getSwizFeedback().getMessage());
//
//								} else {
//									SC.warn("ERROR", result.getSwizFeedback().getMessage());
//								}
//
//							} else {
//								SC.say("ERROR", "Unknow error");
//							}
//
//						}
//					});

					SC.showPrompt("", "", new SwizimaLoader());

					dispatcher.execute(new SysAction(PAConstant.SAVE_DOUBLEENTRY, doubleentry),
							new AsyncCallback<SysResult>() {
								public void onFailure(Throwable caught) {
									System.out.println(caught.getMessage());
								}

								public void onSuccess(SysResult result) {

									SC.clearPrompt();

									if (result != null) {

										if (result.getSwizFeedback().isResponse()) {
											ClearForm(window);
											SC.say("SUCCESS", result.getSwizFeedback().getMessage());

											getView().getDoubleEntryPane().getDoubleEntryListgrid()
													.addRecordsToGrid(result.getDoubleEntrys());
										} else {
											SC.warn("ERROR", result.getSwizFeedback().getMessage());
										}

									} else {
										SC.say("ERROR", "Unknow error");
									}

								}
							});
				}
			}
		});
	}

	private void loadAccounts() {
		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_DOUBLEENTRY), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					getView().getDoubleEntryPane().getDoubleEntryListgrid().addRecordsToGrid(result.getDoubleEntrys());

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});
	}

	private void onDeletebuttonClicked() {
		getView().getDoubleEntryPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				delete();

			}
		});

		getView().getDoubleEntryPane().getControlsPane().getDeleteButton().addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				delete();

			}
		});
	}

	private void delete() {
		if (getView().getDoubleEntryPane().getDoubleEntryListgrid().anySelected()) {

			SC.ask("Confirm", "Are you sure you want to delete the selected record?", new BooleanCallback() {

				public void execute(Boolean value) {

					if (value) {

						Theadp account = new Theadp();
						account.setId(getView().getDoubleEntryPane().getDoubleEntryListgrid().getSelectedRecord()
								.getAttribute(DoubleEntryListgrid.ID));

						SC.showPrompt("", "", new SwizimaLoader());

						dispatcher.execute(new SysAction(PAConstant.DELETE_DOUBLEENTRY, account),
								new AsyncCallback<SysResult>() {
									public void onFailure(Throwable caught) {
										System.out.println(caught.getMessage());
									}

									public void onSuccess(SysResult result) {

										SC.clearPrompt();

										if (result != null) {

											if (result.getSwizFeedback().isResponse()) {

												SC.say("SUCCESS", result.getSwizFeedback().getMessage());

												getView().getDoubleEntryPane().getDoubleEntryListgrid()
														.addRecordsToGrid(result.getDoubleEntrys());
											} else {
												SC.warn("ERROR", result.getSwizFeedback().getMessage());
											}

										} else {
											SC.say("ERROR", "Unknow error");
										}

									}
								});

					}

				}
			});

		} else {
			SC.say("ERROR", "ERROR: Please select record to delete?");
		}
	}

//	private void searchbar(final DoubleEntryWindow window) {
//		window.getSearchbar().addClickHandler(new ClickHandler() {
//			public void onClick(ClickEvent event) {
//			dispatcher.execute(new SysAction(PAConstant.GET_DOUBLEENTRY), new AsyncCallback<SysResult>() {
//			public void onFailure(Throwable caught) {
//			System.out.println(caught.getMessage());
//			}
//			public void onSuccess(SysResult result) {
//			// TODO Auto-generated method stub
//			SC.clearPrompt();
//
//			if (result != null) {
//			for (Theadp thead : result.getTheadps()) {
//			String enteredString = window.getSearchbar().getValueAsString();
//			String id = thead.getHDNO();
//			if (enteredString == id) {
//            String tdate =thead.getTdates().toString().toUpperCase();
//			String vno = thead.getVNO().toUpperCase();
//			String remarks = thead.getRemarks().toUpperCase();
//			float amt = thead.getAmount();
//			
//			SC.say(tdate+" "+vno + " " + remarks +" "+amt);
////		window.setTransdate(thead.getTdates());
////		window.setPaydet(thead.getVNO().getBytes());
////		window.setAmount(thead.getAmount());
////		window.setRemarks(thead.getRemarks());
////			window.setAccredit(thead.getCredit());
////			window.setAcdebit(thead.getDebit());
////			
//			
//			
//			}}}}});}});}

}

//	
//	private void searchbar(final SearchWindow sw) {
//		sw.getGo().addClickHandler(new ClickHandler() {
//			String enteredString = sw.getSearchbar().getValueAsString();
//			@Override
//			public void onClick(ClickEvent event) {
////							SC.say("worked");
//
//			dispatcher.execute(new SysAction(PAConstant.GET_DOUBLEENTRY), new AsyncCallback<SysResult>() {
//			public void onFailure(Throwable caught) {
//			System.out.println(caught.getMessage());
//			}
//
//			@Override
//			public void onSuccess(SysResult result) {
//			// TODO Auto-generated method stub
//			SC.clearPrompt();
//
//			if (result != null) {
//			for (Theadp loans : result.getTheadps()) {
//			String enteredString = sw.getSearchbar().getValueAsString();
//			String id = loans.getHDNO();
//			if (enteredString == id) {
//
//			String Vno = loans.getVNO();
//			float sname = loans.getAmount();
//			SC.say(Vno + " " + sname);
//			}}
