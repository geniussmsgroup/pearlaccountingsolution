
package com.pearl.accounting.sysui.client.views.windows;

import com.pearl.accounting.sysui.client.views.widgets.ComboBox;
import com.pearl.accounting.sysui.client.views.widgets.TextField;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class LoanDepositWindow extends Window {
  //  private TextField currentdate;
    private TextField currentperiod;
    private DateItem currentstartdate;
    private DateItem currentenddate;
	private TextField loanDeposit;
	private TextField loaninterestdeposit;
	private TextField loan;
	private ComboBox customeraccount;
	private TextField remarks;
	private TextField paydetails;
	private DateItem depositdate;
	private TextField year;
	private TextField loandisplay;
	private ComboBox account;

	private TextField period;
	private TextField periodcode;
	private ComboBox receipts;

	private TextField amountinwords;
	private IButton saveButton;

	public LoanDepositWindow() {
		super();
		
		
		currentperiod = new TextField();
		currentperiod.setTitle("currentperiod");
		currentperiod.setWidth("10px");

		currentstartdate = new DateItem();
		currentstartdate.setTitle("Transaction Date");
		currentstartdate.setUseTextField(true);
		currentstartdate.setWidth("*");
		currentstartdate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		currentenddate = new DateItem();
		currentenddate.setTitle("Transaction Date");
		currentenddate.setUseTextField(true);
		currentstartdate.setWidth("*");
		currentstartdate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
		
		
		
		//loandisplay
		loandisplay = new TextField();
		loandisplay.setTitle("loandisplay");
		loandisplay.setWidth("5px");
		loanDeposit = new TextField();
		loanDeposit.setTitle("loanDeposit");
		loaninterestdeposit = new TextField();
		loaninterestdeposit.setTitle("loaninterestdeposit");
		loaninterestdeposit.setDefaultValue(0);
		remarks = new TextField();
		remarks.setTitle("remarks");
		paydetails = new TextField();
		paydetails.setTitle("paydetails");
		depositdate = new DateItem();
		depositdate.setTitle("depositdate");
		depositdate.setUseTextField(true);
		depositdate.setWidth("10px");
		depositdate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
		///
		year = new TextField();
		year.setTitle("year");
//		test = new TextField();
//		test.setTitle("test");
		period = new TextField();
		period.setTitle("period");
		periodcode = new TextField();
		periodcode.setTitle("periodcode");
		loan = new TextField();
		loan.setTitle("loan");
		loan.setHint("select the customer too view the loan");
		account = new ComboBox();
		account.setTitle("account");
		customeraccount = new ComboBox();
		customeraccount.setTitle("customeraccount");

		amountinwords = new TextField();
		amountinwords.setTitle("amount in words");
		receipts = new ComboBox();
		receipts.setTitle("receipts numbers");
		
		saveButton = new IButton("save");
		DynamicForm form = new DynamicForm();
		form.setFields( year, period, periodcode);
		form.setWrapItemTitles(true);
		form.setMargin(10);
		form.setNumCols(4);

		DynamicForm form1 = new DynamicForm();
		form1.setFields(depositdate,loanDeposit, loaninterestdeposit, loan );
		form1.setWrapItemTitles(true);
		form1.setMargin(10);
		form1.setNumCols(4);

		DynamicForm form2 = new DynamicForm();
		form2.setFields(customeraccount,loandisplay, account, remarks, paydetails);
		form2.setWrapItemTitles(true);
		form2.setMargin(10);
		form2.setNumCols(4);
		DynamicForm form3 = new DynamicForm();
		form3.setFields(receipts, amountinwords);
		form3.setWrapItemTitles(true);
		form3.setMargin(10);
		form3.setNumCols(4);
		DynamicForm form4 = new DynamicForm();
		form4.setFields(currentstartdate, currentperiod,currentenddate);
		form4.setWrapItemTitles(true);
		form4.setMargin(10);
		form4.setNumCols(6);
//		form4.setNumCols(4);
		HLayout buttonLayout1 = new HLayout();
		buttonLayout1.addMember(form4);
		buttonLayout1.setAutoHeight();
		buttonLayout1.setWidth100();
		buttonLayout1.setMargin(5);
		buttonLayout1.setMembersMargin(4);

		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(saveButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
        layout.addMember(buttonLayout1);		
		layout.addMembers(form4,form, form1, form2,form3);
		layout.addMember(buttonLayout);

		layout.setMargin(10);
		this.addItem(layout);
		this.setWidth("60%");
		this.setHeight("60%");
		this.setAutoCenter(true);
		this.setTitle("Loan Deposits");
		this.setIsModal(true);
		this.setShowModalMask(true);
	}

	
	
	
	public TextField getLoandisplay() {
		return loandisplay;
	}




	public TextField getLoanDeposit() {
		return loanDeposit;
	}

	public TextField getLoaninterestdeposit() {
		return loaninterestdeposit;
	}

	public TextField getLoan() {
		return loan;
	}

	public ComboBox getReceipts() {
		return receipts;
	}

	public TextField getAmountinwords() {
		return amountinwords;
	}

	public ComboBox getCustomeraccount() {
		return customeraccount;
	}

	public TextField getRemarks() {
		return remarks;
	}

	public TextField getPaydetails() {
		return paydetails;
	}

	public DateItem getDepositdate() {
		return depositdate;
	}

	public TextField getYear() {
		return year;
	}
//
//	public TextField getTest() {
//		return test;
//	}

	public ComboBox getAccount() {
		return account;
	}

	public TextField getPeriod() {
		return period;
	}

	public TextField getPeriodcode() {
		return periodcode;
	}

	public IButton getSaveButton() {
		return saveButton;
	}

	public TextField getCurrentperiod() {
		return currentperiod;
	}

	public DateItem getCurrentstartdate() {
		return currentstartdate;
	}

	public DateItem getCurrentenddate() {
		return currentenddate;
	}

}
