package com.pearl.accounting.sysui.client.presenters;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.pearl.accounting.sysui.client.views.panes.GeneralLedgerPane;
import com.smartgwt.client.widgets.layout.VLayout;

public class GeneralLedgerView extends ViewImpl implements GeneralLedgerPresenter.MyView {

	private static final String DEFAULT_MARGIN = "0px"; 
	private VLayout panel;
	private GeneralLedgerPane generalLedgerPane;

	@Inject
	public GeneralLedgerView() {

		panel = new VLayout();

		generalLedgerPane = new GeneralLedgerPane();

		panel.setMembers(generalLedgerPane);
		panel.setWidth100();
		panel.setHeight("90%");
		Window.enableScrolling(false);
		Window.setMargin(DEFAULT_MARGIN);

	}

	public Widget asWidget() {
		return panel;
	}

	public GeneralLedgerPane getGeneralLedgerPane() {
		return generalLedgerPane;
	}
	
	

}
