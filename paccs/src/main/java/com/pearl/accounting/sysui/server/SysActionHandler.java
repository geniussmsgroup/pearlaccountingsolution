package com.pearl.accounting.sysui.server;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.gwtplatform.dispatch.server.ExecutionContext;
import com.gwtplatform.dispatch.server.actionhandler.AbstractActionHandler;
import com.gwtplatform.dispatch.shared.ActionException;
import com.pearl.accounting.syscore.services.AccountService;
import com.pearl.accounting.syscore.services.AssessmentPeriodService;
import com.pearl.accounting.syscore.services.AtypeAservice;
import com.pearl.accounting.syscore.services.CashBookPaymentService;
import com.pearl.accounting.syscore.services.CashBookReceiptService;
import com.pearl.accounting.syscore.services.ClientStatementService;
import com.pearl.accounting.syscore.services.CustomerAccountService;
import com.pearl.accounting.syscore.services.DoubleEntryService;
import com.pearl.accounting.syscore.services.FinancialYearService;
import com.pearl.accounting.syscore.services.FixedDataService;
import com.pearl.accounting.syscore.services.GeneralLedgerService;
import com.pearl.accounting.syscore.services.IntervaltbService;
import com.pearl.accounting.syscore.services.LoanBalanceViewService;
import com.pearl.accounting.syscore.services.LoanDepositService;
import com.pearl.accounting.syscore.services.LoanRepaymentService;
import com.pearl.accounting.syscore.services.LoanRepaymentsService;
import com.pearl.accounting.syscore.services.LoanService;
import com.pearl.accounting.syscore.services.LoaninterestViewService;
import com.pearl.accounting.syscore.services.MonthDataService;
import com.pearl.accounting.syscore.services.MultiJournalService;
import com.pearl.accounting.syscore.services.MultiService;
import com.pearl.accounting.syscore.services.PeriodicDepositService;
import com.pearl.accounting.syscore.services.ProductSetupService;
import com.pearl.accounting.syscore.services.ReceiptnoService;
//import com.pearl.accounting.syscore.services.ProductTestService;
//import com.pearl.accounting.syscore.services.ProductdetailsService;
import com.pearl.accounting.syscore.services.SalaryService;
import com.pearl.accounting.syscore.services.SystemUserService;
import com.pearl.accounting.syscore.services.TheadpService;
import com.pearl.accounting.syscore.services.TotalinterestService;
import com.pearl.accounting.syscore.services.TransGLService;
import com.pearl.accounting.syscore.services.TransMLService;
import com.pearl.accounting.syscore.services.UserPermissionService;
import com.pearl.accounting.syscore.services.UserRoleService;
import com.pearl.accounting.syscore.services.WithdrawalService;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.AssessmentPeriod;
import com.pearl.accounting.sysmodel.AtypeA;
import com.pearl.accounting.sysmodel.CashBookPayment;
import com.pearl.accounting.sysmodel.CashBookPaymentDetail;
import com.pearl.accounting.sysmodel.CashBookReceipt;
import com.pearl.accounting.sysmodel.CashBookReceiptDetail;
import com.pearl.accounting.sysmodel.ClientStatement;
import com.pearl.accounting.sysmodel.CustomerAccount;
import com.pearl.accounting.sysmodel.DoubleEntry;
import com.pearl.accounting.sysmodel.FinancialYear;
import com.pearl.accounting.sysmodel.FixedData;
import com.pearl.accounting.sysmodel.GeneralLedger;
import com.pearl.accounting.sysmodel.Intervaltb;
import com.pearl.accounting.sysmodel.Loan;
import com.pearl.accounting.sysmodel.LoanDeposit;
import com.pearl.accounting.sysmodel.LoanRepayment;
import com.pearl.accounting.sysmodel.LoanRepayments;
import com.pearl.accounting.sysmodel.LoanbalanceView;
import com.pearl.accounting.sysmodel.LoaninterestView;
import com.pearl.accounting.sysmodel.Multi;
import com.pearl.accounting.sysmodel.MultiDetail;
import com.pearl.accounting.sysmodel.MultiJournal;
import com.pearl.accounting.sysmodel.MultiJournalDetail;
import com.pearl.accounting.sysmodel.PeriodicDeposit;
import com.pearl.accounting.sysmodel.ProductSetup;
import com.pearl.accounting.sysmodel.Receiptno;
import com.pearl.accounting.sysmodel.Salary;
import com.pearl.accounting.sysmodel.SwizFeedback;
import com.pearl.accounting.sysmodel.SystemUser;
import com.pearl.accounting.sysmodel.Theadp;
import com.pearl.accounting.sysmodel.Totalinterest;
import com.pearl.accounting.sysmodel.TransGL;
import com.pearl.accounting.sysmodel.TransML;
import com.pearl.accounting.sysmodel.UserPermission;
import com.pearl.accounting.sysmodel.UserRole;
import com.pearl.accounting.sysmodel.Withdrawal;
//import com.pearl.accounting.sysmodel.productdetails;
import com.pearl.accounting.sysui.client.utils.PAConstant;
import com.pearl.accounting.sysui.shared.SysAction;
import com.pearl.accounting.sysui.shared.SysResult;

public class SysActionHandler extends AbstractActionHandler<SysAction, SysResult> {
	@Autowired
	private TransMLService transmlService;
	@Autowired
	private IntervaltbService intervalService;
	@Autowired
	private MonthDataService monthDataService;
	@Autowired
	private UserRoleService userRoleService;
	@Autowired
	private TotalinterestService totalinterestService;
	@Autowired
	private UserPermissionService userPermissionService;

	@Autowired
	private SystemUserService systemUserService;
	@Autowired
	private LoanRepaymentsService loanrepaymentsService;

	@Autowired
	private ProductSetupService productsetupService;

	@Autowired
	private FinancialYearService financialYearService;
	@Autowired
	private DoubleEntryService doubleentryService;

	@Autowired
	private AssessmentPeriodService assessmentPeriodService;

	@Autowired
	private AccountService accountService;

	@Autowired
	private CustomerAccountService customerAccountService;

	@Autowired
	private PeriodicDepositService periodicDepositService;
	@Autowired
	private LoanDepositService loandepositService;
	@Autowired
	private LoanService loanService;

	@Autowired
	private WithdrawalService withdrawalService;

	@Autowired
	private LoanRepaymentService loanRepaymentService;
	@Autowired
	private LoanBalanceViewService loanbalanceviewService;
	@Autowired
	private LoaninterestViewService loaninterestService;
	@Autowired
	private FixedDataService fixeddataService;
	// LoanDeposit

	@Autowired
	private TheadpService theadpService;
	@Autowired
	private MultiService multiService;

	@Autowired
	private GeneralLedgerService generalLedgerService;

	@Autowired
	private ClientStatementService clientStatementService;

	@Autowired
	private CashBookPaymentService cashBookPaymentService;

	@Autowired
	private CashBookReceiptService cashBookReceiptService;

	@Autowired
	private MultiJournalService multiJournalService;

	@Autowired
	private SalaryService salaryService;
	@Autowired
	private TransGLService transglService;

	@Autowired
	private AtypeAservice atypeaService;

	

	@Autowired
	private ReceiptnoService receiptService;

	
	
//	@Autowired
//	private ProductTestService productTestService;

	public SysActionHandler() {
		super(SysAction.class);
	}

	public SysResult execute(SysAction action, ExecutionContext context) throws ActionException {
		if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_FinancialYear)) {

			SwizFeedback feedback = financialYearService.save(action.getFinancialYear());
			List<FinancialYear> list = financialYearService.find();

			return new SysResult(feedback, list, null);

		} else if (action.getOperation().equalsIgnoreCase(PAConstant.UPDATE_FinancialYear)) {

			SwizFeedback feedback = financialYearService.update(action.getFinancialYear());
			List<FinancialYear> list = financialYearService.find();

			return new SysResult(feedback, list, null);

		} else if (action.getOperation().equalsIgnoreCase(PAConstant.DELETE_FinancialYear)) {

			SwizFeedback feedback = financialYearService.delete(action.getFinancialYear());
			List<FinancialYear> list = financialYearService.find();

			return new SysResult(feedback, list, null);

		} else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_FinancialYear)) {

			SwizFeedback feedback = new SwizFeedback();
			List<FinancialYear> list = financialYearService.find();

			return new SysResult(feedback, list, null);

		}


		if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_RECEIPT)) {

			SwizFeedback feedback = receiptService.save(action.getReceiptno());
			List<Receiptno> list = receiptService.find();

			return new SysResult(feedback, list, null);

		}
		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_RECEIPT)) {
			SwizFeedback feedback = new SwizFeedback();
			List<Receiptno> list = receiptService.find();
			return new SysResult(feedback, list, null);

		}

		
		
		
		
		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_FixedData)) {
		SwizFeedback feedback = new SwizFeedback();
		List<FixedData> list = fixeddataService.find();
		return new SysResult(feedback, list, null);

	}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.ACTIVATE_FinancialYear)) {

			SwizFeedback feedback = financialYearService.activate(action.getFinancialYear());
			List<FinancialYear> list = financialYearService.find();

			return new SysResult(feedback, list, null);

		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.DEACTIVATE_FinancialYear)) {

			SwizFeedback feedback = financialYearService.deactivate(action.getFinancialYear());
			List<FinancialYear> list = financialYearService.find();

			return new SysResult(feedback, list, null);

		}
		//GET_CustomersLoanid
		else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_PRODUCTSETUP)) {

			SwizFeedback feedback = productsetupService.save(action.getProductSetup());

			List<ProductSetup> list = productsetupService.find();

			return new SysResult(feedback, list, null);
		} else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_PRODUCTSETUP)) {

			List<ProductSetup> list = productsetupService.find();

			return new SysResult(null, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_AtypeA)) {

			SwizFeedback feedback = atypeaService.save(action.getAtypeA());

			List<AtypeA> list = atypeaService.find();

			return new SysResult(feedback, list, null);
		} else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_AtypeA)) {

			List<AtypeA> list = atypeaService.find();

			return new SysResult(null, list, null);
		}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_ROLE)) {

			SwizFeedback feedback = userRoleService.save(action.getUserRole());

			List<UserRole> list = userRoleService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.EDIT_ROLE)) {

			SwizFeedback feedback = userRoleService.update(action.getUserRole());

			List<UserRole> list = userRoleService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.DELETE_ROLE)) {

			SwizFeedback feedback = userRoleService.delete(action.getUserRole());

			List<UserRole> list = userRoleService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.RETRIVE_ROLE)) {

			List<UserRole> list = userRoleService.find();

			return new SysResult(null, list, null);
		} else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_LOANREPAYMENTS)) {
//
			List<LoanRepayments> list = loanrepaymentsService.find();

			return new SysResult(null, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_UserPermission)) {

			SwizFeedback feedback = userPermissionService.save(action.getUserPermission());

			List<UserPermission> list = userPermissionService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.EDIT_UserPermission)) {

			SwizFeedback feedback = userPermissionService.update(action.getUserPermission());

			List<UserPermission> list = userPermissionService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.DELETE_UserPermission)) {

			SwizFeedback feedback = userPermissionService.delete(action.getUserPermission());

			List<UserPermission> list = userPermissionService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_UserPermission)) {

			List<UserPermission> list = userPermissionService.find();

			return new SysResult(null, list, null);
		}
/////
		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_CurrentPeriod)) {

			List<Intervaltb> list = intervalService.findActive();

			return new SysResult(null, list, null);
		}

		///////
		else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_SystemUser)) {

			SwizFeedback feedback = systemUserService.save(action.getSystemUser());

			List<SystemUser> list = systemUserService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.EDIT_SystemUser)) {

			SwizFeedback feedback = systemUserService.update(action.getSystemUser());

			List<SystemUser> list = systemUserService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.DELETE_SystemUser)) {

			SwizFeedback feedback = systemUserService.delete(action.getSystemUser());

			List<SystemUser> list = systemUserService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_SystemUser)) {

			List<SystemUser> list = systemUserService.find();

			return new SysResult(null, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.RESET_PASSWORD)) {

			System.out.println("RESET_PASSWORD");
			SwizFeedback feedback = systemUserService.resetUserPassword(action.getSystemUser());

			List<SystemUser> list = systemUserService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_AssessmentPeriod)) {

			SwizFeedback feedback = assessmentPeriodService.save(action.getAssessmentPeriod());

			List<AssessmentPeriod> list = assessmentPeriodService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.EDIT_AssessmentPeriod)) {

			SwizFeedback feedback = assessmentPeriodService.update(action.getAssessmentPeriod());

			List<AssessmentPeriod> list = assessmentPeriodService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.DELETE_AssessmentPeriod)) {

			SwizFeedback feedback = assessmentPeriodService.delete(action.getAssessmentPeriod());

			List<AssessmentPeriod> list = assessmentPeriodService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_AssessmentPeriod)) {

			System.out.println("GET_AssessmentPeriod");

			List<AssessmentPeriod> list = assessmentPeriodService.find();

			System.out.println("GET_AssessmentPeriod: " + list.size());

			return new SysResult(null, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.ACTIVATE_AssessmentPeriod)) {

			SwizFeedback feedback = assessmentPeriodService.activate(action.getAssessmentPeriod());

			List<AssessmentPeriod> list = assessmentPeriodService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.DEACTIVATE_AssessmentPeriod)) {

			SwizFeedback feedback = assessmentPeriodService.deactivate(action.getAssessmentPeriod());

			List<AssessmentPeriod> list = assessmentPeriodService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_ACCOUNT)) {

			SwizFeedback feedback = accountService.save(action.getAccount());

			List<Account> list = accountService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.EDIT_ACCOUNT)) {

			SwizFeedback feedback = accountService.update(action.getAccount());

			List<Account> list = accountService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.DELETE_ACCOUNT)) {

			SwizFeedback feedback = accountService.delete(action.getAccount());

			List<Account> list = accountService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_ACCOUNT)) {

			List<Account> list = accountService.find();

			return new SysResult(null, list, null);
		} else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_LoanBlanceQry)) {

			List<LoanbalanceView> list = loanbalanceviewService.find();

			return new SysResult(null, list, null);

		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_LoanRateQry)) {

			List<LoaninterestView> list = loaninterestService.find();

			return new SysResult(null, list, null);

		} else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_Totalinterest)) {

			List<Totalinterest> list = totalinterestService.find();

			return new SysResult(null, list, null);

		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_Totalinterest)) {

			SwizFeedback feedback = totalinterestService.save(action.getTotalinterest());

			List<Totalinterest> list = totalinterestService.find();

			return new SysResult(feedback, list, null);
		}
		//////////////////////// uhbjshloandeposits
		else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_LOANDEPOSITS)) {

			SwizFeedback feedback = loandepositService.save(action.getLoandeposit());

			List<LoanDeposit> list = loandepositService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.EDIT_LOANDEPOSITS)) {

			SwizFeedback feedback = loandepositService.update(action.getLoandeposit());

			List<LoanDeposit> list = loandepositService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.DELETE_LOANDEPOSITS)) {

			SwizFeedback feedback = loandepositService.delete(action.getLoandeposit());

			List<LoanDeposit> list = loandepositService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_LOANDEPOSITS)) {

			List<LoanDeposit> list = loandepositService.find();

			return new SysResult(null, list, null);
		}
		//// end
		else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_Thead)) {

			SwizFeedback feedback = theadpService.save(action.getTheadp());

			List<Theadp> list = theadpService.find();

			return new SysResult(feedback, list, null);
		} else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_Thead)) {

			List<Theadp> list = theadpService.find();

			return new SysResult(null, list, null);
		} else if (action.getOperation().equalsIgnoreCase(PAConstant.DELETE_Thead)) {
			SwizFeedback feedback = theadpService.delete(action.getTheadp());
			List<Theadp> list = theadpService.find();

			return new SysResult(feedback, list, null);
		}

////

		else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_DOUBLEENTRY)) {

			SwizFeedback feedback = doubleentryService.save(action.getDoubleEntry());

			List<DoubleEntry> list = doubleentryService.find();

			return new SysResult(feedback, list, null);
		} else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_DOUBLEENTRY)) {

			List<DoubleEntry> list = doubleentryService.find();

			return new SysResult(null, list, null);
		} else if (action.getOperation().equalsIgnoreCase(PAConstant.DELETE_DoubleEntry)) {
			SwizFeedback feedback = doubleentryService.delete(action.getDoubleEntry());
			List<DoubleEntry> list = doubleentryService.find();
			return new SysResult(feedback, list, null);
		}

//		else if (action.getOperation().equalsIgnoreCase(PAConstant.EDIT_ACCOUNT)) {
//
//			SwizFeedback feedback = theadpService.update(action.getTheadp());
//
//			List<Theadp> list = theadpService.find();
//
//			return new SysResult(feedback, list, null);
//		}
//

		//////

		//////////////////

		else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_customerAccount)) {

			SwizFeedback feedback = customerAccountService.save(action.getCustomerAccount());

			List<CustomerAccount> list = customerAccountService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.EDIT_customerAccount)) {

			SwizFeedback feedback = customerAccountService.update(action.getCustomerAccount());

			List<CustomerAccount> list = customerAccountService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.DELETE_customerAccount)) {

			SwizFeedback feedback = customerAccountService.delete(action.getCustomerAccount());

			List<CustomerAccount> list = customerAccountService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_customerAccount)) {

			List<CustomerAccount> list = customerAccountService.find();

			return new SysResult(null, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_PeriodicDeposit)) {

			SwizFeedback feedback = periodicDepositService.save(action.getPeriodicDeposit());

			List<PeriodicDeposit> list = periodicDepositService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.EDIT_PeriodicDeposit)) {

			SwizFeedback feedback = periodicDepositService.update(action.getPeriodicDeposit());

			List<PeriodicDeposit> list = periodicDepositService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.DELETE_PeriodicDeposit)) {

			SwizFeedback feedback = periodicDepositService.delete(action.getPeriodicDeposit());

			List<PeriodicDeposit> list = periodicDepositService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_PeriodicDeposit)) {

			List<PeriodicDeposit> list = periodicDepositService.find();

			return new SysResult(null, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_LOAN)) {

			SwizFeedback feedback = loanService.save(action.getLoan());

			List<Loan> list = loanService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.EDIT_LOAN)) {

			SwizFeedback feedback = loanService.update(action.getLoan());

			List<Loan> list = loanService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.DELETE_LOAN)) {

			SwizFeedback feedback = loanService.delete(action.getLoan());

			List<Loan> list = loanService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_LOAN)) {

			// SwizFeedback feedback = loanService.delete(action.getLoan());

			List<Loan> list = loanService.find();

			return new SysResult(null, list, null);
		}
		
		
//		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_CustomersLoanid)) {
//
//		   Loan loan = loanService.findBy(action.getCustomerAccount());
//			//List<Loan> list = loanService.find();
//			return new SysResult(null, loan, null);
//		}
//		
		//GET_CustomersLoanid
////////////////////////////
		else if (action.getOperation().equalsIgnoreCase(PAConstant.ACTIVE_INTERVAL_PERIOD)) {

			SwizFeedback feedback = intervalService.activeperiod(action.getIntervaltb());
			List<Intervaltb> list = intervalService.find();

			return new SysResult(feedback, list, null);

		}
		
		
		/////////////loandeposit LoanDeposit

		else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_WITHDRAWAL)) {

			SwizFeedback feedback = withdrawalService.save(action.getWithdrawal());

			List<Withdrawal> list = withdrawalService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.EDIT_WITHDRAWAL)) {

			SwizFeedback feedback = withdrawalService.update(action.getWithdrawal());

			List<Withdrawal> list = withdrawalService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.DELETE_WITHDRAWAL)) {

			SwizFeedback feedback = withdrawalService.delete(action.getWithdrawal());

			List<Withdrawal> list = withdrawalService.find();

			return new SysResult(feedback, list, null);

		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_WITHDRAWAL)) {

			List<Withdrawal> list = withdrawalService.find();

			return new SysResult(null, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_LOAN_REPAYMENT)) {

			SwizFeedback feedback = loanRepaymentService.save(action.getLoanRepayment());

			// List<RepaymentSchedule> list =
			// loanService.getRepaymentSchedule(action.getLoan());

			List<LoanRepayment> loanRepayments = loanRepaymentService.find(action.getLoan());

			return new SysResult(feedback, null, loanRepayments, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_RepaymentSchedules)) {

			// List<RepaymentSchedule> list =
			// loanService.getRepaymentSchedule(action.getLoan());

			List<LoanRepayment> loanRepayments = loanRepaymentService.find(action.getLoan());

			return new SysResult(null, null, loanRepayments, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_GENERAL_LEDGER)) {

			List<GeneralLedger> list = generalLedgerService.find();

			return new SysResult(null, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_CLIENT_STATEMENT)) {

			List<ClientStatement> list = clientStatementService.find();

			return new SysResult(null, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_CASHBOOK_PAYMENT)) {

//			

			SwizFeedback feedback = cashBookPaymentService.savetoSpring(action.getCashBookPayment());

			List<CashBookPayment> list = cashBookPaymentService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.EDIT_CASHBOOK_PAYMENT)) {

			SwizFeedback feedback = cashBookPaymentService.update(action.getCashBookPayment());

			List<CashBookPayment> list = cashBookPaymentService.find();
			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.DELETE_CASHBOOK_PAYMENT)) {

			SwizFeedback feedback = cashBookPaymentService.delete(action.getCashBookPayment());

			List<CashBookPayment> list = cashBookPaymentService.find();

			return new SysResult(feedback, list, null);

		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_CASHBOOK_PAYMENT)) {

			List<CashBookPayment> list = cashBookPaymentService.find();

			return new SysResult(null, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_CASHBOOK_PAYMENT_DETAILS)) {

			List<CashBookPaymentDetail> list = cashBookPaymentService.find(action.getCashBookPayment());

			return new SysResult(null, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_CASHBOOK_RECEIPT)) {

			SwizFeedback feedback = cashBookReceiptService.save(action.getCashBookReceipt());

			List<CashBookReceipt> list = cashBookReceiptService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.EDIT_CASHBOOK_RECEIPT)) {

			SwizFeedback feedback = cashBookReceiptService.update(action.getCashBookReceipt());

			List<CashBookReceipt> list = cashBookReceiptService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.DELETE_CASHBOOK_RECEIPT)) {

			SwizFeedback feedback = cashBookReceiptService.delete(action.getCashBookReceipt());

			List<CashBookReceipt> list = cashBookReceiptService.find();

			return new SysResult(feedback, list, null);

		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_CASHBOOK_RECEIPT)) {

			List<CashBookReceipt> list = cashBookReceiptService.find();

			return new SysResult(null, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_CASHBOOK_RECEIPT_DETAILS)) {

			List<CashBookReceiptDetail> list = cashBookReceiptService.find(action.getCashBookReceipt());

			return new SysResult(null, list, null);
		}
/////////////////////////////////
		else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_MULTI_JOURNAL)) {

			// System.out.println(action.getMultiJournal().getMultiJournalDetails().get(0).getLoan().getId());
			// System.out.println(action.getMultiJournal().getMultiJournalDetails().get(1).getLoan().getId());

			SwizFeedback feedback = multiJournalService.save(action.getMultiJournal());

			List<MultiJournal> list = multiJournalService.find();

			return new SysResult(feedback, list, null);

		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.EDIT_MULTI_JOURNAL)) {

			SwizFeedback feedback = multiJournalService.update(action.getMultiJournal());

			List<MultiJournal> list = multiJournalService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.DELETE_MULTI_JOURNAL)) {

			SwizFeedback feedback = multiJournalService.delete(action.getMultiJournal());

			List<MultiJournal> list = multiJournalService.find();

			return new SysResult(feedback, list, null);

		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_MULTI_JOURNAL)) {

			List<MultiJournal> list = multiJournalService.find();

			return new SysResult(null, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_MULTI_JOURNAL_DETAILS)) {

			List<MultiJournalDetail> list = multiJournalService.find(action.getMultiJournal());

			return new SysResult(null, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_MULTI_JOURNAL_DETAILS_INPUT_DATA)) {

			List<Account> list = accountService.find();
			List<CustomerAccount> list2 = customerAccountService.find();
			List<Loan> list3 = loanService.find();
			// List<productdetails>list4 = productdetailsService.find();
			return new SysResult(null, list, list2, list3, null);
		} else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_SALARY)) {

			SwizFeedback feedback = salaryService.save(action.getSalary());

			List<Salary> list = salaryService.find();

			return new SysResult(feedback, list, null);

		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_Multi)) {

			// System.out.println(action.getMultiJournal().getMultiJournalDetails().get(0).getLoan().getId());
			// System.out.println(action.getMultiJournal().getMultiJournalDetails().get(1).getLoan().getId());

			SwizFeedback feedback = multiService.save(action.getMulti());

			List<Multi> list = multiService.find();

			return new SysResult(feedback, list, null);

		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.EDIT_Multi)) {

			SwizFeedback feedback = multiService.update(action.getMulti());

			List<Multi> list = multiService.find();

			return new SysResult(feedback, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.DELETE_Multi)) {

			SwizFeedback feedback = multiService.delete(action.getMulti());

			List<Multi> list = multiService.find();

			return new SysResult(feedback, list, null);

		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_Multi)) {

			List<Multi> list = multiService.find();

			return new SysResult(null, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_Multi_DETAILS)) {

			List<MultiDetail> list = multiService.find(action.getMulti());

			return new SysResult(null, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_MULTI_DETAILS_INPUT)) {

			List<Account> list = accountService.find();
			List<CustomerAccount> list2 = customerAccountService.find();
			List<Loan> list3 = loanService.find();
			// List<productdetails>list4 = productdetailsService.find();
			return new SysResult(null, list, list2, list3, new CustomerAccount());
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_Interval)) {

			List<Intervaltb> list = intervalService.find();

			return new SysResult(null, list, null);
		} else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_Interval)) {

			SwizFeedback feedback = intervalService.save(action.getIntervaltb());

			List<Intervaltb> list = intervalService.find();

			return new SysResult(feedback, list, null);

		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_TRANSGL)) {

			List<TransGL> list = transglService.find();

			return new SysResult(null, list, null);
		}

		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_TRANSML)) {

			List<TransML> list = transmlService.find();

			return new SysResult(null, list, null);
		}

//		} else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_PRODUCTTYPE)) {
//
//			SwizFeedback feedback = productdetailsService.save(action.getProductdetails());
//			
//			List<productdetails> list = productdetailsService.find();
//
//			return new SysResult(feedback, list, null);
//			
//
//		} else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_PRODUCTTEST)) {
//
//			SwizFeedback feedback = productTestService.save(action.getProductTest());
//			
//			List<ProductTest> list = productTestService.find();
//
//			return new SysResult(feedback, list, null);
//		}
//		else if (action.getOperation().equalsIgnoreCase(PAConstant.GET_PRODUCTTYPE)) {
//
//
//			List<productdetails> list = productdetailsService.find();
//
//			System.out.println("GET_PRODUCTTYPE: " + list.size());
//
//			return new SysResult(null, list, null);
//		}
//
//		else if (action.getOperation().equalsIgnoreCase(PAConstant.DELETE_PRODUCTTYPE)) {
//
//			SwizFeedback feedback = productdetailsService.delete(action.getProductdetails());
//
//			List<productdetails> list = productdetailsService.find();
//

		else if (action.getOperation().equalsIgnoreCase(PAConstant.SAVE_SystemUser)) {

			SwizFeedback feedback = systemUserService.save(action.getSystemUser());

			List<SystemUser> list = systemUserService.find();

			return new SysResult(feedback, list, null);
		} // return new SysResult(feedback, list, null);
//		}
		else if (action.getOperation().equalsIgnoreCase(PAConstant.NEXT_Interval)) {

			SwizFeedback feed = monthDataService.NextInterval();

			return new SysResult(feed, null, null, null);
		}
		
		return null;
	}

	
	
	public void undo(SysAction action, SysResult result, ExecutionContext context) throws ActionException {
	}

	@Override
	public Class<SysAction> getActionType() {
		return SysAction.class;
	}

}
