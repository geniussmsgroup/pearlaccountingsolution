package com.pearl.accounting.sysui.client.presenters;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl; 
import com.pearl.accounting.sysui.client.views.panes.CashBookPaymentPane;
import com.smartgwt.client.widgets.layout.VLayout;

public class CashBookPaymentView extends ViewImpl implements CashBookPaymentPresenter.MyView {

	private static final String DEFAULT_MARGIN = "0px";
	private VLayout panel;
	private CashBookPaymentPane cashBookPaymentPane;

	@Inject
	public CashBookPaymentView() {

		panel = new VLayout();

		cashBookPaymentPane = new CashBookPaymentPane();

		panel.setMembers(cashBookPaymentPane); 
		panel.setWidth100();
		panel.setHeight("90%");
		Window.enableScrolling(false);
		Window.setMargin(DEFAULT_MARGIN);

	}

	public Widget asWidget() {
		return panel;
	}

	public CashBookPaymentPane getCashBookPaymentPane() {
		return cashBookPaymentPane;
	}


}
