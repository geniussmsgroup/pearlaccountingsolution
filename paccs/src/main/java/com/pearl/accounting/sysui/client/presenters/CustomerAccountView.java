package com.pearl.accounting.sysui.client.presenters;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl; 
import com.pearl.accounting.sysui.client.views.panes.CustomerAccountPane;
import com.smartgwt.client.widgets.layout.VLayout;

public class CustomerAccountView extends ViewImpl implements CustomerAccountPresenter.MyView {

	private static final String DEFAULT_MARGIN = "0px";
	private VLayout panel;
	private CustomerAccountPane customerAccountPane;

	@Inject
	public CustomerAccountView() {

		panel = new VLayout();

		customerAccountPane = new CustomerAccountPane();

		panel.setMembers(customerAccountPane);
		panel.setWidth100();
		panel.setHeight("90%");
		Window.enableScrolling(false);
		Window.setMargin(DEFAULT_MARGIN);

	}

	public Widget asWidget() {
		return panel;
	}

	public CustomerAccountPane getCustomerAccountPane() {
		return customerAccountPane;
	}

}
