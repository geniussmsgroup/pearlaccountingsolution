package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.Intervaltb;
import com.pearl.accounting.sysmodel.TransGL;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class IntervalListgrid extends SuperListGrid {

	public static String ID = "id";
	public static String year = "Year";
	public static String period = "period";
	public static String startdate = "startdate";
	public static String enddate = "enddate";
	public static String yr = "yr";
	public static String Current = "Current";

	public IntervalListgrid() {

		super();

		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);
		ListGridField Year = new ListGridField(year, "year");
		ListGridField Period = new ListGridField(period, "period");
		ListGridField Startdate = new ListGridField(startdate, "Startdate");
		ListGridField Enddate = new ListGridField(enddate, "Enddate");
		ListGridField yrc= new ListGridField(yr, "yearcode");
		ListGridField curr= new ListGridField(Current, "Current");

		this.setFields(id, Year,Period,Startdate,Enddate,yrc,curr);
	}
	public ListGridRecord addRowData(Intervaltb account) {
		ListGridRecord record = new ListGridRecord(); 
		record.setAttribute(ID, account.getSysId());
		record.setAttribute(period, account.getFINTNAME());
		record.setAttribute(year, account.getFINTCODE());
		record.setAttribute(startdate, account.getFSTART());
		record.setAttribute(enddate, account.getFEND());
		record.setAttribute(yr, account.getYRCODE());
		return record;

		}
	public void addRecordsToGrid(List<Intervaltb> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (Intervaltb item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
	}

}
