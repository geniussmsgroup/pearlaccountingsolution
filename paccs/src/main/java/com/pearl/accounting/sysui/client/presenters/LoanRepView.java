//package com.pearl.accounting.sysui.client.presenters;
//
//import com.google.gwt.user.client.Window;
//import com.google.gwt.user.client.ui.Widget;
//import com.google.inject.Inject;
//import com.gwtplatform.mvp.client.ViewImpl;
//import com.pearl.accounting.sysui.client.views.panes.LoanRepayment2Pane;
//import com.pearl.accounting.sysui.client.views.panes.PeriodicDepositPane;
//import com.smartgwt.client.widgets.layout.VLayout;
//
//public class LoanRepView extends ViewImpl implements LoanRepayment2Presenter.MyView {
//
//	private static final String DEFAULT_MARGIN = "0px";
//	private VLayout panel;
//	private LoanRepayment2Pane loanRepayment2Pane;
//
//	@Inject
//	public LoanRepayment2View() {
//
//		panel = new VLayout();
//
//		loanRepayment2Pane = new LoanRepayment2Pane();
//
//		panel.setMembers(loanRepayment2Pane);
//		panel.setWidth100();
//		panel.setHeight("90%");
//		Window.enableScrolling(false);
//		Window.setMargin(DEFAULT_MARGIN);
//
//	}
//
//	public Widget asWidget() {
//		return panel;
//	}
//
//	public LoanRepayment2Pane getLoanRepayment2Pane() {
//		return loanRepayment2Pane;
//	}
//
//}
