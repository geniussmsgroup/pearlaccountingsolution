package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.pearl.accounting.sysmodel.SystemUser;
import com.smartgwt.client.types.SortDirection;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;

public class UserListgrid extends SuperListGrid {

	public static String ID = "id";
	public static String USER_NAME = "userName";
	public static String FIRST_NAME = "firstName";
	public static String LAST_NAME = "lastName";
	public static String PHONE_NUMBER = "phoneNo";
	public static String EMAIL = "email";
	public static String ROLE_ID = "roleId";
	public static String ROLE = "role";
	public static String PARTNER = "partner";
	
	public static String PARTNER_ID = "partnerId";
	
	private UserDS dataSource;

	public UserListgrid() {
		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);

		ListGridField userName = new ListGridField(USER_NAME, "User Name");

		ListGridField firstName = new ListGridField(FIRST_NAME, "First Name");
		ListGridField lastName = new ListGridField(LAST_NAME, "Last Name");

		ListGridField phoneNo = new ListGridField(PHONE_NUMBER, "Phone number");
		ListGridField role = new ListGridField(ROLE, "Role");

		ListGridField roleId = new ListGridField(ROLE_ID, "Role Id");
		roleId.setHidden(true);

		ListGridField email = new ListGridField(EMAIL, "Email");
		
		ListGridField partner = new ListGridField(PARTNER, "Organisation/Partner");
		ListGridField partnerId = new ListGridField(PARTNER_ID, "Partner Id");
		partnerId.setHidden(true);

		this.setFields(id, userName, firstName, lastName, phoneNo, roleId, role, email, partner,partnerId);
		this.setCanExpandRecords(true);
		
		this.setSortField(USER_NAME);
		this.setSortDirection(SortDirection.ASCENDING);

		this.setShowFilterEditor(true); 
		dataSource = new UserDS(this);
		this.setDataSource(dataSource);
		
	}

	public ListGridRecord addRowData(SystemUser systemUser) {
		ListGridRecord record = new ListGridRecord();
		record.setAttribute(ID, systemUser.getId());

		record.setAttribute(USER_NAME, systemUser.getUsername());
		record.setAttribute(FIRST_NAME, systemUser.getFirstName());
		record.setAttribute(LAST_NAME, systemUser.getLastName());

		record.setAttribute(PHONE_NUMBER, systemUser.getPhoneNumber());

		record.setAttribute(EMAIL, systemUser.getEmailAddress());

		if (systemUser.getUserRole() != null) {

			record.setAttribute(ROLE, systemUser.getUserRole().getEmployeeRole());

			record.setAttribute(ROLE_ID, systemUser.getUserRole().getId());
		}
		 

		return record;
	}

	public void addRecordsToGrid(List<SystemUser> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (SystemUser item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
		dataSource.reloadRecords(records);
	}

}
