package com.pearl.accounting.sysui.client.views.windows;

import java.util.List;

import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysui.client.views.listgrids.CashBookReceiptDetailListgrid;
import com.pearl.accounting.sysui.client.views.widgets.ActivePeriod;
import com.pearl.accounting.sysui.client.views.widgets.ComboBox;
import com.pearl.accounting.sysui.client.views.widgets.TextField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class CashBookReceiptWindow extends Window {
	private TextField currentperiod;
    private DateItem currentstartdate;
    private DateItem currentenddate;
	private TextField payee;
	private TextField periodcode;
	private TextField paidAmount;
	private ComboBox assessmentPeriod;
	private ComboBox financialYear;
	private DateItem paymentDate;
	private ComboBox receivingAccount;
	private TextField paymentDetails;
	private TextField remarks;
	private static DateItem from;
	private static DateItem to;
	private CashBookReceiptDetailListgrid cashBookReceiptDetailListgrid;

	private IButton saveButton;
	private IButton addButton;
	private IButton deleteButton;
	private ActivePeriod acti;

	public CashBookReceiptWindow(List<Account> accounts) {
		super();
		currentperiod = new TextField();
		currentperiod.setTitle("currentperiod");
		currentperiod.setWidth("10px");

		currentstartdate = new DateItem();
		currentstartdate.setTitle("Transaction Date");
		currentstartdate.setUseTextField(true);
		currentstartdate.setWidth("*");
		currentstartdate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		currentenddate = new DateItem();
		currentenddate.setTitle("Transaction Date");
		currentenddate.setUseTextField(true);
		currentstartdate.setWidth("*");
		currentstartdate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);
	
		
		financialYear = new ComboBox();
		financialYear.setTitle("Year");

		acti = new ActivePeriod();

		assessmentPeriod = new ComboBox();
		assessmentPeriod.setTitle("Selected Period");

		payee = new TextField();
		payee.setTitle("From");

		paidAmount = new TextField();
		paidAmount.setTitle("Amount");
		paidAmount.setKeyPressFilter("[0-9.]");

		paymentDate = new DateItem();
		paymentDate.setTitle("Payment Date");
		paymentDate.setUseTextField(true);
		paymentDate.setWidth("*");
		paymentDate.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		periodcode = new TextField();
		periodcode.setTitle("periodcode");
		periodcode.setWidth("10px");

		receivingAccount = new ComboBox();
		receivingAccount.setTitle("Receiving A/C");

		paymentDetails = new TextField();
		paymentDetails.setTitle("Receipt No.");

		remarks = new TextField();
		remarks.setTitle("Remarks");

		from = new DateItem();
		from.setTitle("From");
		from.setUseTextField(true);
		from.setWidth("*");
		from.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		to = new DateItem();
		to.setTitle("To");
		to.setUseTextField(true);
		to.setWidth("*");
		to.setDisplayFormat(DateDisplayFormat.TOEUROPEANSHORTDATE);

		saveButton = new IButton("Save");
		addButton = new IButton("Add");
		deleteButton = new IButton("delete");

		cashBookReceiptDetailListgrid = new CashBookReceiptDetailListgrid(accounts);

		DynamicForm form = new DynamicForm();
		form.setFields(assessmentPeriod, periodcode, financialYear, from, to);
		form.setWrapItemTitles(true);
		form.setMargin(10);
		form.setNumCols(8);

		DynamicForm form2 = new DynamicForm();
		form2.setFields(payee, paidAmount, paymentDate, receivingAccount, paymentDetails, remarks);
		form2.setWrapItemTitles(true);
		form2.setMargin(10);
		form2.setNumCols(4);
		DynamicForm form4 = new DynamicForm();
		form4.setFields(currentstartdate, currentperiod,currentenddate);
		form4.setWrapItemTitles(true);
		form4.setMargin(10);
		form4.setNumCols(6);
		HLayout buttonLayout = new HLayout();
		buttonLayout.setMembers(saveButton);
		buttonLayout.setAutoHeight();
		buttonLayout.setWidth100();
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(4);

		Label subheader = new Label();
		subheader.setStyleName("crm-ContextArea-Header-Label");
		subheader.setContents("Paying accounts details");
		subheader.setWidth("1%");
		subheader.setAutoHeight();
		// subheader.setMargin(10);
		subheader.setAlign(Alignment.LEFT);

		HLayout subbuttonLayout = new HLayout();
		subbuttonLayout.setMembers(addButton, deleteButton, subheader);
		subbuttonLayout.setAutoHeight();
		subbuttonLayout.setWidth100();
		subbuttonLayout.setMargin(5);
		// subbuttonLayout.setMembersMargin(4);

		VLayout layout = new VLayout();
		layout.addMember(form4);

		layout.addMember(form);
		layout.addMember(form2);
		layout.addMember(subbuttonLayout);
		layout.addMember(cashBookReceiptDetailListgrid);
		layout.addMember(buttonLayout);
		layout.setMargin(10);

		this.addItem(layout);
		this.setWidth("70%");
		this.setHeight("70%");
		this.setAutoCenter(true);
		this.setTitle("Cashbook Receipt definition");
		this.setIsModal(true);
		this.setShowModalMask(true);
	}

	public TextField getPayee() {
		return payee;
	}

	public TextField getPaidAmount() {
		return paidAmount;
	}

	public ComboBox getAssessmentPeriod() {
		return assessmentPeriod;
	}

	public ComboBox getFinancialYear() {
		return financialYear;
	}

	public DateItem getPaymentDate() {
		return paymentDate;
	}

	public ComboBox getReceivingAccount() {
		return receivingAccount;
	}

	public TextField getPaymentDetails() {
		return paymentDetails;
	}

	public TextField getPeriodcode() {
		return periodcode;
	}

	public TextField getRemarks() {
		return remarks;
	}

	public CashBookReceiptDetailListgrid getCashBookReceiptDetailListgrid() {
		return cashBookReceiptDetailListgrid;
	}

	public IButton getSaveButton() {
		return saveButton;
	}

	public IButton getAddButton() {
		return addButton;
	}

	public IButton getDeleteButton() {
		return deleteButton;
	}

	public void setDeleteButton(IButton deleteButton) {
		this.deleteButton = deleteButton;
	}

	public ActivePeriod getActi() {
		return acti;
	}

	public void setActi(ActivePeriod acti) {
		this.acti = acti;
	}

	public static DateItem getFrom() {
		return from;
	}

	public static void setFrom(DateItem from) {
		CashBookReceiptWindow.from = from;
	}

	public static DateItem getTo() {
		return to;
	}

	public static void setTo(DateItem to) {
		CashBookReceiptWindow.to = to;
	}

	public TextField getCurrentperiod() {
		return currentperiod;
	}

	public DateItem getCurrentstartdate() {
		return currentstartdate;
	}

	public DateItem getCurrentenddate() {
		return currentenddate;
	}

}
