package com.pearl.accounting.sysui.client.views.listgrids;

import java.util.List;

import com.google.gwt.i18n.client.NumberFormat;
import com.pearl.accounting.sysmodel.Account;
import com.pearl.accounting.sysmodel.DoubleEntry;
import com.pearl.accounting.sysmodel.Theadp;
import com.pearl.accounting.sysui.client.utils.PAManager;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.SummaryFunction;

public class DoubleEntryListgrid extends SuperListGrid {
	public static String ID = "id";
	public static String VNO = "Vno";
	public static String AMOUNT = "Amount";
	
	public static String RemarkS = "Remarks";
	public static String TransDate = "Tdate";
	//public static String TrackPData = "trackPData";
	NumberFormat nf = NumberFormat.getFormat("#,##0.00");

	public DoubleEntryListgrid() {

		super();
		ListGridField id = new ListGridField(ID, "Id");
		id.setHidden(true);
		ListGridField Remarks = new ListGridField(RemarkS, "Remarks");
		 
		ListGridField Tdate = new ListGridField(TransDate, "TransactionDate");
		ListGridField Vno = new ListGridField(VNO, "Vno");
       ListGridField Amount = new ListGridField(AMOUNT, "Amount");
		
		Amount.setShowGridSummary(true);

		Amount.setSummaryFunction(new SummaryFunction() {
			public Object getSummaryValue(Record[] records, ListGridField field) {

				long totalAmount = 0;
				for (Record record : records) {

					totalAmount += Float
							.parseFloat(PAManager.getInstance().unformatCash(record.getAttribute(AMOUNT)));

				}
				return "" + PAManager.getInstance().formatCash(totalAmount);
			}
		});
		
		
		//ListGridField trackPData = new ListGridField(TrackPData, "Track personal data");
		  

		this.setFields(id,Tdate, Vno,Amount,Remarks);
		this.setCanExpandRecords(true);
		this.setShowGridSummary(true);

	}

	public ListGridRecord addRowData(DoubleEntry account) {
		ListGridRecord record = new ListGridRecord(); 
		record.setAttribute(ID, account.getHDNO());
		record.setAttribute(VNO, account.getVNO());
		//record.setAttribute(AMOUNT, account.getAmount());
		record.setAttribute(TransDate, account.getTdates());
		record.setAttribute(RemarkS, account.getRemarks());
		
		record.setAttribute(AMOUNT, nf.format(account.getAmount()));

		 

		return record;
	}

	public void addRecordsToGrid(List<DoubleEntry> list) {
		ListGridRecord[] records = new ListGridRecord[list.size()];
		int row = 0;
		for (DoubleEntry item : list) {
			records[row] = addRowData(item);
			row++;
		}
		this.setData(records);
	}


}
