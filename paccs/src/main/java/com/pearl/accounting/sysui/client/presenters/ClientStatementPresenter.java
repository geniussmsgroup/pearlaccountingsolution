package com.pearl.accounting.sysui.client.presenters;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.dispatch.shared.DispatchAsync;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyCodeSplit;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.client.proxy.RevealContentEvent;
import com.pearl.accounting.sysui.client.place.NameTokens;
import com.pearl.accounting.sysui.client.utils.PAConstant;
import com.pearl.accounting.sysui.client.views.panes.ClientStatementPane;
import com.pearl.accounting.sysui.client.views.widgets.SwizimaLoader;
import com.pearl.accounting.sysui.shared.SysAction;
import com.pearl.accounting.sysui.shared.SysResult;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;

public class ClientStatementPresenter
		extends Presenter<ClientStatementPresenter.MyView, ClientStatementPresenter.MyProxy> {

	private final PlaceManager placeManager;
	private final DispatchAsync dispatcher;

	public interface MyView extends View {
		
		public ClientStatementPane getClientStatementPane();
		
	}

	@ProxyCodeSplit
	@NameToken(NameTokens.clientstatement)
	public interface MyProxy extends ProxyPlace<ClientStatementPresenter> {
	}

	@Inject
	public ClientStatementPresenter(final EventBus eventBus, final MyView view, final MyProxy proxy,
			final DispatchAsync dispatcher, final PlaceManager placeManager) {
		super(eventBus, view, proxy);

		this.dispatcher = dispatcher;
		this.placeManager = placeManager;
	}

	@Override
	protected void revealInParent() {
		RevealContentEvent.fire(this, MainPagePresenter.TYPE_SetContextAreaContent, this);
	}

	@Override
	protected void onBind() {
		super.onBind();
		
		onLoadButtonClicked();

	}

	private void onLoadButtonClicked(){
		getView().getClientStatementPane().getLoadButton().addClickHandler(new ClickHandler() {
			
			public void onClick(ClickEvent event) {
				
				loadClientStatements();
				
			}
		});
		
		getView().getClientStatementPane().getControlsPane().getRefreshButton().addClickHandler(new  ClickHandler() {
			
			public void onClick(ClickEvent event) {
				
				loadClientStatements();
				
			}
		});
	}
	private void loadClientStatements() {

		SC.showPrompt("", "", new SwizimaLoader());

		dispatcher.execute(new SysAction(PAConstant.GET_CLIENT_STATEMENT), new AsyncCallback<SysResult>() {
			public void onFailure(Throwable caught) {
				System.out.println(caught.getMessage());
			}

			public void onSuccess(SysResult result) {

				SC.clearPrompt();

				if (result != null) {

					getView().getClientStatementPane().getClientStatementListgrid()
							.addRecordsToGrid(result.getClientStatements());

				} else {
					SC.say("ERROR", "Unknow error");
				}

			}
		});

	}

}
