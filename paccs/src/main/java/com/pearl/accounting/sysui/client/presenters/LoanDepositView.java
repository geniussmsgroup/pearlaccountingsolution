package com.pearl.accounting.sysui.client.presenters;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewImpl;
import com.pearl.accounting.sysui.client.views.panes.AccountPane;
import com.pearl.accounting.sysui.client.views.panes.LoanDepositPane;
import com.smartgwt.client.widgets.layout.VLayout;

public class LoanDepositView extends ViewImpl implements LoanDepositPresenter.MyView {

	private static final String DEFAULT_MARGIN = "0px";
	private VLayout panel;
	private LoanDepositPane loanDepositPane;

	@Inject
	public LoanDepositView() {

		panel = new VLayout();

		loanDepositPane = new LoanDepositPane();

		panel.setMembers(loanDepositPane);
		panel.setWidth100();
		panel.setHeight("90%");
		Window.enableScrolling(false);
		Window.setMargin(DEFAULT_MARGIN);

	}

	public Widget asWidget() {
		return panel;
	}

	public LoanDepositPane getLoanDepositPane() {
		return loanDepositPane;
	}

}
