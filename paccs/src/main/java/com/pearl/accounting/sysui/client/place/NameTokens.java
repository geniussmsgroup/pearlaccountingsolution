package com.pearl.accounting.sysui.client.place;

public class NameTokens {
	public static final String ProductType = "ProductType";
	public static final String ProductSetup = "PRODUCTSETUPs";
	public static final String MY = "MY";
	public static final String ACCOUNTS = "ACCOUNTS";
	public static final String Interval = "Interval";
	public static final String LoanDeposit = "LoanDeposit";
	public static final String LoanRepayments = "LoanRepayments";

	public static final String DoubleEntry = "DoubleEntry";
	public static final String mainpage = "mainpage";
	public static final String notifications = "Notifications";
	public static final String users = "Users";
	public static final String geographics = "Geographics";
	public static final String multi = "multi";
	public static final String TransML = "TransML";
	public static final String DayComputeinterest = "DayComputeinterest";

	public static final String periods = "Periods";
	public static final String years = "Years";

	public static final String accounts = "Accounts";
	public static final String customers = "Account opening";
	public static final String periodicdeposits = "Periodic deposits";

	public static final String savingswithdrawals = "Savings withdrawals";

	public static final String loans = "Loans";
	public static final String loanRepayment2 = "LoanRepayments";

	public static final String generalledger = "General ledger";
	// public static final String LoanREPayment = "LoanREPayment and data";

	public static final String clientstatement = "Client statements";

	public static final String cashbookpayments = "Cashbook payments";

	public static final String cashbookreceipts = "Cashbook receipts";

	public static final String multijournal = "Multi-journal";
	public static final String TransGL = "TransGL";

	public static String getMainpage() {
		return mainpage;
	}

	public static String getNotifications() {
		return notifications;
	}

	public static String getUsers() {
		return users;
	}

	public static String getGeographics() {
		return geographics;
	}

	public static String getPeriods() {
		return periods;
	}

	public static String getYears() {
		return years;
	}

}
