package com.pearl.accounting.sysmodel;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Table(name = "init")

public class Init extends ParentEntity {
	/**
		 * 
		 */
	private static final long serialVersionUID = 1L;
	private String Lnbal;
	private String MemberId;
	private String ProductID;
	private String Acode;
	private String Aname;
	private AtypeA TtypeCd;
	private ProductID IncExp;
	private String CurrencyYcd;
	private float Xrate;
	private String SysA;
	private String SysName;
	private float SysBal;
	private ProductSetup ProductId;
	private float Pstat;
	private String Pstyle;
	private float Cbal;
	private String Ctype;
	private boolean AnalRequired;
	private boolean Pdata;
	private String DrField;
	private String CrField;
	private String Fsave;

	public String getAcode() {
		return Acode;
	}

	public void setAcode(String acode) {
		this.Acode = acode;
	}

	public String getAname() {
		return Aname;
	}

	public void setAname(String aname) {
		this.Aname = aname;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public AtypeA getTtypeCd() {
		return TtypeCd;
	}

	public void setTtypeCd(AtypeA ttypeCd) {
		this.TtypeCd = ttypeCd;
	}

	public String getLnbal() {
		return Lnbal;
	}

	public void setLnbal(String lnbal) {
		this.Lnbal = lnbal;
	}

	public String getMemberId() {
		return MemberId;
	}

	public void setMemberId(String memberId) {
		this.MemberId = memberId;
	}

	public String getProductID() {
		return ProductID;
	}

	public void setProductID(String productID) {
		this.ProductID = productID;
	}

	@Enumerated(EnumType.STRING)
	public ProductID getIncExp() {
		return IncExp;
	}

	public void setIncExp(ProductID incExp) {
		this.IncExp = incExp;
	}

	public String getCurrencyYcd() {
		return CurrencyYcd;
	}

	public void setCurrencyYcd(String currencyYcd) {
		this.CurrencyYcd = currencyYcd;
	}

	public float getXrate() {
		return Xrate;
	}

	public void setXrate(float xrate) {
		this.Xrate = xrate;
	}

	public String getSysA() {
		return SysA;
	}

	public void setSysA(String sysA) {
		this.SysA = sysA;
	}

	public String getSysName() {
		return SysName;
	}

	public void setSysName(String sysName) {
		this.SysName = sysName;
	}

	public float getSysBal() {
		return SysBal;
	}

	public void setSysBal(float sysBal) {
		this.SysBal = sysBal;
	}

	@OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public ProductSetup getProductId() {
		return ProductId;
	}

	public void setProductId(ProductSetup productId) {
		this.ProductId = productId;
	}

	public float getPstat() {
		return Pstat;
	}

	public void setPstat(float pstat) {
		this.Pstat = pstat;
	}

	public String getPstyle() {
		return Pstyle;
	}

	public void setPstyle(String pstyle) {
		this.Pstyle = pstyle;
	}

	public float getCbal() {
		return Cbal;
	}

	public void setCbal(float cbal) {
		this.Cbal = cbal;
	}

	public String getCtype() {
		return Ctype;
	}

	public void setCtype(String ctype) {
		this.Ctype = ctype;
	}

	public boolean isAnalRequired() {
		return AnalRequired;
	}

	public void setAnalRequired(boolean analRequired) {
		this.AnalRequired = analRequired;
	}

	public boolean isPdata() {
		return Pdata;
	}

	public void setPdata(boolean pdata) {
		this.Pdata = pdata;
	}

	public String getDrField() {
		return DrField;
	}

	public void setDrField(String drField) {
		this.DrField = drField;
	}

	public String getCrField() {
		return CrField;
	}

	public void setCrField(String crField) {
		this.CrField = crField;
	}

	public String getFsave() {
		return Fsave;
	}

	public void setFsave(String fsave) {
		this.Fsave = fsave;
	}

}
