package com.pearl.accounting.sysmodel;

public enum ProductType { 
	NA("NA"),
	Loan("Loan"),
	Savings("Savings"), 
	Shares("Shares"), 
	LoanInterest("Loan Interest"), 
	SavingsInterest("Savings interest"),
    LIRP("LIRP"),
	LNP("LNP");
    
	private String type;

	ProductType(String type) {
		this.type = type;
	}

	public String getProductType() {
		return type;
	}

	public void setProductType(String type) {
		this.type = type;
	}

	public static ProductType getProductType(String productType) {
		for (ProductType type : ProductType.values()) {
			if (type.getProductType().equalsIgnoreCase(productType)) {
				return type;
			}
		}
		return null;
	}

}
