package com.pearl.accounting.sysmodel;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "doubleentry")
public class DoubleEntry extends ParentEntity {
	private static final long serialVersionUID = 1L;
	private DataSourceScreen dataSourceScreen;

	private String acodeC;
	private String HDNO;
	private String acodeD;
	private String acctNo;
	private Date Tdates;
	private int year;
	private String period;

	private String VNO;

	private float Amount;
	private String Remarks;
	private Account credit;
	private Account debit;
	private Ttype ttype;
	// private float interest;
	private CustomerAccount customerAccount;
	private Loan loanaccount;
	private String loanNumber;
	private String periodcode;

	public DoubleEntry() {

	}

	public DataSourceScreen getDataSourceScreen() {
		return dataSourceScreen;
	}

	public void setDataSourceScreen(DataSourceScreen dataSourceScreen) {
		this.dataSourceScreen = dataSourceScreen;
	}

	public String getAcodeC() {
		return acodeC;
	}

	public void setAcodeC(String acodeC) {
		this.acodeC = acodeC;
	}

	public String getHDNO() {
		return HDNO;
	}

	public void setHDNO(String hDNO) {
		HDNO = hDNO;
	}

	public String getAcodeD() {
		return acodeD;
	}

	public void setAcodeD(String acodeD) {
		this.acodeD = acodeD;
	}

	public String getAcctNo() {
		return acctNo;
	}

	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}

	public Date getTdates() {
		return Tdates;
	}

	public void setTdates(Date tdates) {
		Tdates = tdates;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getVNO() {
		return VNO;
	}

	public void setVNO(String vNO) {
		VNO = vNO;
	}

	public float getAmount() {
		return Amount;
	}

	public void setAmount(float amount) {
		Amount = amount;
	}

	public String getRemarks() {
		return Remarks;
	}

	public void setRemarks(String remarks) {
		Remarks = remarks;
	}
//
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Account getCredit() {
		return credit;
	}

	public void setCredit(Account credit) {
		this.credit = credit;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Account getDebit() {
		return debit;
	}

	public void setDebit(Account debit) {
		this.debit = debit;
	}

	@Enumerated(EnumType.STRING)
	public Ttype getTtype() {
		return ttype;
	}

	public void setTtype(Ttype ttype) {
		this.ttype = ttype;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Loan getLoanaccount() {
		return loanaccount;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public CustomerAccount getCustomerAccount() {
		return customerAccount;
	}

	public void setCustomerAccount(CustomerAccount customerAccount) {
		this.customerAccount = customerAccount;
	}

	public void setLoanaccount(Loan loanaccount) {
		this.loanaccount = loanaccount;
	}

	public String getLoanNumber() {
		return loanNumber;
	}

	public void setLoanNumber(String loanNumber) {
		this.loanNumber = loanNumber;
	}

	public String getPeriodcode() {
		return periodcode;
	}

	public void setPeriodcode(String periodcode) {
		this.periodcode = periodcode;
	}

}