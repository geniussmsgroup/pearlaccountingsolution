package com.pearl.accounting.sysmodel;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "multijournals")
public class MultiJournal extends ParentEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int year;
	private String period;
	private String periodcode;

	private float amountPosted;
	private Date transactionDate; 
	
	//private Account transactionAccount;
	private String remarks;
	private String transactionDetails;

	private String transactionNumber;
	private String Thead;
	private ProductType productType;

	private List<MultiJournalDetail> multiJournalDetails;

	public MultiJournal() {

	}

	public String getThead() {
		return Thead;
	}

	public void setThead(String thead) {
		Thead = thead;
	}

	

	public String getPeriodcode() {
		return periodcode;
	}

	public void setPeriodcode(String periodcode) {
		this.periodcode = periodcode;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public float getAmountPosted() {
		return amountPosted;
	}

	public void setAmountPosted(float amountPosted) {
		this.amountPosted = amountPosted;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

//	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//	public Account getTransactionAccount() {
//		return transactionAccount;
//	}
//	 
//	public void setTransactionAccount(Account transactionAccount) {
//		this.transactionAccount = transactionAccount;
//	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getTransactionDetails() {
		return transactionDetails;
	}

	public void setTransactionDetails(String transactionDetails) {
		this.transactionDetails = transactionDetails;
	}

	public String getTransactionNumber() {
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "multiJournal", targetEntity = MultiJournalDetail.class)
	public List<MultiJournalDetail> getMultiJournalDetails() {
		return multiJournalDetails;
	}

	public void setMultiJournalDetails(List<MultiJournalDetail> multiJournalDetails) {
		this.multiJournalDetails = multiJournalDetails;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	  

	
}
