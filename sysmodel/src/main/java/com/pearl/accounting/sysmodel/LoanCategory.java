package com.pearl.accounting.sysmodel;

public enum LoanCategory {

	BSL("BSL");

	private String category;

	LoanCategory(String category) {
		this.category = category;
	}

	public String getLoanCategory() {
		return category;
	}

	public void setLoanCategory(String category) {
		this.category = category;
	}

	public static LoanCategory getLoanCategory(String category) {
		for (LoanCategory gd : LoanCategory.values()) {
			if (gd.getLoanCategory().equalsIgnoreCase(category)) {
				return gd;
			}
		}
		return null;
	}
}
