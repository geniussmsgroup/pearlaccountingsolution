package com.pearl.accounting.sysmodel;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "cashbookpayments")
public class CashBookPayment extends ParentEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String periodcode;

	private int year;
	private String  Period;
	private String payee;
	private float amountPaid;
	private Date paymentDate;
    private String acodeC;
	private Account payingAccount;
	private String remarks;
	private String voucherNumber;

	private String transactionNumber;

	private List<CashBookPaymentDetail> cashBookPaymentDetails;
	
	private String thead;

	public CashBookPayment() {

	}
	
	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getPeriod() {
		return Period;
	}



	public void setPeriod(String period) {
		Period = period;
	}



	public String getPayee() {
		return payee;
	}

	public String getPeriodcode() {
		return periodcode;
	}

	public void setPeriodcode(String periodcode) {
		this.periodcode = periodcode;
	}

	public void setPayee(String payee) {
		this.payee = payee;
	}

	public float getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(float amountPaid) {
		this.amountPaid = amountPaid;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Account getPayingAccount() {
		return payingAccount;
	}

	public void setPayingAccount(Account payingAccount) {
		this.payingAccount = payingAccount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public String getTransactionNumber() {
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}

	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,mappedBy="cashBookPayment",targetEntity=CashBookPaymentDetail.class)
	public List<CashBookPaymentDetail> getCashBookPaymentDetails() {
		return cashBookPaymentDetails;
	}

	public void setCashBookPaymentDetails(List<CashBookPaymentDetail> cashBookPaymentDetails) {
		this.cashBookPaymentDetails = cashBookPaymentDetails;
	}

	public String getThead() {
		return thead;
	}

	public void setThead(String thead) {
		this.thead = thead;
	}

	public String getAcodeCredit() {
		return acodeC;
	}

	public void setAcodeCredit(String acodeC) {
		this.acodeC = acodeC;
	}

}
