package com.pearl.accounting.sysmodel;

public enum SystemView {
	
	VIEW_SYSTEMS_ADMIN("VIEW_SYSTEMS_ADMIN"),
	VIEW_ATTENDANCE("VIEW_PROJECT_OUTPUTS"),
	VIEW_CURR_CAVERAGE("VIEW_REPORTS"),
	VIEW_SYS_DATA_SETUP("VIEW_SYS_DATA_SETUP");

	private String systemView;

	SystemView(String systemView) {
		this.systemView = systemView;
	}

	public String getSystemView() {
		return systemView;
	}

	public void setSystemView(String systemView) {
		this.systemView = systemView;
	}

	public static SystemView getSystemView(String view) {
		for (SystemView systemView : SystemView.values()) {
			if (systemView.getSystemView().equalsIgnoreCase(view)) {
				return systemView;
			}
		}
		return null;
	}

	public static String getSystemViewValue(String view) {
		int i = 0;
		for (SystemView systemView : SystemView.values()) {
			if (systemView.getSystemView().equalsIgnoreCase(view)) {
				return Integer.toString(i);
			}
			i++;
		}
		return null;
	}

}
