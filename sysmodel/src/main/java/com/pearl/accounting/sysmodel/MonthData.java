package com.pearl.accounting.sysmodel;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
@Entity
@Table(name ="MonthData")
public class MonthData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int YRCODE;
	private String FINTCODE;
	private String FINTNAME	;
	private Date FSTART;
	private Date FEND;
	private Date DaysInMonth;
	private String sysid;


	public int getYRCODE() {
		return YRCODE;
	}

	public void setYRCODE(int yRCODE) {
		YRCODE = yRCODE;
	}
	@Id
	public String getSysid() {
		return sysid;
	}

	public void setSysid(String sysid) {
		this.sysid = sysid;
	}

	public String getFINTCODE() {
		return FINTCODE;
	}

	public void setFINTCODE(String fINTCODE) {
		FINTCODE = fINTCODE;
	}

	public String getFINTNAME() {
		return FINTNAME;
	}

	public void setFINTNAME(String fINTNAME) {
		FINTNAME = fINTNAME;
	}

	public Date getFSTART() {
		return FSTART;
	}

	public void setFSTART(Date fSTART) {
		FSTART = fSTART;
	}

	public Date getFEND() {
		return FEND;
	}

	public void setFEND(Date fEND) {
		FEND = fEND;
	}

	public Date getDaysInMonth() {
		return DaysInMonth;
	}

	public void setDaysInMonth(Date daysInMonth) {
		DaysInMonth = daysInMonth;
	}
	
}
