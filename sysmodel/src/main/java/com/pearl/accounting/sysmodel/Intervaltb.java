package com.pearl.accounting.sysmodel;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name ="intervaltb")
public class Intervaltb  implements Serializable{
	private static final long serialVersionUID = 1L;
	private String FINTCODE;
	private String FINTNAME	;
	private String FSTATUS;
	private String FTRANS;
	private String FYrClose	;
	private String FyrReOpen;
	private int YRCODE;
	private int Xrate;
	private int Bxrate;
	private int Exrate;
	private Date FSTART;
	private Date FEND;
	private double FCLOSE;
	private String FP;
	private String  SysId;
	private Date dateCreated;
	private Date dateUpdated;
	private Status status;
	private SystemUser createdBy;
	private SystemUser updatedBy;

	
public Intervaltb() {
	
}
public Date getDateCreated() {
	return dateCreated;
}

public void setDateCreated(Date dateCreated) {
	this.dateCreated = dateCreated;
}

public Date getDateUpdated() {
	return dateUpdated;
}

public void setDateUpdated(Date dateUpdated) {
	this.dateUpdated = dateUpdated;
}

@Enumerated(EnumType.STRING)
public Status getStatus() {
	return status;
}

public void setStatus(Status status) {
	this.status = status;
}

@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
public SystemUser getCreatedBy() {
	return createdBy;
}

public void setCreatedBy(SystemUser createdBy) {
	this.createdBy = createdBy;
}

@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
public SystemUser getUpdatedBy() {
	return updatedBy;
}

public void setUpdatedBy(SystemUser updatedBy) {
	this.updatedBy = updatedBy;
}

public String getFINTCODE() {
	return FINTCODE;
}
public void setFINTCODE(String fINTCODE) {
	this.FINTCODE = fINTCODE;
}
public String getFINTNAME() {
	return FINTNAME;
}
public void setFINTNAME(String fINTNAME) {
	this.FINTNAME = fINTNAME;
}
public String getFSTATUS() {
	return FSTATUS;
}
public void setFSTATUS(String fSTATUS) {
	this.FSTATUS = fSTATUS;
}
public String getFTRANS() {
	return FTRANS;
}
public void setFTRANS(String fTRANS) {
	this.FTRANS = fTRANS;
}
public String getFYrClose() {
	return FYrClose;
}
public void setFYrClose(String fYrClose) {
	this.FYrClose = fYrClose;
}
public String getFyrReOpen() {
	return FyrReOpen;
}
public void setFyrReOpen(String fyrReOpen) {
	this.FyrReOpen = fyrReOpen;
}
public int getYRCODE() {
	return YRCODE;
}
public void setYRCODE(int yRCODE) {
	this.YRCODE = yRCODE;
}
public int getXrate() {
	return Xrate;
}
public void setXrate(int xrate) {
	this.Xrate = xrate;
}
public int getBxrate() {
	return Bxrate;
}
public void setBxrate(int bxrate) {
	this.Bxrate = bxrate;
}
public int getExrate() {
	return Exrate;
}
public void setExrate(int exrate) {
	this.Exrate = exrate;
}
public Date getFSTART() {
	return FSTART;
}
public void setFSTART(Date fSTART) {
	this.FSTART = fSTART;
}
public Date getFEND() {
	return FEND;
}
public void setFEND(Date fEND) {
	this.FEND = fEND;
}
public double getFCLOSE() {
	return FCLOSE;
}
public void setFCLOSE(double fCLOSE) {
	this.FCLOSE = fCLOSE;
}
public String getFP() {
	return FP;
}
public void setFP(String fP) {
	this.FP = fP;
}
@Id
public String getSysId() {
	return SysId;
}
public void setSysId(String sysId) {
	this.SysId = sysId;
}




}
