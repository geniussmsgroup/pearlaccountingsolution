package com.pearl.accounting.sysmodel;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "multidetails")
public class MultiDetail extends ParentEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Multi multi;	
	private Account Account;
	private CustomerAccount customerAccount;
	private Loan loan;
	private float amountPaid;
	private String Cust;
	private Date PaymentDate;
	private String remarks;
	private Ttype ttype;
    private String paydetails;	

	public MultiDetail() {

	}
	public String getCust() {
		return Cust;
	}
	public void setCust(String cust) {
		Cust = cust;
	}
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Multi getMulti() {
		return multi;
	}

	public void setMulti(Multi multi) {
		this.multi = multi;
	}

	public Date getPaymentDate() {
		return PaymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		PaymentDate = paymentDate;
	}

	public String getPaydetails() {
		return paydetails;
	}

	public void setPaydetails(String paydetails) {
		this.paydetails = paydetails;
	}

	

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Account getAccount() {
		return Account;
	}

	public void setAccount(Account Account) {
		this.Account = Account;
	}



	public float getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(float amountPaid) {
		this.amountPaid = amountPaid;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	
	@Enumerated(EnumType.STRING)
	public Ttype getTtype() {
		return ttype;
	}
	public void setTtype(Ttype ttype) {
		this.ttype = ttype;
	}
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public CustomerAccount getCustomerAccount() {
		return customerAccount;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Loan getLoan() {
		return loan;
	}
//
//	
	public void setCustomerAccount(CustomerAccount customerAccount) {
		this.customerAccount = customerAccount;
	}
//	
	public void setLoan(Loan loan) {
		this.loan = loan;
	}
	

}
