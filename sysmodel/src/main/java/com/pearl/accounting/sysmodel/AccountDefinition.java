package com.pearl.accounting.sysmodel;

public enum AccountDefinition {

	I("I"), E("E"),NONE("None");

	private String definition;

	AccountDefinition(String definition) {
		this.definition = definition;
	}

	public String getDefinition() {
		return definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public static AccountDefinition getDefinition(String definition) {
		for (AccountDefinition gd : AccountDefinition.values()) {
			if (gd.getDefinition().equalsIgnoreCase(definition)) {
				return gd;
			}
		}
		return null;
	}

}
