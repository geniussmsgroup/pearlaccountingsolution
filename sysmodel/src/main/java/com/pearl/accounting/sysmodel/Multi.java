package com.pearl.accounting.sysmodel;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "multis")
public class Multi extends ParentEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private int year;
private String period;

	private float amountRecieved;
	private Date PaymentDate; 
	private String receiptNumber;
	private Account transactionAccount;
	private String remarks;
	private String transactionDetails;

	private String transactionNumber;
	private String Thead;
	private ProductType productType;

	private List<MultiDetail> multiDetails;

	public Multi() {

	}
	

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public Date getPaymentDate() {
		return PaymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		PaymentDate = paymentDate;
	}

	public String getThead() {
		return Thead;
	}

	public void setThead(String thead) {
		Thead = thead;
	}

	



	public int getYear() {
		return year;
	}


	public void setYear(int year) {
		this.year = year;
	}


	public String getPeriod() {
		return period;
	}


	public void setPeriod(String period) {
		this.period = period;
	}


	public float getAmountRecieved() {
		return amountRecieved;
	}

	public void setAmountRecieved(float amountRecieved) {
		this.amountRecieved = amountRecieved;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	public Account getTransactionAccount() {
		return transactionAccount;
	}
	 
	public void setTransactionAccount(Account transactionAccount) {
		this.transactionAccount = transactionAccount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getTransactionDetails() {
		return transactionDetails;
	}

	public void setTransactionDetails(String transactionDetails) {
		this.transactionDetails = transactionDetails;
	}

	public String getTransactionNumber() {
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}




	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,mappedBy="multi",targetEntity=MultiDetail.class)
	public List<MultiDetail> getMultiDetails() {
		return multiDetails;
	}

	public void setMultiDetails(List<MultiDetail> multiDetails) {
		this.multiDetails = multiDetails;
	}

	
}
