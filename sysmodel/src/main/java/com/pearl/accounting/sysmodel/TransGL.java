package com.pearl.accounting.sysmodel;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name ="transgl")

public class TransGL extends ParentEntity {
	private static final long serialVersionUID = 1L;

private String ACODE;
//private int PCODE;
private float AMOUNT;
private float AmtUsd;
private float RecAmt;
private Date TDATE;
private Ttype TTYPE;
private String PAYDET;
private String REMARKS;
private String PAYPERSON;

private String ICODE;

private String  EPOS;
private String MemberID;
private  int YRCODE;

private Fsave fsave;
private Date RDATE;
private String TheadID;
//private int RECYR;
private String RTVRNO;
//private double JNLNO;
private String StaffName;
private String ANALCD;
private String MnRec;
private int Trec;
private int  YrRec;

private String 	CheckNo;
private String TrecN;
//private String RECMON;
private Account account;
private String LoanID;
private Loan loan;
private CustomerAccount customeraccount;
private DataSourceScreen dataSourceScreen;
//private Status status;	


public  TransGL() {
	
}
//@Enumerated(EnumType.STRING)
//public Status getStatus() {
//	return status;
//}
//
//public void setStatus(Status status) {
//	this.status = status;
//}

@Enumerated(EnumType.STRING)
public DataSourceScreen getDataSourceScreen() {
	return dataSourceScreen;
}

//public int getPCODE() {
//	return PCODE;
//}
//
//public void setPCODE(int pCODE) {
//	PCODE = pCODE;
//}

public void setDataSourceScreen(DataSourceScreen dataSourceScreen) {
	this.dataSourceScreen = dataSourceScreen;
}

public String getACODE() {
	return ACODE;
}

public void setACODE(String aCODE) {
	ACODE = aCODE;
}
public float getAMOUNT() {
	return AMOUNT;
}

public void setAMOUNT(float aMOUNT) {
	AMOUNT = aMOUNT;
}

public float getAmtUsd() {
	return AmtUsd;
}

public void setAmtUsd(float amtUsd) {
	AmtUsd = amtUsd;
}

public float getRecAmt() {
	return RecAmt;
}

public void setRecAmt(float recAmt) {
	RecAmt = recAmt;
}

public Date getTDATE() {
	return TDATE;
}

public void setTDATE(Date tDATE) {
	TDATE = tDATE;
}
@Enumerated(EnumType.STRING)
public Ttype getTTYPE() {
	return TTYPE;
}

public void setTTYPE(Ttype tTYPE) {
	TTYPE = tTYPE;
}

public String getPAYDET() {
	return PAYDET;
}

public void setPAYDET(String pAYDET) {
	PAYDET = pAYDET;
}

public String getREMARKS() {
	return REMARKS;
}

public void setREMARKS(String rEMARKS) {
	REMARKS = rEMARKS;
}


public String getPAYPERSON() {
	return PAYPERSON;
}

public void setPAYPERSON(String pAYPERSON) {
	PAYPERSON = pAYPERSON;
}

public String getICODE() {
	return ICODE;
}

public void setICODE(String iCODE) {
	ICODE = iCODE;
}

@Id
public String getEPOS() {
	return EPOS;
}

public void setEPOS(String ePOS) {
	EPOS = ePOS;
}

public String getMemberID() {
	return MemberID;
}

public void setMemberID(String memberID) {
	MemberID = memberID;
}

public int getYRCODE() {
	return YRCODE;
}

public void setYRCODE(int yRCODE) {
	YRCODE = yRCODE;
}

@Enumerated(EnumType.STRING)
public Fsave getFsave() {
	return fsave;
}

public void setFsave(Fsave fsave) {
	this.fsave = fsave;
}

public Date getRDATE() {
	return RDATE;
}

public void setRDATE(Date rDATE) {
	RDATE = rDATE;
}

public String getTheadID() {
	return TheadID;
}

public void setTheadID(String theadID) {
	TheadID = theadID;
}


public String getRTVRNO() {
	return RTVRNO;
}

public void setRTVRNO(String rTVRNO) {
	RTVRNO = rTVRNO;
}

//public double getJNLNO() {
//	return JNLNO;
//}
//
//public void setJNLNO(double jNLNO) {
//	JNLNO = jNLNO;
//}

public String getStaffName() {
	return StaffName;
}

public void setStaffName(String staffName) {
	StaffName = staffName;
}

public String getANALCD() {
	return ANALCD;
}

public void setANALCD(String aNALCD) {
	ANALCD = aNALCD;
}

public String getMnRec() {
	return MnRec;
}

public void setMnRec(String mnRec) {
	MnRec = mnRec;
}



public int getTrec() {
	return Trec;
}

public void setTrec(int trec) {
	Trec = trec;
}

public int getYrRec() {
	return YrRec;
}

public void setYrRec(int yrRec) {
	YrRec = yrRec;
}

public String getCheckNo() {
	return CheckNo;
}

public void setCheckNo(String checkNo) {
	CheckNo = checkNo;
}

public String getTrecN() {
	return TrecN;
}

public void setTrecN(String trecN) {
	TrecN = trecN;
}

@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
public Account getAccount() {
	return account;
}

public void setAccount(Account account) {
	this.account = account;
}

public String getLoanID() {
	return LoanID;
}

public void setLoanID(String loanID) {
	LoanID = loanID;
}
@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
public Loan getLoan() {
	return loan;
}

public void setLoan(Loan loan) {
	this.loan = loan;
}
@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
public CustomerAccount getCustomeraccount() {
	return customeraccount;
}

public void setCustomeraccount(CustomerAccount customeraccount) {
	this.customeraccount = customeraccount;
}










	
	
		
}