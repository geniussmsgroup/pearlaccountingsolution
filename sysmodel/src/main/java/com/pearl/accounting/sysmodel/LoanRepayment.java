package com.pearl.accounting.sysmodel;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "loanrepayments")
public class LoanRepayment extends ParentEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private int year;
private String period;
	private float totalAmount;
	private CustomerAccount customerAccount;
    private float interestPay;
	private float balance;
	private Loan loan;
	private float depositedAmount;
	private Date depositDate;
	private Account receiveingAccount;
	private String paymentDetails;
	private String remarks;
	private float exchangeRate;
    private String Thead;
	private Account payingAccount;
   
	public LoanRepayment() {

	}
	
	public String getThead() {
		return Thead;
	}

	public void setThead(String thead) {
		this.Thead = thead;
	}

	public float getInterestPay() {
		return interestPay;
	}

	public void setInterestPay(float interestPay) {
		this.interestPay = interestPay;
	}

	@ManyToOne(cascade=CascadeType.REFRESH,fetch=FetchType.EAGER)
	public CustomerAccount getCustomerAccount() {
		return customerAccount;
	}

	public void setCustomerAccount(CustomerAccount customerAccount) {
		this.customerAccount = customerAccount;
	}
	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	public Loan getLoan() {
		return loan;
	}

	public void setLoan(Loan loan) {
		this.loan = loan;
	}

	public float getDepositedAmount() {
		return depositedAmount;
	}

	public void setDepositedAmount(float depositedAmount) {
		this.depositedAmount = depositedAmount;
	}

	public Date getDepositDate() {
		return depositDate;
	}

	public void setDepositDate(Date depositDate) {
		this.depositDate = depositDate;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Account getReceiveingAccount() {
		return receiveingAccount;
	}

	public void setReceiveingAccount(Account receiveingAccount) {
		this.receiveingAccount = receiveingAccount;
	}

	public String getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public float getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(float exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Account getPayingAccount() {
		return payingAccount;
	}

	public void setPayingAccount(Account payingAccount) {
		this.payingAccount = payingAccount;
	}

	public float getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(float totalAmount) {
		this.totalAmount = totalAmount;
	}

	public float getBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}

}
