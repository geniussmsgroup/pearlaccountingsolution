package com.pearl.accounting.sysmodel;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "repaymentschedules")
public class RepaymentSchedule implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String periodcode;

	private float principleAmount;
	private float interestAmount;
	private float totalAmount;
	private Date paymentDate;
	private int paymentLotNumber;
	private Loan loan;
	private Date dateCreated;
	private Date dateUpdated;
	private Status status;
	private SystemUser createdBy;
	private SystemUser updatedBy;


	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public SystemUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(SystemUser createdBy) {
		this.createdBy = createdBy;
	}

	public SystemUser getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(SystemUser updatedBy) {
		this.updatedBy = updatedBy;
	}

	public RepaymentSchedule() {

	}

	public float getPrincipleAmount() {
		return principleAmount;
	}

	public void setPrincipleAmount(float principleAmount) {
		this.principleAmount = principleAmount;
	}

	public float getInterestAmount() {
		return interestAmount;
	}

	public void setInterestAmount(float interestAmount) {
		this.interestAmount = interestAmount;
	}

	public float getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(float totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getPeriodcode() {
		return periodcode;
	}

	public void setPeriodcode(String periodcode) {
		this.periodcode = periodcode;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public int getPaymentLotNumber() {
		return paymentLotNumber;
	}

	public void setPaymentLotNumber(int paymentLotNumber) {
		this.paymentLotNumber = paymentLotNumber;
	}
    @Id
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Loan getLoan() {
		return loan;
	}

	public void setLoan(Loan loan) {
		this.loan = loan;
	}

}
