package com.pearl.accounting.sysmodel;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "userpermissions")
public class UserPermission extends ParentEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UserRole userRole;
	private SystemView systemView;

	public UserPermission() {

	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	public SystemView getSystemView() {
		return systemView;
	}

	public void setSystemView(SystemView systemView) {
		this.systemView = systemView;
	}

}
