package com.pearl.accounting.sysmodel;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "fixeddata")
public class FixedData implements Serializable {

	private static final long serialVersionUID = 1L;
	private String IDNo;
	private double ShareIR;
	private double SaveIR;
	private double LoanIR;
	private double SaveMinEarn;
	private double AdminFee;
	private Date BFToDate;
	private int ChristmasIR;
	private int PensionIR;
	private int SchoolFSIR;
	private int SchoolFMIR;
	private int SchoolFLIR;
	private int ShareVOriginal;
	private int ShareVCurrent;
	private double ShareTransferP;

	private int TransYear;

	private int CurrentYear;
	private int LastPeriod;
	private double LoanDisbursefee;
	private int SaveWithDrawFee;
	private double LnDPercent;
	private double LnSPercent;
	private int LnApplyFee;
	private int CurrentNo;
	private Date NextIntDte;
    private int theadid;
    private float receiptno;
    
	public FixedData() {

	}

	@Id
	public String getIDNo() {
		return IDNo;
	}

	public void setIDNo(String iDNo) {
		this.IDNo = iDNo;
	}

	public double getShareIR() {
		return ShareIR;
	}

	public void setShareIR(double shareIR) {
		this.ShareIR = shareIR;
	}

	public double getSaveIR() {
		return SaveIR;
	}

	public void setSaveIR(double saveIR) {
		this.SaveIR = saveIR;
	}

	public double getLoanIR() {
		return LoanIR;
	}

	public void setLoanIR(double loanIR) {
		this.LoanIR = loanIR;
	}

	public double getSaveMinEarn() {
		return SaveMinEarn;
	}

	public void setSaveMinEarn(double saveMinEarn) {
		this.SaveMinEarn = saveMinEarn;
	}

	public double getAdminFee() {
		return AdminFee;
	}

	public void setAdminFee(double adminFee) {
		this.AdminFee = adminFee;
	}

	public Date getBFToDate() {
		return BFToDate;
	}

	public void setBFToDate(Date bFToDate) {
		this.BFToDate = bFToDate;
	}

	public int getChristmasIR() {
		return ChristmasIR;
	}

	public void setChristmasIR(int christmasIR) {
		this.ChristmasIR = christmasIR;
	}

	public int getPensionIR() {
		return PensionIR;
	}

	public void setPensionIR(int pensionIR) {
		this.PensionIR = pensionIR;
	}

	public int getSchoolFSIR() {
		return SchoolFSIR;
	}

	public void setSchoolFSIR(int schoolFSIR) {
		this.SchoolFSIR = schoolFSIR;
	}

	public int getSchoolFMIR() {
		return SchoolFMIR;
	}

	public void setSchoolFMIR(int schoolFMIR) {
		this.SchoolFMIR = schoolFMIR;
	}

	public int getSchoolFLIR() {
		return SchoolFLIR;
	}

	public void setSchoolFLIR(int schoolFLIR) {
		this.SchoolFLIR = schoolFLIR;
	}

	public int getShareVOriginal() {
		return ShareVOriginal;
	}

	public void setShareVOriginal(int shareVOriginal) {
		this.ShareVOriginal = shareVOriginal;
	}

	public int getShareVCurrent() {
		return ShareVCurrent;
	}

	public void setShareVCurrent(int shareVCurrent) {
		this.ShareVCurrent = shareVCurrent;
	}

	public double getShareTransferP() {
		return ShareTransferP;
	}

	public void setShareTransferP(double shareTransferP) {
		this.ShareTransferP = shareTransferP;
	}

	public int getTransYear() {
		return TransYear;
	}

	public void setTransYear(int transYear) {
		this.TransYear = transYear;
	}

	public int getCurrentYear() {
		return CurrentYear;
	}

	public void setCurrentYear(int currentYear) {
		this.CurrentYear = currentYear;
	}

	public int getLastPeriod() {
		return LastPeriod;
	}

	public void setLastPeriod(int lastPeriod) {
		this.LastPeriod = lastPeriod;
	}

	public double getLoanDisbursefee() {
		return LoanDisbursefee;
	}

	public void setLoanDisbursefee(double loanDisbursefee) {
		this.LoanDisbursefee = loanDisbursefee;
	}

	public int getSaveWithDrawFee() {
		return SaveWithDrawFee;
	}

	public void setSaveWithDrawFee(int saveWithDrawFee) {
		this.SaveWithDrawFee = saveWithDrawFee;
	}

	public double getLnDPercent() {
		return LnDPercent;
	}

	public void setLnDPercent(double lnDPercent) {
		this.LnDPercent = lnDPercent;
	}

	public double getLnSPercent() {
		return LnSPercent;
	}

	public void setLnSPercent(double lnSPercent) {
		this.LnSPercent = lnSPercent;
	}

	public int getLnApplyFee() {
		return LnApplyFee;
	}

	public void setLnApplyFee(int lnApplyFee) {
		this.LnApplyFee = lnApplyFee;
	}

	public int getCurrentNo() {
		return CurrentNo;
	}

	public void setCurrentNo(int currentNo) {
		this.CurrentNo = currentNo;
	}

	public Date getNextIntDte() {
		return NextIntDte;
	}

	public void setNextIntDte(Date nextIntDte) {
		this.NextIntDte = nextIntDte;
	}

	public int getTheadid() {
		return theadid;
	}

	public void setTheadid(int theadid) {
		this.theadid = theadid;
	}

	public float getReceiptno() {
		return receiptno;
	}

	public void setReceiptno(float receiptno) {
		this.receiptno = receiptno;
	}



}
