package com.pearl.accounting.sysmodel;

import java.io.Serializable;

public class SwizFeedback implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean response;
	private String message;

	public SwizFeedback() {

	}

	public SwizFeedback(boolean response, String message ) {
		this.response = response;
		this.message = message;

	}

	public boolean isResponse() {
		return response;
	}

	public void setResponse(boolean response) {
		this.response = response;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
