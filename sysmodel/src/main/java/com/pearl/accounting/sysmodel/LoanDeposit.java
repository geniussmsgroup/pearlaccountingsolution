package com.pearl.accounting.sysmodel;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "loandeposit")
public class LoanDeposit extends ParentEntity {

	private static final long serialVersionUID = 1L;

	private float loandeposit;
	private float interestdeposit;
	private String remarks;
	private Date depositdate;
	private String paydetails;
	private Account account;
	private Loan loan;
	private CustomerAccount customeraccount;
	private String Thead;
	private String period;
	private String periodcode;
	private String year;
///	private float loanbalance;

public LoanDeposit() {
	
}

public float getLoandeposit() {
	return loandeposit;
}

public void setLoandeposit(float loandeposit) {
	this.loandeposit = loandeposit;
}

public float getInterestdeposit() {
	return interestdeposit;
}

public void setInterestdeposit(float interestdeposit) {
	this.interestdeposit = interestdeposit;
}

public String getRemarks() {
	return remarks;
}

public void setRemarks(String remarks) {
	this.remarks = remarks;
}

public Date getDepositdate() {
	return depositdate;
}

public void setDepositdate(Date depositdate) {
	this.depositdate = depositdate;
}

public String getPaydetails() {
	return paydetails;
}

public void setPaydetails(String paydetails) {
	this.paydetails = paydetails;
}
@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
public Account getAccount() {
	return account;
}

public void setAccount(Account account) {
	this.account = account;
}
@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
public Loan getLoan() {
	return loan;
}

public void setLoan(Loan loan) {
	this.loan = loan;
}
@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
public CustomerAccount getCustomeraccount() {
	return customeraccount;
}

public void setCustomeraccount(CustomerAccount customeraccount) {
	this.customeraccount = customeraccount;
}

public String getThead() {
	return Thead;
}

public void setThead(String thead) {
	Thead = thead;
}

public String getPeriod() {
	return period;
}

public void setPeriod(String period) {
	this.period = period;
}

public String getPeriodcode() {
	return periodcode;
}

public void setPeriodcode(String periodcode) {
	this.periodcode = periodcode;
}

public String getYear() {
	return year;
}

public void setYear(String year) {
	this.year = year;
}

//public float getLoanbalance() {
//	return loanbalance;
//}
//
//public void setLoanbalance(float loanbalance) {
//	this.loanbalance = loanbalance;
//}



}
