package com.pearl.accounting.sysmodel;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cashbookreceiptdetails")
public class CashBookReceiptDetail extends ParentEntity {

	/**cashBookPayment
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private CashBookReceipt cashBookReceipt;
private int year;
private String periodcode;

private String period ;
	private Account payingAccount;
	private float amountPaid;
	private Date paymentDate;
	private String remarks;
    private String acodeC;
    
	public CashBookReceiptDetail() {

	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getAcodeC() {
		return acodeC;
	}

	public void setAcodeC(String acodeC) {
		this.acodeC = acodeC;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public CashBookReceipt getCashBookReceipt() {
		return cashBookReceipt;
	}

	public void setCashBookReceipt(CashBookReceipt cashBookReceipt) {
		this.cashBookReceipt = cashBookReceipt;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Account getPayingAccount() {
		return payingAccount;
	}

	public void setPayingAccount(Account payingAccount) {
		this.payingAccount = payingAccount;
	}

	public float getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(float amountPaid) {
		this.amountPaid = amountPaid;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getPeriodcode() {
		return periodcode;
	}

	public void setPeriodcode(String periodcode) {
		this.periodcode = periodcode;
	}

}
