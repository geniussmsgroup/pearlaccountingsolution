package com.pearl.accounting.sysmodel;

public enum AccountType {
	AssetCurrent("ACA,    ASSET CURRENT	(CASH/BANK)"), 
	AssetsCurrent("ACU,    ASSETS/CURRENT(OTHER)"),
	AssetsFixed("AFI,   ASSETS FIXED"),
	CapitWork("CPT,   CAPITAL/WORKING FUND"),
	LiabilityCurrent("LIC,   LIABILITY(CURRENT)"),
	LiabilityOther("LIO,   LIABILITY(OTHER)"),
	Norminal("NML,   NORMINAL");
	private String type;

	AccountType(String type) {
		this.type = type;
	}

	public String getAccountType() {
		return type;
	}

	public void setAccountType(String type) {
		this.type = type;
	}

	public static AccountType getAccountType(String type) {
		for (AccountType gd : AccountType.values()) {
			if (gd.getAccountType().equalsIgnoreCase(type)) {
				return gd;
			}
		}
		return null;
	}

}
