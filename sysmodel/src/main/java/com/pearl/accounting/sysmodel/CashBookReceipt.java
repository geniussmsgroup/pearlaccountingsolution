package com.pearl.accounting.sysmodel;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "cashbookreceipts")
public class CashBookReceipt extends ParentEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int year;
	private String period;
	private String periodcode;

	private String paymentfrom;
	private float amountReceived;
	private Date paymentDate;

	private Account receivingAccount;
	private String remarks;
	private String receiptNumber;
    private String acodeD;
	private String transactionNumber;
	private String Thead;
	private List<CashBookReceiptDetail> cashBookReceiptDetails;

	public CashBookReceipt() {

	}

	

	public int getYear() {
		return year;
	}



	public void setYear(int year) {
		this.year = year;
	}



	public String getPeriod() {
		return period;
	}



	public void setPeriod(String period) {
		this.period = period;
	}



	public String getAcodeD() {
		return acodeD;
	}



	public void setAcodeD(String acodeD) {
		this.acodeD = acodeD;
	}



	public String getThead() {
		return Thead;
	}



	public void setThead(String thead) {
		this.Thead = thead;
	}



	
	public String getPeriodcode() {
		return periodcode;
	}



	public void setPeriodcode(String periodcode) {
		this.periodcode = periodcode;
	}



	public String getPaymentfrom() {
		return paymentfrom;
	}

	public void setPaymentfrom(String paymentfrom) {
		this.paymentfrom = paymentfrom;
	}

	public float getAmountReceived() {
		return amountReceived;
	}

	public void setAmountReceived(float amountReceived) {
		this.amountReceived = amountReceived;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Account getReceivingAccount() {
		return receivingAccount;
	}

	public void setReceivingAccount(Account receivingAccount) {
		this.receivingAccount = receivingAccount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public String getTransactionNumber() {
		return transactionNumber;
	}

	public void setTransactionNumber(String transactionNumber) {
		this.transactionNumber = transactionNumber;
	}

	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.LAZY,mappedBy="cashBookReceipt",targetEntity=CashBookReceiptDetail.class)
	public List<CashBookReceiptDetail> getCashBookReceiptDetails() {
		return cashBookReceiptDetails;
	}

	public void setCashBookReceiptDetails(List<CashBookReceiptDetail> cashBookReceiptDetails) {
		this.cashBookReceiptDetails = cashBookReceiptDetails;
	}



	

}
