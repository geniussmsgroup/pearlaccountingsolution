package com.pearl.accounting.sysmodel;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "generalledger")
public class GeneralLedger extends ParentEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    private int year;	

    private String period;	
    private String periodcode;

	private Date transationDate;
	private String transactionDescription;
	private String postReferenceNo;
	private Account account;
	private Ttype ttype;
	private float transactionDebt;
	private float transactionCredit;
	private float balanceDebt;
	private float balanceCredit;
	private String paymentDetails;
	private String remarks;
	private DataSourceScreen dataSourceScreen;
	private Loan loanid;
	private String loanNumber;
	private String thead;
	private String acctNo;
	private String acodeD;
	private String acodeC;
//private int theads;
	public GeneralLedger() {

	}
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Loan getLoanid() {
		return loanid;
	}

	public void setLoanid(Loan loanid) {
		this.loanid = loanid;
	}

	public String getLoanNumber() {
		return loanNumber;
	}

	public void setLoanNumber(String loanNumber) {
		this.loanNumber = loanNumber;
	}
	
	public String getPeriodcode() {
		return periodcode;
	}
	public void setPeriodcode(String periodcode) {
		this.periodcode = periodcode;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public Date getTransationDate() {
		return transationDate;
	}

	public void setTransationDate(Date transationDate) {
		this.transationDate = transationDate;
	}

	public String getTransactionDescription() {
		return transactionDescription;
	}

	public void setTransactionDescription(String transactionDescription) {
		this.transactionDescription = transactionDescription;
	}

	public String getPostReferenceNo() {
		return postReferenceNo;
	}

	public void setPostReferenceNo(String postReferenceNo) {
		this.postReferenceNo = postReferenceNo;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	

	public float getTransactionDebt() {
		return transactionDebt;
	}
	@Enumerated(EnumType.STRING)
	public Ttype getTtype() {
		return ttype;
	}
	public void setTtype(Ttype ttype) {
		this.ttype = ttype;
	}
	public void setTransactionDebt(float transactionDebt) {
		this.transactionDebt = transactionDebt;
	}

	public float getTransactionCredit() {
		return transactionCredit;
	}

	public void setTransactionCredit(float transactionCredit) {
		this.transactionCredit = transactionCredit;
	}

	public float getBalanceDebt() {
		return balanceDebt;
	}

	public void setBalanceDebt(float balanceDebt) {
		this.balanceDebt = balanceDebt;
	}

	public float getBalanceCredit() {
		return balanceCredit;
	}

	public void setBalanceCredit(float balanceCredit) {
		this.balanceCredit = balanceCredit;
	}

	public String getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public DataSourceScreen getDataSourceScreen() {
		return dataSourceScreen;
	}

	public void setDataSourceScreen(DataSourceScreen dataSourceScreen) {
		this.dataSourceScreen = dataSourceScreen;
	}

	public String getThead() {
		return thead;
	}

	public void setThead(String thead) {
		this.thead = thead;
	}
	public String getAcctNo() {
		return acctNo;
	}
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	public String getAcodeD() {
		return acodeD;
	}
	public void setAcodeD(String acodeD) {
		this.acodeD = acodeD;
	}
	public String getAcodeC() {
		return acodeC;
	}
	public void setAcodeC(String acodeC) {
		this.acodeC = acodeC;
	}

	
}
