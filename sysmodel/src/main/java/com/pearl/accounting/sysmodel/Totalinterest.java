package com.pearl.accounting.sysmodel;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "totalint")
public class Totalinterest implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private String Sysid;
//private String Amount;
	private Date Tdate;
	private float Amount;
	private DataSourceScreen datasourcecreen;
	private Date dateCreated;
	private Date dateUpdated;
	private Status status;
	private SystemUser createdBy;
	private SystemUser updatedBy;
	private String Thead;
	private String periodcode;
	private int yearcode;
	private String fsave;
 //   private  List<Float> amounts; 
	public Totalinterest() {

	}

//	public List<Float> getAmounts() {
//		return amounts;
//	}
//
//	public void setAmounts(List<Float> amounts) {
//		this.amounts = amounts;
//	}

	public String getPeriodcode() {
		return periodcode;
	}

	public String getFsave() {
		return fsave;
	}

	public void setFsave(String fsave) {
		this.fsave = fsave;
	}

	public void setPeriodcode(String periodcode) {
		this.periodcode = periodcode;
	}

	public int getYearcode() {
		return yearcode;
	}

	public void setYearcode(int yearcode) {
		this.yearcode = yearcode;
	}

	@Id
	public String getSysid() {
		return Sysid;
	}

	public void setSysid(String sysid) {
		Sysid = sysid;
	}

	public String getThead() {
		return Thead;
	}

	public void setThead(String thead) {
		Thead = thead;
	}

	public Date getTdate() {
		return Tdate;
	}

	public void setTdate(Date tdate) {
		Tdate = tdate;
	}

	public float getAmount() {
		return Amount;
	}

	public void setAmount(float amount) {
		Amount = amount;
	}

	public DataSourceScreen getDatasourcecreen() {
		return datasourcecreen;
	}

	public void setDatasourcecreen(DataSourceScreen datasourcecreen) {
		this.datasourcecreen = datasourcecreen;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	@Enumerated(EnumType.STRING)
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public SystemUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(SystemUser createdBy) {
		this.createdBy = createdBy;
	}
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public SystemUser getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(SystemUser updatedBy) {
		this.updatedBy = updatedBy;
	}

}
