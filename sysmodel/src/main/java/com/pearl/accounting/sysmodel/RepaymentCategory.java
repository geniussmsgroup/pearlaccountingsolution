package com.pearl.accounting.sysmodel;

public enum RepaymentCategory {

	Mon("Mon"), WK("WK"), BY_WEEKLY("BW"),QtZ("Qtz");

	private String repaymentCategory;

	RepaymentCategory(String repaymentCategory) {
		this.repaymentCategory = repaymentCategory;
	}

	public String getRepaymentCategory() {
		return repaymentCategory;
	}

	public void setRepaymentCategory(String repaymentCategory) {
		this.repaymentCategory = repaymentCategory;
	}

	public static RepaymentCategory getLoanCategory(String repaymentCategory) {
		for (RepaymentCategory type : RepaymentCategory.values()) {
			if (type.getRepaymentCategory().equalsIgnoreCase(repaymentCategory)) {
				return type;
			}
		}
		return null;
	}
}
