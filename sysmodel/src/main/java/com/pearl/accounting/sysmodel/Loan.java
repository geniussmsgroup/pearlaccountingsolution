package com.pearl.accounting.sysmodel;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "loaninfo")
public class Loan implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String periodcode;
	private int YrCode;
	private CustomerAccount customerAccount;
	private Date loanDisbursementDate;
	private Date TDATE2;
	private Date LastIntDate;
	private Date PayDate;
	private int repaymentPeriod;
	private LoanCategory loanCategory;
	private float LoanAmount;
	private float InterestRate;
	private String paymentDetails;
	private String loanPurpose;
	private String repaymentCategory;
	private Account payingAccount;
	private Account receivingAccount;
	private Fsave fsave;
	private String ClientCat;
	private String thead;
	private InterestType interestType;
	private String LOANID;
	private float loanInsuranceFee;
	private float loanFormFee;
	private float loanPhotoFee;
	private float loanFee;
	private String APPROVED;
	private float PAYAMOUNT;
	private float ApproveAmt;
	private String MEMBERID;
	private Date dateCreated;
	private Date dateUpdated;
	private Status status;
	private SystemUser createdBy;
	private SystemUser updatedBy;

	@Id
	public String getLOANID() {
		return LOANID;
	}

	public void setLOANID(String lOANID) {
		LOANID = lOANID;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	@Enumerated(EnumType.STRING)
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public SystemUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(SystemUser createdBy) {
		this.createdBy = createdBy;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public SystemUser getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(SystemUser updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Loan() {

	}

	public String getMEMBERID() {
		return MEMBERID;
	}

	public void setMEMBERID(String mEMBERID) {
		MEMBERID = mEMBERID;
	}

	public int getYear() {
		return YrCode;
	}

	public void setYear(int year) {
		this.YrCode = year;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public CustomerAccount getCustomerAccount() {
		return customerAccount;
	}

	public void setCustomerAccount(CustomerAccount customerAccount) {
		this.customerAccount = customerAccount;
	}

	public Date getLoanDisbursementDate() {
		return loanDisbursementDate;
	}

	public void setLoanDisbursementDate(Date loanDisbursementDate) {
		this.loanDisbursementDate = loanDisbursementDate;
	}

	public int getRepaymentPeriod() {
		return repaymentPeriod;
	}

	public void setRepaymentPeriod(int repaymentPeriod) {
		this.repaymentPeriod = repaymentPeriod;
	}

	public LoanCategory getLoanCategory() {
		return loanCategory;
	}

	public void setLoanCategory(LoanCategory loanCategory) {
		this.loanCategory = loanCategory;
	}

	public String getPeriodcode() {
		return periodcode;
	}

	public void setPeriodcode(String periodcode) {
		this.periodcode = periodcode;
	}

	public float getLoanAmount() {
		return LoanAmount;
	}

	public void setLoanAmount(float loanAmount) {
		this.LoanAmount = loanAmount;
	}

	public float getInterestRate() {
		return InterestRate;
	}

	public void setInterestRate(float interestRate) {
		this.InterestRate = interestRate;
	}

	public String getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public String getLoanPurpose() {
		return loanPurpose;
	}

	public void setLoanPurpose(String loanPurpose) {
		this.loanPurpose = loanPurpose;
	}

	public float getLoanFee() {
		return loanFee;
	}

	public void setLoanFee(float loanFee) {
		this.loanFee = loanFee;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Account getPayingAccount() {
		return payingAccount;
	}

	public void setPayingAccount(Account payingAccount) {
		this.payingAccount = payingAccount;
	}

	public float getLoanInsuranceFee() {
		return loanInsuranceFee;
	}

	public void setLoanInsuranceFee(float loanInsuranceFee) {
		this.loanInsuranceFee = loanInsuranceFee;
	}

	public float getLoanFormFee() {
		return loanFormFee;
	}

	public void setLoanFormFee(float loanFormFee) {
		this.loanFormFee = loanFormFee;
	}

	public float getLoanPhotoFee() {
		return loanPhotoFee;
	}

	public void setLoanPhotoFee(float loanPhotoFee) {
		this.loanPhotoFee = loanPhotoFee;
	}

	public String getRepaymentCategory() {
		return repaymentCategory;
	}

	public void setRepaymentCategory(String repaymentCategory) {
		this.repaymentCategory = repaymentCategory;
	}

	@Enumerated(EnumType.STRING)
	public InterestType getInterestType() {
		return interestType;
	}

	public void setInterestType(InterestType interestType) {
		this.interestType = interestType;
	}

//	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "loan", targetEntity = RepaymentSchedule.class)
//	public List<RepaymentSchedule> getRepaymentSchedules() {
//		return repaymentSchedules;
//	}
//
//	public void setRepaymentSchedules(List<RepaymentSchedule> repaymentSchedules) {
//		this.repaymentSchedules = repaymentSchedules;
//	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Account getReceivingAccount() {
		return receivingAccount;
	}

	public void setReceivingAccount(Account receivingAccount) {
		this.receivingAccount = receivingAccount;
	}

	public String getThead() {
		return thead;
	}

	public void setThead(String thead) {
		this.thead = thead;
	}

	@Enumerated(EnumType.STRING)
	public Fsave getFsave() {
		return fsave;
	}

	public void setFsave(Fsave fsave) {
		this.fsave = fsave;
	}

	public String getClientCat() {
		return ClientCat;
	}

	public void setClientCat(String clientCat) {
		ClientCat = clientCat;
	}

	public Date getTDATE2() {
		return TDATE2;
	}

	public void setTDATE2(Date tDATE2) {
		TDATE2 = tDATE2;
	}

	public Date getLastIntDate() {
		return LastIntDate;
	}

	public void setLastIntDate(Date lastIntDate) {
		LastIntDate = lastIntDate;
	}

	public Date getPayDate() {
		return PayDate;
	}

	public void setPayDate(Date payDate) {
		PayDate = payDate;
	}

	public String getAPPROVED() {
		return APPROVED;
	}

	public void setAPPROVED(String aPPROVED) {
		APPROVED = aPPROVED;
	}

	public float getPAYAMOUNT() {
		return PAYAMOUNT;
	}

	public void setPAYAMOUNT(float pAYAMOUNT) {
		PAYAMOUNT = pAYAMOUNT;
	}

	public float getApproveAmt() {
		return ApproveAmt;
	}

	public void setApproveAmt(float approveAmt) {
		ApproveAmt = approveAmt;
	}

}
