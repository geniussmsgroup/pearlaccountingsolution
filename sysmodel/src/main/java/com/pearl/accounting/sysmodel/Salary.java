package com.pearl.accounting.sysmodel;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "salaries")
public class Salary extends ParentEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String firstName;
	private float amount;
	private Date transactionDate;

	public Salary() {

	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

}
