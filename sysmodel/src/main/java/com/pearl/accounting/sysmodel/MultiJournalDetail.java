package com.pearl.accounting.sysmodel;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "multiJournaldetails")
public class MultiJournalDetail extends ParentEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MultiJournal multiJournal;
	private Account transactionAccount;
	private CustomerAccount customerAccount;
	private Loan loan;
	private String periodcode;

	private float amountPosted;
	private Date transactionDate;
	private String remarks;
	private Ttype ttype;
	private String acctNo;
	private String acodeC;
	private String acodeD;
	private int year;
	private String period;
	private String paydetails;
    private String TheadId;
	public MultiJournalDetail() {

	}

	public String getTheadId() {
		return TheadId;
	}

	public void setTheadId(String theadId) {
		this.TheadId = theadId;
	}

	public String getPaydetails() {
		return paydetails;
	}

	public void setPaydetails(String paydetails) {
		this.paydetails = paydetails;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public MultiJournal getMultiJournal() {
		return multiJournal;
	}

	public void setMultiJournal(MultiJournal multiJournal) {
		this.multiJournal = multiJournal;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Account getTransactionAccount() {
		return transactionAccount;
	}

	public void setTransactionAccount(Account transactionAccount) {
		this.transactionAccount = transactionAccount;
	}

	public float getAmountPosted() {
		return amountPosted;
	}

	public void setAmountPosted(float amountPosted) {
		this.amountPosted = amountPosted;
	}

	public String getPeriodcode() {
		return periodcode;
	}

	public void setPeriodcode(String periodcode) {
		this.periodcode = periodcode;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public CustomerAccount getCustomerAccount() {
		return customerAccount;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Loan getLoan() {
		return loan;
	}

	public void setCustomerAccount(CustomerAccount customerAccount) {
		this.customerAccount = customerAccount;
	}

	public void setLoan(Loan loan) {
		this.loan = loan;
	}

	@Enumerated(EnumType.STRING)
	public Ttype getTtype() {
		return ttype;
	}

	public void setTtype(Ttype ttype) {
		this.ttype = ttype;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getAcctNo() {
		return acctNo;
	}

	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}

	public String getAcodeC() {
		return acodeC;
	}

	public void setAcodeC(String acodeC) {
		this.acodeC = acodeC;
	}

	public String getAcodeD() {
		return acodeD;
	}

	public void setAcodeD(String acodeD) {
		this.acodeD = acodeD;
	}

}
