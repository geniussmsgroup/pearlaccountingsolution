package com.pearl.accounting.sysmodel;

public enum InterestType {

	R("R"), C("C");

	private String interestType;

	InterestType(String interestType) {
		this.interestType = interestType;
	}

	public String getInterestType() {
		return interestType;
	}

	public void setInterestType(String interestType) {
		this.interestType = interestType;
	}

	public static InterestType getLoanCategory(String interestType) {
		for (InterestType type : InterestType.values()) {
			if (type.getInterestType().equalsIgnoreCase(interestType)) {
				return type;
			}
		}
		return null;
	}
}
