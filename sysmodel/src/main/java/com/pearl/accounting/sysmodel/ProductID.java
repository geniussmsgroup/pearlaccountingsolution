package com.pearl.accounting.sysmodel;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

public enum ProductID {
	I("I"), E("E"),NONE("None");

	private String product;

	ProductID(String product) {
		this.product = product;
	}
	@Enumerated(EnumType.STRING)
	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public static ProductID getProduct(String product) {
		for (ProductID gd : ProductID.values()) {
			if (gd.getProduct().equalsIgnoreCase(product)) {
				return gd;
			}
		}
		return null;
	}

}
