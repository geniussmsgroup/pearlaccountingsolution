package com.pearl.accounting.sysmodel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "emailnotificationconfigurations")

public class EmailNotificationConfiguration implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String hostConfiguration;
	private String userConfiguration;
	private String passwordConfiguration;
	private String portNumberConfiguration;
	private String subjectConfiguration;
	private String messageBodyConfinguration;
	private String applicationLinkConfiguration;

	public EmailNotificationConfiguration() {

	}

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHostConfiguration() {
		return hostConfiguration;
	}

	public void setHostConfiguration(String hostConfiguration) {
		this.hostConfiguration = hostConfiguration;
	}

	public String getUserConfiguration() {
		return userConfiguration;
	}

	public void setUserConfiguration(String userConfiguration) {
		this.userConfiguration = userConfiguration;
	}

	public String getPasswordConfiguration() {
		return passwordConfiguration;
	}

	public void setPasswordConfiguration(String passwordConfiguration) {
		this.passwordConfiguration = passwordConfiguration;
	}

	public String getPortNumberConfiguration() {
		return portNumberConfiguration;
	}

	public void setPortNumberConfiguration(String portNumberConfiguration) {
		this.portNumberConfiguration = portNumberConfiguration;
	}

	public String getSubjectConfiguration() {
		return subjectConfiguration;
	}

	public void setSubjectConfiguration(String subjectConfiguration) {
		this.subjectConfiguration = subjectConfiguration;
	}

	@Column(columnDefinition = "TEXT")
	public String getMessageBodyConfinguration() {
		return messageBodyConfinguration;
	}

	public void setMessageBodyConfinguration(String messageBodyConfinguration) {
		this.messageBodyConfinguration = messageBodyConfinguration;
	}

	public String getApplicationLinkConfiguration() {
		return applicationLinkConfiguration;
	}

	public void setApplicationLinkConfiguration(String applicationLinkConfiguration) {
		this.applicationLinkConfiguration = applicationLinkConfiguration;
	}
 

}
