package com.pearl.accounting.sysmodel;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "withdrawals")
public class Withdrawal extends ParentEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String periodcode;

	private String acctNo;
	private CustomerAccount customerAccount;
	private float withdrawalAmount;
	private float withdrawalCharge;
	private Date withdrawalDate;
	private int year;
	private String period;

	private String acodeC;
	private Account payingAccount;
	private String paymentDetails;
	private String remarks;
	private String acodeD;
	private Account receivingAccount;

	public Withdrawal() {

	}

	public String getAcctNo() {
		return acctNo;
	}

	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}

	public String getAcodeC() {
		return acodeC;
	}

	public void setAcodeC(String acodeC) {
		this.acodeC = acodeC;
	}

	public String getAcodeD() {
		return acodeD;
	}

	public void setAcodeD(String acodeD) {
		this.acodeD = acodeD;
	}

	public String getPeriodcode() {
		return periodcode;
	}

	public void setPeriodcode(String periodcode) {
		this.periodcode = periodcode;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public CustomerAccount getCustomerAccount() {
		return customerAccount;
	}

	public void setCustomerAccount(CustomerAccount customerAccount) {
		this.customerAccount = customerAccount;
	}

	public float getWithdrawalAmount() {
		return withdrawalAmount;
	}

	public void setWithdrawalAmount(float withdrawalAmount) {
		this.withdrawalAmount = withdrawalAmount;
	}

	public float getWithdrawalCharge() {
		return withdrawalCharge;
	}

	public void setWithdrawalCharge(float withdrawalCharge) {
		this.withdrawalCharge = withdrawalCharge;
	}

	public Date getWithdrawalDate() {
		return withdrawalDate;
	}

	public void setWithdrawalDate(Date withdrawalDate) {
		this.withdrawalDate = withdrawalDate;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Account getPayingAccount() {
		return payingAccount;
	}

	public void setPayingAccount(Account payingAccount) {
		this.payingAccount = payingAccount;
	}

	public String getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Account getReceivingAccount() {
		return receivingAccount;
	}

	public void setReceivingAccount(Account receivingAccount) {
		this.receivingAccount = receivingAccount;
	}

}
