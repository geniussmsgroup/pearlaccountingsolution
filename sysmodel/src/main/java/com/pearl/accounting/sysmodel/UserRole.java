package com.pearl.accounting.sysmodel;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "userroles")
public class UserRole extends ParentEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String code;
	private String employeeRole;
	private String description;

	public UserRole() {

	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getEmployeeRole() {
		return employeeRole;
	}

	public void setEmployeeRole(String employeeRole) {
		this.employeeRole = employeeRole;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
