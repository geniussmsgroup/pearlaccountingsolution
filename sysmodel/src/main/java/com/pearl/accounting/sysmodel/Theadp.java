
package com.pearl.accounting.sysmodel;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "theadp")
public class Theadp extends ParentEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DataSourceScreen dataSourceScreen;

	private Account OCODE;
	private Account account;

	private String HDNO;
	private String Acode;
	private String acctNo;
	private Date Tdates;
	private int year;
	private String period;

	private String VNO;
	private float Amount;
	private String Remarks;;
	private Ttype ttype;
	private float interest;
	private CustomerAccount customerAccount;
	private Loan loanaccount;
	private String loanNumber;
	private String periodcode;

	public Theadp() {

	}

	@Enumerated(EnumType.STRING)
	public DataSourceScreen getDataSourceScreen() {
		return dataSourceScreen;
	}

	public void setDataSourceScreen(DataSourceScreen dataSourceScreen) {
		this.dataSourceScreen = dataSourceScreen;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Account getOCODE() {
		return OCODE;
	}

	public void setOCODE(Account oCODE) {
		OCODE = oCODE;
	}

	public String getHDNO() {
		return HDNO;
	}

	public void setHDNO(String hDNO) {
		HDNO = hDNO;
	}

	public String getAcode() {
		return Acode;
	}

	public void setAcode(String acode) {
		Acode = acode;
	}

	public String getAcctNo() {
		return acctNo;
	}

	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}

	public Date getTdates() {
		return Tdates;
	}

	public void setTdates(Date tdates) {
		Tdates = tdates;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getVNO() {
		return VNO;
	}

	public void setVNO(String vNO) {
		VNO = vNO;
	}

	public float getAmount() {
		return Amount;
	}

	public void setAmount(float amount) {
		Amount = amount;
	}

	public String getRemarks() {
		return Remarks;
	}

	public void setRemarks(String remarks) {
		Remarks = remarks;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@Enumerated(EnumType.STRING)
	public Ttype getTtype() {
		return ttype;
	}

	public void setTtype(Ttype ttype) {
		this.ttype = ttype;
	}

	public float getInterest() {
		return interest;
	}

	public void setInterest(float interest) {
		this.interest = interest;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public CustomerAccount getCustomerAccount() {
		return customerAccount;
	}

	public void setCustomerAccount(CustomerAccount customerAccount) {
		this.customerAccount = customerAccount;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Loan getLoanaccount() {
		return loanaccount;
	}

	public void setLoanaccount(Loan loanaccount) {
		this.loanaccount = loanaccount;
	}

	public String getLoanNumber() {
		return loanNumber;
	}

	public void setLoanNumber(String loanNumber) {
		this.loanNumber = loanNumber;
	}

	public String getPeriodcode() {
		return periodcode;
	}

	public void setPeriodcode(String periodcode) {
		this.periodcode = periodcode;
	}

}
