package com.pearl.accounting.sysmodel;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "thead")
public class Thead extends ParentEntity {

	private String hdNo;

	public Thead() {

	}

	public String getHdNo() {
		return hdNo;
	}

	public void setHdNo(String hdNo) {
		this.hdNo = hdNo;
	}

}
