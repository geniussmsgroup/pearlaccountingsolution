package com.pearl.accounting.sysmodel;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "periodicdeposits")
public class PeriodicDeposit extends ParentEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String acctNo;
	private String periodcode;
    private String Thead;
	private String acodeD;
	private String acodeC;
	private CustomerAccount customerAccount;
	private String depositedAmount;
	private int year;
	private String period;

	private Date depositDate;
	private Account receiveingAccount;
	private String paymentDetails;
	private String remarks;
	private String exchangeRate;

	private Account payingAccount;

	public PeriodicDeposit() {

	}

	public String getPeriodcode() {
		return periodcode;
	}

	public void setPeriodcode(String periodcode) {
		this.periodcode = periodcode;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public CustomerAccount getCustomerAccount() {
		return customerAccount;
	}

	public String getThead() {
		return Thead;
	}

	public void setThead(String thead) {
		Thead = thead;
	}

	public void setCustomerAccount(CustomerAccount customerAccount) {
		this.customerAccount = customerAccount;
	}

	public String getDepositedAmount() {
		return depositedAmount;
	}

	public void setDepositedAmount(String depositedAmount) {
		this.depositedAmount = depositedAmount;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public Date getDepositDate() {
		return depositDate;
	}

	public void setDepositDate(Date depositDate) {
		this.depositDate = depositDate;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Account getReceiveingAccount() {
		return receiveingAccount;
	}

	public void setReceiveingAccount(Account receiveingAccount) {
		this.receiveingAccount = receiveingAccount;
	}

	public String getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	@Column(columnDefinition = "TEXT")
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Account getPayingAccount() {
		return payingAccount;
	}

	public void setPayingAccount(Account payingAccount) {
		this.payingAccount = payingAccount;
	}

	public String getAcodeD() {
		return acodeD;
	}

	public void setAcodeD(String acodeD) {
		this.acodeD = acodeD;
	}

	public String getAcodeC() {
		return acodeC;
	}

	public void setAcodeC(String acodeC) {
		this.acodeC = acodeC;
	}

	public String getAcctNo() {
		return acctNo;
	}

	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}

}
