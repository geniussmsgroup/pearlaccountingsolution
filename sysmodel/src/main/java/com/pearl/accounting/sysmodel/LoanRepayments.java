package com.pearl.accounting.sysmodel;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "lnrepaysch")
public class LoanRepayments implements Serializable {

	private static final long serialVersionUID = 1L;
	private String Icode;
	private String ClientCode;
	private String SysId;
	private Date TDate;
	private int YrCode;
	private int LoanId;
	private float PrincipalPay;
	private float InterestPay;
	private float TotalPay;
	private float Averagepay;
	private float LoanBal;

	public String getIcode() {
		return Icode;
	}

	public void setIcode(String icode) {
		this.Icode = icode;
	}

	public String getClientCode() {
		return ClientCode;
	}

	public void setClientCode(String clientCode) {
		this.ClientCode = clientCode;
	}

	@Id
	public String getSysId() {
		return SysId;
	}

	public void setSysId(String sysId) {
		this.SysId = sysId;
	}

	public Date getTDate() {
		return TDate;
	}

	public void setTDate(Date tDate) {
		this.TDate = tDate;
	}

	public int getYrCode() {
		return YrCode;
	}

	public void setYrCode(int yrCode) {
		this.YrCode = yrCode;
	}

	public int getLoanId() {
		return LoanId;
	}

	public void setLoanId(int loanId) {
		this.LoanId = loanId;
	}

	public float getPrincipalPay() {
		return PrincipalPay;
	}

	public void setPrincipalPay(float principalPay) {
		this.PrincipalPay = principalPay;
	}

	public float getInterestPay() {
		return InterestPay;
	}

	public void setInterestPay(float interestPay) {
		this.InterestPay = interestPay;
	}

	public float getTotalPay() {
		return TotalPay;
	}

	public void setTotalPay(float totalPay) {
		this.TotalPay = totalPay;
	}

	public float getAveragepay() {
		return Averagepay;
	}

	public void setAveragepay(float averagepay) {
		this.Averagepay = averagepay;
	}

	public float getLoanBal() {
		return LoanBal;
	}

	public void setLoanBal(float loanBal) {
		this.LoanBal = loanBal;
	}

}