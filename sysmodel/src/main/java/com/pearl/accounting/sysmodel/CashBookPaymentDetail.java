package com.pearl.accounting.sysmodel;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cashbookpaymentdetails")
public class CashBookPaymentDetail extends ParentEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String period;
	private String periodcode;

	private int year;
	private CashBookPayment cashBookPayment;
	private Account receivingAccount;
	private float receivedAmount;
	private Date paymentDate;
	private String remarks;
    private String acodeD;
	public CashBookPaymentDetail() {

	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public CashBookPayment getCashBookPayment() {
		return cashBookPayment;
	}

	public void setCashBookPayment(CashBookPayment cashBookPayment) {
		this.cashBookPayment = cashBookPayment;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Account getReceivingAccount() {
		return receivingAccount;
	}

	public void setReceivingAccount(Account receivingAccount) {
		this.receivingAccount = receivingAccount;
	}
	
	

	public String getPeriodcode() {
		return periodcode;
	}

	public void setPeriodcode(String periodcode) {
		this.periodcode = periodcode;
	}

	public float getReceivedAmount() {
		return receivedAmount;
	}

	public void setReceivedAmount(float receivedAmount) {
		this.receivedAmount = receivedAmount;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getAcodeDebit() {
		return acodeD;
	}

	public void setAcodeDebit(String acodeD) {
		this.acodeD = acodeD;
	}

}
