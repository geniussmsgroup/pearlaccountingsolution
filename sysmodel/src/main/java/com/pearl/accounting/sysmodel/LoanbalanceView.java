package com.pearl.accounting.sysmodel;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;
@Entity
@Table(name = "loanbalance")
@Immutable

public class LoanbalanceView implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
private float Lnbal 	;
private String ProductID 	;
private String MemberId ;
	
public 	LoanbalanceView() {
	
}

public float getLnbal() {
	return Lnbal;
}

public void setLnbal(float lnbal) {
	Lnbal = lnbal;
}

public String getProductID() {
	return ProductID;
}

public void setProductID(String productID) {
	ProductID = productID;
}
@Id
public String getMemberId() {
	return MemberId;
}

public void setMemberId(String memberId) {
	MemberId = memberId;
}
	

}
