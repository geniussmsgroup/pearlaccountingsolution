package com.pearl.accounting.sysmodel;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "atypea")
public class AtypeA implements Serializable {
	/**
	 * cashbookpaymentdetails 
	 */
	private static final long serialVersionUID = 1L;

	private String atypecd;
	private String atypenme;
	private String abs;
	private String deftype;

	@Id
	public String getAtypecd() {
		return atypecd;
	}

	public void setAtypecd(String atypecd) {
		this.atypecd = atypecd;
	}

	public String getAtypenme() {
		return atypenme;
	}

	public void setAtypenme(String atypenme) {
		this.atypenme = atypenme;
	}

	public String getAbs() {
		return abs;
	}

	public void setAbs(String abs) {
		this.abs = abs;
	}

	public String getDeftype() {
		return deftype;
	}

	public void setDeftype(String deftype) {
		this.deftype = deftype;
	}

}
