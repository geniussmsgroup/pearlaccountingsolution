package com.pearl.accounting.sysmodel;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "clientinfo")
public class CustomerAccount implements Serializable {


	private static final long serialVersionUID = 1L;
	private String ClientCode;
	private String Surname;
	private String OtherNames;
	private Date BirthDate;
	private String gender;
	private int Age;
	private String CompanyCode;
	private String CompanyAddress;
	private String Address2;
	private String Address1;
	private String email;
	private String MaritalStatus;
	private String Recomender;
	private String Title;
	private String Department;
	private String OfficePhone;
	private String Approved;
	private Date ExportDate;
	private Date ModifyDate;
	private int RLnID;
	private String PayrollNo;
	private String NextofKin;
	private String KinAddress;
	private String KinRelation;
	private String KinContact;
	private String Kinresidence;
	private String workPlace;
	private String Occupation;
	private Fsave fsave;
	private String phoneNumber;
	private String nationalId;
	private String Sysid;
	private Date dateCreated;
	private Date dateUpdated;
	private Status status;
	private SystemUser createdBy;
	private SystemUser updatedBy;



	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public Date getDateUpdated() {
		return dateUpdated;
	}
	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	@Enumerated(EnumType.STRING)
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public SystemUser getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(SystemUser createdBy) {
		this.createdBy = createdBy;
	}
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public SystemUser getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(SystemUser updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Id
	public String getSysid() {
		return Sysid;
	}

	public void setSysid(String sysid) {
		Sysid = sysid;
	}

	public String getKinAddress() {
		return KinAddress;
	}

	public void setKinAddress(String kinAddress) {
		KinAddress = kinAddress;
	}

	public String getClientCode() {
		return ClientCode;
	}

	public void setClientCode(String clientCode) {
		ClientCode = clientCode;
	}

	public String getSurname() {
		return Surname;
	}

	public void setSurname(String surname) {
		Surname = surname;
	}

	public String getOtherNames() {
		return OtherNames;
	}

	public void setOtherNames(String otherNames) {
		OtherNames = otherNames;
	}

	public Date getBirthDate() {
		return BirthDate;
	}

	public void setBirthDate(Date birthDate) {
		BirthDate = birthDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return Age;
	}

	public void setAge(int age) {
		Age = age;
	}

	public String getCompanyCode() {
		return CompanyCode;
	}

	public void setCompanyCode(String companyCode) {
		CompanyCode = companyCode;
	}

	public String getCompanyAddress() {
		return CompanyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		CompanyAddress = companyAddress;
	}

	public String getAddress2() {
		return Address2;
	}

	public void setAddress2(String address2) {
		Address2 = address2;
	}

	public String getAddress1() {
		return Address1;
	}

	public void setAddress1(String address1) {
		Address1 = address1;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

//	public String getOccupation() {
//		return Occupation;
//	}
//	public void setOccupation(String occupation) {
//		Occupation = occupation;
//	}
	public String getMaritalStatus() {
		return MaritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		MaritalStatus = maritalStatus;
	}

//	public String getCellLocation() {
//		return CellLocation;
//	}
//	public void setCellLocation(String cellLocation) {
//		CellLocation = cellLocation;
//	}
//	public String getCellLeader() {
//		return CellLeader;
//	}
//	public void setCellLeader(String cellLeader) {
//		CellLeader = cellLeader;
//	}
	public String getRecomender() {
		return Recomender;
	}

	public void setRecomender(String recomender) {
		Recomender = recomender;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getDepartment() {
		return Department;
	}

	public void setDepartment(String department) {
		Department = department;
	}

	public String getOfficePhone() {
		return OfficePhone;
	}

	public void setOfficePhone(String officePhone) {
		OfficePhone = officePhone;
	}

	public String getApproved() {
		return Approved;
	}

	public void setApproved(String approved) {
		Approved = approved;
	}

	public Date getExportDate() {
		return ExportDate;
	}

	public void setExportDate(Date exportDate) {
		ExportDate = exportDate;
	}

	public Date getModifyDate() {
		return ModifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		ModifyDate = modifyDate;
	}

	public int getRLnID() {
		return RLnID;
	}

	public void setRLnID(int rLnID) {
		RLnID = rLnID;
	}

	public String getPayrollNo() {
		return PayrollNo;
	}

	public void setPayrollNo(String payrollNo) {
		PayrollNo = payrollNo;
	}

	public String getNextofKin() {
		return NextofKin;
	}

	public void setNextofKin(String nextofKin) {
		NextofKin = nextofKin;
	}

	public String getKinRelation() {
		return KinRelation;
	}

	public void setKinRelation(String kinRelation) {
		KinRelation = kinRelation;
	}

	public String getKinContact() {
		return KinContact;
	}

	public void setKinContact(String kinContact) {
		KinContact = kinContact;
	}

	public String getKinresidence() {
		return Kinresidence;
	}

	public void setKinresidence(String kinresidence) {
		Kinresidence = kinresidence;
	}

	public String getWorkPlace() {
		return workPlace;
	}

	public void setWorkPlace(String workPlace) {
		this.workPlace = workPlace;
	}

	public String getOccupation() {
		return Occupation;
	}

	public void setOccupation(String occupation) {
		this.Occupation = occupation;
	}

	@Enumerated(EnumType.STRING)
	public Fsave getFsave() {
		return fsave;
	}

	public void setFsave(Fsave fsave) {
		this.fsave = fsave;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getNationalId() {
		return nationalId;
	}

	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

//	public CustomerAccount() {
//
//	}
//
//	public String getAccountNo() {
//		return accountNo;
//	}
//
//	public void setAccountNo(String accountNo) {
//		this.accountNo = accountNo;
//	}
//
//	public String getFirstName() {
//		return firstName;
//	}
//
//	public void setFirstName(String firstName) {
//		this.firstName = firstName;
//	}
//
//	public String getLastName() {
//		return lastName;
//	}
//
//	public void setLastName(String lastName) {
//		this.lastName = lastName;
//	}
//
//	public Date getDob() {
//		return dob;
//	}
//
//	public void setDob(Date dob) {
//		this.dob = dob;
//	}
//
//	public Gender getGender() {
//		return gender;
//	}
//
//	public void setGender(Gender gender) {
//		this.gender = gender;
//	}
//
//	public String getNextofKin() {
//		return nextofKin;
//	}
//
//	public void setNextofKin(String nextofKin) {
//		this.nextofKin = nextofKin;
//	}
//
//	public String getRelationShip() {
//		return relationShip;
//	}
//
//	public void setRelationShip(String relationShip) {
//		this.relationShip = relationShip;
//	}
//
//	public String getRelationShipContact() {
//		return relationShipContact;
//	}
//
//	public void setRelationShipContact(String relationShipContact) {
//		this.relationShipContact = relationShipContact;
//	}
//
//	public String getResidence() {
//		return residence;
//	}
//
//	public void setResidence(String residence) {
//		this.residence = residence;
//	}
//
//	public String getWorkPlace() {
//		return workPlace;
//	}
//
//	public void setWorkPlace(String workPlace) {
//		this.workPlace = workPlace;
//	}
////
////	public String getCurrencAdress() {
////		return currencAdress;
////	}
//
//	public void setCurrencAdress(String currencAdress) {
//		this.currencAdress = currencAdress;
//	}
//
//	public String getProfession() {
//		return profession;
//	}
//
//	public void setProfession(String profession) {
//		this.profession = profession;
//	}
//
//	public String getPhoneNumber() {
//		return phoneNumber;
//	}
//
//	public void setPhoneNumber(String phoneNumber) {
//		this.phoneNumber = phoneNumber;
//	}
//
//	public String getEmailAddress() {
//		return emailAddress;
//	}
//
//	public void setEmailAddress(String emailAddress) {
//		this.emailAddress = emailAddress;
//	}
//
//	public String getNationalId() {
//		return nationalId;
//	}
//
//	public void setNationalId(String nationalId) {
//		this.nationalId = nationalId;
//	}

}
