package com.pearl.accounting.sysmodel;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "transml")

public class TransML extends ParentEntity {

	private static final long serialVersionUID = 1L;

	private String MEMBERID;
	private String PRODUCTID;
	private int LOANID;
	private Date TDATE;
	private Ttype TTYPE;
	private double AMOUNT;
	private String PAYDET;
	private String VNO;
	private String PTRANS;
	private Fsave FSAVE;
	private String BFBAL;
//private double JNLID;
	private String THEADID;
	private String STAFFNAME;
	private String SYSID;
	private CustomerAccount customeraccount;
	private Loan loan;
	private DataSourceScreen dataSourceScreen;

//private Status status;	
	public TransML() {

	}

	public String getPRODUCTID() {
		return PRODUCTID;
	}

	public void setPRODUCTID(String pRODUCTID) {
		PRODUCTID = pRODUCTID;
	}

	public String getMEMBERID() {
		return MEMBERID;
	}

	public void setMEMBERID(String mEMBERID) {
		MEMBERID = mEMBERID;
	}

	public int getLOANID() {
		return LOANID;
	}

	public void setLOANID(int lOANID) {
		LOANID = lOANID;
	}

	public Date getTDATE() {
		return TDATE;
	}

	public void setTDATE(Date tDATE) {
		TDATE = tDATE;
	}

	@Enumerated(EnumType.STRING)
	public Ttype getTTYPE() {
		return TTYPE;
	}

	public void setTTYPE(Ttype tTYPE) {
		TTYPE = tTYPE;
	}

	public double getAMOUNT() {
		return AMOUNT;
	}

	public void setAMOUNT(double aMOUNT) {
		AMOUNT = aMOUNT;
	}

	public String getPAYDET() {
		return PAYDET;
	}

	public void setPAYDET(String pAYDET) {
		PAYDET = pAYDET;
	}

	public String getVNO() {
		return VNO;
	}

	public void setVNO(String vNO) {
		VNO = vNO;
	}

	public String getPTRANS() {
		return PTRANS;
	}

	public void setPTRANS(String pTRANS) {
		PTRANS = pTRANS;
	}

	@Enumerated(EnumType.STRING)
	public Fsave getFSAVE() {
		return FSAVE;
	}

	public void setFSAVE(Fsave fSAVE) {
		FSAVE = fSAVE;
	}

	public String getBFBAL() {
		return BFBAL;
	}

	public void setBFBAL(String bFBAL) {
		BFBAL = bFBAL;
	}

	public String getTHEADID() {
		return THEADID;
	}

	public void setTHEADID(String tHEADID) {
		THEADID = tHEADID;
	}

	public String getSTAFFNAME() {
		return STAFFNAME;
	}

	public void setSTAFFNAME(String sTAFFNAME) {
		STAFFNAME = sTAFFNAME;
	}

	@Id
	public String getSYSID() {
		return SYSID;
	}

	public void setSYSID(String sYSID) {
		SYSID = sYSID;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public CustomerAccount getCustomeraccount() {
		return customeraccount;
	}

	public void setCustomeraccount(CustomerAccount customeraccount) {
		this.customeraccount = customeraccount;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public Loan getLoan() {
		return loan;
	}

	public void setLoan(Loan loan) {
		this.loan = loan;
	}

	@Enumerated(EnumType.STRING)
	public DataSourceScreen getDataSourceScreen() {
		return dataSourceScreen;
	}

	public void setDataSourceScreen(DataSourceScreen dataSourceScreen) {
		this.dataSourceScreen = dataSourceScreen;
	}

}