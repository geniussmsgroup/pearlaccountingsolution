package com.pearl.accounting.sysmodel;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Entity
@Table(name ="MY")
public class My extends ParentEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String accountCode;
	private String accountName;
	private AccountType accountType;
	private AccountDefinition accountDefinition;
	
	private boolean trackPData;
	
	private ProductType productType;

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	@Enumerated(EnumType.STRING)
	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	@Enumerated(EnumType.STRING)
	public AccountDefinition getAccountDefinition() {
		return accountDefinition;
	}

	public void setAccountDefinition(AccountDefinition accountDefinition) {
		this.accountDefinition = accountDefinition;
	}

	public boolean isTrackPData() {
		return trackPData;
	}

	public void setTrackPData(boolean trackPData) {
		this.trackPData = trackPData;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

}
