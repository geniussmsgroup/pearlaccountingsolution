package com.pearl.accounting.sysmodel;

public enum Ttype {

	D("D"), C("C"),d("d");
	private String type;

	Ttype(String type) {
		this.type = type;
	}

	public String getTransactionType() {
		return type;
	}

	public void setTransactionType(String type) {
		this.type = type;
	}

	public static Ttype getTransactionType(String transactionType) {
		for (Ttype type : Ttype.values()) {
			if (type.getTransactionType().equalsIgnoreCase(transactionType)) {
				return type;
			}
		}
		return null;
	}

}
