package com.pearl.accounting.sysmodel;

public enum Fsave {

	Y("N"), N("N"),D("D");

	private String fsave;

	Fsave(String definition) {
		this.fsave = definition;
	}

	public String getDefinition() {
		return fsave;
	}

	public void setDefinition(String fsave) {
		this.fsave = fsave;
	}

	public static Fsave getDefinition(String definition) {
		for (Fsave gd : Fsave.values()) {
			if (gd.getDefinition().equalsIgnoreCase(definition)) {
				return gd;
			}
		}
		return null;
	}

}
