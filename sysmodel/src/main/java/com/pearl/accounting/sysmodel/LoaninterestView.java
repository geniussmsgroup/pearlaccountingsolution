package com.pearl.accounting.sysmodel;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;
@Entity
@Table(name = "loaninterestqry")
@Immutable

public class LoaninterestView implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

private String MemberId ;
	private int  LoanID  ;
	private float InterestRate;
	
	
	
	
	
	
public 	LoaninterestView() {
	
}


public int getLoanID() {
	return LoanID;
}


public void setLoanID(int loanID) {
	this.LoanID = loanID;
}


public float getInterestRate() {
	return InterestRate;
}


public void setInterestRate(float interestRate) {
	this.InterestRate = interestRate;
}


@Id
public String getMemberId() {
	return MemberId;
}

public void setMemberId(String memberId) {
	MemberId = memberId;
}
	

}
