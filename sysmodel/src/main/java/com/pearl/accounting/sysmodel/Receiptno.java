package com.pearl.accounting.sysmodel;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "rtvrdet")
public class Receiptno implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private int transactionid;
	private String AMTWORDS;
	private String RTVRNO;
	private int TheadID;
	private double Amount;

	public Receiptno() {

	}

	@Id
   //// @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "TID")
	public int getTransactionid() {
		return transactionid;
	}

	public void setTransactionid(int transactionid) {
		this.transactionid = transactionid;
	}

	public String getAMTWORDS() {
		return AMTWORDS;
	}

	public void setAMTWORDS(String aMTWORDS) {
		AMTWORDS = aMTWORDS;
	}

	public String getRTVRNO() {
		return RTVRNO;
	}

	public void setRTVRNO(String  rTVRNO) {
		RTVRNO = rTVRNO;
	}

	public int getTheadID() {
		return TheadID;
	}

	public void setTheadID(int theadID) {
		TheadID = theadID;
	}

	public double getAmount() {
		return Amount;
	}

	public void setAmount(double amount) {
		Amount = amount;
	}

}
