package com.pearl.accounting.sysmodel;

public enum DataSourceScreen {
	Loan("Loan"), Savings("Savings"), Shares("Shares"),LoanDeposit("LoanDeposit"), LoanInterest("Loan Interest"), SavingsInterest(
			"Savings interest"), LoanRepayment("LoanRepayment"),Multi("mulit"), WITHDRAWAL("Withdrawal"), CASH_BOOK_PAYMENTS(
					"Cash book payments"), CASH_BOOK_RECEIPTS("Cash book receipts"), MULTI_JOURNAL("Multi-Journal"),DoubleEntry("DOUBLEENTRY");

	private String type;

	DataSourceScreen(String type) {
		this.type = type;
	}

	public String getDataSourceScreen() {
		return type;
	}

	public void setDataSourceScreen(String type) {
		this.type = type;
	}

	public static DataSourceScreen getDataSourceScreen(String source) {
		for (DataSourceScreen type : DataSourceScreen.values()) {
			if (type.getDataSourceScreen().equalsIgnoreCase(source)) {
				return type;
			}
		}
		return null;
	}
}
