package com.pearl.accounting.sysmodel;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "account")
public class Account implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String ACODE;
	private String accountName;
	private AtypeA accountType;
	private String accountDefinition;
	private String ATYPECD;
	private Date RDATE;
	private String CurrencyCd;
	private int Xrate;
	// private double IBAL;
//	private String ITYPE;
	private String SYSA;
	private String SysName;
	private double PSTAT;
	private double PSTATOC;
	private String GBUDGET;
	private Fsave fsave;
	private float CBAL;
	private String CTYPE;
	private String PSTYPE;
	private double SysBal;
	private String AnalReq;
	private ProductSetup productType;
	private String productid;
	private String Sysid;
	private Date dateCreated;
	private Date dateUpdated;
	private Status status;
	private SystemUser createdBy;
	private SystemUser updatedBy;

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	@Enumerated(EnumType.STRING)
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public SystemUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(SystemUser createdBy) {
		this.createdBy = createdBy;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public SystemUser getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(SystemUser updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getATYPECD() {
		return ATYPECD;
	}

	@Id
	public String getSysid() {
		return Sysid;
	}

	public void setSysid(String sysid) {
		Sysid = sysid;
	}

	public void setATYPECD(String aTYPECD) {
		this.ATYPECD = aTYPECD;
	}

	public String getProductid() {
		return productid;
	}

	public void setProductid(String productid) {
		this.productid = productid;
	}

	public String getACODE() {
		return ACODE;
	}

	public void setACODE(String ACODE) {
		this.ACODE = ACODE;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public AtypeA getAccountType() {
		return accountType;
	}

	public void setAccountType(AtypeA accountType) {
		this.accountType = accountType;
	}

	public String getAccountDefinition() {
		return accountDefinition;
	}

	public void setAccountDefinition(String accountDefinition) {
		this.accountDefinition = accountDefinition;
	}

	public Date getRDATE() {
		return RDATE;
	}

	public void setRDATE(Date rDATE) {
		this.RDATE = rDATE;
	}

	public String getCurrencyCd() {
		return CurrencyCd;
	}

	public void setCurrencyCd(String currencyCd) {
		this.CurrencyCd = currencyCd;
	}

	public int getXrate() {
		return Xrate;
	}

	public void setXrate(int xrate) {
		this.Xrate = xrate;
	}

//	public double getIBAL() {
//		return IBAL;
//	}
//
//	public void setIBAL(double iBAL) {
//		this.IBAL = iBAL;
//	}
//
//	public String getITYPE() {
//		return ITYPE;
//	}
//
//	public void setITYPE(String iTYPE) {
//		this.ITYPE = iTYPE;
//	}

	public String getSYSA() {
		return SYSA;
	}

	public void setSYSA(String sYSA) {
		this.SYSA = sYSA;
	}

	public String getSysName() {
		return SysName;
	}

	public void setSysName(String sysName) {
		this.SysName = sysName;
	}

	public double getPSTAT() {
		return PSTAT;
	}

	public void setPSTAT(double pSTAT) {
		this.PSTAT = pSTAT;
	}

	public double getPSTATOC() {
		return PSTATOC;
	}

	public void setPSTATOC(double pSTATOC) {
		this.PSTATOC = pSTATOC;
	}

	public String getGBUDGET() {
		return GBUDGET;
	}

	public void setGBUDGET(String gBUDGET) {
		this.GBUDGET = gBUDGET;
	}

	@Enumerated(EnumType.STRING)
	public Fsave getFsave() {
		return fsave;
	}

	public void setFsave(Fsave fsave) {
		this.fsave = fsave;
	}

	public float getCBAL() {
		return CBAL;
	}

	public void setCBAL(float cBAL) {
		this.CBAL = cBAL;
	}

	public String getCTYPE() {
		return CTYPE;
	}

	public void setCTYPE(String cTYPE) {
		this.CTYPE = cTYPE;
	}

	public String getPSTYPE() {
		return PSTYPE;
	}

	public void setPSTYPE(String pSTYPE) {
		this.PSTYPE = pSTYPE;
	}

	public double getSysBal() {
		return SysBal;
	}

	public void setSysBal(double sysBal) {
		this.SysBal = sysBal;
	}

	public String getAnalReq() {
		return AnalReq;
	}

	public void setAnalReq(String analReq) {
		this.AnalReq = analReq;
	}

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	public ProductSetup getProductType() {
		return productType;
	}

	public void setProductType(ProductSetup productType) {
		this.productType = productType;
	}

//	@Enumerated(EnumType.STRING)

//	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)

//	public float getCBAL() {
//		return CBAL;
//	}
//
//	public void setCBAL(float cBAL) {
//		CBAL = cBAL;
//	}
//
//	public Ttype getCTYPE() {
//		return CTYPE;
//	}
//
//	public void setCTYPE(Ttype cTYPE) {
//		CTYPE = cTYPE;
//	}
//
//	public float getPSTAT() {
//		return PSTAT;
//	}
//
//	public void setPSTAT(float pSTAT) {
//		PSTAT = pSTAT;
//	}
//
//	@Enumerated(EnumType.STRING)
//	public Ttype getPSTYPE() {
//		return PSTYPE;
//	}
//
//	public void setPSTYPE(Ttype pSTYPE) {
//		PSTYPE = pSTYPE;
//	}
//
//	public float getSysBal() {
//		return SysBal;
//	}
//
//	public void setSysBal(float sysBal) {
//		SysBal = sysBal;
//	}
//
//	public String getAccountCode() {
//		return accountCode;
//	}
//
//	public void setAccountCode(String accountCode) {
//		this.accountCode = accountCode;
//	}
//
//	public String getAccountName() {
//		return accountName;
//	}
//
//	public void setAccountName(String accountName) {
//		this.accountName = accountName;
//	}
//
//	@Enumerated(EnumType.STRING)
//	public AccountDefinition getAccountDefinition() {
//		return accountDefinition;
//	}
//
//	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
//
//	public void setAccountDefinition(AccountDefinition accountDefinition) {
//		this.accountDefinition = accountDefinition;
//	}
//
//	public boolean isTrackPData() {
//		return trackPData;
//	}
//
//	public void setTrackPData(boolean trackPData) {
//		this.trackPData = trackPData;
//	}
//	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
//	public AtypeA getAccountType() {
//		return accountType;
//	}
//
//	public void setAccountType(AtypeA accountType) {
//		this.accountType = accountType;
//	}
//	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
//	public ProductSetup getProductType() {
//		return productType;
//	}
//
//	public void setProductType(ProductSetup productType) {
//		this.productType = productType;
//	}
//
//}
}